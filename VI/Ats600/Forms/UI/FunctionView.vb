'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\FunctionView.vb
'
' summary:	Function view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A Function view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class FunctionView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="FunctionView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="FunctionView"/>. </returns>
    Public Shared Function Create() As FunctionView
        Dim view As FunctionView = Nothing
        Try
            view = New FunctionView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As Ats600Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As Ats600Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As Ats600Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindThermostreamSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As Ats600Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " THERMO STREAM "

    ''' <summary> Gets or sets the Thermostream subsystem. </summary>
    ''' <value> The Thermostream subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ThermostreamSubsystem As VI.Ats600.ThermostreamSubsystem

    ''' <summary> Bind Thermostream subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindThermostreamSubsystem(ByVal device As Ats600.Ats600Device)
        If Me.ThermostreamSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ThermostreamSubsystem)
            Me._ThermostreamSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._ThermostreamSubsystem = device.ThermostreamSubsystem
            Me.BindSubsystem(True, Me.ThermostreamSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ThermostreamSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ThermostreamSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(Ats600.ThermostreamSubsystem.IsHeadUp))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsHeadUp))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsReady))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.CycleCount))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.MaximumTestTime))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.RampRate))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.SetpointNumber))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.Setpoint))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.SetpointWindow))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.SoakTime))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsAtTemperature))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsCycleCompleted))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsCyclesCompleted))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsCyclingStopped))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsTestTimeElapsed))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Ats600.ThermostreamSubsystem.IsNotAtTemperature))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ThermostreamSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ThermostreamSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsHeadUp)
                Me._HeadDownCheckBox.Checked = Not subsystem.IsHeadUp
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsReady)
                Me._ReadyCheckBox.Checked = subsystem.IsReady
            Case NameOf(VI.Ats600.ThermostreamSubsystem.CycleCount)
                Me._CycleCountNumeric.ValueSetter(CType(subsystem.CycleCount, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.MaximumTestTime)
                Me._MaxTestTimeNumeric.ValueSetter(CType(subsystem.MaximumTestTime, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.RampRate)
                Me._RampRateNumeric.ValueSetter(CType(subsystem.RampRate, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.SetpointNumber)
                Me._SetpointNumberNumeric.ValueSetter(CType(subsystem.SetpointNumber, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.Setpoint)
                Me._SetpointNumeric.ValueSetter(CType(subsystem.Setpoint, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.SetpointWindow)
                Me._SetpointWindowNumeric.ValueSetter(CType(subsystem.SetpointWindow, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.SoakTime)
                Me._SoakTimeNumeric.ValueSetter(CType(subsystem.SoakTime, Decimal?))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsAtTemperature)
                Me._AtTempCheckBox.Checked = subsystem.IsAtTemperature
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsCycleCompleted)
                Me._CycleCompletedCheckBox1.CheckBoxControl.Checked = subsystem.IsCycleCompleted
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsCyclesCompleted)
                Me._CyclesCompletedCheckBox1.CheckBoxControl.Checked = subsystem.IsCyclesCompleted
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsCyclingStopped)
                Me._CyclingStoppedCheckBox.CheckBoxControl.Checked = subsystem.IsCyclingStopped
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsTestTimeElapsed)
                Me._TestTimeElapsedCheckBox.Checked = subsystem.IsTestTimeElapsed
            Case NameOf(VI.Ats600.ThermostreamSubsystem.IsNotAtTemperature)
                Me._NotAtTempCheckBox.Checked = subsystem.IsNotAtTemperature
        End Select
    End Sub

    ''' <summary> Sense Current subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ThermostreamSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ThermostreamSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ThermostreamSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ThermostreamSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: CYCLE PANEL "

    ''' <summary> Searches for the last setpoint button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FindLastSetpointButton_Click(sender As System.Object, e As System.EventArgs) Handles _FindLastSetpointButton1.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} finding last setpoint"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me._FindLastSetpointButton1.Text = String.Format("Find Last Setpoint: {0}", Me.Device.ThermostreamSubsystem.FindLastSetpointNumber)
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Queries temporary events. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub QueryTempEvents(sender As System.Object)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading temperature events"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.QueryTemperatureEventStatus()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Queries auxiliary status. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub QueryAuxiliaryStatus(sender As System.Object)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading auxiliary event status"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.QueryAuxiliaryEventStatus()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by QueryTempEventButton for click events. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub QueryTempEventButton_Click(sender As System.Object, e As System.EventArgs)
        Me.QueryTempEvents(sender)
    End Sub

    ''' <summary> Queries auxiliary status button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub QueryAuxiliaryStatusButton_Click(sender As System.Object, e As System.EventArgs) Handles _QueryStatusButton.Click
        Me.QueryAuxiliaryStatus(sender)
        Me.QueryTempEvents(sender)
    End Sub

    ''' <summary> Setpoint number numeric value selected. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetpointNumberNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _SetpointNumberNumeric.ValueSelected
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} selecting setpoint"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                If Me._SetpointNumberNumeric.SelectedValue.HasValue Then
                    Me.Device.ThermostreamSubsystem.WriteSetpointNumber(CInt(Me._SetpointNumberNumeric.Value))
                    Windows.Forms.Application.DoEvents()
                End If
                Me.Device.ThermostreamSubsystem.QuerySetpointNumber()
                Windows.Forms.Application.DoEvents()
                Me.Device.ThermostreamSubsystem.ReadCurrentSetpointValues()
                Windows.Forms.Application.DoEvents()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Queries head status. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub QueryHeadStatus(sender As System.Object)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading head status"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.QueryHeadDown()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads set point button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadSetpointButton_Click(sender As System.Object, e As System.EventArgs) Handles _ReadSetpointButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading the setpoint values"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.ReadCurrentSetpointValues()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Next button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub NextButton_Click(sender As System.Object, e As System.EventArgs) Handles _NextSetpointButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} moving to the next setpoint"
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.NextSetpoint()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Maximum test time numeric value selected. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MaxTestTimeNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _MaxTestTimeNumeric.ValueSelected
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} setting and reading the maximum test time"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                If Me._MaxTestTimeNumeric.SelectedValue.HasValue Then
                    Me.Device.ThermostreamSubsystem.WriteMaximumTestTime(Me._MaxTestTimeNumeric.SelectedValue.Value)
                End If
                Me.Device.ThermostreamSubsystem.QueryMaximumTestTime()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Cycle count numeric value selected. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CycleCountNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _CycleCountNumeric.ValueSelected
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} setting and reading the cycle count"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen AndAlso Me._CycleCountNumeric.SelectedValue.HasValue Then
                Me.Device.ThermostreamSubsystem.ApplyCycleCount(CInt(Me._CycleCountNumeric.SelectedValue.Value))
                Me.Device.ThermostreamSubsystem.QueryCycleCount()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Starts button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartButton_Click(sender As System.Object, e As System.EventArgs) Handles _StartCycleButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} starting cycling"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.StartCycling()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Stops cycle button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StopCycleButton_Click(sender As System.Object, e As System.EventArgs) Handles _StopCycleButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} stopping cycling"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.ThermostreamSubsystem.StopCycling()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Resets the system mode. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="resetOperator"> True to reset operator. </param>
    ''' <param name="sender">        Source of the event. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ResetSystemMode(ByVal resetOperator As Boolean, sender As System.Object)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} resetting system mode"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Dim refrectoryPeriod As TimeSpan = TimeSpan.Zero
                If resetOperator Then
                    refrectoryPeriod = Me.Device.ThermostreamSubsystem.ResetOperatorScreenRefractoryTimeSpan
                    Me.Device.ThermostreamSubsystem.ResetOperatorScreen()
                Else
                    refrectoryPeriod = Me.Device.ThermostreamSubsystem.ResetCycleScreenRefractoryTimeSpan
                    Me.Device.ThermostreamSubsystem.ResetCycleScreen()
                End If
                Me.PublishInfo("Awaiting reset;. ")
                If Me.Device.IsSessionOpen Then
                    isr.Core.ApplianceBase.DoEventsWait(refrectoryPeriod)
                End If
                Me.Device.ThermostreamSubsystem.QuerySystemScreen()
                Me._ResetOperatorModeButton.Text = If(Me.Device.ThermostreamSubsystem.OperatorScreen.HasValue,
                        String.Format("Operator Mode: {0}", IIf(Me.Device.ThermostreamSubsystem.OperatorScreen.Value, "ON", "OFF")),
                        String.Format("Operator Mode: {0}", "N/A"))
                Me._ResetCycleModeButton.Text = If(Me.Device.ThermostreamSubsystem.CycleScreen.HasValue,
                        String.Format("Cycle Mode: {0}", IIf(Me.Device.ThermostreamSubsystem.CycleScreen.Value, "ON", "OFF")),
                        String.Format("Cycle Mode: {0}", "N/A"))
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Resets the operator mode button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResetOperatorModeButton_Click(sender As System.Object, e As System.EventArgs) Handles _ResetOperatorModeButton.Click
        Me.ResetSystemMode(True, sender)
    End Sub

    ''' <summary> Resets the cycle mode button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResetCycleModeButton_Click(sender As System.Object, e As System.EventArgs) Handles _ResetCycleModeButton.Click
        Me.ResetSystemMode(False, sender)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
