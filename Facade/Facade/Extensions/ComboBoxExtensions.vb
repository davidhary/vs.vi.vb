'---------------------------------------------------------------------------------------------------
' file:		.\Extensions\ComboBoxExtensions.vb
'
' summary:	Combo box extensions class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices

Imports isr.Core.EnumExtensions

Namespace ComboBoxExtensions

    Partial Public Module Methods

#Region " ARM SOURCES "

        ''' <summary> Lists the arm sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">         The list control. </param>
        ''' <param name="supportedArmSources"> The supported arm sources. </param>
        <Extension>
        Public Sub ListSupportedArmSources(ByVal listControl As ListControl, ByVal supportedArmSources As VI.ArmSources)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.DataSource = GetType(VI.ArmSources).EnumValues.IncludeFilter(supportedArmSources).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub

        ''' <summary> Lists the arm sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">         The list control. </param>
        ''' <param name="supportedArmSources"> The supported arm sources. </param>
        <Extension>
        Public Sub ListSupportedArmSources(ByVal listControl As ComboBox, ByVal supportedArmSources As VI.ArmSources)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim selectedIndex As Integer = listControl.SelectedIndex
            listControl.DataSource = Nothing
            listControl.Items.Clear()
            listControl.DataSource = GetType(VI.ArmSources).EnumValues.IncludeFilter(supportedArmSources).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If listControl.Items.Count > 0 Then
                listControl.SelectedIndex = Math.Min(listControl.Items.Count - 1, Math.Max(selectedIndex, 0))
            End If
        End Sub


#End Region

#Region " ADATER TYPE "

        ''' <summary> List adapter types. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">              The list control. </param>
        ''' <param name="supportedAdapterTypes"> List of types of the supported adapters. </param>
        <Extension>
        Public Sub ListSupportedAdapters(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedAdapterTypes As VI.AdapterTypes)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.AdapterTypes).EnumValues.IncludeFilter(supportedAdapterTypes).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the adapter type selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The SenseAdapterType. </returns>
        <Extension>
        Public Function SelectedAdapterType(ByVal listControl As Windows.Forms.ListControl) As VI.AdapterTypes
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.AdapterTypes)
        End Function

        ''' <summary> Select adapter type. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">    The list control. </param>
        ''' <param name="adapterType"> The adapter type. </param>
        ''' <returns> A VI.Scpi.AdapterType. </returns>
        <Extension>
        Public Function SelectAdapterType(ByVal comboBox As Windows.Forms.ComboBox, ByVal adapterType As VI.AdapterTypes?) As VI.AdapterTypes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If adapterType.HasValue AndAlso adapterType.Value <> VI.AdapterTypes.None AndAlso adapterType.Value <> comboBox.SelectedAdapterType Then
                comboBox.SelectedItem = adapterType.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedAdapterType
        End Function

        ''' <summary> Safe select adapter type. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">    The list control. </param>
        ''' <param name="adapterType"> The adapter type. </param>
        ''' <returns> The VI.AdapterTypes. </returns>
        <Extension>
        Public Function SafeSelectAdapterType(ByVal comboBox As Windows.Forms.ComboBox, ByVal adapterType As VI.AdapterTypes?) As VI.AdapterTypes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If adapterType.HasValue AndAlso adapterType.Value <> VI.AdapterTypes.None AndAlso adapterType.Value <> comboBox.SelectedAdapterType Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.AdapterTypes)(AddressOf Methods.SafeSelectAdapterType), New Object() {comboBox, adapterType})
                Else
                    comboBox.SelectedItem = adapterType.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedAdapterType
        End Function

#End Region

#Region " DIGITAL ACTIVE LEVEL "

        ''' <summary> List digital active levels. </summary>
        ''' <remarks> David, 2020-11-13. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">                      The list control. </param>
        ''' <param name="supportedListDigitalActiveLevels"> The supported list digital active levels. </param>
        <Extension>
        Public Sub ListDigitalActiveLevels(ByVal listControl As ListControl, ByVal supportedListDigitalActiveLevels As VI.DigitalActiveLevels)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.DataSource = GetType(VI.DigitalActiveLevels).EnumValues.IncludeFilter(supportedListDigitalActiveLevels).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub

        ''' <summary> List digital active levels. </summary>
        ''' <remarks> David, 2020-11-13. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">                      The list control. </param>
        ''' <param name="supportedListDigitalActiveLevels"> The supported list digital active levels. </param>
        <Extension>
        Public Sub ListDigitalActiveLevels(ByVal listControl As ComboBox, ByVal supportedListDigitalActiveLevels As VI.DigitalActiveLevels)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.Items.Clear()
            listControl.DataSource = GetType(VI.DigitalActiveLevels).EnumValues.IncludeFilter(supportedListDigitalActiveLevels).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub


#End Region

#Region " FEED CONTROLS "

        ''' <summary> Lists the Feed Controls. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">           The list control. </param>
        ''' <param name="supportedFeedControls"> The supported Feed Controls. </param>
        <Extension>
        Public Sub ListSupportedFeedControls(ByVal listControl As ListControl, ByVal supportedFeedControls As VI.FeedControls)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.DataSource = GetType(VI.FeedControls).EnumValues.IncludeFilter(supportedFeedControls).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub

        ''' <summary> Lists the Feed Controls. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">           The list control. </param>
        ''' <param name="supportedFeedControls"> The supported Feed Controls. </param>
        <Extension>
        Public Sub ListSupportedFeedControls(ByVal listControl As ComboBox, ByVal supportedFeedControls As VI.FeedControls)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.Items.Clear()
            listControl.DataSource = GetType(VI.FeedControls).EnumValues.IncludeFilter(supportedFeedControls).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub

#End Region

#Region " FEED SOURCES "

        ''' <summary> Lists the Feed sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">          The list control. </param>
        ''' <param name="supportedFeedSources"> The supported Feed sources. </param>
        <Extension>
        Public Sub ListSupportedFeedSources(ByVal listControl As ListControl, ByVal supportedFeedSources As VI.FeedSources)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.DataSource = GetType(VI.FeedSources).EnumValues.IncludeFilter(supportedFeedSources).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub

        ''' <summary> Lists the Feed sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">          The list control. </param>
        ''' <param name="supportedFeedSources"> The supported Feed sources. </param>
        <Extension>
        Public Sub ListSupportedFeedSources(ByVal listControl As ComboBox, ByVal supportedFeedSources As VI.FeedSources)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.Items.Clear()
            listControl.DataSource = GetType(VI.FeedSources).EnumValues.IncludeFilter(supportedFeedSources).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub


#End Region

#Region " MULTIMETER FUNCTION MODE "

        ''' <summary> List Multimeter function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The list control. </param>
        <Extension>
        Public Sub ListMultimeterFunctionModes(ByVal comboBox As Windows.Forms.ComboBox)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.MultimeterFunctionModes).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the Multimeter function mode selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The MultimeterMultimeterFunctionModes. </returns>
        <Extension>
        Public Function SelectedMultimeterFunctionMode(ByVal listControl As Windows.Forms.ListControl) As VI.MultimeterFunctionModes
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.MultimeterFunctionModes)
        End Function

        ''' <summary> Select multimeter function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">               The list control. </param>
        ''' <param name="multimeterFunctionMode"> The multimeter function modes. </param>
        ''' <returns> The vi.tsp2.MultimeterFunctionMode. </returns>
        <Extension>
        Public Function SelectMultimeterFunctionMode(ByVal comboBox As Windows.Forms.ComboBox, ByVal multimeterFunctionMode As VI.MultimeterFunctionModes?) As VI.MultimeterFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If multimeterFunctionMode.HasValue AndAlso multimeterFunctionMode.Value <> VI.MultimeterFunctionModes.None AndAlso multimeterFunctionMode.Value <> comboBox.SelectedMultimeterFunctionMode Then
                comboBox.SelectedItem = multimeterFunctionMode.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedMultimeterFunctionMode
        End Function

        ''' <summary> Safe select multimeter function mode. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">               The list control. </param>
        ''' <param name="multimeterFunctionMode"> The multimeter function mode. </param>
        ''' <returns> A VI.Tsp2.MultimeterFunctionMode. </returns>
        <Extension>
        Public Function SafeSelectMultimeterFunctionMode(ByVal comboBox As Windows.Forms.ComboBox, ByVal multimeterFunctionMode As VI.MultimeterFunctionModes?) As VI.MultimeterFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If multimeterFunctionMode.HasValue AndAlso multimeterFunctionMode.Value <> VI.MultimeterFunctionModes.None AndAlso multimeterFunctionMode.Value <> comboBox.SelectedMultimeterFunctionMode Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.MultimeterFunctionModes?)(AddressOf Methods.SafeSelectMultimeterFunctionMode), New Object() {comboBox, multimeterFunctionMode})
                Else
                    comboBox.SelectedItem = multimeterFunctionMode.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedMultimeterFunctionMode
        End Function

#End Region

#Region " READING ELEMENT TYPES "

        ''' <summary> List Reading Element Types. </summary>
        ''' <param name="comboBox">    The list control. </param>
        ''' <param name="includeMask"> The include mask. </param>
        ''' <param name="excludeMask"> The exclude mask. </param>
        <Extension>
        Public Sub ListReadingElementTypes(ByVal comboBox As Windows.Forms.ComboBox, ByVal includeMask As VI.ReadingElementTypes, ByVal excludeMask As VI.ReadingElementTypes)
            Methods.ListSupportedReadingElementTypes(comboBox, includeMask And Not excludeMask)
        End Sub

        ''' <summary> List supported reading Element types. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                     The list control. </param>
        ''' <param name="supportedReadingElementTypes"> List of types of the supported reading element. </param>
        <Extension>
        Public Sub ListSupportedReadingElementTypes(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedReadingElementTypes As VI.ReadingElementTypes)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.ReadingElementTypes).EnumValues.IncludeFilter(supportedReadingElementTypes).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the Reading Element Types selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The VI.ReadingElementTypes. </returns>
        <Extension>
        Public Function SelectedReadingElementType(ByVal listControl As Windows.Forms.ListControl) As VI.ReadingElementTypes
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.ReadingElementTypes)
        End Function

        ''' <summary> Select Reading Element Type. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">           The list control. </param>
        ''' <param name="readingElementType"> Types of the reading element. </param>
        ''' <returns> The VI.ReadingElementTypes. </returns>
        <Extension>
        Public Function SelectReadingElementType(ByVal comboBox As Windows.Forms.ComboBox, ByVal readingElementType As VI.ReadingElementTypes?) As VI.ReadingElementTypes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If readingElementType.HasValue AndAlso readingElementType.Value <> VI.ReadingElementTypes.None AndAlso readingElementType.Value <> comboBox.SelectedReadingElementType Then
                comboBox.SelectedItem = readingElementType.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedReadingElementType
        End Function

        ''' <summary> Thread Safe select Reading Element Types. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">           The list control. </param>
        ''' <param name="readingElementType"> The Reading Element Type. </param>
        ''' <returns> The VI.ReadingElementTypes. </returns>
        <Extension>
        Public Function SafeSelectReadingElementType(ByVal comboBox As Windows.Forms.ComboBox, ByVal readingElementType As VI.ReadingElementTypes?) As VI.ReadingElementTypes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If readingElementType.HasValue AndAlso readingElementType.Value <> VI.ReadingElementTypes.None AndAlso readingElementType.Value <> comboBox.SelectedReadingElementType Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.ReadingElementTypes?)(AddressOf Methods.SafeSelectReadingElementType), New Object() {comboBox, readingElementType})
                Else
                    comboBox.SelectedItem = readingElementType.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedReadingElementType
        End Function

#End Region

#Region " RESISTANCE RANGE CURRENT "

        ''' <summary> List resistance range currents. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                The list control. </param>
        ''' <param name="resistanceRangeCurrents"> The resistance range currents. </param>
        ''' <param name="excludedIndexes">         The excluded indexes. </param>
        <Extension>
        Public Sub ListResistanceRangeCurrents(ByVal comboBox As Windows.Forms.ComboBox, ByVal resistanceRangeCurrents As VI.ResistanceRangeCurrentCollection,
                                               ByVal excludedIndexes As IEnumerable(Of Integer))
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If resistanceRangeCurrents Is Nothing Then Throw New ArgumentNullException(NameOf(resistanceRangeCurrents))
            If excludedIndexes Is Nothing Then Throw New ArgumentNullException(NameOf(excludedIndexes))
            Dim clonedvalues As New ResistanceRangeCurrentCollection(resistanceRangeCurrents)
            If excludedIndexes?.Any Then
                Dim l As New List(Of Integer)(excludedIndexes)
                l.Sort()
                l.Reverse()
                For Each i As Integer In l
                    clonedvalues.RemoveAt(i)
                Next
            End If
            Methods.ListResistanceRangeCurrents(comboBox, clonedvalues)
        End Sub

        ''' <summary> List resistance range currents. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                The list control. </param>
        ''' <param name="resistanceRangeCurrents"> The resistance range currents. </param>
        <Extension>
        Public Sub ListResistanceRangeCurrents(ByVal comboBox As Windows.Forms.ComboBox, ByVal resistanceRangeCurrents As VI.ResistanceRangeCurrentCollection)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If resistanceRangeCurrents Is Nothing Then Throw New ArgumentNullException(NameOf(resistanceRangeCurrents))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            Dim wasEnabled As Boolean = comboBox.Enabled
            comboBox.Enabled = False
            comboBox.DataSource = Nothing
            comboBox.Items.Clear()
            comboBox.DataSource = resistanceRangeCurrents
            comboBox.ValueMember = NameOf(VI.ResistanceRangeCurrent.ResistanceRange)
            comboBox.DisplayMember = NameOf(VI.ResistanceRangeCurrent.Caption)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
            comboBox.Enabled = wasEnabled
        End Sub

        ''' <summary> Returns the Resistance Range Current selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The SenseResistanceRangeCurrents. </returns>
        <Extension>
        Public Function SelectedResistanceRangeCurrent(ByVal listControl As Windows.Forms.ListControl) As VI.ResistanceRangeCurrent
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.ResistanceRangeCurrent)
        End Function

        ''' <summary> Select Resistance Range Currents. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">               The list control. </param>
        ''' <param name="resistanceRangeCurrent"> The resistance range current. </param>
        ''' <returns> A VI.ResistanceRangeCurrent. </returns>
        <Extension>
        Public Function SelectResistanceRangeCurrent(ByVal comboBox As Windows.Forms.ComboBox, ByVal resistanceRangeCurrent As VI.ResistanceRangeCurrent) As VI.ResistanceRangeCurrent
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If resistanceRangeCurrent IsNot Nothing AndAlso resistanceRangeCurrent.ResistanceRange <> comboBox.SelectedResistanceRangeCurrent.ResistanceRange Then
                comboBox.SelectedItem = resistanceRangeCurrent
            End If
            Return comboBox.SelectedResistanceRangeCurrent
        End Function

        ''' <summary> Select Resistance Range Currents. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                The list control. </param>
        ''' <param name="resistanceRangeCurrents"> The resistance range currents. </param>
        ''' <param name="range">                   The range. </param>
        ''' <returns> A VI.ResistanceRangeCurrent. </returns>
        <Extension>
        Public Function SelectResistanceRangeCurrent(ByVal comboBox As Windows.Forms.ComboBox, ByVal resistanceRangeCurrents As VI.ResistanceRangeCurrentCollection, ByVal range As Decimal) As VI.ResistanceRangeCurrent
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If resistanceRangeCurrents Is Nothing Then Throw New ArgumentNullException(NameOf(resistanceRangeCurrents))
            Methods.SelectResistanceRangeCurrent(comboBox, resistanceRangeCurrents.MatchResistanceRange(range))
            Return comboBox.SelectedResistanceRangeCurrent
        End Function

        ''' <summary> Select Resistance Range Currents. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                The list control. </param>
        ''' <param name="resistanceRangeCurrents"> The resistance range currents. </param>
        ''' <param name="range">                   The range. </param>
        ''' <param name="current">                 The current. </param>
        ''' <returns> A VI.ResistanceRangeCurrent. </returns>
        <Extension>
        Public Function SelectResistanceRangeCurrent(ByVal comboBox As Windows.Forms.ComboBox, ByVal resistanceRangeCurrents As VI.ResistanceRangeCurrentCollection,
                                             ByVal range As Decimal, ByVal current As Decimal) As VI.ResistanceRangeCurrent
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If resistanceRangeCurrents Is Nothing Then Throw New ArgumentNullException(NameOf(resistanceRangeCurrents))
            Methods.SelectResistanceRangeCurrent(comboBox, resistanceRangeCurrents.MatchResistanceRange(range, current))
            Return comboBox.SelectedResistanceRangeCurrent
        End Function

#End Region

#Region " SENSE FUNCTION MODES "

        ''' <summary> List supported sense function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">               The list control. </param>
        ''' <param name="supportedFunctionModes"> The supported function modes. </param>
        <Extension>
        Public Sub ListSupportedSenseFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedFunctionModes As VI.SenseFunctionModes)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.SenseFunctionModes).EnumValues.IncludeFilter(supportedFunctionModes).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the sense function modes selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The SenseSenseFunctionModes. </returns>
        <Extension>
        Public Function SelectedSenseFunctionModes(ByVal listControl As Windows.Forms.ListControl) As VI.SenseFunctionModes
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.SenseFunctionModes)
        End Function

        ''' <summary> Select sense function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">           The list control. </param>
        ''' <param name="senseFunctionModes"> The sense function mode. </param>
        ''' <returns> The VI.SenseFunctionModes. </returns>
        <Extension>
        Public Function SelectSenseFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal senseFunctionModes As VI.SenseFunctionModes?) As VI.SenseFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If senseFunctionModes.HasValue AndAlso senseFunctionModes.Value <> VI.SenseFunctionModes.None AndAlso senseFunctionModes.Value <> comboBox.SelectedSenseFunctionModes Then
                comboBox.SelectedItem = senseFunctionModes.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedSenseFunctionModes
        End Function

        ''' <summary> Safe select sense function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">           The list control. </param>
        ''' <param name="senseFunctionModes"> The sense function mode. </param>
        ''' <returns> The VI.SenseFunctionModes. </returns>
        <Extension>
        Public Function SafeSelectSenseFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal senseFunctionModes As VI.SenseFunctionModes?) As VI.SenseFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If senseFunctionModes.HasValue AndAlso senseFunctionModes.Value <> VI.SenseFunctionModes.None AndAlso senseFunctionModes.Value <> comboBox.SelectedSenseFunctionModes Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.SenseFunctionModes?)(AddressOf Methods.SafeSelectSenseFunctionModes), New Object() {comboBox, senseFunctionModes})
                Else
                    comboBox.SelectedItem = senseFunctionModes.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedSenseFunctionModes
        End Function

#End Region

#Region " SOURCE FUNCTION MODES "

        ''' <summary> List supported source function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                     The list control. </param>
        ''' <param name="supportedSourceFunctionModes"> The supported source function modes. </param>
        <Extension>
        Public Sub ListSupportedSourceFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedSourceFunctionModes As VI.SourceFunctionModes)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.SourceFunctionModes).EnumValues.IncludeFilter(supportedSourceFunctionModes).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the Source function modes selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The SourceSourceFunctionModes. </returns>
        <Extension>
        Public Function SelectedSourceFunctionModes(ByVal listControl As Windows.Forms.ListControl) As VI.SourceFunctionModes
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.SourceFunctionModes)
        End Function

        ''' <summary> Select Source function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">            The list control. </param>
        ''' <param name="sourceFunctionModes"> The Source function mode. </param>
        ''' <returns> The VI.SourceFunctionModes. </returns>
        <Extension>
        Public Function SelectSourceFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal sourceFunctionModes As VI.SourceFunctionModes?) As VI.SourceFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If sourceFunctionModes.HasValue AndAlso sourceFunctionModes.Value <> VI.SourceFunctionModes.None AndAlso sourceFunctionModes.Value <> comboBox.SelectedSourceFunctionModes Then
                comboBox.SelectedItem = sourceFunctionModes.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedSourceFunctionModes
        End Function

        ''' <summary> Safe select Source function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">            The list control. </param>
        ''' <param name="sourceFunctionModes"> The Source function mode. </param>
        ''' <returns> The VI.SourceFunctionModes. </returns>
        <Extension>
        Public Function SafeSelectSourceFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal sourceFunctionModes As VI.SourceFunctionModes?) As VI.SourceFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If sourceFunctionModes.HasValue AndAlso sourceFunctionModes.Value <> VI.SourceFunctionModes.None AndAlso sourceFunctionModes.Value <> comboBox.SelectedSourceFunctionModes Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.SourceFunctionModes?)(AddressOf Methods.SafeSelectSourceFunctionModes), New Object() {comboBox, sourceFunctionModes})
                Else
                    comboBox.SelectedItem = sourceFunctionModes.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedSourceFunctionModes
        End Function

#End Region

#Region " TRIGGER EVENTS "

        ''' <summary> List supported trigger events. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">               The list control. </param>
        ''' <param name="supportedTriggerEvents"> The supported trigger events. </param>
        <Extension>
        Public Sub ListSupportedTriggerEvents(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedTriggerEvents As VI.TriggerEvents)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.TriggerEvents).EnumValues.IncludeFilter(supportedTriggerEvents).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the trigger event selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The SenseTriggerEvent. </returns>
        <Extension>
        Public Function SelectedTriggerEvent(ByVal listControl As Windows.Forms.ListControl) As VI.TriggerEvents
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.TriggerEvents)
        End Function

        ''' <summary> Select trigger event. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">     The list control. </param>
        ''' <param name="triggerEvent"> The trigger event. </param>
        ''' <returns> A VI.Scpi.TriggerEvent. </returns>
        <Extension>
        Public Function SelectTriggerEvent(ByVal comboBox As Windows.Forms.ComboBox, ByVal triggerEvent As VI.TriggerEvents?) As VI.TriggerEvents
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If triggerEvent.HasValue AndAlso triggerEvent.Value <> VI.TriggerEvents.None AndAlso triggerEvent.Value <> comboBox.SelectedTriggerEvent Then
                comboBox.SelectedItem = triggerEvent.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedTriggerEvent
        End Function

        ''' <summary> Safe select trigger event. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">     The list control. </param>
        ''' <param name="triggerEvent"> The trigger event. </param>
        ''' <returns> A VI.Scpi.TriggerEvent. </returns>
        <Extension>
        Public Function SafeSelectTriggerEvent(ByVal comboBox As Windows.Forms.ComboBox, ByVal triggerEvent As VI.TriggerEvents?) As VI.TriggerEvents
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If triggerEvent.HasValue AndAlso triggerEvent.Value <> VI.TriggerEvents.None AndAlso triggerEvent.Value <> comboBox.SelectedTriggerEvent Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.TriggerEvents)(AddressOf Methods.SafeSelectTriggerEvent), New Object() {comboBox, triggerEvent})
                Else
                    comboBox.SelectedItem = triggerEvent.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedTriggerEvent
        End Function

#End Region

#Region " TRIGGER SOURCES "

        ''' <summary> List supported trigger sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                The list control. </param>
        ''' <param name="supportedTriggerSources"> The supported trigger sources. </param>
        <Extension>
        Public Sub ListSupportedTriggerSources(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedTriggerSources As VI.TriggerSources)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.TriggerSources).EnumValues.IncludeFilter(supportedTriggerSources).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Min(comboBox.Items.Count - 1, Math.Max(selectedIndex, 0))
            End If
        End Sub

        ''' <summary> Selected trigger sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The VI.TriggerSources. </returns>
        <Extension>
        Public Function SelectedTriggerSources(ByVal listControl As Windows.Forms.ListControl) As VI.TriggerSources
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.TriggerSources)
        End Function

        ''' <summary> Select trigger Sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">       The list control. </param>
        ''' <param name="triggerSources"> The trigger Sources. </param>
        ''' <returns> A VI.TriggerSources. </returns>
        <Extension>
        Public Function SelectTriggerSources(ByVal comboBox As Windows.Forms.ComboBox, ByVal triggerSources As VI.TriggerSources?) As VI.TriggerSources
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If triggerSources.HasValue AndAlso triggerSources.Value <> VI.TriggerSources.None AndAlso triggerSources.Value <> comboBox.SelectedTriggerSources Then
                comboBox.SelectedItem = triggerSources.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedTriggerSources
        End Function

        ''' <summary> Safe select trigger Sources. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">       The list control. </param>
        ''' <param name="triggerSources"> The trigger Sources. </param>
        ''' <returns> A VI.TriggerSources. </returns>
        <Extension>
        Public Function SafeSelectTriggerSources(ByVal comboBox As Windows.Forms.ComboBox, ByVal triggerSources As VI.TriggerSources?) As VI.TriggerSources
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If triggerSources.HasValue AndAlso triggerSources.Value <> VI.TriggerSources.None AndAlso triggerSources.Value <> comboBox.SelectedTriggerSources Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.TriggerSources)(AddressOf Methods.SafeSelectTriggerSources), New Object() {comboBox, triggerSources})
                Else
                    comboBox.SelectedItem = triggerSources.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedTriggerSources
        End Function

        ''' <summary> Returns the trigger source selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">     The list control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The SenseTriggerSources. </returns>
        <Extension>
        Public Function SelectedTriggerSources(ByVal comboBox As Windows.Forms.ComboBox, ByVal defaultValue As VI.TriggerSources) As VI.TriggerSources
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Return If(comboBox.SelectedItem Is Nothing, defaultValue, CType(comboBox.SelectedValue, VI.TriggerSources))
        End Function

#End Region

#Region " TRACE PARAMETERS "

        ''' <summary> List supported trace parameters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">                 The list control. </param>
        ''' <param name="supportedTraceParameters"> The supported trace parameters. </param>
        <Extension>
        Public Sub ListSupportedTraceParameters(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedTraceParameters As VI.TraceParameters)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(VI.TraceParameters).EnumValues.IncludeFilter(supportedTraceParameters).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the Trace Parameter selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The SenseTraceParameters. </returns>
        <Extension>
        Public Function SelectedTraceParameters(ByVal listControl As Windows.Forms.ListControl) As VI.TraceParameters
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.TraceParameters)
        End Function

        ''' <summary> Select trace parameters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">        The list control. </param>
        ''' <param name="traceParameters"> The trace parameters. </param>
        ''' <returns> The VI.TraceParameters. </returns>
        <Extension>
        Public Function SelectTraceParameters(ByVal comboBox As Windows.Forms.ComboBox, ByVal traceParameters As VI.TraceParameters?) As VI.TraceParameters
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If traceParameters.HasValue AndAlso traceParameters.Value <> VI.TraceParameters.None AndAlso traceParameters.Value <> comboBox.SelectedTraceParameters Then
                comboBox.SelectedItem = traceParameters.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedTraceParameters
        End Function

        ''' <summary> Safe select trace parameters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">        The list control. </param>
        ''' <param name="traceParameters"> The trace parameters. </param>
        ''' <returns> The VI.TraceParameters. </returns>
        <Extension>
        Public Function SafeSelectTraceParameters(ByVal comboBox As Windows.Forms.ComboBox, ByVal traceParameters As VI.TraceParameters?) As VI.TraceParameters
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If traceParameters.HasValue AndAlso traceParameters.Value <> VI.TraceParameters.None AndAlso traceParameters.Value <> comboBox.SelectedTraceParameters Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.TraceParameters)(AddressOf Methods.SafeSelectTraceParameters), New Object() {comboBox, traceParameters})
                Else
                    comboBox.SelectedItem = traceParameters.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedTraceParameters
        End Function

#End Region

#Region " VENT LOG MODES "

        ''' <summary> List supported event log modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl">            The list control. </param>
        ''' <param name="supportedEventLogModes"> The supported event log modes. </param>
        <Extension>
        Public Sub ListSupportedEventLogModes(ByVal listControl As ListControl, ByVal supportedEventLogModes As VI.Tsp.Syntax.EventLogModes)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            listControl.DataSource = Nothing
            listControl.DataSource = GetType(VI.Tsp.Syntax.EventLogModes).EnumValues.IncludeFilter(supportedEventLogModes).ValueDescriptionPairs.ToList
            listControl.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            listControl.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        End Sub

        ''' <summary> Selected event log modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> The VI.Tsp.Syntax.EventLogModes. </returns>
        <Extension>
        Public Function SelectedEventLogModes(ByVal listControl As Windows.Forms.ListControl) As VI.Tsp.Syntax.EventLogModes
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Return CType(listControl.SelectedValue, VI.Tsp.Syntax.EventLogModes)
        End Function

        ''' <summary> Select event log modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">      The list control. </param>
        ''' <param name="eventLogModes"> The event log modes. </param>
        ''' <returns> The VI.Tsp.Syntax.EventLogModes. </returns>
        <Extension>
        Public Function SelectEventLogModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal eventLogModes As VI.Tsp.Syntax.EventLogModes?) As VI.Tsp.Syntax.EventLogModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If eventLogModes.HasValue AndAlso eventLogModes.Value <> VI.Tsp.Syntax.EventLogModes.None AndAlso eventLogModes.Value <> comboBox.SelectedEventLogModes Then
                comboBox.SelectedItem = eventLogModes.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedEventLogModes
        End Function

        ''' <summary> Safe select event log modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">      The list control. </param>
        ''' <param name="eventLogModes"> The event log modes. </param>
        ''' <returns> The VI.Tsp.Syntax.EventLogModes. </returns>
        <Extension>
        Public Function SafeSelectEventLogModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal eventLogModes As VI.Tsp.Syntax.EventLogModes?) As VI.Tsp.Syntax.EventLogModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If eventLogModes.HasValue AndAlso eventLogModes.Value <> VI.Tsp.Syntax.EventLogModes.None AndAlso eventLogModes.Value <> comboBox.SelectedEventLogModes Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, VI.Tsp.Syntax.EventLogModes)(AddressOf Methods.SafeSelectEventLogModes), New Object() {comboBox, eventLogModes})
                Else
                    comboBox.SelectedItem = eventLogModes.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedEventLogModes
        End Function

#End Region

    End Module

End Namespace
