<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LimitView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LimitView))
        Me._LimitToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PerformLimitStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadLimitTestMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LimitEnabledToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._AutoClearToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._LimitFailedButton = New System.Windows.Forms.ToolStripButton()
        Me._UpperLimitToolStrip = New System.Windows.Forms.ToolStrip()
        Me._UpperLimitLabel = New System.Windows.Forms.ToolStripLabel()
        Me._UpperLimitDecimalsNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._UpperLimitDecimalsNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._UpperLimitNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._UpperLimitNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._UpperLimitBitPatternNumericButton = New System.Windows.Forms.ToolStripButton()
        Me._UpperLimitBitPatternNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._LowerLimitToolStrip = New System.Windows.Forms.ToolStrip()
        Me._LowerLimitLabel = New System.Windows.Forms.ToolStripLabel()
        Me._LowerLimitDecimalsNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._LowerLimitDecimalsNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._LowerLimitNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._LowerLimitNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._LowerLimitBitPatternNumericButton = New System.Windows.Forms.ToolStripButton()
        Me._LowerLimitBitPatternNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._LimitToolStripPanel.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me._UpperLimitToolStrip.SuspendLayout()
        Me._LowerLimitToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_LimitToolStripPanel
        '
        Me._LimitToolStripPanel.BackColor = System.Drawing.Color.Transparent
        Me._LimitToolStripPanel.Controls.Add(Me._SubsystemToolStrip)
        Me._LimitToolStripPanel.Controls.Add(Me._UpperLimitToolStrip)
        Me._LimitToolStripPanel.Controls.Add(Me._LowerLimitToolStrip)
        Me._LimitToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._LimitToolStripPanel.Location = New System.Drawing.Point(1, 1)
        Me._LimitToolStripPanel.Name = "_LimitToolStripPanel"
        Me._LimitToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._LimitToolStripPanel.RowMargin = New System.Windows.Forms.Padding(0)
        Me._LimitToolStripPanel.Size = New System.Drawing.Size(387, 81)
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._LimitEnabledToggleButton, Me._AutoClearToggleButton, Me._LimitFailedButton})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(387, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 3
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DoubleClickEnabled = True
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem, Me._PerformLimitStripMenuItem, Me._ReadLimitTestMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(57, 22)
        Me._SubsystemSplitButton.Text = "Limit1"
        Me._SubsystemSplitButton.ToolTipText = "Double-click to read limit state"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(184, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(184, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument"
        '
        '_PerformLimitStripMenuItem
        '
        Me._PerformLimitStripMenuItem.Name = "_PerformLimitStripMenuItem"
        Me._PerformLimitStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me._PerformLimitStripMenuItem.Text = "Perform Limit Test"
        Me._PerformLimitStripMenuItem.ToolTipText = "Perform limit test"
        '
        '_ReadLimitTestMenuItem
        '
        Me._ReadLimitTestMenuItem.Name = "_ReadLimitTestMenuItem"
        Me._ReadLimitTestMenuItem.Size = New System.Drawing.Size(184, 22)
        Me._ReadLimitTestMenuItem.Text = "Read Limit Test State"
        Me._ReadLimitTestMenuItem.ToolTipText = "Read limit test result"
        '
        '_LimitEnabledToggleButton
        '
        Me._LimitEnabledToggleButton.Checked = True
        Me._LimitEnabledToggleButton.CheckOnClick = True
        Me._LimitEnabledToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._LimitEnabledToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._LimitEnabledToggleButton.Image = CType(resources.GetObject("_LimitEnabledToggleButton.Image"), System.Drawing.Image)
        Me._LimitEnabledToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LimitEnabledToggleButton.Name = "_LimitEnabledToggleButton"
        Me._LimitEnabledToggleButton.Size = New System.Drawing.Size(56, 22)
        Me._LimitEnabledToggleButton.Text = "Disabled"
        Me._LimitEnabledToggleButton.ToolTipText = "toggle to enable or disable this limit"
        '
        '_AutoClearToggleButton
        '
        Me._AutoClearToggleButton.Checked = True
        Me._AutoClearToggleButton.CheckOnClick = True
        Me._AutoClearToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._AutoClearToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AutoClearToggleButton.Image = CType(resources.GetObject("_AutoClearToggleButton.Image"), System.Drawing.Image)
        Me._AutoClearToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AutoClearToggleButton.Name = "_AutoClearToggleButton"
        Me._AutoClearToggleButton.Size = New System.Drawing.Size(90, 22)
        Me._AutoClearToggleButton.Text = "Auto Clear: Off"
        Me._AutoClearToggleButton.ToolTipText = "Toggle to turn auto clear on or off"
        '
        '_LimitFailedButton
        '
        Me._LimitFailedButton.Checked = True
        Me._LimitFailedButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._LimitFailedButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._LimitFailedButton.Image = CType(resources.GetObject("_LimitFailedButton.Image"), System.Drawing.Image)
        Me._LimitFailedButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LimitFailedButton.Name = "_LimitFailedButton"
        Me._LimitFailedButton.Size = New System.Drawing.Size(47, 22)
        Me._LimitFailedButton.Text = "Failed?"
        Me._LimitFailedButton.ToolTipText = "displays fail state"
        '
        '_UpperLimitToolStrip
        '
        Me._UpperLimitToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._UpperLimitToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._UpperLimitToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._UpperLimitToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._UpperLimitLabel, Me._UpperLimitDecimalsNumericLabel, Me._UpperLimitDecimalsNumeric, Me._UpperLimitNumericLabel, Me._UpperLimitNumeric, Me._UpperLimitBitPatternNumericButton, Me._UpperLimitBitPatternNumeric})
        Me._UpperLimitToolStrip.Location = New System.Drawing.Point(0, 25)
        Me._UpperLimitToolStrip.Name = "_UpperLimitToolStrip"
        Me._UpperLimitToolStrip.Size = New System.Drawing.Size(340, 28)
        Me._UpperLimitToolStrip.TabIndex = 2
        '
        '_UpperLimitLabel
        '
        Me._UpperLimitLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._UpperLimitLabel.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._UpperLimitLabel.Name = "_UpperLimitLabel"
        Me._UpperLimitLabel.Size = New System.Drawing.Size(39, 25)
        Me._UpperLimitLabel.Text = "Upper"
        '
        '_UpperLimitDecimalsNumericLabel
        '
        Me._UpperLimitDecimalsNumericLabel.Name = "_UpperLimitDecimalsNumericLabel"
        Me._UpperLimitDecimalsNumericLabel.Size = New System.Drawing.Size(58, 25)
        Me._UpperLimitDecimalsNumericLabel.Text = "Decimals:"
        '
        '_UpperLimitDecimalsNumeric
        '
        Me._UpperLimitDecimalsNumeric.Name = "_UpperLimitDecimalsNumeric"
        Me._UpperLimitDecimalsNumeric.Size = New System.Drawing.Size(41, 25)
        Me._UpperLimitDecimalsNumeric.Text = "3"
        Me._UpperLimitDecimalsNumeric.ToolTipText = "Number of decimal places for setting the limits"
        Me._UpperLimitDecimalsNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_UpperLimitNumericLabel
        '
        Me._UpperLimitNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._UpperLimitNumericLabel.Name = "_UpperLimitNumericLabel"
        Me._UpperLimitNumericLabel.Size = New System.Drawing.Size(38, 25)
        Me._UpperLimitNumericLabel.Text = "Value:"
        '
        '_UpperLimitNumeric
        '
        Me._UpperLimitNumeric.Name = "_UpperLimitNumeric"
        Me._UpperLimitNumeric.Size = New System.Drawing.Size(41, 25)
        Me._UpperLimitNumeric.Text = "0"
        Me._UpperLimitNumeric.ToolTipText = "Upper limit"
        Me._UpperLimitNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_UpperLimitBitPatternNumericButton
        '
        Me._UpperLimitBitPatternNumericButton.CheckOnClick = True
        Me._UpperLimitBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._UpperLimitBitPatternNumericButton.Image = CType(resources.GetObject("_UpperLimitBitPatternNumericButton.Image"), System.Drawing.Image)
        Me._UpperLimitBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._UpperLimitBitPatternNumericButton.Name = "_UpperLimitBitPatternNumericButton"
        Me._UpperLimitBitPatternNumericButton.Size = New System.Drawing.Size(65, 25)
        Me._UpperLimitBitPatternNumericButton.Text = "Source: 0x"
        Me._UpperLimitBitPatternNumericButton.ToolTipText = "Toggle to switch between decimal and hex display"
        '
        '_UpperLimitBitPatternNumeric
        '
        Me._UpperLimitBitPatternNumeric.Name = "_UpperLimitBitPatternNumeric"
        Me._UpperLimitBitPatternNumeric.Size = New System.Drawing.Size(41, 25)
        Me._UpperLimitBitPatternNumeric.Text = "0"
        Me._UpperLimitBitPatternNumeric.ToolTipText = "Upper limit bit pattern"
        Me._UpperLimitBitPatternNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_LowerLimitToolStrip
        '
        Me._LowerLimitToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._LowerLimitToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._LowerLimitToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._LowerLimitToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LowerLimitLabel, Me._LowerLimitDecimalsNumericLabel, Me._LowerLimitDecimalsNumeric, Me._LowerLimitNumericLabel, Me._LowerLimitNumeric, Me._LowerLimitBitPatternNumericButton, Me._LowerLimitBitPatternNumeric})
        Me._LowerLimitToolStrip.Location = New System.Drawing.Point(0, 53)
        Me._LowerLimitToolStrip.Name = "_LowerLimitToolStrip"
        Me._LowerLimitToolStrip.Size = New System.Drawing.Size(341, 28)
        Me._LowerLimitToolStrip.TabIndex = 1
        '
        '_LowerLimitLabel
        '
        Me._LowerLimitLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LowerLimitLabel.Margin = New System.Windows.Forms.Padding(6, 1, 2, 2)
        Me._LowerLimitLabel.Name = "_LowerLimitLabel"
        Me._LowerLimitLabel.Size = New System.Drawing.Size(38, 25)
        Me._LowerLimitLabel.Text = "Lower"
        '
        '_LowerLimitDecimalsNumericLabel
        '
        Me._LowerLimitDecimalsNumericLabel.Name = "_LowerLimitDecimalsNumericLabel"
        Me._LowerLimitDecimalsNumericLabel.Size = New System.Drawing.Size(58, 25)
        Me._LowerLimitDecimalsNumericLabel.Text = "Decimals:"
        '
        '_LowerLimitDecimalsNumeric
        '
        Me._LowerLimitDecimalsNumeric.Name = "_LowerLimitDecimalsNumeric"
        Me._LowerLimitDecimalsNumeric.Size = New System.Drawing.Size(41, 25)
        Me._LowerLimitDecimalsNumeric.Text = "3"
        Me._LowerLimitDecimalsNumeric.ToolTipText = "Number of decimal places for setting the limits"
        Me._LowerLimitDecimalsNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_LowerLimitNumericLabel
        '
        Me._LowerLimitNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._LowerLimitNumericLabel.Name = "_LowerLimitNumericLabel"
        Me._LowerLimitNumericLabel.Size = New System.Drawing.Size(38, 25)
        Me._LowerLimitNumericLabel.Text = "Value:"
        '
        '_LowerLimitNumeric
        '
        Me._LowerLimitNumeric.Name = "_LowerLimitNumeric"
        Me._LowerLimitNumeric.Size = New System.Drawing.Size(41, 25)
        Me._LowerLimitNumeric.Text = "0"
        Me._LowerLimitNumeric.ToolTipText = "Start delay in seconds"
        Me._LowerLimitNumeric.Value = New Decimal(New Integer() {20, 0, 0, 196608})
        '
        '_LowerLimitBitPatternNumericButton
        '
        Me._LowerLimitBitPatternNumericButton.CheckOnClick = True
        Me._LowerLimitBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._LowerLimitBitPatternNumericButton.Image = CType(resources.GetObject("_LowerLimitBitPatternNumericButton.Image"), System.Drawing.Image)
        Me._LowerLimitBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LowerLimitBitPatternNumericButton.Name = "_LowerLimitBitPatternNumericButton"
        Me._LowerLimitBitPatternNumericButton.Size = New System.Drawing.Size(65, 25)
        Me._LowerLimitBitPatternNumericButton.Text = "Source: 0x"
        Me._LowerLimitBitPatternNumericButton.ToolTipText = "Toggle to switch between decimal and hex display"
        '
        '_LowerLimitBitPatternNumeric
        '
        Me._LowerLimitBitPatternNumeric.Name = "_LowerLimitBitPatternNumeric"
        Me._LowerLimitBitPatternNumeric.Size = New System.Drawing.Size(41, 25)
        Me._LowerLimitBitPatternNumeric.Text = "1"
        Me._LowerLimitBitPatternNumeric.ToolTipText = "Lower limit bit pattern"
        Me._LowerLimitBitPatternNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LimitView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._LimitToolStripPanel)
        Me.Name = "LimitView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(389, 81)
        Me._LimitToolStripPanel.ResumeLayout(False)
        Me._LimitToolStripPanel.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me._UpperLimitToolStrip.ResumeLayout(False)
        Me._UpperLimitToolStrip.PerformLayout()
        Me._LowerLimitToolStrip.ResumeLayout(False)
        Me._LowerLimitToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private _LimitToolStripPanel As Windows.Forms.ToolStripPanel
    Private _LowerLimitToolStrip As Windows.Forms.ToolStrip
    Private _LowerLimitLabel As Windows.Forms.ToolStripLabel
    Private _LowerLimitNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _LowerLimitNumeric As Core.Controls.ToolStripNumericUpDown
    Private _LowerLimitDecimalsNumericLabel As Windows.Forms.ToolStripLabel
    Private _UpperLimitToolStrip As Windows.Forms.ToolStrip
    Private _UpperLimitLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _UpperLimitDecimalsNumeric As Core.Controls.ToolStripNumericUpDown
    Private _UpperLimitNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _UpperLimitNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _UpperLimitBitPatternNumericButton As Windows.Forms.ToolStripButton
    Private WithEvents _UpperLimitBitPatternNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _LowerLimitBitPatternNumericButton As Windows.Forms.ToolStripButton
    Private WithEvents _LowerLimitBitPatternNumeric As Core.Controls.ToolStripNumericUpDown
    Private _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _LimitEnabledToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _LimitFailedButton As Windows.Forms.ToolStripButton
    Private _UpperLimitDecimalsNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _LowerLimitDecimalsNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _AutoClearToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PerformLimitStripMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadLimitTestMenuItem As Windows.Forms.ToolStripMenuItem
End Class
