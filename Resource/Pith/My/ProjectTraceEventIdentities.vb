'---------------------------------------------------------------------------------------------------
' file:		Pith\My\MyLibrary_Identities.vb
'
' summary:	My library identities class
'---------------------------------------------------------------------------------------------------
Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Enum ProjectTraceEventId

        ''' <summary> An enum constant representing the none option. </summary>
        <System.ComponentModel.Description("Not specified")>
        None

        ''' <summary> An enum constant representing the pith option. </summary>
        <System.ComponentModel.Description("Pith")>
        Pith = isr.Core.ProjectTraceEventId.VirtualInstruments

        ''' <summary> An enum constant representing the national visa option. </summary>
        <System.ComponentModel.Description("NI Visa")>
        NationalVisa = ProjectTraceEventId.Pith + &H1

        ''' <summary> An enum constant representing the foundation visa option. </summary>
        <System.ComponentModel.Description("IVI Visa")>
        FoundationVisa = ProjectTraceEventId.Pith + &H2

        ''' <summary> An enum constant representing the keysight visa option. </summary>
        <System.ComponentModel.Description("Keysight Visa")>
        KeysightVisa = ProjectTraceEventId.Pith + &H4

        ''' <summary> An enum constant representing the dummy visa option. </summary>
        <System.ComponentModel.Description("Dummy Visa")>
        DummyVisa = ProjectTraceEventId.Pith + &H5

        ''' <summary> An enum constant representing the device option. </summary>
        <System.ComponentModel.Description("Device")>
        Device = ProjectTraceEventId.Pith + &H6

        ''' <summary> An enum constant representing the tsp device option. </summary>
        <System.ComponentModel.Description("Tsp Device")>
        TspDevice = ProjectTraceEventId.Pith + &H7

        ''' <summary> An enum constant representing the tsp script option. </summary>
        <System.ComponentModel.Description("Tsp Script ")>
        TspScript = ProjectTraceEventId.Pith + &H8

        ''' <summary> An enum constant representing the tsp 2 device option. </summary>
        <System.ComponentModel.Description("Tsp2 Device")>
        Tsp2Device = ProjectTraceEventId.Pith + &HA

        ''' <summary> An enum constant representing the facade option. </summary>
        <System.ComponentModel.Description("Facade")>
        Facade = ProjectTraceEventId.Pith + &H10

        ''' <summary> An enum constant representing the instrument option. </summary>
        <System.ComponentModel.Description("Instrument")>
        Instrument = ProjectTraceEventId.Facade + &H1

        ''' <summary> An enum constant representing the 5700 option. </summary>
        <System.ComponentModel.Description("N5700 Power Supply")>
        N5700 = ProjectTraceEventId.Pith + &H20

        ''' <summary> An enum constant representing the source measure option. </summary>
        <System.ComponentModel.Description("FREE `")>
        SourceMeasure = ProjectTraceEventId.N5700 + &H1

        ''' <summary> An enum constant representing the scanner option. </summary>
        <System.ComponentModel.Description("FREE 2")>
        Scanner = ProjectTraceEventId.N5700 + &H2

        ''' <summary> An enum constant representing the multimeter option. </summary>
        <System.ComponentModel.Description("FREE 3")>
        Multimeter = ProjectTraceEventId.N5700 + &H3

        ''' <summary> An enum constant representing the eg 2000 prober option. </summary>
        <System.ComponentModel.Description("EG2000 Prober")>
        EG2000Prober = ProjectTraceEventId.N5700 + &H4

        ''' <summary> An enum constant representing the ats 600 option. </summary>
        <System.ComponentModel.Description("Temptonic ATS600")>
        Ats600 = ProjectTraceEventId.N5700 + &H5

        ''' <summary> An enum constant representing the 2002 option. </summary>
        <System.ComponentModel.Description("K2002 Meter")>
        K2002 = ProjectTraceEventId.N5700 + &H6

        ''' <summary> An enum constant representing the 2700 option. </summary>
        <System.ComponentModel.Description("K2700 Meter/Scanner")>
        K2700 = ProjectTraceEventId.N5700 + &H7

        ''' <summary> An enum constant representing the 1700 option. </summary>
        <System.ComponentModel.Description("Tegam T1700 Meter")>
        T1700 = ProjectTraceEventId.N5700 + &H8

        ''' <summary> An enum constant representing the 7000 option. </summary>
        <System.ComponentModel.Description("K7000 Switch")>
        K7000 = ProjectTraceEventId.N5700 + &H9

        ''' <summary> An enum constant representing the 3700 option. </summary>
        <System.ComponentModel.Description("K3700 Meter/Scanner")>
        K3700 = ProjectTraceEventId.N5700 + &HA

        ''' <summary> An enum constant representing the ttm driver option. </summary>
        <System.ComponentModel.Description("TTM Driver")>
        TtmDriver = ProjectTraceEventId.N5700 + &HB

        ''' <summary> An enum constant representing the 2600 option. </summary>
        <System.ComponentModel.Description("K2600 Source Meter")>
        K2600 = ProjectTraceEventId.N5700 + &HC

        ''' <summary> An enum constant representing the 4990 option. </summary>
        <System.ComponentModel.Description("E4990 Impedance Analyzer")>
        E4990 = ProjectTraceEventId.N5700 + &HD

        ''' <summary> An enum constant representing the 2400 option. </summary>
        <System.ComponentModel.Description("K2400 Source Meter")>
        K2400 = ProjectTraceEventId.N5700 + &HE

        ''' <summary> An enum constant representing the 7500 option. </summary>
        <System.ComponentModel.Description("K7500 Meter")>
        K7500 = ProjectTraceEventId.N5700 + &HF

        ''' <summary> An enum constant representing the 34980 option. </summary>
        <System.ComponentModel.Description("K34980 Meter/Scanner")>
        K34980 = ProjectTraceEventId.N5700 + &H10

        ''' <summary> An enum constant representing the 3458 option. </summary>
        <System.ComponentModel.Description("K3458 Meter")>
        K3458 = ProjectTraceEventId.N5700 + &H11

        ''' <summary> An enum constant representing the 2450 option. </summary>
        <System.ComponentModel.Description("K2450 Source Meter")>
        K2450 = ProjectTraceEventId.N5700 + &H12

        ''' <summary> An enum constant representing the 7500 tsp option. </summary>
        <System.ComponentModel.Description("K7500 TSP2 Meter")>
        K7500Tsp = ProjectTraceEventId.N5700 + &H13

        ''' <summary> An enum constant representing the tsp rig option. </summary>
        <System.ComponentModel.Description("TSP Rig")>
        TspRig = ProjectTraceEventId.N5700 + &H14

        ''' <summary> An enum constant representing the device facade option. </summary>
        <System.ComponentModel.Description("Device Facade")>
        DeviceFacade = ProjectTraceEventId.Pith + &H40

        ''' <summary> An enum constant representing the ttm console option. </summary>
        <System.ComponentModel.Description("TTM Console")>
        TtmConsole = ProjectTraceEventId.DeviceFacade + &H1

        ''' <summary> An enum constant representing the device tests option. </summary>
        <System.ComponentModel.Description("Device Tests")>
        DeviceTests = ProjectTraceEventId.Pith + &H50

        ''' <summary> An enum constant representing the try tests option. </summary>
        <System.ComponentModel.Description("Try Tests")>
        TryTests = ProjectTraceEventId.DeviceTests + &H1

        ''' <summary> An enum constant representing the eg 2000 prober tests option. </summary>
        <System.ComponentModel.Description("EG2000 Tests")>
        EG2000ProberTests = ProjectTraceEventId.DeviceTests + &H2

        ''' <summary> An enum constant representing the thermostream tests option. </summary>
        <System.ComponentModel.Description("Thermostream Tests")>
        ThermostreamTests = ProjectTraceEventId.DeviceTests + &H3

        ''' <summary> An enum constant representing the 2002 tests option. </summary>
        <System.ComponentModel.Description("K2002 Tests")>
        K2002Tests = ProjectTraceEventId.DeviceTests + &H4

        ''' <summary> An enum constant representing the 2700 tests option. </summary>
        <System.ComponentModel.Description("K2700 Tests")>
        K2700Tests = ProjectTraceEventId.DeviceTests + &H5

        ''' <summary> An enum constant representing the tegam tests option. </summary>
        <System.ComponentModel.Description("Tegam Tests")>
        TegamTests = ProjectTraceEventId.DeviceTests + &H6

        ''' <summary> An enum constant representing the 7000 tests option. </summary>
        <System.ComponentModel.Description("K7000 Tests")>
        K7000Tests = ProjectTraceEventId.DeviceTests + &H7

        ''' <summary> An enum constant representing the 3700 tests option. </summary>
        <System.ComponentModel.Description("K3700 Tests")>
        K3700Tests = ProjectTraceEventId.DeviceTests + &H8

        ''' <summary> An enum constant representing the ttm driver tests option. </summary>
        <System.ComponentModel.Description("TTM Driver Tests")>
        TtmDriverTests = ProjectTraceEventId.DeviceTests + &H9

        ''' <summary> An enum constant representing the 2600 tests option. </summary>
        <System.ComponentModel.Description("K2600 Tests")>
        K2600Tests = ProjectTraceEventId.DeviceTests + &HA

        ''' <summary> An enum constant representing the 4990 tests option. </summary>
        <System.ComponentModel.Description("E4990 Tests")>
        E4990Tests = ProjectTraceEventId.DeviceTests + &HB

        ''' <summary> An enum constant representing the 2400 tests option. </summary>
        <System.ComponentModel.Description("K2400 Tests")>
        K2400Tests = ProjectTraceEventId.DeviceTests + &HD

        ''' <summary> An enum constant representing the 7500 tests option. </summary>
        <System.ComponentModel.Description("K7500 Tests")>
        K7500Tests = ProjectTraceEventId.DeviceTests + &HD

        ''' <summary> An enum constant representing the 34980 tests option. </summary>
        <System.ComponentModel.Description("K34980 Tests")>
        K34980Tests = ProjectTraceEventId.DeviceTests + &HE

        ''' <summary> An enum constant representing the 3458 tests option. </summary>
        <System.ComponentModel.Description("K3458 Tests")>
        K3458Tests = ProjectTraceEventId.DeviceTests + &HF

        ''' <summary> An enum constant representing the 2450 tests option. </summary>
        <System.ComponentModel.Description("K2450 Tests")>
        K2450Tests = ProjectTraceEventId.DeviceTests + &H10

        ''' <summary> An enum constant representing the 7500 tsp tests option. </summary>
        <System.ComponentModel.Description("K7500 TSP2 Tests")>
        K7500TspTests = ProjectTraceEventId.DeviceTests + &H11

        ''' <summary> An enum constant representing the 5700 tests option. </summary>
        <System.ComponentModel.Description("N5700 Tests")>
        N5700Tests = ProjectTraceEventId.DeviceTests + &H12

    End Enum


End Namespace

