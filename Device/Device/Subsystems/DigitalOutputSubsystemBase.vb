'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\DigitalOutputSubsystemBase.vb
'
' summary:	Digital output subsystem base class
'---------------------------------------------------------------------------------------------------
Imports System.Threading.Tasks

Imports isr.Core.Constructs

''' <summary> Defines the SCPI Digital Output subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para> 
''' David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
''' </remarks>
Public MustInherit Class DigitalOutputSubsystemBase
    Inherits SubsystemPlusStatusBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DigitalOutputSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefineOutputModeReadWrites()
        Me.DefineDigitalActiveLevelReadWrites()
        Me.DigitalOutputLines = New DigitalOutputLineCollection
        Me._BinningStrobeTasker = New isr.Core.Tasker
        ' make sure the dispatcher is instantiated. This takes a bit of time the first time around
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> True to disposed value. </summary>
    Private _DisposedValue As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me._DisposedValue Then
            If disposing Then
                Me.BinningStrobeTasker?.Dispose()
            End If
            Me._DisposedValue = True
        End If
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(disposing:=True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.BitSize = 4
        Me.Level = 15
        Me.AutoClearEnabled = False
        Me._DelayRange = New Range(Of TimeSpan)(TimeSpan.Zero, TimeSpan.FromSeconds(60))
        Me.NotifyPropertyChanged(NameOf(DigitalOutputSubsystemBase.DelayRange))
        Me.Delay = TimeSpan.FromSeconds(0.0001)
        Me.OutputMode = OutputModes.EndTest
        Me.CurrentDigitalActiveLevel = DigitalActiveLevels.Low
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Digital Outputs clear command. </summary>
    ''' <remarks> SCPI: ":SOUR2:CLE". </remarks>
    ''' <value> The Digital Outputs clear command. </value>
    Protected Overridable Property ClearCommand As String

    ''' <summary> Clears Digital Outputs. </summary>
    Public Sub ClearOutput()
        Me.Session.Execute(Me.ClearCommand)
    End Sub

#End Region

#Region " AUTO CLEAR ENABLED "

    ''' <summary> The automatic clear enabled. </summary>
    Private _AutoClearEnabled As Boolean?

    ''' <summary> Gets or sets the cached Digital Outputs Auto Clear enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Digital Outputs Auto Clear enabled is not known; <c>True</c> if output is on;
    ''' otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoClearEnabled As Boolean?
        Get
            Return Me._AutoClearEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoClearEnabled, value) Then
                Me._AutoClearEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Digital Outputs Auto Clear enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoClearEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoClearEnabled(value)
        Return Me.QueryAutoClearEnabled()
    End Function

    ''' <summary> Gets or sets the Digital Outputs Auto Clear enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR2:CLE:AUTO?". </remarks>
    ''' <value> The Digital Outputs Auto Clear enabled query command. </value>
    Protected Overridable Property AutoClearEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Enabled sentinel. Also sets the
    ''' <see cref="AutoClearEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoClearEnabled() As Boolean?
        Me.AutoClearEnabled = Me.Query(Me.AutoClearEnabled, Me.AutoClearEnabledQueryCommand)
        Return Me.AutoClearEnabled
    End Function

    ''' <summary> Gets or sets the Digital Outputs Auto Clear enabled command Format. </summary>
    ''' <remarks> SCPI: ":SOUR2:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Digital Outputs Auto Clear enabled query command. </value>
    Protected Overridable Property AutoClearEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoClearEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoClearEnabled = Me.Write(value, Me.AutoClearEnabledCommandFormat)
        Return Me.AutoClearEnabled
    End Function

#End Region

#Region " BIT SIZE "

    ''' <summary> Size of the bit. </summary>
    Private _BitSize As Integer?

    ''' <summary> Gets or sets the cached Digital Outputs Bit Size. </summary>
    ''' <value> The Digital Outputs Bit Size or none if not set or unknown. </value>
    Public Overloads Property BitSize As Integer?
        Get
            Return Me._BitSize
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.BitSize, value) Then
                Me._BitSize = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Digital Outputs Bit Size. </summary>
    ''' <param name="value"> The current Digital Outputs Bit Size. </param>
    ''' <returns> The Digital Outputs Bit Size or none if unknown. </returns>
    Public Function ApplyBitSize(ByVal value As Integer) As Integer?
        Me.WriteBitSize(value)
        Return Me.QueryBitSize()
    End Function

    ''' <summary> Gets or sets the Lower Limit Bit Size query command. </summary>
    ''' <remarks> SCPI: ":SOUR2:BSIZ?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property BitSizeQueryCommand As String

    ''' <summary> Queries the current Lower Limit Bit Size. </summary>
    ''' <returns> The Lower Limit Bit Size or none if unknown. </returns>
    Public Function QueryBitSize() As Integer?
        Me.BitSize = Me.Query(Me.BitSize, Me.BitSizeQueryCommand)
        Return Me.BitSize
    End Function

    ''' <summary> Gets or sets the Lower Limit Bit Size query command. </summary>
    ''' <remarks> SCPI: "::SOUR2:BSIZ {0}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property BitSizeCommandFormat As String

    ''' <summary>
    ''' Sets back the Lower Limit Bit Size without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current Lower Limit Bit Size. </param>
    ''' <returns> The Lower Limit Bit Size or none if unknown. </returns>
    Public Function WriteBitSize(ByVal value As Integer) As Integer?
        Me.BitSize = Me.Write(value, Me.BitSizeCommandFormat)
        Return Me.BitSize
    End Function

#End Region

#Region " DELAY (PULSE WIDTH) "

    ''' <summary> The delay range. </summary>
    Private _DelayRange As Range(Of TimeSpan)

    ''' <summary> Gets or sets the delay (pulse width) range. </summary>
    ''' <value> The delay range. </value>
    Public Overridable Property DelayRange As Range(Of TimeSpan)
        Get
            Return Me._DelayRange
        End Get
        Set(value As Range(Of TimeSpan))
            If Me.DelayRange <> value Then
                Me._DelayRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The delay. </summary>
    Private _Delay As TimeSpan?

    ''' <summary> Gets or sets the cached output delay (pulse width). </summary>
    ''' <remarks>
    ''' The delay is used to delay operation in the Source layer. After the programmed Source event
    ''' occurs, the instrument waits until the delay period expires before performing the Device
    ''' Action.
    ''' </remarks>
    ''' <value> The output delay (pulse width) or none if not set or unknown. </value>
    Public Overloads Property Delay As TimeSpan?
        Get
            Return Me._Delay
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Nullable.Equals(Me.Delay, value) Then
                Me._Delay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the output delay (pulse width). </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The output delay (pulse width) or none if unknown. </returns>
    Public Function ApplyDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteDelay(value)
        Me.QueryDelay()
    End Function

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <remarks> SCPI: ":SOUR:DEL?". </remarks>
    ''' <value> The delay query command. </value>
    Protected Overridable Property DelayQueryCommand As String

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Delay query command. </value>
    Protected Overridable Property DelayFormat As String

    ''' <summary> Queries the Delay. </summary>
    ''' <returns> The Delay or none if unknown. </returns>
    Public Function QueryDelay() As TimeSpan?
        Me.Delay = Me.Query(Me.Delay, Me.DelayFormat, Me.DelayQueryCommand)
        Return Me.Delay
    End Function

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <remarks> SCPI: ":SOUR:DEL {0:s\.FFFFFFF}". </remarks>
    ''' <value> The delay command format. </value>
    Protected Overridable Property DelayCommandFormat As String

    ''' <summary>
    ''' Writes the output delay (pulse width) without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The output delay (pulse width) or none if unknown. </returns>
    Public Function WriteDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.Delay = Me.Write(value, Me.DelayCommandFormat)
        Return Me.Delay
    End Function

#End Region

#Region " LEVEL "

    ''' <summary> The level. </summary>
    Private _Level As Integer?

    ''' <summary> Gets or sets the cached Digital Outputs Level. </summary>
    ''' <value> The Digital Outputs Level or none if not set or unknown. </value>
    Public Overloads Property Level As Integer?
        Get
            Return Me._Level
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Level, value) Then
                Me._Level = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Digital Outputs Level. </summary>
    ''' <param name="value"> The current Digital Outputs Level. </param>
    ''' <returns> The Digital Outputs Level or none if unknown. </returns>
    Public Function ApplyLevel(ByVal value As Integer) As Integer?
        Me.WriteLevel(value)
        Return Me.QueryLevel()
    End Function

    ''' <summary> Gets or sets the Level query command. </summary>
    ''' <remarks> SCPI: ":SOUR2:TTL:ACT?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property LevelQueryCommand As String

    ''' <summary> Queries the current Lower Limit Level. </summary>
    ''' <returns> The Level or none if unknown. </returns>
    Public Function QueryLevel() As Integer?
        Me.Level = Me.Query(Me.Level, Me.LevelQueryCommand)
        Return Me.Level
    End Function

    ''' <summary> Gets or sets the Level query command. </summary>
    ''' <remarks> SCPI: "::SOUR2:TTL:LEVEL {0}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property LevelCommandFormat As String

    ''' <summary> Sets back the Level without reading back the value from the device. </summary>
    ''' <param name="value"> The current Lower Limit Level. </param>
    ''' <returns> The Level or none if unknown. </returns>
    Public Function WriteLevel(ByVal value As Integer) As Integer?
        Me.Level = Me.Write(value, Me.LevelCommandFormat)
        Return Me.Level
    End Function

#End Region

#Region " LINE LEVEL "

    ''' <summary> Writes and reads back the Digital Outputs LineLevel. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The current Digital Outputs LineLevel. </param>
    ''' <returns> The Digital Outputs Line Level or none if unknown. </returns>
    Public Function ApplyLineLevel(ByVal lineNumber As Integer, ByVal value As Integer) As Integer?
        Me.WriteLineLevel(lineNumber, value)
        Return Me.QueryLineLevel(lineNumber)
    End Function

    ''' <summary> Gets or sets the Line Level query command. </summary>
    ''' <remarks> SCPI: ":SOUR:TTL{0}:LEV?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property LineLevelQueryCommand As String

    ''' <summary> Queries the current Line Level. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> The Line Level or none if unknown. </returns>
    Public Function QueryLineLevel(ByVal lineNumber As Integer) As Integer?
        Dim value As Integer? = Me.Query(lineNumber, String.Format(Me.LineLevelQueryCommand, lineNumber))
        Me.DigitalOutputLines.Update(lineNumber, value)
        Me.NotifyPropertyChanged(NameOf(DigitalOutputSubsystemBase.DigitalOutputLines))
        Return value
    End Function

    ''' <summary> Gets or sets the Line Level query command. </summary>
    ''' <remarks> SCPI: "::SOUR2:TTL{0}:LEV {{0}}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property LineLevelCommandFormat As String

    ''' <summary> Sets back the Line Level without reading back the value from the device. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The current Line Level. </param>
    ''' <returns> The Line Level or none if unknown. </returns>
    Public Function WriteLineLevel(ByVal lineNumber As Integer, ByVal value As Integer) As Integer?
        Dim result As Integer? = Me.Write(value, String.Format(Me.LineLevelCommandFormat, lineNumber))
        Me.DigitalOutputLines.Update(lineNumber, result)
        Me.NotifyPropertyChanged(NameOf(DigitalOutputSubsystemBase.DigitalOutputLines))
        Return result
    End Function

    ''' <summary> Outputs a pulse. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <param name="lineNumber">     The strobe line. </param>
    ''' <param name="duration"> Duration of the strobe. </param>
    Public Sub Pulse(ByVal lineNumber As Integer, ByVal lineLevel As Integer, ByVal duration As TimeSpan)
        Me.WriteLineLevel(lineNumber, lineLevel)
        isr.Core.ApplianceBase.Delay(duration)
        Me.WriteLineLevel(lineNumber, If(lineLevel = 0, 1, 0))
    End Sub

    ''' <summary> Outputs to bin line and strobes the strobe line. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <param name="strobeLine">     The strobe line. </param>
    ''' <param name="strobeDuration"> Duration of the strobe. </param>
    ''' <param name="binLine">        The bin line. </param>
    ''' <param name="binDuration">    Duration of the bin. </param>
    Public Sub Strobe(ByVal strobeLine As Integer, ByVal strobeDuration As TimeSpan, ByVal binLine As Integer, ByVal binDuration As TimeSpan)
        If binDuration <= strobeDuration Then binDuration = strobeDuration.Add(TimeSpan.FromMilliseconds(1))
        Me.WriteLineLevel(binLine, 1)
        isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(0.5 * (binDuration - strobeDuration).TotalMilliseconds))
        Me.WriteLineLevel(strobeLine, 1)
        isr.Core.ApplianceBase.DoEventsWait(strobeDuration)
        Me.WriteLineLevel(strobeLine, 0)
        isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(0.5 * (binDuration - strobeDuration).TotalMilliseconds))
        Me.WriteLineLevel(binLine, 0)
    End Sub

    ''' <summary> Gets or sets the tasker. </summary>
    ''' <value> The tasker. </value>
    Public ReadOnly Property BinningStrobeTasker As isr.Core.Tasker

    ''' <summary> Starts strobe task. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="strobeLine">     The strobe line. </param>
    ''' <param name="strobeDuration"> Duration of the strobe. </param>
    ''' <param name="binLine">        The bin line. </param>
    ''' <param name="binDuration">    Duration of the bin. </param>
    Public Sub StartStrobeTask(ByVal strobeLine As Integer, ByVal strobeDuration As TimeSpan, ByVal binLine As Integer, ByVal binDuration As TimeSpan)
        If Me._BinningStrobeTasker?.IsBusy Then Throw New InvalidOperationException($"{NameOf(isr.Core.Tasker)} is busy")
        Me._BinningStrobeTasker.StartAction(Sub()
                                                Me.Strobe(strobeLine, strobeDuration, binLine, binDuration)
                                            End Sub)
    End Sub

#End Region

End Class
