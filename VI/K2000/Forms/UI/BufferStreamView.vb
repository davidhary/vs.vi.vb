'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\BufferStreamView.vb
'
' summary:	Buffer stream view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

''' <summary>
''' A Keithley 2002 edition of the basic <see cref="Facade.BufferStreamView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-01-11 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class BufferStreamView
    Inherits isr.VI.Facade.BufferStreamView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="BufferStreamView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="BufferStreamView"/>. </returns>
    Public Overloads Shared Function Create() As BufferStreamView
        Dim view As BufferStreamView = Nothing
        Try
            view = New BufferStreamView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K2002Device As K2002Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K2002Device)
        MyBase.AssignDevice(value)
        Me._K2002Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, BinningSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SenseFunctionSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SenseSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SystemSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TraceSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase))
            Me.BindSubsystem(CType(Nothing, DigitalOutputSubsystemBase))
            Me.BindSubsystem(CType(Nothing, RouteSubsystemBase))
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem(CType(Nothing, BufferSubsystemBase), String.Empty)
        Else
            Me.BindSubsystem(value.BinningSubsystem)
            Me.BindSubsystem(value.SenseFunctionSubsystem)
            Me.BindSubsystem(value.SenseSubsystem)
            Me.BindSubsystem(value.SystemSubsystem)
            Me.BindSubsystem(value.TraceSubsystem)
            Me.BindSubsystem(value.TriggerSubsystem)
            Me.BindSubsystem(value.DigitalOutputSubsystem)
            Me.BindSubsystem1(value.ArmLayer1Subsystem)
            Me.BindSubsystem2(value.ArmLayer2Subsystem)
            Me.BindSubsystem(value.RouteSubsystem)
        End If
    End Sub

#End Region

#Region " SENSE FUNCTION HANDLING "

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overrides Sub HandleFunctionModesChanged(ByVal subsystem As SenseSubsystemBase)
        MyBase.HandleFunctionModesChanged(subsystem)
        Select Case subsystem.FunctionMode
            Case VI.SenseFunctionModes.CurrentDC
                Me.BindSubsystem(Me.K2002Device.SenseCurrentSubsystem)
            Case VI.SenseFunctionModes.VoltageDC
                Me.BindSubsystem(Me.K2002Device.SenseVoltageSubsystem)
            Case VI.SenseFunctionModes.ResistanceFourWire
                Me.BindSubsystem(Me.K2002Device.SenseResistanceFourWireSubsystem)
            Case VI.SenseFunctionModes.Resistance
                Me.BindSubsystem(Me.K2002Device.SenseResistanceSubsystem)
        End Select
    End Sub

#End Region

#Region " BUFFER STREAM  "

    ''' <summary> Configure trigger plan. </summary>
    ''' <remarks> David, 2020-07-25. </remarks>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="sampleCount">   Number of samples. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    Protected Overrides Sub ConfigureTriggerPlan(ByVal triggerCount As Integer, ByVal sampleCount As Integer, ByVal triggerSource As isr.VI.TriggerSources)
        MyBase.ConfigureTriggerPlan(triggerCount, sampleCount, triggerSource)

        Me.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        Me.ArmLayer1Subsystem.ApplyArmCount(1)
        Me.ArmLayer1Subsystem.ApplyArmLayerBypassMode(TriggerLayerBypassModes.Acceptor)

        Me.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        Me.ArmLayer2Subsystem.ApplyArmCount(sampleCount)
        Me.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)
        Me.ArmLayer2Subsystem.ApplyArmLayerBypassMode(TriggerLayerBypassModes.Acceptor)

        Me.TriggerSubsystem.ApplyTriggerSource(triggerSource)
        Me.TriggerSubsystem.ApplyTriggerCount(triggerCount)
        Me.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
        Me.TriggerSubsystem.ApplyTriggerLayerBypassMode(TriggerLayerBypassModes.Acceptor)

    End Sub


#End Region

End Class
