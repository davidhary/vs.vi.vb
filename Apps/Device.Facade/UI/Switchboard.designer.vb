'---------------------------------------------------------------------------------------------------
' file:		.\UI\Switchboard.designer.vb
'
' summary:	Switchboard.designer class
'---------------------------------------------------------------------------------------------------
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Switchboard

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me._ControlsPanel = New System.Windows.Forms.Panel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me._OptionsSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DisplayExecutablePathMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OpenVirtualInstrumentConsoleButton = New System.Windows.Forms.ToolStripButton()
        Me._VirtualInstrumentsComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._MessagesTextBox = New System.Windows.Forms.TextBox()
        Me._ControlsPanel.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ControlsPanel
        '
        Me._ControlsPanel.Controls.Add(Me.ToolStrip1)
        Me._ControlsPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ControlsPanel.Location = New System.Drawing.Point(0, 328)
        Me._ControlsPanel.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ControlsPanel.Name = "_ControlsPanel"
        Me._ControlsPanel.Size = New System.Drawing.Size(516, 34)
        Me._ControlsPanel.TabIndex = 15
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._OptionsSplitButton, Me._OpenVirtualInstrumentConsoleButton, Me._VirtualInstrumentsComboBox})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(516, 25)
        Me.ToolStrip1.TabIndex = 13
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        '_OptionsSplitButton
        '
        Me._OptionsSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OptionsSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DisplayExecutablePathMenuItem})
        Me._OptionsSplitButton.Image = CType(resources.GetObject("_OptionsSplitButton.Image"), System.Drawing.Image)
        Me._OptionsSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OptionsSplitButton.Name = "_OptionsSplitButton"
        Me._OptionsSplitButton.Size = New System.Drawing.Size(63, 22)
        Me._OptionsSplitButton.Text = "Select..."
        Me._OptionsSplitButton.ToolTipText = "Select option"
        '
        '_DisplayExecutablePathMenuItem
        '
        Me._DisplayExecutablePathMenuItem.Name = "_DisplayExecutablePathMenuItem"
        Me._DisplayExecutablePathMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._DisplayExecutablePathMenuItem.Text = "Display Execution Path"
        Me._DisplayExecutablePathMenuItem.ToolTipText = "Displays the execution path"
        '
        '_OpenVirtualInstrumentConsoleButton
        '
        Me._OpenVirtualInstrumentConsoleButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._OpenVirtualInstrumentConsoleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OpenVirtualInstrumentConsoleButton.Image = CType(resources.GetObject("_OpenVirtualInstrumentConsoleButton.Image"), System.Drawing.Image)
        Me._OpenVirtualInstrumentConsoleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenVirtualInstrumentConsoleButton.Name = "_OpenVirtualInstrumentConsoleButton"
        Me._OpenVirtualInstrumentConsoleButton.Size = New System.Drawing.Size(49, 22)
        Me._OpenVirtualInstrumentConsoleButton.Text = "Open..."
        '
        '_VirtualInstrumentsComboBox
        '
        Me._VirtualInstrumentsComboBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._VirtualInstrumentsComboBox.AutoSize = False
        Me._VirtualInstrumentsComboBox.Name = "_VirtualInstrumentsComboBox"
        Me._VirtualInstrumentsComboBox.Size = New System.Drawing.Size(160, 25)
        Me._VirtualInstrumentsComboBox.Text = "Select virtual instrument"
        '
        '_MessagesTextBox
        '
        Me._MessagesTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._MessagesTextBox.CausesValidation = False
        Me._MessagesTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MessagesTextBox.Location = New System.Drawing.Point(0, 0)
        Me._MessagesTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._MessagesTextBox.Multiline = True
        Me._MessagesTextBox.Name = "_MessagesTextBox"
        Me._MessagesTextBox.ReadOnly = True
        Me._MessagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._MessagesTextBox.Size = New System.Drawing.Size(516, 328)
        Me._MessagesTextBox.TabIndex = 16
        '
        'Switchboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(516, 362)
        Me.Controls.Add(Me._MessagesTextBox)
        Me.Controls.Add(Me._ControlsPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "Switchboard"
        Me.Text = "Form1"
        Me._ControlsPanel.ResumeLayout(False)
        Me._ControlsPanel.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

        Private WithEvents _ControlsPanel As System.Windows.Forms.Panel

    ''' <summary> The with events control. </summary>
    Private WithEvents _MessagesTextBox As System.Windows.Forms.TextBox

        Private WithEvents ToolStrip1 As ToolStrip

        Private WithEvents _OptionsSplitButton As ToolStripSplitButton

        Private WithEvents _DisplayExecutablePathMenuItem As ToolStripMenuItem

        Private WithEvents _OpenVirtualInstrumentConsoleButton As ToolStripButton

        Private WithEvents _VirtualInstrumentsComboBox As ToolStripComboBox
End Class
