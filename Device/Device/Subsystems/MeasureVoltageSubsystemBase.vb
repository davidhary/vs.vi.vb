Imports isr.Core.TimeSpanExtensions
''' <summary>
''' Defines the contract that must be implemented by a Measure Voltage Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class MeasureVoltageSubsystemBase
    Inherits VI.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MeasureVoltageSubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-08-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem, readingAmounts)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Level = 0
    End Sub

#End Region

#Region " MEASURE VOLTAGE "

    ''' <summary> Waits for the voltage to exceed a minimum voltage level. </summary>
    ''' <param name="limen">   The threshold level. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns>
    ''' <c>True</c> if level was reached before the <paramref name="timeout">timeout</paramref>
    ''' expired; <c>False</c> otherwise.
    ''' </returns>
    Public Function AwaitMinimumLevel(ByVal limen As Double, timeout As TimeSpan) As Boolean
        Dim sw As Stopwatch = Stopwatch.StartNew
        Do
            TimeSpan.FromMilliseconds(1).SpinWait()
            Me.MeasureReadingAmounts()
        Loop Until limen <= Me.Level OrElse sw.Elapsed > timeout
        Return Me.Level.HasValue AndAlso limen <= Me.Level.Value
    End Function

    ''' <summary> Waits for the voltage to attain a level. </summary>
    ''' <param name="targetLevel"> The target level. </param>
    ''' <param name="delta">       The delta. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns>
    ''' <c>True</c> if level was reached before the <paramref name="timeout">timeout</paramref>
    ''' expired; <c>False</c> otherwise.
    ''' </returns>
    Public Function AwaitLevel(ByVal targetLevel As Double, ByVal delta As Double, ByVal timeout As TimeSpan) As Boolean
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim hasValue As Boolean
        Do
            TimeSpan.FromMilliseconds(1).SpinWait()
            Me.MeasureReadingAmounts()
            hasValue = Me.Level.HasValue AndAlso Math.Abs(targetLevel - Me.Level.Value) <= delta
        Loop Until hasValue OrElse sw.Elapsed > timeout
        Return hasValue
    End Function

    ''' <summary> The level. </summary>
    Private _Level As Double?

    ''' <summary> Gets or sets the cached voltage level. </summary>
    ''' <value> The voltage. </value>
    Public Property Level As Double?
        Get
            Return Me._Level
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Level, value) Then
                Me._Level = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
