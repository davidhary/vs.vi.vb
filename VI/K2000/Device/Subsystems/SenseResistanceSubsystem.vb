''' <summary> Defines a SCPI Sense Resistance Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2014-03-01, 3.0.5173. </para>
''' </remarks>
Public Class SenseResistanceSubsystem
    Inherits VI.SenseResistanceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ohm)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm
        Me.DefineFunctionModeReadWrites("""{0}""", "'{0}'")

        Me.ResistanceRangeCurrents.Clear()
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0, 0, "Auto Range"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20, 0.00072D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 7.2 mA"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200, 0.00096D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000, 0.00096D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000, 0.000096D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 96 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200000, 0.0000096D, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 9.6 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000000, 0.0000019D, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.9 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000000, 0.0000014D, $"20 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.4 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200000000, 0.0000014D, $"200 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.4 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(1000000000, 0.0000014D, $"1 G{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.4 {Arebis.StandardUnits.UnitSymbols.MU}A"))

    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm
        MyBase.DefineKnownResetState()
        Me.FunctionUnit = Me.DefaultFunctionUnit
    End Sub

#End Region

#Region " APERTURE "

    ''' <summary> Gets or sets The Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property ApertureQueryCommand As String = ":SENS:RES:APER {0}"

    ''' <summary> Gets or sets The Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overrides Property ApertureCommandFormat As String = ":SENS:RES:APER?"

#End Region

#Region " AVERAGE COUNT "

    ''' <summary> Gets or sets The average count command format. </summary>
    ''' <value> The average count command format. </value>
    Protected Overrides Property AverageCountCommandFormat As String = ":SENS:RES:AVER:COUN {0}"

    ''' <summary> Gets or sets The average count query command. </summary>
    ''' <value> The average count query command. </value>
    Protected Overrides Property AverageCountQueryCommand As String = ":SENS:RES:AVER:COUN?"

#End Region

#Region " AVERAGE ENABLED "

    ''' <summary> Gets or sets the Average enabled command Format. </summary>
    ''' <value> The Average enabled query command. </value>
    Protected Overrides Property AverageEnabledCommandFormat As String = ":SENS:RES:AVER:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Average enabled query command. </summary>
    ''' <value> The Average enabled query command. </value>
    Protected Overrides Property AverageEnabledQueryCommand As String = ":SENS:RES:AVER:STAT?"

#End Region

#Region " AVERAGE FILTER TYPE "

    ''' <summary> Gets or sets the Average Filter Type command format. </summary>
    ''' <value> The write Average Filter Type command format. </value>
    Protected Overrides Property AverageFilterTypeCommandFormat As String = ":SENS:RES:AVER:TCON {0}"

    ''' <summary> Gets or sets the Average Filter Type query command. </summary>
    ''' <value> The Average Filter Type query command. </value>
    Protected Overrides Property AverageFilterTypeQueryCommand As String = ":SENS:RES:AVER:TCON?"

#End Region

#Region " AVERAGE PERCENT WINDOW "

    ''' <summary> Gets or sets The Average Percent Window command format. </summary>
    ''' <value> The Average Percent Window command format. </value>
    Protected Overrides Property AveragePercentWindowCommandFormat As String = ":SENS:RES:AVER:ADV:NTOL {0}"

    ''' <summary> Gets or sets The Average Percent Window query command. </summary>
    ''' <value> The Average Percent Window query command. </value>
    Protected Overrides Property AveragePercentWindowQueryCommand As String = ":SENS:RES:AVER:ADV:NTOL?"

#End Region

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:RES:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:RES:RANG:AUTO?"

#End Region

#Region " AUTO ZERO "

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = String.Empty

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:RES:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:RES:NPLC?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:RES:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SENS:RES:RANG?"

#End Region

#Region " RESOLUTION DIGITS "

    ''' <summary> Gets or sets the Resolution Digits command format. </summary>
    ''' <value> The Resolution Digits command format. </value>
    Protected Overrides Property ResolutionDigitsCommandFormat As String = ":SENS:RES:DIG {0:0}"

    ''' <summary> Gets or sets the Resolution Digits query command. </summary>
    ''' <value> The Resolution Digits query command. </value>
    Protected Overrides Property ResolutionDigitsQueryCommand As String = ":SENS:RES:DIG?"

#End Region

End Class

