''' <summary> Implements a measured value amount. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-01 </para>
''' </remarks>
Public Class MeasuredAmount
    Inherits ReadingAmount

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a measured value without specifying the value or its validity, which must be
    ''' specified for the value to be made valid.
    ''' </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    ''' <param name="unit">        The unit. </param>
    Public Sub New(ByVal readingType As ReadingElementTypes, ByVal unit As Arebis.TypedUnits.Unit)
        MyBase.New(readingType, unit)
        Me._MetaStatus = New MetaStatus()
        Me._ComplianceLimitMargin = 0.001
        Me._HighLimit = VI.Pith.Scpi.Syntax.Infinity
        Me._LowLimit = VI.Pith.Scpi.Syntax.NegativeInfinity
        Me._ComplianceLimit = 0
        Me._InfinityUncertainty = 1
    End Sub

    ''' <summary> Constructs a copy of an existing value. </summary>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As MeasuredAmount)
        MyBase.New(model)
        If model IsNot Nothing Then
            Me._HighLimit = model._HighLimit
            Me._LowLimit = model._LowLimit
            Me._MetaStatus = New MetaStatus(model.MetaStatus)
            Me._ComplianceLimit = model._ComplianceLimit
            Me._ComplianceLimitMargin = model._ComplianceLimitMargin
        End If
    End Sub

    ''' <summary> Constructs a copy of an existing value. </summary>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As BufferReading)
        MyBase.New(BufferReading.Validated(model).MeasuredElementType, model.Amount.Unit)
        Me.Value = If(model.HasReading, model.Value, New Double?)
    End Sub

#End Region

#Region " AMOUNT "

    ''' <summary> Resets measured value to nothing. </summary>
    Public Overrides Sub Reset()
        MyBase.Reset()
        Me.MetaStatus.Reset()
        Me.Generator.Min = Me.LowLimit
        Me.Generator.Max = Me.HighLimit
    End Sub

    ''' <summary> Returns the default string representation of the value. </summary>
    ''' <param name="format"> The format string. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        Return If(Me.MetaStatus.IsValid, Me.Amount.ToString(format), Me._MetaStatus.ToShortDescription("p"))
    End Function

#End Region

#Region " META STATUS "

    ''' <summary>
    ''' Gets the infinity uncertainty. Allows the value to be close infinity for triggering the
    ''' infinity status.
    ''' </summary>
    ''' <value> The infinity uncertainty. </value>
    Public Property InfinityUncertainty As Double

    ''' <summary> Gets the measured value meta status. </summary>
    ''' <value> The measured value status. </value>
    Public Property MetaStatus() As MetaStatus

    ''' <summary> Attempts to evaluate using the applied reading and given status. </summary>
    ''' <param name="reading"> The reading. </param>
    ''' <returns> <c>True</c> if evaluated. </returns>
    Public Overrides Function TryEvaluate(ByVal reading As Double) As Boolean
        MyBase.TryEvaluate(reading)
        Me.MetaStatus.HasValue = True
        Me.MetaStatus.Infinity = Math.Abs(reading - VI.Pith.Scpi.Syntax.Infinity) < Me.InfinityUncertainty
        If Me.MetaStatus.Infinity Then Return Me.MetaStatus.IsValid
        Me.MetaStatus.NegativeInfinity = Math.Abs(reading - VI.Pith.Scpi.Syntax.NegativeInfinity) < Me.InfinityUncertainty
        If Me.MetaStatus.NegativeInfinity Then Return Me.MetaStatus.IsValid
        Me.MetaStatus.NotANumber = Math.Abs(reading - VI.Pith.Scpi.Syntax.NotANumber) < Me.InfinityUncertainty
        If Me.MetaStatus.NotANumber Then Return Me.MetaStatus.IsValid
        Me.MetaStatus.HitLevelCompliance = Not (reading >= Me.ComplianceLimitLevel) Xor (Me._ComplianceLimit > 0)
        If Me.MetaStatus.HitLevelCompliance Then Return Me.MetaStatus.IsValid
        Me.MetaStatus.IsHigh = Me.Value.Value.CompareTo(Me.HighLimit) > 0
        Me.MetaStatus.IsLow = Me.Value.Value.CompareTo(Me.LowLimit) < 0
        Return Me.MetaStatus.IsValid
    End Function

    ''' <summary> Attempts to evaluate using the applied reading and given status. </summary>
    ''' <param name="status"> The status. </param>
    ''' <returns> <c>True</c> if evaluated. </returns>
    Public Overrides Function TryEvaluate(ByVal status As Long) As Boolean
        ' update the status to preserve the validity state.
        Me.MetaStatus.Preset(Me.MetaStatus.StatusValue Or status)
        If Me.MetaStatus.IsValid Then Me.TryEvaluate(Me.Value.Value)
        Return Me.MetaStatus.IsValid
    End Function

#End Region

#Region " STATUS "

    ''' <summary> Gets the high limit. </summary>
    ''' <value> The high limit. </value>
    Public Property HighLimit() As Double

    ''' <summary> Gets the low limit. </summary>
    ''' <value> The low limit. </value>
    Public Property LowLimit() As Double

    ''' <summary>
    ''' Gets the compliance limit for testing if the reading exceeded the compliance level.
    ''' </summary>
    ''' <value> A <see cref="System.Double">Double</see> value. </value>
    Public Property ComplianceLimit() As Double

    ''' <summary>
    ''' Gets the margin of how close will allow the measured value to the compliance limit.  For
    ''' instance, if the margin is 0.001, the measured value must not exceed 99.9% of the compliance
    ''' limit. The default is 0.001.
    ''' </summary>
    ''' <value> A <see cref="System.Double">Double</see> value. </value>
    Public Property ComplianceLimitMargin() As Double

    ''' <summary>
    ''' Gets the compliance limit deviation. The factor by which to reduce the complaince limit for
    ''' checking complaince.
    ''' </summary>
    ''' <value> The compliance limit deviation. </value>
    Public Property ComplianceLimitDeviation() As Double

    ''' <summary> Gets the compliance limit level. </summary>
    ''' <value> The compliance limit level. </value>
    Public ReadOnly Property ComplianceLimitLevel() As Double
        Get
            Return Me.ComplianceLimit * (1 - Me.ComplianceLimitDeviation)
        End Get
    End Property

#End Region

#Region " AMOUNT "

    ''' <summary>
    ''' Applies the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <remarks> Assumes that reading is a number. </remarks>
    ''' <param name="rawValueReading"> The raw the value reading. </param>
    ''' <param name="rawUnitsReading"> The raw units reading. </param>
    ''' <returns> <c>True</c> if parsed; Otherwise, <c>False</c>. </returns>
    Public Overrides Function TryApplyReading(ByVal rawValueReading As String, ByVal rawUnitsReading As String) As Boolean
        Me.MetaStatus.Reset()
        Me.MetaStatus.IsValid = MyBase.TryApplyReading(rawValueReading, rawUnitsReading)
        Return Me.MetaStatus.IsValid
    End Function

    ''' <summary>
    ''' Applies the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <remarks> Assumes that reading is a number. </remarks>
    ''' <param name="rawValueReading"> Specifies the value reading. </param>
    ''' <returns> <c>True</c> if parsed; Otherwise, <c>False</c>. </returns>
    Public Overrides Function TryApplyReading(ByVal rawValueReading As String) As Boolean
        Me.MetaStatus.Reset()
        Me.MetaStatus.IsValid = MyBase.TryApplyReading(rawValueReading)
        If Me.MetaStatus.IsValid Then Me.TryEvaluate(Me.Value.Value)
        Return Me.MetaStatus.IsValid
    End Function

    ''' <summary> Applies the unit described by unit. </summary>
    ''' <param name="unit"> The unit. </param>
    Public Overrides Sub ApplyUnit(ByVal unit As Arebis.TypedUnits.Unit)
        If Me.Amount.Unit <> unit Then
            Me.MetaStatus.Reset()
            Me.MetaStatus.IsValid = False
        End If
        MyBase.ApplyUnit(unit)
    End Sub

#End Region

End Class
