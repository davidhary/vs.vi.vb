'---------------------------------------------------------------------------------------------------
' file:		Device\Subsystems\DigitalOutputSubsystem.vb
'
' summary:	Digital output subsystem class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

''' <summary>
''' Defines the contract that must be implemented by a SCPI Digital Output Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class DigitalOutputSubsystem
    Inherits VI.DigitalOutputSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DigitalOutputSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DigitalActiveLevelReadWrites.Clear()
        Me.DigitalActiveLevelReadWrites.Add(VI.DigitalActiveLevels.High, "AHIG", "AHIG", VI.DigitalActiveLevels.High.DescriptionUntil)
        Me.DigitalActiveLevelReadWrites.Add(VI.DigitalActiveLevels.Low, "ALOW", "ALOW", VI.DigitalActiveLevels.Low.DescriptionUntil)
        Me.SupportedDigitalActiveLevels = VI.DigitalActiveLevels.Low Or VI.DigitalActiveLevels.High
    End Sub

#End Region

#Region " OUTPUT SUBSYSTEM "

    ''' <summary> Gets or sets the Output Polarity query command. </summary>
    ''' <remarks> SCPI: OUTP:TTL{0}:LSEN? </remarks>
    ''' <value> The Output Polarity query command. </value>
    Protected Overrides Property DigitalActiveLevelQueryCommand As String = "OUTP:TTL{0}:LSEN?"

    ''' <summary> Gets or sets the Output Polarity command format. </summary>
    ''' <remarks> SCPI: OUTP:TTL{0}:LSEN {1} </remarks>
    ''' <value> The Output Polarity command format. </value>
    Protected Overrides Property DigitalActiveLevelCommandFormat As String = "OUTP:TTL{0}:LSEN {{0}}"

#End Region

#Region " SOURCE SUBSYSTEM "

    ''' <summary> Gets or sets the digital output Level query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LevelQueryCommand As String = ":SOUR:TTL:LEV?"

    ''' <summary> Gets or sets the digital output Level command format. </summary>
    ''' <value> The Level query command format. </value>
    Protected Overrides Property LevelCommandFormat As String = ":SOUR:TTL:LEV {0}"

    ''' <summary> Gets or sets the digital output Line Level query command format. </summary>
    ''' <value> The Line Level query command. </value>
    Protected Overrides Property LineLevelQueryCommand As String = ":SOUR:TTL{0}:LEV?"

    ''' <summary> Gets or sets the digital output Line Level query command format. </summary>
    ''' <value> The Line Level query command format. </value>
    Protected Overrides Property LineLevelCommandFormat As String = ":SOUR:TTL{0}:LEV {{0}}"

#End Region

End Class
