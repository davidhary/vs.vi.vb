''' <summary> Source subsystem. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-03 </para>
''' </remarks>
Public Class SourceSubsystem
    Inherits SourceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SourceSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.CurrentDC).SetRange(-1.05, 1.05)
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.VoltageDC).SetRange(-210, 210)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Level = 0
        Me.Limit = 0.000105
        Me.Range = New Double?
        Me.AutoRangeEnabled = True
        Me.AutoDelayEnabled = True
        Me.FunctionMode = VI.SourceFunctionModes.VoltageDC
        Me.Range = 0.02
        Me.LimitTripped = False
        Me.OutputEnabled = False
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO DELAY ENABLED "

    ''' <summary> Gets or sets the automatic Delay enabled command Format. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledCommandFormat As String = "_G.smu.source.autodelay={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the automatic Delay enabled query command. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledQueryCommand As String = "_G.print(_G.smu.source.autodelay==smu.ON)"

#End Region

#Region " AUTO RANGE STATE "

    ''' <summary> The Auto Range state command format. </summary>
    ''' <value> The automatic range state command format. </value>
    Protected Overrides Property AutoRangeStateCommandFormat As String = "_G.smu.source.autorange={0}"

    ''' <summary> Gets or sets the Auto Range state query command. </summary>
    ''' <value> The AutoRange state query command. </value>
    Protected Overrides Property AutoRangeStateQueryCommand As String = "_G.smu.source.autorange"

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = "_G.smu.source.autorange={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = "_G.print(_G.smu.source.autorange==smu.ON)"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the function mode query command. </summary>
    ''' <value> The function mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = "_G.smu.source.func"

    ''' <summary> Gets or sets the function mode command format. </summary>
    ''' <value> The function mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = "_G.smu.source.func={0}"

#End Region

#Region " LEVEL "

    ''' <summary> Gets or sets the level query command. </summary>
    ''' <value> The level query command. </value>
    Protected Overrides Property LevelQueryCommand As String = "_G.print(_G.smu.source.level)"

    ''' <summary> Gets or sets the level command format. </summary>
    ''' <value> The level command format. </value>
    Protected Overrides Property LevelCommandFormat As String = "_G.smu.source.level={0}"

#End Region

#Region " LIMIT "

    ''' <summary> Gets or sets the limit query command. </summary>
    ''' <value> The limit query command. </value>
    Protected Overrides Property LimitQueryCommandFormat As String = "_G.smu.source.{0}limit.level"

    ''' <summary> Gets or sets the limit command format. </summary>
    ''' <value> The limit command format. </value>
    Protected Overrides Property LimitCommandFormat As String = "_G.smu.source.{0}limit.level={1}"

#End Region

#Region " LIMIT TRIPPED "

    ''' <summary> Gets or sets the limit tripped query command format. </summary>
    ''' <value> The limit tripped query command format. </value>
    Protected Overrides Property LimitTrippedPrintCommandFormat As String = "_G.print(_G.smu.source.{0}limit.tripped==smu.ON)"

#End Region

#Region " OUTPUT ENABLED "

    ''' <summary> Gets or sets the Output enabled command Format. </summary>
    ''' <value> The Output enabled query command. </value>
    Protected Overrides Property OutputEnabledCommandFormat As String = "_G.smu.source.output={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the Output enabled query print command. </summary>
    ''' <value> The Output enabled query command. </value>
    Protected Overrides Property OutputEnabledQueryCommand As String = "_G.print(_G.smu.source.output==smu.ON)"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the Range query command. </summary>
    ''' <value> The Range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = "_G.print(_G.smu.source.range)"

    ''' <summary> Gets or sets the Range command format. </summary>
    ''' <value> The Range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = "_G.smu.source.range={0}"

#End Region

#Region " READ BACK ENABLED "

    ''' <summary> Gets or sets the Read Back enabled command Format. </summary>
    ''' <value> The Read Back enabled query command. </value>
    Protected Overrides Property ReadBackEnabledCommandFormat As String = "_G.smu.source.readback={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the Read Back enabled query command. </summary>
    ''' <value> The Read Back enabled query command. </value>
    Protected Overrides Property ReadBackEnabledQueryCommand As String = "_G.print(_G.smu.source.readback==smu.ON)"

#End Region



#End Region

End Class
