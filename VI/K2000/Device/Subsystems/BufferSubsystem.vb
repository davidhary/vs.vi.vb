﻿''' <summary> Buffer subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class BufferSubsystem
    Inherits VI.BufferSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="BufferSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        Me.New(BufferSubsystem.DefaultBuffer1Name, statusSubsystem)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="bufferName">      Name of the buffer. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal bufferName As String, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.BufferName = bufferName
    End Sub

#End Region

#Region " BUFFER NAME "

    ''' <summary> The default buffer 1 name. </summary>
    Public Const DefaultBuffer1Name As String = "defbuffer1"

    ''' <summary> The default buffer 2 name. </summary>
    Public Const DefaultBuffer2Name As String = "defbuffer2"

    ''' <summary> Name of the buffer. </summary>
    Private _BufferName As String

    ''' <summary> Gets or sets the name of the buffer. </summary>
    ''' <value> The name of the buffer. </value>
    Public Property BufferName As String
        Get
            Return Me._BufferName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.BufferName) Then
                Me._BufferName = If(String.IsNullOrWhiteSpace(value), BufferSubsystem.DefaultBuffer1Name, value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " MEMORY OPTION "

    ''' <summary> The buffer size standard full. </summary>
    Protected Const BufferSizeStandardFull As Integer = 404

    ''' <summary> The buffer size standard compact. </summary>
    Protected Const BufferSizeStandardCompact As Integer = 2027

    ''' <summary> The buffer size memory 1 full. </summary>
    Protected Const BufferSizeMemory1Full As Integer = 1381

    ''' <summary> The buffer size memory 1 compact. </summary>
    Protected Const BufferSizeMemory1Compact As Integer = 6909

    ''' <summary> The buffer size memory 2 full. </summary>
    Protected Const BufferSizeMemory2Full As Integer = 5980

    ''' <summary> The buffer size memory 2 compact. </summary>
    Protected Const BufferSizeMemory2Compact As Integer = 29908

    ''' <summary> The memory option. </summary>
    Private _MemoryOption As Integer?

    ''' <summary> Gets or sets the memory Option. </summary>
    ''' <value> The memory Option. </value>
    Public Property MemoryOption() As Integer?
        Get
            Return Me._MemoryOption
        End Get
        Set(ByVal value As Integer?)
            If Not Integer?.Equals(Me.MemoryOption, value) Then
                Me._MemoryOption = value
                Select Case value
                    Case 0
                        Me.MaximumCapacity = BufferSizeStandardFull
                    Case 1
                        Me.MaximumCapacity = BufferSizeMemory1Full
                    Case 2
                        Me.MaximumCapacity = BufferSizeMemory2Full
                    Case Else
                        Me.MaximumCapacity = BufferSizeStandardFull
                End Select
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the Clear Buffer command. </summary>
    ''' <value> The ClearBuffer command. </value>
    Protected Overrides Property ClearBufferCommand As String = ":TRAC:CLE"

    ''' <summary> Gets or sets the points count query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property CapacityQueryCommand As String = ":TRAC:POIN?"

    ''' <summary> Gets or sets the points count command format. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:COUN {0}". </remarks>
    ''' <value> The points count query command format. </value>
    Protected Overrides Property CapacityCommandFormat As String = ":TRAC:POIN {0}"

    ''' <summary> Gets or sets the ActualPoint count query command. </summary>
    ''' <value> The ActualPoint count query command. </value>
    Protected Overrides Property ActualPointCountQueryCommand As String = String.Empty

    ''' <summary> Gets or sets The First Point Number (1-based) query command. </summary>
    ''' <value> The First Point Number query command. </value>
    Protected Overrides Property FirstPointNumberQueryCommand As String = String.Empty

    ''' <summary> Gets or sets The Last Point Number (1-based) query command. </summary>
    ''' <value> The Last Point Number query command. </value>
    Protected Overrides Property LastPointNumberQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the Buffer Free query command. </summary>
    ''' <value> The buffer free query command. </value>
    Protected Overrides Property BufferFreePointCountQueryCommand As String = ":TRAC:FREE?"

    ''' <summary> Gets or sets the automatic Delay enabled query command. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property FillOnceEnabledQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the automatic Delay enabled command Format. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property FillOnceEnabledCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the buffer read command format. </summary>
    ''' <value> The buffer read command format. </value>
    Public Overrides ReadOnly Property BufferReadCommandFormat As String = ":TRACE:DATA?"

    ''' <summary> Gets or sets the data query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property DataQueryCommand As String = ":TRACE:DATA?"

#End Region


End Class
