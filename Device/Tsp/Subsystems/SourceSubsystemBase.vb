'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SourceSubsystemBase.vb
'
' summary:	Source subsystem base class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary> Defines the contract that must be implemented by a Source Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class SourceSubsystemBase
    Inherits SourceMeasureUnitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.SourceFunction = SourceFunctionMode.VoltageDC
    End Sub

#End Region

#Region " SOURCE FUNCTION "

    ''' <summary> The Source Function. </summary>
    Private _SourceFunction As SourceFunctionMode?

    ''' <summary> Gets or sets the cached Source Function. </summary>
    ''' <value>
    ''' The <see cref="SourceFunctionMode">Source Function</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property SourceFunction As SourceFunctionMode?
        Get
            Return Me._SourceFunction
        End Get
        Protected Set(ByVal value As SourceFunctionMode?)
            If Not Nullable.Equals(Me.SourceFunction, value) Then
                Me._SourceFunction = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Source Function. </summary>
    ''' <param name="value"> The  Source Function. </param>
    ''' <returns>
    ''' The <see cref="SourceFunctionMode">Source Function</see> or none if unknown.
    ''' </returns>
    Public Function ApplySourceFunction(ByVal value As SourceFunctionMode) As SourceFunctionMode?
        Me.WriteSourceFunction(value)
        Return Me.QuerySourceFunction()
    End Function

    ''' <summary> Queries the Source Function. </summary>
    ''' <returns>
    ''' The <see cref="SourceFunctionMode">Source Function</see> or none if unknown.
    ''' </returns>
    Public Function QuerySourceFunction() As SourceFunctionMode?
        Dim currentValue As String = Me.SourceFunction.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(currentValue)
        currentValue = Me.Session.QueryTrimEnd($"_G.print({Me.SourceMeasureUnitReference}.source.func)")
        If String.IsNullOrWhiteSpace(currentValue) Then
            Dim message As String = "Failed fetching Source Function"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.SourceFunction = New SourceFunctionMode?
        Else
            Dim se As New StringEnumerator(Of SourceFunctionMode)
            Me.SourceFunction = se.ParseContained(currentValue.BuildDelimitedValue)
        End If
        Return Me.SourceFunction
    End Function

    ''' <summary> Writes the Source Function Without reading back the value from the device. </summary>
    ''' <param name="value"> The Source Function. </param>
    ''' <returns>
    ''' The <see cref="SourceFunctionMode">Source Function</see> or none if unknown.
    ''' </returns>
    Public Function WriteSourceFunction(ByVal value As SourceFunctionMode) As SourceFunctionMode?
        Me.Session.WriteLine("{0}.source.func = {0}.{1}", Me.SourceMeasureUnitReference, value.ExtractBetween())
        Me.SourceFunction = value
        Return Me.SourceFunction
    End Function

#End Region

End Class

''' <summary> Specifies the source function modes. </summary>
Public Enum SourceFunctionMode

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("None")>
    None

    ''' <summary> An enum constant representing the voltage Device-context option. </summary>
    <Description("DC Voltage (OUTPUT_DCVOLTS)")>
    VoltageDC

    ''' <summary> An enum constant representing the current Device-context option. </summary>
    <Description("DC Current (OUTPUT_DCAMPS)")>
    CurrentDC
End Enum

