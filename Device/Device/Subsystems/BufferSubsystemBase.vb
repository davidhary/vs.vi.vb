'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\BufferSubsystemBase.vb
'
' summary:	Buffer subsystem base class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Threading

Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Defines the contract that must be implemented by a Buffer Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class BufferSubsystemBase
    Inherits SubsystemPlusStatusBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BufferSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._BufferStreamTasker = New isr.Core.Tasker
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                                                                            resources;
    '''                                                                            False if this
    '''                                                                            method releases
    '''                                                                            only unmanaged
    '''                                                                            resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._ItemsLocker IsNot Nothing Then
                    Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
                End If
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Capacity = 0
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Clear Buffer command. </summary>
    ''' <remarks> SCPI: ":TRAC:CLE". </remarks>
    ''' <value> The ClearBuffer command. </value>
    Protected Overridable Property ClearBufferCommand As String

    ''' <summary> Clears the buffer. </summary>
    Public Sub ClearBuffer()
        Me.Write(Me.ClearBufferCommand)
    End Sub

#End Region

#Region " FILL ONCE ENABLED "

    ''' <summary> Fill Once enabled. </summary>
    Private _FillOnceEnabled As Boolean?

    ''' <summary> Gets or sets the cached Fill Once Enabled sentinel. </summary>
    ''' <remarks> When this is enabled, a delay is added before each measurement. </remarks>
    ''' <value>
    ''' <c>null</c> if Fill Once Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property FillOnceEnabled As Boolean?
        Get
            Return Me._FillOnceEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.FillOnceEnabled, value) Then
                Me._FillOnceEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Fill Once Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyFillOnceEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteFillOnceEnabled(value)
        Return Me.QueryFillOnceEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Delay enabled query command. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overridable Property FillOnceEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Fill Once Enabled sentinel. Also sets the
    ''' <see cref="FillOnceEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryFillOnceEnabled() As Boolean?
        Me.FillOnceEnabled = Me.Query(Me.FillOnceEnabled, Me.FillOnceEnabledQueryCommand)
        Return Me.FillOnceEnabled
    End Function

    ''' <summary> Gets or sets the automatic Delay enabled command Format. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overridable Property FillOnceEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Fill Once Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteFillOnceEnabled(ByVal value As Boolean) As Boolean?
        Me.FillOnceEnabled = Me.Write(value, Me.FillOnceEnabledCommandFormat)
        Return Me.FillOnceEnabled
    End Function

#End Region

#Region " CAPACITY "

    ''' <summary> The maximum capacity. </summary>
    Private _MaximumCapacity As Integer?

    ''' <summary> Gets or sets the maximum Buffer Capacity. </summary>
    ''' <value> The Maximum Buffer Capacity or none if not set or unknown. </value>
    Public Overridable Property MaximumCapacity As Integer?
        Get
            Return Me._MaximumCapacity
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.MaximumCapacity, value) Then
                Me._MaximumCapacity = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The capacity. </summary>
    Private _Capacity As Integer?

    ''' <summary> Gets or sets the cached Buffer Capacity. </summary>
    ''' <value> The Buffer Capacity or none if not set or unknown. </value>
    Public Overridable Property Capacity As Integer?
        Get
            Return Me._Capacity
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Capacity, value) Then
                Me._Capacity = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Buffer Capacity. </summary>
    ''' <param name="value"> The current Capacity. </param>
    ''' <returns> The Capacity or none if unknown. </returns>
    Public Function ApplyCapacity(ByVal value As Integer) As Integer?
        Me.WriteCapacity(value)
        Return Me.QueryCapacity()
    End Function

    ''' <summary> Gets or sets the points count query command. </summary>
    ''' <remarks> SCPI: :TRAC:POIN:COUN? </remarks>
    ''' <value> The points count query command. </value>
    Protected Overridable Property CapacityQueryCommand As String

    ''' <summary> Queries the current Capacity. </summary>
    ''' <returns> The Capacity or none if unknown. </returns>
    Public Function QueryCapacity() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.CapacityQueryCommand) Then
            Me.Capacity = Me.Session.Query(0I, Me.CapacityQueryCommand)
        End If
        Return Me.Capacity
    End Function

    ''' <summary> Gets or sets the points count command format. </summary>
    ''' <remarks> SCPI: :TRAC:POIN:COUN {0} </remarks>
    ''' <value> The points count query command format. </value>
    Protected Overridable Property CapacityCommandFormat As String

    ''' <summary> Write the Buffer Capacity without reading back the value from the device. </summary>
    ''' <param name="value"> The current Capacity. </param>
    ''' <returns> The Capacity or none if unknown. </returns>
    Public Function WriteCapacity(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.CapacityCommandFormat) Then
            Me.Session.WriteLine(Me.CapacityCommandFormat, value)
        End If
        Me.Capacity = value
        Return Me.Capacity
    End Function

#End Region

#Region " ACTUAL POINT COUNT "

    ''' <summary> Number of ActualPoint. </summary>
    Private _ActualPointCount As Integer?

    ''' <summary> Gets or sets the cached Buffer ActualPointCount. </summary>
    ''' <value> The Buffer ActualPointCount or none if not set or unknown. </value>
    Public Overloads Property ActualPointCount As Integer?
        Get
            Return Me._ActualPointCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.ActualPointCount, value) Then
                Me._ActualPointCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the ActualPoint count query command. </summary>
    ''' <remarks> SCPI: :TRAC:ACT? </remarks>
    ''' <value> The ActualPoint count query command. </value>
    Protected Overridable Property ActualPointCountQueryCommand As String

    ''' <summary> Queries the current ActualPointCount. </summary>
    ''' <returns> The ActualPointCount or none if unknown. </returns>
    Public Function QueryActualPointCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.ActualPointCountQueryCommand) Then
            Me.ActualPointCount = Me.Session.Query(0I, Me.ActualPointCountQueryCommand)
        End If
        Return Me.ActualPointCount
    End Function

#End Region

#Region " FIRST POINT NUMBER "

    ''' <summary> Number of First Point. </summary>
    Private _FirstPointNumber As Integer?

    ''' <summary> Gets or sets the cached buffer First Point Number. </summary>
    ''' <value> The buffer First Point Number or none if not set or unknown. </value>
    Public Overloads Property FirstPointNumber As Integer?
        Get
            Return Me._FirstPointNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.FirstPointNumber, value) Then
                Me._FirstPointNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets The First Point Number query command. </summary>
    ''' <remarks> SCPI: :TRAC:ACT:STA? </remarks>
    ''' <value> The First Point Number query command. </value>
    Protected Overridable Property FirstPointNumberQueryCommand As String

    ''' <summary> Queries the current FirstPointNumber. </summary>
    ''' <returns> The First Point Number or none if unknown. </returns>
    Public Function QueryFirstPointNumber() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.FirstPointNumberQueryCommand) Then
            Me.FirstPointNumber = Me.Session.Query(0I, Me.FirstPointNumberQueryCommand)
        End If
        Return Me.FirstPointNumber
    End Function

#End Region

#Region " LAST POINT NUMBER "

    ''' <summary> Number of Last Point. </summary>
    Private _LastPointNumber As Integer?

    ''' <summary> Gets or sets the cached buffer Last Point Number. </summary>
    ''' <value> The buffer Last Point Number or none if not set or unknown. </value>
    Public Overloads Property LastPointNumber As Integer?
        Get
            Return Me._LastPointNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.LastPointNumber, value) Then
                Me._LastPointNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets The Last Point Number query command. </summary>
    ''' <remarks> SCPI: :TRAC:ACT:END? </remarks>
    ''' <value> The Last Point Number query command. </value>
    Protected Overridable Property LastPointNumberQueryCommand As String

    ''' <summary> Queries the current Last Point Number. </summary>
    ''' <returns> The LastPointNumber or none if unknown. </returns>
    Public Function QueryLastPointNumber() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.LastPointNumberQueryCommand) Then
            Me.LastPointNumber = Me.Session.Query(0I, Me.LastPointNumberQueryCommand)
        End If
        Return Me.LastPointNumber
    End Function

#End Region

#Region " AVAILABLE POINT COUNT "

    ''' <summary> Number of Available Points. </summary>
    Private _AvailablePointCount As Integer?

    ''' <summary> Gets or sets the number of points still available to fill in the buffer. </summary>
    ''' <value> The Available Points Count or none if not set or unknown. </value>
    Public Overloads Property AvailablePointCount As Integer?
        Get
            Return Me._AvailablePointCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.AvailablePointCount, value) Then
                Me._AvailablePointCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Available Points count query command. </summary>
    ''' <remarks> SCPI: ":TRAC:FREE?". </remarks>
    ''' <value> The Available Points count query command. </value>
    Protected Overridable Property AvailablePointCountQueryCommand As String

    ''' <summary> Queries the current Available Point Count. </summary>
    ''' <returns> The Available Point Count or none if unknown. </returns>
    Public Function QueryAvailablePointCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.AvailablePointCountQueryCommand) Then
            Me.AvailablePointCount = Me.Session.Query(0I, Me.AvailablePointCountQueryCommand)
        End If
        Return Me.AvailablePointCount
    End Function

#End Region

#Region " BUFFER FREE "

    ''' <summary> Gets or sets the Buffer Free query command. </summary>
    ''' <remarks> SCPI: ":TRAC:FREE?". </remarks>
    ''' <value> The buffer free query command. </value>
    Protected Overridable Property BufferFreePointCountQueryCommand As String

    ''' <summary> Queries the buffer free count. </summary>
    ''' <returns> The buffer free and actual point count. </returns>
    Public Function QueryBufferFreePointCount() As (BufferFree As Integer, ActualPointCount As Integer)
        Dim bufferFree As Integer = 0
        Dim actualPointCount As Integer = 0
        If Not String.IsNullOrWhiteSpace(Me.ActualPointCountQueryCommand) Then
            Dim reading As String = Me.Session.QueryTrimTermination(Me.BufferFreePointCountQueryCommand)
            If Not String.IsNullOrEmpty(reading) Then
                Dim values As New Queue(Of String)(reading.Split(","c))
                If values.Any AndAlso Integer.TryParse(values.Dequeue, bufferFree) Then
                    If values.Any AndAlso Integer.TryParse(values.Dequeue, actualPointCount) Then
                    End If
                End If
            End If
        End If
        Me.ActualPointCount = actualPointCount
        Me.AvailablePointCount = bufferFree
        Return (bufferFree, actualPointCount)
    End Function

#End Region

#Region " DATA "

    ''' <summary> String. </summary>
    Private _Data As String

    ''' <summary> Gets or sets the cached Buffer Data. </summary>
    ''' <value> The data. </value>
    Public Overloads Property Data As String
        Get
            Return Me._Data
        End Get
        Protected Set(ByVal value As String)
            If Not Nullable.Equals(Me.Data, value) Then
                Me._Data = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the data query command. </summary>
    ''' <remarks> SCPI: ":TRAC:DATA?". </remarks>
    ''' <value> The data query command. </value>
    Protected Overridable Property DataQueryCommand As String

    ''' <summary> Queries the current Data. </summary>
    ''' <returns> The Data or none if unknown. </returns>
    Public Function QueryData() As String
        If Not String.IsNullOrWhiteSpace(Me.DataQueryCommand) Then
            Me.Session.WriteLine(Me.DataQueryCommand)
            ' read the entire data.
            Me.Data = Me.Session.ReadFreeLineTrimEnd()
        End If
        Return Me.Data
    End Function

    ''' <summary> Queries the current Data. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The Data or empty if none. </returns>
    Public Function QueryData(ByVal queryCommand As String) As String
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            Me.Data = String.Empty
            Me.Session.WriteLine(queryCommand)
            ' read the entire data.
            Me.Data = Me.Session.ReadFreeLineTrimEnd()
        End If
        Return Me.Data
    End Function

#End Region

#Region " BUFFER STREAM "

    ''' <summary> Queries the current Data. </summary>
    ''' <returns> The Data or empty if none. </returns>
    Public Function QueryBufferReadings() As IList(Of BufferReading)
        Dim count As Integer = Me.QueryActualPointCount().GetValueOrDefault(0)
        If count > 0 Then
            Dim first As Integer = Me.QueryFirstPointNumber().GetValueOrDefault(0)
            Dim last As Integer = Me.QueryLastPointNumber().GetValueOrDefault(0)
            Return Me.QueryBufferReadings(first, last)
        Else
            Return New List(Of BufferReading)
        End If
    End Function

    ''' <summary> Gets or sets the buffer read command format. </summary>
    ''' <value> The buffer read command format. </value>
    Public Overridable ReadOnly Property BufferReadCommandFormat As String = ":TRAC:DATA? {0},{1},'defbuffer1',READ,TST,STAT,UNIT"

    ''' <summary> Queries the current Data. </summary>
    ''' <param name="firstIndex"> Zero-based index of the first. </param>
    ''' <param name="lastIndex">  Zero-based index of the last. </param>
    ''' <returns> The Data or empty if none. </returns>
    Public Function QueryBufferReadings(ByVal firstIndex As Integer, ByVal lastIndex As Integer) As IList(Of BufferReading)
        Me.QueryData(String.Format(Me.BufferReadCommandFormat, firstIndex, lastIndex))
        Return Me.EnumerateBufferReadings(Me.Data)
    End Function

    ''' <summary> List of types of the ordered reading elements. </summary>
    Private _OrderedReadingElementTypes As IEnumerable(Of ReadingElementTypes) = New List(Of ReadingElementTypes) From {VI.ReadingElementTypes.Reading, VI.ReadingElementTypes.Timestamp,
                                                                                          VI.ReadingElementTypes.Status, VI.ReadingElementTypes.Units}

    ''' <summary> Gets or sets a list of types of the readings. </summary>
    ''' <value> A list of types of the readings. </value>
    Public Property OrderedReadingElementTypes As IEnumerable(Of ReadingElementTypes)
        Get
            Return Me._OrderedReadingElementTypes
        End Get
        Set(value As IEnumerable(Of ReadingElementTypes))
            If Not Array.Equals(value, Me.OrderedReadingElementTypes) Then
                Me._OrderedReadingElementTypes = value
                Me.NotifyPropertyChanged()
            End If
            Me.ReadingElementTypes = BufferSubsystemBase.JoinReadingElementTypes(value)
        End Set
    End Property

    ''' <summary> Join reading Element types. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The combined ReadingElementTypes. </returns>
    Private Shared Function JoinReadingElementTypes(ByVal values As IEnumerable(Of ReadingElementTypes)) As ReadingElementTypes
        Dim result As ReadingElementTypes
        For Each v As VI.ReadingElementTypes In values
            result = result Or v
        Next
        Return result
    End Function

    ''' <summary> Type of the measured element. </summary>
    Private _MeasuredElementType As ReadingElementTypes

    ''' <summary> Gets or sets the type of the measured element. </summary>
    ''' <value> The type of the measured element. </value>
    Public Property MeasuredElementType As ReadingElementTypes
        Get
            Return Me._MeasuredElementType
        End Get
        Set(value As ReadingElementTypes)
            If Me.MeasuredElementType <> value Then
                Me._MeasuredElementType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> List of types of the reading elements. </summary>
    Private _ReadingElementTypes As ReadingElementTypes

    ''' <summary> Gets or sets a list of types of the reading elements. </summary>
    ''' <value> A list of types of the reading elements. </value>
    Public Property ReadingElementTypes As ReadingElementTypes
        Get
            Return Me._ReadingElementTypes
        End Get
        Set(value As ReadingElementTypes)
            If Me.ReadingElementTypes <> value Then
                Me._ReadingElementTypes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Enumerates the buffer readings in this collection. </summary>
    ''' <param name="commaSeparatedValues"> The comma separated values. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the buffer readings in this
    ''' collection.
    ''' </returns>
    Public Function EnumerateBufferReadings(ByVal commaSeparatedValues As String) As IList(Of BufferReading)
        Dim l As New List(Of BufferReading)
        If Not String.IsNullOrWhiteSpace(commaSeparatedValues) Then
            Dim q As New Queue(Of String)(commaSeparatedValues.Split(","c))
            Do While q.Any
                Dim reading As BufferReading = New BufferReading(Me.MeasuredElementType, q, Me.OrderedReadingElementTypes)
                If Not Me.OrderedReadingElementTypes.Contains(ReadingElementTypes.Units) Then
                    reading.BuildAmount(Me.BufferReadingUnit)
                End If
                l.Add(reading)
            Loop
        End If
        Return l
    End Function

    ''' <summary> Gets a queue of new buffer readings. </summary>
    ''' <value> A thread safe Queue of buffer readings. </value>
    Public ReadOnly Property NewBufferReadingsQueue As System.Collections.Concurrent.ConcurrentQueue(Of BufferReading)

    ''' <summary> Gets the buffer reading binding list. </summary>
    ''' <value> The buffer readings. </value>
    Public ReadOnly Property BufferReadingsBindingList As New BufferReadingBindingList

    ''' <summary> Gets the number of buffer readings. </summary>
    ''' <value> The number of buffer readings. </value>
    Public ReadOnly Property BufferReadingsCount As Integer
        Get
            Return Me.BufferReadingsBindingList.Count
        End Get
    End Property

    ''' <summary> The buffer reading unit. </summary>
    Private _BufferReadingUnit As Arebis.TypedUnits.Unit

    ''' <summary> Gets or sets the buffer reading unit. </summary>
    ''' <value> The buffer reading unit. </value>
    Public Property BufferReadingUnit As Arebis.TypedUnits.Unit
        Get
            Return Me._BufferReadingUnit
        End Get
        Set(value As Arebis.TypedUnits.Unit)
            If Not Arebis.TypedUnits.Unit.Equals(value, Me.BufferReadingUnit) Then
                Me._BufferReadingUnit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last reading. </summary>
    Private _LastReading As BufferReading

    ''' <summary> Gets or sets the last buffer reading. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastReading As BufferReading
        Get
            Return Me._LastReading
        End Get
        Set(value As BufferReading)
            Me._LastReading = value
            If value Is Nothing Then
                Me.LastReadingCaption = "-.---"
                Me.LastRawReading = String.Empty
                Me.LastReadingStatus = String.Empty
            Else
                Me.LastReadingCaption = Me.LastReading.ReadingCaption
                Me.LastRawReading = Me.LastReading.Reading
                Me.LastReadingStatus = Me.LastReading.StatusReading
            End If
        End Set
    End Property

    ''' <summary> The last reading status. </summary>
    Private _LastReadingStatus As String

    ''' <summary> Gets or sets the last buffer reading readings. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastReadingStatus As String
        Get
            Return Me._LastReadingStatus
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastReadingStatus) Then
                Me._LastReadingStatus = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last raw reading. </summary>
    Private _LastRawReading As String

    ''' <summary> Gets or sets the last buffer reading readings. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastRawReading As String
        Get
            Return Me._LastRawReading
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastRawReading) Then
                Me._LastRawReading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last reading caption. </summary>
    Private _LastReadingCaption As String

    ''' <summary> Gets or sets the last buffer reading. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastReadingCaption As String
        Get
            Return Me._LastReadingCaption
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastReadingCaption) Then
                Me._LastReadingCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the items locker. </summary>
    ''' <value> The items locker. </value>
    Protected ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    ''' <summary> Enqueue range. </summary>
    ''' <param name="items"> The items. </param>
    Public Sub EnqueueRange(ByVal items As IList(Of BufferReading))
        If items Is Nothing OrElse Not items.Any Then Return
        Me.ItemsLocker.EnterWriteLock()
        Try
            For Each item As BufferReading In items
                Me.NewBufferReadingsQueue.Enqueue(item)
            Next item
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Enumerates dequeue range in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process dequeue range in this collection.
    ''' </returns>
    Public Function DequeueRange() As IList(Of BufferReading)
        Dim result As New List(Of BufferReading)
        Dim value As BufferReading = Nothing
        Do While Me.NewBufferReadingsQueue.Any
            If Me.NewBufferReadingsQueue.TryDequeue(value) Then
                result.Add(value)
            End If
        Loop
        Return result
    End Function

    ''' <summary> The buffer streaming enabled. </summary>
    Private ReadOnly _BufferStreamingEnabled As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the buffer streaming enabled. </summary>
    ''' <value> The buffer streaming enabled. </value>
    Public Property BufferStreamingEnabled As Boolean
        Get
            Return Me._BufferStreamingEnabled.Value
        End Get
        Set(value As Boolean)
            If Me.BufferStreamingEnabled <> value Then
                Me._BufferStreamingEnabled.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The buffer streaming active. </summary>
    Private ReadOnly _BufferStreamingActive As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the buffer streaming Active. </summary>
    ''' <value> The buffer streaming Active. </value>
    Public Property BufferStreamingActive As Boolean
        Get
            Return Me._BufferStreamingActive.Value
        End Get
        Set(value As Boolean)
            If Me.BufferStreamingActive <> value Then
                Me._BufferStreamingActive.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The buffer streaming alert. </summary>
    Private ReadOnly _BufferStreamingAlert As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the buffer streaming Alert. </summary>
    ''' <value> The buffer streaming Alert. </value>
    Public Property BufferStreamingAlert As Boolean
        Get
            Return Me._BufferStreamingAlert.Value
        End Get
        Set(value As Boolean)
            If Me.BufferStreamingAlert <> value Then
                Me._BufferStreamingAlert.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Stream buffer. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    Private Sub StreamBufferThis(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan)
        Dim first As Integer = 0
        Dim last As Integer = 0
        Me.BufferStreamingEnabled = True
        Me.BufferReadingsBindingList.Clear()
        Me._NewBufferReadingsQueue = New Concurrent.ConcurrentQueue(Of BufferReading)
        triggerSubsystem.QueryTriggerState()
        Me.BufferStreamingActive = True
        Dim pollStopwatch As Stopwatch = Stopwatch.StartNew
        Do While triggerSubsystem.IsTriggerStateActive AndAlso Me.BufferStreamingEnabled
            If first = 0 Then first = Me.QueryFirstPointNumber().GetValueOrDefault(0)
            If first > 0 Then
                last = Me.QueryLastPointNumber().GetValueOrDefault(0)
                If (last - first + 1) > Me.BufferReadingsCount Then
                    Dim newReadings As IList(Of BufferReading) = Me.QueryBufferReadings(Me.BufferReadingsCount + 1, last)
                    Me.BufferReadingsBindingList.Add(newReadings)
                    Me.EnqueueRange(newReadings)
                    Me.SyncNotifyPropertyChanged(NameOf(VI.BufferSubsystemBase.BufferReadingsCount))
                    isr.Core.ApplianceBase.DoEvents()
                    pollStopwatch.Restart()
                End If
            End If
            pollPeriod.Subtract(pollStopwatch.Elapsed).SpinWait()
            pollStopwatch.Restart()
            isr.Core.ApplianceBase.DoEvents()
            triggerSubsystem.QueryTriggerState()
        Loop
        Me.BufferStreamingActive = False
    End Sub

    ''' <summary> Stream buffer. </summary>
    ''' <remarks> David, 2020-07-23. </remarks>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Overridable Sub StreamBuffer(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan)
        Try
            Me.StreamBufferThis(triggerSubsystem, pollPeriod)
        Catch
            ' stop buffer streaming; the exception is handled in the Async Completed event handler
            Me.BufferStreamingActive = False
            Me.BufferStreamingAlert = True
            Me.BufferStreamingEnabled = False
        End Try
    End Sub

    ''' <summary> Gets or sets the buffer stream tasker. </summary>
    ''' <value> The buffer stream tasker. </value>
    Public ReadOnly Property BufferStreamTasker As isr.Core.Tasker

    ''' <summary> Starts buffer stream. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    Public Sub StartBufferStream(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan)
        If triggerSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(triggerSubsystem))
        Me._BufferStreamTasker.StartAction(Sub()
                                               Me.StreamBuffer(triggerSubsystem, pollPeriod)
                                           End Sub)
    End Sub

    ''' <summary>
    ''' Estimates the stream stop timeout interval as scale margin times the cycle and poll intervals.
    ''' </summary>
    ''' <remarks> David, 2020-04-13. </remarks>
    ''' <param name="streamCycleDuration"> Duration of the stream cycle. </param>
    ''' <param name="pollInterval">        The poll interval. </param>
    ''' <param name="scaleMargin">         The scale margin. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function EstimateStreamStopTimeoutInterval(ByVal streamCycleDuration As TimeSpan, ByVal pollInterval As TimeSpan, ByVal scaleMargin As Double) As TimeSpan
        Return TimeSpan.FromTicks(CLng(scaleMargin * (streamCycleDuration.Ticks + pollInterval.Ticks)))
    End Function

    ''' <summary> Stops buffer stream and wait for completion or timeout. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function StopBufferStream(ByVal timeout As TimeSpan) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String)
        Me.BufferStreamingEnabled = False
        Dim status As Tasks.TaskStatus = Me.BufferStreamTasker.AwaitCompletion(timeout)
        result = If(Tasks.TaskStatus.RanToCompletion = status,
                        If(Me.BufferStreamingActive,
                                        (False, $"Failed stopping buffer streaming; still active"),
                                        (True, "buffer streaming completed")),
                        (False, $"buffer streaming task existed with a {status} {NameOf(Tasks.TaskStatus)}"))
        Return result
    End Function

#End Region

End Class

