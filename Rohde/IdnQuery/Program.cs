// This example uses Manufacturer-independent interface 
// You need to adjust the Resource string to fit your instrument before running the example.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ivi.Visa; //This .NET assembly is installed with IVI.NET Shared Components

namespace Rohde.IdnQuery
{
    /// <summary>
    /// Example of manufacturer-independent interface Ivi.Visa.NET
    /// Before running the example, adjust the ResourceName to fit your instrument
    /// </summary>
    class Program
    {
        private static readonly string _ResourceName = Rohde.IdnQuery.Properties.Settings.Default.ResourceName ;

        static void Main()
        {
            IMessageBasedSession io;
            // Separate try-catch for the instrument initialization prevents accessing uninitialized object
            try
            {
                // Initialization:
                io = GlobalResourceManager.Open(_ResourceName) as IMessageBasedSession;
                io.Clear();
                // Timeout for VISA Read Operations
                io.TimeoutMilliseconds = 3000;
            }
            catch (NativeVisaException e)
            {
                Console.WriteLine("Error initializing the io session:\n{0}", e.Message);
                Console.WriteLine("Press any key to finish.");
                Console.ReadKey();
                return;
            }
            
            // try block to catch any InstrumentErrorException()
            try
            {
                Console.WriteLine("IVI GlobalResourceManager selected the following VISA.NET:");
                Console.WriteLine("Manufacturer: {0}", io.ResourceManufacturerName);
                Console.WriteLine("Implementation Version: {0}\n", io.ResourceImplementationVersion);

                io.RawIO.Write("*RST;*CLS"); // Reset the instrument, clear the Error queue
                io.RawIO.Write("*IDN?");
                var idnResponse = io.RawIO.ReadString();

                Console.WriteLine("Instrument Identification string:\n{0}", idnResponse);
            }
            catch (VisaException e)
            {
                Console.WriteLine("Instrument reports error(s):\n{0}", e.Message);
            }
            finally
            {
                Console.WriteLine("Press any key to finish.");
                Console.ReadKey();
            }
        }
    }
}
