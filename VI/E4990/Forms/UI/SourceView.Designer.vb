﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SourceView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._ApplySourceSettingButton = New System.Windows.Forms.Button()
        Me._SourceFunctionComboBoxLabel = New System.Windows.Forms.Label()
        Me._LevelNumericLabel = New System.Windows.Forms.Label()
        Me._SourceFunctionComboBox = New System.Windows.Forms.ComboBox()
        Me._LevelNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ApplySourceFunctionButton = New System.Windows.Forms.Button()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        CType(Me._LevelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(381, 324)
        Me._Layout.TabIndex = 0
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._ApplySourceSettingButton)
        Me._Panel.Controls.Add(Me._SourceFunctionComboBoxLabel)
        Me._Panel.Controls.Add(Me._LevelNumericLabel)
        Me._Panel.Controls.Add(Me._SourceFunctionComboBox)
        Me._Panel.Controls.Add(Me._LevelNumeric)
        Me._Panel.Controls.Add(Me._ApplySourceFunctionButton)
        Me._Panel.Location = New System.Drawing.Point(39, 99)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(302, 125)
        Me._Panel.TabIndex = 0
        '
        '_ApplySourceSettingButton
        '
        Me._ApplySourceSettingButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ApplySourceSettingButton.Location = New System.Drawing.Point(227, 83)
        Me._ApplySourceSettingButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ApplySourceSettingButton.Name = "_ApplySourceSettingButton"
        Me._ApplySourceSettingButton.Size = New System.Drawing.Size(58, 30)
        Me._ApplySourceSettingButton.TabIndex = 28
        Me._ApplySourceSettingButton.Text = "Apply"
        Me._ApplySourceSettingButton.UseVisualStyleBackColor = True
        '
        '_SourceFunctionComboBoxLabel
        '
        Me._SourceFunctionComboBoxLabel.AutoSize = True
        Me._SourceFunctionComboBoxLabel.Location = New System.Drawing.Point(17, 19)
        Me._SourceFunctionComboBoxLabel.Name = "_SourceFunctionComboBoxLabel"
        Me._SourceFunctionComboBoxLabel.Size = New System.Drawing.Size(59, 17)
        Me._SourceFunctionComboBoxLabel.TabIndex = 26
        Me._SourceFunctionComboBoxLabel.Text = "Function:"
        '
        '_LevelNumericLabel
        '
        Me._LevelNumericLabel.AutoSize = True
        Me._LevelNumericLabel.Location = New System.Drawing.Point(125, 53)
        Me._LevelNumericLabel.Name = "_LevelNumericLabel"
        Me._LevelNumericLabel.Size = New System.Drawing.Size(71, 17)
        Me._LevelNumericLabel.TabIndex = 30
        Me._LevelNumericLabel.Text = "Level [mA]:"
        '
        '_SourceFunctionComboBox
        '
        Me._SourceFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SourceFunctionComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SourceFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
        Me._SourceFunctionComboBox.Location = New System.Drawing.Point(79, 15)
        Me._SourceFunctionComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SourceFunctionComboBox.Name = "_SourceFunctionComboBox"
        Me._SourceFunctionComboBox.Size = New System.Drawing.Size(150, 25)
        Me._SourceFunctionComboBox.TabIndex = 27
        '
        '_LevelNumeric
        '
        Me._LevelNumeric.Location = New System.Drawing.Point(199, 49)
        Me._LevelNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._LevelNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 196608})
        Me._LevelNumeric.Name = "_LevelNumeric"
        Me._LevelNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LevelNumeric.Size = New System.Drawing.Size(85, 25)
        Me._LevelNumeric.TabIndex = 31
        Me._LevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._LevelNumeric.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        '_ApplySourceFunctionButton
        '
        Me._ApplySourceFunctionButton.Location = New System.Drawing.Point(232, 12)
        Me._ApplySourceFunctionButton.Name = "_ApplySourceFunctionButton"
        Me._ApplySourceFunctionButton.Size = New System.Drawing.Size(53, 30)
        Me._ApplySourceFunctionButton.TabIndex = 29
        Me._ApplySourceFunctionButton.Text = "Apply"
        Me._ApplySourceFunctionButton.UseVisualStyleBackColor = True
        '
        'SourceView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Name = "SourceView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(383, 326)
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        CType(Me._LevelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _ApplySourceSettingButton As Windows.Forms.Button
    Private WithEvents _SourceFunctionComboBoxLabel As Windows.Forms.Label
    Private WithEvents _LevelNumericLabel As Windows.Forms.Label
    Private WithEvents _SourceFunctionComboBox As Windows.Forms.ComboBox
    Private WithEvents _LevelNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ApplySourceFunctionButton As Windows.Forms.Button
End Class
