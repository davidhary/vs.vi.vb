**HIPOT 2017**

**Release Notes**

Current release: 1.2.6319; 04/20/17

*1.2.6318 04/20/17*  
Adds exception extensions to display full blown
exception information.

*1.2.5362 09/06/14*  
Tested with Visual Studio 2013. Updated to NI Visa for
.Net 4.0. Uses the same dynamic link libraries and TTM 2014 revisions
*3.x and 4.x. Installation package includes both 32 and 64 bit
assemblies.

*1.1.4905 06/06/13*  
Converted to Visual Studio 2010. Removed COM project.
Remove VB6 project.

\(c\) 2010 by Integrated Scientific Resources, Inc. All rights reserved.
