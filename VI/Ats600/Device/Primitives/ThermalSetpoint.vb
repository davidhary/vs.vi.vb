﻿''' <summary> Thermal setpoint. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-03-26 </para>
''' </remarks>
Public Class ThermalSetpoint

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="ordinalNumber"> The ordinal number. </param>
    Public Sub New(ByVal ordinalNumber As Integer)
        Me.New()
        Me._OrdinalNumber = ordinalNumber
    End Sub

    ''' <summary> Gets or sets the profile element. </summary>
    ''' <value> The setpoint number. </value>
    Public Property OrdinalNumber As Integer

    ''' <summary> Gets or sets the temperature. </summary>
    ''' <value> The temperature. </value>
    Public Property Temperature As Double

    ''' <summary> Gets or sets the ramp rate in degrees per second. </summary>
    ''' <value> The ramp rate. </value>
    Public Property RampRate As Double

    ''' <summary> Gets or sets the soak seconds. </summary>
    ''' <value> The soak seconds. </value>
    Public Property SoakSeconds As Integer

    ''' <summary> Gets or sets the window. </summary>
    ''' <value> The window. </value>
    Public Property Window As Double

End Class

''' <summary> Thermal profile collection. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-03-26 </para>
''' </remarks>
Public Class ThermalProfileCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, ThermalSetpoint)

    ''' <summary> Gets key for item. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The item. </param>
    ''' <returns> The key for item. </returns>
    Protected Overrides Function GetKeyForItem(item As ThermalSetpoint) As Integer
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.OrdinalNumber
    End Function

    ''' <summary> Adds a new setpoint. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="ordinalNumber"> The ordinal number. </param>
    ''' <returns> A ThermalSetpoint. </returns>
    Public Function AddNewSetpoint(ByVal ordinalNumber As Integer) As ThermalSetpoint
        Dim setPoint As ThermalSetpoint = New ThermalSetpoint(ordinalNumber)
        Me.Add(setPoint)
        Return setPoint
    End Function

End Class