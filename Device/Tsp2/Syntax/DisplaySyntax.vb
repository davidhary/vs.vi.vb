﻿'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Syntax\DisplaySyntax.vb
'
' summary:	Display syntax class
'---------------------------------------------------------------------------------------------------
Namespace TspSyntax.Display

    ''' <summary> Defines the TSP Display syntax. Modified for TSP2. </summary>
    ''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2005-01-15, 1.0.1841.x. </para></remarks>
    Public Module DisplaySyntax

        ''' <summary> The name of the display subsystem. </summary>
        Public Const SubsystemName As String = "display"

        ''' <summary> The clear display command. </summary>
        Public Const ClearCommand As String = "display.clear()"

        ''' <summary> The display screen command format. </summary>
        Public Const DisplayScreenCommandFormat As String = "display.changescreen(_G.display.SCREEN_{0})"

        ''' <summary> The set text format. </summary>
        Public Const SetTextLineCommandFormat As String = "display.settext(display.TEXT{0},'{1}')"

        ''' <summary> The restore main screen and wait complete command. </summary>
        Public Const RestoreMainWaitCompleteCommand As String = "display.screen = display.MAIN or 0 _G.waitcomplete(0)"

        ''' <summary> The length of the first line. </summary>
        Public Const FirstLineLength As Integer = 20

        ''' <summary> The length of the second line. </summary>
        Public Const SecondLineLength As Integer = 32

    End Module

End Namespace

