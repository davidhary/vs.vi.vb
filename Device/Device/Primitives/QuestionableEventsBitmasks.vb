'---------------------------------------------------------------------------------------------------
' file:		.\Primitives\QuestionableEventsBitmasks.vb
'
' summary:	Questionable events bitmasks class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

''' <summary>
''' A dictionary of measurement events bitmasks capable of detecting bits status.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-12-19 </para>
''' </remarks>
<Serializable>
Public Class QuestionableEventsBitmaskDictionary
    Inherits BitmasksDictionary

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary> Adds key. </summary>
    ''' <param name="key">     The bitmask key. </param>
    ''' <param name="bitmask"> The bitmask. </param>
    Public Overloads Sub Add(ByVal key As QuestionableEventBitmaskKey, ByVal bitmask As Integer)
        Me.Add(CInt(key), bitmask)
    End Sub

    ''' <summary> Adds key. </summary>
    ''' <param name="key">            The bitmask key. </param>
    ''' <param name="bitmask">        The bitmask. </param>
    ''' <param name="excludeFromAll"> True to exclude, false to include from all. </param>
    Public Overloads Sub Add(ByVal key As QuestionableEventBitmaskKey, ByVal bitmask As Integer, excludeFromAll As Boolean)
        Me.Add(CInt(key), bitmask, excludeFromAll)
    End Sub

    ''' <summary> Query if 'status' bas any bit on. </summary>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> <c>true</c> if any bit on; otherwise <c>false</c> </returns>
    Public Overloads Function IsAnyBitOn(ByVal status As Integer, ByVal key As QuestionableEventBitmaskKey) As Boolean
        Return Me.IsAnyBitOn(status, CInt(key))
    End Function

    ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> A Boolean. </returns>
    Public Overloads Function AreAllBitsOn(ByVal status As Integer, ByVal key As QuestionableEventBitmaskKey) As Boolean
        Return Me.AreAllBitsOn(status, CInt(key))
    End Function

    ''' <summary> Return the masked status value. </summary>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> An Integer? </returns>
    Public Overloads Function MaskedValue(ByVal status As Integer, ByVal key As QuestionableEventBitmaskKey) As Integer
        Return Me.MaskedValue(status, CInt(key))
    End Function

    ''' <summary> Query if 'status' bas any bit on. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> <c>true</c> if any bit on; otherwise <c>false</c> </returns>
    Public Overloads Function IsAnyBitOn(ByVal key As QuestionableEventBitmaskKey) As Boolean
        Return Me.IsAnyBitOn(Me.Status, key)
    End Function

    ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> A Boolean. </returns>
    Public Overloads Function AreAllBitsOn(ByVal key As QuestionableEventBitmaskKey) As Boolean
        Return Me.AreAllBitsOn(Me.Status, key)
    End Function

    ''' <summary> Return the masked status value. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> An Integer? </returns>
    Public Overloads Function MaskedValue(ByVal key As QuestionableEventBitmaskKey) As Integer
        Return Me.MaskedValue(Me.Status, key)
    End Function

End Class

''' <summary> Values that represent questionable event bitmask keys. </summary>
Public Enum QuestionableEventBitmaskKey

    ''' <summary> The questionable event command warning bitmask key value. </summary>
    <Description("Command Warning")>
    CommandWarning

    ''' <summary> The questionable event calibration summary bitmask key value. </summary>
    <Description("Calibration Summary")>
    CalibrationSummary

    ''' <summary> The questionable event temperature summary bitmask key value. </summary>
    <Description("Temperature Summary")>
    TemperatureSummary

End Enum
