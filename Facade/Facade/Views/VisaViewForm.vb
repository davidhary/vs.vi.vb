''' <summary> Form for viewing the visa view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-01-03 </para>
''' </remarks>
Public Class VisaViewForm
    Inherits isr.Core.Forma.ConsoleForm

#Region " CONSTRUCTION and CLEAN UP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.Name = "Visa.View.Form"
    End Sub

    ''' <summary> Creates a new VisaViewForm. </summary>
    ''' <returns> A VisaViewForm. </returns>
    Public Shared Function Create() As VisaViewForm
        Dim visaView As VisaView = Nothing
        Try
            visaView = New VisaView()
            Return VisaViewForm.Create(visaView)
        Catch
            If visaView IsNot Nothing Then
                visaView.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary> Creates a new VisaViewForm. </summary>
    ''' <param name="visaView"> The visa view. </param>
    ''' <returns> A VisaViewForm. </returns>
    Public Shared Function Create(ByVal visaView As VisaView) As VisaViewForm
        Dim result As VisaViewForm = Nothing
        Try
            result = New VisaViewForm With {.VisaView = visaView}
        Catch
            If result IsNot Nothing Then
                result.Dispose()
            End If

            Throw
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                If Me.VisaViewDisposeEnabled AndAlso Me.VisaView IsNot Nothing Then Me.VisaView.Dispose()
                If Me.VisaSessionBase IsNot Nothing Then Me.VisaSessionBase.Dispose() : Me.VisaSessionBase = Nothing
                Me._VisaView = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding Visa View"
            ' the talker control publishes the device messages which thus get published to the form message box.
            Me.AddTalkerControl(Me.VisaView.VisaSessionBase.CandidateResourceTitle, Me.VisaView, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.MyLibrary.Logger)
            If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.CandidateResourceName) Then
                activity = $"{Me.Name}; starting {Me.VisaView.VisaSessionBase.CandidateResourceName} selection task"
                Me.VisaView.VisaSessionBase.AsyncValidateResourceName(Me.VisaView.VisaSessionBase.CandidateResourceName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await the resource name validation task enabled. </summary>
    ''' <value> The await the resource name validation task enabled. </value>
    Protected Property AwaitResourceNameValidationTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.VisaView.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)
            If Me.VisaView.VisaSessionBase.IsValidatingResourceName Then
                If Me.AwaitResourceNameValidationTaskEnabled Then
                    activity = $"{Me.Name}; awaiting {Me.VisaView.VisaSessionBase.CandidateResourceName} validation"
                    Me.VisaView.VisaSessionBase.AwaitResourceNameValidation(My.Settings.ResourceNameSelectionTimeout)
                Else
                    activity = $"{Me.Name}; validating {Me.VisaView.VisaSessionBase.CandidateResourceName}"
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.VisaView IsNot Nothing Then Me.VisaView.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " VISA VIEW "

    ''' <summary> Gets or sets the visa view. </summary>
    ''' <value> The visa view. </value>
    Public Property VisaView As isr.VI.Facade.VisaView

    ''' <summary> Gets or sets the visa view dispose enabled. </summary>
    ''' <value> The visa view dispose enabled. </value>
    Public Property VisaViewDisposeEnabled As Boolean

    ''' <summary> Gets or sets the locally assigned visa session if any. </summary>
    ''' <value> The visa session. </value>
    Protected Property VisaSessionBase As VisaSessionBase

    ''' <summary> Creates view. </summary>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    Private Sub CreateViewView(ByVal candidateResourceName As String, ByVal candidateResourceTitle As String)
        Me.VisaSessionBase = New VisaSession
        Me.VisaView = New isr.VI.Facade.VisaView(Me.VisaSessionBase)
        Me.VisaView.VisaSessionBase.CandidateResourceTitle = candidateResourceTitle
        Me.VisaView.VisaSessionBase.CandidateResourceName = candidateResourceName
    End Sub

#End Region

#Region " SHOW DIALOG "

    ''' <summary> Shows the form dialog. </summary>
    ''' <param name="owner">                  The owner. </param>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal candidateResourceName As String, ByVal candidateResourceTitle As String) As Windows.Forms.DialogResult
        Dim visaView As isr.VI.Facade.VisaView = Nothing
        Try
            Me.CreateViewView(candidateResourceName, candidateResourceTitle)
            Return Me.ShowDialog(owner, visaView)
        Catch
            If visaView IsNot Nothing Then
                visaView.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary> Shows the form dialog. </summary>
    ''' <param name="owner">    The owner. </param>
    ''' <param name="visaView"> The visa view. </param>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal visaView As isr.VI.Facade.VisaView) As Windows.Forms.DialogResult
        Me.VisaView = visaView
        Return MyBase.ShowDialog(owner)
    End Function

    ''' <summary> Shows the form. </summary>
    ''' <param name="owner">                  The owner. </param>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal candidateResourceName As String, ByVal candidateResourceTitle As String)
        Dim visaView As isr.VI.Facade.VisaView = Nothing
        Try
            Me.CreateViewView(candidateResourceName, candidateResourceTitle)
            Me.Show(owner, visaView)
        Catch
            If visaView IsNot Nothing Then
                visaView.Dispose()
            End If

            Throw
        End Try
    End Sub

    ''' <summary> Shows the form. </summary>
    ''' <param name="owner">    The owner. </param>
    ''' <param name="visaView"> The visa view. </param>
    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal visaView As isr.VI.Facade.VisaView)
        Me.VisaView = visaView
        MyBase.Show(owner)
    End Sub

#End Region

End Class
