''' <summary>
''' This is a test class for T1700.MeasureSubsystemTest and is intended to contain all
''' MeasureSubsystemTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-10-12. </remarks>
<TestClass(), TestCategory("t1700")>
Public Class T1700MeasureSubsystemTest

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="rangeMode">       The range mode. </param>
    ''' <param name="expectedCurrent"> The expected current. </param>
    ''' <param name="expectedRange">   The expected range. </param>
    Public Sub TryParseTest(ByVal rangeMode As T1700.ResistanceRangeMode, ByVal expectedCurrent As String, ByVal expectedRange As String)
        Dim current As String = String.Empty
        Dim range As String = String.Empty
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = T1700.MeasureSubsystem.TryParse(rangeMode, current, range)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expectedCurrent, current)
        Assert.AreEqual(expectedRange, range)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryParseTest()
        Me.TryParseTest(T1700.ResistanceRangeMode.R10, "10 mA", "200 ohm")
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="rangeMode">       The range mode. </param>
    ''' <param name="expectedCurrent"> The expected current. </param>
    ''' <param name="expectedRange">   The expected range. </param>
    Public Sub TryParseTest(ByVal rangeMode As T1700.ResistanceRangeMode, ByVal expectedCurrent As Double, ByVal expectedRange As Double)
        Dim current As Double
        Dim range As Double
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = T1700.MeasureSubsystem.TryParse(rangeMode, current, range)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expectedCurrent, current, 0.00001 * current)
        Assert.AreEqual(expectedRange, range, 0.00001 * range)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryParseTestNumericR10()
        Me.TryParseTest(T1700.ResistanceRangeMode.R10, 0.01, 200)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryParseTestNumericR1()
        Me.TryParseTest(T1700.ResistanceRangeMode.R1, 1, 0.002)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryParseTestNumericR5()
        Me.TryParseTest(T1700.ResistanceRangeMode.R5, 0.1, 0.2)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryParseTestNumericR14()
        Me.TryParseTest(T1700.ResistanceRangeMode.R14, 0.0001, 2000)
    End Sub

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryParseTestNumericR19()
        Me.TryParseTest(T1700.ResistanceRangeMode.R19, 0.0000001, 20000000.0)
    End Sub

    ''' <summary> A test for Try Convert. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="current">           The current. </param>
    ''' <param name="range">             The range. </param>
    ''' <param name="expectedRangeMode"> The expected range mode. </param>
    Public Sub TryConvertTest(ByVal current As Double, ByVal range As Double, ByVal expectedRangeMode As T1700.ResistanceRangeMode)
        Dim rangeMode As T1700.ResistanceRangeMode = T1700.ResistanceRangeMode.R0
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = T1700.MeasureSubsystem.TryConvert(current, range, rangeMode)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expectedRangeMode, rangeMode)
    End Sub

    ''' <summary> A test for Try Convert. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TryConvertTestNumericR10()
        Me.TryConvertTest(0.01, 200, T1700.ResistanceRangeMode.R10)
    End Sub

End Class

