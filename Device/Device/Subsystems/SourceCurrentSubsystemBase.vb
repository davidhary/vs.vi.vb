''' <summary>
''' Defines the contract that must be implemented by a SCPI Source Current Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SourceCurrentSubsystemBase
    Inherits VI.SourceFunctionSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceCurrentSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SOUR:CURR:RANG:AUTO?"

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SOUR:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}"

#End Region

#Region " LEVEL "

    ''' <summary> Gets or sets the Level query command. </summary>
    ''' <value> The Level query command. </value>
    Protected Overrides Property LevelQueryCommand As String = ":SOUR:CURR?"

    ''' <summary> Gets or sets the Level command format. </summary>
    ''' <value> The Level command format. </value>
    Protected Overrides Property LevelCommandFormat As String = ":SOUR:CURR {0}"

#End Region

#Region " PROTECTION ENABLED "

    ''' <summary> The protection enabled. </summary>
    Private _ProtectionEnabled As Boolean?

    ''' <summary>
    ''' Gets or sets a cached value indicating whether source current protection is enabled.
    ''' </summary>
    ''' <remarks>
    ''' :SOURCE:CURR:PROT:STAT The setter enables or disables the over-current protection (OCP)
    ''' function. The enabled state is On (1); the disabled state is Off (0). If the over-current
    ''' protection function is enabled and the output goes into constant current operation, the
    ''' output is disabled and OCP is set in the Questionable Condition status register. The *RST
    ''' value = Off.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property ProtectionEnabled As Boolean?
        Get
            Return Me._ProtectionEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ProtectionEnabled, value) Then
                Me._ProtectionEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Protection Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyProtectionEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteProtectionEnabled(value)
        Return Me.QueryProtectionEnabled()
    End Function

    ''' <summary> Gets or sets the Protection enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR:CURR:PROT:STAT?". </remarks>
    ''' <value> The Protection enabled query command. </value>
    Protected MustOverride ReadOnly Property ProtectionEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Protection Enabled sentinel. Also sets the
    ''' <see cref="ProtectionEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryProtectionEnabled() As Boolean?
        Me.ProtectionEnabled = Me.Query(Me.ProtectionEnabled, Me.ProtectionEnabledQueryCommand)
        Return Me.ProtectionEnabled
    End Function

    ''' <summary> Gets or sets the Protection enabled command Format. </summary>
    ''' <remarks> SCPI: ":SOUR:CURR:PROT:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Protection enabled query command. </value>
    Protected MustOverride ReadOnly Property ProtectionEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Protection Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteProtectionEnabled(ByVal value As Boolean) As Boolean?
        Me.ProtectionEnabled = Me.Write(value, Me.ProtectionEnabledCommandFormat)
        Return Me.ProtectionEnabled
    End Function

#End Region

#Region " SWEEP START LEVEL "

    ''' <summary> Gets or sets the Sweep Start Level query command. </summary>
    ''' <value> The Sweep Start Level query command. </value>
    Protected Overrides Property SweepStartLevelQueryCommand As String = ":SOUR:CURR:STAR?"

    ''' <summary> Gets or sets the Sweep Start Level command format. </summary>
    ''' <value> The Sweep Start Level command format. </value>
    Protected Overrides Property SweepStartLevelCommandFormat As String = ":SOUR:CURR:STAR {0}"

#End Region

#Region " SWEEP STOP LEVEL "

    ''' <summary> Gets or sets the Sweep Stop Level query command. </summary>
    ''' <value> The Sweep Stop Level query command. </value>
    Protected Overrides Property SweepStopLevelQueryCommand As String = ":SOUR:CURR:STOP?"

    ''' <summary> Gets or sets the Sweep Stop Level command format. </summary>
    ''' <value> The Sweep Stop Level command format. </value>
    Protected Overrides Property SweepStopLevelCommandFormat As String = ":SOUR:CURR:STOP {0}"

#End Region

#Region " SWEEP MODE "

    ''' <summary> Gets or sets the Sweep Mode  query command. </summary>
    ''' <value> The Sweep Mode  query command. </value>
    Protected Overrides Property SweepModeQueryCommand As String = ":SOUR:CURR:MODE?"

    ''' <summary> Gets or sets the Sweep Mode  command format. </summary>
    ''' <value> The Sweep Mode  command format. </value>
    Protected Overrides Property SweepModeCommandFormat As String = ":SOUR:CURR:MODE {0}"

#End Region

End Class
