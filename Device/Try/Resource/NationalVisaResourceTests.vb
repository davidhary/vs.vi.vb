'---------------------------------------------------------------------------------------------------
' file:		.\Resource\ResourceTests.vb
'
' summary:	Resource tests class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

Imports Microsoft.Win32

''' <summary> A resource tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-11 </para>
''' </remarks>
<TestClass(), TestCategory("ni.visa")>
Public Class NationalVisaResourceTests

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    #End Region

    #Region " VERSION TESTS "

    ''' <summary> (Unit Test Method) tests visa functional versions. </summary>
    <TestMethod()>
    Public Sub VisaFunctionalVersionsTest()
        Dim actualVersion As Version = isr.VI.National.Visa.ResourceManager.ImplementationVersion
        Dim expectedVersion As New Version(isr.VI.National.Visa.My.Settings.Default.ImplementationVersion)
        Assert.AreEqual(expectedVersion.ToString, actualVersion.ToString, $"{NameOf(isr.VI.National.Visa.ResourceManager.ImplementationVersion).SplitWords} should match")

        actualVersion = isr.VI.National.Visa.ResourceManager.SpecificationVersion
        expectedVersion = New Version(isr.VI.National.Visa.My.Settings.Default.SpecificationVersion)
        Assert.AreEqual(expectedVersion.ToString, actualVersion.ToString, $"{NameOf(isr.VI.National.Visa.ResourceManager.SpecificationVersion).SplitWords} should match")
    End Sub

    #End Region

End Class
