'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\ScanListView.vb
'
' summary:	Scan list view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Scan list view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ScanListView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ScanView"/> </summary>
    ''' <returns> A <see cref="ScanView"/>. </returns>
    Public Shared Function Create() As ScanView
        Dim view As ScanView = Nothing
        Try
            view = New ScanView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Gets or sets a list of scans. </summary>
    ''' <value> A List of scans. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ScanList As String
        Get
            Return Me._ScanListLabel.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ScanList) Then
                Me._ScanListLabel.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a list of candidate scans. </summary>
    ''' <value> A List of candidate scans. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property CandidateScanList As String
        Get
            Return Me._CandidateScanListComboBox.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.CandidateScanList) Then
                Me.AddScanList(Me._CandidateScanListComboBox, value)
                Me._CandidateScanListComboBox.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Adds a scan list 'value' to the combo box. </summary>
    ''' <param name="comboBox"> The combo box. </param>
    ''' <param name="value">    The value. </param>
    Private Sub AddScanList(ByVal comboBox As ToolStripComboBox, ByVal value As String)
        If Me.Visible Then
            ' check if we are asking for a new channel list
            If comboBox IsNot Nothing AndAlso Not String.IsNullOrEmpty(value) AndAlso comboBox.FindStringExact(value) < 0 Then
                ' if we have a new string, add it to the channel list
                comboBox.Items.Add(value)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Displays a scan list described by value and adds it tot he list of candidate scan lists.
    ''' </summary>
    ''' <param name="value"> The value. </param>
    Public Sub DisplayScanList(ByVal value As String)
        Me.AddScanList(Me._CandidateScanListComboBox, value)
        Me.ScanList = value
    End Sub

    ''' <summary> Applies the settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} updating the scan list"
            Me.RouteSubsystem.ApplyScanList(Me._CandidateScanListComboBox.Text)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadSettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading the scan list settings"
            ScanListView.ReadSettings(Me.RouteSubsystem)
            Me.ApplyPropertyChanged(Me.RouteSubsystem)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Saves to memory. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub SaveToMemory()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} checking candidate memory location {Me._MemoryLocationTextBox.Text}"
            If ScanListView.IsValidCandidateValue(Me._MemoryLocationTextBox.Text, 1, 10) Then
                activity = $"{Me.Device.ResourceNameCaption} saving to memory"
                Me.Device.ClearExecutionState()
                Me.Device.Session.EnableServiceRequestWaitComplete()
                Me.RouteSubsystem.SaveChannelPattern(Int32.Parse(Me._MemoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture),
                                                        TimeSpan.FromSeconds(1))
            Else
                activity = $"Candidate memory location {Me._MemoryLocationTextBox.Text} is out of range [1,10]"
                Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Alert, activity)
                Me.PublishWarning(activity)
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Opens all channels. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub OpenAllChannels()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} opening all channels"
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            Me.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Closes scan list. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub CloseScanList()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} closing a channel"
            Me.Device.Session.EnableServiceRequestWaitComplete()
            Me.RouteSubsystem.ApplyClosedChannels(Me._CandidateScanListComboBox.Text, TimeSpan.FromSeconds(1))
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Opens scan list. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenScanList()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} opening a channel"
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            Me.RouteSubsystem.ApplyOpenChannels(Me._CandidateScanListComboBox.Text, TimeSpan.FromSeconds(1))
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub


#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(ScanListView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the Route subsystem. </summary>
    ''' <value> The Route subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property RouteSubsystem As RouteSubsystemBase

    ''' <summary> Bind Route subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As RouteSubsystemBase)
        If Me.RouteSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.RouteSubsystem)
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.RouteSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As RouteSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.RouteSubsystemPropertyChanged
            ScanListView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.RouteSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As RouteSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(RouteSubsystemBase.ScanList))
        Me.HandlePropertyChanged(subsystem, NameOf(RouteSubsystemBase.ClosedChannels))
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As RouteSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryScanList()
        subsystem.QueryClosedChannels()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the Route subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As RouteSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(RouteSubsystemBase.ScanList)
                Me.DisplayScanList(subsystem.ScanList)
            Case NameOf(RouteSubsystemBase.ClosedChannels)
                Me._ClosedChannelsLabel.Text = subsystem.ClosedChannels
        End Select
    End Sub

    ''' <summary> Route subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RouteSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(RouteSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.RouteSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.RouteSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, RouteSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Applies the settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Query if 'candidateValue' is valid candidate value. </summary>
    ''' <param name="candidateValue"> The candidate value. </param>
    ''' <param name="minimum">        The minimum. </param>
    ''' <param name="maximum">        The maximum. </param>
    ''' <returns> True if valid candidate value, false if not. </returns>
    Private Shared Function IsValidCandidateValue(ByVal candidateValue As String, ByVal minimum As Integer, ByVal maximum As Integer) As Boolean
        Dim result As Boolean
        Dim value As Integer
        result = Not String.IsNullOrWhiteSpace(candidateValue.Trim) AndAlso
                Integer.TryParse(candidateValue, System.Globalization.NumberStyles.Integer,
                                 Globalization.CultureInfo.CurrentCulture, value) AndAlso (value >= minimum AndAlso value <= maximum)
        Return result
    End Function

    ''' <summary> Memory location text box validating. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MemoryLocationTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _MemoryLocationTextBox.Validating
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} validating memory location"
            e.Cancel = Not ScanListView.IsValidCandidateValue(Me._MemoryLocationTextBox.Text, 1, 10)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        End Try
    End Sub

    ''' <summary> Saves to memoty location menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SaveToMemotyLocationMenuItem_Click(sender As Object, e As EventArgs) Handles _SaveToMemotyLocationMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.SaveToMemory()
    End Sub

    ''' <summary> Opens all menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenAllMenuItem_Click(sender As Object, e As EventArgs) Handles _OpenAllMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.OpenAllChannels()
    End Sub

    ''' <summary> Opens menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenMenuItem_Click(sender As Object, e As EventArgs) Handles _OpenMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.OpenScanList()
    End Sub

    ''' <summary> Closes menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CloseMenuItem_Click(sender As Object, e As EventArgs) Handles _CloseMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.CloseScanList()
    End Sub


#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
