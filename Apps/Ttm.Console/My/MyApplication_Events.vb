'---------------------------------------------------------------------------------------------------
' file:		.\My\MyApplication.vb
'
' summary:	My application class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Destroys objects for this project. </summary>
        ''' <remarks> David, 2020-10-11. </remarks>
        Friend Sub Destroy()
            If Me.SplashScreen IsNot Nothing Then
                Me.SplashScreen.Close()
                Me.SplashScreen.Dispose()
            End If
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary> Builds the default caption. </summary>
        ''' <remarks> David, 2020-10-11. </remarks>
        ''' <returns> The caption. </returns>
        Friend Function BuildDefaultCaption() As String
            Dim suffix As New System.Text.StringBuilder
            suffix.Append(" ")
            Return isr.Core.ApplicationInfo.BuildApplicationTitleCaption(suffix.ToString)
        End Function

        ''' <summary> Instantiates the application to its known state. </summary>
        ''' <remarks> David, 2020-10-11. </remarks>
        ''' <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Function TryinitializeKnownState() As Boolean

            Dim affirmative As Boolean = True
            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

                ' show status
                If My.MyApplication.InDesignMode Then
                    Me.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Design Mode.")
                Else
                    Me.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Runtime Mode.")
                End If

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.SplashTraceEvent(TraceEventType.Error, "Exception occurred initializing application known state;. {0}", ex.ToFullBlownString)
                affirmative = False

            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try
            Return affirmative

        End Function

        ''' <summary> Processes the shut down. </summary>
        ''' <remarks> David, 2020-10-11. </remarks>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ProcessShutDown()
            Try
                If My.Application.SaveMySettingsOnExit Then
                    Me.Logger.TraceEventOverride(TraceEventType.Verbose, My.MyApplication.TraceEventId,
                                                            "Saving assembly settings")
                    ' Save library settings here
                End If
            Catch
            Finally
            End Try

        End Sub

        ''' <summary>
        ''' Processes the startup. Sets the event arguments
        ''' <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see>
        ''' value if failed.
        ''' </summary>
        ''' <remarks> David, 2020-10-11. </remarks>
        ''' <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        '''                  instance containing the event data. </param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)
            If e IsNot Nothing Then
                Me.SplashTraceEvent(TraceEventType.Verbose, My.MyApplication.TraceEventId, "Parsing command line")
            End If
        End Sub

    End Class

End Namespace

