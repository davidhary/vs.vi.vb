'---------------------------------------------------------------------------------------------------
' file:		.\My\MyLibrary.vb
'
' summary:	My library class
'---------------------------------------------------------------------------------------------------
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = VI.Pith.My.ProjectTraceEventId.TryTests

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "VI Try"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Unit Tests for the VI Libraries"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "isr.VI.Try"

    End Class

End Namespace

