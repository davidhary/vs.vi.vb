﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CalibrateView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim MySettings1 As isr.VI.E4990.My.MySettings = New isr.VI.E4990.My.MySettings()
        Me._CalLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._CompensationGroupBox = New System.Windows.Forms.GroupBox()
        Me._AdapterComboBox = New System.Windows.Forms.ComboBox()
        Me._AdapterComboBoxLabel = New System.Windows.Forms.Label()
        Me._AcquireCompensationButton = New System.Windows.Forms.Button()
        Me._LoadCompensationTextBoxLabel = New System.Windows.Forms.Label()
        Me._ShortCompensationTextBoxLabel = New System.Windows.Forms.Label()
        Me._OpenCompensationTextBoxLabel = New System.Windows.Forms.Label()
        Me._FrequencyStimulusTextBox = New System.Windows.Forms.TextBox()
        Me._FrequencyStimulusTextBoxLabel = New System.Windows.Forms.Label()
        Me._LoadCompensationTextBox = New System.Windows.Forms.TextBox()
        Me._ShortCompensationTextBox = New System.Windows.Forms.TextBox()
        Me._OpenCompensationTextBox = New System.Windows.Forms.TextBox()
        Me._ApplyLoadButton = New System.Windows.Forms.Button()
        Me._ApplyShortButton = New System.Windows.Forms.Button()
        Me._ApplyOpenButton = New System.Windows.Forms.Button()
        Me._CalLayout.SuspendLayout()
        Me._CompensationGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_CalLayout
        '
        Me._CalLayout.ColumnCount = 3
        Me._CalLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._CalLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._CalLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._CalLayout.Controls.Add(Me._CompensationGroupBox, 1, 1)
        Me._CalLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._CalLayout.Location = New System.Drawing.Point(1, 1)
        Me._CalLayout.Name = "_CalLayout"
        Me._CalLayout.RowCount = 3
        Me._CalLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._CalLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._CalLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._CalLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._CalLayout.Size = New System.Drawing.Size(379, 322)
        Me._CalLayout.TabIndex = 4
        '
        '_CompensationGroupBox
        '
        Me._CompensationGroupBox.Controls.Add(Me._AdapterComboBox)
        Me._CompensationGroupBox.Controls.Add(Me._AdapterComboBoxLabel)
        Me._CompensationGroupBox.Controls.Add(Me._AcquireCompensationButton)
        Me._CompensationGroupBox.Controls.Add(Me._LoadCompensationTextBoxLabel)
        Me._CompensationGroupBox.Controls.Add(Me._ShortCompensationTextBoxLabel)
        Me._CompensationGroupBox.Controls.Add(Me._OpenCompensationTextBoxLabel)
        Me._CompensationGroupBox.Controls.Add(Me._FrequencyStimulusTextBox)
        Me._CompensationGroupBox.Controls.Add(Me._FrequencyStimulusTextBoxLabel)
        Me._CompensationGroupBox.Controls.Add(Me._LoadCompensationTextBox)
        Me._CompensationGroupBox.Controls.Add(Me._ShortCompensationTextBox)
        Me._CompensationGroupBox.Controls.Add(Me._OpenCompensationTextBox)
        Me._CompensationGroupBox.Controls.Add(Me._ApplyLoadButton)
        Me._CompensationGroupBox.Controls.Add(Me._ApplyShortButton)
        Me._CompensationGroupBox.Controls.Add(Me._ApplyOpenButton)
        Me._CompensationGroupBox.Location = New System.Drawing.Point(48, 42)
        Me._CompensationGroupBox.Name = "_CompensationGroupBox"
        Me._CompensationGroupBox.Size = New System.Drawing.Size(283, 237)
        Me._CompensationGroupBox.TabIndex = 0
        Me._CompensationGroupBox.TabStop = False
        Me._CompensationGroupBox.Text = "Compensation"
        '
        '_AdapterComboBox
        '
        Me._AdapterComboBox.Location = New System.Drawing.Point(164, 202)
        Me._AdapterComboBox.Name = "_AdapterComboBox"
        Me._AdapterComboBox.Size = New System.Drawing.Size(108, 25)
        Me._AdapterComboBox.TabIndex = 10
        '
        '_AdapterComboBoxLabel
        '
        Me._AdapterComboBoxLabel.AutoSize = True
        Me._AdapterComboBoxLabel.Location = New System.Drawing.Point(103, 206)
        Me._AdapterComboBoxLabel.Name = "_AdapterComboBoxLabel"
        Me._AdapterComboBoxLabel.Size = New System.Drawing.Size(58, 17)
        Me._AdapterComboBoxLabel.TabIndex = 9
        Me._AdapterComboBoxLabel.Text = "Adapter:"
        '
        '_AcquireCompensationButton
        '
        Me._AcquireCompensationButton.Location = New System.Drawing.Point(6, 201)
        Me._AcquireCompensationButton.Name = "_AcquireCompensationButton"
        Me._AcquireCompensationButton.Size = New System.Drawing.Size(55, 26)
        Me._AcquireCompensationButton.TabIndex = 8
        Me._AcquireCompensationButton.Text = "New"
        Me._AcquireCompensationButton.UseVisualStyleBackColor = True
        '
        '_LoadCompensationTextBoxLabel
        '
        Me._LoadCompensationTextBoxLabel.AutoSize = True
        Me._LoadCompensationTextBoxLabel.Location = New System.Drawing.Point(20, 149)
        Me._LoadCompensationTextBoxLabel.Name = "_LoadCompensationTextBoxLabel"
        Me._LoadCompensationTextBoxLabel.Size = New System.Drawing.Size(40, 17)
        Me._LoadCompensationTextBoxLabel.TabIndex = 7
        Me._LoadCompensationTextBoxLabel.Text = "Load:"
        '
        '_ShortCompensationTextBoxLabel
        '
        Me._ShortCompensationTextBoxLabel.AutoSize = True
        Me._ShortCompensationTextBoxLabel.Location = New System.Drawing.Point(18, 100)
        Me._ShortCompensationTextBoxLabel.Name = "_ShortCompensationTextBoxLabel"
        Me._ShortCompensationTextBoxLabel.Size = New System.Drawing.Size(42, 17)
        Me._ShortCompensationTextBoxLabel.TabIndex = 5
        Me._ShortCompensationTextBoxLabel.Text = "Short:"
        '
        '_OpenCompensationTextBoxLabel
        '
        Me._OpenCompensationTextBoxLabel.AutoSize = True
        Me._OpenCompensationTextBoxLabel.Location = New System.Drawing.Point(17, 52)
        Me._OpenCompensationTextBoxLabel.Name = "_OpenCompensationTextBoxLabel"
        Me._OpenCompensationTextBoxLabel.Size = New System.Drawing.Size(43, 17)
        Me._OpenCompensationTextBoxLabel.TabIndex = 3
        Me._OpenCompensationTextBoxLabel.Text = "Open:"
        '
        '_FrequencyStimulusTextBox
        '
        MySettings1.AdapterType = String.Empty
        MySettings1.ClearRefractoryPeriod = System.TimeSpan.Parse("00:00:00.1000000")
        MySettings1.DeviceClearRefractoryPeriod = System.TimeSpan.Parse("00:00:01.0500000")
        MySettings1.FrequencyArrayReading = String.Empty
        MySettings1.InitializeTimeout = System.TimeSpan.Parse("00:00:05")
        MySettings1.InitRefractoryPeriod = System.TimeSpan.Parse("00:00:00.1000000")
        MySettings1.InterfaceClearRefractoryPeriod = System.TimeSpan.Parse("00:00:00.5000000")
        MySettings1.LoadCompensationReading = String.Empty
        MySettings1.LoadResistance = New Decimal(New Integer() {10, 0, 0, 0})
        MySettings1.OpenCompensationReading = String.Empty
        MySettings1.ResetRefractoryPeriod = System.TimeSpan.Parse("00:00:00.2000000")
        MySettings1.SessionMessageNotificationLevel = 0
        MySettings1.SettingsKey = String.Empty
        MySettings1.ShortCompensationReading = String.Empty
        MySettings1.TraceLogLevel = System.Diagnostics.TraceEventType.Warning
        MySettings1.TraceShowLevel = System.Diagnostics.TraceEventType.Warning
        MySettings1.YardstickInductanceLimit = New Decimal(New Integer() {1, 0, 0, 393216})
        MySettings1.YardstickResistance = New Decimal(New Integer() {5, 0, 0, 0})
        MySettings1.YardstickResistanceTolerance = New Decimal(New Integer() {1, 0, 0, 131072})
        Me._FrequencyStimulusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings1, "FrequencyArrayReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._FrequencyStimulusTextBox.Location = New System.Drawing.Point(63, 22)
        Me._FrequencyStimulusTextBox.Name = "_FrequencyStimulusTextBox"
        Me._FrequencyStimulusTextBox.ReadOnly = True
        Me._FrequencyStimulusTextBox.Size = New System.Drawing.Size(211, 25)
        Me._FrequencyStimulusTextBox.TabIndex = 1
        Me._FrequencyStimulusTextBox.Text = MySettings1.FrequencyArrayReading
        '
        '_FrequencyStimulusTextBoxLabel
        '
        Me._FrequencyStimulusTextBoxLabel.AutoSize = True
        Me._FrequencyStimulusTextBoxLabel.Location = New System.Drawing.Point(17, 26)
        Me._FrequencyStimulusTextBoxLabel.Name = "_FrequencyStimulusTextBoxLabel"
        Me._FrequencyStimulusTextBoxLabel.Size = New System.Drawing.Size(44, 17)
        Me._FrequencyStimulusTextBoxLabel.TabIndex = 0
        Me._FrequencyStimulusTextBoxLabel.Text = "F [Hz]:"
        '
        '_LoadCompensationTextBox
        '
        Me._LoadCompensationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings1, "LoadCompensationReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._LoadCompensationTextBox.Location = New System.Drawing.Point(62, 149)
        Me._LoadCompensationTextBox.Multiline = True
        Me._LoadCompensationTextBox.Name = "_LoadCompensationTextBox"
        Me._LoadCompensationTextBox.ReadOnly = True
        Me._LoadCompensationTextBox.Size = New System.Drawing.Size(212, 45)
        Me._LoadCompensationTextBox.TabIndex = 3
        Me._LoadCompensationTextBox.Text = MySettings1.LoadCompensationReading
        '
        '_ShortCompensationTextBox
        '
        Me._ShortCompensationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings1, "ShortCompensationReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._ShortCompensationTextBox.Location = New System.Drawing.Point(62, 100)
        Me._ShortCompensationTextBox.Multiline = True
        Me._ShortCompensationTextBox.Name = "_ShortCompensationTextBox"
        Me._ShortCompensationTextBox.ReadOnly = True
        Me._ShortCompensationTextBox.Size = New System.Drawing.Size(212, 45)
        Me._ShortCompensationTextBox.TabIndex = 3
        Me._ShortCompensationTextBox.Text = MySettings1.ShortCompensationReading
        '
        '_OpenCompensationTextBox
        '
        Me._OpenCompensationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings1, "OpenCompensationReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._OpenCompensationTextBox.Location = New System.Drawing.Point(62, 51)
        Me._OpenCompensationTextBox.Multiline = True
        Me._OpenCompensationTextBox.Name = "_OpenCompensationTextBox"
        Me._OpenCompensationTextBox.ReadOnly = True
        Me._OpenCompensationTextBox.Size = New System.Drawing.Size(212, 45)
        Me._OpenCompensationTextBox.TabIndex = 2
        Me._OpenCompensationTextBox.Text = MySettings1.OpenCompensationReading
        '
        '_ApplyLoadButton
        '
        Me._ApplyLoadButton.Location = New System.Drawing.Point(6, 168)
        Me._ApplyLoadButton.Name = "_ApplyLoadButton"
        Me._ApplyLoadButton.Size = New System.Drawing.Size(55, 26)
        Me._ApplyLoadButton.TabIndex = 6
        Me._ApplyLoadButton.Text = "Apply"
        Me._ApplyLoadButton.UseVisualStyleBackColor = True
        '
        '_ApplyShortButton
        '
        Me._ApplyShortButton.Location = New System.Drawing.Point(6, 119)
        Me._ApplyShortButton.Name = "_ApplyShortButton"
        Me._ApplyShortButton.Size = New System.Drawing.Size(55, 26)
        Me._ApplyShortButton.TabIndex = 4
        Me._ApplyShortButton.Text = "Apply"
        Me._ApplyShortButton.UseVisualStyleBackColor = True
        '
        '_ApplyOpenButton
        '
        Me._ApplyOpenButton.Location = New System.Drawing.Point(6, 70)
        Me._ApplyOpenButton.Name = "_ApplyOpenButton"
        Me._ApplyOpenButton.Size = New System.Drawing.Size(55, 26)
        Me._ApplyOpenButton.TabIndex = 2
        Me._ApplyOpenButton.Text = "Apply"
        Me._ApplyOpenButton.UseVisualStyleBackColor = True
        '
        'CalibrateView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._CalLayout)
        Me.Name = "CalibrateView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._CalLayout.ResumeLayout(False)
        Me._CompensationGroupBox.ResumeLayout(False)
        Me._CompensationGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _CalLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _CompensationGroupBox As Windows.Forms.GroupBox
    Private WithEvents _AdapterComboBox As Windows.Forms.ComboBox
    Private WithEvents _AdapterComboBoxLabel As Windows.Forms.Label
    Private WithEvents _AcquireCompensationButton As Windows.Forms.Button
    Private WithEvents _LoadCompensationTextBoxLabel As Windows.Forms.Label
    Private WithEvents _ShortCompensationTextBoxLabel As Windows.Forms.Label
    Private WithEvents _OpenCompensationTextBoxLabel As Windows.Forms.Label
    Private WithEvents _FrequencyStimulusTextBox As Windows.Forms.TextBox
    Private WithEvents _FrequencyStimulusTextBoxLabel As Windows.Forms.Label
    Private WithEvents _LoadCompensationTextBox As Windows.Forms.TextBox
    Private WithEvents _ShortCompensationTextBox As Windows.Forms.TextBox
    Private WithEvents _OpenCompensationTextBox As Windows.Forms.TextBox
    Private WithEvents _ApplyLoadButton As Windows.Forms.Button
    Private WithEvents _ApplyShortButton As Windows.Forms.Button
    Private WithEvents _ApplyOpenButton As Windows.Forms.Button
End Class
