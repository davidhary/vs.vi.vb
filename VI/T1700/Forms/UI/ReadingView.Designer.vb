﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReadingView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._MeasureSettingsLabel = New System.Windows.Forms.Label()
        Me._MeasureButton = New System.Windows.Forms.Button()
        Me._PostReadingDelayNumericLabel = New System.Windows.Forms.Label()
        Me._PostReadingDelayNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ReadContinuouslyCheckBox = New System.Windows.Forms.CheckBox()
        Me._CommandComboBox = New System.Windows.Forms.ComboBox()
        Me._WriteButton = New System.Windows.Forms.Button()
        Me._ReadButton = New System.Windows.Forms.Button()
        Me._ReadSRQButton = New System.Windows.Forms.Button()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        CType(Me._PostReadingDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Panel.SuspendLayout()
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_MeasureSettingsLabel
        '
        Me._MeasureSettingsLabel.Location = New System.Drawing.Point(97, 103)
        Me._MeasureSettingsLabel.Name = "_MeasureSettingsLabel"
        Me._MeasureSettingsLabel.Size = New System.Drawing.Size(199, 41)
        Me._MeasureSettingsLabel.TabIndex = 26
        Me._MeasureSettingsLabel.Text = "5 measurements, 150ms delays, 1% accuracy."
        '
        '_MeasureButton
        '
        Me._MeasureButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasureButton.Location = New System.Drawing.Point(4, 104)
        Me._MeasureButton.Name = "_MeasureButton"
        Me._MeasureButton.Size = New System.Drawing.Size(75, 30)
        Me._MeasureButton.TabIndex = 25
        Me._MeasureButton.Text = "&Measure"
        Me._MeasureButton.UseVisualStyleBackColor = True
        '
        '_PostReadingDelayNumericLabel
        '
        Me._PostReadingDelayNumericLabel.AutoSize = True
        Me._PostReadingDelayNumericLabel.Location = New System.Drawing.Point(172, 15)
        Me._PostReadingDelayNumericLabel.Name = "_PostReadingDelayNumericLabel"
        Me._PostReadingDelayNumericLabel.Size = New System.Drawing.Size(72, 17)
        Me._PostReadingDelayNumericLabel.TabIndex = 20
        Me._PostReadingDelayNumericLabel.Text = "Delay [ms]:"
        '
        '_PostReadingDelayNumeric
        '
        Me._PostReadingDelayNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PostReadingDelayNumeric.Location = New System.Drawing.Point(247, 11)
        Me._PostReadingDelayNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._PostReadingDelayNumeric.Name = "_PostReadingDelayNumeric"
        Me._PostReadingDelayNumeric.Size = New System.Drawing.Size(49, 25)
        Me._PostReadingDelayNumeric.TabIndex = 21
        '
        '_ReadContinuouslyCheckBox
        '
        Me._ReadContinuouslyCheckBox.AutoSize = True
        Me._ReadContinuouslyCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadContinuouslyCheckBox.Location = New System.Drawing.Point(68, 13)
        Me._ReadContinuouslyCheckBox.Name = "_ReadContinuouslyCheckBox"
        Me._ReadContinuouslyCheckBox.Size = New System.Drawing.Size(98, 21)
        Me._ReadContinuouslyCheckBox.TabIndex = 19
        Me._ReadContinuouslyCheckBox.Text = "Continuous"
        Me._ReadContinuouslyCheckBox.UseVisualStyleBackColor = True
        '
        '_CommandComboBox
        '
        Me._CommandComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CommandComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CommandComboBox.FormattingEnabled = True
        Me._CommandComboBox.Location = New System.Drawing.Point(68, 55)
        Me._CommandComboBox.Name = "_CommandComboBox"
        Me._CommandComboBox.Size = New System.Drawing.Size(63, 25)
        Me._CommandComboBox.TabIndex = 23
        Me._CommandComboBox.Text = "D111x"
        '
        '_WriteButton
        '
        Me._WriteButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._WriteButton.Location = New System.Drawing.Point(4, 52)
        Me._WriteButton.Name = "_WriteButton"
        Me._WriteButton.Size = New System.Drawing.Size(58, 30)
        Me._WriteButton.TabIndex = 22
        Me._WriteButton.Text = "&Write"
        Me._WriteButton.UseVisualStyleBackColor = True
        '
        '_ReadButton
        '
        Me._ReadButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReadButton.Location = New System.Drawing.Point(4, 8)
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(58, 30)
        Me._ReadButton.TabIndex = 18
        Me._ReadButton.Text = "&Read"
        Me._ReadButton.UseVisualStyleBackColor = True
        '
        '_ReadSRQButton
        '
        Me._ReadSRQButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ReadSRQButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReadSRQButton.Location = New System.Drawing.Point(137, 55)
        Me._ReadSRQButton.Name = "_ReadSRQButton"
        Me._ReadSRQButton.Size = New System.Drawing.Size(160, 30)
        Me._ReadSRQButton.TabIndex = 24
        Me._ReadSRQButton.Text = "Read &Service Request"
        Me._ReadSRQButton.UseVisualStyleBackColor = True
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._MeasureSettingsLabel)
        Me._Panel.Controls.Add(Me._MeasureButton)
        Me._Panel.Controls.Add(Me._PostReadingDelayNumericLabel)
        Me._Panel.Controls.Add(Me._PostReadingDelayNumeric)
        Me._Panel.Controls.Add(Me._ReadContinuouslyCheckBox)
        Me._Panel.Controls.Add(Me._CommandComboBox)
        Me._Panel.Controls.Add(Me._WriteButton)
        Me._Panel.Controls.Add(Me._ReadButton)
        Me._Panel.Controls.Add(Me._ReadSRQButton)
        Me._Panel.Location = New System.Drawing.Point(38, 85)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(307, 155)
        Me._Panel.TabIndex = 0
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(383, 326)
        Me._Layout.TabIndex = 1
        '
        'ReadingView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "ReadingView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(383, 326)
        CType(Me._PostReadingDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()

        Me._Layout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _MeasureSettingsLabel As Windows.Forms.Label
    Private WithEvents _MeasureButton As Windows.Forms.Button
    Private WithEvents _PostReadingDelayNumericLabel As Windows.Forms.Label
    Private WithEvents _PostReadingDelayNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ReadContinuouslyCheckBox As Windows.Forms.CheckBox
    Private WithEvents _CommandComboBox As Windows.Forms.ComboBox
    Private WithEvents _WriteButton As Windows.Forms.Button
    Private WithEvents _ReadButton As Windows.Forms.Button
    Private WithEvents _ReadSRQButton As Windows.Forms.Button
End Class
