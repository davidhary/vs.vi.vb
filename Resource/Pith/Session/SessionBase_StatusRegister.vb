'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase_ServiceRegister.vb
'
' summary:	Session base service register class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices

Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Core.TimeSpanExtensions

Partial Public Class SessionBase

#Region " SERVICE REQUEST REGISTER EVENTS: REPORT "

    ''' <summary> Returns a detailed report of the service request register (SRQ) byte. </summary>
    ''' <param name="value">     Specifies the value that was read from the service request register. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> The structured report. </returns>
    Public Shared Function BuildReport(ByVal value As VI.Pith.ServiceRequests, ByVal delimiter As String) As String

        If String.IsNullOrWhiteSpace(delimiter) Then
            delimiter = "; "
        End If

        Dim builder As New System.Text.StringBuilder

        For Each element As VI.Pith.ServiceRequests In [Enum].GetValues(GetType(VI.Pith.ServiceRequests))
            If (element And value) <> 0 AndAlso element <> VI.Pith.ServiceRequests.All Then
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(element.Description)
            End If
        Next
        Return builder.ToString

    End Function

#End Region

#Region " SUSPENSION "

    ''' <summary> Number of suspended service requested. </summary>
    Private _SuspendedServiceRequestedCount As Integer

    ''' <summary> Gets or sets the number of services requested while suspended. </summary>
    ''' <value> The number of services requested. </value>
    Public Property SuspendedServiceRequestedCount As Integer
        Get
            Return Me._SuspendedServiceRequestedCount
        End Get
        Set(value As Integer)
            If Me.SuspendedServiceRequestedCount <> value Then
                Me._SuspendedServiceRequestedCount = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the type of the service request. </summary>
    ''' <value> The type of the service request. </value>
    Public Property ServiceRequestType As String

    ''' <summary> Gets or sets the service request handling suspended. </summary>
    ''' <value> The service request handling suspended. </value>
    Public Property ServiceRequestHandlingSuspended As Boolean

    ''' <summary> Resume service request handing. </summary>
    Public Overridable Sub ResumeServiceRequestHanding()
        Me.LastAction = "Resuming service request handing"
        If Me.ServiceRequestEventEnabled Then
            Me.ClearLastError()
            Me.SuspendedServiceRequestedCount = 0
            Me.ServiceRequestType = String.Empty
            Me.DiscardServiceRequests()
        End If
        Me.ServiceRequestHandlingSuspended = False
    End Sub

    ''' <summary> Suspends the service request handling. </summary>
    Public Overridable Sub SuspendServiceRequestHanding()
        Me.LastAction = "Suspending service request handing"
        If Me.ServiceRequestEventEnabled Then
            Me.ClearLastError()
            Me.SuspendedServiceRequestedCount = 0
            Me.ServiceRequestType = String.Empty
            Me.DiscardServiceRequests()
        End If
        Me.ServiceRequestHandlingSuspended = True
    End Sub

#End Region

#Region " EVENT "

    ''' <summary> Notifies of the ServiceRequested event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnServiceRequested(ByVal e As System.EventArgs)
        Me.SyncNotifyServiceRequested(e)
    End Sub

    ''' <summary> Removes the ServiceRequested event handlers. </summary>
    Protected Sub RemoveServiceRequestedEventHandlers()
        Me._ServiceRequestedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The ServiceRequested event handlers. </summary>
    Private ReadOnly _ServiceRequestedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in ServiceRequested events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ServiceRequested As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._ServiceRequestedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._ServiceRequestedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._ServiceRequestedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event


    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="ServiceRequested">ServiceRequested Event</see>.
    ''' </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyServiceRequested(ByVal e As System.EventArgs)
        Me._ServiceRequestedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " SRQ ENABLE "

    ''' <summary> The service request enable command. </summary>
    Private _ServiceRequestEnableCommand As String

    ''' <summary> Gets or sets the service request enable command. </summary>
    ''' <value> The service request enable command. </value>
    Public Property ServiceRequestEnableCommand() As String
        Get
            Return Me._ServiceRequestEnableCommand
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(Me.ServiceRequestEnableCommand, value) Then
                Me._ServiceRequestEnableCommand = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the emulated status byte. </summary>
    ''' <value> The emulated status byte. </value>
    Public Property EmulatedStatusByte As VI.Pith.ServiceRequests

    ''' <summary> Makes emulated status byte. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReply(ByVal value As VI.Pith.ServiceRequests)
        Me.EmulatedStatusByte = value
    End Sub

    ''' <summary> Makes emulated status byte if none. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReplyIfEmpty(ByVal value As VI.Pith.ServiceRequests)
        If Me.EmulatedStatusByte = VI.Pith.ServiceRequests.None Then
            Me.MakeEmulatedReply(value)
        End If
    End Sub

    ''' <summary> Queries if a service request is enabled. </summary>
    ''' <param name="bitmask"> The bit mask. </param>
    ''' <returns> <c>true</c> if a service request is enabled; otherwise <c>false</c> </returns>
    Public Function IsServiceRequestEnabled(ByVal bitmask As VI.Pith.ServiceRequests) As Boolean
        Return Me.ServiceRequestEventEnabled AndAlso (Me.ServiceRequestEnabledBitmask.GetValueOrDefault(ServiceRequests.None) And bitmask) <> 0
    End Function

    ''' <summary> The standard event enabled bitmask. </summary>
    Private _ServiceRequestEnabledBitmask As VI.Pith.ServiceRequests?

    ''' <summary> Gets or sets the cached Standard Event enabled bit mask. </summary>
    ''' <value>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Standard
    ''' Events</see>.
    ''' </value>
    Public Property ServiceRequestEnabledBitmask() As VI.Pith.ServiceRequests?
        Get
            Return Me._ServiceRequestEnabledBitmask
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests?)
            If Not Me.ServiceRequestEnabledBitmask.Equals(value) Then
                Me._ServiceRequestEnabledBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The service request enable query command. </summary>
    Private _ServiceRequestEnableQueryCommand As String = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand

    ''' <summary> Gets or sets the service request enable query command. </summary>
    ''' <remarks>
    ''' SCPI: "*SRE?".
    ''' <see cref="VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand"> </see>
    ''' </remarks>
    ''' <value> The service request enable command format. </value>
    Public Property ServiceRequestEnableQueryCommand As String
        Get
            Return Me._ServiceRequestEnableQueryCommand
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ServiceRequestEnableQueryCommand) Then
                Me._ServiceRequestEnableQueryCommand = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The service request enable command format. </summary>
    Private _ServiceRequestEnableCommandFormat As String

    ''' <summary> Gets or sets the service request enable command format. </summary>
    ''' <remarks>
    ''' SCPI: "*SRE {0:D}".
    ''' <see cref="VI.Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat"> </see>
    ''' </remarks>
    ''' <value> The service request enable command format. </value>
    Public Property ServiceRequestEnableCommandFormat As String
        Get
            Return Me._ServiceRequestEnableCommandFormat
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ServiceRequestEnableCommandFormat) Then
                Me._ServiceRequestEnableCommandFormat = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries the service request enable bit mask. </summary>
    ''' <returns>
    ''' <c>null</c> if value is not known; otherwise <see cref="Integer">Service Requests</see>.
    ''' </returns>
    Public Function QueryServiceRequestEnableBitmask() As Integer
        Dim value As Integer = Me.Query(0I, Me.ServiceRequestEnableQueryCommand)
        Me.ServiceRequestEnabledBitmask = CType(value, Pith.ServiceRequests)
        Return value
    End Function

    ''' <summary> Gets the supports service request enable query. </summary>
    ''' <value> The supports service request enable query. </value>
    Public ReadOnly Property SupportsServiceRequestEnableQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ServiceRequestEnableQueryCommand)
        End Get
    End Property


    ''' <summary>
    ''' Apply (Write and then read) bitmask setting the device to turn on the Requesting Service
    ''' (RQS) / Master Summary Status (MSS) bit and issue an SRQ upon any of the SCPI events unmasked
    ''' by the bitmask. Uses *SRE to select (unmask) the events that will issue an SRQ.
    ''' </summary>
    ''' <param name="bitmask"> The bitmask; zero to disable all events. </param>
    Public Sub ApplyServiceRequestEnableBitmask(ByVal bitmask As VI.Pith.ServiceRequests)
        Me.WriteServiceRequestEnableBitmask(bitmask)
        Me.QueryServiceRequestEnableBitmask()
    End Sub


    ''' <summary>
    ''' Apply (Write and then read) bitmask setting the device to turn on the Requesting Service
    ''' (RQS) / Master Summary Status (MSS) bit and issue an SRQ upon any of the SCPI events unmasked
    ''' by the bitmask. Uses *SRE to select (unmask) the events that will issue an SRQ.
    ''' </summary>
    ''' <param name="commandFormat"> The service request enable command format. </param>
    ''' <param name="bitmask">       The bitmask; zero to disable all events. </param>
    Public Sub ApplyServiceRequestEnableBitmask(ByVal commandFormat As String, ByVal bitmask As VI.Pith.ServiceRequests)
        Me.ServiceRequestEnableCommandFormat = commandFormat
        Me.ApplyServiceRequestEnableBitmask(bitmask)
    End Sub

    ''' <summary> Writes a service request enable bitmask. </summary>
    ''' <param name="bitmask"> The bitmask; zero to disable all events. </param>
    Public Sub WriteServiceRequestEnableBitmask(ByVal bitmask As VI.Pith.ServiceRequests)
        If String.IsNullOrWhiteSpace(Me.ServiceRequestEnableCommandFormat) Then
            Me.ServiceRequestEnabledBitmask = VI.Pith.ServiceRequests.None
            Me.ServiceRequestEnableCommand = String.Empty
        Else
            Me.WriteLine(Me.ServiceRequestEnableCommandFormat, CInt(bitmask))
            Me.ServiceRequestEnabledBitmask = bitmask
            Me.ServiceRequestEnableCommand = String.Format(Me.ServiceRequestEnableCommandFormat, CInt(bitmask))
        End If
    End Sub

    ''' <summary> Returns <c>True</c> is this instrument supports service request enable. </summary>
    ''' <value> The supports service request enable. </value>
    Public ReadOnly Property SupportsServiceRequestEnable As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ServiceRequestEnableCommandFormat)
        End Get
    End Property

    ''' <summary> Emulate service request. </summary>
    ''' <param name="statusByte"> The status byte. </param>
    Public Sub EmulateServiceRequest(ByVal statusByte As VI.Pith.ServiceRequests)
        Me.EmulatedStatusByte = statusByte
        If Me.ServiceRequestEventEnabled Then Me.OnServiceRequested(EventArgs.Empty)
    End Sub


    ''' <summary>
    ''' Gets or sets or set the sentinel indication if a service request event handler was enabled
    ''' and registered.
    ''' </summary>
    ''' <value>
    ''' <c>True</c> if service request event is enabled and registered; otherwise, <c>False</c>.
    ''' </value>
    Public MustOverride Property ServiceRequestEventEnabled As Boolean

    ''' <summary> Discard pending service requests. </summary>
    Public MustOverride Sub DiscardServiceRequests()

    ''' <summary> Enables and adds the service request event handler. </summary>
    Public MustOverride Sub EnableServiceRequestEventHandler()

    ''' <summary> Disables and removes the service request event handler. </summary>
    Public MustOverride Sub DisableServiceRequestEventHandler()

#End Region

#Region " STANDARD SERVICE REQUEST ENABLE "

    ''' <summary> The default requesting service bitmask. </summary>
    Private _DefaultRequestingServiceBitmask As ServiceRequests = ServiceRequests.StandardEvent

    ''' <summary> Gets or sets the default requesting service bitmask. </summary>
    ''' <value> The default requesting service bitmask. </value>
    Public Property DefaultRequestingServiceBitmask As ServiceRequests
        Get
            Return Me._DefaultRequestingServiceBitmask
        End Get
        Set(value As ServiceRequests)
            If value <> Me.DefaultRequestingServiceBitmask Then
                Me._DefaultRequestingServiceBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The default operation complete bitmask. </summary>
    Private _DefaultOperationCompleteBitmask As ServiceRequests

    ''' <summary> Gets or sets the default operation complete bitmask. </summary>
    ''' <value> The default operation complete bitmask. </value>
    Public Property DefaultOperationCompleteBitmask As ServiceRequests
        Get
            Return Me._DefaultOperationCompleteBitmask
        End Get
        Set(value As ServiceRequests)
            If value <> Me.DefaultOperationCompleteBitmask Then
                Me._DefaultOperationCompleteBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The default operation service request enable bitmask. </summary>
    Private _DefaultOperationServiceRequestEnableBitmask As ServiceRequests

    ''' <summary> Gets or sets the default Operation service request enable bitmask. </summary>
    ''' <remarks>
    ''' Exclude message available to prevent handling a service request on messages.
    ''' </remarks>
    ''' <value> The default Operation service request enable bitmask. </value>
    Public Property DefaultOperationServiceRequestEnableBitmask As ServiceRequests
        Get
            Return Me._DefaultOperationServiceRequestEnableBitmask
        End Get
        Set(value As ServiceRequests)
            If value <> Me.DefaultOperationServiceRequestEnableBitmask Then
                Me._DefaultOperationServiceRequestEnableBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The default service request enable bitmask. </summary>
    Private _DefaultServiceRequestEnableBitmask As ServiceRequests

    ''' <summary> Gets or sets the default service request enable bitmask. </summary>
    ''' <value> The default service request enable bitmask. </value>
    Public Property DefaultServiceRequestEnableBitmask As ServiceRequests
        Get
            Return Me._DefaultServiceRequestEnableBitmask
        End Get
        Set(value As ServiceRequests)
            If value <> Me.DefaultServiceRequestEnableBitmask Then
                Me._DefaultServiceRequestEnableBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The standard service enable command. </summary>
    Private _StandardServiceEnableCommand As String

    ''' <summary> Gets or sets the standard service enable command. </summary>
    ''' <remarks>
    ''' This command combines the commands for <see cref="ClearActiveState()"/>,
    ''' <see cref="ServiceRequestEnableCommandFormat"/> and
    ''' <see cref="StandardEventEnableCommandFormat"/>. <para>
    ''' SCPI: *CLS; *ESE {0:D}; *SRE {1:D}</para>
    ''' </remarks>
    ''' <value> The standard service enable command. </value>
    Public Property StandardServiceEnableCommand As String
        Get
            Return Me._StandardServiceEnableCommand
        End Get
        Protected Set(value As String)
            If Not String.Equals(value, Me.ServiceRequestEnableCommand) Then
                Me._StandardServiceEnableCommand = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the standard service enable command format. </summary>
    ''' <remarks>
    ''' <see cref="VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat"></see>
    ''' This command combines the commands for <see cref="ClearActiveState()"/>,
    ''' <see cref="ServiceRequestEnableCommandFormat"/> and
    ''' <see cref="StandardEventEnableCommandFormat"/>. <para>
    ''' SCPI: *CLS; *ESE {0:D}; *SRE {1:D}</para>
    ''' </remarks>
    ''' <value> The standard service enable command format. </value>
    Public Property StandardServiceEnableCommandFormat As String = VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat


    ''' <summary>
    ''' Applies (Writes and then reads) the standard and service bitmasks to unmask events to let the
    ''' device set the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and
    ''' Master Summary Status (MSS) upon any of the unmasked SCPI events. Uses *ESE to select
    ''' (unmask) the events that set the ESB and *SRE to select (unmask) the events that will set the
    ''' Master Summary Status (MSS) bit for requesting service. Does not issue *OPC; Does not define
    ''' the default wait complete bitmasks.
    ''' </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    '''                                            SRQ. </param>
    Public Sub ApplyStandardServiceRequestEnableBitmasks(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.ReadStatusRegister()
        If String.IsNullOrWhiteSpace(Me.StandardServiceEnableCommandFormat) Then
            Me.StandardServiceEnableCommand = String.Empty
        Else
            Dim command As String = Me.WriteLine(Me.StandardServiceEnableCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
            Me.StandardServiceEnableCommand = command
            Me.QueryServiceRequestEnableBitmask()
            Me.QueryStandardEventEnableBitmask()
        End If
    End Sub


    ''' <summary>
    ''' Writes the standard and service bitmasks to unmask events to let the device set the Event
    ''' Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
    ''' (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
    ''' the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
    ''' bit for requesting service. Does not issue *OPC; Does not define the default wait complete
    ''' bitmasks.
    ''' </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    '''                                            SRQ. </param>
    Public Sub WriteStandardServiceRequestEnableBitmasks(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.ReadStatusRegister()
        If String.IsNullOrWhiteSpace(Me.StandardServiceEnableCommandFormat) Then
            Me.StandardServiceEnableCommand = String.Empty
        Else
            Dim command As String = Me.WriteLine(Me.StandardServiceEnableCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
            Me.StandardServiceEnableCommand = command
            Me.ServiceRequestEnabledBitmask = serviceRequestEnableBitmask
            Me.StandardEventEnableBitmask = standardEventEnableBitmask
        End If
    End Sub


    ''' <summary>
    ''' Applies (Writes and then reads) the standard and service bitmasks to unmask events to let the
    ''' device set the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and
    ''' Master Summary Status (MSS) upon any of the unmasked SCPI events. Uses *ESE to select
    ''' (unmask) the events that set the ESB and *SRE to select (unmask) the events that will set the
    ''' Master Summary Status (MSS) bit for requesting service. Does not issue *OPC; Does not define
    ''' the default wait complete bitmasks.
    ''' </summary>
    ''' <remarks> 3475. Add Or VI.Pith.Ieee4882.ServiceRequests.OperationEvent. </remarks>
    Public Sub ApplyStandardServiceRequestEnableBitmasks()
        Me.ApplyStandardServiceRequestEnableBitmasks(Me.DefaultStandardEventEnableBitmask, Me.DefaultOperationCompleteBitmask)
    End Sub


    ''' <summary>
    ''' Gets or sets the standard service operation complete enable command format.
    ''' </summary>
    ''' <remarks>
    ''' <see cref="VI.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat"> </see>
    ''' This command combines the commands for <see cref="ClearActiveState()"/>,
    ''' <see cref="ServiceRequestEnableCommandFormat"/>,
    ''' <see cref="StandardEventEnableCommandFormat"/>, and <see cref="OperationCompleteCommand"/>.
    ''' <para>
    ''' SCPI: *CLS; *ESE {0:D}; *SRE {1:D}; *OPC</para>
    ''' </remarks>
    ''' <value> The standard service enable operation complete enable command format. </value>
    Public Property StandardServiceEnableCompleteCommandFormat As String = VI.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat


    ''' <summary>
    ''' Applies (Writes and then reads) the standard and service bitmasks to unmask events to let the
    ''' device set the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and
    ''' Master Summary Status (MSS) upon any of the unmasked SCPI events. Uses *ESE to select
    ''' (unmask) the events that set the ESB and *SRE to select (unmask) the events that will set the
    ''' Master Summary Status (MSS) bit for requesting service. Also issues *OPC and defines the
    ''' default wait complete bitmasks.
    ''' </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    '''                                            SRQ. </param>
    Public Sub ApplyStandardServiceCompleteEnableBitmasks(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.ReadStatusRegister()
        If String.IsNullOrWhiteSpace(Me.StandardServiceEnableCompleteCommandFormat) Then
            Me.ServiceRequestEnabledBitmask = VI.Pith.ServiceRequests.None
            Me.StandardEventEnableBitmask = VI.Pith.StandardEvents.None
        Else
            Me.WriteLine(Me.StandardServiceEnableCompleteCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
            If Me.SupportsServiceRequestEnableQuery Then
                Me.QueryServiceRequestEnableBitmask()
            Else
                Me.ServiceRequestEnabledBitmask = serviceRequestEnableBitmask
            End If
            If Me.ServiceRequestEnabledBitmask.GetValueOrDefault(VI.Pith.ServiceRequests.None) <> VI.Pith.ServiceRequests.None Then
                Me.ServiceRequestWaitCompleteEnabledBitmask = Me.ServiceRequestEnabledBitmask.GetValueOrDefault(VI.Pith.ServiceRequests.None)
            End If
            If Me.SupportsStandardEventEnableQuery Then
                Me.QueryStandardEventEnableBitmask()
            Else
                Me.StandardEventEnableBitmask = standardEventEnableBitmask
            End If
            If Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None) <> VI.Pith.StandardEvents.None Then
                Me.StandardEventWaitCompleteEnabledBitmask = Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None)
            End If
        End If
    End Sub


    ''' <summary>
    ''' Writes the standard and service bitmasks to unmask events to let the device set the Event
    ''' Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
    ''' (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
    ''' the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
    ''' bit for requesting service. Also issues *OPC and defines the default wait complete bitmasks.
    ''' </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    '''                                            SRQ. </param>
    Public Sub WriteStandardServiceCompleteEnableBitmasks(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.ReadStatusRegister()
        If String.IsNullOrWhiteSpace(Me.StandardServiceEnableCompleteCommandFormat) Then
            Me.ServiceRequestEnabledBitmask = VI.Pith.ServiceRequests.None
            Me.StandardEventEnableBitmask = VI.Pith.StandardEvents.None
        Else
            Me.WriteLine(Me.StandardServiceEnableCompleteCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
            Me.ServiceRequestEnabledBitmask = serviceRequestEnableBitmask
            If Me.ServiceRequestEnabledBitmask.GetValueOrDefault(VI.Pith.ServiceRequests.None) <> VI.Pith.ServiceRequests.None Then
                Me.ServiceRequestWaitCompleteEnabledBitmask = Me.ServiceRequestEnabledBitmask.GetValueOrDefault(VI.Pith.ServiceRequests.None)
            End If
            Me.StandardEventEnableBitmask = standardEventEnableBitmask
            If Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None) <> VI.Pith.StandardEvents.None Then
                Me.StandardEventWaitCompleteEnabledBitmask = Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None)
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the operation complete enable command format. </summary>
    ''' <remarks>
    ''' <see cref="VI.Pith.Ieee488.Syntax.OperationCompleteEnableCommandFormat"> </see>
    ''' This command combines the commands for <see cref="ClearActiveState()"/>,
    ''' <see cref="StandardEventEnableCommandFormat"/>, and <see cref="OperationCompleteCommand"/>.
    ''' <para>
    ''' SCPI: *CLS; *ESE {0:D}; *OPC</para>
    ''' </remarks>
    ''' <value> The operation complete enable complete format. </value>
    Public Property OperationCompleteEnableCommandFormat As String = VI.Pith.Ieee488.Syntax.OperationCompleteEnableCommandFormat


    ''' <summary>
    ''' Applies (Writes and then reads) the standard bitmask to unmask events to let the device set
    ''' the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> upon any of the
    ''' SCPI events. Uses *ESE to select (unmask) the events that set the ESB. Also issues *OPC and
    ''' defines the default wait complete bitmasks.
    ''' </summary>
    ''' <param name="standardEventEnableBitmask"> Specifies standard events will issue an SRQ. </param>
    Public Sub ApplyOperationCompleteEnableBitmask(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents)
        Me.ReadStatusRegister()
        If String.IsNullOrWhiteSpace(Me.OperationCompleteEnableCommandFormat) Then
            Me.ServiceRequestEnabledBitmask = VI.Pith.ServiceRequests.None
            Me.StandardEventEnableBitmask = VI.Pith.StandardEvents.None
        Else
            Me.WriteLine(Me.OperationCompleteEnableCommandFormat, CInt(standardEventEnableBitmask))
            If Me.SupportsStandardEventEnableQuery Then
                Me.QueryStandardEventEnableBitmask()
            Else
                Me.StandardEventEnableBitmask = standardEventEnableBitmask
            End If
            If Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None) <> VI.Pith.StandardEvents.None Then
                Me.StandardEventWaitCompleteEnabledBitmask = Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None)
            End If
        End If
    End Sub


    ''' <summary>
    ''' Writes the standard bitmask to unmask events to let the device set the Event Summary bit
    ''' (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> upon any of the SCPI events. Uses *ESE
    ''' to select (unmask) the events that set the ESB. Also issues *OPC and defines the default wait
    ''' complete bitmasks.
    ''' </summary>
    ''' <param name="standardEventEnableBitmask"> Specifies standard events will issue an SRQ. </param>
    Public Sub WriteOperationCompleteEnableBitmask(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents)
        Me.ReadStatusRegister()
        If String.IsNullOrWhiteSpace(Me.OperationCompleteEnableCommandFormat) Then
            Me.ServiceRequestEnabledBitmask = VI.Pith.ServiceRequests.None
            Me.StandardEventEnableBitmask = VI.Pith.StandardEvents.None
        Else
            Me.WriteLine(Me.OperationCompleteEnableCommandFormat, CInt(standardEventEnableBitmask))
            Me.StandardEventEnableBitmask = standardEventEnableBitmask
            If Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None) <> VI.Pith.StandardEvents.None Then
                Me.StandardEventWaitCompleteEnabledBitmask = Me.StandardEventEnableBitmask.GetValueOrDefault(VI.Pith.StandardEvents.None)
            End If
        End If
    End Sub

#End Region

#Region " SERVICE REQUEST STATUS "

    ''' <summary> Bitmask value changed. </summary>
    ''' <param name="bitmask">  The bit mask. </param>
    ''' <param name="oldValue"> The old value. </param>
    ''' <param name="newValue"> The value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Shared Function BitmaskValueChanged(ByVal bitmask As Integer, ByVal oldValue As Integer, ByVal newValue As Integer) As Boolean
        Return (bitmask And oldValue) <> (bitmask And newValue)
    End Function

    ''' <summary> Applies the service request described by value. </summary>
    ''' <param name="value"> Specifies the value that was read from the service request register. </param>
    ''' <returns> The VI.Pith.ServiceRequests. </returns>
    Public Function ApplyServiceRequest(ByVal value As VI.Pith.ServiceRequests) As VI.Pith.ServiceRequests
        Return Me.ApplyServiceRequest(Me.ServiceRequestStatus, value)
    End Function

    ''' <summary> Applies the service request described by value. </summary>
    ''' <param name="oldValue"> The old value. </param>
    ''' <param name="newValue"> The value. </param>
    ''' <returns> The VI.Pith.ServiceRequests. </returns>
    Protected Function ApplyServiceRequest(ByVal oldValue As VI.Pith.ServiceRequests, ByVal newValue As VI.Pith.ServiceRequests) As VI.Pith.ServiceRequests

        ' save the new status value
        Me._ServiceRequestStatus = newValue

        ' error notification does not cause fetching device error is a message is available.
        If SessionBase.BitmaskValueChanged(Me.ErrorAvailableBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.ErrorAvailable))
        If SessionBase.BitmaskValueChanged(Me.MessageAvailableBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.MessageAvailable))
        If SessionBase.BitmaskValueChanged(Me.MeasurementEventBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.HasMeasurementEvent))
        If SessionBase.BitmaskValueChanged(Me.SystemEventBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.HasSystemEvent))
        If SessionBase.BitmaskValueChanged(Me.QuestionableEventBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.HasQuestionableEvent))
        If SessionBase.BitmaskValueChanged(Me.StandardEventBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.HasStandardEvent))
        If SessionBase.BitmaskValueChanged(Me.RequestingServiceBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.RequestingService))
        If SessionBase.BitmaskValueChanged(Me.OperationEventBit, oldValue, newValue) Then Me.NotifyPropertyChanged(NameOf(SessionBase.HasOperationEvent))

        ' Notify is status changed
        If Not newValue.Equals(oldValue) Then Me.SyncNotifyPropertyChanged(NameOf(Me.ServiceRequestStatus))

        ' ignore device error in the presence of a message in order to prevent query interrupted errors.
        If Not Me.MessageAvailable AndAlso Me.ErrorAvailable Then Me.SyncNotifyDeviceErrorOccurred(System.EventArgs.Empty)

        ' update the status caption
        Me.StatusRegisterCaption = String.Format(SessionBase.RegisterValueFormat, CInt(newValue))

        Return newValue
    End Function

    ''' <summary> Gets or sets the cached service request Status. </summary>
    ''' <remarks>
    ''' The service request status is posted to be parsed by the status subsystem that is specific to
    ''' the instrument at hand. This is critical to the proper workings of the status subsystem. The
    ''' service request status is posted asynchronously. This may not be processed fast enough to
    ''' determine the next action. Not sure how to address this at this time.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Service
    ''' Requests</see>.
    ''' </value>
    Public ReadOnly Property ServiceRequestStatus() As VI.Pith.ServiceRequests

    ''' <summary> Delay read status register. </summary>
    ''' <returns> The VI.Pith.ServiceRequests. </returns>
    Public Function DelayReadStatusRegister() As VI.Pith.ServiceRequests
        Return Me.ReadStatusRegister(Me.StatusReadDelay)
    End Function

    ''' <summary> Reads the status register. </summary>
    ''' <param name="readDelay"> The read delay. </param>
    ''' <returns>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Service
    ''' Requests</see>.
    ''' </returns>
    Public Function ReadStatusRegister(ByVal readDelay As TimeSpan) As VI.Pith.ServiceRequests
        isr.Core.ApplianceBase.DoEventsWait(readDelay)
        Return Me.ApplyServiceRequest(Me.ReadStatusByte())
    End Function

    ''' <summary> Reads the status register. </summary>
    ''' <returns>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Service
    ''' Requests</see>.
    ''' </returns>
    Public Function ReadStatusRegister() As VI.Pith.ServiceRequests
        Return Me.ApplyServiceRequest(Me.ReadStatusByte())
    End Function

    ''' <summary> The unknown register value caption. </summary>
    ''' <value> The unknown register value caption. </value>
    Protected Shared Property UnknownRegisterValueCaption As String = "0x.."

    ''' <summary> The register value format. </summary>
    ''' <value> The register value format. </value>
    Protected Shared Property RegisterValueFormat As String = "0x{0:X2}"

    ''' <summary> The status register caption. </summary>
    Private _StatusRegisterCaption As String = "0x.."

    ''' <summary> Gets or sets the status register caption. </summary>
    ''' <value> The status register caption. </value>
    Public Property StatusRegisterCaption As String
        Get
            Return Me._StatusRegisterCaption
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.StatusRegisterCaption) Then
                Me._StatusRegisterCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SRQ WAIT FOR "

    ''' <summary> The default status read turnaround time. </summary>
    Private _DefaultStatusReadTurnaroundTime As TimeSpan = TimeSpan.FromMilliseconds(10)

    ''' <summary> Gets or sets the default status read turnaround time. </summary>
    ''' <value> The default status read turnaround time. </value>
    Public Property DefaultStatusReadTurnaroundTime As TimeSpan
        Get
            Return Me._DefaultStatusReadTurnaroundTime
        End Get
        Set(value As TimeSpan)
            If Not TimeSpan.Equals(value, Me.DefaultStatusReadTurnaroundTime) Then
                Me._DefaultStatusReadTurnaroundTime = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The status read turnaround time. </summary>
    Private _StatusReadTurnaroundTime As TimeSpan

    ''' <summary> Gets or sets the status read turnaround time. </summary>
    ''' <value> The status read turnaround time. </value>
    Public Property StatusReadTurnaroundTime As TimeSpan
        Get
            Return Me._StatusReadTurnaroundTime
        End Get
        Set(value As TimeSpan)
            If Not TimeSpan.Equals(value, Me.StatusReadTurnaroundTime) Then
                Me._StatusReadTurnaroundTime = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the effective status read turnaround time. </summary>
    ''' <value> The effective status read turnaround time. </value>
    Public ReadOnly Property EffectiveStatusReadTurnaroundTime As TimeSpan
        Get
            Return If(Me.StatusReadTurnaroundTime > TimeSpan.Zero, Me.StatusReadTurnaroundTime,
                            If(Me.DefaultStatusReadTurnaroundTime > TimeSpan.Zero, Me.DefaultStatusReadTurnaroundTime, TimeSpan.FromMilliseconds(10)))
        End Get
    End Property

    ''' <summary> Await service request. </summary>
    ''' <remarks>
    ''' Delay defaults to <see cref="StatusReadDelay"/> and poll delay default to
    ''' <see cref="EffectiveStatusReadTurnaroundTime"/>
    ''' </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
    Public Overridable Function AwaitServiceRequest(ByVal timeout As TimeSpan) As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests)
        Return Me.AwaitStatusBitmask(Me.DefaultRequestingServiceBitmask, timeout)
    End Function

    ''' <summary> Await status register bitmask. </summary>
    ''' <remarks>
    ''' Delay defaults to <see cref="StatusReadDelay"/> and poll delay default to
    ''' <see cref="EffectiveStatusReadTurnaroundTime"/>
    ''' </remarks>
    ''' <param name="bitmask"> Specifies the anticipated status bitmask. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
    Public Overridable Function AwaitStatusBitmask(ByVal bitmask As ServiceRequests, ByVal timeout As TimeSpan) As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests)
        ' emulate the reply for disconnected operations.
        Me.MakeEmulatedReplyIfEmpty(bitmask)
        Dim cts As New Threading.CancellationTokenSource
        Dim t As Task(Of (TimedOut As Boolean, Status As ServiceRequests)) = Pith.SessionBase.StartAwaitingBitmaskTask(bitmask, timeout, Me.StatusReadDelay, Me.EffectiveStatusReadTurnaroundTime, Function() Me.ReadStatusByte)
        t.Wait()
        Return t.Result
    End Function

    ''' <summary> Await status register bitmask. </summary>
    ''' <param name="bitmask">   Specifies the anticipated status bitmask. </param>
    ''' <param name="timeout">   The timeout. </param>
    ''' <param name="delay">     The delay. </param>
    ''' <param name="pollDelay"> The poll delay. </param>
    ''' <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
    Public Function AwaitStatusBitmask(ByVal bitmask As ServiceRequests, ByVal timeout As TimeSpan,
                                       ByVal delay As TimeSpan, ByVal pollDelay As TimeSpan) As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests)
        ' emulate the reply for disconnected operations.
        Me.MakeEmulatedReplyIfEmpty(bitmask)
        Dim cts As New Threading.CancellationTokenSource
        Dim t As Task(Of (TimedOut As Boolean, Status As ServiceRequests)) = Pith.SessionBase.StartAwaitingBitmaskTask(bitmask, timeout, delay, pollDelay, Function() Me.ReadStatusByte)
        t.Wait()
        Return t.Result
    End Function

#End Region

#Region " TASKS STARTING METHODS "


    ''' <summary>
    ''' Starts a task waiting for a bitmask. The task complete after a timeout or if the action
    ''' function value matches the bitmask value.
    ''' </summary>
    ''' <remarks>
    ''' The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
    ''' deadlocks attempting to get the task result. For more information see
    ''' https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
    ''' on examples for how to resolve this issue.
    ''' </remarks>
    ''' <param name="bitmask"> Specifies the anticipated bitmask. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="action">  The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of Integer) </returns>
    Public Shared Function StartAwaitingBitmaskTask(ByVal bitmask As Integer, ByVal timeout As TimeSpan,
                                                    ByVal action As Func(Of Integer)) As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer))
        Return Threading.Tasks.Task.Factory.StartNew(Of (Boolean, Integer))(Function()
                                                                                Dim ticks As Long = timeout.Ticks
                                                                                Dim sw As Stopwatch = Stopwatch.StartNew()
                                                                                Dim reading As Integer = action.Invoke
                                                                                If timeout <= TimeSpan.Zero Then Return (False, reading)
                                                                                Dim timedOut As Boolean = sw.ElapsedTicks >= ticks
                                                                                Do Until ((bitmask And reading) = bitmask) OrElse timedOut
                                                                                    Threading.Thread.SpinWait(1)
                                                                                    isr.Core.ApplianceBase.DoEvents()
                                                                                    reading = action.Invoke
                                                                                    timedOut = sw.ElapsedTicks >= ticks
                                                                                Loop
                                                                                Return (timedOut, reading)
                                                                            End Function)
    End Function


    ''' <summary>
    ''' Starts a task waiting for a bitmask. The task complete after a timeout or if the action
    ''' function value matches the bitmask value.
    ''' </summary>
    ''' <remarks>
    ''' The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
    ''' deadlocks attempting to get the task result. For more information see
    ''' https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
    ''' on examples for how to resolve this issue.
    ''' </remarks>
    ''' <param name="bitmask"> Specifies the anticipated bitmask. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="action">  The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of Integer) </returns>
    Public Shared Function StartAwaitingBitmaskTask(ByVal bitmask As Integer, ByVal timeout As TimeSpan,
                                                    ByVal action As Func(Of Integer?)) As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer?))
        Return Threading.Tasks.Task.Factory.StartNew(Of (Boolean, Integer?))(Function()
                                                                                 Dim ticks As Long = timeout.Ticks
                                                                                 Dim sw As Stopwatch = Stopwatch.StartNew()
                                                                                 Dim reading As Integer? = action.Invoke
                                                                                 If timeout <= TimeSpan.Zero Then Return (False, reading)
                                                                                 Dim timedOut As Boolean = sw.ElapsedTicks >= ticks
                                                                                 Do Until ((bitmask And reading) = bitmask) OrElse timedOut
                                                                                     Threading.Thread.SpinWait(1)
                                                                                     isr.Core.ApplianceBase.DoEvents()
                                                                                     reading = action.Invoke
                                                                                     timedOut = sw.ElapsedTicks >= ticks
                                                                                 Loop
                                                                                 Return (timedOut, reading)
                                                                             End Function)
    End Function


    ''' <summary>
    ''' Starts a task waiting for a bitmask. The task complete after a timeout or if the action
    ''' function value matches the bitmask value.
    ''' </summary>
    ''' <remarks>
    ''' The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
    ''' deadlocks attempting to get the task result. For more information see
    ''' https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
    ''' on examples for how to resolve this issue.
    ''' </remarks>
    ''' <param name="bitmask"> Specifies the anticipated bitmask. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="action">  The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of Integer) </returns>
    Public Shared Function StartAwaitingBitmaskTask(ByVal bitmask As ServiceRequests, ByVal timeout As TimeSpan,
                                                    ByVal action As Func(Of ServiceRequests)) As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As VI.Pith.ServiceRequests))
        Return Threading.Tasks.Task.Factory.StartNew(Of (Boolean, ServiceRequests))(Function()
                                                                                        Dim ticks As Long = timeout.Ticks
                                                                                        Dim sw As Stopwatch = Stopwatch.StartNew()
                                                                                        Dim reading As ServiceRequests = action.Invoke
                                                                                        If timeout <= TimeSpan.Zero Then Return (False, reading)
                                                                                        Dim timedOut As Boolean = sw.ElapsedTicks >= ticks
                                                                                        Do Until ((bitmask And reading) = bitmask) OrElse timedOut
                                                                                            Threading.Thread.SpinWait(1)
                                                                                            isr.Core.ApplianceBase.DoEvents()
                                                                                            reading = action.Invoke
                                                                                            timedOut = sw.ElapsedTicks >= ticks
                                                                                        Loop
                                                                                        Return (timedOut, reading)
                                                                                    End Function)
    End Function


    ''' <summary>
    ''' Starts a task waiting for a bitmask. The task complete after a timeout or if the action
    ''' function value matches the bitmask value.
    ''' </summary>
    ''' <remarks>
    ''' The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
    ''' deadlocks attempting to get the task result. For more information see
    ''' https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
    ''' on examples for how to resolve this issue.
    ''' </remarks>
    ''' <param name="bitmask">   Specifies the anticipated bitmask. </param>
    ''' <param name="timeout">   The timeout. </param>
    ''' <param name="delay">     The delay. </param>
    ''' <param name="pollDelay"> Specifies time between serial polls. </param>
    ''' <param name="action">    The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of Integer) </returns>
    Public Shared Function StartAwaitingBitmaskTask(ByVal bitmask As Integer, ByVal timeout As TimeSpan,
                                                    ByVal delay As TimeSpan, ByVal pollDelay As TimeSpan,
                                                    ByVal action As Func(Of Integer?)) As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer?))
        Return Threading.Tasks.Task.Factory.StartNew(Of (Boolean, Integer?))(Function()
                                                                                 delay.SpinWait
                                                                                 Dim ticks As Long = timeout.Ticks
                                                                                 Dim sw As Stopwatch = Stopwatch.StartNew()
                                                                                 Dim reading As Integer? = action.Invoke
                                                                                 If timeout <= TimeSpan.Zero Then Return (False, reading)
                                                                                 Dim timedOut As Boolean = sw.ElapsedTicks >= ticks
                                                                                 Do Until ((bitmask And reading) = bitmask) OrElse timedOut
                                                                                     Threading.Thread.SpinWait(1)
                                                                                     ApplianceBase.DoEvents()
                                                                                     pollDelay.SpinWait()
                                                                                     reading = action.Invoke
                                                                                     timedOut = sw.ElapsedTicks >= ticks
                                                                                 Loop
                                                                                 Return (timedOut, reading)
                                                                             End Function)
    End Function


    ''' <summary>
    ''' Starts a task waiting for a bitmask. The task complete after a timeout or if the action
    ''' function value matches the bitmask value.
    ''' </summary>
    ''' <remarks>
    ''' The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
    ''' deadlocks attempting to get the task result. For more information see
    ''' https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
    ''' on examples for how to resolve this issue.
    ''' </remarks>
    ''' <param name="bitmask">   Specifies the anticipated bitmask. </param>
    ''' <param name="timeout">   The timeout. </param>
    ''' <param name="delay">     The delay. </param>
    ''' <param name="pollDelay"> Specifies time between serial polls. </param>
    ''' <param name="action">    The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of Integer) </returns>
    Public Shared Function StartAwaitingBitmaskTask(ByVal bitmask As Integer, ByVal timeout As TimeSpan,
                                                    ByVal delay As TimeSpan, ByVal pollDelay As TimeSpan,
                                                    ByVal action As Func(Of Integer)) As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer))
        Return Threading.Tasks.Task.Factory.StartNew(Of (Boolean, Integer))(Function()
                                                                                delay.SpinWait()
                                                                                Dim ticks As Long = timeout.Ticks
                                                                                Dim sw As Stopwatch = Stopwatch.StartNew()
                                                                                Dim reading As Integer = action.Invoke
                                                                                If timeout <= TimeSpan.Zero Then Return (False, reading)
                                                                                Dim timedOut As Boolean = sw.ElapsedTicks >= ticks
                                                                                Do Until ((bitmask And reading) = bitmask) OrElse timedOut
                                                                                    Threading.Thread.SpinWait(1)
                                                                                    ApplianceBase.DoEvents()
                                                                                    pollDelay.SpinWait
                                                                                    reading = action.Invoke
                                                                                    timedOut = sw.ElapsedTicks >= ticks
                                                                                Loop
                                                                                Return (timedOut, reading)
                                                                            End Function)
    End Function


    ''' <summary>
    ''' Starts a task waiting for a bitmask. The task complete after a timeout or if the action
    ''' function value matches the bitmask value.
    ''' </summary>
    ''' <remarks>
    ''' The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
    ''' deadlocks attempting to get the task result. For more information see
    ''' https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
    ''' on examples for how to resolve this issue.
    ''' </remarks>
    ''' <param name="bitmask">   Specifies the anticipated bitmask. </param>
    ''' <param name="timeout">   The timeout. </param>
    ''' <param name="delay">     The delay. </param>
    ''' <param name="pollDelay"> Specifies time between serial polls. </param>
    ''' <param name="action">    The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of Integer) </returns>
    Public Shared Function StartAwaitingBitmaskTask(ByVal bitmask As VI.Pith.ServiceRequests, ByVal timeout As TimeSpan,
                                                    ByVal delay As TimeSpan, ByVal pollDelay As TimeSpan,
                                                    ByVal action As Func(Of ServiceRequests)) As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As ServiceRequests))
        Return Threading.Tasks.Task.Factory.StartNew(Of (Boolean, ServiceRequests))(Function()
                                                                                        delay.SpinWait()
                                                                                        Dim ticks As Long = timeout.Ticks
                                                                                        Dim sw As Stopwatch = Stopwatch.StartNew()
                                                                                        Dim reading As ServiceRequests = action.Invoke
                                                                                        If timeout <= TimeSpan.Zero Then Return (False, reading)
                                                                                        Dim timedOut As Boolean = sw.ElapsedTicks >= ticks
                                                                                        Do Until ((bitmask And reading) = bitmask) OrElse timedOut
                                                                                            Threading.Thread.SpinWait(1)
                                                                                            isr.Core.ApplianceBase.DoEvents()
                                                                                            pollDelay.SpinWait()
                                                                                            reading = action.Invoke
                                                                                            timedOut = sw.ElapsedTicks >= ticks
                                                                                        Loop
                                                                                        Return (timedOut, reading)
                                                                                    End Function)
    End Function

#End Region

#Region " QUERY WITH STATUS READ "


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
    ''' after the specified delay. A final read is performed after a delay provided no error occurred.
    ''' </summary>
    ''' <param name="statusReadDelay"> The read delay. </param>
    ''' <param name="readDelay">       The read delay. </param>
    ''' <param name="dataToWrite">     The data to write. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal statusReadDelay As TimeSpan, ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Dim result As String = String.Empty
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.SyncWriteLine(dataToWrite)
            Threading.Thread.SpinWait(10)
            isr.Core.ApplianceBase.DoEventsWait(statusReadDelay)
            Me.ReadStatusRegister()
            If Not Me.ErrorAvailable Then
                Threading.Thread.SpinWait(10)
                isr.Core.ApplianceBase.DoEventsWait(readDelay)
                result = Me.ReadLine
            End If
        End If
        Return result

    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
    ''' after the specified delay. A final read is performed after a delay provided no error occurred.
    ''' </summary>
    ''' <param name="statusReadDelay"> The read delay. </param>
    ''' <param name="readDelay">       The read delay. </param>
    ''' <param name="dataToWrite">     The data to write. </param>
    ''' <returns> The trim end. </returns>
    Public Function QueryTrimEnd(ByVal statusReadDelay As TimeSpan, ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Query(statusReadDelay, readDelay, dataToWrite).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read
    ''' after the specified delay.
    ''' </summary>
    ''' <param name="readDelay">   The read delay. </param>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Dim result As String = String.Empty
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.SyncWriteLine(dataToWrite)
            Threading.Thread.SpinWait(10)
            isr.Core.ApplianceBase.DoEventsWait(readDelay)
            result = Me.ReadLineTrimEnd
        End If
        Return result
    End Function


#End Region

#Region " REGISTERS "

    ''' <summary> Sets the subsystem registers to their reset state. </summary>
    Private Sub ResetRegistersKnownState()
        Me.StandardEventStatus = New Pith.StandardEvents?
    End Sub

    ''' <summary> Reads the event registers based on the service request register status. </summary>
    Public Sub ReadStandardEventRegisters()
        If Me.HasStandardEvent Then Me.QueryStandardEventStatus()
    End Sub

#End Region

End Class

