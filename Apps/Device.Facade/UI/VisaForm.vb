'---------------------------------------------------------------------------------------------------
' file:		.\UI\VisaForm.vb
'
' summary:	Visa Windows Form
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

''' <summary> Form for viewing the visa. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class VisaForm
    Inherits isr.VI.Facade.VisaViewForm

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        Me.New(CommandLineInfo.VirtualInstrument.GetValueOrDefault(VirtualInstrument.ScpiSession))
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="instrumentOption"> The instrument option. </param>
    Public Sub New(ByVal instrumentOption As VirtualInstrument)
        MyBase.New()
        If instrumentOption = VirtualInstrument.Switchboard Then instrumentOption = VirtualInstrument.ScpiSession
        Select Case instrumentOption

            Case VirtualInstrument.Ats600
                Dim device As New VI.Ats600.Ats600Device With {
                    .CandidateResourceTitle = My.MySettings.Default.ATS600ResourceTitle,
                     .CandidateResourceName = My.MySettings.Default.ATS600ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Ats600.Forms.Ats600View(device)

            Case VirtualInstrument.EG2000
                Dim device As New VI.EG2000.EG2000Device With {
                    .CandidateResourceTitle = My.MySettings.Default.EG2000ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.EG2000ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.EG2000.Forms.EG2000View(device)

            Case VirtualInstrument.K2400
                Dim device As New VI.K2400.K2400Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K2400ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2400ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.K2400.Forms.K2400View(device)

            Case VirtualInstrument.K2450
                Dim device As New VI.Tsp2.K2450.K2450Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K2450ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2450ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Tsp2.K2450.Forms.K2450View(device)

            Case VirtualInstrument.K2600
                Dim device As New VI.Tsp.K2600.K2600Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K2600ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2600ResourceName
                }
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Tsp.K2600.Forms.K2600View(device)

            Case VirtualInstrument.K2700
                Dim device As New VI.K2700.K2700Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K2700ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2700ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.K2700.Forms.K2700View(device)

            Case VirtualInstrument.K3700rs
                Dim device As New VI.Tsp.K3700.K3700Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K3700ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K3700ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Tsp.K3700.Forms.K3700View(device) With {.WithResistancesMetersView = True}

            Case VirtualInstrument.K3700
                Dim device As New VI.Tsp.K3700.K3700Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K3700ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K3700ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Tsp.K3700.Forms.K3700View(device) With {.WithResistancesMetersView = False}

            Case VirtualInstrument.K7500
                Dim device As New VI.Tsp2.K7500.K7500Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K7500ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K7500ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Tsp2.K7500.Forms.K7500View(device)

            Case VirtualInstrument.T1750
                Dim device As New VI.T1700.T1700Device With {
                    .CandidateResourceTitle = My.MySettings.Default.T1750ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.T1750ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.T1700.Forms.T1700View(device)

            Case VirtualInstrument.TspRig
                Dim device As New VI.Tsp.Rig.TspDevice With {
                    .CandidateResourceTitle = My.MySettings.Default.K2600ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2600ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaSessionBase = device
                Me.VisaView = New VI.Tsp.Rig.Forms.RigView(device)

            Case VirtualInstrument.ScpiSession

                Dim session As New VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.ScpiSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ScpiSessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaSessionBase = session
                Me.VisaView = New VI.Facade.VisaView(session)

            Case VirtualInstrument.TspSession

                Dim session As New VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.TspSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.TspSessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaSessionBase = session
                Me.VisaView = New VI.Facade.VisaView(session)

            Case VirtualInstrument.Tsp2Session

                Dim session As New VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.Tsp2SessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.Tsp2SessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaSessionBase = session
                Me.VisaView = New VI.Facade.VisaView(session)

            Case Else

                Dim session As New VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.ScpiSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ScpiSessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaSessionBase = session
                Me.VisaView = New VI.Facade.VisaView(session)

        End Select
        Me.VisaViewDisposeEnabled = True
        If Me.VisaViewEnabled Then
            Me.VisaView.ApplyTalkerTraceLevel(Core.ListenerType.Display, TraceEventType.Verbose)
            Me.VisaView.ApplyTalkerTraceLevel(Core.ListenerType.Logger, TraceEventType.Verbose)
        End If
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.VisaView IsNot Nothing Then Me.VisaView.Dispose() : Me.VisaView = Nothing
        ' the session needs to be disposed after the device releases the session.
        If Me.VisaSessionBase IsNot Nothing Then Me.VisaSessionBase.Dispose() : Me.VisaSessionBase = Nothing
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        If Not Me.VisaViewEnabled Then Return
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim inst As VirtualInstrument = CommandLineInfo.VirtualInstrument.GetValueOrDefault(VirtualInstrument.ScpiSession)
            If inst = VirtualInstrument.Switchboard Then inst = VirtualInstrument.ScpiSession
            Select Case inst

                Case VirtualInstrument.Ats600
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ATS600ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ATS600ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.EG2000
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.EG2000ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.EG2000ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2002
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2002ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2002ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2400
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2400ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2400ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2450
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2450ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2450ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2600
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                    End If
                    If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                    End If
                Case VirtualInstrument.K2700
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2700ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2700ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K3700rs
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K3700
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K7000
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K7000ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K7000ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K7500
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K7500S
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.T1750
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.T1750ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.T1750ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.TspRig
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case VirtualInstrument.ScpiSession
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case VirtualInstrument.TspSession
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.TspSessionResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.TspSessionResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case VirtualInstrument.Tsp2Session
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.Tsp2SessionResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.Tsp2SessionResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case Else
                    If Me.VisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceName = Me.VisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceTitle = Me.VisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
            End Select

        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Gets the visa view enabled. </summary>
    ''' <value> The visa view enabled. </value>
    Private ReadOnly Property VisaViewEnabled As Boolean
        Get
            Return Me.VisaView IsNot Nothing
        End Get
    End Property

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        If Me.VisaViewEnabled Then
            Me.AddListener(My.Application.Logger)
        End If
        MyBase.OnLoad(e)
    End Sub

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
