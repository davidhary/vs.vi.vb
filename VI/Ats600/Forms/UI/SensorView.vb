'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\SensorView.vb
'
' summary:	Sensor view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Sensor view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SensorView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SensorView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="SensorView"/>. </returns>
    Public Shared Function Create() As SensorView
        Dim view As SensorView = Nothing
        Try
            view = New SensorView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As Ats600Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As Ats600Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As Ats600Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
            Me.DisplaySensorTypes()
        End If
        Me.BindThermostreamSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As Ats600Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " THERMO STREAM "

    ''' <summary> Gets or sets the Thermostream subsystem. </summary>
    ''' <value> The Thermostream subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ThermostreamSubsystem As VI.Ats600.ThermostreamSubsystem

    ''' <summary> Bind Thermostream subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindThermostreamSubsystem(ByVal device As Ats600.Ats600Device)
        If Me.ThermostreamSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ThermostreamSubsystem)
            Me._ThermostreamSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._ThermostreamSubsystem = device.ThermostreamSubsystem
            Me.BindSubsystem(True, Me.ThermostreamSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ThermostreamSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ThermostreamSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(Ats600.ThermostreamSubsystem.DeviceSensorType))
            Me.HandlePropertyChanged(subsystem, NameOf(Ats600.ThermostreamSubsystem.DeviceThermalConstant))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ThermostreamSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ThermostreamSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Ats600.ThermostreamSubsystem.DeviceSensorType)
                Me._DeviceSensorTypeSelector.ComboBox.SelectValue(subsystem.DeviceSensorType.GetValueOrDefault(0))
            Case NameOf(VI.Ats600.ThermostreamSubsystem.DeviceThermalConstant)
                Me._DeviceThermalConstantSelector.ValueSetter(CType(subsystem.DeviceThermalConstant, Decimal?))
        End Select
    End Sub

    ''' <summary> Sense Current subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ThermostreamSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ThermostreamSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ThermostreamSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ThermostreamSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SENSOR "

    ''' <summary> Type of the device sensor. </summary>
    Private _DeviceSensorType As isr.Core.EnumExtender(Of DeviceSensorType)

    ''' <summary> Displays a sensor types. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub DisplaySensorTypes()
        Me._DeviceSensorType = New isr.Core.EnumExtender(Of DeviceSensorType)
        Me._DeviceSensorTypeSelector.ComboBox.ListValues(Of VI.DeviceSensorType)(Me._DeviceSensorType.ValueDescriptionPairs)
    End Sub

    ''' <summary> Device sensor type selector value selected. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeviceSensorTypeSelector_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _DeviceSensorTypeSelector.ValueSelected
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} setting and reading the device sensor type"
            If Me._DeviceSensorType IsNot Nothing Then

            End If
            If Me._DeviceSensorType IsNot Nothing AndAlso sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Dim v As DeviceSensorType = Me._DeviceSensorTypeSelector.ComboBox.SelectedValue(Of VI.DeviceSensorType)(Me._DeviceSensorType.ValueEnumValuePairs)
                Me.Device.ThermostreamSubsystem.ApplyDeviceSensorType(v)
                Me.Device.ThermostreamSubsystem.QueryDeviceSensorType()
                If Me.Device.ThermostreamSubsystem.DeviceSensorType.GetValueOrDefault(DeviceSensorType.None) = DeviceSensorType.None Then
                    Me.Device.ThermostreamSubsystem.ApplyDeviceControl(False)
                Else
                    Me.Device.ThermostreamSubsystem.ApplyDeviceControl(True)
                End If
                Me.Device.ThermostreamSubsystem.QueryDeviceControl()
                Me.Device.ThermostreamSubsystem.QueryDeviceThermalConstant()
                Me.ReadStatusRegister()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
