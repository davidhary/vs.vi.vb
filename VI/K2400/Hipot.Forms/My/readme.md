**High Potential Meter (HIPOT) Tester**

Installation Information

This program installs the ISR HIPOT Tester application.

This software measures resistance under high potential using the
Keithley 2410 source measure system.

**Table of Contents**


*1.  Supported Instruments

```{=html}
<!-- -->
```
*1.  Supported Interfaces

*2.  First Time Installation

*3.  Installed Files

*4.  Online Resources

*5.  How to Uninstall

*6.  Contact Information

*7.  Attributions

**1. Supported Instruments**

Keithley 2410

**2. Supported Interfaces**

GPIB (IEEE488.2)

**3. First Time Installation**

The first time the program is installed on a computer, you may have to
install the Microsoft .NET framework and the NI VISA drivers. The
installer is capable of detecting the existing .Net version and display
an alert if an update is required.

**4. Installed Files**

The installation program copies files to the destination directory
chosen during installation. The default root folder is under the
*Integrated.Scientific.Resources* folder of the program files folder.
The program configuration files are located in the programs file folder.
When configuration changes are made, these are stored under the program
data folders. The public program data folder In Windows XP is
*C:\\Documents and Settings\\All Users\\Application
Data\\Integrated.Scientific.Resources*. In Windows versions following XP
this folder is located under
*Users\\Public\\AppData\\Local\\Integrated.Scientific.Resources* or
under the *Program Data* folder. The private folders are similarly
located under the specific user name instead of under to *All Users* or
*Public* folders listed above.

**5. On Line Resources**


Click on this [link](http://bit.ly/e0RDKb) to get the most
recent version of the TTM Console.

Click this
[link](http://www.microsoft.com/en-us/download/details.aspx?id=17851)
to get the Microsoft .NET Framework.

Click [.Net Info](http://msdn.microsoft.com/en-us/library/aa139615.aspx)
to get Information on the .NET framework deployment.

Click [NI VISA Runtime](http://ftp.ni.com/support/softlib/visa/VISA%20Run-Time%20Engine)
to download the National Instrument VISA runtime engine. Note that
NI-VISA is included in the NI-488.2 (GPIB) driver package.

**6. How to Uninstall**


Uninstall the program from the Add/Remove Programs applets of the
Control Panel.

**7. Contact Information**
support[at]IntegratedScientificResources[.]com
**9. Attributions**

\(c\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

Licensed under the [Fair End User Use License](http://www.isr.cc/licenses/FairEndUserUseLicense.pdf).

Unless required by applicable law or agreed to in writing, software
distributed under the ISR License is distributed on an \"AS IS\" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Open source used by this software is described and licensed on the
following sites:

Controls VB Library:  
[Flashing Led](https://www.codeproject.com/Articles/8393/Flash-LED-Control)  
[Extended Numeric Up Down Control](http://www.codeproject.com/KB/edit/NumericUpDownEx.aspx)

Core VB Library:  
[Easy String Compression and Encryption](http://www.codeproject.com/KB/string/string_compression.aspx)  
[Enumerations, Flags and C\#](http://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-my-bad-pun.aspx)  
[Generic Event Arguments](http://www.codeproject.com/KB/cs/GenericEventArgs.aspx)  
[Register](http://www.codeproject.com/KB/cs/Register.aspx)  
[String Extensions](http://www.codeproject.com/KB/string/stringconversion.aspx)  
[Strong-Type and Efficient Enumerations](http://www.codeproject.com/KB/cs/efficient_strong_enum.aspx)

Object List View Library:  
[Object List View](http://objectlistview.sourceforge.net)

Windows Forms VB Library:  
[Fade Form](http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx)

This software uses Closed software described and licensed on the
following sites:

VISA VB Library:  
[NI-VISA](http:\www.ni.com)
