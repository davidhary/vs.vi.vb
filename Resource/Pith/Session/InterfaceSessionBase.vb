''' <summary> An interface session base class. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-24 </para>
''' </remarks>
Public MustInherit Class InterfaceSessionBase
    Inherits isr.Core.Models.ViewModelBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "


    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the disposed status. </summary>
    ''' <value> The is disposed. </value>
    Protected ReadOnly Property IsDisposed As Boolean


    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' Overrides should Dispose(disposing As Boolean) has code to free unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

#End Region

#End Region

    ''' <summary> Gets or sets the default open timeout. </summary>
    ''' <value> The default open timeout. </value>
    Public Property DefaultOpenTimeout As TimeSpan = My.Settings.DefaultOpenSessionTimeout

    ''' <summary> Gets or sets name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public ReadOnly Property ResourceName As String

    ''' <summary> Gets or sets the sentinel indicating weather this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public MustOverride ReadOnly Property IsDummy As Boolean

    ''' <summary> True if is open, false if not. </summary>
    Private _IsOpen As Boolean

    ''' <summary> Gets or sets the is open. </summary>
    ''' <value> The is open. </value>
    Public Property IsOpen As Boolean
        Get
            Return Me._IsOpen
        End Get
        Protected Set(value As Boolean)
            If value <> Me.IsOpen Then
                Me._IsOpen = value
                Me.NotifyPropertyChanged(NameOf(InterfaceSessionBase.IsOpen))
                Me.NotifyPropertyChanged(NameOf(InterfaceSessionBase.ResourceName))
            End If
        End Set
    End Property

    ''' <summary> State of the resource open. </summary>
    Private _ResourceOpenState As ResourceOpenState

    ''' <summary> Gets or sets the state of the resource open. </summary>
    ''' <value> The resource open state. </value>
    Public Property ResourceOpenState As ResourceOpenState
        Get
            Return Me._ResourceOpenState
        End Get
        Protected Set(value As ResourceOpenState)
            Me._ResourceOpenState = value
            Me.IsOpen = value <> ResourceOpenState.Unknown
        End Set
    End Property

    ''' <summary> Closes the <see cref="InterfaceSessionBase">Interface Session</see>. </summary>
    Public Overridable Sub CloseSession()
        Me.IsOpen = False
    End Sub

    ''' <summary> Opens a <see cref="InterfaceSessionBase">Interface Session</see>. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="timeout">      The timeout. </param>
    Public Overridable Sub OpenSession(ByVal resourceName As String, ByVal timeout As TimeSpan)
        Me._ResourceName = resourceName
        Me.IsOpen = True
    End Sub

    ''' <summary> Opens a <see cref="InterfaceSessionBase">Interface Session</see>. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Overridable Sub OpenSession(ByVal resourceName As String)
        Me._ResourceName = resourceName
        Me.IsOpen = True
    End Sub

    ''' <summary> Sends the interface clear. </summary>
    Public MustOverride Sub SendInterfaceClear()

    ''' <summary> Returns all instruments to some default state. </summary>
    Public MustOverride Sub ClearDevices()

    ''' <summary> Clears the specified device. </summary>
    ''' <param name="gpibAddress"> The instrument address. </param>
    Public MustOverride Sub SelectiveDeviceClear(ByVal gpibAddress As Integer)

    ''' <summary> Clears the specified device. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public MustOverride Sub SelectiveDeviceClear(ByVal resourceName As String)

    ''' <summary> Clears the interface. </summary>
    Public MustOverride Sub ClearInterface()

    ''' <summary> Gets or sets the type of the hardware interface. </summary>
    ''' <value> The type of the hardware interface. </value>
    Public MustOverride ReadOnly Property HardwareInterfaceType As VI.Pith.HardwareInterfaceType

    ''' <summary> Gets or sets the hardware interface number. </summary>
    ''' <value> The hardware interface number. </value>
    Public MustOverride ReadOnly Property HardwareInterfaceNumber As Integer

End Class

