﻿'---------------------------------------------------------------------------------------------------
' file:		.\My\MyLibrary_Internals.vb
'
' summary:	My library internals class
'---------------------------------------------------------------------------------------------------
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.E4990Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K2002Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K2002FormsTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K3700Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K3700FormsTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K2450FormsTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K3458Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K7500Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K7500FormsTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.OhmniTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.CincoTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.MorpheTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>

