'---------------------------------------------------------------------------------------------------
' file:		.VI\Subsystems\StatusSubsystemBase_Registers.vb
'
' summary:	Status subsystem base registers class
'---------------------------------------------------------------------------------------------------
Partial Public MustInherit Class StatusSubsystemBase

#Region " STATUS REGISTER EVENTS: REQUESTING SERVICE "

    ''' <summary>
    ''' Gets or sets bit that would be set if a requested service or master summary status event has
    ''' occurred.
    ''' </summary>
    ''' <value> The requested service or master summary status bit. </value>
    Public Overridable Property RequestingServiceBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.RequestingServiceBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.RequestingServiceBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.RequestingServiceBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating if the device has requested service. </summary>
    ''' <value> <c>True</c> if the device has requested service; otherwise, <c>False</c>. </value>
    Public ReadOnly Property RequestingService As Boolean
        Get
            Return Me.Session.RequestingService
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: ERROR "

    ''' <summary> Gets or sets the bit that would be set if an error event has occurred. </summary>
    ''' <value> The error event bit. </value>
    Public Overridable Property ErrorAvailableBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.ErrorAvailableBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.ErrorAvailableBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.ErrorAvailableBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether [Error available]. </summary>
    ''' <value> <c>True</c> if [Error available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property ErrorAvailable As Boolean
        Get
            Return Me.Session.ErrorAvailable
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: MESSAGE "

    ''' <summary> Gets or sets the bit that would be set if a message is available. </summary>
    ''' <value> The Message available bit. </value>
    Public Overridable Property MessageAvailableBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.MessageAvailableBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.MessageAvailableBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.MessageAvailableBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether [Message available]. </summary>
    ''' <value> <c>True</c> if [Message available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property MessageAvailable As Boolean
        Get
            Return Me.Session.MessageAvailable
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: MEASUREMENT "

    ''' <summary>
    ''' Gets or sets the bit that would be set when an enabled measurement event has occurred.
    ''' </summary>
    ''' <value> The Measurement event bit value. </value>
    Public Overridable Property MeasurementEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.MeasurementEventBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.MeasurementEventBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.MeasurementEventBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an enabled measurement event has occurred. </summary>
    ''' <value>
    ''' <c>True</c> if an enabled measurement event has occurred; otherwise, <c>False</c>.
    ''' </value>
    Public ReadOnly Property HasMeasurementEvent As Boolean
        Get
            Return Me.Session.HasMeasurementEvent
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: SYSTEM "

    ''' <summary> Gets or sets the bit that would be set if an System event has occurred. </summary>
    ''' <value> The System event bit. </value>
    Public Overridable Property SystemEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.SystemEventBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.SystemEventBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.SystemEventBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an System event has occurred. </summary>
    ''' <value> <c>True</c> if an System event has occurred; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasSystemEvent As Boolean
        Get
            Return Me.Session.HasSystemEvent
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: OPERATION "

    ''' <summary> Gets or sets the bit that would be set if an operation event has occurred. </summary>
    ''' <value> The Operation event bit. </value>
    Public Overridable Property OperationEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.OperationEventBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.OperationEventBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.OperationEventBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an operation event has occurred. </summary>
    ''' <value> <c>True</c> if an operation event has occurred; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasOperationEvent As Boolean
        Get
            Return Me.Session.HasOperationEvent
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: QUESTIONABLE "

    ''' <summary>
    ''' Gets or sets the bit that would be set if an Questionable event has occurred.
    ''' </summary>
    ''' <value> The Questionable event bit. </value>
    Public Overridable Property QuestionableEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.QuestionableEventBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.QuestionableEventBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.QuestionableEventBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an Questionable event has occurred. </summary>
    ''' <value> <c>True</c> if an Questionable event has occurred; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasQuestionableEvent As Boolean
        Get
            Return Me.Session.HasQuestionableEvent
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: STANDARD EVENT "

    ''' <summary>
    ''' Gets or sets bit that would be set if an enabled standard event has occurred.
    ''' </summary>
    ''' <value> The Standard Event bit. </value>
    Public Overridable Property StandardEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me.Session.StandardEventBit
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.StandardEventBit Then
                ' the session sends the notification and notify the status subsystem via the session property change event.
                Me.Session.StandardEventBit = value
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an enabled standard event has occurred. </summary>
    ''' <value>
    ''' <c>True</c> if an enabled standard event has occurred; otherwise, <c>False</c>.
    ''' </value>
    Public ReadOnly Property HasStandardEvent As Boolean
        Get
            Return Me.Session.HasStandardEvent
        End Get
    End Property

#End Region

End Class



