''' <summary>
''' Parses the stack trace as provided by <see cref="Environment.StackTrace">the
''' environment</see>.
''' </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-07-10, 1.2.3478. Created
''' </para>
''' </remarks>
Public Class StackTraceParser

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="stackTrace"> Normally formatted Stack Trace. </param>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    Public Sub New(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer)
        MyBase.New()
        Me._UserCallStack = New System.Text.StringBuilder(&HFF)
        Me._CallStack = New System.Text.StringBuilder(&HFF)
        If StackTraceParser._FrameworkLocations Is Nothing Then
            StackTraceParser._FrameworkLocations = New String() {"at System", "at Microsoft"}
        End If
        Me.ParseThis(stackTrace, skipLines, totalLines)
    End Sub

    ''' <summary>
    ''' Constructs this class using the <see cref="Environment.StackTrace">environment stack
    ''' trace</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    Public Sub New(ByVal skipLines As Integer, ByVal totalLines As Integer)
        Me.New(Environment.StackTrace, skipLines, totalLines)
    End Sub

    ''' <summary> Stack of calls. </summary>
    Private _CallStack As System.Text.StringBuilder

    ''' <summary> Gets a normally formatted Call Stack. </summary>
    ''' <value> A stack of calls. </value>
    Public ReadOnly Property CallStack() As String
        Get
            Return Me._CallStack.ToString.TrimEnd(Environment.NewLine.ToCharArray)
        End Get
    End Property

    ''' <summary> The framework locations. </summary>
    Private Shared _FrameworkLocations As String()

    ''' <summary> Returns true if the line is a framework line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> True if framework line, false if not. </returns>
    Public Shared Function IsFrameworkLine(ByVal value As String) As Boolean
        If Not String.IsNullOrWhiteSpace(value) Then
            For Each locationName As String In StackTraceParser._FrameworkLocations
                If value.TrimStart.StartsWith(locationName, StringComparison.OrdinalIgnoreCase) Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function

    ''' <summary> Parses the stack trace into the user and full call stacks. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="stackTrace"> Normally formatted Stack Trace. </param>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    Public Sub Parse(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer)
        Me.ParseThis(stackTrace, skipLines, totalLines)
    End Sub

    ''' <summary>
    ''' Parses the <see cref="Environment.StackTrace">environment stack trace</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    Public Sub Parse(ByVal skipLines As Integer, ByVal totalLines As Integer)
        Me.ParseThis(skipLines, totalLines)
    End Sub

    ''' <summary>
    ''' Parses the <see cref="Environment.StackTrace">environment stack trace</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    Private Sub ParseThis(ByVal skipLines As Integer, ByVal totalLines As Integer)
        Me.ParseThis(Environment.StackTrace, skipLines, totalLines)
    End Sub

    ''' <summary> Parses the stack trace into the user and full call stacks. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="stackTrace"> Normally formatted Stack Trace. </param>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    Private Sub ParseThis(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer)
        Me._UserCallStack = New System.Text.StringBuilder(&HFF)
        Me._CallStack = New System.Text.StringBuilder(&HFF)
        If String.IsNullOrWhiteSpace(stackTrace) Then
            Return
        End If
        Dim callStack As String() = stackTrace.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        If callStack.Length <= 0 Then
            Return
        End If
        Dim isFrameworkBlock As Boolean = False
        Dim lineNumber As Integer = 0
        Dim lineCount As Integer = 0
        For Each line As String In callStack
            If totalLines = 0 OrElse lineCount <= totalLines Then
                lineNumber += 1
                If lineNumber > skipLines Then
                    If StackTraceParser.IsFrameworkLine(line) Then
                        isFrameworkBlock = True
                        Me._CallStack.AppendLine(line)
                    Else
                        If isFrameworkBlock Then
                            ' if previously had external code, append
                            Me._UserCallStack.AppendLine("   at [External Code]")
                            isFrameworkBlock = False
                        End If
                        Me._CallStack.AppendLine(line)
                        Me._UserCallStack.AppendLine(line)
                        lineCount += 1
                    End If
                End If
            End If
        Next
    End Sub

    ''' <summary> Stack of user calls. </summary>
    Private _UserCallStack As System.Text.StringBuilder

    ''' <summary> Gets the user call stack, which includes no .NET Framework functions. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A String. </returns>
    Public Function UserCallStack() As String
        Return Me._UserCallStack.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

    ''' <summary> Parses and returns the user call stack. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="stackTrace"> Normally formatted Stack Trace. </param>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    ''' <returns> A String. </returns>
    Public Shared Function UserCallStack(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
        Dim parser As New StackTraceParser(stackTrace, skipLines, totalLines)
        Return parser.UserCallStack
    End Function

    ''' <summary>
    ''' Parses and returns the user call stack using the
    ''' <see cref="Environment.StackTrace">environment stack trace</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
    '''                           environment methods. </param>
    ''' <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
    '''                           to include all lines. </param>
    ''' <returns> A String. </returns>
    Public Shared Function UserCallStack(ByVal skipLines As Integer, ByVal totalLines As Integer) As String
        Dim parser As New StackTraceParser(skipLines, totalLines)
        Return parser.UserCallStack
    End Function

End Class
