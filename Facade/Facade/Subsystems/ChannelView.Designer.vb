<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ChannelView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChannelView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._CloseChannelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OpenChannelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OpenAllMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ChannelNumberNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ChannelNumberNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._CloseChannelButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenChannelButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenChannelsButton = New System.Windows.Forms.ToolStripButton()
        Me._ClosedChannelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._ChannelNumberNumericLabel, Me._ChannelNumberNumeric, Me._CloseChannelButton, Me._OpenChannelButton, Me._OpenChannelsButton, Me._ClosedChannelLabel})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(382, 26)
        Me._SubsystemToolStrip.TabIndex = 0
        Me._SubsystemToolStrip.Text = "ChannelToolStrip"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._CloseChannelMenuItem, Me._OpenChannelMenuItem, Me._OpenAllMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(68, 23)
        Me._SubsystemSplitButton.Text = "Channel"
        '
        '_CloseChannelMenuItem
        '
        Me._CloseChannelMenuItem.Name = "_CloseChannelMenuItem"
        Me._CloseChannelMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._CloseChannelMenuItem.Text = "Close Channel"
        Me._CloseChannelMenuItem.ToolTipText = "Closes the specified channel"
        '
        '_OpenChannelMenuItem
        '
        Me._OpenChannelMenuItem.Name = "_OpenChannelMenuItem"
        Me._OpenChannelMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._OpenChannelMenuItem.Text = "Open Channel"
        Me._OpenChannelMenuItem.ToolTipText = "Opens the selected channel"
        '
        '_OpenAllMenuItem
        '
        Me._OpenAllMenuItem.Name = "_OpenAllMenuItem"
        Me._OpenAllMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._OpenAllMenuItem.Text = "Open All"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument"
        '
        '_ChannelNumberNumericLabel
        '
        Me._ChannelNumberNumericLabel.Name = "_ChannelNumberNumericLabel"
        Me._ChannelNumberNumericLabel.Size = New System.Drawing.Size(14, 23)
        Me._ChannelNumberNumericLabel.Text = "#"
        '
        '_ChannelNumberNumeric
        '
        Me._ChannelNumberNumeric.Name = "_ChannelNumberNumeric"
        Me._ChannelNumberNumeric.Size = New System.Drawing.Size(41, 23)
        Me._ChannelNumberNumeric.Text = "0"
        Me._ChannelNumberNumeric.ToolTipText = "Channel Number"
        Me._ChannelNumberNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_CloseChannelButton
        '
        Me._CloseChannelButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._CloseChannelButton.Image = CType(resources.GetObject("_CloseChannelButton.Image"), System.Drawing.Image)
        Me._CloseChannelButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._CloseChannelButton.Name = "_CloseChannelButton"
        Me._CloseChannelButton.Size = New System.Drawing.Size(40, 23)
        Me._CloseChannelButton.Text = "Close"
        Me._CloseChannelButton.ToolTipText = "Closes the selected channel"
        '
        '_OpenChannelButton
        '
        Me._OpenChannelButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OpenChannelButton.Image = CType(resources.GetObject("_OpenChannelButton.Image"), System.Drawing.Image)
        Me._OpenChannelButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenChannelButton.Name = "_OpenChannelButton"
        Me._OpenChannelButton.Size = New System.Drawing.Size(40, 23)
        Me._OpenChannelButton.Text = "Open"
        Me._OpenChannelButton.ToolTipText = "Opens the selected channel"
        '
        '_OpenChannelsButton
        '
        Me._OpenChannelsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OpenChannelsButton.Image = CType(resources.GetObject("_OpenChannelsButton.Image"), System.Drawing.Image)
        Me._OpenChannelsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenChannelsButton.Name = "_OpenChannelsButton"
        Me._OpenChannelsButton.Size = New System.Drawing.Size(57, 23)
        Me._OpenChannelsButton.Text = "Open All"
        Me._OpenChannelsButton.ToolTipText = "Opens all channels"
        '
        '_ClosedChannelLabel
        '
        Me._ClosedChannelLabel.Name = "_ClosedChannelLabel"
        Me._ClosedChannelLabel.Size = New System.Drawing.Size(54, 23)
        Me._ClosedChannelLabel.Text = "Closed: ?"
        '
        'ChannelView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "ChannelView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(384, 26)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ChannelNumberNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ChannelNumberNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _ClosedChannelLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _CloseChannelButton As Windows.Forms.ToolStripButton
    Private WithEvents _OpenChannelButton As Windows.Forms.ToolStripButton
    Private WithEvents _OpenChannelsButton As Windows.Forms.ToolStripButton
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _CloseChannelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _OpenChannelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _OpenAllMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
End Class
