''' <summary> Defines the contract that must be implemented by an Output Subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-12-12, 3.0.5093. </para>
''' </remarks>
Public MustInherit Class AccessSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="OutputSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.CertifyTimeout = TimeSpan.FromMilliseconds(20000)
        Me.Certified = New Boolean?
    End Sub

#End Region

#Region " CERTIFY "

    ''' <summary> The certify timeout. </summary>
    Private _CertifyTimeout As TimeSpan

    ''' <summary> Gets or sets the timeout time span allowed for certifying the device. </summary>
    ''' <value> The certify timeout. </value>
    Public Property CertifyTimeout As TimeSpan
        Get
            Return Me._CertifyTimeout
        End Get
        Set(ByVal value As TimeSpan)
            If Not Me.CertifyTimeout.Equals(value) Then
                Me._CertifyTimeout = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> State of the output on. </summary>
    Private _Certified As Boolean?

    ''' <summary> Gets or sets the cached certification sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if not known; <c>True</c> if certified on; otherwise, <c>False</c>.
    ''' </value>
    Public Property Certified As Boolean?
        Get
            Return Me._Certified
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Certified, value) Then
                Me._Certified = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Certifies. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' <c>null</c> if not known; <c>True</c> if certified on; otherwise, <c>False</c>.
    ''' </returns>
    Public MustOverride Function Certify(ByVal value As String) As Boolean?

    ''' <summary> Tries to certify. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if certified on; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryCertify(ByVal value As String) As Boolean
        Try
            Return Me.Certify(value).GetValueOrDefault(False)
        Catch ex As Exception
            Me.PublishException("certifying access", ex)
            Return False
        End Try
    End Function

#End Region

#Region " RESOURCES "

    ''' <summary> The certified instruments. </summary>
    Private _CertifiedInstruments As String

    ''' <summary> Gets or sets the list of certified instruments. </summary>
    ''' <value> The certified instruments. </value>
    Public Property CertifiedInstruments() As String
        Get
            Return Me._CertifiedInstruments
        End Get
        Protected Set(ByVal value As String)
            Me._CertifiedInstruments = value
            Me.NotifyPropertyChanged()
        End Set

    End Property

    ''' <summary> The released instruments. </summary>
    Private _ReleasedInstruments As String

    ''' <summary> Gets or sets the list of released instruments. </summary>
    ''' <value> The released instruments. </value>
    Public Property ReleasedInstruments() As String
        Get
            Return Me._ReleasedInstruments
        End Get
        Set(ByVal value As String)
            Me._ReleasedInstruments = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The salt. </summary>
    Private _Salt As String

    ''' <summary> Gets or sets the list of released instruments. </summary>
    ''' <value> The released instruments. </value>
    Public Property Salt() As String
        Get
            Return Me._Salt
        End Get
        Set(ByVal value As String)
            Me._Salt = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

#End Region

#Region " ACCESS MANAGEMENT "

    ''' <summary> Checks if the custom scripts loaded successfully. </summary>
    ''' <returns> <c>True</c> if loaded; otherwise, <c>False</c>. </returns>
    Public Overridable Function Loaded() As Boolean
        Return False
    End Function

    ''' <summary> Gets the release code for the controller instrument. </summary>
    ''' <param name="serialNumber"> The serial number. </param>
    ''' <param name="salt">         The released instruments. </param>
    ''' <returns> The release value of the controller instrument. </returns>
    Public Overridable Function ReleaseValue(ByVal serialNumber As String, ByVal salt As String) As String
        Return String.Empty
    End Function

#End Region

End Class
