Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports isr.Core.EnumExtensions
Imports isr.VI.Facade.ComboBoxExtensions
Namespace ComboBoxExtensions

    Public Module Methods

#Region " SCPI: SENSE FUNCTION MODES "

        ''' <summary> List sense function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The list control. </param>
        ''' <param name="supportedSenseFunctionModes"> The sense function modes. </param>
        <Extension>
        Public Sub ListSupportedSenseFunctionModes(ByVal comboBox As Windows.Forms.ComboBox, ByVal supportedSenseFunctionModes As SenseFunctionModes)
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Dim selectedIndex As Integer = comboBox.SelectedIndex
            comboBox.DataSource = Nothing
            comboBox.DataSource = GetType(SenseFunctionModes).EnumValues.IncludeFilter(supportedSenseFunctionModes).ValueDescriptionPairs.ToList
            comboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            comboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If comboBox.Items.Count > 0 Then
                comboBox.SelectedIndex = Math.Max(selectedIndex, 0)
            End If
        End Sub

        ''' <summary> Returns the sense function modes selected by the list control. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox"> The list control. </param>
        ''' <returns> The SenseSenseFunctionMode. </returns>
        <Extension>
        Function SelectedSenseFunctionMode(ByVal comboBox As Windows.Forms.ComboBox) As SenseFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            Return CType(CType(comboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, SenseFunctionModes)
        End Function

        ''' <summary> Select sense function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">           The list control. </param>
        ''' <returns> The SenseFunctionMode. </returns>
        <Extension>
        Public Function SelectSenseFunctionMode(ByVal comboBox As Windows.Forms.ComboBox, ByVal senseFunctionMode As SenseFunctionModes?) As SenseFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If senseFunctionMode.HasValue AndAlso senseFunctionMode.Value <> VI.SenseFunctionModes.None AndAlso senseFunctionMode.Value <> comboBox.SelectedSenseFunctionMode Then
                comboBox.SelectedItem = senseFunctionMode.Value.ValueDescriptionPair
            End If
            Return comboBox.SelectedSenseFunctionMode
        End Function

        ''' <summary> Safe select sense function modes. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="comboBox">     The list control </param>
        ''' <param name="senseFunctionMode"> The sense function mode </param>
        <Extension>
        Public Function SafeSelectSenseFunctionMode(ByVal comboBox As Windows.Forms.ComboBox, ByVal senseFunctionMode As SenseFunctionModes?) As SenseFunctionModes
            If comboBox Is Nothing Then Throw New ArgumentNullException(NameOf(comboBox))
            If senseFunctionMode.HasValue AndAlso senseFunctionMode.Value <> VI.SenseFunctionModes.None AndAlso senseFunctionMode.Value <> comboBox.SelectedSenseFunctionMode Then
                If comboBox.InvokeRequired Then
                    comboBox.Invoke(New Action(Of ComboBox, SenseFunctionModes?)(AddressOf Methods.SafeSelectSenseFunctionMode), New Object() {comboBox, senseFunctionMode})
                Else
                    comboBox.SelectedItem = senseFunctionMode.Value.ValueDescriptionPair
                End If
            End If
            Return comboBox.SelectedSenseFunctionMode
        End Function

#End Region

    End Module

End Namespace


