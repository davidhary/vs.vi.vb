''' <summary>
''' Defines the contract that must be implemented by a Measure Resistance Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class MeasureResistanceSubsystemBase
    Inherits SourceMeasureUnitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MeasureResistanceSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " READING "

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary>
    ''' Gets or sets  or sets (protected) the reading.  When set, the value is converted to
    ''' resistance.
    ''' </summary>
    ''' <value> The reading. </value>
    Public Property Reading() As String
        Get
            Return Me._Reading
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.Reading, StringComparison.OrdinalIgnoreCase) Then
                Me._Reading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " RESISTANCE "

    ''' <summary> The resistance. </summary>
    Private _Resistance As Double?

    ''' <summary> Gets or sets (protected) the measured resistance. </summary>
    ''' <value> The resistance. </value>
    Public Property Resistance() As Double?
        Get
            Return Me._Resistance
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(value, Me.Resistance) Then
                Me._Resistance = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Measures and reads the resistance. </summary>
    ''' <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
    '''                                         type. </exception>
    Public Sub Measure()

        Dim printFormat As String = "%8.5f"
        Me.Session.WriteLine("{0}.source.output = {0}.OUTPUT_ON waitcomplete() print(string.format('{1}',{0}.measure.r())) ",
                                 Me.SourceMeasureUnitReference, printFormat)
        Me.Reading = Me.Session.ReadLine()
        Dim value As Double = 0
        If String.IsNullOrWhiteSpace(Me.Reading) Then
            Me.Resistance = New Double?
        Else
            If Double.TryParse(Me.Reading, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent,
                               Globalization.CultureInfo.InvariantCulture, value) Then
                Me.Resistance = value
            Else
                Me.Resistance = New Double?
                Throw New InvalidCastException(String.Format(Globalization.CultureInfo.InvariantCulture,
                                                              "Failed parsing {0} to number reading '{1}'", Me.Reading, Me.Session.LastMessageSent))

            End If
        End If
    End Sub

#End Region

End Class

