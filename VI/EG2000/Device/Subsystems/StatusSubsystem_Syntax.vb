
Partial Public Class StatusSubsystem

#Region " PRESET "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <remarks>
    ''' SCPI: ":STAT:PRES".
    ''' <see cref="F:isr.VI.Pith.Scpi.Syntax.ScpiSyntax.StatusPresetCommand"></see>
    ''' </remarks>
    ''' <value> The preset command. </value>
    Protected Overrides Property PresetCommand As String = String.Empty

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = String.Empty

    ''' <summary> Gets or sets the 'Next Error' query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property DequeueErrorQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String = "?E"

    ''' <summary> Gets or sets the error queue query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = String.Empty

#End Region

#Region " LINE FREQUENCY "

    ''' <summary> Gets or sets line frequency query command. </summary>
    ''' <value> The line frequency query command. </value>
    Protected Overrides Property LineFrequencyQueryCommand As String = VI.Pith.Scpi.Syntax.ReadLineFrequencyCommand

#End Region

End Class


