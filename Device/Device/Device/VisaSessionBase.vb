'---------------------------------------------------------------------------------------------------
' file:		.\Device\VisaSessionBase.vb
'
' summary:	Visa session base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Pith

''' <summary> Defines the contract that must be implemented by session opening classes. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-24, . from device base. </para>
''' </remarks>
Public MustInherit Class VisaSessionBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="session"> The session. </param>
    ''' <param name="talker">  The talker. </param>
    Protected Sub New(ByVal session As VI.Pith.SessionBase, ByVal talker As TraceMessageTalker)
        MyBase.New(talker)
        Me.NewThis(session, False)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="session"> The session. </param>
    Protected Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New()
        Me.NewThis(session, False)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New()
        Me._Subsystems = New SubsystemCollection From {statusSubsystem}
        Me.StatusSubsystemBase = statusSubsystem
        Me.NewThis(StatusSubsystemBase.Validated(statusSubsystem).Session, False)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    ''' <param name="talker">          The talker. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal talker As TraceMessageTalker)
        MyBase.New(talker)
        Me._Subsystems = New SubsystemCollection From {statusSubsystem}
        Me.StatusSubsystemBase = statusSubsystem
        Me.NewThis(StatusSubsystemBase.Validated(statusSubsystem).Session, False)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="VisaSessionBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
        Me._Subsystems = New SubsystemCollection
        Me.NewThis(isr.VI.SessionFactory.Get.Factory.Session(), True)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="VisaSessionBase" /> class. </summary>
    ''' <param name="session">        A reference to a <see cref="SessionBase">message based
    '''                               session</see>. </param>
    ''' <param name="isSessionOwner"> true if this object is session owner. </param>
    Private Sub NewThis(ByVal session As VI.Pith.SessionBase, ByVal isSessionOwner As Boolean)
        Me._ElapsedTimeStopwatch = New Stopwatch()
        If session Is Nothing Then
            isSessionOwner = True
            Me.Assign_Session(isr.VI.SessionFactory.Get.Factory.Session(), isSessionOwner)
        Else
            Me.Assign_Session(session, isSessionOwner)
        End If
        MyBase.ResourceClosedCaption = isr.VI.Pith.My.MySettings.Default.ClosedCaption
        MyBase.CandidateResourceTitle = isr.VI.Pith.My.MySettings.Default.ResourceTitle
        Me._ServiceNotificationLevel = NotifySyncLevel.Sync
        Me._Subsystems.AssignTalker(MyBase.Talker)
        Me._SessionFactory = New isr.VI.SessionFactory(MyBase.Talker) With {.Searchable = True,
                                                                            .PingFilterEnabled = False,
                                                                            .ResourcesFilter = session.ResourcesFilter}
        Me._PollTimer = New System.Timers.Timer() With {.Enabled = False, .AutoReset = True, .Interval = 1000}
        Me._PollEnabled = New isr.Core.Constructs.ConcurrentToken(Of Boolean) With {.Value = False}
    End Sub

#Region " I Disposable Support "

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the disposed status. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._PollEnabled IsNot Nothing Then Me._PollEnabled.Dispose() : Me._PollEnabled = Nothing
                    If Me._PollTimer IsNot Nothing Then Me._PollTimer.Dispose() : Me._PollTimer = Nothing
                    ' release both managed and unmanaged resources
                    Me.Session?.DisableServiceRequestEventHandler()
                    Me.RemoveServiceRequestEventHandler()
                    Me.RemoveServiceRequestedEventHandlers()
                    Me.RemoveOpeningEventHandlers()
                    Me.RemoveOpeningEventHandlers()
                    Me.RemoveClosingEventHandlers()
                    Me.RemoveClosedEventHandlers()
                    Me.RemoveInitializingEventHandlers()
                    Me.RemoveInitializingEventHandlers()
                    ' this also removes the talker that was assigned to the subsystems.
                    Me.AssignTalker(Nothing)
                    If Me._IsSessionOwner Then
                        Try
                            Me.CloseSession()
                            Me.Assign_Session(Nothing, True)
                        Catch ex As ObjectDisposedException
                            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
                        End Try
                    End If
                    Me._StatusSubsystemBase = Nothing
                    Me.Subsystems.Clear()
                Else
                    ' release only unmanaged resources.
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
    ''' unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

#End Region

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears the interface if interface clear is supported for this resource. </summary>
    Public Sub ClearInterface()
        If Me.Session.SupportsClearInterface Then
            Me.PublishVerbose($"{Me.ResourceNameCaption} clearing interface")
            Me.Session.ClearInterface()
            ' this will publish a report if device errors occurred
            Me.Session.ReadStatusRegister()
        End If
    End Sub

    ''' <summary>
    ''' Issues <see cref="SessionBase.ClearActiveState">Selective device clear (SDC)</see>. and
    ''' applies default settings and clears the Device.
    ''' </summary>
    Public Overrides Sub ClearActiveState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} clearing active state (SDC)")
        Me.Session.ClearActiveState(Me.Session.DeviceClearRefractoryPeriod)
        ' this will publish a report if device errors occurred
        Me.Session.ReadStatusRegister()
    End Sub

    ''' <summary>
    ''' Defines the clear execution state (CLS) by setting system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    Public Overridable Sub DefineClearExecutionState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} defining system clear execution state")
        Me.Subsystems.DefineClearExecutionState()
        ' this will publish a report if device errors occurred
        Me.Session.ReadStatusRegister()
    End Sub

    ''' <summary>
    ''' Clears the queues and resets all registers to zero. Sets system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    ''' <remarks> *CLS. </remarks>
    Public Sub ClearExecutionState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} clearing execution state")
        Me.Session.ClearExecutionState()
        Me.DefineClearExecutionState()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Sub InitKnownState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} initializing known state")
        If Me.StatusSubsystemBase IsNot Nothing Then
            Me.Subsystems.InitKnownState()
            Me.IsInitialized = True
            isr.Core.ApplianceBase.DoEventsWait(Me.Session.StatusReadDelay)
            ' this will publish a report if device errors occurred
            Me.Session.ReadStatusRegister()
        End If
        Me.IsInitialized = True
    End Sub

    ''' <summary> Sets the system to its Preset known state. </summary>
    Public Sub PresetKnownState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} presetting system known state")
        Me.Subsystems.PresetKnownState()
        isr.Core.ApplianceBase.DoEventsWait(Me.Session.StatusReadDelay)
        ' this will publish a report if device errors occurred
        Me.Session.ReadStatusRegister()
    End Sub

    ''' <summary> Defines the system and subsystems reset (RST) (default) known state. </summary>
    Public Overridable Sub DefineResetKnownState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} resetting subsystems known state")
        Me.Subsystems.DefineKnownResetState()
        isr.Core.ApplianceBase.DoEventsWait(Me.Session.StatusReadDelay)
        ' this will publish a report if device errors occurred
        Me.Session.ReadStatusRegister()
    End Sub

    ''' <summary>
    ''' Resets the device to its known reset (RST) (default) state and defines the relevant system
    ''' and subsystems state values.
    ''' </summary>
    ''' <remarks> *RST. </remarks>
    Public Overridable Sub ResetKnownState()
        Me.PublishVerbose($"{Me.ResourceNameCaption} resetting known state")
        Me.Session.ResetKnownState()
        Me.DefineResetKnownState()
    End Sub

    ''' <summary>
    ''' Resets, clears and initializes the device. Starts with issuing a selective-device-clear,
    ''' reset (RST), Clear Status (CLS, and clear error queue) and initialize.
    ''' </summary>
    Public Sub ResetClearInit()

        ' issues selective device clear.
        Me.ClearActiveState()

        ' reset device
        Me.ResetKnownState()

        ' Clear the device Status and set more defaults
        Me.ClearExecutionState()

        ' initialize the device must be done after clear (5892)
        Me.InitKnownState()

    End Sub

    ''' <summary>
    ''' Resets, clears and initializes the device. Starts with issuing a selective-device-clear,
    ''' reset (RST), Clear Status (CLS, and clear error queue) and initialize.
    ''' </summary>
    ''' <param name="timeout"> The timeout to use. This allows using a longer timeout than the
    '''                        minimal timeout set for the session. Typically, a source meter
    '''                        requires a 5000 milliseconds timeout. </param>
    Public Sub ResetClearInit(ByVal timeout As TimeSpan)
        Try
            Me.Session.StoreCommunicationTimeout(timeout)
            Me.ResetClearInit()
        Catch
            Throw
        Finally
            Me.Session.RestoreCommunicationTimeout()
        End Try
    End Sub

#End Region

#Region " RESOURCE NAME INFO "

    ''' <summary>
    ''' Checks if the specified resource name exists. Use for checking if the instrument is turned on.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resourceName">    Name of the resource. </param>
    ''' <param name="resourcesFilter"> The resources search pattern. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function Find(ByVal resourceName As String, ByVal resourcesFilter As String) As Boolean
        If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))
        Using rm As VI.Pith.ResourcesProviderBase = isr.VI.SessionFactory.Get.Factory.ResourcesProvider()
            Return If(String.IsNullOrWhiteSpace(resourcesFilter),
                rm.FindResources().ToArray.Contains(resourceName),
                rm.FindResources(resourcesFilter).ToArray.Contains(resourceName))
        End Using
    End Function

    ''' <summary>
    ''' Checks if the candidate resource name exists thus checking if the instrument is turned on.
    ''' </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ValidateCandidateResourceName() As Boolean
        Return Me.Session.ValidateCandidateResourceName(isr.VI.SessionFactory.Get.Factory.ResourcesProvider())
    End Function

#End Region

#Region " SESSION "

    ''' <summary> true if this object is session owner. </summary>
    ''' <value> The is session owner. </value>
    Private Property IsSessionOwner As Boolean

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The session. </summary>
    Private WithEvents _Session As VI.Pith.SessionBase
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the session. </summary>
    ''' <value> The session. </value>
    Public ReadOnly Property Session As VI.Pith.SessionBase
        Get
            Return Me._Session
        End Get
    End Property

    ''' <summary>
    ''' Constructor-safe session assignment. If session owner and session exists, must close first.
    ''' </summary>
    ''' <param name="session">        The value. </param>
    ''' <param name="isSessionOwner"> true if this object is session owner. </param>
    Private Sub Assign_Session(ByVal session As VI.Pith.SessionBase, ByVal isSessionOwner As Boolean)
        If Me._Session IsNot Nothing Then
            RemoveHandler Me._Session.PropertyChanged, AddressOf Me.SessionPropertyChanged
            RemoveHandler Me._Session.DeviceErrorOccurred, AddressOf Me.HandleDeviceErrorOccurred
            isr.Core.ApplianceBase.DoEvents()
            If Me.IsSessionOwner Then
                Me._Session.Dispose()
                ' release the session
                ' Trying to null the session raises an ObjectDisposedException 
                ' if session service request handler was not released. 
                Me._Session = Nothing
            End If
        End If
        Me.IsSessionOwner = isSessionOwner
        Me._Session = session
        If Me._Session IsNot Nothing Then
            AddHandler Me._Session.DeviceErrorOccurred, AddressOf Me.HandleDeviceErrorOccurred
            AddHandler Me._Session.PropertyChanged, AddressOf Me.SessionPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the session property changed action. </summary>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal sender As SessionBase, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Pith.SessionBase.Enabled)
                Me.NotifyPropertyChanged(NameOf(VisaSessionBase.Enabled))
                isr.Core.ApplianceBase.DoEvents()
                Me.PublishInfo($"{Me.ResourceNameCaption} {Me.Enabled.GetHashCode:enabled;enabled;disabled};. ")
                isr.Core.ApplianceBase.DoEvents()
            Case NameOf(VI.Pith.SessionBase.CandidateResourceName)
                Me.CandidateResourceName = sender.CandidateResourceName
            Case NameOf(VI.Pith.SessionBase.CandidateResourceTitle)
                Me.CandidateResourceTitle = sender.CandidateResourceTitle
            Case NameOf(VI.Pith.SessionBase.OpenResourceTitle)
                Me.OpenResourceTitle = sender.OpenResourceTitle
            Case NameOf(VI.Pith.SessionBase.OpenResourceName)
                Me.OpenResourceName = sender.OpenResourceName
            Case NameOf(VI.Pith.SessionBase.ResourceTitleCaption)
                Me.ResourceTitleCaption = sender.ResourceTitleCaption
            Case NameOf(VI.Pith.SessionBase.ResourceNameCaption)
                Me.ResourceNameCaption = sender.ResourceNameCaption
            Case NameOf(VI.Pith.SessionBase.ServiceRequestEventEnabled)
                If sender.ServiceRequestEventEnabled Then
                    Me.PollEnabled = False
                End If
            Case NameOf(VI.Pith.SessionBase.ResourcesFilter)
                Me.ResourcesFilter = sender.ResourcesFilter
        End Select
    End Sub

    ''' <summary> Handles the Session property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.Pith.SessionBase)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, VI.Pith.SessionBase), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Gets or sets the Enabled sentinel of the device. A device is enabled when hardware can be
    ''' used.
    ''' </summary>
    ''' <value> <c>True</c> if hardware device is enabled; <c>False</c> otherwise. </value>
    Public Overridable Property Enabled As Boolean
        Get
            Return Me.Session.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.Session.Enabled = value
        End Set
    End Property

    ''' <summary> The last compound error. </summary>
    Private _LastCompoundError As String

    ''' <summary> Gets or sets the last compound error. </summary>
    ''' <value> The last compound error. </value>
    Public Property LastCompoundError As String
        Get
            Return Me._LastCompoundError
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastCompoundError, StringComparison.OrdinalIgnoreCase) Then
                Me._LastCompoundError = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last error report. </summary>
    Private _LastErrorReport As String

    ''' <summary> Gets or sets the last error report. </summary>
    ''' <value> The last error report. </value>
    Public Property LastErrorReport As String
        Get
            Return Me._LastErrorReport
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastErrorReport, StringComparison.OrdinalIgnoreCase) Then
                Me._LastErrorReport = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Handles the <see cref="isr.VI.Pith.SessionBase.DeviceErrorOccurred"/> event.
    ''' </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="e">         Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Overloads Sub HandleErrorOccurred(ByVal subsystem As Pith.SessionBase, ByVal e As System.EventArgs)
        If subsystem Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(Pith.SessionBase)}.{NameOf(Pith.SessionBase.DeviceErrorOccurred)} event"
        Try
            If Me.StatusSubsystemBase Is Nothing Then
                If Me.Session.ErrorAvailable Then
                    activity = $"report error bit"
                    Me.LastCompoundError = $"Error bit {CInt(Me.Session.ErrorAvailableBit):X2}"
                    Me.PublishWarning($"{Me.ResourceNameCaption} {Me.LastCompoundError }")
                End If
            Else
                ' when this event is reported, the status subsystem has already fetched the error 
                ' and created a report.  This method saves this report. 
                activity = $"updating the error report"
                ' the error gets published when the subsystem queries the device errors. 
                Me.LastErrorReport = Me.StatusSubsystemBase.DeviceErrorReport
                activity = $"updating the compound error"
                Me.LastCompoundError = Me.StatusSubsystemBase.CompoundErrorMessage
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Handles the <see cref="isr.VI.Pith.SessionBase.DeviceErrorOccurred"/> event.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceErrorOccurred(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(Pith.SessionBase)}.{NameOf(Pith.SessionBase.DeviceErrorOccurred)} event"
        Try
            Me.HandleErrorOccurred(TryCast(sender, Pith.SessionBase), e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#Region " SESSION INITIALIZATION PROPERTIES "

    ''' <summary>
    ''' Gets or sets the bits that would be set for detecting if an error is available.
    ''' </summary>
    ''' <value> The error available bits. </value>
    Protected Overridable Property ErrorAvailableBits() As VI.Pith.ServiceRequests = VI.Pith.ServiceRequests.ErrorAvailable

    ''' <summary> Gets or sets the keep alive interval. </summary>
    ''' <remarks> Required only with VISA Non-Standard. </remarks>
    ''' <value> The keep alive interval. </value>
    Protected Overridable Property KeepAliveInterval As TimeSpan = TimeSpan.Zero ' TimeSpan.FromSeconds(58)

    ''' <summary> Gets or sets the is alive command. </summary>
    ''' <remarks> Required only with VISA Non-Standard. </remarks>
    ''' <value> The is alive command. </value>
    Protected Overridable Property IsAliveCommand As String = String.Empty ' "*OPC"

    ''' <summary> Gets or sets the is alive query command. </summary>
    ''' <remarks> Required only with VISA Non-Standard. </remarks>
    ''' <value> The is alive query command. </value>
    Protected Overridable Property IsAliveQueryCommand As String = String.Empty ' "*OPC?"

#End Region

#End Region

#Region " SERVICE REQUEST MANAGEMENT "

    ''' <summary> The service notification level. </summary>
    Private _ServiceNotificationLevel As isr.VI.Pith.NotifySyncLevel

    ''' <summary> Gets or sets the service request notification level. </summary>
    ''' <value> The service request notification level. </value>
    Public Property ServiceNotificationLevel As isr.VI.Pith.NotifySyncLevel
        Get
            Return Me._ServiceNotificationLevel
        End Get
        Set(value As isr.VI.Pith.NotifySyncLevel)
            If value <> Me.ServiceNotificationLevel Then
                Me._ServiceNotificationLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the service request enabled bitmask. </summary>
    ''' <value> The service request enabled bitmask. </value>
    Public ReadOnly Property ServiceRequestEnabledBitmask As VI.Pith.ServiceRequests
        Get
            Return If(Me.IsDeviceOpen,
                Me.Session.ServiceRequestEnabledBitmask.GetValueOrDefault(Pith.ServiceRequests.None),
                VI.Pith.ServiceRequests.None)
        End Get
    End Property

    ''' <summary> Gets the session service request event enabled sentinel. </summary>
    ''' <value> <c>True</c> if session service request event is enabled. </value>
    Public ReadOnly Property ServiceRequestEventEnabled As Boolean
        Get
            Return Me.IsSessionOpen AndAlso Me.Session.ServiceRequestEventEnabled
        End Get
    End Property

    ''' <summary> True if service request handler assigned. </summary>
    Private _ServiceRequestHandlerAssigned As Boolean

    ''' <summary>
    ''' Gets or sets an indication if an handler was assigned to the service request event.
    ''' </summary>
    ''' <value> <c>True</c> if a handler was assigned to the service request event. </value>
    Public Property ServiceRequestHandlerAssigned As Boolean
        Get
            Return Me._ServiceRequestHandlerAssigned
        End Get
        Protected Set(value As Boolean)
            Me._ServiceRequestHandlerAssigned = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Registers the device service request handler. </summary>
    ''' <remarks>
    ''' The Session service request handler must be registered and enabled before registering the
    ''' device event handler.
    ''' </remarks>
    Public Sub AddServiceRequestEventHandler()
        If Not Me.ServiceRequestHandlerAssigned Then
            AddHandler Me.Session.ServiceRequested, AddressOf Me.SessionBaseServiceRequested
            Me.ServiceRequestHandlerAssigned = True
        End If
    End Sub

    ''' <summary> Removes the device service request event handler. </summary>
    Public Sub RemoveServiceRequestEventHandler()
        If Me.ServiceRequestHandlerAssigned AndAlso Me.Session IsNot Nothing Then
            RemoveHandler Me.Session.ServiceRequested, AddressOf Me.SessionBaseServiceRequested
        End If
        Me.ServiceRequestHandlerAssigned = False
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Enumerates the Presettable subsystems. </summary>
    ''' <value> The subsystems. </value>
    Public ReadOnly Property Subsystems As SubsystemCollection

#End Region

#Region " STATUS SUBSYSTEM BASE "

    ''' <summary> Gets or sets the Status SubsystemBase. </summary>
    ''' <value> The Status SubsystemBase. </value>
    Public ReadOnly Property StatusSubsystemBase As StatusSubsystemBase

#End Region

#Region " MY SETTINGS "

    ''' <summary> Applies the settings. </summary>
    Protected MustOverride Sub ApplySettings()

#End Region

End Class

