'---------------------------------------------------------------------------------------------------
' file:		.\Tsp\DigitalLine.vb
'
' summary:	Digital line class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary> A digital line. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-06-29 </para>
''' </remarks>
Public Class DigitalLine
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <param name="lineNumber">        The line number. </param>
    ''' <param name="supportsReadWrite"> True to supports read write. </param>
    Public Sub New(ByVal lineNumber As Integer, ByVal supportsReadWrite As Boolean)
        MyBase.New
        Me.LineNumber = lineNumber
        Me.DigitalLineReadWriteEnabled = supportsReadWrite
        Me.ResetKnownStateThis()
    End Sub

    ''' <summary> Gets or sets the line number. </summary>
    ''' <value> The line number. </value>
    Public ReadOnly Property LineNumber As Integer

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Resets the known state this. </summary>
    Private Sub ResetKnownStateThis()
        Me._DigitalLineMode = VI.DigitalLineMode.DigitalInput
    End Sub

    ''' <summary> Sets the known reset (default) state. </summary>
    Public Sub ResetKnownState()
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " DIGITAL LINE MODE "

    ''' <summary> The digital line mode. </summary>
    Private _DigitalLineMode As DigitalLineMode?

    ''' <summary> Gets or sets the digital line mode. </summary>
    ''' <value> The digital line mode. </value>
    Public Property DigitalLineMode As DigitalLineMode?
        Get
            Return Me._DigitalLineMode
        End Get
        Protected Set(ByVal value As DigitalLineMode?)
            If Not Nullable.Equals(Me.DigitalLineMode, value) Then
                Me._DigitalLineMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the digital line Mode. </summary>
    ''' <param name="session">       The session. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="queryCommand">  The query command. </param>
    ''' <param name="value">         The  digital line Mode. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">digital line Mode</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalLineMode(ByVal session As VI.Pith.SessionBase, ByVal commandFormat As String, ByVal queryCommand As String, ByVal value As DigitalLineMode) As DigitalLineMode?
        Me.WriteDigitalLineMode(session, commandFormat, value)
        Me.QueryDigitalLineMode(session, queryCommand)
        Return Me.DigitalLineMode
    End Function

    ''' <summary> Queries the digital line Mode. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">      The session. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">digital line Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryDigitalLineMode(ByVal session As VI.Pith.SessionBase, ByVal queryCommand As String) As DigitalLineMode?
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            Dim mode As String = Me.DigitalLineMode.ToString
            session.MakeEmulatedReplyIfEmpty(mode)
            mode = session.QueryPrintTrimEnd(queryCommand)
            If String.IsNullOrWhiteSpace(mode) Then
                Dim message As String = $"Failed fetching {NameOf(DigitalLine)}({Me.LineNumber}).{NameOf(DigitalLine.DigitalLineMode)}"
                Debug.Assert(Not Debugger.IsAttached, message)
                Me.DigitalLineMode = New DigitalLineMode?
            Else
                Dim se As New StringEnumerator(Of DigitalLineMode)
                Me.DigitalLineMode = se.ParseContained(mode.BuildDelimitedValue)
            End If
        End If
        Return Me.DigitalLineMode
    End Function

    ''' <summary>
    ''' Writes the digital line Mode without reading back the value from the device.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">       The session. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="value">         The digital line Mode. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">digital line Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteDigitalLineMode(ByVal session As VI.Pith.SessionBase, ByVal commandFormat As String, ByVal value As DigitalLineMode) As DigitalLineMode?
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            session.WriteLine(commandFormat, value.ExtractBetween())
        End If
        Me.DigitalLineMode = value
        Return Me.DigitalLineMode
    End Function

#End Region

#Region " DIGITAL LINE RESET "

    ''' <summary> Resets the digital line. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">       The session. </param>
    ''' <param name="commandFormat"> The command format. </param>
    Public Overridable Sub ResetDigitalLine(ByVal session As VI.Pith.SessionBase, ByVal commandFormat As String)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            session.Execute(commandFormat)
        End If
        Me.DigitalLineMode = VI.DigitalLineMode.DigitalInput
    End Sub

#End Region

#Region " DIGITAL LINE STATE "

    ''' <summary> True to enable, false to disable the digital line read write. </summary>
    Private _DigitalLineReadWriteEnabled As Boolean

    ''' <summary> Gets or sets the digital line read write enabled. </summary>
    ''' <value>
    ''' The digital line read write enabled. <c>True</c> if both read and write are supported and
    ''' enabled; <c>False</c> otherwise.
    ''' </value>
    Public Property DigitalLineReadWriteEnabled As Boolean
        Get
            Return Me._DigitalLineReadWriteEnabled
        End Get
        Protected Set(ByVal value As Boolean)
            If Not Nullable.Equals(Me.DigitalLineReadWriteEnabled, value) Then
                Me._DigitalLineReadWriteEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> State of the digital line. </summary>
    Private _DigitalLineState As DigitalLineState?

    ''' <summary> Gets or sets the digital line State. </summary>
    ''' <value> The digital line State. </value>
    Public Property DigitalLineState As DigitalLineState?
        Get
            Return Me._DigitalLineState
        End Get
        Protected Set(ByVal value As DigitalLineState?)
            If Not Nullable.Equals(Me.DigitalLineState, value) Then
                Me._DigitalLineState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the digital line State. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="session">       The session. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="queryCommand">  The query command. </param>
    ''' <param name="value">         The  digital line State. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineState">digital line State</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalLineState(ByVal session As VI.Pith.SessionBase, ByVal commandFormat As String, ByVal queryCommand As String, ByVal value As DigitalLineState) As DigitalLineState?
        If Me.DigitalLineReadWriteEnabled Then
            Me.WriteDigitalLineState(session, commandFormat, value)
            Me.QueryDigitalLineState(session, queryCommand)
        Else
            Throw New InvalidOperationException($"Digital line #{Me.LineNumber} does not support Read and Write")
        End If
        Return Me.DigitalLineState
    End Function

    ''' <summary> Queries the digital line State. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">      The session. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineState">digital line State</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryDigitalLineState(ByVal session As VI.Pith.SessionBase, ByVal queryCommand As String) As DigitalLineState?
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            Dim State As String = Me.DigitalLineState.ToString
            session.MakeEmulatedReplyIfEmpty(State)
            State = session.QueryPrintTrimEnd(queryCommand)
            If String.IsNullOrWhiteSpace(State) Then
                Dim message As String = $"Failed fetching {NameOf(DigitalLine)}({Me.LineNumber}).{NameOf(DigitalLine.DigitalLineState)}"
                Debug.Assert(Not Debugger.IsAttached, message)
                Me.DigitalLineState = New DigitalLineState?
            Else
                Dim se As New StringEnumerator(Of DigitalLineState)
                Me.DigitalLineState = se.ParseContained(State.BuildDelimitedValue)
            End If
        End If
        Return Me.DigitalLineState
    End Function

    ''' <summary>
    ''' Writes the digital line State without reading back the value from the device.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">       The session. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="value">         The digital line State. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineState">digital line State</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteDigitalLineState(ByVal session As VI.Pith.SessionBase, ByVal commandFormat As String, ByVal value As DigitalLineState) As DigitalLineState?
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            session.WriteLine(commandFormat, value.ExtractBetween())
        End If
        Me.DigitalLineState = value
        Return Me.DigitalLineState
    End Function

#End Region

End Class

''' <summary> Collection of digital lines. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-06-29 </para>
''' </remarks>
Public Class DigitalLineCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, DigitalLine)

    ''' <summary> Constructor. </summary>
    ''' <param name="count">            Number of. </param>
    ''' <param name="supportReadWrite"> True to support read write. </param>
    Public Sub New(ByVal count As Integer, ByVal supportReadWrite As Boolean)
        MyBase.New()
        Me.Populate(count, supportReadWrite)
    End Sub

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As DigitalLine) As Integer
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.LineNumber
    End Function

    ''' <summary> Populates. </summary>
    ''' <param name="count">            Number of. </param>
    ''' <param name="supportReadWrite"> True to support read write. </param>
    Private Sub Populate(ByVal count As Integer, ByVal supportReadWrite As Boolean)
        For i As Integer = 1 To count
            Try
                Dim line As DigitalLine = New DigitalLine(i, supportReadWrite)

                Me.Add(line)
            Catch
                Throw
            Finally
            End Try
        Next
    End Sub

End Class

''' <summary> Values that represent digital line states. </summary>
Public Enum DigitalLineState

    ''' <summary> An enum constant representing the low option. </summary>
    <ComponentModel.Description("Low (digio.STATE_LOW)")>
    Low

    ''' <summary> An enum constant representing the high option. </summary>
    <ComponentModel.Description("High (digio.STATE_HIGH)")>
    High
End Enum

''' <summary> Values that represent digital line modes. </summary>
Public Enum DigitalLineMode

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None

    ''' <summary> An enum constant representing the digital input option.
    '''           The instrument automatically detects externally generated logic
    '''           levels. You can read an input line, but you cannot write To it. </summary>
    <ComponentModel.Description("Digital control, input (digio.MODE_DIGITAL_IN)")>
    DigitalInput

    ''' <summary> An enum constant representing the digital output option. 
    '''           You can set the line as logic high (+5 V) or as logic low (0 V). 
    '''           The Default level Is logic low (0 V). When the instrument Is In output mode, 
    '''           the line Is actively driven high Or low. </summary>
    <ComponentModel.Description("Digital control, output (digio.MODE_DIGITAL_OUT)")>
    DigitalOutput

    ''' <summary> An enum constant representing the digital open drain option. 
    '''           The line can serve as an input, an output Or both. When a digital 
    '''           I/O line is used as an input in open-drain mode, you must write a 1 To it.</summary>
    <ComponentModel.Description("Digital control, open-drain (digio.MODE_DIGITAL_OPEN_DRAIN)")>
    DigitalOpenDrain

    ''' <summary> An enum constant representing the trigger input option. 
    '''           The line automatically responds to and detects externally generated
    '''           triggers. It detects falling-edge, rising-edge, Or either-edge triggers As input. 
    '''           This line state uses the edge setting specified by the trigger.digin[N].edge attribute.</summary>
    <ComponentModel.Description("Trigger control, input (digio.MODE_TRIGGER_IN)")>
    TriggerInput

    ''' <summary> An enum constant representing the trigger output option.
    '''           The line is automatically set high or low depending on the output
    '''           logic setting. Use the negative logic setting When you want 
    '''           to generate a falling edge trigger and use the positive logic 
    '''           setting When you want To generate a rising edge trigger. </summary>
    <ComponentModel.Description("Trigger control, output (digio.MODE_TRIGGER_OUT)")>
    TriggerOutput

    ''' <summary> An enum constant representing the trigger open drain option. 
    '''           Configures the line to be an open-drain signal. You can
    '''           use the line To detect input triggers Or generate output triggers. 
    '''           This line state uses the edge setting specified by the trigger.digin[N].edge attribute. </summary>
    <ComponentModel.Description("Trigger control, open-drain (digio.MODE_TRIGGER_OPEN_DRAIN)")>
    TriggerOpenDrain

    ''' <summary> An enum constant representing the synchronous master option. 
    '''           When the line Is set as a synchronous master, the line detects 
    '''           rising-edge triggers as input. For output, the line asserts a TTL-low pulse. </summary>
    <ComponentModel.Description("Synchronous master (digio.MODE_SYNCHRONOUS_MASTER)")>
    SynchronousMaster

    ''' <summary> An enum constant representing the synchronous acceptor option.
    '''           When the line is set as a synchronous acceptor, the line detects
    '''           the falling-edge input triggers and automatically latches And drives the trigger line low.
    '''           Asserting an output trigger releases the latched line. </summary>
    <ComponentModel.Description("Synchronous acceptor (digio.MODE_SYNCHRONOUS_ACCEPTOR)")>
    SynchronousAcceptor
End Enum
