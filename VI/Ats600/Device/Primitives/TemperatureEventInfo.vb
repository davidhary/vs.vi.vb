'---------------------------------------------------------------------------------------------------
' file:		Device\Primitives\TemperatureEventInfo.vb
'
' summary:	Temperature event information class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

Imports System.ComponentModel

''' <summary> Information about the Temperature Event. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-01-09 </para>
''' </remarks>
Public Class TemperatureEventInfo

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="temperatureEvent"> The temperature event. </param>
    Public Sub New(ByVal temperatureEvent As TemperatureEvents)
        Me.New()
        Me._TemperatureEvent = temperatureEvent
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As TemperatureEventInfo)
        Me.New()
        If value IsNot Nothing Then
            Me._TemperatureEvent = value.TemperatureEvent
        End If
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub ClearKnownStateThis()
        Me._TemperatureEvent = TemperatureEvents.None
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

#End Region

#Region " VALUE "

    ''' <summary> The temperature event. </summary>
    Private _TemperatureEvent As TemperatureEvents

    ''' <summary> Gets the temperature event. </summary>
    ''' <value> The temperature event. </value>
    Public ReadOnly Property TemperatureEvent As TemperatureEvents
        Get
            Return Me._TemperatureEvent
        End Get
    End Property

#End Region

#Region " BIT VALUES "

    ''' <summary>
    ''' Query if the Bits of the <paramref name="bits">Temperature Events</paramref> are on.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <param name="bits">  The bits. </param>
    ''' <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
    Public Shared Function IsBit(ByVal value As TemperatureEvents, ByVal bits As TemperatureEvents) As Boolean
        Return (value And bits) <> 0
    End Function

    ''' <summary>
    ''' Query if the Bit of the <paramref name="bit">Temperature Event</paramref> is on.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="bit"> The bit. </param>
    ''' <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
    Public Function IsBit(ByVal bit As TemperatureEventBit) As Boolean
        Return Me.IsBit(CType(CInt(2 ^ CInt(bit)), TemperatureEvents))
    End Function

    ''' <summary>
    ''' Query if the Bits of the <paramref name="bits">Temperature Events</paramref> are on.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="bits"> The bits. </param>
    ''' <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
    Public Function IsBit(ByVal bits As TemperatureEvents) As Boolean
        Return (Me.TemperatureEvent And bits) <> 0
    End Function

    ''' <summary> Query if this object is end of cycles. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>true</c> if end of cycles; otherwise <c>false</c> </returns>
    Public Function IsEndOfCycles() As Boolean
        Return Me.IsBit(TemperatureEvents.EndOfCycles)
    End Function

    ''' <summary> Query if this object is at temperature. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>true</c> if at temperature; otherwise <c>false</c> </returns>
    Public Function IsAtTemperature() As Boolean
        Return Me.IsBit(TemperatureEvents.AtTemperature)
    End Function

    ''' <summary> Query if this object is not at temperature. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>true</c> if not at temperature; otherwise <c>false</c> </returns>
    Public Function IsNotAtTemperature() As Boolean
        Return Me.IsBit(TemperatureEvents.NotAtTemperature)
    End Function

    ''' <summary> Query if this object is end of cycle. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>true</c> if end of cycle; otherwise <c>false</c> </returns>
    Public Function IsEndOfCycle() As Boolean
        Return Me.IsBit(TemperatureEvents.EndOfCycle)
    End Function

    ''' <summary> Query if this object is cycling stopped. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>true</c> if cycling stopped; otherwise <c>false</c> </returns>
    Public Function IsCyclingStopped() As Boolean
        Return Me.IsBit(TemperatureEvents.CyclingStopped)
    End Function

    ''' <summary> Query if this object is test time elapsed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>true</c> if test time elapsed; otherwise <c>false</c> </returns>
    Public Function IsTestTimeElapsed() As Boolean
        Return Me.IsBit(TemperatureEvents.TestTimeElapsed)
    End Function

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Public ReadOnly Property Description As String
        Get
            Return Me.TemperatureEvent.Description
        End Get
    End Property

    ''' <summary> Query if any <see cref="TemperatureEvent">bits</see> are. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="included"> The included. </param>
    ''' <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
    Public Function IsIncluded(ByVal included As TemperatureEvents) As Boolean
        Return TemperatureEventInfo.IsIncluded(Me.TemperatureEvent, included)
    End Function

#End Region

#Region " HELPERS "

    ''' <summary> Builds Temperature Events. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="excluded"> The excluded. </param>
    ''' <param name="included"> The included. </param>
    ''' <returns> The TemperatureEvents. </returns>
    Public Shared Function BuildTemperatureEvents(ByVal value As TemperatureEvents, ByVal excluded As TemperatureEvents, ByVal included As TemperatureEvents) As TemperatureEvents
        Return (value Or included) And Not excluded
    End Function

    ''' <summary> Query if 'value' is included. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="included"> The included. </param>
    ''' <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
    Public Shared Function IsIncluded(ByVal value As TemperatureEvents, ByVal included As TemperatureEvents) As Boolean
        Return (value And included) <> TemperatureEvents.None
    End Function

#End Region

End Class

''' <summary> Values that represent the Temperature Event bit. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Enum TemperatureEventBit
    ''' <summary> An enum constant representing at temperature option. </summary>

    <Description("At Temperature")>
    AtTemperature = 0
    ''' <summary> An enum constant representing the not at temperature option. </summary>

    <Description("Not At Temperature")>
    NotAtTemperature = 1
    ''' <summary> An enum constant representing the test time elapsed option. </summary>

    <Description("Test Time Elapsed")>
    TestTimeElapsed = 2
    ''' <summary> An enum constant representing the end of cycle option. </summary>

    <Description("End Of Cycle")>
    EndOfCycle = 3
    ''' <summary> An enum constant representing the end of cycles option. </summary>

    <Description("End Of Cycles")>
    EndOfCycles = 4
    ''' <summary> An enum constant representing the cycling stopped option. </summary>

    <Description("Cycling Stopped")>
    CyclingStopped = 5
End Enum

''' <summary>
''' A bit field of flags for specifying combination of Temperature Event values.
''' </summary>
''' <remarks> David, 2020-10-11. </remarks>
<Flags()>
Public Enum TemperatureEvents
    ''' <summary> An enum constant representing the none option. </summary>

    <Description("None")>
    None = 0
    ''' <summary> An enum constant representing at temperature option. </summary>

    <Description("At Temperature")>
    AtTemperature = CInt(2 ^ CInt(TemperatureEventBit.AtTemperature))
    ''' <summary> An enum constant representing the not at temperature option. </summary>

    <Description("Not At Temperature")>
    NotAtTemperature = CInt(2 ^ CInt(TemperatureEventBit.NotAtTemperature))
    ''' <summary> An enum constant representing the test time elapsed option. </summary>

    <Description("Test Time Elapsed")>
    TestTimeElapsed = CInt(2 ^ CInt(TemperatureEventBit.TestTimeElapsed))
    ''' <summary> An enum constant representing the end of cycle option. </summary>

    <Description("End Of Cycle")>
    EndOfCycle = CInt(2 ^ CInt(TemperatureEventBit.EndOfCycle))
    ''' <summary> An enum constant representing the end of cycles option. </summary>

    <Description("End Of Cycles")>
    EndOfCycles = CInt(2 ^ CInt(TemperatureEventBit.EndOfCycles))
    ''' <summary> An enum constant representing the cycling stopped option. </summary>

    <Description("Cycling Stopped")>
    CyclingStopped = CInt(2 ^ CInt(TemperatureEventBit.CyclingStopped))
End Enum
