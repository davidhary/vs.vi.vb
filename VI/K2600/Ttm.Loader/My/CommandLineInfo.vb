''' <summary>
''' A sealed class the parses the command line and provides the command line values.
''' </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2011-02-02, x.x.4050.x. </para>
''' </remarks>
Public NotInheritable Class CommandLineInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " PARSER "

    ''' <summary> Gets the no-operation command line option. </summary>
    ''' <value> The no operation option. </value>
    Public Shared ReadOnly Property NoOperationOption As String
        Get
            Return "/nop"
        End Get
    End Property

    ''' <summary> Gets the resource name command line option. </summary>
    ''' <value> The resource name option. </value>
    Public Shared ReadOnly Property ResourceNameOption As String
        Get
            Return "/r:"
        End Get
    End Property

    ''' <summary> Gets the source measure unit name command line option. </summary>
    ''' <value> The resource name option. </value>
    Public Shared ReadOnly Property SourceMeasureUnitNameOption As String
        Get
            Return "/smu:"
        End Get
    End Property

    ''' <summary> Gets the Device-Enabled option. </summary>
    ''' <value> The Device enabled option. </value>
    Public Shared ReadOnly Property DevicesEnabledOption As String
        Get
            Return "/d:"
        End Get
    End Property

    ''' <summary> Validate the command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentException"> This exception is raised if a command line argument is
    '''                                      not handled. </exception>
    ''' <param name="commandLineArguments"> The command line arguments. </param>
    Public Shared Sub ValidateCommandLine(ByVal commandLineArguments As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArguments IsNot Nothing Then
            For Each argument As String In commandLineArguments
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.ResourceNameOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.SourceMeasureUnitNameOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Nop = True
                Else
                    Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Unknown command line argument '{0}' was detected. Should be Ignored.", argument),
                                                          NameOf(commandLineArguments))
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArgs IsNot Nothing Then
            For Each argument As String In commandLineArgs
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                    Dim value As String = argument.Substring(CommandLineInfo.DevicesEnabledOption.Length)
                    CommandLineInfo.DevicesEnabled = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                ElseIf argument.StartsWith(CommandLineInfo.ResourceNameOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.ResourceName = argument.Substring(CommandLineInfo.ResourceNameOption.Length)
                ElseIf argument.StartsWith(CommandLineInfo.SourceMeasureUnitNameOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.SourceMeasureUnitName = String.Format("smu{0}", argument.Substring(CommandLineInfo.ResourceNameOption.Length))
                ElseIf argument.StartsWith(CommandLineInfo.NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Nop = True
                Else
                    ' do nothing
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    ''' <returns> True if success or false if Exception occurred. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

        Try

            CommandLineInfo.ParseCommandLine(commandLineArgs)
            Return True

        Catch ex As ArgumentException

            My.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId, "Unknown argument ignored")
            Return True

        Catch ex As System.Exception

            If commandLineArgs Is Nothing Then
                My.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId, "Failed parsing empty command line")
            Else
#Disable Warning CA1825 ' Avoid zero-length array allocations.
                Dim args As String() = {}
#Enable Warning CA1825 ' Avoid zero-length array allocations.
                commandLineArgs.CopyTo(args, 0)
                My.Application.Logger.WriteExceptionDetails(ex, TraceEventType.Error, My.MyApplication.TraceEventId,
                                                            "Failed parsing command line '{0}'", String.Join(" ", args))
            End If

            Return False
        End Try
    End Function

#End Region

#Region " COMMAND LINE ELEMENTS "

    ''' <summary> Gets or sets the source measure unit name. </summary>
    ''' <value> The resource name. </value>
    Public Shared Property SourceMeasureUnitName As String

    ''' <summary> Gets or sets the resource name. </summary>
    ''' <value> The resource name. </value>
    Public Shared Property ResourceName As String

    ''' <summary> Gets or sets a value indicating whether actual use of Device is enabled. </summary>
    ''' <value>
    ''' <c>null</c> if no value, <c>True</c> if Device are enabled; otherwise, <c>False</c>.
    ''' </value>
    Public Shared Property DevicesEnabled() As Boolean?

    ''' <summary> Gets or sets the no operation option. </summary>
    ''' <value> The nop. </value>
    Public Shared Property Nop As Boolean

#End Region

End Class
