﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReadingView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReadingView))
        Me._ReadingToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReadButton = New System.Windows.Forms.ToolStripButton()
        Me._ReadingComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._AbortButton = New System.Windows.Forms.ToolStripButton()
        Me._InitiateTriggerButton = New System.Windows.Forms.ToolStripButton()
        Me._EventsMenuButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._HandleMeasurementEventMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AutoInitiateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._BufferToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReadBufferButton = New System.Windows.Forms.ToolStripButton()
        Me._ReadingsCountLabel = New isr.Core.Controls.ToolStripLabel()
        Me._ClearBufferDisplayButton = New System.Windows.Forms.ToolStripButton()
        Me._ReadingsDataGridView = New System.Windows.Forms.DataGridView()
        Me._ReadingToolStrip.SuspendLayout()
        Me._BufferToolStrip.SuspendLayout()
        CType(Me._ReadingsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ReadingToolStrip
        '
        Me._ReadingToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadButton, Me._ReadingComboBox, Me._AbortButton, Me._InitiateTriggerButton, Me._EventsMenuButton})
        Me._ReadingToolStrip.Location = New System.Drawing.Point(0, 0)

        Me._ReadingToolStrip.Name = "_ReadingToolStrip"
        Me._ReadingToolStrip.Size = New System.Drawing.Size(379, 25)
        Me._ReadingToolStrip.TabIndex = 19
        Me._ReadingToolStrip.Text = "ToolStrip1"
        '
        '_ReadButton
        '
        Me._ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadButton.Image = CType(resources.GetObject("_ReadButton.Image"), System.Drawing.Image)
        Me._ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(37, 22)
        Me._ReadButton.Text = "Read"
        Me._ReadButton.ToolTipText = "Read single reading"
        '
        '_ReadingComboBox
        '
        Me._ReadingComboBox.Name = "_ReadingComboBox"
        Me._ReadingComboBox.Size = New System.Drawing.Size(121, 25)
        Me._ReadingComboBox.Text = "<reading>"
        Me._ReadingComboBox.ToolTipText = "Select reading type"
        '
        '_AbortButton
        '
        Me._AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AbortButton.Image = CType(resources.GetObject("_AbortButton.Image"), System.Drawing.Image)
        Me._AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(41, 22)
        Me._AbortButton.Text = "Abort"
        Me._AbortButton.ToolTipText = "Aborts active trigger"
        '
        '_InitiateTriggerButton
        '
        Me._InitiateTriggerButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._InitiateTriggerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._InitiateTriggerButton.Image = CType(resources.GetObject("_InitiateTriggerButton.Image"), System.Drawing.Image)
        Me._InitiateTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._InitiateTriggerButton.Name = "_InitiateTriggerButton"
        Me._InitiateTriggerButton.Size = New System.Drawing.Size(47, 22)
        Me._InitiateTriggerButton.Text = "Initiate"
        Me._InitiateTriggerButton.ToolTipText = "Initiates a trigger"
        '
        '_EventsMenuButton
        '
        Me._EventsMenuButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._EventsMenuButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._EventsMenuButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._HandleMeasurementEventMenuItem, Me._AutoInitiateMenuItem})
        Me._EventsMenuButton.Image = CType(resources.GetObject("_EventsMenuButton.Image"), System.Drawing.Image)
        Me._EventsMenuButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._EventsMenuButton.Name = "_EventsMenuButton"
        Me._EventsMenuButton.Size = New System.Drawing.Size(57, 22)
        Me._EventsMenuButton.Text = "Events"
        Me._EventsMenuButton.ToolTipText = "Enable event handling"
        '
        '_HandleMeasurementEventMenuItem
        '
        Me._HandleMeasurementEventMenuItem.CheckOnClick = True
        Me._HandleMeasurementEventMenuItem.Name = "_HandleMeasurementEventMenuItem"
        Me._HandleMeasurementEventMenuItem.Size = New System.Drawing.Size(147, 22)
        Me._HandleMeasurementEventMenuItem.Text = "Measurement"
        Me._HandleMeasurementEventMenuItem.ToolTipText = "Check to handle measurement events"
        '
        '_AutoInitiateMenuItem
        '
        Me._AutoInitiateMenuItem.CheckOnClick = True
        Me._AutoInitiateMenuItem.Name = "_AutoInitiateMenuItem"
        Me._AutoInitiateMenuItem.Size = New System.Drawing.Size(147, 22)
        Me._AutoInitiateMenuItem.Text = "Auto Initiate"
        Me._AutoInitiateMenuItem.ToolTipText = "Check to automatically initiates a new trigger upon handling the event"
        '
        '_BufferToolStrip
        '
        Me._BufferToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadBufferButton, Me._ReadingsCountLabel, Me._ClearBufferDisplayButton})
        Me._BufferToolStrip.Location = New System.Drawing.Point(0, 25)
        Me._BufferToolStrip.Name = "_BufferToolStrip"
        Me._BufferToolStrip.Size = New System.Drawing.Size(379, 25)
        Me._BufferToolStrip.TabIndex = 22
        Me._BufferToolStrip.Text = "ToolStrip1"
        '
        '_ReadBufferButton
        '
        Me._ReadBufferButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadBufferButton.Image = CType(resources.GetObject("_ReadBufferButton.Image"), System.Drawing.Image)
        Me._ReadBufferButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadBufferButton.Name = "_ReadBufferButton"
        Me._ReadBufferButton.Size = New System.Drawing.Size(72, 22)
        Me._ReadBufferButton.Text = "Read Buffer"
        Me._ReadBufferButton.ToolTipText = "Reads the buffer"
        '
        '_ReadingsCountLabel
        '
        Me._ReadingsCountLabel.Name = "_ReadingsCountLabel"
        Me._ReadingsCountLabel.Size = New System.Drawing.Size(13, 22)
        Me._ReadingsCountLabel.Text = "0"
        Me._ReadingsCountLabel.ToolTipText = "Buffer count"
        '
        '_ClearBufferDisplayButton
        '
        Me._ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ClearBufferDisplayButton.Font = New System.Drawing.Font("Wingdings", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me._ClearBufferDisplayButton.Image = CType(resources.GetObject("_ClearBufferDisplayButton.Image"), System.Drawing.Image)
        Me._ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton"
        Me._ClearBufferDisplayButton.Size = New System.Drawing.Size(25, 22)
        Me._ClearBufferDisplayButton.Text = ""
        Me._ClearBufferDisplayButton.ToolTipText = "Clears the grid"
        '
        '_ReadingsDataGridView
        '
        Me._ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ReadingsDataGridView.Location = New System.Drawing.Point(0, 50)
        Me._ReadingsDataGridView.Name = "_ReadingsDataGridView"
        Me._ReadingsDataGridView.Size = New System.Drawing.Size(379, 305)
        Me._ReadingsDataGridView.TabIndex = 23
        '
        'ReadingView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ReadingsDataGridView)
        Me.Controls.Add(Me._BufferToolStrip)
        Me.Controls.Add(Me._ReadingToolStrip)
        Me.Name = "ReadingView"
        Me.Size = New System.Drawing.Size(379, 355)
        Me._ReadingToolStrip.ResumeLayout(False)
        Me._ReadingToolStrip.PerformLayout()
        Me._BufferToolStrip.ResumeLayout(False)
        Me._BufferToolStrip.PerformLayout()
        CType(Me._ReadingsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ReadingToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ReadButton As Windows.Forms.ToolStripButton
    Private WithEvents _ReadingComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _AbortButton As Windows.Forms.ToolStripButton
    Private WithEvents _InitiateTriggerButton As Windows.Forms.ToolStripButton
    Private WithEvents _EventsMenuButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _HandleMeasurementEventMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AutoInitiateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _BufferToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ReadBufferButton As Windows.Forms.ToolStripButton
    Private WithEvents _ReadingsCountLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _ClearBufferDisplayButton As Windows.Forms.ToolStripButton
    Private WithEvents _ReadingsDataGridView As Windows.Forms.DataGridView
End Class
