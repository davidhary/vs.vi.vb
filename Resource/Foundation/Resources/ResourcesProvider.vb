'---------------------------------------------------------------------------------------------------
' file:		.\Resources\ResourcesProvider.vb
'
' summary:	Resources provider class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Foundation.ExceptionExtensions

''' <summary> The VISA resources provider. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para>
''' </remarks>
Public Class ResourcesProvider
    Inherits VI.Pith.ResourcesProviderBase

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
    End Sub

#Region "IDisposable Support"

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing resource manager", $"Exception {ex.ToFullBlownString}")
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region

    #End Region

    #Region " RESOURCE MANAGER "

    ''' <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public Overrides ReadOnly Property IsDummy As Boolean = False

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Parse resource. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
    Public Overrides Function ParseResource(ByVal resourceName As String) As VI.Pith.ResourceNameInfo
        Dim activity As String = $"parsing resource {resourceName}"
        Try
            Return Me.ResourceFinder.ParseResource(resourceName)
        Catch ex As System.MissingMethodException
            Throw New isr.Core.OperationFailedException($"Failed {activity}; IVI.Visa assembly {My.Settings.VisaAssemblyVersion} and system file {My.Settings.VisaSystem64FileProductVersion} versions are expected", ex)
        Catch
            Throw
        End Try
    End Function

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources() As IEnumerable(Of String)
        Return Me.ResourceFinder.FindResources()
    End Function

    ''' <summary> Tries to find resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-08. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindResources() As (Success As Boolean, Details As String)
        Return Me.ResourceFinder.TryFindResources()
    End Function

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources(ByVal filter As String) As IEnumerable(Of String)
        Return Me.ResourceFinder.FindResources(filter)
    End Function

    ''' <summary> Tries to find resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-08. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindResources(ByVal filter As String) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Return Me.ResourceFinder.TryFindResources(filter)
    End Function

    ''' <summary> Returns true if the specified resource exists in the resource names cache. </summary>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function Exists(ByVal resourceName As String) As Boolean
        Return Me.ResourceFinder.Exists(resourceName) AndAlso
               Not isr.VI.Pith.ResourceNamesManager.IsTcpipResource(resourceName) OrElse isr.VI.Pith.ResourceNamesManager.PingTcpipResource(resourceName)
    End Function

#End Region

#Region " INTERFACES "

    ''' <summary> Searches for the interface in the resource names cache. </summary>
    ''' <param name="resourceName"> The interface resource name. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function InterfaceExists(ByVal resourceName As String) As Boolean
        Return Me.ResourceFinder.InterfaceExists(resourceName)
    End Function

    ''' <summary> Searches for all interfaces in the resource names cache. </summary>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces() As IEnumerable(Of String)
        Return Me.ResourceFinder.FindInterfaces(VI.Pith.ResourceNamesManager.BuildInterfaceFilter)
    End Function

    ''' <summary> Try find interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInterfaces() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Return Me.ResourceFinder.TryFindInterfaces()
    End Function

    ''' <summary> Searches for the interfaces in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Return Me.ResourceFinder.FindInterfaces(VI.Pith.ResourceNamesManager.BuildInterfaceFilter(interfaceType))
    End Function

    ''' <summary> Try find interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Return Me.ResourceFinder.TryFindInterfaces(interfaceType)
    End Function

#End Region

#Region " INSTRUMENTS  "

    ''' <summary> Searches for the instrument in the resource names cache. </summary>
    ''' <param name="resourceName"> The instrument resource name. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function FindInstrument(ByVal resourceName As String) As Boolean
        Return Me.ResourceFinder.FindInstrument(resourceName)
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments() As IEnumerable(Of String)
        Return Me.ResourceFinder.FindInstruments(VI.Pith.ResourceNamesManager.BuildInstrumentFilter())
    End Function

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    '''    <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    Public Overrides Function TryFindInstruments() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Return Me.ResourceFinder.TryFindInstruments()
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Return Me.ResourceFinder.FindInstruments(VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType))
    End Function

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    '''    <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    Public Overrides Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Return Me.ResourceFinder.TryFindInstruments(interfaceType)
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)
        Return Me.ResourceFinder.FindInstruments(VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType, boardNumber))
    End Function

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                                     ByVal interfaceNumber As Integer) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Return Me.ResourceFinder.TryFindInstruments(interfaceType, interfaceNumber)
    End Function

#End Region

#Region " VERSION VALIDATION "

    ''' <summary>
    ''' Validates the implementation specification visa versions against settings values.
    ''' </summary>
    ''' <remarks> David, 2020-04-12. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Overrides Function ValidateFunctionalVisaVersions() As (Success As Boolean, Details As String)
        Return VisaVersionValidator.ValidateFunctionalVisaVersions
    End Function

    ''' <summary> Validates the visa assembly versions against settings values. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Overrides Function ValidateVisaAssemblyVersions() As (Success As Boolean, Details As String)
        Return VisaVersionValidator.ValidateVisaAssemblyVersions
    End Function

#End Region

End Class

