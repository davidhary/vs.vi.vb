''' <summary> Form for viewing the split visa view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-01-03 </para>
''' </remarks>
Public Class SplitVisaViewForm
    Inherits isr.Core.Forma.ConsoleForm

#Region " CONSTRUCTION and CLEAN UP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.Name = "Visa.View.Form"
    End Sub

    ''' <summary> Creates a new SplitVisaViewForm. </summary>
    ''' <returns> A SplitVisaViewForm. </returns>
    Public Shared Function Create() As SplitVisaViewForm
        Dim SplitVisaView As SplitVisaView = Nothing
        Try
            SplitVisaView = New SplitVisaView()
            Return SplitVisaViewForm.Create(SplitVisaView)
        Catch
            If SplitVisaView IsNot Nothing Then
                SplitVisaView.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary> Creates a new SplitVisaViewForm. </summary>
    ''' <param name="splitVisaView"> The visa view. </param>
    ''' <returns> A SplitVisaViewForm. </returns>
    Public Shared Function Create(ByVal splitVisaView As SplitVisaView) As SplitVisaViewForm
        Dim result As SplitVisaViewForm = Nothing
        Try
            result = New SplitVisaViewForm With {.SplitVisaView = splitVisaView}
        Catch
            If result IsNot Nothing Then
                result.Dispose()
            End If

            Throw
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.VisaViewDisposeEnabled AndAlso Me.SplitVisaView IsNot Nothing Then
                    Me.SplitVisaView.Dispose()
                    If Me._DisplayView IsNot Nothing Then Me._DisplayView.Dispose()
                    If Me._SessionStatusView IsNot Nothing Then Me._SessionStatusView.Dispose()
                End If
                Me._SplitVisaView = Nothing
                Me._DisplayView = Nothing
                Me._SessionStatusView = Nothing
                If Me.VisaSessionBase IsNot Nothing Then Me.VisaSessionBase.Dispose() : Me.VisaSessionBase = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding Visa View"
            ' the talker control publishes the device messages which thus get published to the form message box.
            Me.AddTalkerControl(Me.SplitVisaView.VisaSessionBase.CandidateResourceTitle, Me.SplitVisaView, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.MyLibrary.Logger)
            If Not String.IsNullOrWhiteSpace(Me.SplitVisaView.VisaSessionBase.CandidateResourceName) Then
                activity = $"{Me.Name}; starting {Me.SplitVisaView.VisaSessionBase.CandidateResourceName} selection task"
                Me.SplitVisaView.VisaSessionBase.AsyncValidateResourceName(Me.SplitVisaView.VisaSessionBase.CandidateResourceName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await the resource name validation task enabled. </summary>
    ''' <value> The await the resource name validation task enabled. </value>
    Protected Property AwaitResourceNameValidationTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.SplitVisaView.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)
            If Me.SplitVisaView.VisaSessionBase.IsValidatingResourceName Then
                If Me.AwaitResourceNameValidationTaskEnabled Then
                    activity = $"{Me.Name}; awaiting {Me.SplitVisaView.VisaSessionBase.CandidateResourceName} validation"
                    Me.SplitVisaView.VisaSessionBase.AwaitResourceNameValidation(My.Settings.ResourceNameSelectionTimeout)
                Else
                    activity = $"{Me.Name}; validating {Me.SplitVisaView.VisaSessionBase.CandidateResourceName}"
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.SplitVisaView IsNot Nothing Then Me.SplitVisaView.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " VISA "

    ''' <summary> Gets or sets the locally assigned visa session if any. </summary>
    ''' <value> The visa session. </value>
    Protected Property VisaSessionBase As VisaSessionBase

    ''' <summary> Gets or sets the display view. </summary>
    ''' <value> The display view. </value>
    Private Property DisplayView As DisplayView

    ''' <summary> Gets or sets the session status view. </summary>
    ''' <value> The session status view. </value>
    Private Property SessionStatusView As SessionStatusView

    ''' <summary> Gets or sets the visa view. </summary>
    ''' <value> The visa view. </value>
    Public Property SplitVisaView As isr.VI.Facade.SplitVisaView

    ''' <summary> Gets or sets the visa view dispose enabled. </summary>
    ''' <value> The visa view dispose enabled. </value>
    Public Property VisaViewDisposeEnabled As Boolean

    ''' <summary> Creates split view. </summary>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    Private Sub CreateSplitView(ByVal candidateResourceName As String, ByVal candidateResourceTitle As String)
        Me.VisaViewDisposeEnabled = True
        Me.VisaSessionBase = New VisaSession
        Me.SplitVisaView = New isr.VI.Facade.SplitVisaView()
        Me.DisplayView = New isr.VI.Facade.DisplayView
        Me.DisplayView.BindVisaSessionBase(Me.VisaSessionBase)
        Me.SessionStatusView = New isr.VI.Facade.SessionStatusView
        Me.SessionStatusView.BindVisaSessionBase(Me.VisaSessionBase)
        Me.SplitVisaView.BindVisaSessionBase(Me.VisaSessionBase)
        Me.SplitVisaView.AddHeader(Me.DisplayView)
        Me.SplitVisaView.AddNode("Session", "Session", Me.SessionStatusView)
        Me.SplitVisaView.VisaSessionBase.CandidateResourceTitle = candidateResourceTitle
        Me.SplitVisaView.VisaSessionBase.CandidateResourceName = candidateResourceName
    End Sub

#End Region

#Region " SHOW DIALOG "

    ''' <summary> Shows the form dialog. </summary>
    ''' <param name="owner">                  The owner. </param>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal candidateResourceName As String, ByVal candidateResourceTitle As String) As Windows.Forms.DialogResult
        Dim SplitVisaView As isr.VI.Facade.SplitVisaView = Nothing
        Try
            Me.CreateSplitView(candidateResourceName, candidateResourceTitle)
            Return Me.ShowDialog(owner, Me.SplitVisaView)
        Catch
            If SplitVisaView IsNot Nothing Then
                SplitVisaView.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary> Shows the form dialog. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="splitVisaView"> The visa view. </param>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal splitVisaView As isr.VI.Facade.SplitVisaView) As Windows.Forms.DialogResult
        Me.SplitVisaView = splitVisaView
        Return MyBase.ShowDialog(owner)
    End Function

    ''' <summary> Shows the form. </summary>
    ''' <param name="owner">                  The owner. </param>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal candidateResourceName As String, ByVal candidateResourceTitle As String)
        Dim SplitVisaView As isr.VI.Facade.SplitVisaView = Nothing
        Try
            Me.CreateSplitView(candidateResourceName, candidateResourceTitle)
            Me.Show(owner, Me.SplitVisaView)
        Catch
            If SplitVisaView IsNot Nothing Then
                SplitVisaView.Dispose()
            End If

            Throw
        End Try
    End Sub

    ''' <summary> Shows the form. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="splitVisaView"> The visa view. </param>
    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal splitVisaView As isr.VI.Facade.SplitVisaView)
        Me.SplitVisaView = splitVisaView
        MyBase.Show(owner)
    End Sub

#End Region

End Class
