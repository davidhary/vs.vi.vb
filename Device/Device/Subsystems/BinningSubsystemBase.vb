''' <summary> Defines a Scpi Binning Subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-15, 1.0.1841.x. </para>
''' </remarks>
Public MustInherit Class BinningSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BinningSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefineFeedSourceReadWrites()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.BinningStrobeDuration = TimeSpan.Zero
        Me.FeedSource = VI.FeedSources.Voltage
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <remarks> SCPI: ":CALC3:IMM". </remarks>
    ''' <value> The Immediate command. </value>
    Protected Overridable Property ImmediateCommand As String

    ''' <summary> Immediately calculate limits. </summary>
    Public Sub Immediate()
        Me.Session.Execute(Me.ImmediateCommand)
    End Sub

    ''' <summary> Gets or sets the limit 1 clear command. </summary>
    ''' <value> The limit 1 clear command. </value>
    Protected Overridable Property Limit1ClearCommand As String

    ''' <summary> Clear limit 1. </summary>
    Public Sub Limit1Clear()
        Me.Session.Execute(Me.Limit1ClearCommand)
    End Sub

    ''' <summary> Gets or sets the limit 2 clear command. </summary>
    ''' <value> The limit 2 clear command. </value>
    Protected Overridable Property Limit2ClearCommand As String

    ''' <summary> Clear limit 2. </summary>
    Public Sub Limit2Clear()
        Me.Session.Execute(Me.Limit2ClearCommand)
    End Sub

#End Region

#Region " DIGITAL I/O: FORCED DIGITAL OUTPUT PATTERN ENABLED "

    ''' <summary> The forced digital output pattern enabled. </summary>
    Private _ForcedDigitalOutputPatternEnabled As Boolean?

    ''' <summary> Gets or sets the cached Forced Digital Output Pattern Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Forced Digital Output Pattern Enabled is not known; <c>True</c> if output is
    ''' on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property ForcedDigitalOutputPatternEnabled As Boolean?
        Get
            Return Me._ForcedDigitalOutputPatternEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ForcedDigitalOutputPatternEnabled, value) Then
                Me._ForcedDigitalOutputPatternEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Forced Digital Output Pattern Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyForcedDigitalOutputPatternEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteForcedDigitalOutputPatternEnabled(value)
        Return Me.QueryForcedDigitalOutputPatternEnabled()
    End Function

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <remarks> SCPI: ":CALC3:FORC:STAT?". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property ForcedDigitalOutputPatternEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Forced Digital Output Pattern Enabled sentinel. Also sets the
    ''' <see cref="ForcedDigitalOutputPatternEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryForcedDigitalOutputPatternEnabled() As Boolean?
        Me.ForcedDigitalOutputPatternEnabled = Me.Query(Me.ForcedDigitalOutputPatternEnabled, Me.ForcedDigitalOutputPatternEnabledQueryCommand)
        Return Me.ForcedDigitalOutputPatternEnabled
    End Function

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <remarks> SCPI: ":CALC3:FORC:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property ForcedDigitalOutputPatternEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Forced Digital Output Pattern Enabled sentinel. Does not read back from the
    ''' instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteForcedDigitalOutputPatternEnabled(ByVal value As Boolean) As Boolean?
        Me.ForcedDigitalOutputPatternEnabled = Me.Write(value, Me.ForcedDigitalOutputPatternEnabledCommandFormat)
        Return Me.ForcedDigitalOutputPatternEnabled
    End Function

#End Region

#Region " DIGITAL I/O: FORCED DIGITAL OUTPUT PATTERN "

    ''' <summary> A pattern specifying the forced digital output. </summary>
    Private _ForcedDigitalOutputPattern As Integer?

    ''' <summary> Gets or sets the cached Forced Digital Output Pattern. </summary>
    ''' <value> The Forced Digital Output Pattern or none if not set or unknown. </value>
    Public Overloads Property ForcedDigitalOutputPattern As Integer?
        Get
            Return Me._ForcedDigitalOutputPattern
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.ForcedDigitalOutputPattern, value) Then
                Me._ForcedDigitalOutputPattern = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Forced Digital Output Pattern. </summary>
    ''' <param name="value"> The current Forced Digital Output Pattern. </param>
    ''' <returns> The Forced Digital Output Pattern or none if unknown. </returns>
    Public Function ApplyForcedDigitalOutputPattern(ByVal value As Integer) As Integer?
        Me.WriteForcedDigitalOutputPattern(value)
        Return Me.QueryForcedDigitalOutputPattern()
    End Function

    ''' <summary> Gets or sets the forced digital output Pattern query command. </summary>
    ''' <remarks> SCPI ":CALC3:FORC:PATT". </remarks>
    ''' <value> The forced digital output Pattern query command. </value>
    Protected Overridable Property ForcedDigitalOutputPatternQueryCommand As String

    ''' <summary> Queries the current Forced Digital Output Pattern. </summary>
    ''' <returns> The Forced Digital Output Pattern or none if unknown. </returns>
    Public Function QueryForcedDigitalOutputPattern() As Integer?
        Me.ForcedDigitalOutputPattern = Me.Query(Me.ForcedDigitalOutputPattern, Me.ForcedDigitalOutputPatternQueryCommand)
        Return Me.ForcedDigitalOutputPattern

    End Function

    ''' <summary> Gets or sets the forced digital output Pattern command format. </summary>
    ''' <remarks> SCPI ":CALC3:FORC:PATT". </remarks>
    ''' <value> The forced digital output Pattern command format. </value>
    Protected Overridable Property ForcedDigitalOutputPatternCommandFormat As String

    ''' <summary>
    ''' Sets back the Forced Digital Output Pattern without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current Forced Digital Output Pattern. </param>
    ''' <returns> The Forced Digital Output Pattern or none if unknown. </returns>
    Public Function WriteForcedDigitalOutputPattern(ByVal value As Integer) As Integer?
        Me.ForcedDigitalOutputPattern = Me.Write(value, Me.ForcedDigitalOutputPatternCommandFormat)
        Return Me.ForcedDigitalOutputPattern
    End Function

#End Region

#Region " BINNING STROBE "

    ''' <summary> Duration of the binning strobe. </summary>
    Private _BinningStrobeDuration As TimeSpan

    ''' <summary> Gets or sets the duration of the binning strobe. </summary>
    ''' <value> The binning strobe duration. </value>
    Public Property BinningStrobeDuration As TimeSpan
        Get
            Return Me._BinningStrobeDuration
        End Get
        Set(value As TimeSpan)
            If value <> Me.BinningStrobeDuration Then
                Me._BinningStrobeDuration = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Binning Strobe Enabled status. </summary>
    Private _BinningStrobeEnabled As Boolean?

    ''' <summary> Gets or sets the cached status of outputting a Binning strobe. </summary>
    ''' <value>
    ''' <c>null</c> if a Binning Strobe Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property BinningStrobeEnabled As Boolean?
        Get
            Return Me._BinningStrobeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.BinningStrobeEnabled, value) Then
                Me._BinningStrobeEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Binning Strobe Enabled status. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyBinningStrobeEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteBinningStrobeEnabled(value)
        Return Me.QueryBinningStrobeEnabled()
    End Function

    ''' <summary> Gets or sets the Binning Strobe Enabled status query command. </summary>
    ''' <remarks> SCPI: ":CALC3:BSTR:STAT?". </remarks>
    ''' <value> The Binning enabled query command. </value>
    Protected Overridable Property BinningStrobeEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Binning Strobe Enabled status. Also sets the
    ''' <see cref="BinningStrobeEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryBinningStrobeEnabled() As Boolean?
        Me.BinningStrobeEnabled = Me.Query(Me.BinningStrobeEnabled, Me.BinningStrobeEnabledQueryCommand)
        Return Me.BinningStrobeEnabled
    End Function

    ''' <summary> Gets or sets the Binning Strobe Enabled status command Format. </summary>
    ''' <remarks> SCPI: ":CALC3:BSTR:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Binning enabled query command. </value>
    Protected Overridable Property BinningStrobeEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Binning Strobe Enabled status. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteBinningStrobeEnabled(ByVal value As Boolean) As Boolean?
        Me.BinningStrobeEnabled = Me.Write(value, Me.BinningStrobeEnabledCommandFormat)
        Return Me.BinningStrobeEnabled
    End Function

#End Region

#Region " FEED SOURCE "

    ''' <summary> Define feed source read writes. </summary>
    Private Sub DefineFeedSourceReadWrites()
        Me._FeedSourceReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As FeedSources In [Enum].GetValues(GetType(FeedSources))
            Me._FeedSourceReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Feed source parses. </summary>
    ''' <value> A Dictionary of Feed source parses. </value>
    Public ReadOnly Property FeedSourceReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported feed sources. </summary>
    Private _SupportedFeedSources As FeedSources

    ''' <summary> Gets or sets the supported Feed sources. </summary>
    ''' <value> The supported Feed sources. </value>
    Public Property SupportedFeedSources() As FeedSources
        Get
            Return Me._SupportedFeedSources
        End Get
        Set(ByVal value As FeedSources)
            If Not Me.SupportedFeedSources.Equals(value) Then
                Me._SupportedFeedSources = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The feed source. </summary>
    Private _FeedSource As FeedSources?

    ''' <summary> Gets or sets the cached source FeedSource. </summary>
    ''' <value>
    ''' The <see cref="FeedSource">source Feed Source</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property FeedSource As FeedSources?
        Get
            Return Me._FeedSource
        End Get
        Protected Set(ByVal value As FeedSources?)
            If Not Me.FeedSource.Equals(value) Then
                Me._FeedSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Feed Source. </summary>
    ''' <param name="value"> The  Source Feed Source. </param>
    ''' <returns> The <see cref="FeedSource">source Feed Source</see> or none if unknown. </returns>
    Public Function ApplyFeedSource(ByVal value As FeedSources) As FeedSources?
        Me.WriteFeedSource(value)
        Return Me.QueryFeedSource()
    End Function

    ''' <summary> Gets or sets the Feed source query command. </summary>
    ''' <remarks> SCPI: "?". </remarks>
    ''' <value> The Feed source query command. </value>
    Protected Overridable Property FeedSourceQueryCommand As String

    ''' <summary> Queries the Feed source. </summary>
    ''' <returns> The <see cref="FeedSource">Feed source</see> or none if unknown. </returns>
    Public Function QueryFeedSource() As FeedSources?
        Me.FeedSource = Me.Query(Of FeedSources)(Me.FeedSourceQueryCommand,
                                                       Me.FeedSource.GetValueOrDefault(FeedSources.None),
                                                       Me.FeedSourceReadWrites)
        Return Me.FeedSource
    End Function

    ''' <summary> Gets or sets the Feed source command format. </summary>
    ''' <remarks> SCPI: " {0}". </remarks>
    ''' <value> The write Feed source command format. </value>
    Protected Overridable Property FeedSourceCommandFormat As String

    ''' <summary> Writes the Feed Source without reading back the value from the device. </summary>
    ''' <param name="value"> The Feed Source. </param>
    ''' <returns> The <see cref="FeedSource">Feed Source</see> or none if unknown. </returns>
    Public Function WriteFeedSource(ByVal value As FeedSources) As FeedSources?
        Me.FeedSource = Me.Write(Of FeedSources)(Me.FeedSourceCommandFormat, value, Me.FeedSourceReadWrites)
        Return Me.FeedSource
    End Function

#End Region

#Region " LIMITS FAILED "

    ''' <summary> Limits Failed. </summary>
    Private _LimitsFailed As Boolean?

    ''' <summary> Gets or sets the cached Limits Failed sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limits Failed is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property LimitsFailed As Boolean?
        Get
            Return Me._LimitsFailed
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.LimitsFailed, value) Then
                Me._LimitsFailed = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Limits Failed query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM1:FAIL?". </remarks>
    ''' <value> The Limits Failed query command. </value>
    Protected Overridable Property LimitsFailedQueryCommand As String

    ''' <summary>
    ''' Queries the Limits Failed sentinel. Also sets the
    ''' <see cref="LimitsFailed">Failed</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
    Public Function QueryLimitsFailed() As Boolean?
        Me.LimitsFailed = Me.Query(Me.LimitsFailed, Me.LimitsFailedQueryCommand)
        Return Me.LimitsFailed
    End Function

#End Region

#Region " PASS SOURCE "

    ''' <summary> The Pass Source. </summary>
    Private _PassSource As Integer?

    ''' <summary> Gets or sets the cached Pass Source. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property PassSource As Integer?
        Get
            Return Me._PassSource
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.PassSource, value) Then
                Me._PassSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Pass Source. </summary>
    ''' <param name="value"> The Pass Source. </param>
    ''' <returns> The Pass Source. </returns>
    Public Function ApplyPassSource(ByVal value As Integer) As Integer?
        Me.WritePassSource(value)
        Return Me.QueryPassSource
    End Function

    ''' <summary> Gets or sets The Pass Source query command. </summary>
    ''' <value> The Pass Source query command. </value>
    Protected Overridable Property PassSourceQueryCommand As String

    ''' <summary> Queries The Pass Source. </summary>
    ''' <returns> The Pass Source or none if unknown. </returns>
    Public Function QueryPassSource() As Integer?
        Me.PassSource = Me.Query(Me.PassSource, Me.PassSourceQueryCommand)
        Return Me.PassSource
    End Function

    ''' <summary> Gets or sets The Pass Source command format. </summary>
    ''' <value> The Pass Source command format. </value>
    Protected Overridable Property PassSourceCommandFormat As String

    ''' <summary> Writes The Pass Source without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Pass Source. </remarks>
    ''' <param name="value"> The Pass Source. </param>
    ''' <returns> The Pass Source. </returns>
    Public Function WritePassSource(ByVal value As Integer) As Integer?
        Me.PassSource = Me.Write(value, Me.PassSourceCommandFormat)
        Return Me.PassSource
    End Function

#End Region

#Region " LIMIT 1 "

#Region " LIMIT1 ENABLED "

    ''' <summary> Limit1 enabled. </summary>
    Private _Limit1Enabled As Boolean?

    ''' <summary> Gets or sets the cached Limit1 Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit1 Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit1Enabled As Boolean?
        Get
            Return Me._Limit1Enabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit1Enabled, value) Then
                Me._Limit1Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit1Enabled(ByVal value As Boolean) As Boolean?
        Me.WriteLimit1Enabled(value)
        Return Me.QueryLimit1Enabled()
    End Function

    ''' <summary> Gets or sets the Limit1 enabled query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM1:STAT?". </remarks>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overridable Property Limit1EnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Limit1 Enabled sentinel. Also sets the
    ''' <see cref="Limit1Enabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryLimit1Enabled() As Boolean?
        Me.Limit1Enabled = Me.Query(Me.Limit1Enabled, Me.Limit1EnabledQueryCommand)
        Return Me.Limit1Enabled
    End Function

    ''' <summary> Gets or sets the Limit1 enabled command Format. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM1:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overridable Property Limit1EnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Limit1 Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteLimit1Enabled(ByVal value As Boolean) As Boolean?
        Me.Limit1Enabled = Me.Write(value, Me.Limit1EnabledCommandFormat)
        Return Me.Limit1Enabled
    End Function

#End Region

#Region " LIMIT1 FAILED "

    ''' <summary> Limit1 Failed. </summary>
    Private _Limit1Failed As Boolean?

    ''' <summary> Gets or sets the cached Limit1 Failed sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit1 Failed is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit1Failed As Boolean?
        Get
            Return Me._Limit1Failed
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit1Failed, value) Then
                Me._Limit1Failed = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Limit1 Failed query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM1:FAIL?". </remarks>
    ''' <value> The Limit1 Failed query command. </value>
    Protected Overridable Property Limit1FailedQueryCommand As String

    ''' <summary>
    ''' Queries the Limit1 Failed sentinel. Also sets the
    ''' <see cref="Limit1Failed">Failed</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
    Public Function QueryLimit1Failed() As Boolean?
        Me.Limit1Failed = Me.Query(Me.Limit1Failed, Me.Limit1FailedQueryCommand)
        Return Me.Limit1Failed
    End Function

#End Region

#Region " LIMIT1 LOWER LEVEL "

    ''' <summary> The Limit1 Lower Level. </summary>
    Private _Limit1LowerLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit1 Lower Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit1LowerLevel As Double?
        Get
            Return Me._Limit1LowerLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit1LowerLevel, value) Then
                Me._Limit1LowerLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Lower Level. </summary>
    ''' <param name="value"> The Limit1 Lower Level. </param>
    ''' <returns> The Limit1 Lower Level. </returns>
    Public Function ApplyLimit1LowerLevel(ByVal value As Double) As Double?
        Me.WriteLimit1LowerLevel(value)
        Return Me.QueryLimit1LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Lower Level query command. </summary>
    ''' <value> The Limit1 Lower Level query command. </value>
    Protected Overridable Property Limit1LowerLevelQueryCommand As String

    ''' <summary> Queries The Limit1 Lower Level. </summary>
    ''' <returns> The Limit1 Lower Level or none if unknown. </returns>
    Public Function QueryLimit1LowerLevel() As Double?
        Me.Limit1LowerLevel = Me.Query(Me.Limit1LowerLevel, Me.Limit1LowerLevelQueryCommand)
        Return Me.Limit1LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Lower Level command format. </summary>
    ''' <value> The Limit1 Lower Level command format. </value>
    Protected Overridable Property Limit1LowerLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit1 Lower Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit1 Lower Level. </remarks>
    ''' <param name="value"> The Limit1 Lower Level. </param>
    ''' <returns> The Limit1 Lower Level. </returns>
    Public Function WriteLimit1LowerLevel(ByVal value As Double) As Double?
        Me.Limit1LowerLevel = Me.Write(value, Me.Limit1LowerLevelCommandFormat)
        Return Me.Limit1LowerLevel
    End Function

#End Region

#Region " LIMIT1 UPPER LEVEL "

    ''' <summary> The Limit1 Upper Level. </summary>
    Private _Limit1UpperLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit1 Upper Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit1UpperLevel As Double?
        Get
            Return Me._Limit1UpperLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit1UpperLevel, value) Then
                Me._Limit1UpperLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Upper Level. </summary>
    ''' <param name="value"> The Limit1 Upper Level. </param>
    ''' <returns> The Limit1 Upper Level. </returns>
    Public Function ApplyLimit1UpperLevel(ByVal value As Double) As Double?
        Me.WriteLimit1UpperLevel(value)
        Return Me.QueryLimit1UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Upper Level query command. </summary>
    ''' <value> The Limit1 Upper Level query command. </value>
    Protected Overridable Property Limit1UpperLevelQueryCommand As String

    ''' <summary> Queries The Limit1 Upper Level. </summary>
    ''' <returns> The Limit1 Upper Level or none if unknown. </returns>
    Public Function QueryLimit1UpperLevel() As Double?
        Me.Limit1UpperLevel = Me.Query(Me.Limit1UpperLevel, Me.Limit1UpperLevelQueryCommand)
        Return Me.Limit1UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Upper Level command format. </summary>
    ''' <value> The Limit1 Upper Level command format. </value>
    Protected Overridable Property Limit1UpperLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit1 Upper Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit1 Upper Level. </remarks>
    ''' <param name="value"> The Limit1 Upper Level. </param>
    ''' <returns> The Limit1 Upper Level. </returns>
    Public Function WriteLimit1UpperLevel(ByVal value As Double) As Double?
        Me.Limit1UpperLevel = Me.Write(value, Me.Limit1UpperLevelCommandFormat)
        Return Me.Limit1UpperLevel
    End Function

#End Region

#Region " LIMIT1 AUTO CLEAR "

    ''' <summary> Limit1 Auto Clear. </summary>
    Private _Limit1AutoClear As Boolean?

    ''' <summary> Gets or sets the cached Limit1 Auto Clear sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit1 Auto Clear is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit1AutoClear As Boolean?
        Get
            Return Me._Limit1AutoClear
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit1AutoClear, value) Then
                Me._Limit1AutoClear = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Auto Clear sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit1AutoClear(ByVal value As Boolean) As Boolean?
        Me.WriteLimit1AutoClear(value)
        Return Me.QueryLimit1AutoClear()
    End Function

    ''' <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM1:STAT?". </remarks>
    ''' <value> The Limit1 Auto Clear query command. </value>
    Protected Overridable Property Limit1AutoClearQueryCommand As String

    ''' <summary>
    ''' Queries the Limit1 Auto Clear sentinel. Also sets the
    ''' <see cref="Limit1AutoClear">AutoClear</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function QueryLimit1AutoClear() As Boolean?
        Me.Limit1AutoClear = Me.Query(Me.Limit1AutoClear, Me.Limit1AutoClearQueryCommand)
        Return Me.Limit1AutoClear
    End Function

    ''' <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM1:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit1 Auto Clear query command. </value>
    Protected Overridable Property Limit1AutoClearCommandFormat As String

    ''' <summary>
    ''' Writes the Limit1 Auto Clear sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is Auto Clear. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function WriteLimit1AutoClear(ByVal value As Boolean) As Boolean?
        Me.Limit1AutoClear = Me.Write(value, Me.Limit1AutoClearCommandFormat)
        Return Me.Limit1AutoClear
    End Function

#End Region

#Region " LIMIT1 LOWER SOURCE "

    ''' <summary> The Limit1 Lower Source. </summary>
    Private _Limit1LowerSource As Integer?

    ''' <summary> Gets or sets the cached Limit1 Lower Source. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit1LowerSource As Integer?
        Get
            Return Me._Limit1LowerSource
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Limit1LowerSource, value) Then
                Me._Limit1LowerSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Lower Source. </summary>
    ''' <param name="value"> The Limit1 Lower Source. </param>
    ''' <returns> The Limit1 Lower Source. </returns>
    Public Function ApplyLimit1LowerSource(ByVal value As Integer) As Integer?
        Me.WriteLimit1LowerSource(value)
        Return Me.QueryLimit1LowerSource
    End Function

    ''' <summary> Gets or sets The Limit1 Lower Source query command. </summary>
    ''' <value> The Limit1 Lower Source query command. </value>
    Protected Overridable Property Limit1LowerSourceQueryCommand As String

    ''' <summary> Queries The Limit1 Lower Source. </summary>
    ''' <returns> The Limit1 Lower Source or none if unknown. </returns>
    Public Function QueryLimit1LowerSource() As Integer?
        Me.Limit1LowerSource = Me.Query(Me.Limit1LowerSource, Me.Limit1LowerSourceQueryCommand)
        Return Me.Limit1LowerSource
    End Function

    ''' <summary> Gets or sets The Limit1 Lower Source command format. </summary>
    ''' <value> The Limit1 Lower Source command format. </value>
    Protected Overridable Property Limit1LowerSourceCommandFormat As String

    ''' <summary>
    ''' Writes The Limit1 Lower Source without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit1 Lower Source. </remarks>
    ''' <param name="value"> The Limit1 Lower Source. </param>
    ''' <returns> The Limit1 Lower Source. </returns>
    Public Function WriteLimit1LowerSource(ByVal value As Integer) As Integer?
        Me.Limit1LowerSource = Me.Write(value, Me.Limit1LowerSourceCommandFormat)
        Return Me.Limit1LowerSource
    End Function

#End Region

#Region " LIMIT1 UPPER SOURCE "

    ''' <summary> The Limit1 Upper Source. </summary>
    Private _Limit1UpperSource As Integer?

    ''' <summary> Gets or sets the cached Limit1 Upper Source. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit1UpperSource As Integer?
        Get
            Return Me._Limit1UpperSource
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Limit1UpperSource, value) Then
                Me._Limit1UpperSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Upper Source. </summary>
    ''' <param name="value"> The Limit1 Upper Source. </param>
    ''' <returns> The Limit1 Upper Source. </returns>
    Public Function ApplyLimit1UpperSource(ByVal value As Integer) As Integer?
        Me.WriteLimit1UpperSource(value)
        Return Me.QueryLimit1UpperSource
    End Function

    ''' <summary> Gets or sets The Limit1 Upper Source query command. </summary>
    ''' <value> The Limit1 Upper Source query command. </value>
    Protected Overridable Property Limit1UpperSourceQueryCommand As String

    ''' <summary> Queries The Limit1 Upper Source. </summary>
    ''' <returns> The Limit1 Upper Source or none if unknown. </returns>
    Public Function QueryLimit1UpperSource() As Integer?
        Me.Limit1UpperSource = Me.Query(Me.Limit1UpperSource, Me.Limit1UpperSourceQueryCommand)
        Return Me.Limit1UpperSource
    End Function

    ''' <summary> Gets or sets The Limit1 Upper Source command format. </summary>
    ''' <value> The Limit1 Upper Source command format. </value>
    Protected Overridable Property Limit1UpperSourceCommandFormat As String

    ''' <summary>
    ''' Writes The Limit1 Upper Source without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit1 Upper Source. </remarks>
    ''' <param name="value"> The Limit1 Upper Source. </param>
    ''' <returns> The Limit1 Upper Source. </returns>
    Public Function WriteLimit1UpperSource(ByVal value As Integer) As Integer?
        Me.Limit1UpperSource = Me.Write(value, Me.Limit1UpperSourceCommandFormat)
        Return Me.Limit1UpperSource
    End Function

#End Region

#End Region

#Region " LIMIT 2 "

#Region " LIMIT2 ENABLED "

    ''' <summary> Limit2 enabled. </summary>
    Private _Limit2Enabled As Boolean?

    ''' <summary> Gets or sets the cached Limit2 Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit2 Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit2Enabled As Boolean?
        Get
            Return Me._Limit2Enabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit2Enabled, value) Then
                Me._Limit2Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit2Enabled(ByVal value As Boolean) As Boolean?
        Me.WriteLimit2Enabled(value)
        Return Me.QueryLimit2Enabled()
    End Function

    ''' <summary> Gets or sets the Limit2 enabled query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM2:STAT?". </remarks>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overridable Property Limit2EnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Limit2 Enabled sentinel. Also sets the
    ''' <see cref="Limit2Enabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryLimit2Enabled() As Boolean?
        Me.Limit2Enabled = Me.Query(Me.Limit2Enabled, Me.Limit2EnabledQueryCommand)
        Return Me.Limit2Enabled
    End Function

    ''' <summary> Gets or sets the Limit2 enabled command Format. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM2:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overridable Property Limit2EnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Limit2 Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteLimit2Enabled(ByVal value As Boolean) As Boolean?
        Me.Limit2Enabled = Me.Write(value, Me.Limit2EnabledCommandFormat)
        Return Me.Limit2Enabled
    End Function

#End Region

#Region " LIMIT2 FAILED "

    ''' <summary> Limit2 Failed. </summary>
    Private _Limit2Failed As Boolean?

    ''' <summary> Gets or sets the cached Limit2 Failed sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit2 Failed is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit2Failed As Boolean?
        Get
            Return Me._Limit2Failed
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit2Failed, value) Then
                Me._Limit2Failed = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Limit2 Failed query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM2:FAIL?". </remarks>
    ''' <value> The Limit2 Failed query command. </value>
    Protected Overridable Property Limit2FailedQueryCommand As String

    ''' <summary>
    ''' Queries the Limit2 Failed sentinel. Also sets the
    ''' <see cref="Limit2Failed">Failed</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
    Public Function QueryLimit2Failed() As Boolean?
        Me.Limit2Failed = Me.Query(Me.Limit2Failed, Me.Limit2FailedQueryCommand)
        Return Me.Limit2Failed
    End Function

#End Region

#Region " LIMIT2 LOWER LEVEL "

    ''' <summary> The Limit2 Lower Level. </summary>
    Private _Limit2LowerLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit2 Lower Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit2LowerLevel As Double?
        Get
            Return Me._Limit2LowerLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit2LowerLevel, value) Then
                Me._Limit2LowerLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Lower Level. </summary>
    ''' <param name="value"> The Limit2 Lower Level. </param>
    ''' <returns> The Limit2 Lower Level. </returns>
    Public Function ApplyLimit2LowerLevel(ByVal value As Double) As Double?
        Me.WriteLimit2LowerLevel(value)
        Return Me.QueryLimit2LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Lower Level query command. </summary>
    ''' <value> The Limit2 Lower Level query command. </value>
    Protected Overridable Property Limit2LowerLevelQueryCommand As String

    ''' <summary> Queries The Limit2 Lower Level. </summary>
    ''' <returns> The Limit2 Lower Level or none if unknown. </returns>
    Public Function QueryLimit2LowerLevel() As Double?
        Me.Limit2LowerLevel = Me.Query(Me.Limit2LowerLevel, Me.Limit2LowerLevelQueryCommand)
        Return Me.Limit2LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Lower Level command format. </summary>
    ''' <value> The Limit2 Lower Level command format. </value>
    Protected Overridable Property Limit2LowerLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit2 Lower Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit2 Lower Level. </remarks>
    ''' <param name="value"> The Limit2 Lower Level. </param>
    ''' <returns> The Limit2 Lower Level. </returns>
    Public Function WriteLimit2LowerLevel(ByVal value As Double) As Double?
        Me.Limit2LowerLevel = Me.Write(value, Me.Limit2LowerLevelCommandFormat)
        Return Me.Limit2LowerLevel
    End Function

#End Region

#Region " LIMIT2 UPPER LEVEL "

    ''' <summary> The Limit2 Upper Level. </summary>
    Private _Limit2UpperLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit2 Upper Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit2UpperLevel As Double?
        Get
            Return Me._Limit2UpperLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit2UpperLevel, value) Then
                Me._Limit2UpperLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Upper Level. </summary>
    ''' <param name="value"> The Limit2 Upper Level. </param>
    ''' <returns> The Limit2 Upper Level. </returns>
    Public Function ApplyLimit2UpperLevel(ByVal value As Double) As Double?
        Me.WriteLimit2UpperLevel(value)
        Return Me.QueryLimit2UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Upper Level query command. </summary>
    ''' <value> The Limit2 Upper Level query command. </value>
    Protected Overridable Property Limit2UpperLevelQueryCommand As String

    ''' <summary> Queries The Limit2 Upper Level. </summary>
    ''' <returns> The Limit2 Upper Level or none if unknown. </returns>
    Public Function QueryLimit2UpperLevel() As Double?
        Me.Limit2UpperLevel = Me.Query(Me.Limit2UpperLevel, Me.Limit2UpperLevelQueryCommand)
        Return Me.Limit2UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Upper Level command format. </summary>
    ''' <value> The Limit2 Upper Level command format. </value>
    Protected Overridable Property Limit2UpperLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit2 Upper Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit2 Upper Level. </remarks>
    ''' <param name="value"> The Limit2 Upper Level. </param>
    ''' <returns> The Limit2 Upper Level. </returns>
    Public Function WriteLimit2UpperLevel(ByVal value As Double) As Double?
        Me.Limit2UpperLevel = Me.Write(value, Me.Limit2UpperLevelCommandFormat)
        Return Me.Limit2UpperLevel
    End Function

#End Region

#Region " LIMIT2 AUTO CLEAR "

    ''' <summary> Limit2 Auto Clear. </summary>
    Private _Limit2AutoClear As Boolean?

    ''' <summary> Gets or sets the cached Limit2 Auto Clear sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit2 Auto Clear is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit2AutoClear As Boolean?
        Get
            Return Me._Limit2AutoClear
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit2AutoClear, value) Then
                Me._Limit2AutoClear = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Auto Clear sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit2AutoClear(ByVal value As Boolean) As Boolean?
        Me.WriteLimit2AutoClear(value)
        Return Me.QueryLimit2AutoClear()
    End Function

    ''' <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM2:STAT?". </remarks>
    ''' <value> The Limit2 Auto Clear query command. </value>
    Protected Overridable Property Limit2AutoClearQueryCommand As String

    ''' <summary>
    ''' Queries the Limit2 Auto Clear sentinel. Also sets the
    ''' <see cref="Limit2AutoClear">AutoClear</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function QueryLimit2AutoClear() As Boolean?
        Me.Limit2AutoClear = Me.Query(Me.Limit2AutoClear, Me.Limit2AutoClearQueryCommand)
        Return Me.Limit2AutoClear
    End Function

    ''' <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
    ''' <remarks> SCPI: ":CALC3:LIM2:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit2 Auto Clear query command. </value>
    Protected Overridable Property Limit2AutoClearCommandFormat As String

    ''' <summary>
    ''' Writes the Limit2 Auto Clear sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is Auto Clear. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function WriteLimit2AutoClear(ByVal value As Boolean) As Boolean?
        Me.Limit2AutoClear = Me.Write(value, Me.Limit2AutoClearCommandFormat)
        Return Me.Limit2AutoClear
    End Function

#End Region

#Region " LIMIT2 LOWER SOURCE "

    ''' <summary> The Limit2 Lower Source. </summary>
    Private _Limit2LowerSource As Integer?

    ''' <summary> Gets or sets the cached Limit2 Lower Source. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit2LowerSource As Integer?
        Get
            Return Me._Limit2LowerSource
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Limit2LowerSource, value) Then
                Me._Limit2LowerSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Lower Source. </summary>
    ''' <param name="value"> The Limit2 Lower Source. </param>
    ''' <returns> The Limit2 Lower Source. </returns>
    Public Function ApplyLimit2LowerSource(ByVal value As Integer) As Integer?
        Me.WriteLimit2LowerSource(value)
        Return Me.QueryLimit2LowerSource
    End Function

    ''' <summary> Gets or sets The Limit2 Lower Source query command. </summary>
    ''' <value> The Limit2 Lower Source query command. </value>
    Protected Overridable Property Limit2LowerSourceQueryCommand As String

    ''' <summary> Queries The Limit2 Lower Source. </summary>
    ''' <returns> The Limit2 Lower Source or none if unknown. </returns>
    Public Function QueryLimit2LowerSource() As Integer?
        Me.Limit2LowerSource = Me.Query(Me.Limit2LowerSource, Me.Limit2LowerSourceQueryCommand)
        Return Me.Limit2LowerSource
    End Function

    ''' <summary> Gets or sets The Limit2 Lower Source command format. </summary>
    ''' <value> The Limit2 Lower Source command format. </value>
    Protected Overridable Property Limit2LowerSourceCommandFormat As String

    ''' <summary>
    ''' Writes The Limit2 Lower Source without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit2 Lower Source. </remarks>
    ''' <param name="value"> The Limit2 Lower Source. </param>
    ''' <returns> The Limit2 Lower Source. </returns>
    Public Function WriteLimit2LowerSource(ByVal value As Integer) As Integer?
        Me.Limit2LowerSource = Me.Write(value, Me.Limit2LowerSourceCommandFormat)
        Return Me.Limit2LowerSource
    End Function

#End Region

#Region " LIMIT2 UPPER SOURCE "

    ''' <summary> The Limit2 Upper Source. </summary>
    Private _Limit2UpperSource As Integer?

    ''' <summary> Gets or sets the cached Limit2 Upper Source. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit2UpperSource As Integer?
        Get
            Return Me._Limit2UpperSource
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Limit2UpperSource, value) Then
                Me._Limit2UpperSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Upper Source. </summary>
    ''' <param name="value"> The Limit2 Upper Source. </param>
    ''' <returns> The Limit2 Upper Source. </returns>
    Public Function ApplyLimit2UpperSource(ByVal value As Integer) As Integer?
        Me.WriteLimit2UpperSource(value)
        Return Me.QueryLimit2UpperSource
    End Function

    ''' <summary> Gets or sets The Limit2 Upper Source query command. </summary>
    ''' <value> The Limit2 Upper Source query command. </value>
    Protected Overridable Property Limit2UpperSourceQueryCommand As String

    ''' <summary> Queries The Limit2 Upper Source. </summary>
    ''' <returns> The Limit2 Upper Source or none if unknown. </returns>
    Public Function QueryLimit2UpperSource() As Integer?
        Me.Limit2UpperSource = Me.Query(Me.Limit2UpperSource, Me.Limit2UpperSourceQueryCommand)
        Return Me.Limit2UpperSource
    End Function

    ''' <summary> Gets or sets The Limit2 Upper Source command format. </summary>
    ''' <value> The Limit2 Upper Source command format. </value>
    Protected Overridable Property Limit2UpperSourceCommandFormat As String

    ''' <summary>
    ''' Writes The Limit2 Upper Source without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit2 Upper Source. </remarks>
    ''' <param name="value"> The Limit2 Upper Source. </param>
    ''' <returns> The Limit2 Upper Source. </returns>
    Public Function WriteLimit2UpperSource(ByVal value As Integer) As Integer?
        Me.Limit2UpperSource = Me.Write(value, Me.Limit2UpperSourceCommandFormat)
        Return Me.Limit2UpperSource
    End Function

#End Region

#End Region


End Class


