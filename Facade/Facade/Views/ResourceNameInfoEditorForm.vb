''' <summary> Form for editing the resource name Information. </summary>
''' <remarks>
''' David, 2020-06-07. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class ResourceNameInfoEditorForm
    Inherits isr.Core.Forma.FadeFormBase

#Region " CONSTRUCTION and CLEAN UP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me.Name = "Resource.Editor.Form"
        Me.Text = "Resource Names Editor"
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
#Disable Warning IDE0059 ' Unnecessary assignment of a value
        Dim activity As String = String.Empty
#Enable Warning IDE0059 ' Unnecessary assignment of a value
        Try
        Catch ex As Exception
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await the resource name validation task enabled. </summary>
    ''' <value> The await the resource name validation task enabled. </value>
    Protected Property AwaitResourceNameValidationTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
#Disable Warning IDE0059 ' Unnecessary assignment of a value
        Dim activity As String = String.Empty
#Enable Warning IDE0059 ' Unnecessary assignment of a value
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            MyBase.OnShown(e)
        Catch ex As Exception
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

End Class
