'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\TraceSubsystemBase.vb
'
' summary:	Trace subsystem base class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.InteropServices.WindowsRuntime
Imports System.Threading

Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Defines the contract that must be implemented by a Trace Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class TraceSubsystemBase
    Inherits SubsystemPlusStatusBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TraceSubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefineFeedControlReadWrites()
        Me.DefineFeedSourceReadWrites()
        Me.BufferReadingsBindingList = New BufferReadingBindingList
        Me.BusTriggerRequestedToken = New isr.Core.Constructs.ConcurrentToken(Of Boolean)
        Me.BufferStreamingEnabledToken = New isr.Core.Constructs.ConcurrentToken(Of Boolean)
        Me.BufferStreamingActiveToken = New isr.Core.Constructs.ConcurrentToken(Of Boolean)
        Me.BufferStreamingAlertToken = New isr.Core.Constructs.ConcurrentToken(Of Boolean)
        Me.BinningStrobeRequestedToken = New isr.Core.Constructs.ConcurrentToken(Of Boolean)
        Me.NewBufferReadingsQueue = New Concurrent.ConcurrentQueue(Of BufferReading)
        Me.MeasuredElementType = ReadingElementTypes.Voltage
        Me._BufferStreamTasker = New isr.Core.Tasker
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                                                   resources;
    '''                                                   False if this method releases only unmanaged
    '''                                                   resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose()
                Me._ItemsLocker = Nothing
                If Me.BufferStreamingEnabledToken IsNot Nothing Then Me.BufferStreamingEnabledToken.Dispose()
                If Me.BufferStreamingActiveToken IsNot Nothing Then Me.BufferStreamingActiveToken.Dispose()
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PointsCount = 0
        Me.FeedSource = FeedSources.None
        Me.FeedControl = FeedControls.Never
        Me._BufferReadingsBindingList = New BufferReadingBindingList
        Me.ReadingElementTypes = TraceSubsystemBase.JoinReadingElementTypes(Me.OrderedReadingElementTypes)
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Clear Buffer command. </summary>
    ''' <remarks> SCPI: ":TRAC:CLE". </remarks>
    ''' <value> The ClearBuffer command. </value>
    Protected Overridable Property ClearBufferCommand As String

    ''' <summary> Clears the buffer. </summary>
    Public Sub ClearBuffer()
        Me.Write(Me.ClearBufferCommand)
    End Sub

#End Region

#Region " AUTO POINTS ENABLED "

    ''' <summary> Auto Points enabled. </summary>
    Private _AutoPointsEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Points Enabled sentinel. </summary>
    ''' <remarks>
    ''' When setting to auto points, the buffer points default to the trigger plan trigger count.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Auto Points Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Overridable Property AutoPointsEnabled As Boolean?
        Get
            Return Me._AutoPointsEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoPointsEnabled, value) Then
                Me._AutoPointsEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Points Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoPointsEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoPointsEnabled(value)
        Return Me.QueryAutoPointsEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Points enabled query command. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:AUTO?". </remarks>
    ''' <value> The automatic Points enabled query command. </value>
    Protected Overridable Property AutoPointsEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Points Enabled sentinel. Also sets the
    ''' <see cref="AutoPointsEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoPointsEnabled() As Boolean?
        Me.AutoPointsEnabled = Me.Query(Me.AutoPointsEnabled, Me.AutoPointsEnabledQueryCommand)
        Return Me.AutoPointsEnabled
    End Function

    ''' <summary> Gets or sets the automatic Points enabled command Format. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Points enabled query command. </value>
    Protected Overridable Property AutoPointsEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Points Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoPointsEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoPointsEnabled = Me.Write(value, Me.AutoPointsEnabledCommandFormat)
        Return Me.AutoPointsEnabled
    End Function

#End Region

#Region " BUFFER CAPACITY "

    ''' <summary> The maximum capacity. </summary>
    Private _MaximumCapacity As Integer?

    ''' <summary> Gets or sets the maximum Buffer Capacity. </summary>
    ''' <value> The Maximum Buffer Capacity or none if not set or unknown. </value>
    Public Overridable Property MaximumCapacity As Integer?
        Get
            Return Me._MaximumCapacity
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.MaximumCapacity, value) Then
                Me._MaximumCapacity = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " POINTS COUNT "

    ''' <summary> Query if data trace is enabled. </summary>
    ''' <returns> <c>true</c> if data trace enabled; otherwise <c>false</c> </returns>
    Public Function IsDataTraceEnabled() As Boolean
        Return Me.PointsCount.GetValueOrDefault(0) > 0 AndAlso
            (Me.FeedSource.GetValueOrDefault(FeedSources.None) <> FeedSources.None) AndAlso
            (Me.FeedControl.GetValueOrDefault(FeedControls.None) <> FeedControls.None)
    End Function

    ''' <summary> Number of points. </summary>
    Private _PointsCount As Integer?

    ''' <summary> Gets or sets the cached Trace Points Count. </summary>
    ''' <value> The Trace Points Count or none if not set or unknown. </value>
    Public Overloads Property PointsCount As Integer?
        Get
            Return Me._PointsCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.PointsCount, value) Then
                Me._PointsCount = value
                Me.NotifyPropertyChanged()
                Me.ActualPointCount = value
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trace Points Count. </summary>
    ''' <param name="value"> The current PointsCount. </param>
    ''' <returns> The PointsCount or none if unknown. </returns>
    Public Function ApplyPointsCount(ByVal value As Integer) As Integer?
        Me.WritePointsCount(value)
        Return Me.QueryPointsCount()
    End Function

    ''' <summary> Gets or sets the points count query command. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:COUN?". </remarks>
    ''' <value> The points count query command. </value>
    Protected Overridable Property PointsCountQueryCommand As String

    ''' <summary> Queries the current PointsCount. </summary>
    ''' <returns> The PointsCount or none if unknown. </returns>
    Public Function QueryPointsCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.PointsCountQueryCommand) Then
            Me.PointsCount = Me.Session.Query(0I, Me.PointsCountQueryCommand)
        End If
        Return Me.PointsCount
    End Function

    ''' <summary> Gets or sets the points count command format. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:COUN {0}". </remarks>
    ''' <value> The points count query command format. </value>
    Protected Overridable Property PointsCountCommandFormat As String

    ''' <summary>
    ''' Write the Trace Points Count without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current Points Count. </param>
    ''' <returns> The Points Count or none if unknown. </returns>
    Public Function WritePointsCount(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.PointsCountCommandFormat) Then
            Me.Session.WriteLine(Me.PointsCountCommandFormat, value)
        End If
        Me.PointsCount = value
        Return Me.PointsCount
    End Function

#End Region

#Region " ACTUAL POINT COUNT "

    ''' <summary> Number of ActualPoint. </summary>
    Private _ActualPointCount As Integer?

    ''' <summary> Gets or sets the cached Trace Actual Point Count. </summary>
    ''' <value> The Trace Actual Point Count or none if not set or unknown. </value>
    Public Overloads Property ActualPointCount As Integer?
        Get
            Return Me._ActualPointCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.ActualPointCount, value) Then
                Me._ActualPointCount = value
                Me.NotifyPropertyChanged()
                Me.PointsCount = value
            End If
        End Set
    End Property

    ''' <summary> Gets the Actual Points count query command. </summary>
    ''' <remarks> SCPI: ":TRAC:ACT?". </remarks>
    ''' <value> The Actual Points count query command. </value>
    Protected Overridable Property ActualPointCountQueryCommand As String

    ''' <summary> Queries the current Actual Point Count. </summary>
    ''' <returns> The Actual Point Count or none if unknown. </returns>
    Public Overridable Function QueryActualPointCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.ActualPointCountQueryCommand) Then
            Me.ActualPointCount = Me.Session.Query(0I, Me.ActualPointCountQueryCommand)
        End If
        Return Me.ActualPointCount
    End Function

    ''' <summary>
    ''' Gets an indication if this subsystem supports reading buffer actual points.
    ''' </summary>
    ''' <value> <c>True</c> if this subsystem supports reading buffer actual points. </value>
    Public ReadOnly Property SupportsActualPointCount As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.ActualPointCountQueryCommand)
        End Get
    End Property

#End Region

#Region " FIRST POINT NUMBER "

    ''' <summary> Number of First Point. </summary>
    Private _FirstPointNumber As Integer?

    ''' <summary> Gets or sets the cached buffer First Point Number. </summary>
    ''' <value> The buffer First Point Number or none if not set or unknown. </value>
    Public Overloads Property FirstPointNumber As Integer?
        Get
            Return Me._FirstPointNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.FirstPointNumber, value) Then
                Me._FirstPointNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets The First Point Number query command. </summary>
    ''' <remarks> SCPI: ":TRAC:ACT:STA?". </remarks>
    ''' <value> The First Point Number query command. </value>
    Protected Overridable Property FirstPointNumberQueryCommand As String

    ''' <summary> Queries the current FirstPointNumber. </summary>
    ''' <returns> The First Point Number or none if unknown. </returns>
    Public Overridable Function QueryFirstPointNumber() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.FirstPointNumberQueryCommand) Then
            Me.FirstPointNumber = Me.Session.Query(0I, Me.FirstPointNumberQueryCommand)
        End If
        Return Me.FirstPointNumber
    End Function

    ''' <summary>
    ''' Gets an indication if this subsystem supports reading buffer first point number.
    ''' </summary>
    ''' <value> <c>True</c> if this subsystem supports reading buffer first point number. </value>
    Public ReadOnly Property SupportsFirstPointNumber As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.FirstPointNumberQueryCommand)
        End Get
    End Property

#End Region

#Region " LAST POINT NUMBER "

    ''' <summary> Number of Last Point. </summary>
    Private _LastPointNumber As Integer?

    ''' <summary> Gets or sets the cached buffer Last Point Number. </summary>
    ''' <value> The buffer Last Point Number or none if not set or unknown. </value>
    Public Overloads Property LastPointNumber As Integer?
        Get
            Return Me._LastPointNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.LastPointNumber, value) Then
                Me._LastPointNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets The Last Point Number query command. </summary>
    ''' <remarks> SCPI: ":TRAC:ACT:END?". </remarks>
    ''' <value> The Last Point Number query command. </value>
    Protected Overridable Property LastPointNumberQueryCommand As String

    ''' <summary> Queries the current Last Point Number. </summary>
    ''' <returns> The LastPointNumber or none if unknown. </returns>
    Public Overridable Function QueryLastPointNumber() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.LastPointNumberQueryCommand) Then
            Me.LastPointNumber = Me.Session.Query(0I, Me.LastPointNumberQueryCommand)
        End If
        Return Me.LastPointNumber
    End Function

    ''' <summary>
    ''' Gets an indication if this subsystem supports reading buffer Last point number.
    ''' </summary>
    ''' <value> <c>True</c> if this subsystem supports reading buffer Last point number. </value>
    Public ReadOnly Property SupportsLastPointNumber As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.LastPointNumberQueryCommand)
        End Get
    End Property

#End Region

#Region " AVAILABLE POINT COUNT "

    ''' <summary> Number of Available Points. </summary>
    Private _AvailablePointCount As Integer?

    ''' <summary> Gets or sets the number of points still available to fill in the buffer. </summary>
    ''' <value> The Available Points Count or none if not set or unknown. </value>
    Public Overloads Property AvailablePointCount As Integer?
        Get
            Return Me._AvailablePointCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.AvailablePointCount, value) Then
                Me._AvailablePointCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Available Points count query command. </summary>
    ''' <remarks> SCPI: ":TRAC:FREE?". </remarks>
    ''' <value> The Available Points count query command. </value>
    Protected Overridable Property AvailablePointCountQueryCommand As String

    ''' <summary> Queries the current Available Point Count. </summary>
    ''' <returns> The Available Point Count or none if unknown. </returns>
    Public Function QueryAvailablePointCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.AvailablePointCountQueryCommand) Then
            Me.AvailablePointCount = Me.Session.Query(0I, Me.AvailablePointCountQueryCommand)
        End If
        Return Me.AvailablePointCount
    End Function

#End Region

#Region " BUFFER FREE "

    ''' <summary> Gets or sets the Buffer Free query command. </summary>
    ''' <remarks> SCPI: ":TRAC:FREE?". </remarks>
    ''' <value> The buffer free query command. </value>
    Protected Overridable Property BufferFreePointCountQueryCommand As String

    ''' <summary> Queries the buffer free count. </summary>
    ''' <returns> The buffer free and actual point count. </returns>
    Public Function QueryBufferFreePointCount() As (BufferFree As Integer, ActualPointCount As Integer)
        Dim bufferFree As Integer = 0
        Dim actualPointCount As Integer = 0
        If Not String.IsNullOrWhiteSpace(Me.BufferFreePointCountQueryCommand) Then
            Dim reading As String = Me.Session.QueryTrimTermination(Me.BufferFreePointCountQueryCommand)
            If Not String.IsNullOrEmpty(reading) Then
                Dim values As New Queue(Of String)(reading.Split(","c))
                If values.Any AndAlso Integer.TryParse(values.Dequeue, bufferFree) Then
                    If values.Any AndAlso Integer.TryParse(values.Dequeue, actualPointCount) Then
                    End If
                End If
            End If
        End If
        Me.ActualPointCount = actualPointCount
        Me.AvailablePointCount = bufferFree
        Return (bufferFree, actualPointCount)
    End Function

#End Region

#Region " DATA "

    ''' <summary> String. </summary>
    Private _Data As String

    ''' <summary> Gets or sets the cached Trace Data. </summary>
    ''' <value> The data. </value>
    Public Overloads Property Data As String
        Get
            Return Me._Data
        End Get
        Protected Set(ByVal value As String)
            If Not Nullable.Equals(Me.Data, value) Then
                Me._Data = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the data query command. </summary>
    ''' <remarks> SCPI: ":TRAC:DATA?". </remarks>
    ''' <value> The points count query command. </value>
    Protected Overridable Property DataQueryCommand As String

    ''' <summary> Queries the current Data. </summary>
    ''' <returns> The Data or none if unknown. </returns>
    Public Function QueryData() As String
        If Not String.IsNullOrWhiteSpace(Me.DataQueryCommand) Then
            Me.Session.WriteLine(Me.DataQueryCommand)
            ' read the entire data.
            Me.Data = Me.Session.ReadFreeLineTrimEnd()
        End If
        Return Me.Data
    End Function

    ''' <summary> Queries the current Data. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The Data or empty if none. </returns>
    Public Function QueryData(ByVal queryCommand As String) As String
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            Me.Data = String.Empty
            Me.Session.WriteLine(queryCommand)
            ' read the entire data.
            Me.Data = Me.Session.ReadFreeLineTrimEnd()
        End If
        Return Me.Data
    End Function

    ''' <summary> Queries the current Data. </summary>
    ''' <remarks> David, 2020-09-04. </remarks>
    ''' <param name="baseReading"> The base reading. </param>
    ''' <returns> The Data or empty if none. </returns>
    Public Overridable Function QueryReadings(ByVal baseReading As ReadingAmounts) As IList(Of ReadingAmounts)
        Dim values As String = Me.QueryData()
        Return baseReading.Parse(baseReading, values)
    End Function

#End Region

#Region " FETCH "

    ''' <summary> Gets or sets the fetch command. </summary>
    ''' <remarks> SCPI: 'FETCh?'. </remarks>
    ''' <value> The fetch command. </value>
    Protected Overridable Property FetchCommand As String

    ''' <summary> Fetches the data. </summary>
    ''' <remarks>
    ''' Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
    ''' there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
    ''' the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
    ''' Buffer. Thus, subsequent executions of FETCh? acquire the same data.
    ''' </remarks>
    ''' <returns> The reading. </returns>
    Public Overridable Function FetchReading() As String
        Return Me.Query(Me.Session.EmulatedReply, Me.FetchCommand)
    End Function

#End Region

#Region " BUFFER STREAM "

    ''' <summary> Duration of the binning. </summary>
    Private _BinningDuration As TimeSpan

    ''' <summary> Gets or sets the duration of the binning. </summary>
    ''' <value> The binning duration. </value>
    Public Property BinningDuration As TimeSpan
        Get
            Return Me._BinningDuration
        End Get
        Set(value As TimeSpan)
            If value <> Me.BinningDuration Then
                Me._BinningDuration = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries the current Data. </summary>
    ''' <returns> The Data or empty if none. </returns>
    Public Overridable Function QueryBufferReadings() As IList(Of BufferReading)
        Dim count As Integer = Me.QueryActualPointCount().GetValueOrDefault(0)
        If count > 0 Then
            Dim first As Integer = Me.QueryFirstPointNumber().GetValueOrDefault(0)
            Dim last As Integer = Me.QueryLastPointNumber().GetValueOrDefault(0)
            Return Me.QueryBufferReadings(first, last)
        Else
            Return New List(Of BufferReading)
        End If
    End Function

    ''' <summary> Gets or sets the default buffer 1 read command format. </summary>
    ''' <value> The default buffer 1 read command format. </value>
    Public ReadOnly Property DefaultBuffer1ReadCommandFormat As String = ":TRAC:DATA? {0},{1},'defbuffer1',READ,TST,STAT,UNIT"

    ''' <summary> List of types of the ordered reading elements. </summary>
    Private _OrderedReadingElementTypes As IEnumerable(Of ReadingElementTypes) = New List(Of ReadingElementTypes) From {VI.ReadingElementTypes.Reading, VI.ReadingElementTypes.Timestamp,
                                                                                                   VI.ReadingElementTypes.Status, VI.ReadingElementTypes.Units}

    ''' <summary> Gets or sets a list of types of the reading Elements. </summary>
    ''' <value> A list of types of the reading elements. </value>
    Public Property OrderedReadingElementTypes As IEnumerable(Of ReadingElementTypes)
        Get
            Return Me._OrderedReadingElementTypes
        End Get
        Set(value As IEnumerable(Of ReadingElementTypes))
            If Not Array.Equals(value, Me.OrderedReadingElementTypes) Then
                Me._OrderedReadingElementTypes = value
                Me.NotifyPropertyChanged()
            End If
            Me.ReadingElementTypes = TraceSubsystemBase.JoinReadingElementTypes(value)
        End Set
    End Property

    ''' <summary> Join reading Element types. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The combined ReadingElementTypes. </returns>
    Private Shared Function JoinReadingElementTypes(ByVal values As IEnumerable(Of ReadingElementTypes)) As ReadingElementTypes
        Dim result As ReadingElementTypes
        For Each v As VI.ReadingElementTypes In values
            result = result Or v
        Next
        Return result
    End Function

    ''' <summary> Type of the measured element. </summary>
    Private _MeasuredElementType As ReadingElementTypes

    ''' <summary> Gets or sets the type of the measured element. </summary>
    ''' <value> The type of the measured element. </value>
    Public Property MeasuredElementType As ReadingElementTypes
        Get
            Return Me._MeasuredElementType
        End Get
        Set(value As ReadingElementTypes)
            If Me.MeasuredElementType <> value Then
                Me._MeasuredElementType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> List of types of the reading elements. </summary>
    Private _ReadingElementTypes As ReadingElementTypes

    ''' <summary> Gets or sets a list of types of the reading elements. </summary>
    ''' <value> A list of types of the reading elements. </value>
    Public Property ReadingElementTypes As ReadingElementTypes
        Get
            Return Me._ReadingElementTypes
        End Get
        Set(value As ReadingElementTypes)
            If Me.ReadingElementTypes <> value Then
                Me._ReadingElementTypes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Enumerates the buffer readings in this collection. </summary>
    ''' <param name="commaSeparatedValues"> The comma separated values. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the buffer readings in this
    ''' collection.
    ''' </returns>
    Public Function EnumerateBufferReadings(ByVal commaSeparatedValues As String) As IList(Of BufferReading)
        Dim l As New List(Of BufferReading)
        If Not String.IsNullOrWhiteSpace(commaSeparatedValues) Then
            Dim q As New Queue(Of String)(commaSeparatedValues.Split(","c))
            Do While q.Any
                l.Add(New BufferReading(Me.MeasuredElementType, q, Me.OrderedReadingElementTypes))
            Loop
        End If
        Return l
    End Function

    ''' <summary> Queries the current Data. </summary>
    ''' <param name="firstIndex"> Zero-based index of the first. </param>
    ''' <param name="lastIndex">  Zero-based index of the last. </param>
    ''' <returns> The Data or empty if none. </returns>
    Public Overridable Function QueryBufferReadings(ByVal firstIndex As Integer, ByVal lastIndex As Integer) As IList(Of BufferReading)
        Me.QueryData(String.Format(Me.DefaultBuffer1ReadCommandFormat, firstIndex, lastIndex))
        Return Me.EnumerateBufferReadings(Me.Data)
    End Function

    ''' <summary> Gets the buffer reading binding list. </summary>
    ''' <value> The buffer readings. </value>
    Public ReadOnly Property BufferReadingsBindingList As BufferReadingBindingList

    ''' <summary> Gets the number of buffer readings. </summary>
    ''' <value> The number of buffer readings. </value>
    Public ReadOnly Property BufferReadingsCount As Integer
        Get
            Return Me.BufferReadingsBindingList.Count
        End Get
    End Property

    ''' <summary> The last buffer reading. </summary>
    Private _LastBufferReading As BufferReading

    ''' <summary> Gets or sets the last buffer reading. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastBufferReading As BufferReading
        Get
            Return Me._LastBufferReading
        End Get
        Set(value As BufferReading)
            Me._LastBufferReading = value
            If value Is Nothing Then
                Me.LastBufferReadingCaption = "-.---"
                Me.LastRawReading = String.Empty
                Me.LastBufferReadingStatus = String.Empty
            Else
                Me.LastBufferReadingCaption = Me.LastBufferReading.ReadingCaption
                Me.LastRawReading = Me.LastBufferReading.Reading
                Me.LastBufferReadingStatus = Me.LastBufferReading.StatusReading
            End If
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The last buffer reading status. </summary>
    Private _LastBufferReadingStatus As String

    ''' <summary> Gets or sets the last buffer reading readings. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastBufferReadingStatus As String
        Get
            Return Me._LastBufferReadingStatus
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastBufferReadingStatus) Then
                Me._LastBufferReadingStatus = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last raw reading. </summary>
    Private _LastRawReading As String

    ''' <summary> Gets or sets the last buffer reading readings. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastRawReading As String
        Get
            Return Me._LastRawReading
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastRawReading) Then
                Me._LastRawReading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The buffer reading unit. </summary>
    Private _BufferReadingUnit As Arebis.TypedUnits.Unit

    ''' <summary> Gets or sets the buffer reading unit. </summary>
    ''' <value> The buffer reading unit. </value>
    Public Property BufferReadingUnit As Arebis.TypedUnits.Unit
        Get
            Return Me._BufferReadingUnit
        End Get
        Protected Set(value As Arebis.TypedUnits.Unit)
            If Not Arebis.TypedUnits.Unit.Equals(value, Me.BufferReadingUnit) Then
                Me._BufferReadingUnit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last buffer reading caption. </summary>
    Private _LastBufferReadingCaption As String

    ''' <summary> Gets or sets the last buffer reading. </summary>
    ''' <value> The last buffer reading. </value>
    Public Property LastBufferReadingCaption As String
        Get
            Return Me._LastBufferReadingCaption
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LastBufferReadingCaption) Then
                Me._LastBufferReadingCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the items locker. </summary>
    ''' <value> The items locker. </value>
    Protected ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()

    ''' <summary> Gets a queue of new buffer readings. </summary>
    ''' <value> A thread safe Queue of buffer readings. </value>
    Public ReadOnly Property NewBufferReadingsQueue As System.Collections.Concurrent.ConcurrentQueue(Of BufferReading)

    ''' <summary> Clears the buffer readings queue. </summary>
    Public Sub ClearBufferReadingsQueue()
        Me._NewBufferReadingsQueue = New Concurrent.ConcurrentQueue(Of BufferReading)
    End Sub

    ''' <summary> Gets the number of new readings. </summary>
    ''' <value> The number of new readings. </value>
    Public ReadOnly Property NewReadingsCount As Integer
        Get
            Try
                Me.ItemsLocker.EnterReadLock()
                Return Me.NewBufferReadingsQueue.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary> Enqueue range. </summary>
    ''' <param name="items"> The items. </param>
    Public Sub EnqueueRange(ByVal items As IList(Of BufferReading))
        If items Is Nothing OrElse Not items.Any Then Return
        Me.ItemsLocker.EnterWriteLock()
        Try
            For Each item As BufferReading In items
                Me.NewBufferReadingsQueue.Enqueue(item)
            Next item
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Dequeues the specified number of new values. </summary>
    ''' <remarks> David, 2020-08-01. </remarks>
    ''' <param name="count"> Number of values to dequeue. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process dequeue range in this collection.
    ''' </returns>
    Public Function DequeueRange(ByVal count As Integer) As IList(Of BufferReading)
        Dim result As New List(Of BufferReading)
        Try
            Me.ItemsLocker.EnterReadLock()
            Dim value As BufferReading = Nothing
            Do While Me.NewBufferReadingsQueue.Any And count > 0
                If Me.NewBufferReadingsQueue.TryDequeue(value) Then
                    result.Add(value)
                    count -= 1
                End If
            Loop
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
        Return result
    End Function

    ''' <summary> Adds buffer readings. </summary>
    ''' <param name="bufferInfo"> Information describing the buffer. </param>
    ''' <returns> An Integer. </returns>
    Private Function AddBufferReadings(ByVal bufferInfo As BufferInfo) As BufferInfo
        Dim nextBufferInfo As BufferInfo = If(bufferInfo.IsEmpty,
                                                New BufferInfo(Me.QueryFirstPointNumber().GetValueOrDefault(BufferInfo.EmptyBufferFirstBufferNumber),
                                                               BufferInfo.EmptyBufferLastBufferNumber),
                                                New BufferInfo(bufferInfo))
        If Not nextBufferInfo.IsEmpty Then
            ' if has at least one reading, get the last reading; it might be the first reading.
            nextBufferInfo = New BufferInfo(nextBufferInfo.First, Me.QueryLastPointNumber().GetValueOrDefault(BufferInfo.EmptyBufferLastBufferNumber))
            ' check if we have new readings.
            If nextBufferInfo.Count > Me.BufferReadingsCount Then
                Dim newReadings As IList(Of BufferReading) = Me.QueryBufferReadings(Me.BufferReadingsCount + 1, nextBufferInfo.Last)
                For Each reading As BufferReading In newReadings
                    reading.BuildAmount(Me.BufferReadingUnit)
                Next
                Me.BufferReadingsBindingList.Add(newReadings)
                Me.EnqueueRange(newReadings)
                ' !@# fixes the missing last buffer reading.
                Me.LastBufferReading = Me.BufferReadingsBindingList.LastReading
            End If
        End If
        Return nextBufferInfo
    End Function

    ''' <summary> Adds all buffer readings. </summary>
    ''' <returns> An Integer. </returns>
    Public Overridable Function AddAllBufferReadings() As Integer
        Dim newReadings As IList(Of BufferReading) = Me.QueryBufferReadings()
        If newReadings.Any Then
            Me.BufferReadingsBindingList.Add(newReadings)
            Me.EnqueueRange(newReadings)
            Me.LastBufferReading = Me.BufferReadingsBindingList.LastReading
            Me.NotifyPropertyChanged(NameOf(VI.TraceSubsystemBase.BufferReadingsCount))
        End If
        Return newReadings.Count
    End Function

    ''' <summary> Gets or sets the binning strobe requested token. </summary>
    ''' <value> The binning strobe requested token. </value>
    Private ReadOnly Property BinningStrobeRequestedToken As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the binning strobe requested. </summary>
    ''' <value> The binning strobe requested. </value>
    Public Property BinningStrobeRequested As Boolean
        Get
            Return Me.BinningStrobeRequestedToken.Value
        End Get
        Set(value As Boolean)
            If Me.BinningStrobeRequested <> value Then
                Me.BinningStrobeRequestedToken.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the binning strobe action. </summary>
    ''' <value> The binning strobe action. </value>
    Public Property BinningStrobeAction As Action

    ''' <summary> Gets or sets the bus trigger requested token. </summary>
    ''' <value> The bus trigger requested token. </value>
    Private ReadOnly Property BusTriggerRequestedToken As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets BUS Trigger Requested. </summary>
    ''' <value> The Bus Trigger Requested value. </value>
    Public Property BusTriggerRequested As Boolean
        Get
            Return Me.BusTriggerRequestedToken.Value
        End Get
        Set(value As Boolean)
            If Me.BusTriggerRequested <> value Then
                Me.BusTriggerRequestedToken.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the buffer streaming enabled token. </summary>
    ''' <value> The buffer streaming enabled token. </value>
    Private ReadOnly Property BufferStreamingEnabledToken As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the buffer streaming enabled. </summary>
    ''' <value> The buffer streaming enabled. </value>
    Public Property BufferStreamingEnabled As Boolean
        Get
            Return Me.BufferStreamingEnabledToken.Value
        End Get
        Protected Set(value As Boolean)
            If Me.BufferStreamingEnabled <> value Then
                Me.BufferStreamingEnabledToken.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the buffer streaming alert token. </summary>
    ''' <value> The buffer streaming alert token. </value>
    Private ReadOnly Property BufferStreamingAlertToken As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the buffer streaming Alert. </summary>
    ''' <value> The buffer streaming Alert. </value>
    Public Property BufferStreamingAlert As Boolean
        Get
            Return Me.BufferStreamingAlertToken.Value
        End Get
        Set(value As Boolean)
            If Me.BufferStreamingAlert <> value Then
                Me.BufferStreamingAlertToken.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the buffer streaming active token. </summary>
    ''' <value> The buffer streaming active token. </value>
    Private ReadOnly Property BufferStreamingActiveToken As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the buffer streaming Active. </summary>
    ''' <value> The buffer streaming Active. </value>
    Public Property BufferStreamingActive As Boolean
        Get
            Return Me.BufferStreamingActiveToken.Value
        End Get
        Protected Set(value As Boolean)
            If Me.BufferStreamingActive <> value Then
                Me.BufferStreamingActiveToken.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Duration of the stream cycle. </summary>
    Private _StreamCycleDuration As TimeSpan

    ''' <summary> Gets or sets the duration of the stream cycle. </summary>
    ''' <value> The stream cycle duration. </value>
    Public Property StreamCycleDuration As TimeSpan
        Get
            Return Me._StreamCycleDuration
        End Get
        Set(value As TimeSpan)
            If Not TimeSpan.Equals(value, Me.StreamCycleDuration) Then
                Me._StreamCycleDuration = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Stream buffer. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    Private Sub StreamBufferThis(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan)
        Dim currentBufferInfo As BufferInfo = BufferInfo.Empty
        Dim previousBufferInfo As BufferInfo = BufferInfo.Empty
        Me.BufferStreamingEnabled = True
        Me.BufferReadingsBindingList.Clear()
        Me.ClearBufferReadingsQueue()
        Me.BufferStreamingActive = True
        triggerSubsystem.QueryTriggerState()
        Me.StreamCycleDuration = TimeSpan.FromTicks(10 * pollPeriod.Ticks)
        Dim bufferCycleDurationStopwatch As Stopwatch = Stopwatch.StartNew
        Dim pollStopwatch As Stopwatch = Stopwatch.StartNew
        Do While triggerSubsystem.IsTriggerStateActive AndAlso Me.BufferStreamingEnabled
            currentBufferInfo = Me.AddBufferReadings(previousBufferInfo)
            If previousBufferInfo.Count < currentBufferInfo.Count Then
                ' if having new readings, update cycle duration and notify
                Me.StreamCycleDuration = bufferCycleDurationStopwatch.Elapsed
                bufferCycleDurationStopwatch.Restart()
                Me.SyncNotifyPropertyChanged(NameOf(VI.TraceSubsystemBase.BufferReadingsCount))
                previousBufferInfo = currentBufferInfo
                isr.Core.ApplianceBase.DoEvents()
                pollStopwatch.Restart()
            End If
            If Me.BusTriggerRequested Then Me.BusTriggerRequested = False : Me.Session.AssertTrigger()
            pollPeriod.Subtract(pollStopwatch.Elapsed).SpinWait()
            pollStopwatch.Restart()
            isr.Core.ApplianceBase.DoEvents()
            triggerSubsystem.QueryTriggerState()
        Loop
        Me.BufferStreamingActive = False
    End Sub

    ''' <summary> Stream buffer. </summary>
    ''' <remarks> David, 2020-07-23. </remarks>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    ''' <param name="readingUnit">      The reading unit. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Overridable Sub StreamBuffer(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan, ByVal readingUnit As Arebis.TypedUnits.Unit)
        Try
            Me.BufferReadingUnit = readingUnit
            Me.StreamBufferThis(triggerSubsystem, pollPeriod)
        Catch
            ' stop buffer streaming; the exception is handled in the Async Completed event handler
            Me.BufferStreamingAlert = True
            Me.BufferStreamingEnabled = False
            Me.BufferStreamingActive = False
        End Try
    End Sub

    ''' <summary> Gets the buffer stream tasker. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The buffer stream tasker. </value>
    Public ReadOnly Property BufferStreamTasker As isr.Core.Tasker

    ''' <summary> Starts buffer stream. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    ''' <param name="readingUnit">      The reading unit. </param>
    Public Sub StartBufferStream(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan, ByVal readingUnit As Arebis.TypedUnits.Unit)
        If triggerSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(triggerSubsystem))
        Me._BufferStreamTasker.StartAction(Sub()
                                               Me.StreamBuffer(triggerSubsystem, pollPeriod, readingUnit)
                                           End Sub)
    End Sub

    ''' <summary>
    ''' Estimates the stream stop timeout interval as scale margin times the cycle and poll intervals.
    ''' </summary>
    ''' <remarks> David, 2020-04-13. </remarks>
    ''' <param name="streamCycleDuration"> Duration of the stream cycle. </param>
    ''' <param name="pollInterval">        The poll interval. </param>
    ''' <param name="scaleMargin">         The scale margin. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function EstimateStreamStopTimeoutInterval(ByVal streamCycleDuration As TimeSpan, ByVal pollInterval As TimeSpan, ByVal scaleMargin As Double) As TimeSpan
        Return TimeSpan.FromTicks(CLng(scaleMargin * (streamCycleDuration.Ticks + pollInterval.Ticks)))
    End Function

    ''' <summary> Stops buffer stream and wait for completion or timeout. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function StopBufferStream(ByVal timeout As TimeSpan) As (Success As Boolean, Details As String)
        Me.BufferStreamingEnabled = False
        Dim status As Tasks.TaskStatus = Me.BufferStreamTasker.AwaitCompletion(timeout)
        Return If(Tasks.TaskStatus.RanToCompletion = status,
            If(Me.BufferStreamingActive,
                            (False, $"Failed stopping buffer streaming; still active"),
                            (True, "buffer streaming completed")),
            (False, $"buffer streaming task existed with a {status} {NameOf(Tasks.TaskStatus)}"))
    End Function

#End Region

End Class

''' <summary> Information about the buffer. </summary>
''' <remarks> David, 2020-08-01. </remarks>
Public Structure BufferInfo

    ''' <summary> The empty buffer first buffer number. </summary>
    Public Const EmptyBufferFirstBufferNumber As Integer = 0

    ''' <summary> The empty buffer last buffer number. </summary>
    Public Const EmptyBufferLastBufferNumber As Integer = 0

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-01. </remarks>
    ''' <param name="first"> The first. </param>
    ''' <param name="last">  The last. </param>
    Public Sub New(ByVal first As Integer, ByVal last As Integer)
        Me.First = first
        Me.Last = last
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-01. </remarks>
    ''' <param name="bufferInfo"> Information describing the buffer. </param>
    Public Sub New(ByVal bufferInfo As BufferInfo)
        Me.First = bufferInfo.First
        Me.Last = bufferInfo.Last
    End Sub

    ''' <summary> Gets the empty. </summary>
    ''' <remarks> David, 2020-08-01. </remarks>
    ''' <returns> A BufferInfo. </returns>
    Public Shared Function Empty() As BufferInfo
        Return New BufferInfo(BufferInfo.EmptyBufferFirstBufferNumber, BufferInfo.EmptyBufferLastBufferNumber)
    End Function

    ''' <summary> Gets the first. </summary>
    ''' <value> The first. </value>
    Public Property First As Integer

    ''' <summary> Gets the last. </summary>
    ''' <value> The last. </value>
    Public Property Last As Integer

    ''' <summary> Gets the is empty. </summary>
    ''' <value> The is empty. </value>
    Public ReadOnly Property IsEmpty As Boolean
        Get
            Return Me.First <= 0
        End Get
    End Property

    ''' <summary> Gets the number of. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer
        Get
            Return If(Me.IsEmpty, 0, Me.Last - Me.First + 1)
        End Get
    End Property

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <param name="obj"> The object to compare with the current instance. </param>
    ''' <returns>
    ''' <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and
    ''' represent the same value; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.Equals(CType(obj, BufferInfo))
    End Function

    ''' <summary> Tests if this BufferInfo is considered equal to another. </summary>
    ''' <param name="other"> The buffer information to compare to this object. </param>
    ''' <returns> True if the objects are considered equal, false if they are not. </returns>
    Public Overloads Function Equals(other As BufferInfo) As Boolean
        Return Me.First = other.First AndAlso Me.Last = other.Last
    End Function

    ''' <summary> Returns the hash code for this instance. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <returns> A 32-bit signed integer that is the hash code for this instance. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.First Xor Me.Last
        Throw New NotImplementedException()
    End Function

    ''' <summary> Cast that converts the given BufferInfo to a =. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(left As BufferInfo, right As BufferInfo) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary> Cast that converts the given BufferInfo to a &lt;&gt; </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(left As BufferInfo, right As BufferInfo) As Boolean
        Return Not left = right
    End Operator
End Structure
