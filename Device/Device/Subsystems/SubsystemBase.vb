''' <summary> Defines the contract that must be implemented by Subsystems. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-15, 1.0.1841.x. </para>
''' </remarks>
Public MustInherit Class SubsystemBase
    Inherits isr.Core.Models.ViewModelTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SubsystemBase" /> class. </summary>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Protected Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New()
        Me.ApplySessionThis(session)
        Me.ElapsedTimeStopwatch = New Stopwatch
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the clear execution state (CLS) by setting system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    ''' <remarks> Clears the queues and sets all registers to zero. </remarks>
    Public Overridable Sub DefineClearExecutionState()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overridable Sub InitKnownState()
    End Sub

    ''' <summary> Sets the subsystem known preset state. </summary>
    Public Overridable Sub PresetKnownState()
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> Clears the queues and sets all registers to zero. </remarks>
    Public Overridable Sub DefineKnownResetState()
    End Sub

#End Region

#Region " SESSION "

    ''' <summary> Gets the session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The session. </value>
    Public ReadOnly Property Session As VI.Pith.SessionBase

    ''' <summary> Applies the session described by value. </summary>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                            session</see>. </param>
    Public Sub ApplySession(ByVal session As VI.Pith.SessionBase)
        Me.ApplySessionThis(session)
        Me.NotifyPropertyChanged(NameOf(SubsystemBase.ResourceNameCaption))
        Me.NotifyPropertyChanged(NameOf(SubsystemBase.ResourceTitleCaption))
        Me.NotifyPropertyChanged(NameOf(SubsystemBase.IsDeviceOpen))
    End Sub

    ''' <summary> Applies the session described by value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Private Sub ApplySessionThis(ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Me._Session = session
        AddHandler session.PropertyChanged, AddressOf Me.SessionPropertyChanged
    End Sub

    ''' <summary> Handles the session property changed action. </summary>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Sub HandlePropertyChanged(ByVal sender As Pith.SessionBase, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Pith.SessionBase.ResourceTitleCaption)
                Me.NotifyPropertyChanged(NameOf(SubsystemBase.ResourceTitleCaption))
            Case NameOf(VI.Pith.SessionBase.ResourceNameCaption)
                Me.NotifyPropertyChanged(NameOf(SubsystemBase.ResourceNameCaption))
        End Select
    End Sub

    ''' <summary> Handles the Session property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.Pith.SessionBase)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, VI.Pith.SessionBase), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether a session to the device is open. See also
    ''' <see cref="VI.Pith.SessionBase.IsDeviceOpen"/>.
    ''' </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me.Session IsNot Nothing AndAlso Me.Session.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Gets the resource title. </summary>
    ''' <value> The resource title. </value>
    Public ReadOnly Property ResourceTitleCaption As String
        Get
            Return Me.Session?.ResourceTitleCaption
        End Get
    End Property

    ''' <summary> Gets the resource name caption. </summary>
    ''' <value> The resource name caption. </value>
    Public ReadOnly Property ResourceNameCaption As String
        Get
            Return Me.Session?.ResourceNameCaption
        End Get
    End Property

#End Region

#Region " QUERY / WRITE / EXECUTE "

#Region " ENUMERATION "

    ''' <summary> Queries first value returned from the instrument. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <param name="value">        The value. </param>
    ''' <returns> The first value. </returns>
    Public Function QueryFirstValue(Of T As Structure)(ByVal queryCommand As String, ByVal value As Nullable(Of T)) As Nullable(Of T)
        Return If(Not String.IsNullOrWhiteSpace(queryCommand), Me.Session.QueryFirstEnumValue(Of T)(value, queryCommand), value)
    End Function

    ''' <summary> Issues the query command and parses the returned enum value into an Enum. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <param name="value">        The value. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function QueryValue(Of T As Structure)(ByVal queryCommand As String, ByVal value As Nullable(Of T)) As Nullable(Of T)
        Return If(Not String.IsNullOrWhiteSpace(queryCommand), Me.Session.QueryEnumValue(Of T)(value, queryCommand), value)
    End Function

    ''' <summary> Writes the Enum value without reading back the value from the device. </summary>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="value">         The value. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function WriteValue(Of T As Structure)(ByVal commandFormat As String, ByVal value As T) As Nullable(Of T)
        Return Me.Session.WriteEnumValue(Of T)(value, commandFormat)
    End Function

    ''' <summary>
    ''' Issues the query command and parses the returned enum value name into an Enum.
    ''' </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <param name="value">        The value. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function Query(Of T As Structure)(ByVal queryCommand As String, ByVal value As Nullable(Of T)) As Nullable(Of T)
        Return If(Not String.IsNullOrWhiteSpace(queryCommand), Me.Session.QueryEnum(Of T)(value, queryCommand), value)
    End Function

    ''' <summary>
    ''' Issues the query command and parses the returned enum value name into an Enum.
    ''' </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <param name="value">        The value. </param>
    ''' <param name="readWrites">   The read writes. </param>
    ''' <returns> The parsed value or none if unknown. </returns>
    Public Function Query(Of T As Structure)(ByVal queryCommand As String, ByVal value As T, ByVal readWrites As Pith.EnumReadWriteCollection) As Nullable(Of T)
        Return If(Not String.IsNullOrWhiteSpace(queryCommand), Me.Session.Query(Of T)(value, readWrites, queryCommand), value)
    End Function

    ''' <summary> Queries first value returned from the instrument. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <param name="value">        The value. </param>
    ''' <param name="readWrites">   The read writes. </param>
    ''' <returns> The first value. </returns>
    Public Function QueryFirstValue(Of T As Structure)(ByVal queryCommand As String, ByVal value As T, ByVal readWrites As Pith.EnumReadWriteCollection) As Nullable(Of T)
        Return If(Not String.IsNullOrWhiteSpace(queryCommand), Me.Session.QueryFirst(Of T)(value, readWrites, queryCommand), value)
    End Function

    ''' <summary> Writes the Enum value name without reading back the value from the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="value">         The value. </param>
    ''' <param name="readWrites">    Dictionary of parses. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function Write(Of T As Structure)(ByVal commandFormat As String, ByVal value As T, ByVal readWrites As Pith.EnumReadWriteCollection) As Nullable(Of T)
        Return Me.Session.Write(Of T)(value, commandFormat, readWrites)
    End Function

    ''' <summary> Writes the Enum value name without reading back the value from the device. </summary>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="value">         The value. </param>
    ''' <returns> The value or none if unknown. </returns>
    Public Function Write(Of T As Structure)(ByVal commandFormat As String, ByVal value As T) As Nullable(Of T)
        Return Me.Session.Write(Of T)(value, commandFormat)
    End Function

#End Region

#Region " BOOLEAN "

    ''' <summary> Queries a <see cref="T:Boolean">Boolean</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Boolean?, ByVal queryCommand As String) As Boolean?
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(value.GetValueOrDefault(True), queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Boolean, ByVal commandFormat As String) As Boolean?
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, value.GetHashCode)
        End If
        Return value
    End Function

#End Region

#Region " INTEGER "

    ''' <summary> Queries an <see cref="T:Integer">integer</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Integer?, ByVal queryCommand As String) As Integer?
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(value.GetValueOrDefault(0), queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Integer, ByVal commandFormat As String) As Integer?
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, value)
        End If
        Return value
    End Function

#End Region

#Region " DOUBLE "

    ''' <summary> Queries and parses the second value from the instrument. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The second. </returns>
    Public Function QuerySecond(ByVal value As Double?, ByVal queryCommand As String) As Double?
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.QuerySecond(value.GetValueOrDefault(0), queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Queries an <see cref="T:Double">Double</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As Double?, ByVal queryCommand As String) As Double?
        If Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(value.GetValueOrDefault(0), queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As Double, ByVal commandFormat As String) As Double?
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            If value >= (VI.Pith.Scpi.Syntax.Infinity - 1) Then
                Me.Session.WriteLine(commandFormat, "MAX")
                value = VI.Pith.Scpi.Syntax.Infinity
            ElseIf value <= (VI.Pith.Scpi.Syntax.NegativeInfinity + 1) Then
                Me.Session.WriteLine(commandFormat, "MIN")
                value = VI.Pith.Scpi.Syntax.NegativeInfinity
            Else
                Me.Session.WriteLine(commandFormat, value)
            End If
        End If
        Return value
    End Function

#End Region

#Region " STRING "

    ''' <summary> Queries a <see cref="T:String">String</see> value. </summary>
    ''' <param name="value">        The present value. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As String, ByVal queryCommand As String) As String
        Return If(Not String.IsNullOrWhiteSpace(queryCommand), Me.Session.QueryTrimEnd(queryCommand), value)
    End Function

    ''' <summary> Queries a <see cref="T:String">String</see> value. </summary>
    ''' <param name="value">         The present value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="args">          A variable-length parameters list containing arguments. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As String, ByVal commandFormat As String, ByVal ParamArray args() As Object) As String
        Return Me.Query(value, String.Format(commandFormat, args))
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="args">          A variable-length parameters list containing arguments. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal commandFormat As String, ByVal ParamArray args() As Object) As String
        Return Me.Write(String.Format(commandFormat, args))
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As String) As String
        If Not String.IsNullOrWhiteSpace(value) Then Me.Session.WriteLine(value)
        Return value
    End Function

#End Region

#Region " TIME SPAN "

    ''' <summary> Queries an <see cref="T:TimeSpan">TimeSpan</see> value. </summary>
    ''' <param name="value">        The value. </param>
    ''' <param name="format">       Describes the format to use. </param>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The value. </returns>
    Public Function Query(ByVal value As TimeSpan?, ByVal format As String, ByVal queryCommand As String) As TimeSpan?
        If Not String.IsNullOrWhiteSpace(format) AndAlso Not String.IsNullOrWhiteSpace(queryCommand) Then
            value = Me.Session.Query(format, queryCommand)
        End If
        Return value
    End Function

    ''' <summary> Write the value without reading back the value from the device. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <returns> The value. </returns>
    Public Function Write(ByVal value As TimeSpan, ByVal commandFormat As String) As TimeSpan?
        If Not String.IsNullOrWhiteSpace(commandFormat) Then
            Me.Session.WriteLine(commandFormat, value)
        End If
        Return value
    End Function

#End Region

#Region " PAYLOAD "

    ''' <summary> Issues the query command and parses the returned payload. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="payload"> The payload. </param>
    ''' <returns>
    ''' <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
    ''' <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
    ''' </returns>
    Public Function Query(ByVal payload As VI.Pith.PayloadBase) As Boolean
        If payload Is Nothing Then Throw New ArgumentNullException(NameOf(payload))
        Dim result As Boolean = True
        If Not String.IsNullOrWhiteSpace(payload.QueryCommand) Then
            result = Me.Session.Query(payload)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Write the payload. A <see cref="Query(vi.Pith.PayloadBase)"/> must be issued to get the value
    ''' from the device.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="payload"> The payload. </param>
    ''' <returns>
    ''' <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
    ''' <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
    ''' </returns>
    Public Function Write(ByVal payload As VI.Pith.PayloadBase) As Boolean
        If payload Is Nothing Then Throw New ArgumentNullException(NameOf(payload))
        Dim result As Boolean = True
        If Not String.IsNullOrWhiteSpace(payload.CommandFormat) Then
            result = Me.Session.Write(payload)
        End If
        Return result
    End Function

#End Region

#Region " EXECUTE "

    ''' <summary> Executes the command. </summary>
    ''' <param name="command"> The command. </param>
    Public Sub Execute(ByVal command As String)
        If Not String.IsNullOrWhiteSpace(command) Then Me.Session.Execute(command)
    End Sub

    ''' <summary> Executes the command. </summary>
    ''' <param name="commandFormat"> The command format. </param>
    ''' <param name="args">          A variable-length parameters list containing arguments. </param>
    Public Sub Execute(ByVal commandFormat As String, ByVal ParamArray args() As Object)
        If Not String.IsNullOrWhiteSpace(commandFormat) Then Me.Session.Execute(String.Format(commandFormat, args))
    End Sub

#End Region

#Region " QUERY WITH STATUS READ "

    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
    ''' after the specified delay. A final read is performed after a delay provided no error occurred.
    ''' </summary>
    ''' <param name="statusReadDelay"> The read delay. </param>
    ''' <param name="readDelay">       The read delay. </param>
    ''' <param name="dataToWrite">     The data to write. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal statusReadDelay As TimeSpan, ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Session.Query(statusReadDelay, readDelay, dataToWrite)
    End Function

    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
    ''' after the specified delay. A final read is performed after a delay provided no error occurred.
    ''' </summary>
    ''' <param name="statusReadDelay"> The read delay. </param>
    ''' <param name="readDelay">       The read delay. </param>
    ''' <param name="dataToWrite">     The data to write. </param>
    ''' <returns> The trim end. </returns>
    Public Function QueryTrimEnd(ByVal statusReadDelay As TimeSpan, ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Session.QueryTrimEnd(statusReadDelay, readDelay, dataToWrite)
    End Function

    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read
    ''' after the specified delay.
    ''' </summary>
    ''' <param name="readDelay">   The read delay. </param>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Session.Query(readDelay, dataToWrite)
    End Function

    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read
    ''' after the specified delay.
    ''' </summary>
    ''' <param name="readDelay">   The read delay. </param>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> The trim end. </returns>
    Public Function QueryTrimEnd(ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Session.QueryTrimEnd(readDelay, dataToWrite)
    End Function

#End Region

#End Region

#Region " ELAPSED TIME "

    ''' <summary> The last action elapsed time. </summary>
    Private _LastActionElapsedTime As TimeSpan

    ''' <summary> Gets or sets the last reading. </summary>
    ''' <value> The last reading. </value>
    Public Property LastActionElapsedTime As TimeSpan
        Get
            Return Me._LastActionElapsedTime
        End Get
        Set(value As TimeSpan)
            If value <> Me.LastActionElapsedTime Then
                Me._LastActionElapsedTime = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
