﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConnectorView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ConnectorView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._ConnectGroupBox = New System.Windows.Forms.GroupBox()
        Me._ResourceSelectorConnector = New isr.Core.Controls.SelectorOpener()
        Me._ResourceInfoLabel = New System.Windows.Forms.Label()
        Me._IdentityTextBox = New System.Windows.Forms.TextBox()
        Me._Layout.SuspendLayout()
        Me._ConnectGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))

        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._ConnectGroupBox, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(548, 419)
        Me._Layout.TabIndex = 1
        '
        '_ConnectGroupBox
        '
        Me._ConnectGroupBox.Controls.Add(Me._ResourceSelectorConnector)
        Me._ConnectGroupBox.Controls.Add(Me._ResourceInfoLabel)
        Me._ConnectGroupBox.Controls.Add(Me._IdentityTextBox)
        Me._ConnectGroupBox.Location = New System.Drawing.Point(42, 106)
        Me._ConnectGroupBox.Name = "_ConnectGroupBox"
        Me._ConnectGroupBox.Size = New System.Drawing.Size(464, 207)
        Me._ConnectGroupBox.TabIndex = 2
        Me._ConnectGroupBox.TabStop = False
        Me._ConnectGroupBox.Text = "CONNECT"
        '
        '_ResourceSelectorConnector
        '
        Me._ResourceSelectorConnector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ResourceSelectorConnector.BackColor = System.Drawing.Color.Transparent
        Me._ResourceSelectorConnector.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResourceSelectorConnector.Location = New System.Drawing.Point(24, 21)
        Me._ResourceSelectorConnector.Margin = New System.Windows.Forms.Padding(0)
        Me._ResourceSelectorConnector.Name = "_ResourceSelectorConnector"
        Me._ResourceSelectorConnector.PublishBindingSuccessEnabled = False
        Me._ResourceSelectorConnector.Size = New System.Drawing.Size(419, 29)
        Me._ResourceSelectorConnector.TabIndex = 5
        Me._ResourceSelectorConnector.TraceLogEvent = CType(resources.GetObject("_ResourceSelectorConnector.TraceLogEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._ResourceSelectorConnector.TraceLogLevel = System.Diagnostics.TraceEventType.Information
        Me._ResourceSelectorConnector.TraceShowEvent = CType(resources.GetObject("_ResourceSelectorConnector.TraceShowEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._ResourceSelectorConnector.TraceShowLevel = System.Diagnostics.TraceEventType.Information
        '
        '_ResourceInfoLabel
        '
        Me._ResourceInfoLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ResourceInfoLabel.Location = New System.Drawing.Point(28, 89)
        Me._ResourceInfoLabel.Name = "_ResourceInfoLabel"
        Me._ResourceInfoLabel.Size = New System.Drawing.Size(408, 102)
        Me._ResourceInfoLabel.TabIndex = 4
        Me._ResourceInfoLabel.Text = resources.GetString("_ResourceInfoLabel.Text")
        '
        '_IdentityTextBox
        '
        Me._IdentityTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._IdentityTextBox.Location = New System.Drawing.Point(28, 61)
        Me._IdentityTextBox.Name = "_IdentityTextBox"
        Me._IdentityTextBox.ReadOnly = True
        Me._IdentityTextBox.Size = New System.Drawing.Size(408, 25)
        Me._IdentityTextBox.TabIndex = 3
        '
        'ConnectorView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Name = "ConnectorView"
        Me.Size = New System.Drawing.Size(548, 419)
        Me._Layout.ResumeLayout(False)
        Me._ConnectGroupBox.ResumeLayout(False)
        Me._ConnectGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _ConnectGroupBox As Windows.Forms.GroupBox
    Private WithEvents _ResourceSelectorConnector As Core.Controls.SelectorOpener
    Private WithEvents _ResourceInfoLabel As Windows.Forms.Label
    Private WithEvents _IdentityTextBox As Windows.Forms.TextBox
End Class
