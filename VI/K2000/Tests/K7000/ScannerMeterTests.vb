'---------------------------------------------------------------------------------------------------
' file:		Tests\K7000\ScannerMeter_Tests.vb
'
' summary:	Scanner meter tests class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions
Imports isr.Core.SplitExtensions

''' <summary> K7000 Scanner Multimeter Scan Card unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2002"), TestCategory("k7001")>
Public Class ScannerMeterTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()

        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(K2002ResourceInfo.Exists, $"{GetType(K2002Tests.ResourceSettings)} settings not found")
        Assert.IsTrue(K2002SubsystemsInfo.Exists, $"{GetType(K2002Tests.SubsystemsSettings)} settings not found")
        Assert.IsTrue(K7000SubsystemInfo.Exists, $"{GetType(K2002Tests.K7000ResourceSettings)} settings not found")
        Assert.IsTrue(K7000SubsystemInfo.Exists, $"{GetType(K2002Tests.ScannerMeterSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            If _TestSite Is Nothing Then
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            End If
            Return _TestSite
        End Get
    End Property

#End Region

#Region " OPEN SESSIONS "

    ''' <summary> (Unit Test Method) tests open sessions. </summary>
    <TestMethod()>
    Public Sub OpenSessionsTest()
        If Not K2002ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K2002ResourceInfo.ResourceTitle} not found")
        If Not K7000ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K7000ResourceInfo.ResourceTitle} not found")
        Using k7000Device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            k7000Device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using K2002Device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                K2002Device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, K2002Device, K2002ResourceInfo)
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, k7000Device, K7000ResourceInfo)
                Catch
                    Throw
                Finally
                    K2002Tests.DeviceManager.CloseSession(TestInfo, k7000Device)
                    K2002Tests.DeviceManager.CloseSession(TestInfo, K2002Device)
                End Try
            End Using
        End Using
    End Sub

#End Region

#Region " BUS TRIGGER PLAN: INSTRUMENT CONFIGURATION "

    ''' <summary> Assert configure digital output signal polarity. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalOutputLineNumber"> The digital output line number. </param>
    ''' <param name="subsystem">               The subsystem. </param>
    Public Shared Sub AssertConfigureDigitalOutputSignalPolarity(ByVal digitalOutputLineNumber As Integer, ByVal subsystem As VI.DigitalOutputSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))

        Dim propertyName As String = String.Empty
        Dim deviceName As String = subsystem.ResourceNameCaption
        propertyName = $"{GetType(VI.DigitalOutputSubsystemBase)}.{NameOf(VI.DigitalOutputSubsystemBase.CurrentDigitalActiveLevel)}"
        Dim activity As String = $"Reading [{deviceName}].[{propertyName}({digitalOutputLineNumber})] initial value"
        Dim initialPolarity As VI.DigitalActiveLevels? = subsystem.QueryDigitalActiveLevel(digitalOutputLineNumber)
        Assert.IsTrue(initialPolarity.HasValue, $"{activity} should have a value")

        Dim expectedPolarity As VI.DigitalActiveLevels = If(initialPolarity.Value = DigitalActiveLevels.High, DigitalActiveLevels.Low, DigitalActiveLevels.High)
        activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}"
        Dim actualPolarity As VI.DigitalActiveLevels? = subsystem.ApplyDigitalActiveLevel(digitalOutputLineNumber, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

        expectedPolarity = initialPolarity.Value
        activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}"
        actualPolarity = subsystem.ApplyDigitalActiveLevel(digitalOutputLineNumber, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

    End Sub

    ''' <summary> Configure digital output signal polarity. </summary>
    ''' <remarks> David, 2020-07-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalOutputLineNumber"> The digital output line number. </param>
    ''' <param name="subsystem">               The subsystem. </param>
    ''' <param name="outputSignalPolarity">    The output signal polarity. </param>
    Public Shared Sub ConfigureDigitalOutputSignalPolarity(ByVal digitalOutputLineNumber As Integer, ByVal subsystem As VI.DigitalOutputSubsystemBase,
                                                           ByVal outputSignalPolarity As VI.DigitalActiveLevels)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))

        Dim propertyName As String = String.Empty
        Dim deviceName As String = subsystem.ResourceNameCaption
        propertyName = $"{GetType(VI.DigitalOutputSubsystemBase)}.{NameOf(VI.DigitalOutputSubsystemBase.CurrentDigitalActiveLevel)}"
        Dim activity As String = $"Reading [{deviceName}].[{propertyName}({digitalOutputLineNumber})] initial value"
        Dim initialPolarity As VI.DigitalActiveLevels? = subsystem.QueryDigitalActiveLevel(digitalOutputLineNumber)
        Assert.IsTrue(initialPolarity.HasValue, $"{activity} should have a value")

        Dim expectedPolarity As VI.DigitalActiveLevels = outputSignalPolarity
        activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}"
        Dim actualPolarity As VI.DigitalActiveLevels? = subsystem.ApplyDigitalActiveLevel(digitalOutputLineNumber, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

    End Sub

    ''' <summary> Configure limit binning. </summary>
    ''' <param name="device">                 The device. </param>
    ''' <param name="outputSignalPolarities"> The output signal polarities. </param>
    Friend Shared Sub ConfigureLimitBinning(ByVal device As VI.K2002.K2002Device, ByVal outputSignalPolarities As VI.DigitalActiveLevels)

        Dim propertyName As String = String.Empty
        Dim title As String = device.OpenResourceTitle

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.PassSource)}"
        Dim expectedBinValue As Integer = ScannerMeterSettings.Get.PassOutputValue
        Dim actualBinValue As Integer = device.BinningSubsystem.ApplyPassSource(expectedBinValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2LowerLevel)}"
        Dim expectedValue As Double = ScannerMeterSettings.Get.ExpectedResistance * (1 - ScannerMeterSettings.Get.ResistanceTolerance)
        Dim actualValue As Double = device.BinningSubsystem.ApplyLimit2LowerLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2UpperLevel)}"
        expectedValue = ScannerMeterSettings.Get.ExpectedResistance * (1 + ScannerMeterSettings.Get.ResistanceTolerance)
        actualValue = device.BinningSubsystem.ApplyLimit2UpperLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2AutoClear)}"
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean = device.BinningSubsystem.ApplyLimit2AutoClear(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2Enabled)}"
        expectedBoolean = True
        actualBoolean = device.BinningSubsystem.ApplyLimit2Enabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.ApplyLimit2LowerSource)}"
        expectedBinValue = ScannerMeterSettings.Get.FailOutputValue
        actualBinValue = device.BinningSubsystem.ApplyLimit2LowerSource(expectedBinValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.ApplyLimit2UpperSource)}"
        expectedBinValue = ScannerMeterSettings.Get.FailOutputValue
        actualBinValue = device.BinningSubsystem.ApplyLimit2UpperSource(expectedBinValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1LowerLevel)}"
        expectedValue = ScannerMeterSettings.Get.ExpectedResistance * ScannerMeterSettings.Get.ResistanceTolerance
        actualValue = device.BinningSubsystem.ApplyLimit1LowerLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1UpperLevel)}"
        expectedValue = ScannerMeterSettings.Get.OpenLimit
        actualValue = device.BinningSubsystem.ApplyLimit1UpperLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1AutoClear)}"
        expectedBoolean = True
        actualBoolean = device.BinningSubsystem.ApplyLimit1AutoClear(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1Enabled)}"
        expectedBoolean = True
        actualBoolean = device.BinningSubsystem.ApplyLimit1Enabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.ApplyLimit1LowerSource)}"
        expectedBinValue = ScannerMeterSettings.Get.OutsideRangeOutputValue
        actualBinValue = device.BinningSubsystem.ApplyLimit1LowerSource(expectedBinValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.ApplyLimit1UpperSource)}"
        expectedBinValue = ScannerMeterSettings.Get.OutsideRangeOutputValue
        actualBinValue = device.BinningSubsystem.ApplyLimit1UpperSource(expectedBinValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.BinningStrobeEnabled)}"
        expectedBoolean = True
        actualBoolean = device.BinningSubsystem.ApplyBinningStrobeEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        For i As Integer = 1 To 4
            ScannerMeterTests.ConfigureDigitalOutputSignalPolarity(i, device.DigitalOutputSubsystem, DigitalActiveLevels.Low)
        Next

    End Sub

    ''' <summary> Configure measurement. </summary>
    ''' <param name="device"> The device. </param>
    Friend Shared Sub ConfigureMeasurement(ByVal device As VI.K2002.K2002Device)
        Dim propertyName As String = String.Empty
        Dim title As String = device.OpenResourceTitle

        propertyName = $"{GetType(VI.SystemSubsystemBase)}.{NameOf(VI.SystemSubsystemBase.AutoZeroEnabled)}"
        Dim expectedBoolean As Boolean = ScannerMeterSettings.Get.AutoZeroEnabled
        Dim actualBoolean As Boolean = device.SystemSubsystem.ApplyAutoZeroEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.FunctionMode)}"
        Dim expectedFunctionMode As VI.SenseFunctionModes = ScannerMeterSettings.Get.SenseFunction
        Dim actualFunctionMode As VI.SenseFunctionModes = device.SenseSubsystem.ApplyFunctionMode(expectedFunctionMode).GetValueOrDefault(VI.SenseFunctionModes.None)
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.Aperture)}"

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.PowerLineCycles)}"
        Dim expectedPowerLineCycles As Double = ScannerMeterSettings.Get.PowerLineCycles
        Dim actualPowerLineCycles As Double = device.SenseFunctionSubsystem.ApplyPowerLineCycles(expectedPowerLineCycles).GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, device.StatusSubsystemBase.LineFrequency.Value / TimeSpan.TicksPerSecond,
                        $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.AutoRangeEnabled)}"
        expectedBoolean = ScannerMeterSettings.Get.AutoRangeEnabled
        actualBoolean = device.SenseFunctionSubsystem.ApplyAutoRangeEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.ResolutionDigits)}"
        Dim expectedResolution As Double = ScannerMeterSettings.Get.ResolutionDigits
        Dim actualResolution As Double = device.SenseFunctionSubsystem.ApplyResolutionDigits(expectedResolution).GetValueOrDefault(0)
        Assert.AreEqual(expectedResolution, actualResolution, $"[{title}].[{propertyName}] should equal expected value")

    End Sub

    ''' <summary> Validates the trace. </summary>
    ''' <param name="device">          The device. </param>
    ''' <param name="triggerCount">    Number of triggers. </param>
    ''' <param name="sampleCount">     Number of samples. </param>
    ''' <param name="usingAutoPoints"> True to using automatic points. </param>
    Private Shared Sub ValidateTrace(ByVal device As VI.K2002.K2002Device, ByVal triggerCount As Integer, ByVal sampleCount As Integer, ByVal usingAutoPoints As Boolean)

        Dim propertyName As String = NameOf(VI.TraceSubsystemBase.FeedSource).SplitWords
        Dim title As String = device.OpenResourceTitle

        propertyName = NameOf(MeasurementEventBitmaskKey.BufferAvailable).SplitWords
        Assert.IsTrue(device.StatusSubsystemBase.MeasurementEventsBitmasks.ContainsKey(MeasurementEventBitmaskKey.BufferAvailable), $"[{title}].[{propertyName}] should be included in [{NameOf(StatusSubsystemBase.MeasurementEventsBitmasks).SplitWords}]")
        Assert.IsTrue(0 < device.StatusSubsystemBase.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferAvailable), $"[{title}].[{propertyName}] bitmask value should be greater than 0")

        propertyName = NameOf(StatusSubsystemBase.MeasurementEventCondition).SplitWords
        device.StatusSubsystem.MeasurementEventsBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
        Assert.IsFalse(device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn(MeasurementEventBitmaskKey.BufferAvailable),
                      $"[{title}].[{propertyName}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString.SplitWords}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferAvailable):X4} should be off after clearing the buffer")

        Dim expectedFeedSource As FeedSources = FeedSources.Sense
        Dim actualFeedSource As FeedSources? = device.TraceSubsystem.QueryFeedSource()
        Assert.IsTrue(actualFeedSource.HasValue, $"[{propertyName}] should have a value")
        Assert.AreEqual(expectedFeedSource, actualFeedSource.Value, $"{title}.[{propertyName}] should match")

        Dim expectedAutoEnabled As Boolean = usingAutoPoints
        Dim actualAutoPointsEnabeldCount As Boolean?
        propertyName = NameOf(TraceSubsystemBase.AutoPointsEnabled).SplitWords
        actualAutoPointsEnabeldCount = device.TraceSubsystem.QueryAutoPointsEnabled()
        Assert.AreEqual(expectedAutoEnabled, actualAutoPointsEnabeldCount.Value, $"{title}.[{propertyName}] should match")
        If usingAutoPoints Then
        ElseIf triggerCount > 1 Then
            Assert.AreEqual(expectedAutoEnabled, actualAutoPointsEnabeldCount.Value, $"{title}.[{propertyName}] should match")
            ' was used to testing.
            propertyName = NameOf(VI.TraceSubsystemBase.PointsCount).SplitWords
            Dim actualTraceCount As Integer? = device.TraceSubsystem.QueryPointsCount()
            Assert.IsTrue(actualTraceCount.HasValue, $"[{propertyName}] should have a value")
            Assert.AreEqual(triggerCount, actualTraceCount.Value, $"[{propertyName}] should match")
        Else
            propertyName = NameOf(VI.TraceSubsystemBase.PointsCount).SplitWords
            Dim actualTraceCount As Integer? = device.TraceSubsystem.QueryPointsCount()
            Assert.IsTrue(actualTraceCount.HasValue, $"[{propertyName}] should have a value")
            Assert.AreEqual(sampleCount, actualTraceCount.Value, $"[{propertyName}] should match")
        End If

        propertyName = NameOf(VI.TraceSubsystemBase.FeedControl).SplitWords
        Dim expectedFeedControl As FeedControls = FeedControls.Next
        Dim actualFeedControl As FeedControls? = device.TraceSubsystem.QueryFeedControl()
        Assert.IsTrue(actualFeedControl.HasValue, $"[{propertyName}] should have a value")
        Assert.AreEqual(expectedFeedControl, actualFeedControl.Value, $"[{propertyName}] should match")

    End Sub

    ''' <summary> Configure trace. </summary>
    ''' <param name="device">          The device. </param>
    ''' <param name="triggerCount">    Number of triggers. </param>
    ''' <param name="usingAutoPoints"> True to using automatic points. </param>
    Private Shared Sub ConfigureTrace(ByVal device As VI.K2002.K2002Device, ByVal triggerCount As Integer, ByVal usingAutoPoints As Boolean)
        Dim propertyName As String = NameOf(VI.TraceSubsystemBase.FeedSource).SplitWords
        Dim title As String = device.OpenResourceTitle
        device.TraceSubsystem.ClearBuffer()

        device.TraceSubsystem.BinningDuration = TimeSpan.FromMilliseconds(100)

        propertyName = NameOf(MeasurementEventBitmaskKey.BufferAvailable).SplitWords
        Assert.IsTrue(device.StatusSubsystemBase.MeasurementEventsBitmasks.ContainsKey(MeasurementEventBitmaskKey.BufferAvailable), $"[{title}].[{propertyName}] should be included in [{NameOf(StatusSubsystemBase.MeasurementEventsBitmasks).SplitWords}]")
        Assert.IsTrue(0 < device.StatusSubsystemBase.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferAvailable), $"[{title}].[{propertyName}] bitmask value should be greater than 0")

        propertyName = NameOf(StatusSubsystemBase.MeasurementEventCondition).SplitWords
        device.StatusSubsystem.MeasurementEventsBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
        Assert.IsFalse(device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn(MeasurementEventBitmaskKey.BufferAvailable),
                      $"[{title}].[{propertyName}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString.SplitWords}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferAvailable):X4} should be off after clearing the buffer")

        Dim expectedFeedSource As FeedSources = FeedSources.Sense
        Dim actualFeedSource As FeedSources? = device.TraceSubsystem.ApplyFeedSource(expectedFeedSource)
        Assert.IsTrue(actualFeedSource.HasValue, $"{title}.[{propertyName}] should have a value")
        Assert.AreEqual(expectedFeedSource, actualFeedSource.Value, $"{title}.[{propertyName}] should match")

        If usingAutoPoints Then
            propertyName = NameOf(TraceSubsystemBase.AutoPointsEnabled).SplitWords
            Dim expectedAutoPointsCount As Boolean = True
            Dim actualAutoPointsCount As Boolean? = device.TraceSubsystem.ApplyAutoPointsEnabled(expectedAutoPointsCount)
            Assert.AreEqual(expectedAutoPointsCount, actualAutoPointsCount.Value, $"{title}.[{propertyName}] should match")
        ElseIf triggerCount > 1 Then
            ' was used to testing.
            propertyName = NameOf(VI.TraceSubsystemBase.PointsCount).SplitWords
            Dim actualTraceCount As Integer? = device.TraceSubsystem.ApplyPointsCount(triggerCount)
            Assert.IsTrue(actualTraceCount.HasValue, $"{title}.[{propertyName}] should have a value")
            Assert.AreEqual(triggerCount, actualTraceCount.Value, $"{title}.[{propertyName}] should match")
        Else
            ' minimum buffer count is 2.
            triggerCount = 2
            propertyName = NameOf(VI.TraceSubsystemBase.PointsCount).SplitWords
            Dim actualTraceCount As Integer? = device.TraceSubsystem.ApplyPointsCount(triggerCount)
            Assert.IsTrue(actualTraceCount.HasValue, $"{title}.[{propertyName}] should have a value")
            Assert.AreEqual(triggerCount, actualTraceCount.Value, $"{title}.[{propertyName}] should match")
        End If

        ' the traced buffer includes only status values.
        device.TraceSubsystem.OrderedReadingElementTypes = New List(Of VI.ReadingElementTypes) From {VI.ReadingElementTypes.Reading}

        propertyName = NameOf(VI.TraceSubsystemBase.FeedControl).SplitWords
        Dim expectedFeedControl As FeedControls = FeedControls.Next
        Dim actualFeedControl As FeedControls? = device.TraceSubsystem.ApplyFeedControl(expectedFeedControl)
        Assert.IsTrue(actualFeedControl.HasValue, $"{title}.[{propertyName}] should have a value")
        Assert.AreEqual(expectedFeedControl, actualFeedControl.Value, $"{title}.[{propertyName}] should match")

    End Sub

    ''' <summary> Configure trace for fetching only. </summary>
    ''' <param name="device"> The device. </param>
    Private Shared Sub ConfigureTrace(ByVal device As VI.K2002.K2002Device)
        Dim propertyName As String = NameOf(VI.TraceSubsystemBase.FeedSource).SplitWords
        Dim title As String = device.OpenResourceTitle
        device.TraceSubsystem.ClearBuffer()

        device.TraceSubsystem.BinningDuration = TimeSpan.FromMilliseconds(100)

        propertyName = NameOf(MeasurementEventBitmaskKey.BufferAvailable).SplitWords
        Assert.IsTrue(device.StatusSubsystemBase.MeasurementEventsBitmasks.ContainsKey(MeasurementEventBitmaskKey.BufferAvailable), $"[{title}].[{propertyName}] should be included in [{NameOf(StatusSubsystemBase.MeasurementEventsBitmasks).SplitWords}]")
        Assert.IsTrue(0 < device.StatusSubsystemBase.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferAvailable), $"[{title}].[{propertyName}] bitmask value should be greater than 0")

        propertyName = NameOf(StatusSubsystemBase.MeasurementEventCondition).SplitWords
        device.StatusSubsystem.MeasurementEventsBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
        Assert.IsFalse(device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn(MeasurementEventBitmaskKey.BufferAvailable),
                      $"[{title}].[{propertyName}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString.SplitWords}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferAvailable):X4} should be off after clearing the buffer")

        propertyName = NameOf(TraceSubsystemBase.FeedSource).SplitWords
        Dim expectedFeedSource As FeedSources = FeedSources.Sense
        Dim actualFeedSource As FeedSources? = device.TraceSubsystem.ApplyFeedSource(expectedFeedSource)
        Assert.IsTrue(actualFeedSource.HasValue, $"{title}.[{propertyName}] should have a value")
        Assert.AreEqual(expectedFeedSource, actualFeedSource.Value, $"{title}.[{propertyName}] should match")

        ' the fetched buffer includes only reading values.
        device.TraceSubsystem.OrderedReadingElementTypes = New List(Of ReadingElementTypes) From {VI.ReadingElementTypes.Reading}

        propertyName = NameOf(VI.TraceSubsystemBase.FeedControl).SplitWords
        Dim expectedFeedControl As FeedControls = FeedControls.Never
        Dim actualFeedControl As FeedControls? = device.TraceSubsystem.ApplyFeedControl(expectedFeedControl)
        Assert.IsTrue(actualFeedControl.HasValue, $"{title}.[{propertyName}] should have a value")
        Assert.AreEqual(expectedFeedControl, actualFeedControl.Value, $"{title}.[{propertyName}] should match")

    End Sub

    ''' <summary> Configure external trigger measurements. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="device">        The device. </param>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="sampleCount">   Number of samples. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    Friend Shared Sub ConfigureTriggerPlan(ByVal device As VI.K2002.K2002Device, ByVal triggerCount As Integer,
                                           ByVal sampleCount As Integer, ByVal triggerSource As VI.TriggerSources)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords

        device.ClearExecutionState()
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.TriggerSubsystem.Abort()
        device.Session.EnableServiceRequestWaitComplete()
        device.Session.ReadStatusRegister()

        device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
        device.RouteSubsystem.QueryClosedChannels()

        device.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer1Subsystem.ApplyArmCount(1)

        propertyName = NameOf(VI.ArmLayerSubsystemBase.ArmLayerBypassMode).SplitWords
        Dim expectedBypassMode As VI.TriggerLayerBypassModes = TriggerLayerBypassModes.Acceptor
        Dim actualBypassMode As VI.TriggerLayerBypassModes? = device.ArmLayer1Subsystem.ApplyArmLayerBypassMode(expectedBypassMode)
        Assert.AreEqual(Of VI.TriggerLayerBypassModes)(expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for arm layer should match")

        device.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer2Subsystem.ApplyArmCount(sampleCount)
        device.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)

        propertyName = NameOf(VI.ArmLayerSubsystemBase.ArmLayerBypassMode).SplitWords
        expectedBypassMode = TriggerLayerBypassModes.Acceptor
        actualBypassMode = device.ArmLayer2Subsystem.ApplyArmLayerBypassMode(expectedBypassMode)
        Assert.AreEqual(Of VI.TriggerLayerBypassModes)(expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for scan layer should match")

        ' device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.External)
        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords
        Dim expectedTriggerSource As VI.TriggerSources = triggerSource
        Dim actualTriggerSource As VI.TriggerSources? = device.TriggerSubsystem.ApplyTriggerSource(expectedTriggerSource)
        Assert.IsTrue(actualTriggerSource.HasValue, $"{title}.[{propertyName}] should have a value")
        Assert.AreEqual(Of VI.TriggerSources)(expectedTriggerSource, actualTriggerSource.Value, $"{title}.[{propertyName}] should match")

        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerCount).SplitWords
        If triggerCount = Integer.MaxValue Then
            Dim actualTriggerCount As Integer = device.TriggerSubsystem.ApplyTriggerCount(triggerCount).GetValueOrDefault(0)
            Assert.AreEqual(triggerCount, actualTriggerCount, $"{title}.[{propertyName}] should match")
        Else
            Dim actualTriggerCount As Integer = device.TriggerSubsystem.ApplyTriggerCount(triggerCount).GetValueOrDefault(0)
            Assert.AreEqual(triggerCount, actualTriggerCount, $"{title}.[{propertyName}] should match")
        End If

        propertyName = NameOf(VI.TriggerSubsystemBase.Delay).SplitWords
        Dim actualDelay As TimeSpan = device.TriggerSubsystem.ApplyDelay(TimeSpan.Zero).GetValueOrDefault(TimeSpan.FromSeconds(10))
        Assert.AreEqual(TimeSpan.Zero, actualDelay, $"{title}.[{propertyName}] should match")

        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerLayerBypassMode).SplitWords
        expectedBypassMode = TriggerLayerBypassModes.Acceptor
        actualBypassMode = device.TriggerSubsystem.ApplyTriggerLayerBypassMode(expectedBypassMode)
        Assert.AreEqual(Of VI.TriggerLayerBypassModes)(expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for measure layer should match")

    End Sub

    ''' <summary> Configure bus trigger plan. </summary>
    ''' <param name="device"> The device. </param>
    Friend Shared Sub ConfigureBusTriggerPlan(ByVal device As VI.K7000.K7000Device)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords

        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.TriggerSubsystem.Abort()
        device.Session.EnableServiceRequestWaitComplete()
        device.RouteSubsystem.WriteOpenAll(TimeSpan.FromSeconds(1))
        device.Session.ReadStatusRegister()

        device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
        device.RouteSubsystem.QueryClosedChannels()

        propertyName = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords
        Dim expectedScanList As String = K7000SubsystemInfo.BusTriggerTestScanList
        Dim actualScanList As String = device.RouteSubsystem.ApplyScanList(expectedScanList)
        Assert.AreEqual(expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'")

        device.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer1Subsystem.ApplyArmCount(1)

        Dim expectedBypassMode As VI.TriggerLayerBypassModes = TriggerLayerBypassModes.Acceptor
        Dim actualBypassMode As VI.TriggerLayerBypassModes? = device.ArmLayer1Subsystem.ApplyArmLayerBypassMode(expectedBypassMode)
        Assert.AreEqual(Of VI.TriggerLayerBypassModes)(expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for arm layer should match")

        ' 7001 has only a single arm layer
        'device.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        'device.ArmLayer2Subsystem.ApplyArmCount(1)
        'device.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)

        ' device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Bus)
        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords
        Dim expectedTriggerSource As VI.TriggerSources = K7000SubsystemInfo.BusTriggerTestTriggerSource
        Dim actualTriggerSource As VI.TriggerSources? = device.TriggerSubsystem.ApplyTriggerSource(expectedTriggerSource)
        Assert.IsTrue(actualTriggerSource.HasValue, $"{propertyName} should have a value")
        Assert.AreEqual(Of VI.TriggerSources)(expectedTriggerSource, actualTriggerSource.Value, $"{propertyName} should match")

        device.TriggerSubsystem.ApplyTriggerCount(9999) ' in place of infinite
        device.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)

        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerLayerBypassMode).SplitWords
        expectedBypassMode = TriggerLayerBypassModes.Acceptor
        actualBypassMode = device.TriggerSubsystem.ApplyTriggerLayerBypassMode(expectedBypassMode)
        Assert.AreEqual(Of VI.TriggerLayerBypassModes)(expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for scan layer should match")
    End Sub

    ''' <summary> Assert measured values. </summary>
    ''' <param name="device"> Reference to the K2002 device. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process show buffer readings in this
    ''' collection.
    ''' </returns>
    Friend Shared Function ShowBufferReadings(ByVal device As VI.K2002.K2002Device) As IEnumerable(Of BufferReading)

        Dim actualBufferReadingsCount As Integer = device.TraceSubsystem.AddAllBufferReadings()
        Dim propertyName As String = NameOf(Arebis.TypedUnits.Amount)
        Dim title As String = device.OpenResourceTitle

        Dim readingNumber As Integer = 0
        Dim bufferReadings As IEnumerable(Of BufferReading) = device.TraceSubsystem.DequeueRange(device.TraceSubsystem.NewReadingsCount)

        ' build the buffer readings amounts
        For Each bufferReading As BufferReading In bufferReadings
            readingNumber += 1
            ' ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Reading}")
            bufferReading.BuildAmount(device.SenseSubsystem.FunctionUnit)
            ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Amount}")
        Next
        Return bufferReadings
    End Function

    ''' <summary> Assert measured values. </summary>
    ''' <param name="bufferReadings"> The buffer readings. </param>
    ''' <param name="expectedCount">  Number of expected. </param>
    Private Shared Sub ValidateBufferReadings(ByVal bufferReadings As IEnumerable(Of BufferReading), ByVal expectedCount As Integer)

        Assert.AreEqual(expectedCount, bufferReadings.Count, $"Buffer reading count should match")

        Dim propertyName As String = NameOf(Arebis.TypedUnits.Amount)
        Dim expectedResistance As Double = ScannerMeterSettings.Get.ExpectedResistance
        Dim epsilon As Double = expectedResistance * ScannerMeterSettings.Get.ResistanceTolerance
        Dim measuredResistance As Double = 0
        Dim readingNumber As Integer = 0
        propertyName = NameOf(measuredResistance).SplitWords
        readingNumber = 0
        For Each bufferReading As BufferReading In bufferReadings
            readingNumber += 1
            Assert.IsNotNull(bufferReading.Amount, $"Reading #{readingNumber} should not be null")
            measuredResistance = bufferReading.Amount.Value
            Assert.AreEqual(expectedResistance, measuredResistance, epsilon,
                                $"{propertyName}={measuredResistance} but should equals {expectedResistance} within {epsilon}")
        Next
    End Sub

    ''' <summary> Initiate trigger plan. </summary>
    ''' <param name="device"> The Keithley 2002 device. </param>
    Private Shared Sub InitiateTriggerPlan(ByVal device As VI.K2002.K2002Device)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords

        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)

        Dim measurementEvent As MeasurementEventBitmaskKey = MeasurementEventBitmaskKey.BufferAvailable
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        Dim actualOperationIdle As Boolean = True

        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        operationBitmasks.Status = device.StatusSubsystem.QueryOperationEventCondition.Value
        actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
        Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is initiated")
        ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{device.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

        If device.TraceSubsystem.FeedControl.Value = FeedControls.Next Then

            ' clearing the buffer eliminated the buffer trace.
            device.TraceSubsystem.ClearBuffer(VI.FeedControls.Next)

            ' clearing the buffer changes feed control to Never; the above command should restore the feed control but validating nonetheless:
            'Dim usingTraceAutoPoints As Boolean = device.TraceSubsystem.QueryAutoPointsEnabled.Value
            'ScannerMeterTests.ValidateTrace(device, device.TriggerSubsystem.TriggerCount.Value, device.ArmLayer2Subsystem.ArmCount.Value, usingTraceAutoPoints)
        End If

        device.TraceSubsystem.BufferReadingsBindingList.Clear()
        device.TraceSubsystem.ClearBufferReadingsQueue()

        ' start the K2002 trigger model
        device.TriggerSubsystem.Initiate()

        operationBitmasks.Status = device.StatusSubsystem.QueryOperationEventCondition.Value
        actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
        Assert.IsFalse(actualOperationIdle, $"[{title}].[{propertyName}] should be false after triggering is initiated")
        ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{device.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

        ' wait twice the aperture time.
        isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(2 * device.SenseFunctionSubsystem.Aperture.Value))

        If device.TraceSubsystem.FeedControl.Value = FeedControls.Next Then
            ' test if buffer is available; which means the instrument triggered already. 
            measurementEvent = MeasurementEventBitmaskKey.BufferAvailable
            propertyName = NameOf(MeasurementEventBitmaskKey.BufferAvailable).SplitWords
            measurementBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
            Assert.IsFalse(measurementBitmasks.IsAnyBitOn(measurementEvent),
                       $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString.SplitWords}]=0x{measurementBitmasks(measurementEvent):X4} should be off upon initiating the trigger plan")
        End If

    End Sub

    ''' <summary> Fetches the buffer readings in this collection. </summary>
    ''' <remarks> David, 2020-04-13. </remarks>
    ''' <param name="device"> Reference to the K2002 device. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the buffer readings in this
    ''' collection.
    ''' </returns>
    Friend Shared Function FetchBufferReadings(ByVal device As VI.K2002.K2002Device) As IEnumerable(Of BufferReading)

        Dim title As String = device.OpenResourceTitle
        Dim bufferReadings As IEnumerable(Of BufferReading) = device.TraceSubsystem.DequeueRange(device.TraceSubsystem.NewReadingsCount)
        Dim expectedResistance As Double = ScannerMeterSettings.Get.ExpectedResistance
        Dim epsilon As Double = expectedResistance * ScannerMeterSettings.Get.ResistanceTolerance

        ' trace (display) the buffer readings amounts
        Dim readingNumber As Integer = 0
        For Each bufferReading As BufferReading In bufferReadings
            readingNumber += 1
            ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4} {bufferReading.FractionalTimespan.TotalMilliseconds:0}ms")
        Next
        Return bufferReadings

    End Function

    ''' <summary> Assert buffer readings. </summary>
    ''' <param name="device"> The 2002. </param>
    Private Shared Sub AssertBufferReadings(ByVal device As VI.K2002.K2002Device)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String
        Dim bufferReadings As IEnumerable(Of BufferReading) = device.TraceSubsystem.DequeueRange(device.TraceSubsystem.NewReadingsCount)
        Dim expectedResistance As Double = ScannerMeterSettings.Get.ExpectedResistance
        Dim epsilon As Double = expectedResistance * ScannerMeterSettings.Get.ResistanceTolerance

        ' trace (display) the buffer readings amounts
        Dim readingNumber As Integer = 0
        For Each bufferReading As BufferReading In bufferReadings
            readingNumber += 1
            ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4} {bufferReading.FractionalTimespan.TotalMilliseconds:0}ms")
        Next

        Dim measuredResistance As Double
        propertyName = NameOf(measuredResistance).SplitWords
        If device.SenseSubsystem.FunctionMode.Value = ScannerMeterSettings.Get.SenseFunction Then
            readingNumber = 0
            For Each bufferReading As BufferReading In bufferReadings
                readingNumber += 1
                Assert.IsNotNull(bufferReading.Amount, $"[{title}].[Reading #{readingNumber}] should not be null")
                measuredResistance = bufferReading.Amount.Value
                Assert.AreEqual(expectedResistance, measuredResistance, epsilon,
                                $"[{title}].[{propertyName}]={measuredResistance} but should equals {expectedResistance} within {epsilon}")
            Next
        End If

        ' validate binding list count
        Assert.AreEqual(device.TraceSubsystem.BufferReadingsCount, bufferReadings.Count, "Buffer readings count should match the binding list count")

    End Sub

#End Region

#Region " BUS TRIGGERS BUFFER TRACE "

    ''' <summary> A register value. </summary>
    Private Class RegisterValue

        ''' <summary> Constructor. </summary>
        ''' <param name="formatString"> The format string. </param>
        Public Sub New(ByVal formatString As String)
            MyBase.New
            Me.FormatString = formatString
        End Sub

        ''' <summary> Gets or sets the format string. </summary>
        ''' <value> The format string. </value>
        Public Property FormatString As String = "SRE: 0x{0:X2} 0x{1:X2}"

        ''' <summary> Gets or sets the pre trigger value. </summary>
        ''' <value> The pre trigger value. </value>
        Public Property PreTriggerValue As Integer

        ''' <summary> Gets or sets the post trigger value. </summary>
        ''' <value> The post trigger value. </value>
        Public Property PostTriggerValue As Integer

        ''' <summary> Gets the outcome. </summary>
        ''' <returns> A String. </returns>
        Public Function Outcome() As String
            Return String.Format(Me.FormatString, Me.PreTriggerValue, Me.PostTriggerValue)
        End Function

    End Class

    ''' <summary> Assert bus trigger buffer trace. </summary>
    ''' <remarks>
    ''' Seeing the operation reading, suggests a plan for streaming the measurement status of an
    ''' infinite meter trigger plan.  
    ''' <list type="bullet">SRQ and Operation Event Status<item>
    ''' OperationEventCondition=0x0000  </item><item>
    '''     SRQ: Pre;  Post. OSB: Pre;    Post  </item><item>
    ''' #1: SRQ: 0x00; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #2: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #3: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #4: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #5: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #6: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #7: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #8: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #9: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
    ''' #10: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0410  </item><item>
    ''' Reading #1=100.28211 Ω  </item><item>
    ''' Reading #2=100.28249 Ω  </item><item>
    ''' Reading #3=100.2822 Ω  </item><item>
    ''' Reading #4=100.28211 Ω  </item><item>
    ''' Reading #5=100.28218 Ω  </item><item>
    ''' Reading #6=100.28239 Ω  </item><item>
    ''' Reading #7=100.28221 Ω  </item><item>
    ''' Reading #8=100.28232 Ω  </item><item>
    ''' Reading #9=100.28233 Ω  </item><item>
    ''' Reading #10=100.2827 Ω  </item><item>
    ''' OperationEventCondition=0x0400  </item></list>
    ''' </remarks>
    ''' <param name="k7000"> The Keithley 7001 device. </param>
    ''' <param name="k2002"> The Keithley 2002 device. </param>
    Friend Shared Sub AssertBusTriggersBufferTrace(ByVal k7000 As VI.K7000.K7000Device, ByVal k2002 As VI.K2002.K2002Device)

        Dim actualOperationIdle As Boolean = True
        Dim title As String = k2002.OpenResourceTitle
        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords
        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        Dim measurementEvent As MeasurementEventBitmaskKey = MeasurementEventBitmaskKey.BufferAvailable
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        ' enable all measurement status bits.
        k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask(&H7FFF)
        k2002.StatusSubsystem.ApplyOperationEventEnableBitmask(&H7FFF)
        Dim srq As New RegisterValue("SRQ: 0x{0:X2} 0x{1:X2}")
        Dim osb As New RegisterValue("OSB: 0x{0:X4} 0x{1:X4}")
        Dim msb As New RegisterValue("MSB: 0x{0:X4} 0x{1:X4}")
        ScannerMeterTests.InitiateTriggerPlan(k2002)

        Try
            ' start the K7000 trigger model
            k7000.TriggerSubsystem.Initiate()
            Try
                ' Note: device error -211 if query closed channels: Trigger ignored.
                TestInfo.TraceMessage($"    SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post  ")
                For i As Integer = 1 To K7000SubsystemInfo.BusTriggerTestTriggerCount
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    srq.PreTriggerValue = k2002.Session.ReadStatusByte
                    osb.PreTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus.Value
                    msb.PreTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition.Value
                    k7000.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    srq.PostTriggerValue = k2002.Session.ReadStatusByte
                    osb.PostTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus.Value
                    msb.PostTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition.Value
                    TestInfo.TraceMessage($"#{i}: {srq.Outcome} {osb.Outcome} {msb.Outcome}")
                Next

                propertyName = NameOf(StatusSubsystemBase.MeasurementEventCondition).SplitWords
                measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
                Assert.IsTrue(measurementBitmasks.IsAnyBitOn(MeasurementEventBitmaskKey.BufferAvailable),
                              $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString.SplitWords}]=0x{measurementBitmasks(MeasurementEventBitmaskKey.BufferAvailable):X4} should be on after triggering is completed")

                propertyName = NameOf(StatusSubsystemBase.QueryOperationEventCondition).SplitWords
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks(OperationEventBitmaskKey.Idle):X4} bit should high (idle) after triggering is completed")

            Catch
                Throw
            Finally
                k7000.TriggerSubsystem.Abort()
                k7000.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
                k7000.RouteSubsystem.QueryClosedChannels()
            End Try
        Catch
            Throw
        Finally
            k2002.TriggerSubsystem.Abort()
            ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
            actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
            Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted")
        End Try

    End Sub

    ''' <summary> Request binning strobe. </summary>
    ''' <remarks> David, 2020-08-25. </remarks>
    ''' <param name="device"> The device. </param>
    ''' <param name="pass">   True to pass. </param>
    Public Shared Sub RequestBinningStrobe(ByVal device As VI.K2002.K2002Device, ByVal pass As Boolean)
        device.TraceSubsystem.BinningStrobeAction = Sub()
                                                        device.DigitalOutputSubsystem.Strobe(4, TimeSpan.FromMilliseconds(10),
                                                                                             If(pass, 1, 2),
                                                                                             TimeSpan.FromMilliseconds(20))
                                                    End Sub
        device.TraceSubsystem.BinningStrobeRequested = True
    End Sub

    ''' <summary> Assert bus trigger buffer trace. </summary>
    ''' <remarks>
    ''' Seeing the operation reading, suggests a plan for streaming the measurement status of an
    ''' infinite meter trigger plan.  
    ''' <list type="bullet">SRQ and Operation Event Status 2020/04/09<item>
    ''' OperationEventCondition=0x0400 </item><item>
    ''' OperationEventCondition=0x0000 </item><item>
    '''     SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post   </item><item>
    ''' #1: SRQ: 0x20 0xA1 OSB: 0x0000 0x0010 MSB: 0x0000 0x0080 Buffer available </item><item>
    ''' #2: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
    ''' #3: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
    ''' #4: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
    ''' #5: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0180 Buffer available+ Half Full
    ''' </item><item>
    ''' #6: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
    ''' #7: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
    ''' #8: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
    ''' #9: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180  </item><item>
    ''' #10: SRQ: 0x21 0xA1 OSB: 0x0000 0x0410 MSB: 0x0180 0x0380 Buffer available + Half Full + Full
    ''' </item><item>
    ''' Reading #1=100.138885 Ω  </item><item>
    ''' Reading #2=100.13842 Ω </item><item>
    ''' Reading #3=100.137993 Ω  </item><item>
    ''' Reading #4=100.13868 Ω  </item><item>
    ''' Reading #5=100.138382 Ω  </item><item>
    ''' Reading #6=100.137764 Ω  </item><item>
    ''' Reading #7=100.138092 Ω  </item><item>
    ''' Reading #8=100.137527 Ω  </item><item>
    ''' Reading #9=100.137726 Ω  </item><item>
    ''' Reading #10=100.137268 Ω  </item></list>
    ''' OperationEventCondition=0x0400.
    ''' </remarks>
    ''' <param name="k2002">        The Keithley 2002 device. </param>
    ''' <param name="triggerCount"> Number of triggers. </param>
    ''' <param name="delay">        The delay. </param>
    Friend Shared Sub AssertBusTriggersBufferTrace(ByVal k2002 As VI.K2002.K2002Device, ByVal triggerCount As Integer, ByVal delay As TimeSpan)

        Dim actualOperationIdle As Boolean = True
        Dim title As String = k2002.OpenResourceTitle
        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords
        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        Dim measurementEvent As MeasurementEventBitmaskKey = MeasurementEventBitmaskKey.BufferFull
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        ' enable all measurement status bits.
        k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask(&H7FFF)
        k2002.StatusSubsystem.ApplyOperationEventEnableBitmask(&H7FFF)
        Dim srq As New RegisterValue("SRQ: 0x{0:X2} 0x{1:X2}")
        Dim osb As New RegisterValue("OSB: 0x{0:X4} 0x{1:X4}")
        Dim msb As New RegisterValue("MSB: 0x{0:X4} 0x{1:X4}")

        ScannerMeterTests.InitiateTriggerPlan(k2002)

        Try
            Try
                ' Note: device error -211 if query closed channels: Trigger ignored.
                TestInfo.TraceMessage($"    SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post  ")
                For i As Integer = 1 To triggerCount
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    srq.PreTriggerValue = k2002.Session.ReadStatusByte
                    osb.PreTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus.Value
                    msb.PreTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition.Value
                    k2002.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(delay)
                    srq.PostTriggerValue = k2002.Session.ReadStatusByte
                    osb.PostTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus.Value
                    msb.PostTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition.Value
                    TestInfo.TraceMessage($"#{i}: {srq.Outcome} {osb.Outcome} {msb.Outcome}")
                Next

                propertyName = NameOf(StatusSubsystemBase.MeasurementEventCondition).SplitWords
                measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
                Assert.IsTrue(measurementBitmasks.IsAnyBitOn(MeasurementEventBitmaskKey.BufferAvailable),
                              $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString.SplitWords}]=0x{measurementBitmasks(MeasurementEventBitmaskKey.BufferAvailable):X4} should be on after triggering is completed")

                propertyName = NameOf(StatusSubsystemBase.QueryOperationEventCondition).SplitWords
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks(OperationEventBitmaskKey.Idle):X4} bit should high (idle) after triggering is completed")

            Catch
                Throw
            Finally
            End Try
        Catch
            Throw
        Finally
            k2002.TriggerSubsystem.Abort()
            ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
            actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
            Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted")
        End Try

    End Sub

    ''' <summary> (Unit Test Method) tests bus trigger buffer trace. </summary>
    ''' <remarks> Passed 2020-01-10. </remarks>
    Public Shared Sub AssertBusTriggersBufferTraceTest()
        If Not K2002ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K2002ResourceInfo.ResourceTitle} not found")
        If Not K7000ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K7000ResourceInfo.ResourceTitle} not found")
        Using k7000Device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            k7000Device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using K2002Device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                K2002Device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, K2002Device, K2002ResourceInfo)
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, k7000Device, K7000ResourceInfo)
                    Assert.IsTrue(K2002Device.SystemSubsystem.QueryFrontTerminalsSelected.Value, $"Front terminal must be selected")
                    ScannerMeterTests.ConfigureTriggerPlan(K2002Device, K7000SubsystemInfo.BusTriggerTestTriggerCount, 1, ScannerMeterSettings.Get.MeterTriggerSource)
                    ' ScannerMeterTests.ConfigureTriggerPlan(K2002Device, Integer.MaxValue, 1, ScannerMeterSettings.Get.MeterTriggerSource)
                    ScannerMeterTests.ConfigureMeasurement(K2002Device)
                    ScannerMeterTests.ConfigureLimitBinning(K2002Device, DigitalActiveLevels.Low)
                    ScannerMeterTests.ConfigureTrace(K2002Device, K7000SubsystemInfo.BusTriggerTestTriggerCount, True)
                    If VI.TriggerSources.External = ScannerMeterSettings.Get.MeterTriggerSource Then
                        ScannerMeterTests.ConfigureBusTriggerPlan(k7000Device)
                        ScannerMeterTests.AssertBusTriggersBufferTrace(k7000Device, K2002Device)
                    ElseIf VI.TriggerSources.Bus = ScannerMeterSettings.Get.MeterTriggerSource Then
                        ScannerMeterTests.AssertBusTriggersBufferTrace(K2002Device, K7000SubsystemInfo.BusTriggerTestTriggerCount, K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    End If
                    If K2002Device.TraceSubsystem.IsDataTraceEnabled Then
                        ScannerMeterTests.ValidateBufferReadings(ScannerMeterTests.ShowBufferReadings(K2002Device), K7000SubsystemInfo.BusTriggerTestTriggerCount)
                    End If
                Catch
                    Throw
                Finally
                    K2002Tests.DeviceManager.CloseSession(TestInfo, k7000Device)
                    K2002Tests.DeviceManager.CloseSession(TestInfo, K2002Device)
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests bus trigger buffer trace using meter bus. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub MeterBusTriggersBufferTraceTest()
        ScannerMeterSettings.Get.MeterTriggerSource = TriggerSources.Bus
        ScannerMeterTests.AssertBusTriggersBufferTraceTest()
    End Sub

    ''' <summary> (Unit Test Method) tests bus trigger buffer trace using scanner bus. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub ScannerBusTriggersBufferTraceTest()
        ScannerMeterSettings.Get.MeterTriggerSource = TriggerSources.External
        ScannerMeterTests.AssertBusTriggersBufferTraceTest()
    End Sub


#End Region

#Region " BUS TRIGGERS FETCH READING "

    ''' <summary> Fetches buffer reading. </summary>
    ''' <remarks> Does not assume the trigger plan is idle. </remarks>
    ''' <param name="device">           The 2002. </param>
    ''' <param name="triggerNumber">    The trigger number. </param>
    ''' <param name="initialTimestamp"> The initial timestamp. </param>
    ''' <param name="elapsedTime">      The elapsed time. </param>
    Private Shared Sub FetchBufferReading(ByVal device As VI.K2002.K2002Device, ByVal triggerNumber As Integer,
                                          ByVal initialTimestamp As DateTimeOffset, ByVal elapsedTime As TimeSpan)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords

        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)

        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        ' the measurement condition is read without affecting its value (unlike the measurement event status)
        'measurementEvent = MeasurementEventBitmask.ReadingAvailable
        'propertyName = NameOf(MeasurementEventBitmask.ReadingAvailable).SplitWords
        'measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
        'Assert.IsTrue(measurementBitmasks.IsAnyBitOn(measurementEvent), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} bitmask 0x{measurementBitmasks(measurementEvent):X4} should be on after the first trigger")

        'propertyName = NameOf(MeasurementEventBitmask.ReadingAvailable).SplitWords
        'measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
        'Assert.IsTrue(measurementBitmasks.IsAnyBitOn(measurementEvent), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} bitmask 0x{measurementBitmasks(measurementEvent):X4} should be on after the first trigger")

        propertyName = isr.VI.MeasurementEventBitmaskKey.ReadingAvailable.ToString.SplitWords
        Dim timedOut As Boolean
        Dim actualBitmask As Integer?
        Dim expectedBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(isr.VI.MeasurementEventBitmaskKey.ReadingAvailable)
        Dim task As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer?)) = Pith.SessionBase.StartAwaitingBitmaskTask(expectedBitmask, TimeSpan.FromMilliseconds(200),
                                                                                                                                   Function() device.StatusSubsystem.QueryMeasurementEventStatus)
        timedOut = task.Result.TimedOut
        actualBitmask = task.Result.Status

        Assert.IsFalse(timedOut, $"Trigger#{triggerNumber}: {propertyName} should not timeout")

        propertyName = NameOf(StatusSubsystemBase.MeasurementEventStatus).SplitWords
        Assert.AreEqual(expectedBitmask, actualBitmask And expectedBitmask, $"Trigger#{triggerNumber}: {propertyName} should match")

        Dim actualNewReadingsCount As Integer = device.TraceSubsystem.AddBufferReadings(device.SenseSubsystem.FunctionUnit, initialTimestamp, elapsedTime)

        Dim expectedReadingCount As Integer = 1
        Assert.AreEqual(expectedReadingCount, actualNewReadingsCount, "Reading count should match")

        Assert.AreEqual(triggerNumber, device.TraceSubsystem.BufferReadingsBindingList.Count, "Number of reading in binding list should match the trigger number")

    End Sub

    ''' <summary> Assert bus trigger fetch stream. </summary>
    ''' <remarks>
    ''' The operation register triggering bit, which turns on after the trigger, can be used to
    ''' identify the presence or a reading. At this time, it seems the MSB is not reading available
    ''' is not turned on even after a long delay.
    ''' <list type="bullet">2020-01-10<item>
    ''' OperationEventCondition=0x0400  </item><item>
    ''' OperationEventCondition=0x0000  </item><item>
    ''' Reading #1=100.28147 Ω 0x0 1007ms  </item><item>
    ''' Reading #2=100.28132 Ω 0x0 2086ms  </item><item>
    ''' Reading #3=100.28106 Ω 0x0 3140ms  </item><item>
    ''' Reading #4=100.28106 Ω 0x0 4192ms  </item><item>
    ''' Reading #5=100.28121 Ω 0x0 5247ms  </item><item>
    ''' Reading #6=100.28151 Ω 0x0 6300ms  </item><item>
    ''' Reading #7=100.28113 Ω 0x0 7351ms  </item><item>
    ''' Reading #8=100.28138 Ω 0x0 8401ms  </item><item>
    ''' Reading #9=100.28144 Ω 0x0 9458ms  </item><item>
    ''' Reading #10=100.28175 Ω 0x0 10519ms  </item><item>
    ''' OperationEventCondition=0x0400      </item></list>
    ''' </remarks>
    ''' <param name="k7000"> The Keithley 7001 device. </param>
    ''' <param name="k2002"> The Keithley 2002 device. </param>
    Private Shared Sub AssertBusTriggersFetchReading(ByVal k7000 As VI.K7000.K7000Device, ByVal k2002 As VI.K2002.K2002Device)

        Dim actualOperationIdle As Boolean = True
        Dim title As String = k2002.OpenResourceTitle
        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords
        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        ' enable all measurement status bits.
        k2002.Session.ApplyServiceRequestEnableBitmask(k2002.Session.DefaultServiceRequestEnableBitmask)
        k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask(&H7FFF)
        k2002.StatusSubsystem.ApplyOperationEventEnableBitmask(&H7FFF)

        ScannerMeterTests.InitiateTriggerPlan(k2002)

        Try
            ' start the K7000 trigger model
            k7000.TriggerSubsystem.Initiate()
            Dim initalTimestamp As DateTimeOffset = DateTimeOffset.Now
            Dim sw As Stopwatch = Stopwatch.StartNew
            Try
                For i As Integer = 1 To K7000SubsystemInfo.BusTriggerTestTriggerCount
                    ' this delay is used to allow the tester to see the readings come in on the instruments.
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    k7000.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    ScannerMeterTests.FetchBufferReading(k2002, i, initalTimestamp, sw.Elapsed)
                Next

                propertyName = NameOf(StatusSubsystemBase.QueryOperationEventCondition).SplitWords
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks(OperationEventBitmaskKey.Idle):X4} bit should high (idle) after triggering is completed")

                ScannerMeterTests.AssertBufferReadings(k2002)

            Catch
                Throw
            Finally
                k7000.TriggerSubsystem.Abort()
                k7000.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
                k7000.RouteSubsystem.QueryClosedChannels()
            End Try
        Catch
            Throw
        Finally
            k2002.TriggerSubsystem.Abort()
            ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
            actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
            Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted")
        End Try

    End Sub

    ''' <summary> Assert bus trigger fetch stream. </summary>
    ''' <remarks>
    ''' The operation register triggering bit, which turns on after the trigger, can be used to
    ''' identify the presence or a reading. At this time, it seems the MSB is not reading available
    ''' is not turned on even after a long delay.
    ''' <list type="bullet">2020-04-09<item>
    ''' OperationEventCondition=0x0400</item><item>
    ''' OperationEventCondition=0x0000</item><item>
    ''' Reading #1=100.181211 Ω 0x0000 1006ms</item><item>
    ''' Reading #2=100.17957 Ω 0x0000 2071ms</item><item>
    ''' Reading #3=100.178239 Ω 0x0000 3121ms</item><item>
    ''' Reading #4=100.175739 Ω 0x0000 4175ms</item><item>
    ''' Reading #5=100.176403 Ω 0x0000 5227ms</item><item>
    ''' Reading #6=100.176956 Ω 0x0000 6279ms</item><item>
    ''' Reading #7=100.176946 Ω 0x0000 7328ms</item><item>
    ''' Reading #8=100.174168 Ω 0x0000 8380ms</item><item>
    ''' Reading #9=100.171759 Ω 0x0000 9436ms</item><item>
    ''' Reading #10=100.170275 Ω 0x0000 10487ms  </item><item>
    ''' OperationEventCondition=0x0400 </item></list>
    ''' </remarks>
    ''' <param name="k2002">        The Keithley 2002 device. </param>
    ''' <param name="triggerCount"> Number of triggers. </param>
    Private Shared Sub AssertBusTriggersFetchReading(ByVal k2002 As VI.K2002.K2002Device, ByVal triggerCount As Integer)

        Dim actualOperationIdle As Boolean = True
        Dim title As String = k2002.OpenResourceTitle
        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords
        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        ' enable all measurement status bits.
        k2002.Session.ApplyServiceRequestEnableBitmask(k2002.Session.DefaultServiceRequestEnableBitmask)
        k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask(&H7FFF)
        k2002.StatusSubsystem.ApplyOperationEventEnableBitmask(&H7FFF)

        ScannerMeterTests.InitiateTriggerPlan(k2002)

        Try
            Dim initalTimestamp As DateTimeOffset = DateTimeOffset.Now
            Dim sw As Stopwatch = Stopwatch.StartNew
            Try
                For i As Integer = 1 To triggerCount
                    ' this delay is used to allow the tester to see the readings come in on the instruments.
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    k2002.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    ScannerMeterTests.FetchBufferReading(k2002, i, initalTimestamp, sw.Elapsed)
                Next

                propertyName = NameOf(StatusSubsystemBase.QueryOperationEventCondition).SplitWords
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks(OperationEventBitmaskKey.Idle):X4} bit should high (idle) after triggering is completed")

                ScannerMeterTests.AssertBufferReadings(k2002)

            Catch
                Throw
            Finally
            End Try
        Catch
            Throw
        Finally
            k2002.TriggerSubsystem.Abort()
            ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
            actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
            Assert.IsTrue(actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted")
        End Try

    End Sub

    ''' <summary> (Unit Test Method) tests bus triggers fetch reading. </summary>
    ''' <remarks> 2020-01-10. </remarks>
    Public Shared Sub AssertBusTriggersFetchReading()
        If Not K2002ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K2002ResourceInfo.ResourceTitle} not found")
        If Not K7000ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K7000ResourceInfo.ResourceTitle} not found")
        Using k7000Device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            k7000Device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using K2002Device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                K2002Device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, K2002Device, K2002ResourceInfo)
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, k7000Device, K7000ResourceInfo)
                    Assert.IsTrue(K2002Device.SystemSubsystem.QueryFrontTerminalsSelected.Value, $"Front terminal must be selected")

                    ScannerMeterTests.ConfigureTriggerPlan(K2002Device, K7000SubsystemInfo.BusTriggerTestTriggerCount, 1, ScannerMeterSettings.Get.MeterTriggerSource)
                    ScannerMeterTests.ConfigureMeasurement(K2002Device)
                    ScannerMeterTests.ConfigureLimitBinning(K2002Device, DigitalActiveLevels.Low)
                    ScannerMeterTests.ConfigureTrace(K2002Device)
                    If VI.TriggerSources.External = ScannerMeterSettings.Get.MeterTriggerSource Then
                        ScannerMeterTests.ConfigureBusTriggerPlan(k7000Device)
                        ScannerMeterTests.AssertBusTriggersFetchReading(k7000Device, K2002Device)
                    ElseIf VI.TriggerSources.Bus = ScannerMeterSettings.Get.MeterTriggerSource Then
                        ScannerMeterTests.ConfigureBusTriggerPlan(k7000Device)
                        ScannerMeterTests.AssertBusTriggersFetchReading(K2002Device, K7000SubsystemInfo.BusTriggerTestTriggerCount)
                    End If
                Catch
                    Throw
                Finally
                    K2002Tests.DeviceManager.CloseSession(TestInfo, k7000Device)
                    K2002Tests.DeviceManager.CloseSession(TestInfo, K2002Device)
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests bus triggers fetch reading meter bus. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub MeterBusTriggersFetchReadingTest()
        ScannerMeterSettings.Get.MeterTriggerSource = TriggerSources.Bus
        ScannerMeterTests.AssertBusTriggersFetchReading()
    End Sub

    ''' <summary> (Unit Test Method) tests bus triggers fetch reading scanner bus. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub ScannerBusTriggersFetchReadingTest()
        ScannerMeterSettings.Get.MeterTriggerSource = TriggerSources.External
        ScannerMeterTests.AssertBusTriggersFetchReading()
    End Sub


#End Region

#Region " BUS TRIGGERS STREAM "

    ''' <summary> Handles the property change. </summary>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Shared Sub HandlePropertyChange(ByVal sender As VI.TraceSubsystemBase, ByVal propertyName As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.TraceSubsystemBase.BufferReadingsCount)
                    If sender.BufferReadingsCount > 0 Then
                        TestInfo.TraceMessage($"Streaming reading #{sender.BufferReadingsCount}: {sender.LastBufferReading.Amount} 0x{sender.LastBufferReading.StatusWord:X4}")
                    End If
            End Select
        End If
    End Sub

    ''' <summary> Handles the trace subsystem property change. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub HandleTraceSubsystemPropertyChange(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso e IsNot Nothing Then
                ScannerMeterTests.HandlePropertyChange(TryCast(sender, VI.TraceSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Assert.Fail(ex.ToString)
        End Try
    End Sub

    ''' <summary> Restore Trigger State. </summary>
    ''' <param name="device"> The device. </param>
    Private Shared Sub RestoreState(ByVal device As K2002.K2002Device)
        device.TriggerSubsystem.Abort()
        device.Session.QueryOperationCompleted()
        device.ResetKnownState()
        device.Session.QueryOperationCompleted()
        device.ClearExecutionState()
        device.Session.QueryOperationCompleted()
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.Session.EnableServiceRequestWaitComplete()
    End Sub

    ''' <summary> Stops buffer streaming. </summary>
    ''' <param name="device"> The device. </param>
    Private Shared Sub StopBufferStreaming(ByVal device As K2002.K2002Device)
        Dim timeout As TimeSpan = VI.TraceSubsystemBase.EstimateStreamStopTimeoutInterval(device.TraceSubsystem.StreamCycleDuration, ScannerMeterSettings.Get.BufferStreamPollInteval, 1.5)
        Dim r As (Success As Boolean, Details As String) = device.TraceSubsystem.StopBufferStream(timeout)
        If r.Success Then
            device.TriggerSubsystem.Abort()
            device.Session.ReadStatusRegister()
            device.TriggerSubsystem.QueryTriggerState()
            device.Session.ReadStatusRegister()
            TestInfo.TraceMessage($"Streaming ended with { r.Details}")
        Else
            Assert.Fail($"buffer streaming failed; { r.Details}")
        End If
        RemoveHandler device.TraceSubsystem.PropertyChanged, AddressOf ScannerMeterTests.HandleTraceSubsystemPropertyChange
    End Sub

    ''' <summary> Starts buffer streaming. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub StartBufferStreaming(ByVal device As VI.K2002.K2002Device)
        AddHandler device.TraceSubsystem.PropertyChanged, AddressOf ScannerMeterTests.HandleTraceSubsystemPropertyChange
        Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed before starting streaming")
        device.TriggerSubsystem.Initiate()
        isr.Core.ApplianceBase.DoEvents()
        AddHandler device.TraceSubsystem.BufferStreamTasker.AsyncCompleted, Sub(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
                                                                                Try
                                                                                    If e.Error IsNot Nothing Then
                                                                                        Assert.IsNull(e.Error, $"Exception streaming: {e.Error.ToFullBlownString}")
                                                                                    End If
                                                                                Finally
                                                                                    ScannerMeterTests.StopBufferStreaming(device)
                                                                                End Try
                                                                            End Sub
        device.TraceSubsystem.StartBufferStream(device.TriggerSubsystem, ScannerMeterSettings.Get.BufferStreamPollInteval, device.SenseSubsystem.FunctionUnit)
    End Sub

#End Region

#Region " BUS TRIGGERS STREAM READING "

    ''' <summary> Assert bus trigger scan. </summary>
    ''' <remarks>
    ''' <list type="bullet">2020-01-10<item>
    ''' OperationEventCondition=0x0400    </item><item>
    ''' Streaming reading #1: 100.29333 Ω 0x0000    </item><item>
    ''' Streaming reading #2: 100.29309 Ω 0x0000    </item><item>
    ''' Streaming reading #3: 100.29311 Ω 0x0000    </item><item>
    ''' Streaming reading #4: 100.29301 Ω 0x0000    </item><item>
    ''' Streaming reading #5: 100.29288 Ω 0x0000    </item><item>
    ''' Streaming reading #6: 100.29291 Ω 0x0000    </item><item>
    ''' Streaming reading #7: 100.29242 Ω 0x0000    </item><item>
    ''' Streaming reading #8: 100.29265 Ω 0x0000    </item><item>
    ''' Streaming reading #9: 100.29249 Ω 0x0000    </item><item>
    ''' Streaming reading #10: 100.2923 Ω 0x0000    </item><item>
    ''' Reading #1=100.29333 Ω 0x0000 622ms     </item><item>
    ''' Reading #2=100.29309 Ω 0x0000 1614ms    </item><item>
    ''' Reading #3=100.29311 Ω 0x0000 2621ms    </item><item>
    ''' Reading #4=100.29301 Ω 0x0000 3621ms    </item><item>
    ''' Reading #5=100.29288 Ω 0x0000 4639ms    </item><item>
    ''' Reading #6=100.29291 Ω 0x0000 5668ms    </item><item>
    ''' Reading #7=100.29242 Ω 0x0000 6696ms    </item><item>
    ''' Reading #8=100.29265 Ω 0x0000 7711ms    </item><item>
    ''' Reading #9=100.29249 Ω 0x0000 8736ms    </item><item>
    ''' Reading #10=100.2923 Ω 0x0000 9740ms    </item><item>
    ''' OperationEventCondition=0x0400          </item></list>
    ''' </remarks>
    ''' <param name="k7000"> The Keithley 7001 device. </param>
    ''' <param name="k2002"> The Keithley 2002 device. </param>
    Friend Shared Sub AssertBusTriggersStreamReadings(ByVal k7000 As VI.K7000.K7000Device, ByVal k2002 As VI.K2002.K2002Device)

        Dim actualOperationIdle As Boolean = True
        Dim K2002Title As String = k2002.OpenResourceTitle

        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)

        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords
        operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
        actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
        Assert.IsTrue(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true before triggering is initiated")
        ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

        ScannerMeterTests.StartBufferStreaming(k2002)

        'operationBitmasks.Status = K2002.StatusSubsystem.QueryOperationEventCondition.Value
        'actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmask.Idle)
        'Assert.IsFalse(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be false after triggering is initiated")
        'ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{K2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

        Try
            ' start the K7000 trigger model
            k7000.TriggerSubsystem.Initiate()
            Try

                For i As Integer = 1 To K7000SubsystemInfo.BusTriggerTestTriggerCount
                    ' this delay is used to allow the operator to see the values as they come in.
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    k7000.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                Next

                ' NOTE: this causes a query interrupted error on occasion and needs to be investigated further.
                ScannerMeterTests.StopBufferStreaming(k2002)

                propertyName = NameOf(StatusSubsystemBase.QueryOperationEventCondition).SplitWords
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Assert.IsTrue(actualOperationIdle, $"[{K2002Title}].[{propertyName}] 0x{operationBitmasks(OperationEventBitmaskKey.Idle):X4} bit should high (idle) after triggering is completed")

            Catch
                Throw
            Finally
                k7000.TriggerSubsystem.Abort()
                k7000.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
                k7000.RouteSubsystem.QueryClosedChannels()
            End Try
        Catch
            Throw
        Finally
            ScannerMeterTests.StopBufferStreaming(k2002)
            ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
            actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
            Assert.IsTrue(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true before triggering is aborted")
        End Try

    End Sub

    ''' <summary> Assert bus trigger scan. </summary>
    ''' <remarks>
    ''' <list type="bullet">2020-04-09<item>
    ''' OperationEventCondition=0x0400 </item><item>
    ''' Streaming reading #1: 100.147557 Ω 0x0000  </item><item>
    ''' Streaming reading #2: 100.14756 Ω 0x0000  </item><item>
    ''' Streaming reading #3: 100.147442 Ω 0x0000  </item><item>
    ''' Streaming reading #4: 100.147776 Ω 0x0000  </item><item>
    ''' Streaming reading #5: 100.147449 Ω 0x0000  </item><item>
    ''' Streaming reading #6: 100.147428 Ω 0x0000  </item><item>
    ''' Streaming reading #7: 100.146959 Ω 0x0000  </item><item>
    ''' Streaming reading #8: 100.147494 Ω 0x0000  </item><item>
    ''' Streaming reading #9: 100.147884 Ω 0x0000  </item><item>
    ''' Streaming reading #10: 100.14747 Ω 0x0000  </item><item>
    ''' Reading #1=100.147557 Ω 0x0000 617ms  </item><item>
    ''' Reading #2=100.14756 Ω 0x0000 1554ms  </item><item>
    ''' Reading #3=100.147442 Ω 0x0000 2612ms  </item><item>
    ''' Reading #4=100.147776 Ω 0x0000 3561ms  </item><item>
    ''' Reading #5=100.147449 Ω 0x0000 4640ms  </item><item>
    ''' Reading #6=100.147428 Ω 0x0000 5595ms  </item><item>
    ''' Reading #7=100.146959 Ω 0x0000 6610ms  </item><item>
    ''' Reading #8=100.147494 Ω 0x0000 7623ms  </item><item>
    ''' Reading #9=100.147884 Ω 0x0000 8704ms  </item><item>
    ''' Reading #10=100.14747 Ω 0x0000 9653ms  </item><item>
    ''' OperationEventCondition=0x0400OperationEventCondition=0x0400 </item></list>
    ''' </remarks>
    ''' <param name="k2002">        The Keithley 2002 device. </param>
    ''' <param name="triggerCount"> Number of triggers. </param>
    ''' <param name="delay">        The delay. </param>
    Friend Shared Sub AssertBusTriggersStreamReadings(ByVal k2002 As VI.K2002.K2002Device, ByVal triggerCount As Integer, ByVal delay As TimeSpan)

        Dim actualOperationIdle As Boolean = True
        Dim K2002Title As String = k2002.OpenResourceTitle

        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)

        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)

        Dim operationConditionEventPropertyName As String = NameOf(StatusSubsystemBase.OperationEventCondition)
        Dim propertyName As String = NameOf(OperationEventBitmaskKey.Idle).SplitWords
        operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
        actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
        Assert.IsTrue(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true before triggering is initiated")
        ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

        ScannerMeterTests.StartBufferStreaming(k2002)

        'operationBitmasks.Status = K2002.StatusSubsystem.QueryOperationEventCondition.Value
        'actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmask.Idle)
        'Assert.IsFalse(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be false after triggering is initiated")
        'ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{K2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

        Try
            Try

                For i As Integer = 1 To triggerCount
                    ' this delay is used to allow the operator to see the values as they come in.
                    isr.Core.ApplianceBase.DoEventsWait(delay)
                    k2002.TraceSubsystem.BusTriggerRequested = True
                    ' k2002.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(delay)
                    ScannerMeterTests.RequestBinningStrobe(k2002, True)
                Next

                ' NOTE: this causes a query interrupted error on occasion and needs to be investigated further.
                ScannerMeterTests.StopBufferStreaming(k2002)

                propertyName = NameOf(StatusSubsystemBase.QueryOperationEventCondition).SplitWords
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Assert.IsTrue(actualOperationIdle, $"[{K2002Title}].[{propertyName}] 0x{operationBitmasks(OperationEventBitmaskKey.Idle):X4} bit should high (idle) after triggering is completed")

            Catch
                Throw
            Finally
            End Try
        Catch
            Throw
        Finally
            ScannerMeterTests.StopBufferStreaming(k2002)
            ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
            actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
            Assert.IsTrue(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true after triggering is aborted")
        End Try

    End Sub

    ''' <summary> Assert bus trigger scan. </summary>
    Public Shared Sub AssertBusTriggersStreamReadings()
        If Not K2002ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K2002ResourceInfo.ResourceTitle} not found")
        If Not K7000ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K7000ResourceInfo.ResourceTitle} not found")
        Using k7000Device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            k7000Device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using K2002Device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                K2002Device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, K2002Device, K2002ResourceInfo)
                    Assert.IsTrue(K2002Device.SystemSubsystem.QueryFrontTerminalsSelected.Value, $"Front terminal must be selected")
                    ScannerMeterTests.ConfigureTriggerPlan(K2002Device, Integer.MaxValue, 1, ScannerMeterSettings.Get.MeterTriggerSource)
                    ScannerMeterTests.ConfigureMeasurement(K2002Device)
                    ScannerMeterTests.ConfigureLimitBinning(K2002Device, DigitalActiveLevels.Low)
                    ScannerMeterTests.ConfigureTrace(K2002Device)
                    If TriggerSources.Bus = ScannerMeterSettings.Get.MeterTriggerSource Then
                        ScannerMeterTests.AssertBusTriggersStreamReadings(K2002Device, K7000SubsystemInfo.BusTriggerTestTriggerCount, K7000SubsystemInfo.BusTriggerTestTriggerDelay)
                    ElseIf TriggerSources.External = ScannerMeterSettings.Get.MeterTriggerSource Then
                        isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, k7000Device, K7000ResourceInfo)
                        ScannerMeterTests.ConfigureBusTriggerPlan(k7000Device)
                        ScannerMeterTests.AssertBusTriggersStreamReadings(k7000Device, K2002Device)
                    End If
                    ScannerMeterTests.AssertBufferReadings(K2002Device)
                Catch
                    Throw
                Finally
                    ScannerMeterTests.RestoreState(K2002Device)
                    K2002Tests.DeviceManager.CloseSession(TestInfo, K2002Device)
                    If TriggerSources.External = ScannerMeterSettings.Get.MeterTriggerSource Then
                        K2002Tests.DeviceManager.CloseSession(TestInfo, k7000Device)
                    End If
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests stream readings using meter bus. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub MeterBusTriggersStreamReadingsTest()
        ScannerMeterSettings.Get.MeterTriggerSource = TriggerSources.Bus
        ScannerMeterTests.AssertBusTriggersStreamReadings()
    End Sub

    ''' <summary> (Unit Test Method) tests stream readings using scanner bus. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub ScannerBusTriggersStreamReadingsTest()
        ScannerMeterSettings.Get.MeterTriggerSource = TriggerSources.External
        ScannerMeterTests.AssertBusTriggersStreamReadings()
    End Sub

#End Region

#Region " TBA: AUTO TRIGGER AUTONOMOUS MEASUREMENTS "

    ''' <summary> Configure externalrigger scan. </summary>
    ''' <param name="device"> The device. </param>
    Private Shared Sub ConfigureExternalriggerScan(ByVal device As VI.K7000.K7000Device)

        Dim propertyName As String = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords

        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.TriggerSubsystem.Abort()
        device.Session.EnableServiceRequestWaitComplete()
        device.RouteSubsystem.WriteOpenAll(TimeSpan.FromSeconds(1))
        device.Session.ReadStatusRegister()

        device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
        device.RouteSubsystem.QueryClosedChannels()

        propertyName = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords
        Dim expectedScanList As String = K7000SubsystemInfo.AutonomousTriggerTestScanList
        Dim actualScanList As String = device.RouteSubsystem.ApplyScanList(expectedScanList)
        Assert.AreEqual(expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'")

        device.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer1Subsystem.ApplyArmCount(1)
        device.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer2Subsystem.ApplyArmCount(1)
        device.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)

        ' device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Autonomous)
        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords
        Dim expectedTriggerSource As VI.TriggerSources = TriggerSources.External
        Dim actualTriggerSource As VI.TriggerSources? = device.TriggerSubsystem.ApplyTriggerSource(expectedTriggerSource)
        Assert.IsTrue(actualTriggerSource.HasValue, $"{propertyName} should have a value")
        Assert.AreEqual(Of VI.TriggerSources)(expectedTriggerSource, actualTriggerSource.Value, $"{propertyName} should match")

        device.TriggerSubsystem.ApplyTriggerCount(K7000SubsystemInfo.AutonomousTriggerTestTriggerCount) ' in place of infinite
        device.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
        device.TriggerSubsystem.ApplyTriggerLayerBypassMode(VI.TriggerLayerBypassModes.Source)

    End Sub

    ''' <summary> Assert autonomous trigger scan. </summary>
    ''' <param name="k7000">   The Keithley 7001 device. </param>
    ''' <param name="k2002">   The Keithley 2002 device. </param>
    ''' <param name="timeout"> The timeout. </param>
    Private Shared Sub AssertAutonomousTriggerScan(ByVal k7000 As VI.K7000.K7000Device, ByVal k2002 As VI.K2002.K2002Device, ByVal timeout As TimeSpan)

        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(operationBitmasks)

        ' start the K2002 trigger model
        k2002.TriggerSubsystem.Initiate()

        Try
            ' start the K7000 trigger model
            k7000.TriggerSubsystem.Initiate()
            Try
                Dim sw As Stopwatch = Stopwatch.StartNew
                Dim K2002Idle As Boolean = False
                Dim timedout As Boolean = sw.Elapsed > timeout
                Do Until K2002Idle OrElse timedout
                    timedout = sw.Elapsed > timeout
                    isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromSeconds(10))
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition.Value
                    K2002Idle = operationBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                Loop
                Assert.IsFalse(timedout, $"{NameOf(AssertAutonomousTriggerScan).SplitWords} should complete before timeout time {timeout:s.\f}")
            Catch
                Throw
            Finally
                k7000.TriggerSubsystem.Abort()
                k7000.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
                k7000.RouteSubsystem.QueryClosedChannels()
            End Try
        Catch
            Throw
        Finally
            k2002.TriggerSubsystem.Abort()
        End Try

    End Sub

    ''' <summary> Tests autonomous scan. </summary>
    Public Shared Sub AutonomousScanTest()
        If Not K2002ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K2002ResourceInfo.ResourceTitle} not found")
        If Not K7000ResourceInfo.ResourcePinged Then Assert.Inconclusive($"{K7000ResourceInfo.ResourceTitle} not found")
        Using k7000Device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            k7000Device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using K2002Device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                K2002Device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, K2002Device, K2002ResourceInfo)
                    isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, k7000Device, K7000ResourceInfo)
                    Assert.IsTrue(K2002Device.SystemSubsystem.QueryFrontTerminalsSelected.Value, $"Front terminal must be selected")
                    ScannerMeterTests.ConfigureTriggerPlan(K2002Device, K7000SubsystemInfo.AutonomousTriggerTestTriggerCount, 1, ScannerMeterSettings.Get.MeterTriggerSource)
                    If VI.TriggerSources.Bus = ScannerMeterSettings.Get.MeterTriggerSource Then
                    ElseIf VI.TriggerSources.External = ScannerMeterSettings.Get.MeterTriggerSource Then
                        ScannerMeterTests.ConfigureExternalriggerScan(k7000Device)
                        ScannerMeterTests.AssertAutonomousTriggerScan(k7000Device, K2002Device, TimeSpan.FromSeconds(10))
                    End If
                Catch
                    Throw
                Finally
                    K2002Tests.DeviceManager.CloseSession(TestInfo, k7000Device)
                    K2002Tests.DeviceManager.CloseSession(TestInfo, K2002Device)
                End Try
            End Using
        End Using
    End Sub

#End Region

End Class

