'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Subsystems\LocalNodeSubsystemBase.vb
'
' summary:	Local node subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Defines a local node subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-11-01. Based on legacy status subsystem. </para>
''' </remarks>
Public MustInherit Class LocalNodeSubsystemBase
    Inherits VI.SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SystemSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._InitializeTimeout = TimeSpan.FromMilliseconds(30000)
        Me.ShowEventsStack = New System.Collections.Generic.Stack(Of VI.Tsp.Syntax.EventLogModes?)
        Me.ShowPromptsStack = New System.Collections.Generic.Stack(Of PromptsState)
        If statusSubsystem IsNot Nothing Then AddHandler statusSubsystem.Session.PropertyChanged, AddressOf Me.SessionPropertyChanged
    End Sub

    #End Region

    #Region " I PRESETTABLE "

    ''' <summary> The initialize timeout. </summary>
    Private _InitializeTimeout As TimeSpan

    ''' <summary> Gets or sets the time out for doing a reset and clear on the instrument. </summary>
    ''' <value> The connect timeout. </value>
    Public Property InitializeTimeout() As TimeSpan
        Get
            Return Me._InitializeTimeout
        End Get
        Set(ByVal value As TimeSpan)
            If Not value.Equals(Me.InitializeTimeout) Then
                Me._InitializeTimeout = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Clears the active state. Issues selective device clear. </summary>
    Public Sub ClearActiveState()
        Me.ExecutionState = TspExecutionState.IdleReady
    End Sub

    ''' <summary> Sets values to their known clear execution state. </summary>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.ReadExecutionState()
        ' Set all cached values that get reset by CLS
        Me.ClearStatus()
        Me.Session.QueryOperationCompleted()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Try
            Me.Session.StoreCommunicationTimeout(Me.InitializeTimeout)
            ' turn prompts off. This may not be necessary.
            Me.WriteAutoInstrumentMessages(PromptsState.Disable, VI.Tsp.Syntax.EventLogModes.None)
        Catch ex As Exception
            Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                              "Exception ignored turning off prompts;. {0}", ex.ToFullBlownString)
        Finally
            Me.Session.RestoreCommunicationTimeout()
        End Try

        Try
            ' flush the input buffer in case the instrument has some leftovers.
            Me.Session.DiscardUnreadData()
            If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                   "Data discarded after turning prompts and errors off;. Data: {0}.", Me.Session.DiscardedData)
            End If
        Catch ex As VI.Pith.NativeException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString)
        End Try

        Try
            ' flush write may cause the instrument to send off a new data.
            Me.Session.DiscardUnreadData()
            If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                   "Unread data discarded after discarding unset data;. Data: {0}.", Me.Session.DiscardedData)
            End If
        Catch ex As VI.Pith.NativeException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString)
        End Try

    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()

        ' clear elements.
        Me.ClearStatus()

        MyBase.DefineKnownResetState()

        ' enable processing of execution state.
        Me.ProcessExecutionStateEnabled = True

        ' read the prompts status
        Me.QueryPromptsState()

        ' read the errors status
        Me.QueryShowEvents()

        Me.Session.QueryOperationCompleted()

        Me.ExecutionState = New TspExecutionState?

    End Sub

    #End Region

    #Region " SESSION "

    ''' <summary> Handles the Session property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    Private Sub SessionPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Me.OnSessionPropertyChanged(e)
        End If
    End Sub

    ''' <summary> Handles the property changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnSessionPropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.ProcessExecutionStateEnabled AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Select Case e.PropertyName
                Case NameOf(VI.Pith.SessionBase.LastMessageReceived)
                    ' parse the command to get the TSP execution state.
                    Me.ParseExecutionState(Me.Session.LastMessageReceived, TspExecutionState.IdleReady)
                Case NameOf(VI.Pith.SessionBase.LastMessageSent)
                    ' set the TSP status
                    Me.ExecutionState = TspExecutionState.Processing
            End Select
        End If
    End Sub

    #End Region

    #Region " ASSET TRIGGER "

    ''' <summary> Issues a hardware trigger. </summary>
    Public Sub AssertTrigger()
        Me.ExecutionState = TspExecutionState.IdleReady
        Me.Session.AssertTrigger()
    End Sub

    #End Region

    #Region " EXECUTION STATE "

    ''' <summary> True to enable, false to disable the process execution state. </summary>
    Private _ProcessExecutionStateEnabled As Boolean

    ''' <summary> Gets or sets the process execution state enabled. </summary>
    ''' <value> The process execution state enabled. </value>
    Public Property ProcessExecutionStateEnabled As Boolean
        Get
            Return Me._ProcessExecutionStateEnabled
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me._ProcessExecutionStateEnabled) Then
                Me._ProcessExecutionStateEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> State of the execution. </summary>
    Private _ExecutionState As TspExecutionState?

    ''' <summary>
    ''' Gets or sets the last TSP execution state. Setting the last state is useful when closing the
    ''' Tsp System.
    ''' </summary>
    ''' <value> The last state. </value>
    Public Property ExecutionState() As TspExecutionState?
        Get
            Return Me._ExecutionState
        End Get
        Set(ByVal value As TspExecutionState?)
            If (value.HasValue AndAlso Not Me.ExecutionState.HasValue) OrElse
                (Not value.HasValue AndAlso Me.ExecutionState.HasValue) OrElse Not value.Equals(Me.ExecutionState) Then
                Me._ExecutionState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the instrument Execution State caption. </summary>
    ''' <value> The state caption. </value>
    Public ReadOnly Property ExecutionStateCaption() As String
        Get
            Return If(Me.ExecutionState.HasValue, isr.Core.EnumExtensions.Methods.Description(Me.ExecutionState.Value), "N/A")
        End Get
    End Property

    ''' <summary> Parses the state of the TSP prompt and saves it in the state cache value. </summary>
    ''' <param name="value">        Specifies the read buffer. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> The instrument Execution State. </returns>
    Public Function ParseExecutionState(ByVal value As String, ByVal defaultValue As TspExecutionState) As TspExecutionState
        Dim state As TspExecutionState = defaultValue
        If String.IsNullOrWhiteSpace(value) OrElse value.Length < 4 Then
        Else
            value = value.Substring(0, 4)
            If value.StartsWith(Tsp.Syntax.Constants.ReadyPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                state = TspExecutionState.IdleReady
            ElseIf value.StartsWith(Tsp.Syntax.Constants.ContinuationPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                state = TspExecutionState.IdleContinuation
            ElseIf value.StartsWith(Tsp.Syntax.Constants.ErrorPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                state = TspExecutionState.IdleError
            Else
                ' no prompt -- set to the default state
                state = defaultValue
            End If
        End If
        Me.ExecutionState = state
        Return state
    End Function

    ''' <summary> Reads the state of the TSP prompt and saves it in the state cache value. </summary>
    ''' <returns> The instrument Execution State. </returns>
    Public Function ReadExecutionState() As TspExecutionState?

        ' check status of the prompt flag.
        If Me.PromptsState <> PromptsState.None Then

            ' if prompts are on, 
            If Me.PromptsState = PromptsState.Enable Then

                ' do a read. This raises an event that parses the state
                If Me.Session.QueryMessageAvailableStatus(TimeSpan.FromMilliseconds(1), 3) Then
                    Me.Session.ReadLine()
                End If

            Else

                Me.ExecutionState = TspExecutionState.Unknown

            End If

        Else

            ' check if we have data in the output buffer.  
            If Me.Session.QueryMessageAvailableStatus(TimeSpan.FromMilliseconds(1), 3) Then

                ' if data exists in the buffer, it may indicate that the prompts are already on 
                ' so just go read the output buffer. Once read, the status will be parsed.
                Me.Session.ReadLine()

            Else

                ' if we have no value then we must first read the prompt status
                ' once read, the status will be parsed.
                Me.QueryPromptsState()

            End If

        End If

        Return Me.ExecutionState

    End Function

    #End Region

    #Region " SHOWS EVENTS MODE "

    ''' <summary> The show events. </summary>
    Private _ShowEvents As VI.Tsp.Syntax.EventLogModes?

    ''' <summary> Gets or sets the cached Shows Events Mode. </summary>
    ''' <value> The show events. </value>
    Public Overloads Property ShowEvents As VI.Tsp.Syntax.EventLogModes?
        Get
            Return Me._ShowEvents
        End Get
        Protected Set(ByVal value As VI.Tsp.Syntax.EventLogModes?)
            If Not Nullable.Equals(Me.ShowEvents, value) Then
                Me._ShowEvents = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Shows Events Mode . </summary>
    ''' <param name="value"> the Shows Events Mode . </param>
    ''' <returns> A List of scans. </returns>
    Public Function ApplyShowEvents(ByVal value As VI.Tsp.Syntax.EventLogModes?) As VI.Tsp.Syntax.EventLogModes?
        Me.WriteShowEvents(value)
        Return Me.QueryShowEvents()
    End Function

    ''' <summary> Gets or sets the Shows Events Mode query command. </summary>
    ''' <value> the Shows Events Mode query command. </value>
    Protected Overridable Property ShowEventsQueryCommand As String = "_G.print(_G.localnode.showevents)"

    ''' <summary> Queries the Shows Events Modes. </summary>
    ''' <returns>
    ''' The <see cref="VI.Tsp.Syntax.EventLogModes">Shows Events Modes</see> or none if
    ''' unknown.
    ''' </returns>
    Public Function QueryShowEvents() As VI.Tsp.Syntax.EventLogModes?
        Dim mode As String = Me.ShowEvents.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd(Me.ShowEventsQueryCommand)
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = "Failed fetching Shows Events Modes"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.ShowEvents = New VI.Tsp.Syntax.EventLogModes?
        Else
            Dim eventMode As Integer = 0
            Me.ShowEvents = If(Integer.TryParse(mode, eventMode),
                CType(eventMode, VI.Tsp.Syntax.EventLogModes), New VI.Tsp.Syntax.EventLogModes?)
        End If
        Return Me.ShowEvents
    End Function

    ''' <summary> Gets or sets the Shows Events Mode command format. </summary>
    ''' <remarks> SCPI Base Command: ":FORM:ELEM {0}". </remarks>
    ''' <value> the Shows Events Mode command format. </value>
    Protected Overridable Property ShowEventsCommandFormat As String = "_G.localnode.showevents={0}"

    ''' <summary>
    ''' Writes the Shows Events Modes without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> the Shows Events Mode. </param>
    ''' <returns>
    ''' The <see cref="VI.Tsp.Syntax.EventLogModes">Shows Events Modes</see> or none if
    ''' unknown.
    ''' </returns>
    Public Function WriteShowEvents(ByVal value As VI.Tsp.Syntax.EventLogModes?) As VI.Tsp.Syntax.EventLogModes?
        Me.Session.WriteLine(Me.ShowEventsCommandFormat, CInt(value))
        Me.ShowEvents = value
        Return Me.ShowEvents
    End Function

    #End Region

    #Region " PROMPTS STATE "

    ''' <summary> State of the prompts. </summary>
    Private _PromptsState As PromptsState

    ''' <summary> Gets or sets the prompts state sentinel. </summary>
    ''' <remarks>
    ''' When true, prompts are issued after each command message is processed by the instrument.<para>
    ''' When false prompts are not issued.</para><para>
    ''' Command messages do not generate prompts. Rather, the TSP instrument generates prompts in
    ''' response to command messages. When prompting is enabled, the instrument generates prompts in
    ''' response to command messages. There are three prompts that might be returned:</para><para>
    ''' “TSP&gt;” is the standard prompt. This prompt indicates that everything is normal and the
    ''' command is done processing.</para><para>
    ''' “TSP?” is issued if there are entries in the error queue when the prompt is issued. Like the
    ''' “TSP&gt;” prompt, it indicates the command is done processing. It does not mean the previous
    ''' command generated an error, only that there are still errors in the queue when the command
    ''' was done processing.</para><para>
    ''' “&gt;&gt;&gt;&gt;” is the continuation prompt. This prompt is used when downloading scripts
    ''' or flash images. When downloading scripts or flash images, many command messages must be sent
    ''' as a unit. The continuation prompt indicates that the instrument is expecting more messages
    ''' as part of the current command.</para>
    ''' </remarks>
    ''' <value> The prompts state. </value>
    Public Property PromptsState() As PromptsState
        Get
            Return Me._PromptsState
        End Get
        Protected Set(ByVal value As PromptsState)
            If Not Nullable.Equals(value, Me.PromptsState) Then
                Me._PromptsState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the prompts state. </summary>
    ''' <param name="value"> true to value. </param>
    ''' <returns> A PromptsState. </returns>
    Public Function ApplyPromptsState(ByVal value As PromptsState) As PromptsState
        Me.WritePromptsState(value)
        Return Me.QueryPromptsState()
    End Function

    ''' <summary> Gets or sets the prompts state query command. </summary>
    ''' <value> The prompts state query command. </value>
    Protected Overridable Property PromptsStateQueryCommand As String = "_G.print(_G.localnode.prompts)"

    ''' <summary> Queries the condition for showing prompts. Controls prompting. </summary>
    ''' <returns> The prompts state. </returns>
    Public Function QueryPromptsState() As PromptsState
        Dim mode As String = Me.PromptsState.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd(Me.PromptsStateQueryCommand)
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = "Failed fetching prompts state"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.PromptsState = PromptsState.None
        Else
            Dim se As New StringEnumerator(Of PromptsState)
            Me.PromptsState = se.ParseContained(mode.BuildDelimitedValue)
        End If
        If Not Me.ProcessExecutionStateEnabled Then
            ' read execution state explicitly, because session events are disabled.
            Me.ReadExecutionState()
        End If
        Return Me.PromptsState
    End Function

    ''' <summary> The prompts state command format. </summary>
    ''' <value> The prompts state command format. </value>
    Protected Overridable Property PromptsStateCommandFormat As String = "_G.localnode.prompts={0}"

    ''' <summary> Sets the condition for showing prompts. Controls prompting. </summary>
    ''' <param name="value"> true to value. </param>
    ''' <returns> <c>True</c> to prompts state; otherwise <c>False</c>. </returns>
    Public Function WritePromptsState(ByVal value As PromptsState) As PromptsState
        Me.Session.LastAction = Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"{value} prompts;. ")
        Me.Session.LastNodeNumber = New Integer?
        If value <> PromptsState.None Then
            Me.Session.WriteLine(Me.PromptsStateCommandFormat, value.ExtractBetween)
        End If
        Me.PromptsState = value
        If Not Me.ProcessExecutionStateEnabled Then
            ' read execution state explicitly, because session events are disabled.
            Me.ReadExecutionState()
        End If
        Return Me.PromptsState
    End Function

    ''' <summary>
    ''' Sets the instrument to automatically send or stop sending instrument prompts and events.
    ''' </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="prompts"> The prompts. </param>
    ''' <param name="events">  The events. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub WriteAutoInstrumentMessages(ByVal prompts As PromptsState, ByVal events As VI.Tsp.Syntax.EventLogModes?)

        ' flush the input buffer in case the instrument has some leftovers.
        Me.Session.DiscardUnreadData()

        Dim showPromptsCommand As String = "<failed to issue>"
        Try
            ' sets prompt transmissions
            Me.WritePromptsState(prompts)
            showPromptsCommand = Me.Session.LastMessageSent
        Catch
        End Try

        ' flush again in case turning off prompts added stuff to the buffer.
        Me.Session.DiscardUnreadData()
        If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
            Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                   "Unread data discarded after turning prompts off;. Data: {0}.", Me.Session.DiscardedData)
        End If

        Dim showErrorsCommand As String = "<failed to issue>"
        Try
            ' turn off event log transmissions
            Me.WriteShowEvents(events)
            showErrorsCommand = Me.Session.LastMessageSent
        Catch
        End Try

        ' flush again in case turning off errors added stuff to the buffer.
        Me.Session.DiscardUnreadData()
        If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
            Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                   "Unread data discarded after turning errors off;. Data: {0}.", Me.Session.DiscardedData)
        End If

        ' now validate
        Me.QueryShowEvents()
        If Not Me.ShowEvents.HasValue Then
            Throw New isr.Core.OperationFailedException(Me.ResourceNameCaption, showErrorsCommand, "turning off automatic event display failed; value not set.")
        ElseIf Me.ShowEvents.Value <> events Then
            Throw New isr.Core.OperationFailedException(Me.ResourceNameCaption, showPromptsCommand, $"turning off test script prompts failed; showing {Me.ShowEvents} expected {events}.")
        End If

    End Sub

    #End Region

    #Region " STATUS "

    ''' <summary> Gets or sets the stack for storing the show events states. </summary>
    ''' <value> A stack of show events. </value>
    Private Property ShowEventsStack As System.Collections.Generic.Stack(Of VI.Tsp.Syntax.EventLogModes?)

    ''' <summary> Gets or sets the stack for storing the prompts state states. </summary>
    ''' <value> A stack of show prompts. </value>
    Private Property ShowPromptsStack As System.Collections.Generic.Stack(Of PromptsState)

    ''' <summary> Clears the status. </summary>
    Public Sub ClearStatus()

        ' clear the stacks
        Me.ShowEventsStack.Clear()
        Me.ShowPromptsStack.Clear()

        Me.ExecutionState = If(Me.Session.IsDeviceOpen, TspExecutionState.IdleReady, TspExecutionState.Closed)

    End Sub

    ''' <summary> Restores the status of errors and prompts. </summary>
    Public Sub RestoreStatus()

        If Me.ShowEventsStack.Count > 0 Then
            Dim lastShowEvent As VI.Tsp.Syntax.EventLogModes? = Me.ShowEventsStack.Pop
            If lastShowEvent.HasValue Then
                Me.WriteShowEvents(lastShowEvent.Value)
            End If
        End If
        If Me.ShowPromptsStack.Count > 0 Then
            Me.WritePromptsState(Me.ShowPromptsStack.Pop)
        End If

    End Sub

    ''' <summary> Saves the current status of errors and prompts. </summary>
    Public Sub StoreStatus()
        Me.ShowEventsStack.Push(Me.QueryShowEvents())
        Me.ShowPromptsStack.Push(Me.QueryPromptsState())
    End Sub

#End Region

End Class

''' <summary> Values that represent on off states. </summary>
Public Enum OnOffState

    ''' <summary> An enum constant representing the off] option. </summary>
    <ComponentModel.Description("Off (smu.OFF)")>
    [Off] = 0

    ''' <summary> An enum constant representing the on] option. </summary>
    <ComponentModel.Description("On (smu.ON)")>
    [On] = 1
End Enum

''' <summary> Values that represent prompts states. </summary>
Public Enum PromptsState

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not defined")>
    None

    ''' <summary> An enum constant representing the disable option. </summary>
    <ComponentModel.Description("Disable (localnode.DISABLE)")>
    Disable = 1

    ''' <summary> An enum constant representing the enable option. </summary>
    <ComponentModel.Description("Enable (localnode.ENABLE)")>
    Enable = 2
End Enum

''' <summary> Enumerates the TSP Execution State. </summary>
Public Enum TspExecutionState

    ''' <summary> Not defined. </summary>
    <ComponentModel.Description("Not defined")>
    None

    ''' <summary> Closed. </summary>
    <ComponentModel.Description("Closed")>
    Closed

    ''' <summary> Received the continuation prompt.
    ''' Send between lines when loading a script indicating that
    ''' TSP received script line successfully and is waiting for next line
    ''' or the end script command. </summary>
    <ComponentModel.Description("Continuation")>
    IdleContinuation

    ''' <summary> Received the error prompt. Error occurred; 
    '''           handle as desired. Use “errorqueue” commands to read and clear errors. </summary>
    <ComponentModel.Description("Error")>
    IdleError

    ''' <summary> Received the ready prompt. For example, TSP received script successfully and is ready for next command. </summary>
    <ComponentModel.Description("Ready")>
    IdleReady

    ''' <summary> A command was sent to the instrument. </summary>
    <ComponentModel.Description("Processing")>
    Processing

    ''' <summary> Cannot tell because prompt are off. </summary>
    <ComponentModel.Description("Unknown")>
    Unknown
End Enum

