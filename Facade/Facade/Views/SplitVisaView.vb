'---------------------------------------------------------------------------------------------------
' file:		.\Views\SplitVisaView.vb
'
' summary:	Split visa view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> A split view for VISA Sessions </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para></remarks>
<System.ComponentModel.DisplayName("Split Visa View"), System.ComponentModel.Description("Split View for VISA Sessions"),
      System.Drawing.ToolboxBitmap(GetType(SplitVisaView))>
Public Class SplitVisaView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me._Layout.Dock = DockStyle.Fill
        Me._TreePanel.Dock = DockStyle.Fill
        Me._TreePanel.SplitterDistance = 120
        Me._TreePanel.ClearNodes()
        Me._TreePanel.AddNode(SplitVisaView._MessagesNodeName, SplitVisaView._MessagesNodeName, Me._TraceMessagesBox)

        ' note that the caption is not set if this is run inside the On Load function.
        ' set defaults for the messages box.
        Me._TraceMessagesBox.ResetCount = 500
        Me._TraceMessagesBox.PresetCount = 250
        Me._TraceMessagesBox.ContainerTreeNode = Me._TreePanel.GetNode(SplitVisaView._MessagesNodeName)
        Me._TraceMessagesBox.TabCaption = SplitVisaView._MessagesNodeName
        Me._TraceMessagesBox.AlertsToggleTreeNode = Me._TreePanel.GetNode(SplitVisaView._MessagesNodeName)
        'Me._TraceMessagesBox.ContainerPanel = Me._TreePanel.Panel2
        'Me._TraceMessagesBox.AlertsToggleControl = Me._TreePanel.Panel2
        Me._TraceMessagesBox.CommenceUpdates()
        Me._TreePanel.Enabled = True
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="visaSessionBase"> The visa session. </param>
    Public Sub New(ByVal visaSessionBase As VisaSessionBase)
        Me.New()
        ' assigns the visa session.
        Me.BindVisaSessionBaseThis(visaSessionBase)
    End Sub

    ''' <summary> Creates a new <see cref="SplitVisaView"/> </summary>
    ''' <returns> A <see cref="SplitVisaView"/>. </returns>
    Public Shared Function Create() As SplitVisaView
        Dim view As SplitVisaView = Nothing
        Try
            view = New SplitVisaView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                Me._TraceMessagesBox.SuspendUpdatesReleaseIndicators()
                ' make sure the session is unbound in case the form is closed without closing the session.
                Me.BindVisaSessionBase(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT HANDLERS:  FORM "

    ''' <summary>
    ''' Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overrides Sub OnFormClosing(e As CancelEventArgs)
        MyBase.OnFormClosing(e)
        Try
            If e IsNot Nothing AndAlso Not e.Cancel Then
                Me.RemovePrivateListener(Me._TraceMessagesBox)
            End If
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            activity = $"Loading the driver console form"
            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            If Not Me.DesignMode Then
                ' add listeners before the first talker publish command
                Me.AddPrivateListeners()
            End If

            Me.PublishVerbose($"{activity};. ")

        Catch ex As Exception
            Me.PublishException(activity, ex)
            If isr.Core.MyDialogResult.Abort = isr.Core.WindowsForms.ShowDialogAbortIgnore(ex) Then
                Application.Exit()
            End If
        Finally
            MyBase.OnLoad(e)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()
            isr.Core.ApplianceBase.DoEvents()
        End Try

    End Sub

#End Region

#Region " VISA SESSION BASE (DEVICE BASE) "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VI.VisaSessionBase

    ''' <summary> Bind visa session base. </summary>
    ''' <param name="visaSessionBase"> True to show or False to hide the control. </param>
    Private Sub BindVisaSessionBaseThis(ByVal visaSessionBase As VI.VisaSessionBase)
        If Me._VisaSessionBase IsNot Nothing Then
            RemoveHandler Me.VisaSessionBase.Opening, AddressOf Me.DeviceOpening
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.DeviceOpened
            RemoveHandler Me.VisaSessionBase.Closing, AddressOf Me.DeviceClosing
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.DeviceClosed
            RemoveHandler Me.VisaSessionBase.Initialized, AddressOf Me.DeviceInitialized
            RemoveHandler Me.VisaSessionBase.Initializing, AddressOf Me.DeviceInitializing
            RemoveHandler Me.VisaSessionBase.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged

            Me.AssignTalker(Nothing)
            Me._VisaSessionBase = Nothing
        End If
        Me._VisaSessionBase = visaSessionBase
        If visaSessionBase IsNot Nothing Then
            Me.AssignTalker(Me.VisaSessionBase.Talker)
            AddHandler Me.VisaSessionBase.Opening, AddressOf Me.DeviceOpening
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.DeviceOpened
            AddHandler Me.VisaSessionBase.Closing, AddressOf Me.DeviceClosing
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.DeviceClosed
            AddHandler Me.VisaSessionBase.Initialized, AddressOf Me.DeviceInitialized
            AddHandler Me.VisaSessionBase.Initializing, AddressOf Me.DeviceInitializing
            AddHandler Me.VisaSessionBase.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            ' Me.VisaSessionBase.SessionFactory.CandidateResourceName = isr.VI.Ttm.My.Settings.ResourceName
            If Me.VisaSessionBase.IsSessionOpen Then
                Me.DeviceOpened(Me.VisaSessionBase, System.EventArgs.Empty)
            Else
                Me.DeviceClosed(Me.VisaSessionBase, System.EventArgs.Empty)
            End If
            Me.HandlePropertyChanged(Me.VisaSessionBase.SessionFactory, NameOf(Me.VisaSessionBase.SessionFactory.IsOpen))
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="visaSessionBase"> True to show or False to hide the control. </param>
    Public Overridable Sub BindVisaSessionBase(ByVal visaSessionBase As VI.VisaSessionBase)
        Me.BindVisaSessionBaseThis(visaSessionBase)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.VisaSessionBase.ResourceNameCaption} reading service request"
        Try
            Me.VisaSessionBase.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#Region " SESSION FACTORY "

    ''' <summary> Name of the resource. </summary>
    Private _ResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourceName As String
        Get
            Return Me._ResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceName) Then
                Me._ResourceName = value
                Me.NotifyPropertyChanged()
            End If
            If Me.VisaSessionBase IsNot Nothing Then
                Me.VisaSessionBase.CandidateResourceName = value
            End If
        End Set
    End Property

    ''' <summary> A filter specifying the resource. </summary>
    Private _ResourceFilter As String

    ''' <summary> Gets or sets the Search Pattern of the resource. </summary>
    ''' <value> The Search Pattern of the resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourceFilter As String
        Get
            Return Me._ResourceFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceFilter) Then
                Me._ResourceFilter = value
                Me.NotifyPropertyChanged()
            End If
            If Me.VisaSessionBase IsNot Nothing Then
                Me.VisaSessionBase.ResourcesFilter = value
            End If
        End Set
    End Property

    ''' <summary> Executes the session factory property changed action. </summary>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Sub HandlePropertyChanged(ByVal sender As SessionFactory, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(SessionFactory.ValidatedResourceName)
                If sender.IsOpen Then
                    Me.PublishInfo($"Resource connected;. {sender.ValidatedResourceName}")
                Else
                    Me.PublishInfo($"Resource locate;. {sender.ValidatedResourceName}")
                End If
                Me.ResourceName = sender.ValidatedResourceName
            Case NameOf(SessionFactory.CandidateResourceName)
                If Not sender.IsOpen Then
                    Me.PublishInfo($"Candidate resource;. {sender.ValidatedResourceName}")
                End If
                Me.ResourceName = sender.CandidateResourceName
            Case NameOf(SessionFactory.ResourcesFilter)
        End Select
    End Sub

    ''' <summary> Session factory property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionFactoryPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling session factory {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SessionFactoryPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SessionFactory), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE EVENTS "

    ''' <summary>
    ''' Event handler. Called upon device opening so as to instantiated all subsystems.
    ''' </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary>
    ''' Event handler. Called after the device opened and all subsystems were defined.
    ''' </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim outcome As TraceEventType = TraceEventType.Information
        If Me.VisaSessionBase.Session.Enabled And Not Me.VisaSessionBase.Session.IsSessionOpen Then outcome = TraceEventType.Warning
        Me.Publish(outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ",
                   Me.VisaSessionBase.ResourceTitleCaption, Me.VisaSessionBase.Session.Enabled.GetHashCode,
                   Me.VisaSessionBase.Session.IsDeviceOpen.GetHashCode, Me.VisaSessionBase.Session.IsSessionOpen.GetHashCode)
        Dim activity As String = String.Empty
        Try
            activity = $"Opened {Me.ResourceName}" : Me.PublishVerbose($"{activity};. ")
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " OPENING / OPEN "

    ''' <summary>
    ''' Attempts to open a session to the device using the specified resource name.
    ''' </summary>
    ''' <remarks> David, 2020-06-08. </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The title. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryOpenDeviceSession(ByVal resourceName As String, ByVal resourceTitle As String) As (Success As Boolean, Details As String)
        Dim activity As String = $"opening {resourceName} VISA session"
        Try
            Dim r As (Success As Boolean, Details As String) = Me.VisaSessionBase.TryOpenSession(resourceName, resourceTitle)
            Return If(r.Success, (r.Success, r.Details), (r.Success, $"failed {activity};. { r.Details}"))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Function

#End Region

#Region " INITALIZING / INITIALIZED  "

    ''' <summary> Device initializing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Overridable Sub DeviceInitializing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

#End Region

#Region " CLOSING / CLOSED "

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                activity = $"Disconnecting from {Me.ResourceName}" : Me.PublishInfo($"{activity};. ")
                If Me.VisaSessionBase.Session IsNot Nothing Then Me.VisaSessionBase.Session.DisableServiceRequestEventHandler()
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        If Me.VisaSessionBase IsNot Nothing Then
            Try
                If Me.VisaSessionBase.Session.IsSessionOpen Then
                    activity = $"{Me.VisaSessionBase.Session.ResourceNameCaption} closed but session is still open" : Me.PublishWarning($"{activity};. ")
                ElseIf Me.VisaSessionBase.Session.IsDeviceOpen Then
                    activity = $"{Me.VisaSessionBase.Session.ResourceNameCaption} closed but emulated session is still open" : Me.PublishWarning($"{activity};. ")
                Else
                    activity = $"Disconnected from {Me.ResourceName}" : Me.PublishVerbose($"{activity};. ")
                End If
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        Else
            activity = $"Already disconnected; device disposed" : Me.PublishInfo($"{activity};. ")
        End If
    End Sub

#End Region

#End Region

#Region " LAYOUT "

    ''' <summary> Refresh layout. </summary>
    Private Sub RefreshLayout()
        Me._Layout.RowStyles.Clear()
        Me._Layout.RowStyles.Add(New RowStyle(SizeType.AutoSize))
        Me._Layout.RowStyles.Add(New RowStyle(SizeType.Percent, 100))
        Me._Layout.RowStyles.Add(New RowStyle(SizeType.AutoSize))
        If Me.HeaderControl IsNot Nothing Then
            Me._Layout.SetRow(Me.HeaderControl, 0)
        End If
        If Me._TreePanel IsNot Nothing Then Me._Layout.SetRow(Me._TreePanel, 1)
        If Me.FooterControl IsNot Nothing Then Me._Layout.SetRow(Me.FooterControl, 0)
        Me._Layout.Refresh()
    End Sub

    ''' <summary> Gets the header control. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The header control. </value>
    Private Property HeaderControl As Control

    ''' <summary> Adds a header. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="headerControl"> The control. </param>
    Public Sub AddHeader(ByVal headerControl As Control)
        If headerControl Is Nothing Then Throw New ArgumentNullException(NameOf(headerControl))
        Me.HeaderControl = headerControl
        Me._Layout.Controls.Add(headerControl, 1, 0)
        headerControl.Dock = DockStyle.Top
        Me.RefreshLayout()
    End Sub

    ''' <summary> Gets the footer control. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The footer control. </value>
    Private Property FooterControl As Control

    ''' <summary> Adds a footer. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="footerControl"> The footer control. </param>
    Public Sub AddFooter(ByVal footerControl As Control)
        If footerControl Is Nothing Then Throw New ArgumentNullException(NameOf(footerControl))
        Me.FooterControl = footerControl
        Me._Layout.Controls.Add(footerControl, 1, 2)
        footerControl.Dock = DockStyle.Bottom
        Me.RefreshLayout()
    End Sub

    ''' <summary> Name of the messages node. </summary>
    Private Const _MessagesNodeName As String = "Messages"

    ''' <summary> Selects the navigator tree view node. </summary>
    ''' <param name="nodeName"> The node. </param>
    Public Sub SelectNavigatorTreeViewNode(ByVal nodeName As String)
        Me._TreePanel.SelectNavigatorTreeViewNode(nodeName)
    End Sub

    ''' <summary> Adds a node. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="nodeName">    The node. </param>
    ''' <param name="nodeCaption"> The node caption. </param>
    ''' <param name="nodeControl"> The node control. </param>
    ''' <returns> A TreeNode. </returns>
    Public Function AddNode(ByVal nodeName As String, ByVal nodeCaption As String, ByVal nodeControl As Control) As TreeNode
        Return Me._TreePanel.AddNode(nodeName, nodeCaption, nodeControl)
    End Function

    ''' <summary> Inserts a node. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="index">       Zero-based index of the. </param>
    ''' <param name="nodeName">    The node. </param>
    ''' <param name="nodeCaption"> The node caption. </param>
    ''' <param name="nodeControl"> The node control. </param>
    ''' <returns> A TreeNode. </returns>
    Public Function InsertNode(ByVal index As Integer, ByVal nodeName As String, ByVal nodeCaption As String, ByVal nodeControl As Control) As TreeNode
        Return Me._TreePanel.InsertNode(index, nodeName, nodeCaption, nodeControl)
    End Function

    ''' <summary> Removes the node described by nodeName. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="nodeName"> The node. </param>
    Public Sub RemoveNode(ByVal nodeName As String)
        Me._TreePanel.RemoveNode(nodeName)
    End Sub

    ''' <summary> After node selected. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="e"> Tree view event information. </param>
    Protected Overridable Sub AfterNodeSelected(e As TreeViewEventArgs)
    End Sub

    ''' <summary> Tree panel after node selected. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    Private Sub TreePanel_AfterNodeSelected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles _TreePanel.AfterNodeSelected
        Me.AfterNodeSelected(e)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the current trace messages box. </summary>
    Protected Overloads Sub AddPrivateListeners()
        Me.AddPrivateListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Assign talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(ByVal talker As ITraceMessageTalker)
        MyBase.AssignTalker(talker)
        Me.AddListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

    ''' <summary> Executes the trace messages box property changed action. </summary>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(sender As TraceMessagesBox, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If String.Equals(propertyName, NameOf(isr.Core.Forma.TraceMessagesBox.StatusPrompt)) Then
            Me._StatusLabel.Text = isr.Core.WinForms.CompactExtensions.Methods.Compact(sender.StatusPrompt, Me._StatusLabel)
            Me._StatusLabel.ToolTipText = sender.StatusPrompt
        End If
    End Sub

    ''' <summary> Trace messages box property changed. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceMessagesBox_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} handling trace message box {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TraceMessagesBox_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TraceMessagesBox), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " PROTECTED CONTROLS "

    ''' <summary> Gets the trace messages box. </summary>
    ''' <value> The trace messages box. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property TraceMessagesBox As TraceMessagesBox
        Get
            Return Me._TraceMessagesBox
        End Get
    End Property

    ''' <summary> Gets the status strip. </summary>
    ''' <value> The status strip. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property StatusStrip As StatusStrip
        Get
            Return Me._StatusStrip
        End Get
    End Property

    ''' <summary> Gets the status label. </summary>
    ''' <value> The status label. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property StatusLabel As ToolStripStatusLabel
        Get
            Return Me._StatusLabel
        End Get
    End Property

    ''' <summary> Gets the progress bar. </summary>
    ''' <value> The progress bar. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property ProgressBar As Core.Controls.StatusStripCustomProgressBar
        Get
            Return Me._ProgressBar
        End Get
    End Property

    ''' <summary> Gets or sets the progress percent. </summary>
    ''' <value> The progress percent. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ProgressPercent As Integer
        Get
            Return Me.ProgressBar.Value
        End Get
        Set(value As Integer)
            If value <> Me.ProgressPercent Then
                Me.ProgressBar.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
