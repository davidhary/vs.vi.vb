'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase_ResourceInfo.vb
'
' summary:	Session base resource information class
'---------------------------------------------------------------------------------------------------

Partial Public Class SessionBase

#Region " RESOURCE NAME PARSE INFO "

    ''' <summary> Gets or sets information describing the parsed resource name. </summary>
    ''' <value> Information describing the resource. </value>
    Public ReadOnly Property ResourceNameInfo As ResourceNameInfo

#End Region

#Region " FILTER "

    ''' <summary> A filter specifying the resources. </summary>
    Private _ResourcesFilter As String

    ''' <summary> Gets or sets the resources search pattern. </summary>
    ''' <value> The resources search pattern. </value>
    Public Property ResourcesFilter As String
        Get
            Return Me._ResourcesFilter
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ResourcesFilter) Then
                Me._ResourcesFilter = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " NAME  "

    ''' <summary> Updates the validated and open resource names and title. </summary>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The short title of the device. </param>
    Public Sub HandleSessionOpen(ByVal resourceName As String, ByVal resourceTitle As String)
        Me.ValidatedResourceName = resourceName
        Me.CandidateResourceNameConnected = String.Equals(resourceName, Me.CandidateResourceName, StringComparison.OrdinalIgnoreCase)
        Me.OpenResourceName = resourceName
        Me.OpenResourceTitle = resourceTitle
    End Sub

    ''' <summary> Gets the name of the designated resource. </summary>
    ''' <value> The name of the designated resource. </value>
    Public ReadOnly Property DesignatedResourceName As String
        Get
            Return If(Me.CandidateResourceNameConnected, Me.ValidatedResourceName, Me.CandidateResourceName)
        End Get
    End Property


    ''' <summary>
    ''' Checks if the candidate resource name exists. If so, assign to the
    ''' <see cref="ValidatedResourceName">validated resource name</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function ValidateCandidateResourceName() As Boolean


    ''' <summary>
    ''' Checks if the candidate resource name exists. If so, assign to the
    ''' <see cref="ValidatedResourceName">validated resource name</see>;
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager"> The manager. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function ValidateCandidateResourceName(ByVal manager As VI.Pith.ResourcesProviderBase) As Boolean
        If manager Is Nothing Then Throw New ArgumentNullException(NameOf(manager))
        Dim result As Boolean = If(String.IsNullOrWhiteSpace(Me.ResourcesFilter),
            manager.FindResources().ToArray.Contains(Me.CandidateResourceName),
            manager.FindResources(Me.ResourcesFilter).ToArray.Contains(Me.CandidateResourceName))
        Me.ValidatedResourceName = If(result, Me.CandidateResourceName, String.Empty)
        Me.CandidateResourceNameConnected = result
        Return result
    End Function

    ''' <summary> Name of the validated resource. </summary>
    Private _ValidatedResourceName As String

    ''' <summary> Gets or sets the name of the validated (e.g., located) resource. </summary>
    ''' <value> The name of the validated (e.g., located)  resource. </value>
    Public Property ValidatedResourceName As String
        Get
            Return Me._ValidatedResourceName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ValidatedResourceName) Then
                Me._ValidatedResourceName = value
                Me.NotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> True if candidated resource name validated. </summary>
    Private _CandidatedResourceNameValidated As Boolean

    ''' <summary> Gets or sets the candidate resource name validated. </summary>
    ''' <value> The candidate resource name validated. </value>
    Public Property CandidateResourceNameConnected As Boolean
        Get
            Return Me._CandidatedResourceNameValidated
        End Get
        Set(value As Boolean)
            If value <> Me.CandidateResourceNameConnected Then
                Me._CandidatedResourceNameValidated = value
                Me.NotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> Name of the candidate resource. </summary>
    Private _CandidateResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the candidate resource. </value>
    Public Property CandidateResourceName As String
        Get
            Return Me._CandidateResourceName
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.CandidateResourceName, value) Then
                Me._CandidateResourceName = value
                Me.NotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> Name of the open resource. </summary>
    Private _OpenResourceName As String

    ''' <summary> Gets or sets the name of the open resource. </summary>
    ''' <value> The name of the open resource. </value>
    Public Property OpenResourceName As String
        Get
            Return Me._OpenResourceName
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.OpenResourceName, value) Then
                Me._OpenResourceName = value
                Me.NotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> Updates the captions. </summary>
    Private Sub UpdateCaptions()
        If Me.IsSessionOpen Then
            Me.ResourceNameCaption = $"{Me.OpenResourceTitle}.{Me.OpenResourceName}"
            Me.ResourceTitleCaption = Me.OpenResourceTitle
            Me.CandidateResourceTitle = Me.OpenResourceTitle
        ElseIf String.IsNullOrWhiteSpace(Me.OpenResourceTitle) Then
            ' if session was not open then display the candidate values
            Me.ResourceNameCaption = $"{Me.CandidateResourceTitle}.{Me.CandidateResourceName}.{Me.ResourceClosedCaption}"
            Me.ResourceTitleCaption = $"{Me.CandidateResourceTitle}.{Me.ResourceClosedCaption}"
        Else
            ' if session was open then display the open values
            Me.ResourceNameCaption = $"{Me.OpenResourceTitle}.{Me.OpenResourceName}.{Me.ResourceClosedCaption}"
            Me.ResourceTitleCaption = $"{Me.OpenResourceTitle}.{Me.ResourceClosedCaption}"
        End If
    End Sub

#End Region

#Region " TITLE "

    ''' <summary> The candidate resource title. </summary>
    Private _CandidateResourceTitle As String

    ''' <summary> Gets or sets the candidate resource title. </summary>
    ''' <value> The candidate resource title. </value>
    Public Property CandidateResourceTitle As String
        Get
            Return Me._CandidateResourceTitle
        End Get
        Set(value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(value, Me.CandidateResourceTitle) Then
                Me._CandidateResourceTitle = value
                Me.SyncNotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> The open resource title. </summary>
    Private _OpenResourceTitle As String

    ''' <summary> Gets or sets a short title for the device. </summary>
    ''' <value> The short title of the device. </value>
    Public Property OpenResourceTitle As String
        Get
            Return Me._OpenResourceTitle
        End Get
        Set(value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.OpenResourceTitle, value) Then
                Me._OpenResourceTitle = value
                Me.NotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> The resource title caption. </summary>
    Private _ResourceTitleCaption As String

    ''' <summary> Gets or sets the Title caption. </summary>
    ''' <value> The Title caption. </value>
    Public Property ResourceTitleCaption As String
        Get
            Return Me._ResourceTitleCaption
        End Get
        Set(value As String)
            If Not String.Equals(Me.ResourceTitleCaption, value) Then
                Me._ResourceTitleCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CAPTION "

    ''' <summary> The resource closed caption. </summary>
    Private _ResourceClosedCaption As String

    ''' <summary> Gets or sets the default resource name closed caption. </summary>
    ''' <value> The resource closed caption. </value>
    Public Property ResourceClosedCaption As String
        Get
            Return Me._ResourceClosedCaption
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.ResourceClosedCaption, value) Then
                Me._ResourceClosedCaption = value
                Me.SyncNotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> The resource name caption. </summary>
    Private _ResourceNameCaption As String

    ''' <summary> Gets or sets the resource name caption. </summary>
    ''' <value> The <see cref="ResourceNameCaption"/> resource tagged as closed if not open. </value>
    Public Property ResourceNameCaption As String
        Get
            Return Me._ResourceNameCaption
        End Get
        Set(value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.ResourceNameCaption, value) Then
                Me._ResourceNameCaption = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class

