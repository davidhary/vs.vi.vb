﻿''' <summary> System subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class SystemSubsystem
    Inherits VI.Tsp2.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="P:isr.VI.SubsystemPlusStatusBase.StatusSubsystem">TSP
    '''                                status Subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "



#End Region

#Region " BEEPER IMMEDIATE "

    ''' <summary> Gets or sets the Beeper enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overrides Property BeeperEnabledQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the Beeper enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overrides Property BeeperEnabledCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the beeper immediate command format. </summary>
    ''' <value> The beeper immediate command format. </value>
    Protected Overrides Property BeeperImmediateCommandFormat As String = "beeper.beep({0},{1})"

#End Region

#Region " FAN LEVEL "

    ''' <summary> Converts the specified value to string. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="P:isr.VI.SystemSubsystemBase.FanLevel">Fan Level</see>. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function FromFanLevel(ByVal value As FanLevel) As String
        Return If(value = VI.FanLevel.Normal, "fan.LEVEL_NORMAL", "fan.LEVEL_QUIET")
    End Function

    ''' <summary> Gets or sets the Fan Level query command. </summary>
    ''' <value> The Fan Level command. </value>
    Protected Overrides Property FanLevelQueryCommand As String = "_G.print(_G.fan.level)"

    ''' <summary> Gets or sets the Fan Level command format. </summary>
    ''' <value> The Fan Level command format. </value>
    Protected Overrides Property FanLevelCommandFormat As String = "_G.fan.level={0}"

#End Region

End Class
