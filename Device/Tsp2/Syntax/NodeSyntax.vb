'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Syntax\NodeSyntax.vb
'
' summary:	Node syntax class
'---------------------------------------------------------------------------------------------------
Namespace TspSyntax.Node

    ''' <summary> Defines the TSP Node syntax. Modified for TSP2. </summary>
    ''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2005-01-15, 1.0.1841.x. </para></remarks>
    Public Module NodeSyntax

#Region " NODE IDENTITY "

        ''' <summary> Gets or sets the IDN print command format. </summary>
        ''' <remarks>
        ''' Same as '*IDN?'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para><code>
        ''' Value = String.Format(IdentityPrintCommandFormat,"node name")
        ''' </code>
        ''' </remarks>
        ''' <value> The identity print command format. </value>
        Public Property IdentityPrintCommandBuilder As String = "_G.print(""Keithley Instruments Inc., Model ""..{0}.model.."", ""..{0}.serialno.."", ""..{0}.version)"

#End Region

#Region " NODE COMMANDS "

        ''' <summary> Gets the status clear (CLS) command message. Requires a node number argument. </summary>
        Public Const CollectNodeGarbageFormat As String = "_G.node[{0}].execute('collectgarbage()') _G.waitcomplete({0})"

        ''' <summary> Gets the execute command.  Requires node number and command arguments. </summary>
        Public Const ExecuteNodeCommandFormat As String = "_G.node[{0}].execute(""{1}"") _G.waitcomplete({0})"

        ''' <summary> Gets the value returned by executing a command on the node.
        ''' Requires node number and value to get arguments. </summary>
        Public Const ValueGetterCommandFormat1 As String = "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete({0}) _G.waitcomplete() _G.print(_G.node[{0}].dataqueue.next())"
        '  3517 "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete(0) _G.print(_G.node[{0}].dataqueue.next())"

        ''' <summary> Gets the value returned by executing a command on the node.
        ''' Requires node number, command, and value to get arguments. </summary>
        Public Const ValueGetterCommandFormat2 As String = "_G.node[{0}].execute(""do {1} dataqueue.add({2}) end"") _G.waitcomplete({0}) _G.waitcomplete() _G.print(_G.node[{0}].dataqueue.next())"
        '  3517 "_G.node[{0}].execute(""do {1} dataqueue.add({2}) end"") _G.waitcomplete(0) _G.print(_G.node[{0}].dataqueue.next())"

        ''' <summary> Gets the connect rule command. Requires node number and value arguments. </summary>
        Public Const ConnectRuleSetterCommandFormat As String = "_G.node[{0}].channel.connectrule = {1}  _G.waitcomplete({0})"

#End Region

#Region " SYSTEM COMMAND BUILDERS "

        ''' <summary> Gets or sets the reset command Builder. </summary>
        ''' <remarks>
        ''' Same as '*RST'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The reset known state command builder. </value>
        Public Property ResetKnownStateCommandBuilder As String = "{0}.reset()"

        ''' <summary> Gets or sets the status clear (CLS) command Builder. </summary>
        ''' <remarks>
        ''' Same as '*CLS'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The clear execution state command builder. </value>
        Public Property ClearExecutionStateCommandBuilder As String = "{0}.status.reset()"

#End Region

#Region " ERROR QUEUE COMMAND BUILDERS "

        ''' <summary> Gets or sets the error queue clear command builder. </summary>
        ''' <remarks>
        ''' Same as ':STAT:QUE:CLEAR'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The clear error queue command builder. </value>
        Public Property ClearErrorQueueCommandBuilder As String = "{0}.eventlog.clear()"

        ''' <summary> Gets or sets the error queue print query command builder. </summary>
        ''' <remarks>
        ''' Same as ':STAT:QUE?'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The error queue print command builder. </value>
        Public Property ErrorQueuePrintCommandBuilder As String = "_G.print(string.format('%d,%s,level=%d',{0}.eventlog.next(eventlog.SEV_ERROR)))"

        ''' <summary> Gets or sets the error queue count print query command builder. </summary>
        ''' <remarks> Requires setting the subsystem reference. </remarks>
        ''' <value> The error queue count print command builder. </value>
        Public Property ErrorQueueCountPrintCommandBuilder As String = "_G.print({0}.eventlog.getcount(eventlog.SEV_ERROR))"

#End Region

#Region " REGISTERS "

#Region " OPERATION EVENTS "

        ''' <summary> Gets or sets the operation event enable command format builder. </summary>
        ''' <remarks>
        ''' Same as ''.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The operation event enable command format builder. </value>
        Public Property OperationEventEnableCommandFormatBuilder As String = "{0}.status.operation.enable = {{0}}"

        ''' <summary> Gets or sets the operation event enable print query command builder. </summary>
        ''' <remarks>
        ''' Same as ''.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The operation event enable print command builder. </value>
        Public Property OperationEventEnablePrintCommandBuilder As String = "_G.print(_G.tostring({0}.status.operation.enable))"

        ''' <summary> Gets or sets the operation event status print query command builder. </summary>
        ''' <remarks>
        ''' Same as ''.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The operation event print command builder. </value>
        Public Property OperationEventPrintCommandBuilder As String = "_G.print(_G.tostring({0}.status.operation.event))"

#End Region

#Region " SERVICE REQUEST "

        ''' <summary> Gets or sets the service request enable command format builder. </summary>
        ''' <remarks>
        ''' Same as *SRE {0:D}'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The service request enable command format builder. </value>
        Public Property ServiceRequestEnableCommandFormatBuilder As String = "{0}.status.request_enable = {{0}}"

        ''' <summary> Gets or sets the service request enable print query command builder. </summary>
        ''' <remarks>
        ''' Same as ''.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The service request enable print command builder. </value>
        Public Property ServiceRequestEnablePrintCommandBuilder As String = "_G.print(_G.tostring({0}.status.request_enable))"

        ''' <summary> Gets or sets the service request enable print query command builder. </summary>
        ''' <remarks>
        ''' Same as '*ESR?'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The service request event print command builder. </value>
        Public Property ServiceRequestEventPrintCommandBuilder As String = "_G.print(_G.tostring({0}.status.condition))"

#End Region

#Region " STANDARD EVENTS "

        ''' <summary> Gets or sets the standard event enable command format builder. </summary>
        ''' <remarks>
        ''' Same as *ESE {0:D}'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The standard event enable command format builder. </value>
        Public Property StandardEventEnableCommandFormatBuilder As String = "{0}.status.standard.enable = {{0}}"

        ''' <summary> Gets or sets the standard event enable print query command builder. </summary>
        ''' <remarks>
        ''' Same as ''.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The standard event enable print command builder. </value>
        Public Property StandardEventEnablePrintCommandBuilder As String = "_G.print(_G.tostring({0}.status.standard.enable))"

        ''' <summary> Gets or sets the standard event status print query command builder. </summary>
        ''' <remarks>
        ''' Same as *ESR?'.<para>
        ''' Requires setting the subsystem reference.
        ''' </para>
        ''' </remarks>
        ''' <value> The standard event print command builder. </value>
        Public Property StandardEventPrintCommandBuilder As String = "_G.waitcomplete() _G.print(_G.tostring({0}.status.standard.event))"

#End Region

#End Region

    End Module

End Namespace

