'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\DigitalOutputView.vb
'
' summary:	DigitalOutput view class
'---------------------------------------------------------------------------------------------------
Imports System.Windows.Forms

Imports isr.Core.SplitExtensions

''' <summary>
''' A Keithley 2002 edition of the basic <see cref="Facade.DigitalOutputView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-11-13 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class DigitalOutputView
    Inherits isr.VI.Facade.DigitalOutputView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-11-13. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new <see cref="DigitalOutputView"/> </summary>
    ''' <remarks> David, 2020-11-13. </remarks>
    ''' <returns> A <see cref="DigitalOutputView"/>. </returns>
    Public Overloads Shared Function Create() As DigitalOutputView
        Dim view As DigitalOutputView = Nothing
        Try
            view = New DigitalOutputView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K2002Device As K2002Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-11-13. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K2002Device)
        MyBase.AssignDevice(value)
        Me._K2002Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, DigitalOutputSubsystemBase))
        Else
            Me.BindSubsystem(Me.K2002Device.DigitalOutputSubsystem)
        End If
    End Sub

#End Region

End Class
