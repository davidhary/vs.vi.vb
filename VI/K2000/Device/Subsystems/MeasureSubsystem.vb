''' <summary> Defines a Measure Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class MeasureSubsystem
    Inherits VI.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        ' reading elements are added (reading is initialized) when the format system is reset.
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefineFunctionModeReadWrites("""{0}""", "'{0}'")
    End Sub

#End Region

#Region " I PRESETTABLE "

#End Region

#Region " COMMAND SYNTAX "

#Region " READ, FETCH "

    ''' <summary> Gets or sets the fetch command. </summary>
    ''' <value> The fetch command. </value>
    Protected Overrides Property FetchCommand As String = ":FETCH?"

    ''' <summary> Gets or sets the read command. </summary>
    ''' <value> The read command. </value>
    Protected Overrides Property ReadCommand As String = ":READ?"

    ''' <summary> Gets or sets The Measure query command. </summary>
    ''' <value> The Measure query command. </value>
    Protected Overrides Property MeasureQueryCommand As String = ":READ?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#End Region

#Region " READ  FETCH "

    ''' <summary> Fetches the data. </summary>
    ''' <remarks>
    ''' Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
    ''' there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
    ''' the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
    ''' Buffer. Thus, subsequent executions of FETCh? acquire the same data.
    ''' </remarks>
    ''' <returns> The reading. </returns>
    Public Overrides Function FetchReading() As String
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return MyBase.FetchReading()
    End Function

    ''' <summary> Fetches the data. </summary>
    ''' <remarks>
    ''' Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
    ''' there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
    ''' the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
    ''' Buffer. Thus, subsequent executions of FETCh? acquire the same data.
    ''' </remarks>
    ''' <returns> A Double? </returns>
    Public Overrides Function Fetch() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return MyBase.Fetch()
    End Function

    ''' <summary> Initiates an operation and then fetches the data. </summary>
    ''' <remarks>
    ''' Issues the 'READ?' query, which performs a trigger initiation and then a
    ''' <see cref="M:isr.VI.MeasureSubsystemBase.Fetch">FETCh? </see>
    ''' The initiate triggers a new measurement cycle which puts new data in the Sample Buffer. Fetch
    ''' reads that new data. The
    ''' <see cref="M:isr.VI.MeasureCurrentSubsystemBase.Measure">Measure</see> command places the
    ''' instrument in a “one-shot” mode and then performs a read.
    ''' </remarks>
    ''' <returns> A Double? </returns>
    Public Overrides Function Read() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return MyBase.Read()
    End Function

#End Region

End Class

