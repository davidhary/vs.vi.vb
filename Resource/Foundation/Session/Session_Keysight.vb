'---------------------------------------------------------------------------------------------------
' file:		.\Session\KeysightClass1.vb
'
' summary:	Keysight class 1 class
'---------------------------------------------------------------------------------------------------
Partial Public Class Session

    ''' <summary> Lists the attributes not supported for this vendor file. </summary>
    ''' <value> The vendor unsupported attributes. </value>
    <CLSCompliant(False)>
    Private Shared ReadOnly Property VendorUnsupportedAttributes As ObjectModel.Collection(Of Ivi.Visa.NativeVisaAttribute)
        Get
            If Session._VendorUnsupportedAttributes Is Nothing Then
                Session._VendorUnsupportedAttributes = New ObjectModel.Collection(Of Ivi.Visa.NativeVisaAttribute) From {
                    {Ivi.Visa.NativeVisaAttribute.TcpKeepAlive}
                }
            End If
            Return Session._VendorUnsupportedAttributes
        End Get
    End Property

End Class
