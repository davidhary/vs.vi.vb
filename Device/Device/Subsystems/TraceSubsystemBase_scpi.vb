
Partial Public Class TraceSubsystemBase

#Region " FEED SOURCE "

    ''' <summary> Define feed source read writes. </summary>
    Private Sub DefineFeedSourceReadWrites()
        Me._FeedSourceReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As FeedSources In [Enum].GetValues(GetType(FeedSources))
            Me._FeedSourceReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Feed source parses. </summary>
    ''' <value> A Dictionary of Feed source parses. </value>
    Public ReadOnly Property FeedSourceReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported feed sources. </summary>
    Private _SupportedFeedSources As FeedSources

    ''' <summary> Gets or sets the supported Feed sources. </summary>
    ''' <value> The supported Feed sources. </value>
    Public Property SupportedFeedSources() As FeedSources
        Get
            Return Me._SupportedFeedSources
        End Get
        Set(ByVal value As FeedSources)
            If Not Me.SupportedFeedSources.Equals(value) Then
                Me._SupportedFeedSources = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The feed source. </summary>
    Private _FeedSource As FeedSources?

    ''' <summary> Gets or sets the cached source FeedSource. </summary>
    ''' <value>
    ''' The <see cref="FeedSource">source Feed Source</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property FeedSource As FeedSources?
        Get
            Return Me._FeedSource
        End Get
        Protected Set(ByVal value As FeedSources?)
            If Not Me.FeedSource.Equals(value) Then
                Me._FeedSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Feed Source. </summary>
    ''' <param name="value"> The  Source Feed Source. </param>
    ''' <returns> The <see cref="FeedSource">source Feed Source</see> or none if unknown. </returns>
    Public Function ApplyFeedSource(ByVal value As FeedSources) As FeedSources?
        Me.WriteFeedSource(value)
        Return Me.QueryFeedSource()
    End Function

    ''' <summary> Gets or sets the Feed source query command. </summary>
    ''' <remarks> SCPI: ":TRIG:SOUR?". </remarks>
    ''' <value> The Feed source query command. </value>
    Protected Overridable Property FeedSourceQueryCommand As String

    ''' <summary> Queries the Feed source. </summary>
    ''' <returns> The <see cref="FeedSource">Feed source</see> or none if unknown. </returns>
    Public Function QueryFeedSource() As FeedSources?
        Me.FeedSource = Me.Query(Of FeedSources)(Me.FeedSourceQueryCommand, Me.FeedSource.GetValueOrDefault(FeedSources.None), Me.FeedSourceReadWrites)
        Return Me.FeedSource
    End Function

    ''' <summary> Gets or sets the Feed source command format. </summary>
    ''' <remarks> SCPI: "{0}". </remarks>
    ''' <value> The write Feed source command format. </value>
    Protected Overridable Property FeedSourceCommandFormat As String

    ''' <summary> Writes the Feed Source without reading back the value from the device. </summary>
    ''' <param name="value"> The Feed Source. </param>
    ''' <returns> The <see cref="FeedSource">Feed Source</see> or none if unknown. </returns>
    Public Function WriteFeedSource(ByVal value As FeedSources) As FeedSources?
        Me.FeedSource = Me.Write(Of FeedSources)(Me.FeedSourceCommandFormat, value, Me.FeedSourceReadWrites)
        Return Me.FeedSource
    End Function

#End Region

#Region " FEED CONTROL "

    ''' <summary> Define feed control read writes. </summary>
    Private Sub DefineFeedControlReadWrites()
        Me._FeedControlReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As FeedControls In [Enum].GetValues(GetType(FeedControls))
            Me._FeedControlReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Feed Control parses. </summary>
    ''' <value> A Dictionary of Feed Control parses. </value>
    Public ReadOnly Property FeedControlReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported feed controls. </summary>
    Private _SupportedFeedControls As FeedControls

    ''' <summary> Gets or sets the supported Feed Controls. </summary>
    ''' <value> The supported Feed Controls. </value>
    Public Property SupportedFeedControls() As FeedControls
        Get
            Return Me._SupportedFeedControls
        End Get
        Set(ByVal value As FeedControls)
            If Not Me.SupportedFeedControls.Equals(value) Then
                Me._SupportedFeedControls = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The feed control. </summary>
    Private _FeedControl As FeedControls?

    ''' <summary> Gets or sets the cached Control FeedControl. </summary>
    ''' <value>
    ''' The <see cref="FeedControl">Control Feed Control</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property FeedControl As FeedControls?
        Get
            Return Me._FeedControl
        End Get
        Protected Set(ByVal value As FeedControls?)
            If Not Me.FeedControl.Equals(value) Then
                Me._FeedControl = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Control Feed Control. </summary>
    ''' <param name="value"> The  Control Feed Control. </param>
    ''' <returns> The <see cref="FeedControl">Control Feed Control</see> or none if unknown. </returns>
    Public Function ApplyFeedControl(ByVal value As FeedControls) As FeedControls?
        Me.WriteFeedControl(value)
        Return Me.QueryFeedControl()
    End Function

    ''' <summary> Gets or sets the Feed Control query command. </summary>
    ''' <remarks> SCPI: ":TRIG:SOUR?". </remarks>
    ''' <value> The Feed Control query command. </value>
    Protected Overridable Property FeedControlQueryCommand As String

    ''' <summary> Queries the Feed Control. </summary>
    ''' <returns> The <see cref="FeedControl">Feed Control</see> or none if unknown. </returns>
    Public Function QueryFeedControl() As FeedControls?
        Me.FeedControl = Me.Query(Of FeedControls)(Me.FeedControlQueryCommand,
                                                       Me.FeedControl.GetValueOrDefault(FeedControls.None),
                                                       Me.FeedControlReadWrites)
        Return Me.FeedControl
    End Function

    ''' <summary> Gets or sets the Feed Control command format. </summary>
    ''' <remarks> SCPI: "{0}". </remarks>
    ''' <value> The write Feed Control command format. </value>
    Protected Overridable Property FeedControlCommandFormat As String

    ''' <summary> Writes the Feed Control without reading back the value from the device. </summary>
    ''' <param name="value"> The Feed Control. </param>
    ''' <returns> The <see cref="FeedControl">Feed Control</see> or none if unknown. </returns>
    Public Function WriteFeedControl(ByVal value As FeedControls) As FeedControls?
        Me.FeedControl = Me.Write(Of FeedControls)(Me.FeedControlCommandFormat, value, Me.FeedControlReadWrites)
        Return Me.FeedControl
    End Function

#End Region

End Class

''' <summary> Enumerates the trace feed control. </summary>
<Flags>
Public Enum FeedControls

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the next] option. </summary>
    <ComponentModel.Description("Sense (NEXT)")>
    [Next] = 1

    ''' <summary> An enum constant representing the never] option. </summary>
    <ComponentModel.Description("Never (NEV)")>
    [Never] = 2

    ''' <summary> An enum constant representing the always option. </summary>
    <ComponentModel.Description("Always (ALW)")>
    Always = 3

    ''' <summary> An enum constant representing the pre trigger option. </summary>
    <ComponentModel.Description("Pre-Trigger (PRET)")>
    PreTrigger = 4
End Enum

''' <summary> Enumerates the source of readings. </summary>
<Flags>
Public Enum FeedSources

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None (NONE)")>
    None = 0

    ''' <summary> An enum constant representing the sense option. </summary>
    <ComponentModel.Description("Sense (SENS1)")>
    Sense = 1

    ''' <summary> An enum constant representing the calculate 1 option. </summary>
    <ComponentModel.Description("Calculate 1 (CALC)")>
    Calculate1 = 2

    ''' <summary> An enum constant representing the calculate 2 option. </summary>
    <ComponentModel.Description("Calculate 2 (CALC2)")>
    Calculate2 = 4

    ''' <summary> An enum constant representing the current option. </summary>
    <ComponentModel.Description("Current (CURR)")>
    Current = 8

    ''' <summary> An enum constant representing the voltage option. </summary>
    <ComponentModel.Description("Voltage (VOLT)")>
    Voltage = 16

    ''' <summary> An enum constant representing the resistance option. </summary>
    <ComponentModel.Description("Resistance (RES)")>
    Resistance = 32
End Enum
