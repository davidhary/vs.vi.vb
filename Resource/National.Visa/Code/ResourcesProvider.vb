'---------------------------------------------------------------------------------------------------
' file:		National.Visa\Code\ResourcesProvider.vb
'
' summary:	Resources provider class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.National.Visa.ExceptionExtensions
Imports isr.VI.National.Visa.ResourceManagerExtensions

''' <summary> The VIAA Resources Provider. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para>
''' </remarks>
Public Class ResourcesProvider
    Inherits VI.Pith.ResourcesProviderBase

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me._VisaResourceManager = New NationalInstruments.Visa.ResourceManager
    End Sub

#Region "IDisposable Support"

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._VisaResourceManager IsNot Nothing Then Me._VisaResourceManager.Dispose() : Me._VisaResourceManager = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing resource manager", $"Exception {ex.ToFullBlownString}")
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " RESOURCE MANAGER "

    ''' <summary> Gets or sets the manager for resource. </summary>
    ''' <value> The resource manager. </value>
    Private ReadOnly Property VisaResourceManager As NationalInstruments.Visa.ResourceManager

    ''' <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public Overrides ReadOnly Property IsDummy As Boolean = False

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Parse resource. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A VI.ResourceNameInfo. </returns>
    Public Overrides Function ParseResource(ByVal resourceName As String) As VI.Pith.ResourceNameInfo
        Return Me.VisaResourceManager.ParseResource(resourceName)
    End Function

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources. </summary>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources() As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindResources()
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindResources() As (Success As Boolean, Details As String)
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindResources(resources), "Resources not found")
    End Function

    ''' <summary> Lists all resources. </summary>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources(ByVal filter As String) As IEnumerable(Of String)
        Return Me.VisaResourceManager.Find(filter)
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindResources(ByVal filter As String) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindResources(filter, resources), "Resources not found", resources)
    End Function

    ''' <summary> Returns true if the specified resource exists. </summary>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function Exists(ByVal resourceName As String) As Boolean
        Return Me.VisaResourceManager.Exists(resourceName) AndAlso
               Not isr.VI.Pith.ResourceNamesManager.IsTcpipResource(resourceName) OrElse isr.VI.Pith.ResourceNamesManager.PingTcpipResource(resourceName)
    End Function

#End Region

#Region " INTERFACES "

    ''' <summary> Searches for the interface. </summary>
    ''' <param name="resourceName"> The interface resource name. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function InterfaceExists(ByVal resourceName As String) As Boolean
        Return Me.VisaResourceManager.InterfaceExists(resourceName)
    End Function

    ''' <summary> Searches for all interfaces. </summary>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces() As IEnumerable(Of String)
        Return Me.VisaResourceManager.Find(VI.Pith.ResourceNamesManager.BuildInterfaceFilter)
    End Function

    ''' <summary> Try find interfaces. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInterfaces() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindInterfaces(resources), "Resources not found", resources)
    End Function

    ''' <summary> Searches for the interfaces. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Return Me.VisaResourceManager.Find(VI.Pith.ResourceNamesManager.BuildInterfaceFilter(interfaceType))
    End Function

    ''' <summary> Try find interfaces. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindInterfaces(interfaceType, resources), "Resources not found", resources)
    End Function

#End Region

#Region " INSTRUMENTS  "

    ''' <summary> Searches for the instrument. </summary>
    ''' <param name="resourceName"> The instrument resource name. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function FindInstrument(ByVal resourceName As String) As Boolean
        Return Me.VisaResourceManager.InstrumentExists(resourceName)
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments() As IEnumerable(Of String)
        Return Me.VisaResourceManager.Find(VI.Pith.ResourceNamesManager.BuildInstrumentFilter())
    End Function

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInstruments() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindInstruments(resources), "Resources not found", resources)
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Return Me.VisaResourceManager.Find(VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType))
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindInstruments(interfaceType, resources), "Resources not found", resources)
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)
        Return Me.VisaResourceManager.Find(VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType, boardNumber))
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public Overrides Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                                     ByVal interfaceNumber As Integer) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim resources As IEnumerable(Of String) = New List(Of String)
        Return (Me.VisaResourceManager.TryFindInstruments(interfaceType, interfaceNumber, resources), "Resources not found", resources)
    End Function

#End Region

#Region " VERSION VALIDATION "

    ''' <summary>
    ''' Validates the implementation and specification visa versions against settings values.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Overrides Function ValidateFunctionalVisaVersions() As (Success As Boolean, Details As String)
        Return ResourceManager.ValidateFunctionalVisaVersions
    End Function

    ''' <summary> Validates the visa assembly versions against settings values. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Overrides Function ValidateVisaAssemblyVersions() As (Success As Boolean, Details As String)
        Return ResourceManager.ValidateVisaAssemblyVersions
    End Function

#End Region

End Class

