﻿'---------------------------------------------------------------------------------------------------
' file:		.\UI\TtmForm.vb
'
' summary:	Ttm Windows Form
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

''' <summary> Form for viewing the ttm. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class TtmForm
    Inherits isr.Core.Forma.ConsoleForm

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets or sets the meter view. </summary>
    ''' <value> The meter view. </value>
    Private Property MeterView As Ttm.Forms.MeterView = Nothing

    ''' <summary> Gets or sets the visa session. </summary>
    ''' <value> The visa session. </value>
    Private Property VisaSession As VisaSessionBase = Nothing

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.VisaSession IsNot Nothing Then Me.VisaSession.Dispose() : Me.VisaSession = Nothing
        If Me.MeterView IsNot Nothing Then Me.MeterView.Dispose() : Me.MeterView = Nothing
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            If Me.VisaSession IsNot Nothing Then
                If Me.VisaSession.CandidateResourceNameValidated Then
                    My.Settings.K2600ResourceName = Me.VisaSession.ValidatedResourceName
                End If
                If Not String.IsNullOrWhiteSpace(Me.VisaSession.OpenResourceTitle) Then
                    My.Settings.K2600ResourceTitle = Me.VisaSession.OpenResourceTitle
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding Meter View"
            ' the talker control publishes the device messages which thus get published to the form message box.
            Me.MeterView = New Ttm.Forms.MeterView
            Me.AddTalkerControl(My.Settings.K2600ResourceTitle, Me.MeterView, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.Application.Logger)
            Me.MeterView.ResourceName = My.Settings.K2600ResourceName
            Me.VisaSession = Me.MeterView.Device
            Me.VisaSession.CandidateResourceName = My.Settings.K2600ResourceName
            Me.VisaSession.CandidateResourceTitle = My.Settings.K2600ResourceTitle
            If Not String.IsNullOrWhiteSpace(Me.VisaSession.CandidateResourceName) Then
                activity = $"{Me.Name}; starting {Me.VisaSession.CandidateResourceName} selection task"
                Me.VisaSession.AsyncValidateResourceName(Me.VisaSession.CandidateResourceName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await the resource name validation task enabled. </summary>
    ''' <value> The await the resource name validation task enabled. </value>
    Protected Property AwaitResourceNameValidationTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.MeterView.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)
            If Me.VisaSession IsNot Nothing AndAlso Me.VisaSession.IsValidatingResourceName Then
                If Me.AwaitResourceNameValidationTaskEnabled Then
                    activity = $"{Me.Name}; awaiting {Me.VisaSession.CandidateResourceName} validation"
                    Me.VisaSession.AwaitResourceNameValidation(My.Settings.ResourceNameSelectionTimeout)
                Else
                    activity = $"{Me.Name}; validating {Me.VisaSession.CandidateResourceName}"
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.MeterView IsNot Nothing Then Me.MeterView.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub


#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
