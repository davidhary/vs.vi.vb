<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MeterUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._DataGridView = New System.Windows.Forms.DataGridView()
        Me._MeterView = New isr.VI.K2002.Forms.MeterView()
        CType(Me._DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_DataGridView
        '
        Me._DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._DataGridView.Location = New System.Drawing.Point(1, 136)
        Me._DataGridView.Name = "_DataGridView"
        Me._DataGridView.Size = New System.Drawing.Size(375, 216)
        Me._DataGridView.TabIndex = 23
        '
        '_MeterView
        '
        Me._MeterView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._MeterView.Dock = System.Windows.Forms.DockStyle.Top
        Me._MeterView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeterView.Location = New System.Drawing.Point(1, 1)
        Me._MeterView.Name = "_MeterView"
        Me._MeterView.Padding = New System.Windows.Forms.Padding(1)
        Me._MeterView.Size = New System.Drawing.Size(375, 135)
        Me._MeterView.TabIndex = 24
        '
        'MeterView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._DataGridView)
        Me.Controls.Add(Me._MeterView)
        Me.Name = "MeterView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(377, 353)
        CType(Me._DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _DataGridView As Windows.Forms.DataGridView
    Private WithEvents _MeterView As isr.VI.K2002.Forms.MeterView
End Class
