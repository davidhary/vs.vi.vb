## ISR TTM TM TSB Instructions: TSB Registration information. 
### Revision History

Opening the TTM Test Script Project

Opening the TSB Registering Files

Launching the TSB Registration Program

Obtaining the Registration Key

Validating the Registration Key

Saving the Registration Key

\(c\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

Licensed under the [ISR Fair End User Use License Version
*1.0](http://www.isr.cc/licenses/FairEndUserUseLicense.pdf).
Unless required by applicable law or agreed to in writing, this software
is provided \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.
