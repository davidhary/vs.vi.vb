﻿Imports System.Windows.Forms
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MeterView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._SelectorOpener = New isr.Core.Controls.SelectorOpener()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._PartPanel = New System.Windows.Forms.Panel()
        Me._SubmitButton = New System.Windows.Forms.Button()
        Me._OffsetValueNumericLabel = New System.Windows.Forms.Label()
        Me._OffsetValueNumeric = New System.Windows.Forms.NumericUpDown()
        Me._NominalValueNumericLabel = New System.Windows.Forms.Label()
        Me._NominalValueNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ParseButton = New System.Windows.Forms.Button()
        Me._PartNumberTextBox = New System.Windows.Forms.TextBox()
        Me._PartNumberTextBoxLabel = New System.Windows.Forms.Label()
        Me._SelectorLabel = New System.Windows.Forms.Label()
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ResourceNameLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AutoConnectCheckBox = New isr.Core.Controls.ToolStripCheckBox()
        Me._NominalValueLabelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._NominalValueLabel = New System.Windows.Forms.ToolStripLabel()
        Me._NominalOffsetLabelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._NominalOffsetLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AssignedValueLabelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AssignedValueLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AutoStartCheckBox = New isr.Core.Controls.ToolStripCheckBox()
        Me._Layout.SuspendLayout()
        Me._PartPanel.SuspendLayout()
        CType(Me._OffsetValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._NominalValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._BottomToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ResourceSelectorConnector
        '
        Me._SelectorOpener.BackColor = System.Drawing.Color.Transparent
        Me._SelectorOpener.Dock = System.Windows.Forms.DockStyle.Top
        Me._SelectorOpener.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectorOpener.Location = New System.Drawing.Point(120, 41)
        Me._SelectorOpener.Margin = New System.Windows.Forms.Padding(0)
        Me._SelectorOpener.Name = "_ResourceSelectorConnector"
        Me._SelectorOpener.Size = New System.Drawing.Size(522, 29)
        Me._SelectorOpener.TabIndex = 0
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._SelectorOpener, 1, 2)
        Me._Layout.Controls.Add(Me._PartPanel, 1, 3)
        Me._Layout.Controls.Add(Me._SelectorLabel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 5
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.Size = New System.Drawing.Size(762, 160)
        Me._Layout.TabIndex = 1
        '
        '_PartPanel
        '
        Me._PartPanel.Controls.Add(Me._SubmitButton)
        Me._PartPanel.Controls.Add(Me._OffsetValueNumericLabel)
        Me._PartPanel.Controls.Add(Me._OffsetValueNumeric)
        Me._PartPanel.Controls.Add(Me._NominalValueNumericLabel)
        Me._PartPanel.Controls.Add(Me._NominalValueNumeric)
        Me._PartPanel.Controls.Add(Me._ParseButton)
        Me._PartPanel.Controls.Add(Me._PartNumberTextBox)
        Me._PartPanel.Controls.Add(Me._PartNumberTextBoxLabel)
        Me._PartPanel.Location = New System.Drawing.Point(123, 73)
        Me._PartPanel.Name = "_PartPanel"
        Me._PartPanel.Size = New System.Drawing.Size(516, 78)
        Me._PartPanel.TabIndex = 1
        '
        '_SubmitButton
        '
        Me._SubmitButton.Location = New System.Drawing.Point(434, 40)
        Me._SubmitButton.Name = "_SubmitButton"
        Me._SubmitButton.Size = New System.Drawing.Size(75, 28)
        Me._SubmitButton.TabIndex = 7
        Me._SubmitButton.Text = "&SUBMIT"
        Me._SubmitButton.UseVisualStyleBackColor = True
        '
        '_OffsetValueNumericLabel
        '
        Me._OffsetValueNumericLabel.AutoSize = True
        Me._OffsetValueNumericLabel.Location = New System.Drawing.Point(214, 46)
        Me._OffsetValueNumericLabel.Name = "_OffsetValueNumericLabel"
        Me._OffsetValueNumericLabel.Size = New System.Drawing.Size(46, 17)
        Me._OffsetValueNumericLabel.TabIndex = 6
        Me._OffsetValueNumericLabel.Text = "Offset:"
        '
        '_OffsetValueNumeric
        '
        Me._OffsetValueNumeric.DecimalPlaces = 4
        Me._OffsetValueNumeric.Location = New System.Drawing.Point(265, 42)
        Me._OffsetValueNumeric.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me._OffsetValueNumeric.Name = "_OffsetValueNumeric"
        Me._OffsetValueNumeric.Size = New System.Drawing.Size(120, 25)
        Me._OffsetValueNumeric.TabIndex = 5
        '
        '_NominalValueNumericLabel
        '
        Me._NominalValueNumericLabel.AutoSize = True
        Me._NominalValueNumericLabel.Location = New System.Drawing.Point(6, 46)
        Me._NominalValueNumericLabel.Name = "_NominalValueNumericLabel"
        Me._NominalValueNumericLabel.Size = New System.Drawing.Size(60, 17)
        Me._NominalValueNumericLabel.TabIndex = 4
        Me._NominalValueNumericLabel.Text = "Nominal:"
        '
        '_NominalValueNumeric
        '
        Me._NominalValueNumeric.DecimalPlaces = 3
        Me._NominalValueNumeric.Location = New System.Drawing.Point(69, 42)
        Me._NominalValueNumeric.Maximum = New Decimal(New Integer() {1000000000, 0, 0, 0})
        Me._NominalValueNumeric.Name = "_NominalValueNumeric"
        Me._NominalValueNumeric.Size = New System.Drawing.Size(120, 25)
        Me._NominalValueNumeric.TabIndex = 3
        '
        '_ParseButton
        '
        Me._ParseButton.Location = New System.Drawing.Point(435, 6)
        Me._ParseButton.Name = "_ParseButton"
        Me._ParseButton.Size = New System.Drawing.Size(75, 28)
        Me._ParseButton.TabIndex = 2
        Me._ParseButton.Text = "&PARSE"
        Me._ParseButton.UseVisualStyleBackColor = True
        '
        '_PartNumberTextBox
        '
        Me._PartNumberTextBox.Location = New System.Drawing.Point(69, 8)
        Me._PartNumberTextBox.Name = "_PartNumberTextBox"
        Me._PartNumberTextBox.Size = New System.Drawing.Size(363, 25)
        Me._PartNumberTextBox.TabIndex = 1
        '
        '_PartNumberTextBoxLabel
        '
        Me._PartNumberTextBoxLabel.AutoSize = True
        Me._PartNumberTextBoxLabel.Location = New System.Drawing.Point(32, 12)
        Me._PartNumberTextBoxLabel.Name = "_PartNumberTextBoxLabel"
        Me._PartNumberTextBoxLabel.Size = New System.Drawing.Size(34, 17)
        Me._PartNumberTextBoxLabel.TabIndex = 0
        Me._PartNumberTextBoxLabel.Text = "Part:"
        '
        '_SelectorLabel
        '
        Me._SelectorLabel.AutoSize = True
        Me._SelectorLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._SelectorLabel.Location = New System.Drawing.Point(123, 24)
        Me._SelectorLabel.Name = "_SelectorLabel"
        Me._SelectorLabel.Size = New System.Drawing.Size(516, 17)
        Me._SelectorLabel.TabIndex = 2
        Me._SelectorLabel.Text = "Instrument Resource Name Finder and Connector"
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ResourceNameLabel, Me._AutoConnectCheckBox, Me._NominalValueLabelLabel, Me._NominalValueLabel, Me._NominalOffsetLabelLabel, Me._NominalOffsetLabel, Me._AssignedValueLabelLabel, Me._AssignedValueLabel, Me._AutoStartCheckBox})
        Me._BottomToolStrip.Location = New System.Drawing.Point(0, 160)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(762, 25)
        Me._BottomToolStrip.TabIndex = 2
        Me._BottomToolStrip.Text = "ToolStrip1"
        '
        '_ResourceNameLabel
        '
        Me._ResourceNameLabel.Name = "_ResourceNameLabel"
        Me._ResourceNameLabel.Size = New System.Drawing.Size(101, 22)
        Me._ResourceNameLabel.Text = "<resource name>"
        '
        '_AutoConnectCheckBox
        '
        Me._AutoConnectCheckBox.Checked = False
        Me._AutoConnectCheckBox.Margin = New System.Windows.Forms.Padding(0, 1, 6, 2)
        Me._AutoConnectCheckBox.Name = "_AutoConnectCheckBox"
        Me._AutoConnectCheckBox.Size = New System.Drawing.Size(100, 22)
        Me._AutoConnectCheckBox.Text = "Auto Connect"
        '
        '_NominalValueLabelLabel
        '
        Me._NominalValueLabelLabel.Name = "_NominalValueLabelLabel"
        Me._NominalValueLabelLabel.Size = New System.Drawing.Size(56, 22)
        Me._NominalValueLabelLabel.Text = "Nominal:"
        '
        '_NominalValueLabel
        '
        Me._NominalValueLabel.Name = "_NominalValueLabel"
        Me._NominalValueLabel.Size = New System.Drawing.Size(29, 22)
        Me._NominalValueLabel.Text = "<0>"
        Me._NominalValueLabel.ToolTipText = "Part nominal value"
        '
        '_NominalOffsetLabelLabel
        '
        Me._NominalOffsetLabelLabel.Name = "_NominalOffsetLabelLabel"
        Me._NominalOffsetLabelLabel.Size = New System.Drawing.Size(15, 22)
        Me._NominalOffsetLabelLabel.Text = "+"
        '
        '_NominalOffsetLabel
        '
        Me._NominalOffsetLabel.Name = "_NominalOffsetLabel"
        Me._NominalOffsetLabel.Size = New System.Drawing.Size(29, 22)
        Me._NominalOffsetLabel.Text = "<0>"
        Me._NominalOffsetLabel.ToolTipText = "Offset value"
        '
        '_AssignedValueLabelLabel
        '
        Me._AssignedValueLabelLabel.Name = "_AssignedValueLabelLabel"
        Me._AssignedValueLabelLabel.Size = New System.Drawing.Size(15, 22)
        Me._AssignedValueLabelLabel.Text = "="
        '
        '_AssignedValueLabel
        '
        Me._AssignedValueLabel.Name = "_AssignedValueLabel"
        Me._AssignedValueLabel.Size = New System.Drawing.Size(29, 22)
        Me._AssignedValueLabel.Text = "<0>"
        Me._AssignedValueLabel.ToolTipText = "Assigned nominal value"
        '
        '_AutoStartCheckBox
        '
        Me._AutoStartCheckBox.Checked = False
        Me._AutoStartCheckBox.Name = "_AutoStartCheckBox"
        Me._AutoStartCheckBox.Size = New System.Drawing.Size(79, 22)
        Me._AutoStartCheckBox.Text = "Auto Start"
        Me._AutoStartCheckBox.ToolTipText = "When checked, the instrument is configured upon entering the part number"
        '
        'MeterControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Controls.Add(Me._BottomToolStrip)
        Me.Name = "MeterControl"
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(762, 185)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._PartPanel.ResumeLayout(False)
        Me._PartPanel.PerformLayout()

        CType(Me._OffsetValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._NominalValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _SelectorOpener As isr.Core.Controls.SelectorOpener
    Private WithEvents _Layout As TableLayoutPanel
    Private WithEvents _PartPanel As Panel
    Private WithEvents _SubmitButton As Button
    Private WithEvents _OffsetValueNumericLabel As Label
    Private WithEvents _OffsetValueNumeric As NumericUpDown
    Private WithEvents _NominalValueNumericLabel As Label
    Private WithEvents _NominalValueNumeric As NumericUpDown
    Private WithEvents _ParseButton As Button
    Private WithEvents _PartNumberTextBox As TextBox
    Private WithEvents _PartNumberTextBoxLabel As Label
    Private WithEvents _SelectorLabel As Label
    Private WithEvents _BottomToolStrip As ToolStrip
    Private WithEvents _ResourceNameLabel As ToolStripLabel
    Private WithEvents _AutoConnectCheckBox As isr.Core.Controls.ToolStripCheckBox
    Private WithEvents _NominalValueLabelLabel As ToolStripLabel
    Private WithEvents _NominalValueLabel As ToolStripLabel
    Private WithEvents _NominalOffsetLabelLabel As ToolStripLabel
    Private WithEvents _NominalOffsetLabel As ToolStripLabel
    Private WithEvents _AssignedValueLabelLabel As ToolStripLabel
    Private WithEvents _AssignedValueLabel As ToolStripLabel
    Private WithEvents _AutoStartCheckBox As isr.Core.Controls.ToolStripCheckBox
End Class
