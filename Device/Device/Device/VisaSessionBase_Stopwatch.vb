'---------------------------------------------------------------------------------------------------
' file:		.\Device\VisaSessionBase_Stopwatch.vb
'
' summary:	Visa session base stopwatch class
'---------------------------------------------------------------------------------------------------

Partial Public Class VisaSessionBase

    ''' <summary> Gets or sets the elapsed time stop watch. </summary>
    ''' <value> The elapsed time stop watch. </value>
    Private ReadOnly Property ElapsedTimeStopwatch As Stopwatch

    ''' <summary> Reads elapsed time. </summary>
    ''' <returns> The elapsed time. </returns>
    Public Function ReadElapsedTime() As TimeSpan
        Dim result As TimeSpan = Me.ElapsedTimeStopwatch.Elapsed
        Me.ElapsedTimeStopwatch.Stop()
        Return result
    End Function

    ''' <summary> Reads elapsed time. </summary>
    ''' <param name="stopRequested"> True if stop requested. </param>
    ''' <returns> The elapsed time. </returns>
    Public Function ReadElapsedTime(ByVal stopRequested As Boolean) As TimeSpan
        If stopRequested AndAlso Me.ElapsedTimeStopwatch.IsRunning Then
            Me._ElapsedTimeCount -= 1
            If Me.ElapsedTimeCount <= 0 Then Me.ElapsedTimeStopwatch.Stop()
        End If
        Return Me.ElapsedTimeStopwatch.Elapsed
    End Function

    ''' <summary> The elapsed time. </summary>
    Private _ElapsedTime As TimeSpan

    ''' <summary> Gets or sets the elapsed time. </summary>
    ''' <value> The elapsed time. </value>
    Public Property ElapsedTime As TimeSpan
        Get
            Return Me._ElapsedTime
        End Get
        Set(value As TimeSpan)
            If value <> Me.ElapsedTime Then
                Me._ElapsedTime = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(VisaSessionBase.ElapsedTimeCaption))
            End If
        End Set
    End Property

    ''' <summary> The elapsed time format. </summary>
    Private _ElapsedTimeFormat As String = "s\.ffff"

    ''' <summary> Gets or sets the elapsed time format. </summary>
    ''' <value> The elapsed time. </value>
    Public Property ElapsedTimeFormat As String
        Get
            Return Me._ElapsedTimeFormat
        End Get
        Set(value As String)
            If value <> Me.ElapsedTimeFormat Then
                Me._ElapsedTimeFormat = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(VisaSessionBase.ElapsedTimeCaption))
            End If
        End Set
    End Property

    ''' <summary> Gets the elapsed time caption. </summary>
    ''' <value> The elapsed time caption. </value>
    Public ReadOnly Property ElapsedTimeCaption As String
        Get
            Return Me._ElapsedTime.ToString(Me.ElapsedTimeFormat)
        End Get
    End Property

    ''' <summary> Number of elapsed times. </summary>
    Private _ElapsedTimeCount As Integer

    ''' <summary>
    ''' Gets or sets the number of elapsed times. Some actions require two cycles to get the full
    ''' elapsed time.
    ''' </summary>
    ''' <value> The number of elapsed times. </value>
    Public Property ElapsedTimeCount As Integer
        Get
            Return Me._ElapsedTimeCount
        End Get
        Set(value As Integer)
            If value <> Me.ElapsedTimeCount Then
                Me._ElapsedTimeCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Starts elapsed stopwatch. </summary>
    Public Sub StartElapsedStopwatch()
        Me.StartElapsedStopwatch(0)
    End Sub

    ''' <summary> Starts elapsed stopwatch. </summary>
    ''' <param name="count"> Number of. </param>
    Public Sub StartElapsedStopwatch(ByVal count As Integer)
        Me._ElapsedTimeCount = count
        Me.ElapsedTimeStopwatch.Restart()
    End Sub

End Class
