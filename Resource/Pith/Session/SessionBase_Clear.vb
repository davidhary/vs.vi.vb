'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase_Clear.vb
'
' summary:	Session base clear class
'---------------------------------------------------------------------------------------------------
Partial Public Class SessionBase

#Region " CLS "

    ''' <summary> The clear refractory period. </summary>
    Private _ClearRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the clear refractory period. </summary>
    ''' <value> The clear refractory period. </value>
    Public Property ClearRefractoryPeriod As TimeSpan
        Get
            Return Me._ClearRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.ClearRefractoryPeriod) Then
                Me._ClearRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the clear execution state command. </summary>
    ''' <remarks>
    ''' SCPI: "*CLS".
    ''' <see cref="VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand"> </see>
    ''' </remarks>
    ''' <value> The clear execution state command. </value>
    Public Property ClearExecutionStateCommand As String = VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand

    ''' <summary> Returns <c>True</c> if the instrument supports clear execution state. </summary>
    ''' <value> <c>True</c> if the instrument supports clear execution state. </value>
    Public ReadOnly Property SupportsClearExecutionState As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.ClearExecutionStateCommand)
        End Get
    End Property

    ''' <summary> Sets the known clear state. </summary>
    ''' <remarks> Sends the '*CLS' message. '*CLS' clears the error queue. </remarks>
    Public Overridable Sub ClearExecutionState()
        ' !@# testing using OPC in place of the refractory periods.
        Me.OperationCompleted = New Boolean?
        If Me.IsSessionOpen Then
            If Me.SupportsClearExecutionState Then
                If Me.SupportsOperationComplete Then
                    Me.QueryOperationCompleted()
                Else
                    isr.Core.ApplianceBase.DoEventsWait(Me.StatusReadDelay)
                    ' was before clear; should be after?
                    'isr.Core.ApplianceBase.DoEvents()
                    'isr.Core.ApplianceBase.DoEventsWait(Me.ClearRefractoryPeriod)
                End If
                Me.Execute(Me.ClearExecutionStateCommand)
            End If
            If Me.SupportsOperationComplete Then
                Me.QueryOperationCompleted()
            Else
                ' was before clear; should be after?
                isr.Core.ApplianceBase.DoEventsWait(Me.ClearRefractoryPeriod)
                Me.OperationCompleted = True
            End If
            isr.Core.ApplianceBase.DoEventsWait(Me.StatusReadDelay)
            Me.ReadStatusRegister()
        End If
    End Sub

#End Region

#Region " RST "

    ''' <summary> The reset refractory period. </summary>
    Private _ResetRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the reset refractory period. </summary>
    ''' <value> The reset refractory period. </value>
    Public Property ResetRefractoryPeriod As TimeSpan
        Get
            Return Me._ResetRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.ResetRefractoryPeriod) Then
                Me._ResetRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the reset known state command. </summary>
    ''' <remarks>
    ''' SCPI: "*RST".
    ''' <see cref="VI.Pith.Ieee488.Syntax.ResetKnownStateCommand"> </see>
    ''' </remarks>
    ''' <value> The reset known state command. </value>
    Public Property ResetKnownStateCommand As String = VI.Pith.Ieee488.Syntax.ResetKnownStateCommand

    ''' <summary> Returns <c>True</c> if the instrument supports reset known state. </summary>
    ''' <value> <c>True</c> if the instrument supports reset known state. </value>
    Public ReadOnly Property SupportsResetKnownStateState As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.ResetKnownStateCommand)
        End Get
    End Property


    ''' <summary>
    ''' Sends a <see cref="ResetKnownStateCommand">reset (RST)</see> command to the instrument to set
    ''' the known reset (default) state.
    ''' </summary>
    ''' <remarks> Sends the '*RST' message. </remarks>
    Public Sub ResetKnownState()
        ' !@# testing using OPC in place of the refractory periods.
        Me.ResetRegistersKnownState()
        Me.OperationCompleted = New Boolean?
        If Me.IsSessionOpen Then
            If Me.SupportsResetKnownStateState Then
                If Me.SupportsOperationComplete Then
                    Me.QueryOperationCompleted()
                Else
                    isr.Core.ApplianceBase.DoEventsWait(Me.StatusReadDelay)
                End If
                Me.Execute(Me.ResetKnownStateCommand)
            End If
            If Me.SupportsOperationComplete Then
                Me.QueryOperationCompleted()
            Else
                isr.Core.ApplianceBase.DoEventsWait(Me.ResetRefractoryPeriod)
                Me.OperationCompleted = True
            End If
            isr.Core.ApplianceBase.DoEventsWait(Me.StatusReadDelay)
            Me.ReadStatusRegister()
        End If
    End Sub

#End Region

#Region " STATUS AND READ DELAYS "


    ''' <summary>
    ''' Delays operations by waiting for a task to complete. The delay is preceded and followed with
    ''' <see cref="isr.Core.ApplianceBase.DoEvents"/>
    ''' to release messages currently in the message queue.
    ''' </summary>
    ''' <returns> A TimeSpan. </returns>
    Public Function DoEventsReadDelay() As TimeSpan
        Return isr.Core.ApplianceBase.DoEventsWaitElapsed(Me.ReadDelay)
    End Function

    ''' <summary> The read delay. </summary>
    Private _ReadDelay As TimeSpan


    ''' <summary>
    ''' Gets or sets the to delay time to apply when reading immediately after a write.
    ''' </summary>
    ''' <value> The read delay. </value>
    Public Property ReadDelay As TimeSpan
        Get
            Return Me._ReadDelay
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me._ReadDelay) Then
                Me._ReadDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property


    ''' <summary>
    ''' Delays operations by waiting for a task to complete. The delay is preceded and followed with
    ''' <see cref="isr.Core.ApplianceBase.DoEvents"/>
    ''' to release messages currently in the message queue.
    ''' </summary>
    ''' <returns> A TimeSpan. </returns>
    Public Function DoEventsStatusReadDelay() As TimeSpan
        Return isr.Core.ApplianceBase.DoEventsWaitElapsed(Me.StatusReadDelay)
    End Function

    ''' <summary> The status read delay. </summary>
    Private _StatusReadDelay As TimeSpan


    ''' <summary>
    ''' Gets or sets the to delay time to apply when reading the status byte after a write.
    ''' </summary>
    ''' <value> The read delay. </value>
    Public Property StatusReadDelay As TimeSpan
        Get
            Return Me._StatusReadDelay
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me._StatusReadDelay) Then
                Me._StatusReadDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
