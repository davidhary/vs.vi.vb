''' <summary> Defines a Channel Subsystem for a Keithley 3700 instrument. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-13 </para>
''' </remarks>
Public Class ChannelSubsystem
    Inherits isr.VI.ChannelSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub
#End Region

#Region " COMMAND SYNTAX "

#Region " CHANNELS "

    ''' <summary> Gets or sets the closed channels query command. </summary>
    ''' <value> The closed channels query command. </value>
    Protected Overrides Property ClosedChannelsQueryCommand As String = "_G.print(_G.channel.getclose('allslots'))"

    ''' <summary> Gets or sets the closed channels command format. </summary>
    ''' <value> The closed channels command format. </value>
    Protected Overrides Property ClosedChannelsCommandFormat As String = "_G.channel.close('{0}')"

    ''' <summary> Gets or sets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    Protected Overrides Property OpenChannelsCommandFormat As String = "_G.channel.open('{0}')"

    ''' <summary> Gets or sets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    Protected Overrides Property OpenChannelsCommand As String = "_G.channel.open('allslots')"

#End Region

#End Region

End Class
