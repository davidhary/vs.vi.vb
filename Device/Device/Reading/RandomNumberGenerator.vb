''' <summary>
''' Provides a simulated value for testing measurements without having the benefit of instruments.
''' </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-15, 1.0.1841.x. </para>
''' </remarks>
Public Class RandomNumberGenerator

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.Generator = New Random(CInt(DateTimeOffset.Now.Ticks Mod Integer.MaxValue))
        Me.Min = 0
        Me.Max = 1
    End Sub

    ''' <summary> Holds a shared reference to the number generator. </summary>
    ''' <value> The generator. </value>
    Private Property Generator As Random

    ''' <summary> Gets the range. </summary>
    ''' <value> The range. </value>
    Private ReadOnly Property Range As Double
        Get
            Return Me._Max - Me._Min
        End Get
    End Property

    ''' <summary> Gets the minimum. </summary>
    ''' <value> The minimum value. </value>
    Public Property Min() As Double

    ''' <summary> Gets the maximum. </summary>
    ''' <value> The maximum value. </value>
    Public Property Max() As Double

    ''' <summary> Returns a simulated value. </summary>
    ''' <value> The value. </value>
    Public ReadOnly Property [Value]() As Double
        Get
            Return Me.generator.NextDouble * Me.range + Me.Min
        End Get
    End Property

End Class

