''' <summary> Defines a SCPI Sense Subsystem for a Keithley 7500 Meter. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class SenseSubsystem
    Inherits VI.SenseSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefineFunctionModeReadWrites("{0}", "'{0}'")
        Me.SupportedFunctionModes = VI.SenseFunctionModes.CurrentDC Or
                                    VI.SenseFunctionModes.VoltageDC Or
                                    VI.SenseFunctionModes.Resistance Or
                                    VI.SenseFunctionModes.ResistanceFourWire
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()

        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.Voltage).SetRange(0, 1000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.VoltageAC).SetRange(0, 1000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.VoltageDC).SetRange(0, 1000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.Current).SetRange(0, 10)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.CurrentAC).SetRange(0, 10)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.CurrentDC).SetRange(0, 10)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.ResistanceFourWire).SetRange(0, 2000000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.Resistance).SetRange(0, 1000000000D)

        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.Voltage) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.VoltageAC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.VoltageDC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.Current) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.CurrentAC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.CurrentDC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.ResistanceFourWire) = 0
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.Resistance) = 0
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.FunctionMode = VI.SenseFunctionModes.VoltageDC
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:CURR:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:CURR:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:CURR:NPLC?"

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overrides Property ProtectionLevelCommandFormat As String = ":SENS:CURR:PROT {0}"

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overrides Property ProtectionLevelQueryCommand As String = ":SENS:CURR:PROT?"

#End Region

#Region " LATEST DATA "

    ''' <summary> Gets or sets the latest data query command. </summary>
    ''' <remarks> Not exactly the same as reading the latest data. </remarks>
    ''' <value> The latest data query command. </value>
    Protected Overrides Property LatestDataQueryCommand As String = ":READ?"

#End Region

#End Region

#Region " RESISTANCE RANGE MODE "

    ''' <summary> The resistance range current. </summary>
    Private _ResistanceRangeCurrent As Double?

    ''' <summary> Gets or sets the resistance range current. </summary>
    ''' <value> The resistance range current. </value>
    Public Property ResistanceRangeCurrent As Double?
        Get
            Return Me._ResistanceRangeCurrent
        End Get
        Set(value As Double?)
            If Not Nullable.Equals(value, Me.ResistanceRangeCurrent) Then
                Me._ResistanceRangeCurrent = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The resistance range. </summary>
    Private _ResistanceRange As Double?

    ''' <summary> Gets or sets the resistance range. </summary>
    ''' <value> The resistance range. </value>
    Public Property ResistanceRange As Double?
        Get
            Return Me._ResistanceRange
        End Get
        Set(value As Double?)
            If Not Nullable.Equals(value, Me.ResistanceRange) Then
                Me._ResistanceRange = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
