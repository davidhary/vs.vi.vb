''' <summary> Dictionary of bitmasks. </summary>
''' <remarks>
''' David, 2019-12-21 <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
<Serializable>
Public Class BitmasksDictionary
    Inherits Dictionary(Of Integer, Integer)

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Adds a bitmask. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="key">     The bit mask key. </param>
    ''' <param name="bitmask"> The bitmask. </param>
    Public Shadows Sub Add(key As Integer, ByVal bitmask As Integer)
        If Me.ContainsValue(bitmask) Then Throw New ArgumentException($"Bitmask {bitmask} already exists; bitmasks must be unique", NameOf(bitmask))
        MyBase.Add(key, bitmask)
    End Sub

    ''' <summary> Adds a bitmask. </summary>
    ''' <param name="key">            The bit mask key. </param>
    ''' <param name="bitmask">        The bitmask. </param>
    ''' <param name="excludeFromAll"> True to exclude, false to include from all. </param>
    Public Shadows Sub Add(ByVal key As Integer, ByVal bitmask As Integer, ByVal excludeFromAll As Boolean)
        Me.Add(key, bitmask)
        If Not excludeFromAll Then Me._All = Me._All Or bitmask
    End Sub

    ''' <summary>
    ''' Gets or sets the value include all bitmask key values except those excluded .
    ''' </summary>
    ''' <value> all. </value>
    Public ReadOnly Property All As Integer

    ''' <summary> Query if 'status' bas any bit on. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> <c>true</c> if any bit on; otherwise <c>false</c> </returns>
    Public Function IsAnyBitOn(ByVal status As Integer, ByVal key As Integer) As Boolean
        If Me.ContainsKey(key) Then
            Return 0 <> (status And Me(key))
        Else
            Throw New ArgumentException($"Bitmask not found for {key}", NameOf(key))
        End If
    End Function

    ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> A Boolean. </returns>
    Public Function AreAllBitsOn(ByVal status As Integer, ByVal key As Integer) As Boolean
        If Me.ContainsKey(key) Then
            Return key = (status And key)
        Else
            Throw New ArgumentException($"Bitmask not found for {key}", NameOf(key))
        End If
    End Function

    ''' <summary> Return the masked status value. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> An Integer? </returns>
    Public Function MaskedValue(ByVal status As Integer, ByVal key As Integer) As Integer
        If Me.ContainsKey(key) Then
            Return status And key
        Else
            Throw New ArgumentException($"Bitmask not found for {key}", NameOf(key))
        End If
    End Function

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As Integer

    ''' <summary> Query if 'status' bas any bit on. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> <c>true</c> if any bit on; otherwise <c>false</c> </returns>
    Public Function IsAnyBitOn(ByVal key As Integer) As Boolean
        Return Me.IsAnyBitOn(Me.Status, key)
    End Function

    ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> A Boolean. </returns>
    Public Function AreAllBitsOn(ByVal key As Integer) As Boolean
        Return Me.AreAllBitsOn(Me.Status, key)
    End Function

    ''' <summary> Return the masked status value. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> An Integer? </returns>
    Public Function MaskedValue(ByVal key As Integer) As Integer
        Return Me.MaskedValue(Me.Status, key)
    End Function

#Region " SERIALIZATION "

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then Return
        Me.Status = info.GetInt32(NameOf(BitmasksDictionary.Status))
        Me.All = info.GetInt32(NameOf(BitmasksDictionary.All))
    End Sub

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData" /> method to serialize custom values.
    ''' </summary>
    ''' <param name="info">    The <see cref="Runtime.Serialization.SerializationInfo">serialization
    '''                        information</see>. </param>
    ''' <param name="context"> The <see cref="Runtime.Serialization.StreamingContext">streaming
    '''                        context</see> for the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        If info Is Nothing Then Return
        info.AddValue(NameOf(BitmasksDictionary.All), Me.All, GetType(Int32))
        info.AddValue(NameOf(BitmasksDictionary.Status), Me.Status, GetType(Int32))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class
