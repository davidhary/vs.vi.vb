Imports isr.Core
Imports isr.Core.TimeSpanExtensions
''' <summary> Defines the contract that must be implemented by a Trigger Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class TriggerSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TriggerSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefineTriggerSourceReadWrites()
        Me.DefineTriggerLayerBypassReadWrites()
        Me.DefineTriggerEventReadWrites()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.AutoDelayEnabled = False
        Me.TriggerCount = 1
        Me.Delay = TimeSpan.Zero
        Me.InputLineNumber = 1
        Me.OutputLineNumber = 2
        Me.TimerInterval = TimeSpan.FromSeconds(0.1)
        Me.ContinuousEnabled = False
        Me.TriggerState = VI.TriggerState.None
        Me.MaximumTriggerCount = 99999
        Me.MaximumDelay = TimeSpan.FromSeconds(999999.999)
        Me.ResetScpiKnownState()
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Private Sub ResetScpiKnownState()
        Me.AutoDelayEnabled = False
        Me.TriggerCount = 1
        Me.Delay = TimeSpan.Zero
        Me.TriggerLayerBypassMode = VI.TriggerLayerBypassModes.Acceptor
        Me.InputLineNumber = 1
        Me.OutputLineNumber = 2
        Me.TriggerSource = VI.TriggerSources.Immediate
        Me.TimerInterval = TimeSpan.FromSeconds(0.1)
        Me.SupportedTriggerSources = TriggerSources.Bus Or TriggerSources.External Or TriggerSources.Immediate
        Me.ContinuousEnabled = False
        Me.TriggerState = VI.TriggerState.None
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Abort command. </summary>
    ''' <remarks> SCPI: ":ABOR". </remarks>
    ''' <value> The Abort command. </value>
    Protected Overridable Property AbortCommand As String

    ''' <summary> Aborts operations. </summary>
    ''' <remarks>
    ''' When this action command is sent, the SourceMeter aborts operation and returns to the idle
    ''' state. A faster way to return to idle is to use the DCL or SDC command. With auto output- off
    ''' enabled (:SOURce1:CLEar:AUTO ON), the output will remain on if operation is terminated before
    ''' the output has a chance to automatically turn off.
    ''' </remarks>
    Public Sub Abort()
        If Not String.IsNullOrWhiteSpace(Me.AbortCommand) Then
            Me.Session.WriteLine(Me.AbortCommand)
        End If
    End Sub

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <remarks> SCPI: ":TRIG:CLE". </remarks>
    ''' <value> The clear command. </value>
    Protected Overridable Property ClearCommand As String

    ''' <summary> Clears the triggers. </summary>
    Public Sub ClearTriggers()
        Me.Write(Me.ClearCommand)
    End Sub

    ''' <summary> Gets or sets the clear  trigger model command. </summary>
    ''' <remarks> SCPI: ":TRIG:LOAD 'EMPTY'". </remarks>
    ''' <value> The clear command. </value>
    Protected Overridable Property ClearTriggerModelCommand As String

    ''' <summary> Clears the trigger model. </summary>
    Public Sub ClearTriggerModel()
        Me.Write(Me.ClearTriggerModelCommand)
    End Sub

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <remarks> SCPI: ":INIT". </remarks>
    ''' <value> The initiate command. </value>
    Protected Overridable Property InitiateCommand As String

    ''' <summary> Initiates operations. </summary>
    ''' <remarks>
    ''' This command is used to initiate source-measure operation by taking the SourceMeter out of
    ''' idle. The :READ? and :MEASure? commands also perform an initiation. Note that if auto output-
    ''' off is disabled (SOURce1:CLEar:AUTO OFF), the source output must first be turned on before an
    ''' initiation can be performed. The :MEASure? command automatically turns the output source on
    ''' before performing the initiation.
    ''' </remarks>
    Public Sub Initiate()
        Me.Write(Me.InitiateCommand)
    End Sub

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <remarks> SCPI: ":TRIG:IMM". </remarks>
    ''' <value> The Immediate command. </value>
    Protected Overridable Property ImmediateCommand As String

    ''' <summary> Immediately move tot he next layer. </summary>
    Public Sub Immediate()
        Me.Session.Execute(Me.ImmediateCommand)
    End Sub

#End Region

#Region " AUTO DELAY ENABLED "

    ''' <summary> The automatic delay enabled. </summary>
    Private _AutoDelayEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Delay Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Delay Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoDelayEnabled As Boolean?
        Get
            Return Me._AutoDelayEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoDelayEnabled, value) Then
                Me._AutoDelayEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Delay Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoDelayEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoDelayEnabled(value)
        Return Me.QueryAutoDelayEnabled()
    End Function

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <remarks> SCPI: ":TRIG:DEL:AUTO?". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property AutoDelayEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Enabled sentinel. Also sets the
    ''' <see cref="AutoDelayEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoDelayEnabled() As Boolean?
        Me.AutoDelayEnabled = Me.Query(Me.AutoDelayEnabled, Me.AutoDelayEnabledQueryCommand)
        Return Me.AutoDelayEnabled
    End Function

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <remarks> SCPI: ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property AutoDelayEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoDelayEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoDelayEnabled = Me.Write(value, Me.AutoDelayEnabledCommandFormat)
        Return Me.AutoDelayEnabled
    End Function

#End Region

#Region " AVERAGING ENABLED "

    ''' <summary> The averaging enabled. </summary>
    Private _AveragingEnabled As Boolean?

    ''' <summary> Gets or sets the cached Averaging Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Averaging Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AveragingEnabled As Boolean?
        Get
            Return Me._AveragingEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AveragingEnabled, value) Then
                Me._AveragingEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Averaging Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAveragingEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAveragingEnabled(value)
        Return Me.QueryAveragingEnabled()
    End Function

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <remarks> SCPI: ":TRIG:AVER?". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property AveragingEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Averaging Enabled sentinel. Also sets the
    ''' <see cref="AveragingEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAveragingEnabled() As Boolean?
        Me.AveragingEnabled = Me.Query(Me.AveragingEnabled, Me.AveragingEnabledQueryCommand)
        Return Me.AveragingEnabled
    End Function

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <remarks> SCPI: ":TRIG:AVER {0:1;1;0}". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property AveragingEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Averaging Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAveragingEnabled(ByVal value As Boolean) As Boolean?
        Me.AveragingEnabled = Me.Write(value, Me.AveragingEnabledCommandFormat)
        Return Me.AveragingEnabled
    End Function

#End Region

#Region " CONTINUOUS ENABLED "

    ''' <summary> The continuous enabled. </summary>
    Private _ContinuousEnabled As Boolean?

    ''' <summary> Gets or sets the cached Continuous Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Continuous Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property ContinuousEnabled As Boolean?
        Get
            Return Me._ContinuousEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ContinuousEnabled, value) Then
                Me._ContinuousEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Continuous Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyContinuousEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteContinuousEnabled(value)
        Return Me.QueryContinuousEnabled()
    End Function

    ''' <summary> Gets or sets the Continuous enabled query command. </summary>
    ''' <remarks> SCPI: ":INIT:CONT?". </remarks>
    ''' <value> The Continuous enabled query command. </value>
    Protected Overridable Property ContinuousEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Continuous Enabled sentinel. Also sets the
    ''' <see cref="ContinuousEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryContinuousEnabled() As Boolean?
        Me.ContinuousEnabled = Me.Query(Me.ContinuousEnabled, Me.ContinuousEnabledQueryCommand)
        Return Me.ContinuousEnabled
    End Function

    ''' <summary> Gets or sets the Continuous enabled command Format. </summary>
    ''' <remarks> SCPI: ":INIT:CONT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Continuous enabled query command. </value>
    Protected Overridable Property ContinuousEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Continuous Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteContinuousEnabled(ByVal value As Boolean) As Boolean?
        Me.ContinuousEnabled = Me.Write(value, Me.ContinuousEnabledCommandFormat)
        Return Me.ContinuousEnabled
    End Function

#End Region

#Region " TRIGGER COUNT "

    ''' <summary> Number of maximum triggers. </summary>
    Private _MaximumTriggerCount As Integer

    ''' <summary> Gets or sets the cached Maximum Trigger Count. </summary>
    ''' <remarks>
    ''' Specifies how many times an operation is performed in the specified layer of the Trigger
    ''' model.
    ''' </remarks>
    ''' <value> The Trigger MaximumTriggerCount or none if not set or unknown. </value>
    Public Property MaximumTriggerCount As Integer
        Get
            Return Me._MaximumTriggerCount
        End Get
        Protected Set(ByVal value As Integer)
            If Not Nullable.Equals(Me.MaximumTriggerCount, value) Then
                Me._MaximumTriggerCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Returns <c>True</c> if using an infinite trigger count. </summary>
    ''' <value> The number of is infinite triggers. </value>
    Public ReadOnly Property IsTriggerCountInfinite As Boolean?
        Get
            Return If(Me.TriggerCount.HasValue, Me.TriggerCount >= Integer.MaxValue, New Boolean?)
        End Get
    End Property

    ''' <summary> Number of triggers. </summary>
    Private _TriggerCount As Integer?

    ''' <summary> Gets or sets the cached Trigger Count. </summary>
    ''' <remarks>
    ''' Specifies how many times an operation is performed in the specified layer of the trigger
    ''' model.
    ''' </remarks>
    ''' <value> The trigger count or none if not set or unknown. </value>
    Public Overloads Property TriggerCount As Integer?
        Get
            Return Me._TriggerCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.TriggerCount, value) Then
                Me._TriggerCount = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(TriggerSubsystemBase.IsTriggerCountInfinite))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Count. </summary>
    ''' <param name="value"> The current Trigger Count. </param>
    ''' <returns> The TriggerCount or none if unknown. </returns>
    Public Function ApplyTriggerCount(ByVal value As Integer) As Integer?
        Me.WriteTriggerCount(value)
        Return Me.QueryTriggerCount()
    End Function

    ''' <summary> Gets or sets trigger count query command. </summary>
    ''' <remarks> SCPI: ":TRIG:COUN?". </remarks>
    ''' <value> The trigger count query command. </value>
    Protected Overridable Property TriggerCountQueryCommand As String

    ''' <summary> Queries the current Trigger Count. </summary>
    ''' <returns> The Trigger Count or none if unknown. </returns>
    Public Function QueryTriggerCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.TriggerCountQueryCommand) Then
            Dim candidateTriggerCount As Double = Me.Session.Query(0R, Me.TriggerCountQueryCommand)
            Me.TriggerCount = If(candidateTriggerCount < Integer.MaxValue, CInt(candidateTriggerCount), Integer.MaxValue)
        End If
        Return Me.TriggerCount
    End Function

    ''' <summary> Gets or sets trigger count command format. </summary>
    ''' <remarks> SCPI: ":TRIG:COUN {0}". </remarks>
    ''' <value> The trigger count command format. </value>
    Protected Overridable Property TriggerCountCommandFormat As String

    ''' <summary> Write the Trigger Count without reading back the value from the device. </summary>
    ''' <param name="value"> The current PointsTriggerCount. </param>
    ''' <returns> The Trigger Count or none if unknown. </returns>
    Public Function WriteTriggerCount(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.TriggerCountCommandFormat) Then
            If value = Integer.MaxValue Then
                Me.Session.WriteLine(Me.TriggerCountCommandFormat, "INF")
            Else
                Me.Session.WriteLine(Me.TriggerCountCommandFormat, value)
            End If
        End If
        Me.TriggerCount = value
        Return Me.TriggerCount
    End Function

#End Region

#Region " DELAY "

    ''' <summary> The maximum delay. </summary>
    Private _MaximumDelay As TimeSpan

    ''' <summary> Gets or sets the maximum delay. </summary>
    ''' <value> The maximum delay. </value>
    Public Property MaximumDelay As TimeSpan
        Get
            Return Me._MaximumDelay
        End Get
        Protected Set(ByVal value As TimeSpan)
            If Not Nullable.Equals(Me.MaximumDelay, value) Then
                Me._MaximumDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The delay. </summary>
    Private _Delay As TimeSpan?

    ''' <summary> Gets or sets the cached Trigger Delay. </summary>
    ''' <remarks>
    ''' The delay is used to delay operation in the trigger layer. After the programmed trigger event
    ''' occurs, the instrument waits until the delay period expires before performing the Device
    ''' Action.
    ''' </remarks>
    ''' <value> The Trigger Delay or none if not set or unknown. </value>
    Public Overloads Property Delay As TimeSpan?
        Get
            Return Me._Delay
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Nullable.Equals(Me.Delay, value) Then
                Me._Delay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Delay. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Trigger Delay or none if unknown. </returns>
    Public Function ApplyDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteDelay(value)
        Return Me.QueryDelay()
    End Function

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <remarks> SCPI: ":TRIG:DEL?". </remarks>
    ''' <value> The delay query command. </value>
    Protected Overridable Property DelayQueryCommand As String

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Delay query command. </value>
    Protected Overridable Property DelayFormat As String

    ''' <summary> Queries the Delay. </summary>
    ''' <returns> The Delay or none if unknown. </returns>
    Public Function QueryDelay() As TimeSpan?
        Me.Delay = Me.Query(Me.Delay, Me.DelayFormat, Me.DelayQueryCommand)
        Return Me.Delay
    End Function

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <remarks> SCPI: ":TRIG:DEL {0:s\.FFFFFFF}". </remarks>
    ''' <value> The delay command format. </value>
    Protected Overridable Property DelayCommandFormat As String

    ''' <summary> Writes the Trigger Delay without reading back the value from the device. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Trigger Delay or none if unknown. </returns>
    Public Function WriteDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.Delay = Me.Write(value, Me.DelayCommandFormat)
        Return Me.Delay
    End Function

#End Region

#Region " INPUT LINE NUMBER "

    ''' <summary> The input line number. </summary>
    Private _InputLineNumber As Integer?

    ''' <summary> Gets or sets the cached Trigger Input Line Number. </summary>
    ''' <value> The Trigger InputLineNumber or none if not set or unknown. </value>
    Public Overloads Property InputLineNumber As Integer?
        Get
            Return Me._InputLineNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.InputLineNumber, value) Then
                Me._InputLineNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Input Line Number. </summary>
    ''' <param name="value"> The current Input Line Number. </param>
    ''' <returns> The Input Line Number or none if unknown. </returns>
    Public Function ApplyInputLineNumber(ByVal value As Integer) As Integer?
        Me.WriteInputLineNumber(value)
        Return Me.QueryInputLineNumber()
    End Function

    ''' <summary> Gets or sets the Input Line Number query command. </summary>
    ''' <remarks>
    ''' SCPI: :TRIG:ILIN?
    ''' 2002: :TRIG:TCON:ASYN:ILIN?
    ''' </remarks>
    ''' <value> The Input Line Number query command. </value>
    Protected Overridable Property InputLineNumberQueryCommand As String

    ''' <summary> Queries the InputLineNumber. </summary>
    ''' <returns> The Input Line Number or none if unknown. </returns>
    Public Function QueryInputLineNumber() As Integer?
        Me.InputLineNumber = Me.Query(Me.InputLineNumber, Me.InputLineNumberQueryCommand)
        Return Me.InputLineNumber
    End Function

    ''' <summary> Gets or sets the Input Line Number command format. </summary>
    ''' <remarks>
    ''' SCPI: :TRIG:ILIN {0}
    ''' 2002: :TRIG:TCON:ASYN:ILIN {0}
    ''' </remarks>
    ''' <value> The Input Line Number command format. </value>
    Protected Overridable Property InputLineNumberCommandFormat As String

    ''' <summary>
    ''' Writes the Trigger Input Line Number without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current InputLineNumber. </param>
    ''' <returns> The Trigger Input Line Number or none if unknown. </returns>
    Public Function WriteInputLineNumber(ByVal value As Integer) As Integer?
        Me.InputLineNumber = Me.Write(value, Me.InputLineNumberCommandFormat)
        Return Me.InputLineNumber
    End Function

#End Region

#Region " OUTPUT LINE NUMBER "

    ''' <summary> The output line number. </summary>
    Private _OutputLineNumber As Integer?

    ''' <summary> Gets or sets the cached Trigger Output Line Number. </summary>
    ''' <value> The Trigger OutputLineNumber or none if not set or unknown. </value>
    Public Overloads Property OutputLineNumber As Integer?
        Get
            Return Me._OutputLineNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.OutputLineNumber, value) Then
                Me._OutputLineNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Output Line Number. </summary>
    ''' <param name="value"> The current Output Line Number. </param>
    ''' <returns> The Output Line Number or none if unknown. </returns>
    Public Function ApplyOutputLineNumber(ByVal value As Integer) As Integer?
        Me.WriteOutputLineNumber(value)
        Return Me.QueryOutputLineNumber()
    End Function

    ''' <summary> Gets or sets the Output Line Number query command. </summary>
    ''' <remarks>
    ''' SCPI: :TRIG:OLIN?
    ''' 2002: :TRIG:TCON:ASYN:OLIN?
    ''' </remarks>
    ''' <value> The Output Line Number query command. </value>
    Protected Overridable Property OutputLineNumberQueryCommand As String

    ''' <summary> Queries the OutputLineNumber. </summary>
    ''' <returns> The Output Line Number or none if unknown. </returns>
    Public Function QueryOutputLineNumber() As Integer?
        Me.OutputLineNumber = Me.Query(Me.OutputLineNumber, Me.OutputLineNumberQueryCommand)
        Return Me.OutputLineNumber
    End Function

    ''' <summary> Gets or sets the Output Line Number command format. </summary>
    ''' <remarks>
    ''' SCPI: :TRIG:OLIN {0}
    ''' 2002: :TRIG:TCON:ASYN:OLIN {0}
    ''' </remarks>
    ''' <value> The Output Line Number command format. </value>
    Protected Overridable Property OutputLineNumberCommandFormat As String

    ''' <summary>
    ''' Writes the Trigger Output Line Number without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current OutputLineNumber. </param>
    ''' <returns> The Trigger Output Line Number or none if unknown. </returns>
    Public Function WriteOutputLineNumber(ByVal value As Integer) As Integer?
        Me.OutputLineNumber = Me.Write(value, Me.OutputLineNumberCommandFormat)
        Return Me.OutputLineNumber
    End Function

#End Region

#Region " TIMER TIME SPAN "

    ''' <summary> The timer interval. </summary>
    Private _TimerInterval As TimeSpan?

    ''' <summary> Gets or sets the cached Trigger Timer Interval. </summary>
    ''' <remarks>
    ''' The Timer Interval is used to Timer Interval operation in the trigger layer. After the
    ''' programmed trigger event occurs, the instrument waits until the Timer Interval period expires
    ''' before performing the Device Action.
    ''' </remarks>
    ''' <value> The Trigger Timer Interval or none if not set or unknown. </value>
    Public Overloads Property TimerInterval As TimeSpan?
        Get
            Return Me._TimerInterval
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Me.TimerInterval.Equals(value) Then
                Me._TimerInterval = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Timer Interval. </summary>
    ''' <param name="value"> The current TimerTimeSpan. </param>
    ''' <returns> The Trigger Timer Interval or none if unknown. </returns>
    Public Function ApplyTimerTimeSpan(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteTimerTimeSpan(value)
        Return Me.QueryTimerTimeSpan()
    End Function

    ''' <summary> Gets the Timer Interval query command. </summary>
    ''' <remarks> SCPI: ":TRIG:TIM?". </remarks>
    ''' <value> The Timer Interval query command. </value>
    Protected Overridable Property TimerIntervalQueryCommand As String

    ''' <summary> Gets the Timer Interval format for converting the query to time span. </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Timer Interval query command. </value>
    Protected Overridable Property TimerIntervalFormat As String

    ''' <summary> Queries the Timer Interval. </summary>
    ''' <returns> The Timer Interval or none if unknown. </returns>
    Public Function QueryTimerTimeSpan() As TimeSpan?
        Me.TimerInterval = Me.Query(Me.TimerInterval, Me.TimerIntervalFormat, Me.TimerIntervalQueryCommand)
        Return Me.TimerInterval
    End Function

    ''' <summary> Gets the Timer Interval command format. </summary>
    ''' <remarks> SCPI: ":TRIG:TIM {0:s\.FFFFFFF}". </remarks>
    ''' <value> The query command format. </value>
    Protected Overridable Property TimerIntervalCommandFormat As String

    ''' <summary>
    ''' Writes the Trigger Timer Interval without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current TimerTimeSpan. </param>
    ''' <returns> The Trigger Timer Interval or none if unknown. </returns>
    Public Function WriteTimerTimeSpan(ByVal value As TimeSpan) As TimeSpan?
        Me.TimerInterval = Me.Write(value, Me.TimerIntervalCommandFormat)
    End Function

#End Region

#Region " TRIGGER STATE "

    ''' <summary> Monitor active trigger state. </summary>
    ''' <param name="pollPeriod"> The poll period. </param>
    Public Sub MonitorActiveTriggerState(ByVal pollPeriod As TimeSpan)
        Me.QueryTriggerState()
        Do While Me.IsTriggerStateActive
            pollPeriod.SpinWait()
            ApplianceBase.DoEvents()
            Me.QueryTriggerState()
        Loop
    End Sub

    ''' <summary> Gets the asynchronous task. </summary>
    ''' <value> The asynchronous task. </value>
    Public ReadOnly Property AsyncTask As Threading.Tasks.Task

    ''' <summary> Gets the action task. </summary>
    ''' <value> The action task. </value>
    Public ReadOnly Property ActionTask As Threading.Tasks.Task

    ''' <summary> Asynchronous monitor trigger state. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="pollPeriod"> The poll period. </param>
    ''' <returns> A Threading.Tasks.Task. </returns>
    Public Function AsyncMonitorTriggerState(ByVal pollPeriod As TimeSpan) As Threading.Tasks.Task
        Me._AsyncTask = Me.AsyncAwaitMonitorTriggerState(pollPeriod)
        Return Me.ActionTask
    End Function

    ''' <summary> Asynchronous await monitor trigger state. </summary>
    ''' <param name="pollPeriod"> The poll period. </param>
    ''' <returns> A Threading.Tasks.Task. </returns>
    Private Async Function AsyncAwaitMonitorTriggerState(ByVal pollPeriod As TimeSpan) As Threading.Tasks.Task
        Me._ActionTask = Threading.Tasks.Task.Run(Sub() Me.MonitorActiveTriggerState(pollPeriod))
        Await Me.ActionTask
    End Function

    ''' <summary> Gets the trigger state caption. </summary>
    ''' <value> The trigger state caption. </value>
    Public ReadOnly Property TriggerStateCaption As String
        Get
            Return If(Me.TriggerState.HasValue, Me.TriggerState.ToString, String.Empty)
        End Get
    End Property

    ''' <summary> State of the trigger. </summary>
    Private _TriggerState As TriggerState?

    ''' <summary> Gets or sets the cached State TriggerState. </summary>
    ''' <value>
    ''' The <see cref="TriggerState">State Trigger State</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property TriggerState As TriggerState?
        Get
            Return Me._TriggerState
        End Get
        Protected Set(ByVal value As TriggerState?)
            If Not Me.TriggerState.Equals(value) Then
                Me._TriggerState = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(TriggerSubsystemBase.TriggerStateCaption))
            End If
        End Set
    End Property

    ''' <summary> State of the trigger block. </summary>
    Private _TriggerBlockState As TriggerState?

    ''' <summary> Gets or sets the cached trigger block <see cref="TriggerState"/>. </summary>
    ''' <value>
    ''' The <see cref="TriggerBlockState">State Trigger State</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property TriggerBlockState As TriggerState?
        Get
            Return Me._TriggerBlockState
        End Get
        Protected Set(ByVal value As TriggerState?)
            If Not Me.TriggerBlockState.Equals(value) Then
                Me._TriggerBlockState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The trigger state block number. </summary>
    Private _TriggerStateBlockNumber As Integer?

    ''' <summary> Gets or sets the cached trigger state block number. </summary>
    ''' <value> The block number of the trigger state. </value>
    Public Overloads Property TriggerStateBlockNumber As Integer?
        Get
            Return Me._TriggerStateBlockNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Me.TriggerStateBlockNumber.Equals(value) Then
                Me._TriggerStateBlockNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Query if this object is trigger state done. </summary>
    ''' <returns> <c>true</c> if trigger state done; otherwise <c>false</c> </returns>
    Public Function IsTriggerStateDone() As Boolean
        Dim result As Boolean
        If Me.TriggerState.HasValue Then
            Dim value As TriggerState = Me.TriggerState.Value
            result = (value = VI.TriggerState.Aborted) OrElse
                     (value = VI.TriggerState.Failed) OrElse
                     (value = VI.TriggerState.Empty) OrElse
                     (value = VI.TriggerState.Idle) OrElse
                     (value = VI.TriggerState.None)
        Else
            Return False
        End If
        Return result
    End Function

    ''' <summary> Queries if a trigger state is active. </summary>
    ''' <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
    Public Function IsTriggerStateActive() As Boolean
        Dim result As Boolean
        If Me.TriggerState.HasValue Then
            Dim value As TriggerState = Me.TriggerState.Value
            result = (value = VI.TriggerState.Running) OrElse
                     (value = VI.TriggerState.Waiting)
        Else
            Return False
        End If
        Return result
    End Function

    ''' <summary> Queries if a trigger state is aborting. </summary>
    ''' <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
    Public Function IsTriggerStateAborting() As Boolean
        Dim result As Boolean
        If Me.TriggerState.HasValue Then
            Dim value As TriggerState = Me.TriggerState.Value
            result = (value = VI.TriggerState.Aborting)
        Else
            Return False
        End If
        Return result
    End Function

    ''' <summary> Queries if a trigger state is Failed. </summary>
    ''' <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
    Public Function IsTriggerStateFailed() As Boolean
        Dim result As Boolean

        If Me.TriggerState.HasValue Then
            Dim value As TriggerState = Me.TriggerState.Value
            result = (value = VI.TriggerState.Failed)
        Else
            Return False
        End If
        Return result
    End Function

    ''' <summary> Queries if a trigger state is Idle. </summary>
    ''' <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
    Public Function IsTriggerStateIdle() As Boolean
        Dim result As Boolean
        If Me.TriggerState.HasValue Then
            Dim value As TriggerState = Me.TriggerState.Value
            result = (value = VI.TriggerState.Idle)
        Else
            Return False
        End If
        Return result
    End Function

    ''' <summary> Gets the Trigger State query command. </summary>
    ''' <remarks>
    ''' <c>SCPI: :TRIG:STAT? <para>
    ''' TSP2: trigger.model.state()
    ''' </para></c>
    ''' </remarks>
    ''' <value> The Trigger State query command. </value>
    Protected Overridable Property TriggerStateQueryCommand As String

    ''' <summary> Queries the Trigger State. </summary>
    ''' <returns> The <see cref="TriggerState">Trigger State</see> or none if unknown. </returns>
    Public Overridable Function QueryTriggerState() As TriggerState?
        Dim currentValue As String = Me.TriggerState.ToString
        If String.IsNullOrEmpty(Me.Session.EmulatedReply) Then Me.Session.EmulatedReply = currentValue
        If Not String.IsNullOrWhiteSpace(Me.TriggerStateQueryCommand) Then
            currentValue = Me.Session.QueryTrimEnd(Me.TriggerStateQueryCommand)
        End If
        If String.IsNullOrWhiteSpace(currentValue) Then
            Me.TriggerState = New TriggerState?
        Else
            Dim values As New Queue(Of String)(currentValue.Split(";"c))
            If values.Any Then
                Me.TriggerState = CType([Enum].Parse(GetType(VI.TriggerState), values.Dequeue, True), VI.TriggerState)
            End If
            If values.Any Then
                Me.TriggerBlockState = CType([Enum].Parse(GetType(VI.TriggerState), values.Dequeue, True), VI.TriggerState)
            End If
            If values.Any Then
                Me.TriggerStateBlockNumber = Integer.Parse(values.Dequeue)
            End If
        End If
        Return Me.TriggerState
    End Function

    ''' <summary> Gets the state of the supports trigger. </summary>
    ''' <value> The supports trigger state. </value>
    Public ReadOnly Property SupportsTriggerState As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.TriggerStateQueryCommand)
        End Get
    End Property

#End Region

End Class

''' <summary> Values that represent trigger status. </summary>
Public Enum TriggerState

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Idle (trigger.STATE_NONE)")>
    None

    ''' <summary> An enum constant representing the idle option. </summary>
    <ComponentModel.Description("Idle (trigger.STATE_IDLE)")>
    Idle

    ''' <summary> An enum constant representing the running option. </summary>
    <ComponentModel.Description("Running (trigger.STATE_RUNNING)")>
    Running

    ''' <summary> An enum constant representing the waiting option. </summary>
    <ComponentModel.Description("Waiting (trigger.STATE_WAITING)")>
    Waiting

    ''' <summary> An enum constant representing the empty option. </summary>
    <ComponentModel.Description("Empty (trigger.STATE_EMPTY)")>
    Empty

    ''' <summary> An enum constant representing the building option. </summary>
    <ComponentModel.Description("Building (trigger.STATE_BUILDING)")>
    Building

    ''' <summary> An enum constant representing the failed option. </summary>
    <ComponentModel.Description("Failed (trigger.STATE_FAILED)")>
    Failed

    ''' <summary> An enum constant representing the aborting option. </summary>
    <ComponentModel.Description("Aborting (trigger.STATE_ABORTING)")>
    Aborting

    ''' <summary> An enum constant representing the aborted option. </summary>
    <ComponentModel.Description("Aborted (trigger.STATE_ABORTED)")>
    Aborted
End Enum

