'---------------------------------------------------------------------------------------------------
' file:		.VI\Subsystems\StatusSubsystemBase_DeviceErrors.vb
'
' summary:	Status subsystem base device errors class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions


Partial Public MustInherit Class StatusSubsystemBase

#Region " DEVICE ERRORS: CLEAR "

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <remarks>
    ''' SCPI: ":STAT:QUE:CLEAR".
    ''' <see cref="VI.Pith.Scpi.Syntax.ClearErrorQueueCommand"> </see>
    ''' </remarks>
    ''' <value> The clear error queue command. </value>
    Protected Overridable Property ClearErrorQueueCommand As String = VI.Pith.Scpi.Syntax.ClearSystemErrorQueueCommand

    ''' <summary> Clears messages from the error queue. </summary>
    ''' <remarks>
    ''' Sends the <see cref="ClearErrorQueueCommand">clear error queue</see> message.
    ''' </remarks>
    Public Overridable Sub ClearErrorQueue()
        Me.ClearErrorCache()
        Me.Session.Execute(Me.ClearErrorQueueCommand)
        Me.ErrorQueueCount = New Integer?
    End Sub

    ''' <summary> Clears the error cache. </summary>
    Public Overridable Sub ClearErrorCache()
        Me.DeviceErrorQueue.Clear()
        Me.DeviceErrorBuilder = New System.Text.StringBuilder
    End Sub

    ''' <summary> Gets or sets the last message that was sent before the error. </summary>
    ''' <value> The message sent before error. </value>
    Public ReadOnly Property MessageSentBeforeError As String

    ''' <summary> Gets or sets the last message that was received before the error. </summary>
    ''' <value> The message received before error. </value>
    Public ReadOnly Property MessageReceivedBeforeError As String

#End Region

#Region " DEVICE ERRORS: PUBLIC METHODS AND MEMBERS "

    ''' <summary> Message describing the last orphan. </summary>
    Private _LastOrphanMessage As String

    ''' <summary> Gets or sets a message describing the last orphan. </summary>
    ''' <value> A message describing the last orphan. </value>
    Public Property LastOrphanMessage As String
        Get
            Return Me._LastOrphanMessage
        End Get
        Set(value As String)
            Me._LastOrphanMessage = value
            If Not String.IsNullOrWhiteSpace(value) Then Me._OrphanMessagesBuilder.AppendLine(value)
            Me.NotifyPropertyChanged()
            If Not String.IsNullOrWhiteSpace(value) Then Me.NotifyPropertyChanged(NameOf(Me.OrphanMessages))
        End Set
    End Property

    ''' <summary> The orphan messages builder. </summary>
    Private ReadOnly _OrphanMessagesBuilder As New System.Text.StringBuilder

    ''' <summary> Gets the orphan messages. </summary>
    ''' <value> The orphan messages. </value>
    Public ReadOnly Property OrphanMessages As String
        Get
            Return Me._OrphanMessagesBuilder.ToString
        End Get
    End Property

    ''' <summary> Clears the orphan messages. </summary>
    Public Sub ClearOrphanMessages()
        Me._OrphanMessagesBuilder.Clear()
    End Sub

    ''' <summary>
    ''' Queries any existing device errors. This assumes that error available bits where detected on
    ''' the status register.
    ''' </summary>
    Protected Overridable Sub QueryExistingDeviceErrors()
        If Me.ReadingDeviceErrors Then Return
        If Me.MessageAvailable Then
            ' Developer: This is a bug and run condition; 
            '            On reading the status register, the Session Base class is expected to turn off 
            '            the error available flag if an message is present in the presence of an error.
            '            Please check if a run condition had occurred causing the message to appear after the status
            '            register was read as indicating that no such message existed.
            Me.LastOrphanMessage = Me.Session.ReadLineTrimEnd
            Me.PublishWarning($"{Me.ResourceNameCaption} message {Me.LastOrphanMessage} was added to the cache of orphan messages before reading device errors in order to prevent a Query ZUnterminated error")
        End If
        ' There are currently two queue reading commands and two single error reading commands.
        If Not String.IsNullOrWhiteSpace(Me.NextDeviceErrorQueryCommand) Then
            Me.QueryDeviceErrors()
        ElseIf Not String.IsNullOrWhiteSpace(Me.DequeueErrorQueryCommand) Then
            Me.QueryErrorQueue()
        End If
        If Not String.IsNullOrWhiteSpace(Me.DeviceErrorQueryCommand) Then
            If String.IsNullOrWhiteSpace(Me.NextDeviceErrorQueryCommand) AndAlso
                    String.IsNullOrWhiteSpace(Me.DequeueErrorQueryCommand) Then
                ' If No Queue reading commands, 
                ' clear the cache as this device reports only the last error 
                Me.ClearErrorCache()
            End If
            Me.QueryLastError()
        ElseIf Not String.IsNullOrWhiteSpace(Me.LastSystemErrorQueryCommand) Then
            If String.IsNullOrWhiteSpace(Me.NextDeviceErrorQueryCommand) AndAlso
                    String.IsNullOrWhiteSpace(Me.DequeueErrorQueryCommand) Then
                ' If No Queue reading commands, 
                ' clear the cache as this device reports only the last error 
                Me.ClearErrorCache()
            End If
            Me.WriteLastSystemErrorQueryCommand()
            isr.Core.ApplianceBase.DoEvents()
            Me.ReadLastSystemError()
        End If
    End Sub

    ''' <summary> Attempts to clear error cache from the given data. </summary>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryClearErrorCache() As (Success As Boolean, Details As String)
        Dim activity As String = String.Empty
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Try
            activity = "checking the error available"
            activity = "clearing error cache"
            Me.ClearErrorCache()
        Catch ex As Exception
            result = (False, Me.PublishException(activity, ex))
        End Try
        Return result
    End Function

    ''' <summary> Queries device errors. </summary>
    ''' <returns> (Success, Details) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryQueryExistingDeviceErrors() As (Success As Boolean, Details As String)
        Dim activity As String = "reading device errors"
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Try
            activity = "checking if already reading error"
            If Me.ErrorAvailable AndAlso Not Me.ReadingDeviceErrors Then
                activity = "reading device errors"
                Me.QueryExistingDeviceErrors()
            End If
        Catch ex As Exception
            result = (False, Me.PublishException(activity, ex))
        End Try
        Return result
    End Function

    ''' <summary> Gets or sets the device errors queue. </summary>
    ''' <value> The device errors. </value>
    Public ReadOnly Property DeviceErrorQueue As DeviceErrorQueue

    ''' <summary> Gets or sets a message describing the no error compound message. </summary>
    ''' <value> A message describing the no error compound. </value>
    Public Property NoErrorCompoundMessage As String

    ''' <summary> True to reading device errors. </summary>
    Private _ReadingDeviceErrors As Boolean

    ''' <summary> Gets or sets the reading device errors. </summary>
    ''' <value> The reading device errors. </value>
    Public Property ReadingDeviceErrors As Boolean
        Get
            Return Me._ReadingDeviceErrors
        End Get
        Protected Set(value As Boolean)
            If value <> Me.ReadingDeviceErrors Then
                Me._ReadingDeviceErrors = value
                Me.NotifyPropertyChanged()
                If value Then Me.PublishInfo($"{Me.ResourceNameCaption} Reading device errors;. ")
            End If
        End Set
    End Property

#End Region

#Region " DEVICE ERRORS: REPORT "

    ''' <summary> True if has error report, false if not. </summary>
    Private _HasErrorReport As Boolean

    ''' <summary> Gets or sets the has error report. </summary>
    ''' <value> The has error report. </value>
    Public Property HasErrorReport As Boolean
        Get
            Return Me._HasErrorReport
        End Get
        Set(value As Boolean)
            If value <> Me.HasErrorReport Then
                Me._HasErrorReport = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The device error report. </summary>
    Private _DeviceErrorReport As String

    ''' <summary> Gets or sets the device error report. </summary>
    ''' <value> The device error report. </value>
    Public Property DeviceErrorReport As String
        Get
            Return Me._DeviceErrorReport
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.DeviceErrorReport) Then
                Me._DeviceErrorReport = value
                Me.NotifyPropertyChanged()
                Me.HasErrorReport = Not String.IsNullOrEmpty(value)
            End If
        End Set
    End Property

    ''' <summary> builds the device error report. </summary>
    ''' <returns> A String. </returns>
    Protected Function BuildDeviceErrorReport() As String
        Dim builder As New System.Text.StringBuilder(Me.DeviceErrorBuilder.ToString)
        If builder.Length > 0 Then
            If Not Nullable.Equals(Me.Session.StandardEventStatus, 0) Then
                Dim report As String = Pith.SessionBase.BuildReport(Me.Session.StandardEventStatus.Value, ";")
                If Not String.IsNullOrWhiteSpace(report) Then
                    builder.AppendLine(report)
                End If
                If Not String.IsNullOrWhiteSpace(Me.MessageReceivedBeforeError) Then
                    builder.AppendLine($"Received: {Me.MessageReceivedBeforeError}")
                End If
                If Not String.IsNullOrWhiteSpace(Me.MessageSentBeforeError) Then
                    builder.AppendLine($"Sent: {Me.MessageSentBeforeError}")
                End If
            End If
            Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary> True if has device error, false if not. </summary>
    Private _HasDeviceError As Boolean

    ''' <summary> Gets or sets the has device error. </summary>
    ''' <value> The has device error. </value>
    Public Property HasDeviceError As Boolean
        Get
            Return Me._HasDeviceError
        End Get
        Set(value As Boolean)
            If value <> Me.HasDeviceError Then
                Me._HasDeviceError = value
                Me.NotifyPropertyChanged()
                Me.ErrorForeColor = StatusSubsystemBase.SelectErrorColor(value)
            End If
        End Set
    End Property

    ''' <summary> Message describing the compound error. </summary>
    Private _CompoundErrorMessage As String = String.Empty

    ''' <summary> Gets or sets a message describing the compound error. </summary>
    ''' <value> A message describing the compound error. </value>
    Public Property CompoundErrorMessage As String
        Get
            Return Me._CompoundErrorMessage
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.CompoundErrorMessage) Then
                Me._CompoundErrorMessage = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Select error color. </summary>
    ''' <param name="isError"> True if is error, false if not. </param>
    ''' <returns> A Drawing.Color. </returns>
    Protected Shared Function SelectErrorColor(ByVal isError As Boolean) As Drawing.Color
        Return If(isError, Drawing.Color.OrangeRed, Drawing.Color.Aquamarine)
    End Function

    ''' <summary> The error foreground color. </summary>
    Private _ErrorForeColor As Drawing.Color = Drawing.Color.White

    ''' <summary> Gets or sets the color of the error foreground. </summary>
    ''' <value> The color of the error foreground. </value>
    Public Property ErrorForeColor As Drawing.Color
        Get
            Return Me._ErrorForeColor
        End Get
        Set(value As Drawing.Color)
            If Not Drawing.Color.Equals(value, Me._ErrorForeColor) Then
                Me._ErrorForeColor = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Clears the error report. </summary>
    Public Overridable Sub ClearErrorReport()
        Me.DeviceErrorReport = String.Empty
        Me.CompoundErrorMessage = String.Empty
        Me.HasDeviceError = False
        Me._MessageSentBeforeError = String.Empty
        Me._MessageReceivedBeforeError = String.Empty
    End Sub

    ''' <summary> Reports last error. </summary>
    Protected Sub ReportLastError()
        Me.DeviceErrorReport = Me.BuildDeviceErrorReport
        Dim lastError As DeviceError = Me.DeviceErrorQueue.LastError
        Me.CompoundErrorMessage = lastError.CompoundErrorMessage
        Me.HasDeviceError = lastError.IsError
        If Me.HasDeviceError Then Me.PublishWarning($"{Me.ResourceNameCaption} error;. {Me.CompoundErrorMessage}")
        If Me.HasErrorReport Then Me.PublishWarning($"{Me.ResourceNameCaption} errors;. {Me.DeviceErrorReport}")
    End Sub

#End Region

#Region " DEVICE ERRORS: PROTECTED QUERY METHODS AND MEMBERS "

    ''' <summary> The device errors. </summary>
    ''' <value> The device error builder. </value>
    Protected Property DeviceErrorBuilder As System.Text.StringBuilder

    ''' <summary> Appends a device error message. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub AppendDeviceErrorMessage(ByVal value As String)
        If Not String.IsNullOrWhiteSpace(value) Then Me.DeviceErrorBuilder.AppendLine(value)
    End Sub

    ''' <summary> Enqueues device error. </summary>
    ''' <param name="compoundErrorMessage"> Message describing the compound error. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected Overridable Function EnqueueDeviceError(ByVal compoundErrorMessage As String) As DeviceError
        Dim de As DeviceError = New DeviceError(Me.NoErrorCompoundMessage)
        de.Parse(compoundErrorMessage)
        If de.IsError Then Me.DeviceErrorQueue.Enqueue(de)
        Return de
    End Function

    ''' <summary> Enqueue last error. </summary>
    ''' <param name="compoundErrorMessage"> Message describing the compound error. </param>
    ''' <returns> A DeviceError. </returns>
    Public Function EnqueueLastError(ByVal compoundErrorMessage As String) As DeviceError
        Dim de As DeviceError = Me.EnqueueDeviceError(compoundErrorMessage)
        If de.IsError Then
            Me.AppendDeviceErrorMessage($"{Me.ResourceNameCaption} Last Error:
{de.ErrorMessage}")
            Me.ReportLastError()
        End If
        Return de
    End Function

#End Region

#Region " QUERY DEVICE ERROR: USES NEXT ERROR COMMANDS :STAT:QUE? "

    ''' <summary> Gets or sets the 'Next Error' query command. </summary>
    ''' <remarks>
    ''' SCPI: ":STAT:QUE?".
    ''' <see cref="VI.Pith.Scpi.Syntax.NextErrorQueryCommand"> </see>
    ''' </remarks>
    ''' <value> The error queue query command. </value>
    Protected Overridable Property NextDeviceErrorQueryCommand As String

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> The device errors. </returns>
    Protected Overridable Function QueryDeviceErrors() As String
        If Me.ReadingDeviceErrors Then Return String.Empty
        Dim errorsRecorded As Boolean = False
        Try
            Me._MessageSentBeforeError = Me.Session.LastMessageSent
            Me._MessageReceivedBeforeError = Me.Session.LastMessageReceived
            Me.ReadingDeviceErrors = True
            Me.ClearErrorCache()
            If Not String.IsNullOrWhiteSpace(Me.NextDeviceErrorQueryCommand) AndAlso Me.Session.IsErrorBitSet() Then
                Dim builder As New System.Text.StringBuilder
                Dim de As DeviceError = DeviceError.NoError
                Do
                    de = Me.EnqueueDeviceError(Me.Session.QueryTrimEnd(Me.NextDeviceErrorQueryCommand))
                    If de.IsError Then
                        errorsRecorded = True
                        builder.AppendLine(de.CompoundErrorMessage)
                    End If
                Loop While Me.Session.IsErrorBitSet() AndAlso de.IsError
                ' this is a kludge because the 7510 does not clear the error queue.
                If Not de.IsError AndAlso Me.Session.IsErrorBitSet() Then Me.Session.Execute(Me.ClearErrorQueueCommand)
                If builder.Length > 0 Then
                    Me.Session.QueryStandardEventStatus()
                    Me.AppendDeviceErrorMessage(builder.ToString)
                End If
            End If
            If errorsRecorded Then Me.ReportLastError()
            Return Me.DeviceErrorReport
        Catch
            Throw
        Finally
            Me.ReadingDeviceErrors = False
        End Try
    End Function

#End Region

#Region " QUERY DEVICE ERROR: USES DEQUEUE COMMANDS ?? "

    ''' <summary> Gets or sets the 'Next Error' query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overridable Property DequeueErrorQueryCommand As String

    ''' <summary> Queries the next error from the device error queue. </summary>
    ''' <returns> The <see cref="DeviceError">Device Error structure.</see> </returns>
    Protected Function QueryNextError() As DeviceError
        Dim de As DeviceError = New DeviceError(Me.NoErrorCompoundMessage)
        If Not String.IsNullOrWhiteSpace(Me.DequeueErrorQueryCommand) Then
            de = Me.EnqueueDeviceError(Me.Session.QueryTrimEnd(Me.DequeueErrorQueryCommand))
        End If
        Return de
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> The device errors. </returns>
    Protected Overridable Function QueryErrorQueue() As String
        If Me.ReadingDeviceErrors Then Return String.Empty
        Dim errorsRecorded As Boolean = False
        Try
            Me._MessageSentBeforeError = Me.Session.LastMessageSent
            Me._MessageReceivedBeforeError = Me.Session.LastMessageReceived
            Me.ReadingDeviceErrors = True
            Me.ClearErrorCache()
            If Not String.IsNullOrWhiteSpace(Me.DequeueErrorQueryCommand) AndAlso Me.Session.IsErrorBitSet() Then
                Dim builder As New System.Text.StringBuilder
                Dim de As DeviceError = DeviceError.NoError
                Do
                    de = Me.QueryNextError
                    If de.IsError Then
                        errorsRecorded = True
                        builder.AppendLine(de.CompoundErrorMessage)
                    End If
                Loop While Me.Session.IsErrorBitSet() AndAlso de.IsError

                ' this is a kludge because the 7510 does not clear the error queue.
                If Not de.IsError AndAlso Me.Session.IsErrorBitSet() Then Me.Session.Execute(Me.ClearErrorQueueCommand)

                If builder.Length > 0 Then
                    Me.Session.QueryStandardEventStatus()
                    Me.AppendDeviceErrorMessage(builder.ToString)
                End If
            End If
            If errorsRecorded Then Me.ReportLastError()
            Return Me.DeviceErrorReport
        Catch
            Throw
        Finally
            Me.ReadingDeviceErrors = False
        End Try
    End Function

#End Region

#Region " LAST ERROR "

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overridable Property DeviceErrorQueryCommand As String

    ''' <summary> Queries the last error from the device. </summary>
    ''' <returns> The <see cref="DeviceError">Device Error structure.</see> </returns>
    Protected Overridable Function QueryLastError() As DeviceError
        Dim de As DeviceError = New DeviceError(Me.NoErrorCompoundMessage)
        If Not String.IsNullOrWhiteSpace(Me.DeviceErrorQueryCommand) Then
            de = Me.EnqueueDeviceError(Me.Session.QueryTrimEnd(Me.DeviceErrorQueryCommand))
            If de.IsError Then
                Me.AppendDeviceErrorMessage($"{Me.ResourceNameCaption} Last Error:
{de.ErrorMessage}")
                Me.ReportLastError()
            End If
        End If
        Return de
    End Function

#End Region

#Region " LAST SYSTEM ERROR "

    ''' <summary> Gets or sets the last system error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overridable Property LastSystemErrorQueryCommand As String

    ''' <summary> Reads last error. </summary>
    ''' <returns> The last error. </returns>
    Protected Function ReadLastSystemError() As DeviceError
        Dim de As DeviceError = New DeviceError(Me.NoErrorCompoundMessage)
        If Not String.IsNullOrWhiteSpace(Me.LastSystemErrorQueryCommand) Then
            de = Me.EnqueueLastError(Me.Session.ReadLine)
        End If
        Return de
    End Function

    ''' <summary> Writes the last error query command. </summary>
    Protected Sub WriteLastSystemErrorQueryCommand()
        If Not String.IsNullOrWhiteSpace(Me.LastSystemErrorQueryCommand) Then
            Me.Session.WriteLine(Me.LastSystemErrorQueryCommand)
        End If
    End Sub

#End Region

#Region " ERROR QUEUE COUNT "

    ''' <summary> The ErrorQueueCount. </summary>
    Private _ErrorQueueCount As Integer?

    ''' <summary> Gets or sets the cached error queue count. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property ErrorQueueCount As Integer?
        Get
            Return Me._ErrorQueueCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.ErrorQueueCount, value) Then
                Me._ErrorQueueCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets The ErrorQueueCount query command. </summary>
    ''' <value> The ErrorQueueCount query command. </value>
    Protected Overridable Property ErrorQueueCountQueryCommand As String

    ''' <summary> Queries The ErrorQueueCount. </summary>
    ''' <returns> The ErrorQueueCount or none if unknown. </returns>
    Public Function QueryErrorQueueCount() As Integer?
        Me.ErrorQueueCount = Me.Query(Me.ErrorQueueCount, Me.ErrorQueueCountQueryCommand)
        Return Me.ErrorQueueCount
    End Function

#End Region

End Class

