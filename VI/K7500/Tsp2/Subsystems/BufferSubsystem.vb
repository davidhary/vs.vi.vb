''' <summary> Buffer subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class BufferSubsystem
    Inherits VI.Tsp2.BufferSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="BufferSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="bufferName">      Name of the buffer. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal bufferName As String, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(bufferName, statusSubsystem)
    End Sub

#End Region

End Class
