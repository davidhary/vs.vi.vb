﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReadingView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReadingView))
        Me._BufferDataGridView = New System.Windows.Forms.DataGridView()
        Me._BufferToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReadBufferButton = New System.Windows.Forms.ToolStripButton()
        Me._BufferCountLabel = New isr.Core.Controls.ToolStripLabel()
        Me._LastPointNumberLabel = New isr.Core.Controls.ToolStripLabel()
        Me._FirstPointNumberLabel = New isr.Core.Controls.ToolStripLabel()
        Me._BufferNameLabel = New isr.Core.Controls.ToolStripLabel()
        Me._ClearBufferDisplayButton = New System.Windows.Forms.ToolStripButton()
        Me._BufferSizeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._ReadingToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReadButton = New System.Windows.Forms.ToolStripButton()
        Me._ReadingComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._AbortButton = New System.Windows.Forms.ToolStripButton()
        Me._GoSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._InitiateTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortStartTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MonitorActiveTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InitMonitorReadRepeatMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RepeatMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StreamBufferMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TriggerStateLabel = New isr.Core.Controls.ToolStripLabel()
        CType(Me._BufferDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._BufferToolStrip.SuspendLayout()
        Me._ReadingToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_BufferDataGridView
        '
        Me._BufferDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._BufferDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._BufferDataGridView.Location = New System.Drawing.Point(1, 54)
        Me._BufferDataGridView.Name = "_BufferDataGridView"
        Me._BufferDataGridView.Size = New System.Drawing.Size(345, 313)
        Me._BufferDataGridView.TabIndex = 12
        '
        '_BufferToolStrip
        '
        Me._BufferToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._BufferToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._BufferToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadBufferButton, Me._BufferCountLabel, Me._LastPointNumberLabel, Me._FirstPointNumberLabel, Me._BufferNameLabel, Me._ClearBufferDisplayButton, Me._BufferSizeNumeric})
        Me._BufferToolStrip.Location = New System.Drawing.Point(1, 26)
        Me._BufferToolStrip.Name = "_BufferToolStrip"
        Me._BufferToolStrip.Size = New System.Drawing.Size(345, 28)
        Me._BufferToolStrip.TabIndex = 11
        Me._BufferToolStrip.Text = "Buffer"
        '
        '_ReadBufferButton
        '
        Me._ReadBufferButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadBufferButton.Image = CType(resources.GetObject("_ReadBufferButton.Image"), System.Drawing.Image)
        Me._ReadBufferButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadBufferButton.Name = "_ReadBufferButton"
        Me._ReadBufferButton.Size = New System.Drawing.Size(72, 25)
        Me._ReadBufferButton.Text = "Read Buffer"
        '
        '_BufferCountLabel
        '
        Me._BufferCountLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._BufferCountLabel.Name = "_BufferCountLabel"
        Me._BufferCountLabel.Size = New System.Drawing.Size(13, 25)
        Me._BufferCountLabel.Text = "0"
        Me._BufferCountLabel.ToolTipText = "Buffer count"
        '
        '_LastPointNumberLabel
        '
        Me._LastPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._LastPointNumberLabel.Name = "_LastPointNumberLabel"
        Me._LastPointNumberLabel.Size = New System.Drawing.Size(13, 25)
        Me._LastPointNumberLabel.Text = "2"
        Me._LastPointNumberLabel.ToolTipText = "Number of last buffer reading"
        '
        '_FirstPointNumberLabel
        '
        Me._FirstPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._FirstPointNumberLabel.Name = "_FirstPointNumberLabel"
        Me._FirstPointNumberLabel.Size = New System.Drawing.Size(13, 25)
        Me._FirstPointNumberLabel.Text = "1"
        Me._FirstPointNumberLabel.ToolTipText = "Number of the first buffer reading"
        '
        '_BufferNameLabel
        '
        Me._BufferNameLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._BufferNameLabel.Name = "_BufferNameLabel"
        Me._BufferNameLabel.Size = New System.Drawing.Size(62, 25)
        Me._BufferNameLabel.Text = "defbuffer1"
        '
        '_ClearBufferDisplayButton
        '
        Me._ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ClearBufferDisplayButton.Image = CType(resources.GetObject("_ClearBufferDisplayButton.Image"), System.Drawing.Image)
        Me._ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton"
        Me._ClearBufferDisplayButton.Size = New System.Drawing.Size(38, 25)
        Me._ClearBufferDisplayButton.Text = "Clear"
        Me._ClearBufferDisplayButton.ToolTipText = "Clears the display"
        '
        '_BufferSizeNumeric
        '
        Me._BufferSizeNumeric.AutoSize = False
        Me._BufferSizeNumeric.Name = "_BufferSizeNumeric"
        Me._BufferSizeNumeric.Size = New System.Drawing.Size(71, 25)
        Me._BufferSizeNumeric.Text = "0"
        Me._BufferSizeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_ReadingToolStrip
        '
        Me._ReadingToolStrip.GripMargin = New System.Windows.Forms.Padding(20)
        Me._ReadingToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ReadingToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadButton, Me._ReadingComboBox, Me._AbortButton, Me._GoSplitButton, Me._TriggerStateLabel})
        Me._ReadingToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._ReadingToolStrip.Name = "_ReadingToolStrip"
        Me._ReadingToolStrip.Size = New System.Drawing.Size(345, 25)
        Me._ReadingToolStrip.TabIndex = 10
        Me._ReadingToolStrip.Text = "Reading Tool Strip"
        '
        '_ReadButton
        '
        Me._ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadButton.Image = CType(resources.GetObject("_ReadButton.Image"), System.Drawing.Image)
        Me._ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(37, 22)
        Me._ReadButton.Text = "Read"
        '
        '_ReadingComboBox
        '
        Me._ReadingComboBox.Name = "_ReadingComboBox"
        Me._ReadingComboBox.Size = New System.Drawing.Size(121, 25)
        Me._ReadingComboBox.ToolTipText = "Select reading type to display"
        '
        '_AbortButton
        '
        Me._AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AbortButton.Image = CType(resources.GetObject("_AbortButton.Image"), System.Drawing.Image)
        Me._AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(41, 22)
        Me._AbortButton.Text = "Abort"
        Me._AbortButton.ToolTipText = "Aborts triggering"
        '
        '_GoSplitButton
        '
        Me._GoSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._GoSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._GoSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InitiateTriggerPlanMenuItem, Me._AbortStartTriggerPlanMenuItem, Me._MonitorActiveTriggerPlanMenuItem, Me._InitMonitorReadRepeatMenuItem, Me._RepeatMenuItem, Me._StreamBufferMenuItem})
        Me._GoSplitButton.Image = CType(resources.GetObject("_GoSplitButton.Image"), System.Drawing.Image)
        Me._GoSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._GoSplitButton.Name = "_GoSplitButton"
        Me._GoSplitButton.Size = New System.Drawing.Size(47, 22)
        Me._GoSplitButton.Text = "Go..."
        Me._GoSplitButton.ToolTipText = "Select initiation options"
        '
        '_InitiateTriggerPlanMenuItem
        '
        Me._InitiateTriggerPlanMenuItem.Name = "_InitiateTriggerPlanMenuItem"
        Me._InitiateTriggerPlanMenuItem.Size = New System.Drawing.Size(221, 22)
        Me._InitiateTriggerPlanMenuItem.Text = "Initiate"
        Me._InitiateTriggerPlanMenuItem.ToolTipText = "Sends the Initiate command to the instrument"
        '
        '_AbortStartTriggerPlanMenuItem
        '
        Me._AbortStartTriggerPlanMenuItem.Name = "_AbortStartTriggerPlanMenuItem"
        Me._AbortStartTriggerPlanMenuItem.Size = New System.Drawing.Size(221, 22)
        Me._AbortStartTriggerPlanMenuItem.Text = "Abort, Clear, Initiate"
        Me._AbortStartTriggerPlanMenuItem.ToolTipText = "Aborts, clears buffer and starts the trigger plan"
        '
        '_MonitorActiveTriggerPlanMenuItem
        '
        Me._MonitorActiveTriggerPlanMenuItem.Name = "_MonitorActiveTriggerPlanMenuItem"
        Me._MonitorActiveTriggerPlanMenuItem.Size = New System.Drawing.Size(221, 22)
        Me._MonitorActiveTriggerPlanMenuItem.Text = "Monitor Active Trigger State"
        Me._MonitorActiveTriggerPlanMenuItem.ToolTipText = "Monitors active trigger state. Exits if trigger plan inactive"
        '
        '_InitMonitorReadRepeatMenuItem
        '
        Me._InitMonitorReadRepeatMenuItem.Name = "_InitMonitorReadRepeatMenuItem"
        Me._InitMonitorReadRepeatMenuItem.Size = New System.Drawing.Size(221, 22)
        Me._InitMonitorReadRepeatMenuItem.Text = "Init, Monitor, Read, Repeat"
        Me._InitMonitorReadRepeatMenuItem.ToolTipText = "Initiates a trigger plan, monitors it, reads and displays buffer and repeats if a" &
    "uto repeat is checked"
        '
        '_RepeatMenuItem
        '
        Me._RepeatMenuItem.CheckOnClick = True
        Me._RepeatMenuItem.Name = "_RepeatMenuItem"
        Me._RepeatMenuItem.Size = New System.Drawing.Size(221, 22)
        Me._RepeatMenuItem.Text = "Repeat"
        Me._RepeatMenuItem.ToolTipText = "Repeat initiating the trigger plan"
        '
        '_StreamBufferMenuItem
        '
        Me._StreamBufferMenuItem.CheckOnClick = True
        Me._StreamBufferMenuItem.Name = "_StreamBufferMenuItem"
        Me._StreamBufferMenuItem.Size = New System.Drawing.Size(221, 22)
        Me._StreamBufferMenuItem.Text = "Stream Buffer"
        Me._StreamBufferMenuItem.ToolTipText = "Continuously reads new values while a trigger plan is active"
        '
        '_TriggerStateLabel
        '
        Me._TriggerStateLabel.Name = "_TriggerStateLabel"
        Me._TriggerStateLabel.Size = New System.Drawing.Size(26, 22)
        Me._TriggerStateLabel.Text = "idle"
        Me._TriggerStateLabel.ToolTipText = "Trigger state"
        '
        'ReadingView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._BufferDataGridView)
        Me.Controls.Add(Me._BufferToolStrip)
        Me.Controls.Add(Me._ReadingToolStrip)
        Me.Name = "ReadingView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(347, 368)
        CType(Me._BufferDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me._BufferToolStrip.ResumeLayout(False)
        Me._BufferToolStrip.PerformLayout()
        Me._ReadingToolStrip.ResumeLayout(False)
        Me._ReadingToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _BufferDataGridView As Windows.Forms.DataGridView
    Private WithEvents _BufferToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ReadBufferButton As Windows.Forms.ToolStripButton
    Private WithEvents _BufferCountLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _LastPointNumberLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _FirstPointNumberLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _BufferNameLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _ClearBufferDisplayButton As Windows.Forms.ToolStripButton
    Private WithEvents _BufferSizeNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _ReadingToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ReadButton As Windows.Forms.ToolStripButton
    Private WithEvents _ReadingComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _AbortButton As Windows.Forms.ToolStripButton
    Private WithEvents _GoSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _InitiateTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AbortStartTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _MonitorActiveTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InitMonitorReadRepeatMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RepeatMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _StreamBufferMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _TriggerStateLabel As isr.Core.Controls.ToolStripLabel
End Class
