''' <summary> K2002 Visa Session unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k7001")>
Partial Public Class VisaSessionTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(ResourceSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " RESOURCE "

    ''' <summary> (Unit Test Method) tests visa resource. </summary>
    ''' <remarks> Finds the resource using the session factory resources manager. </remarks>
    <TestMethod()>
    Public Sub VisaResourceTest()
        isr.VI.DeviceTests.DeviceManager.AssertVisaResourceManagerIncludesResource(ResourceSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) tests session resource. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SessionResourceTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            isr.VI.DeviceTests.DeviceManager.AssertResourceFound(session, ResourceSettings.Get)
        End Using
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> (Unit Test Method) tests Visa Session talker. </summary>
    ''' <remarks> Checks if the device adds a trace message to a listener. </remarks>
    <TestMethod()>
    Public Sub TalkerTest()
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            isr.VI.DeviceTests.DeviceManager.AssertDeviceTalks(TestInfo, session)
        End Using
    End Sub

#End Region

End Class
