''' <summary> Defines a Route Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class RouteSubsystem
    Inherits VI.RouteSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " CLOSED CHANNEL "

    ''' <summary> Gets or sets the closed Channel query command. </summary>
    ''' <remarks> Requires either 2000-SCAN or 2001-SCAN cards. </remarks>
    ''' <value> The closed Channel query command. </value>
    Protected Overrides Property ClosedChannelQueryCommand As String = ":ROUT:CLOS:STAT?"

    ''' <summary> Gets or sets the closed Channel command format. </summary>
    ''' <value> The closed Channel command format. </value>
    Protected Overrides Property ClosedChannelCommandFormat As String = ":ROUT:CLOS {0}"

    ''' <summary> Gets or sets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    Protected Overrides Property OpenChannelCommandFormat As String = ":ROUT:OPEN {0}"

#End Region

#Region " CLOSED CHANNELS "

    ''' <summary> Gets the closed channels query command. </summary>
    ''' <value> The closed channels query command. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property ClosedChannelsQueryCommand As String = String.Empty

    ''' <summary> Gets the closed channels command format. </summary>
    ''' <value> The closed channels command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property ClosedChannelsCommandFormat As String = String.Empty

    ''' <summary> Gets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property OpenChannelsCommandFormat As String = String.Empty

#End Region

#Region " CHANNELS "

    ''' <summary> Gets the recall channel pattern command format. </summary>
    ''' <value> The recall channel pattern command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property RecallChannelPatternCommandFormat As String = String.Empty

    ''' <summary> Gets the save channel pattern command format. </summary>
    ''' <value> The save channel pattern command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property SaveChannelPatternCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    Protected Overrides Property OpenChannelsCommand As String = ":ROUT:OPEN:ALL"


#End Region

#Region " SCAN LIST "

    ''' <summary> Gets or sets the scan list command query. </summary>
    ''' <value> The scan list query command. </value>
    Protected Overrides Property ScanListQueryCommand As String = ":ROUT:SCAN?"

    ''' <summary> Gets or sets the scan list command format. </summary>
    ''' <value> The scan list command format. </value>
    Protected Overrides Property ScanListCommandFormat As String = ":ROUT:SCAN {0}"

#End Region

#Region " SCAN LIST FUNCTION "

    ''' <summary> Gets or sets the Scan List Function command format. </summary>
    ''' <value> The Scan List Function command format. </value>
    Protected Overrides Property ScanListFunctionCommandFormat As String = ":ROUT:SCAN:FUNC {0},{1}"

#End Region

#Region " SELECT SCAN LIST "

    ''' <summary> Gets or sets the Selected Scan List Type command query. </summary>
    ''' <value> The Selected Scan List Type query command. </value>
    Protected Overrides Property SelectedScanListTypeQueryCommand As String = ":ROUT:SCAN:LSEL?"

    ''' <summary> Gets or sets the Selected Scan List Type command format. </summary>
    ''' <value> The Selected Scan List Type command format. </value>
    Protected Overrides Property SelectedScanListTypeCommandFormat As String = ":ROUT:SCAN:LSEL {0}"

#End Region

#Region " SLOT CARD TYPE "

    ''' <summary> Gets the slot card type command format. </summary>
    ''' <value> The slot card type command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property SlotCardTypeCommandFormat As String = String.Empty

    ''' <summary> Gets the slot card type query command format. </summary>
    ''' <value> The slot card type query command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property SlotCardTypeQueryCommandFormat As String = String.Empty

#End Region

#Region " SLOT CARD SETTLING TIME "

    ''' <summary> Gets the slot card settling time command format. </summary>
    ''' <value> The slot card settling time command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property SlotCardSettlingTimeCommandFormat As String = String.Empty

    ''' <summary> Gets the slot card settling time query command format. </summary>
    ''' <value> The slot card settling time query command format. </value>
    <Obsolete("Not supported for the K2002 Multi Meters.")>
    Protected Overrides Property SlotCardSettlingTimeQueryCommandFormat As String = String.Empty

#End Region

#Region " TERMINAL MODE "

    ''' <summary> Gets or sets the terminals mode query command. </summary>
    ''' <value> The terminals mode command. </value>
    Protected Overrides Property TerminalsModeQueryCommand As String = ":ROUT:TERM?"

    ''' <summary> Gets or sets the terminals mode command format. </summary>
    ''' <value> The terminals mode command format. </value>
    Protected Overrides Property TerminalsModeCommandFormat As String = String.Empty ' read only; ":ROUT:TERM {0}"

#End Region

#Region " SCAN CARD TEST FUNCTIONS "

    ''' <summary> Clears the trace buffer. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="device"> The device. </param>
    Public Shared Sub ClearBuffer(ByVal device As K2002Device)
        device.TraceSubsystem.ClearBuffer(FeedControls.Next)
    End Sub

    ''' <summary> Clears the trace buffer. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="session"> The session. </param>
    Public Shared Sub ClearBuffer(ByVal session As VI.Pith.SessionBase)
        session.WriteLine("TRAC:CLE")             ' Clear the internal data buffer
        session.WriteLine("TRAC:FEED:CONT NEXT")  ' Set the buffer control mode
    End Sub

    ''' <summary> Builds four wire resistance scan help message. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <returns> A String. </returns>
    Public Shared Function BuildFourWireResistanceScanHelpMessage() As String
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine("Measure channels pairs (1:6, 2:7, 3:8) w/ sense subsystem 4W function.")
        builder.AppendLine("Saving values to the buffer and reading back when done.")
        builder.AppendLine()
        Return builder.ToString
    End Function

    ''' <summary>
    ''' Configures four wire resistance scan using virtual instrument library functions.
    ''' </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="device">        The device. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="timerInterval"> The timer interval. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ConfigureFourWireResistanceScan(ByVal device As K2002Device, ByVal triggerSource As ArmSources,
                                                           ByVal triggerCount As Integer, ByVal timerInterval As TimeSpan) As String
        Return ConfigureFourWireResistanceScan(device, "(@1:3)", 3, triggerSource, triggerCount, timerInterval)
    End Function

    ''' <summary>
    ''' Configures four wire resistance scan using virtual instrument library functions.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="device">        The device. </param>
    ''' <param name="scanList">      List of scans. </param>
    ''' <param name="sampleCount">   Number of samples. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="timerInterval"> The timer interval. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ConfigureFourWireResistanceScan(ByVal device As K2002Device, ByVal scanList As String, ByVal sampleCount As Integer,
                                                           ByVal triggerSource As ArmSources, ByVal triggerCount As Integer,
                                                           ByVal timerInterval As TimeSpan) As String
        Dim result As String = String.Empty
        device.ResetKnownState()
        ' this is required for getting the correct function mode when fetching buffers.
        device.SenseSubsystem.ApplyFunctionMode(SenseFunctionModes.ResistanceFourWire)
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.RouteSubsystem.ApplySelectedScanListType(ScanListType.None)
        device.RouteSubsystem.ApplyScanListFunction("(@1:10)", SenseFunctionModes.None, device.SenseSubsystem.FunctionModeReadWrites)
        device.RouteSubsystem.ApplyScanListFunction(scanList, SenseFunctionModes.ResistanceFourWire, device.SenseSubsystem.FunctionModeReadWrites)
        device.TriggerSubsystem.ApplyTriggerCount(sampleCount)
        device.TriggerSubsystem.ApplyTriggerSource(TriggerSources.Immediate)
        device.ArmLayer2Subsystem.ApplyArmCount(triggerCount)
        device.ArmLayer2Subsystem.ApplyArmSource(triggerSource)
        If ArmSources.Timer = triggerSource Then
            device.ArmLayer2Subsystem.ApplyTimerTimeSpan(timerInterval)
        End If
        device.TraceSubsystem.ClearBuffer()
        device.TraceSubsystem.ApplyPointsCount(sampleCount)
        device.TraceSubsystem.ApplyFeedSource(FeedSources.Sense)
        device.TraceSubsystem.ApplyFeedControl(FeedControls.Next)
        device.RouteSubsystem.ApplySelectedScanListType(ScanListType.Internal)
        Return result
    End Function

    ''' <summary> Configures four wire resistance scan using SCPI commands. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="session">       The session. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="timerInterval"> The timer interval. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ConfigureFourWireResistanceScan(ByVal session As VI.Pith.SessionBase, ByVal triggerSource As String,
                                                           ByVal triggerCount As Integer, ByVal timerInterval As TimeSpan) As String
        session.WriteLine("*RST")
        session.WriteLine("INIT:CONT OFF")        ' Disable continuous triggering
        session.WriteLine("ROUT:SCAN:LSEL NONE")  ' Disable scanning
        session.WriteLine("ROUT:SCAN:FUNC (@1:10), 'NONE'") ' Turn off measurements on channels 4, 5, 9, and 10;
        session.WriteLine("ROUT:SCAN:FUNC (@1:3), 'FRES'") ' Configure scan card paired channels 1-3 for 4W resistance measurement;
        ' session.WriteLine("ROUT:SCAN:FUNC (@4:10), 'NONE'") ' Turn off measurements on channels 4, 5, 9, and 10;

        session.WriteLine("TRIG:COUN 3")          ' Set to capture 3 samples per trigger event
        session.WriteLine("TRIG:SOUR IMM")        ' Trigger immediately after passing the arm layer
        session.WriteLine($"ARM:LAY2:COUN {triggerCount}")    ' Set the number of triggers
        session.WriteLine($"ARM:LAY2:SOUR {triggerSource}")   ' Set the trigger source
        If triggerSource.StartsWith("TIM", StringComparison.OrdinalIgnoreCase) Then
            session.WriteLine($"ARM:LAY2:TIM {0.001 * timerInterval.TotalMilliseconds}")
        End If

        session.WriteLine("TRAC:CLE")             ' Clear the internal data buffer
        session.WriteLine("TRAC:POIN 3")          ' Size the buffer to capture one reading per sample per trigger
        session.WriteLine("TRAC:FEED SENS")       ' Set the source of readings
        session.WriteLine("TRAC:FEED:CONT NEXT")  ' Set the buffer control mode
        session.WriteLine("ROUT:SCAN:LSEL INT")   ' Enable scanning
        Return String.Empty
    End Function

    ''' <summary>
    ''' Initiates four wire resistance scan using virtual instrument library functions.
    ''' </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="device">            The device. </param>
    ''' <param name="bufferFullBitmask"> The buffer full bitmask. </param>
    Public Shared Sub InitiateFourWireResistanceScan(ByVal device As K2002Device, ByVal bufferFullBitmask As Integer)
        device.StatusSubsystem.PresetKnownState()
        device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(bufferFullBitmask)
        device.TriggerSubsystem.Initiate()
    End Sub

    ''' <summary> Initiates a four wire resistance scan using SCPI Commands. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="session">           The session. </param>
    ''' <param name="bufferFullBitmask"> The buffer full bitmask. </param>
    Public Shared Sub InitiateFourWireResistanceScan(ByVal session As VI.Pith.SessionBase, ByVal bufferFullBitmask As Integer)
        session.WriteLine("STAT:PRES;*CLS")
        Dim bitmask As Integer = bufferFullBitmask ' Set measurement event bitmask to buffer available
        session.WriteLine($"STAT:MEAS:ENAB {bitmask}")
        session.WriteLine("*SRE 1")
        session.WriteLine("INIT")                           ' Trigger the scanInitiates that trigger plan
    End Sub

    ''' <summary>
    ''' Fetches four wire resistance scan using virtual instrument library functions.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="device"> The device. </param>
    ''' <returns> The (Status As String, Reading As String) </returns>
    Public Shared Function FetchFourWireResistanceScan(ByVal device As K2002Device) As (Status As String, Value As String, Elapsed As TimeSpan)
        Dim timeout As TimeSpan = TimeSpan.FromSeconds(10)
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim timedOut As Boolean = sw.Elapsed > timeout
        Dim measurementCondition As Integer
        Do Until timedOut OrElse device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn(measurementCondition, MeasurementEventBitmaskKey.BufferFull)
            isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(10))
            measurementCondition = device.StatusSubsystem.QueryMeasurementEventStatus.GetValueOrDefault(0)
            timedOut = sw.Elapsed > timeout
        Loop
        sw.Stop()
        Dim status As String = device.StatusSubsystem.QueryMeasurementEventStatus().ToString
        device.FormatSubsystem.ApplyElements(ReadingElementTypes.Reading Or ReadingElementTypes.Units)
        Return (status, device.TraceSubsystem.QueryData(), sw.Elapsed)
    End Function

    ''' <summary> Configure four wire resistance scan using SCPI Commands. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="session">           The session. </param>
    ''' <param name="bufferFullBitmask"> The buffer full bitmask. </param>
    ''' <returns> The (Status As String, Reading As String) </returns>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Function FetchFourWireResistanceScan(ByVal session As VI.Pith.SessionBase,
                                                       ByVal bufferFullBitmask As Integer) As (Status As String, Reading As String, Elapsed As TimeSpan)
        ' wait for the buffer to fill or some other event that shows up in the status register>>>
        Dim srq As Pith.ServiceRequests = session.ReadStatusRegister()

        ' wait for service request
        Dim expectedRequest As Pith.ServiceRequests = Pith.ServiceRequests.RequestingService
        Dim timeout As TimeSpan = TimeSpan.FromSeconds(10)
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim timedOut As Boolean = sw.Elapsed > timeout
        Do Until expectedRequest = (srq And expectedRequest) OrElse timedOut
            srq = session.ReadStatusRegister(TimeSpan.FromMilliseconds(10))
            timedOut = sw.Elapsed > timeout
        Loop
        session.WriteLine("STAT:MEAS?")                     ' get measurement status
        Dim status As String = session.ReadFreeLineTrimEnd()
        session.WriteLine("TRAC:DATA?")                     ' Extract the data from the buffer
        Dim reading As String = session.ReadFreeLineTrimEnd()
        sw.Stop()
        Return (status, reading, sw.Elapsed)
    End Function

    ''' <summary> Builds four wire resistance timer scan help message. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <returns> A String. </returns>
    Public Shared Function BuildFourWireResistanceTimerScanHelpMessage() As String
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine("Measures measure 4W resistance on channels 1, 2 and 3.")
        builder.AppendLine("The meter takes ten sets Of readings, with each set spaced 1 seconds apart.")
        builder.AppendLine("Each of the three readings in each group taken as fast as possible. The")
        builder.AppendLine("The readings are stored in the buffer.")
        builder.AppendLine("SRQ is asserted SRQ When the buffer Is full.")
        builder.AppendLine("The program waits For the SRQ, then reads the readings from the buffer. ")
        Return builder.ToString
    End Function

#End Region

End Class
