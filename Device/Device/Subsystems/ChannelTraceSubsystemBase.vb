''' <summary> Defines the Channel Trace subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public MustInherit Class ChannelTraceSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ChannelTraceSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="traceNumber">     The Trace number. </param>
    ''' <param name="channelNumber">   The channel number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal traceNumber As Integer, ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.TraceNumber = traceNumber
        Me.ChannelNumber = channelNumber
        Me.DefineTraceParameterReadWrites()
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Gets or sets the channel number. </summary>
    ''' <value> The channel number. </value>
    Public ReadOnly Property ChannelNumber As Integer

#End Region

#Region " TRACE "

    ''' <summary> Gets or sets the Trace number. </summary>
    ''' <value> The Trace number. </value>
    Public ReadOnly Property TraceNumber As Integer

#End Region

#Region " AUTO SCALE "

    ''' <summary> Gets or sets the auto scale command. </summary>
    ''' <remarks> SCPI: ":DISP:WIND&lt;c#&gt;:TRAC&lt;t#&gt;:Y:AUTO". </remarks>
    ''' <value> The auto scale command. </value>
    Protected Overridable Property AutoScaleCommand As String

    ''' <summary> Automatic scale. </summary>
    Public Sub AutoScale()
        If Not String.IsNullOrWhiteSpace(Me.SelectCommand) Then
            Me.Session.WriteLine(Me.SelectCommand)
        End If
    End Sub

#End Region

#Region " SELECT "

    ''' <summary> Gets or sets the Select command. </summary>
    ''' <remarks> SCPI: ":CALC&lt;c#&gt;:PAR&lt;t#&gt;:SEL". </remarks>
    ''' <value> The Select command. </value>
    Protected Overridable Property SelectCommand As String

    ''' <summary> Selects this channel as the active trace. </summary>
    Public Sub [Select]()
        If Not String.IsNullOrWhiteSpace(Me.SelectCommand) Then
            Me.Session.WriteLine(Me.SelectCommand)
        End If
    End Sub

#End Region

End Class

