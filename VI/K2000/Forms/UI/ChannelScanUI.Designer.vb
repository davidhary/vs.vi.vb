<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ChannelScanUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._ChannelScanView = New isr.VI.K2002.Forms.ChannelScanView()
        Me.SuspendLayout()
        '
        '_ChannelScanView
        '
        Me._ChannelScanView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ChannelScanView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ChannelScanView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ChannelScanView.Location = New System.Drawing.Point(1, 1)
        Me._ChannelScanView.Name = "_ChannelScanView"
        Me._ChannelScanView.Padding = New System.Windows.Forms.Padding(1)
        Me._ChannelScanView.Size = New System.Drawing.Size(407, 272)
        Me._ChannelScanView.TabIndex = 0
        '
        'ChannelScanUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ChannelScanView)
        Me.Name = "ChannelScanUI"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(409, 274)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _ChannelScanView As ChannelScanView
End Class
