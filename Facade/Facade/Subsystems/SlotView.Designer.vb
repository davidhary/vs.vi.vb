<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SlotView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SlotView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SlotNumberComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SlotNumberNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._CardTypeTextBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._CardTypeTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._SettlingTimeNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SettlingTimeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SettlingTimeUnitLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._SlotNumberComboBoxLabel, Me._SlotNumberNumeric, Me._CardTypeTextBoxLabel, Me._CardTypeTextBox, Me._SettlingTimeNumericLabel, Me._SettlingTimeNumeric, Me._SettlingTimeUnitLabel})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(447, 27)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 1
        Me._SubsystemToolStrip.Text = "Slot Tool Strip"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(42, 24)
        Me._SubsystemSplitButton.Text = "Slot"
        Me._SubsystemSplitButton.ToolTipText = "Select action"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies the settings onto the instrument"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        '
        '_SlotNumberComboBoxLabel
        '
        Me._SlotNumberComboBoxLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._SlotNumberComboBoxLabel.Name = "_SlotNumberComboBoxLabel"
        Me._SlotNumberComboBoxLabel.Size = New System.Drawing.Size(14, 24)
        Me._SlotNumberComboBoxLabel.Text = "#"
        '
        '_SlotNumberNumeric
        '
        Me._SlotNumberNumeric.Name = "_SlotNumberNumeric"
        Me._SlotNumberNumeric.Size = New System.Drawing.Size(41, 24)
        Me._SlotNumberNumeric.Text = "0"
        Me._SlotNumberNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_CardTypeTextBoxLabel
        '
        Me._CardTypeTextBoxLabel.Name = "_CardTypeTextBoxLabel"
        Me._CardTypeTextBoxLabel.Size = New System.Drawing.Size(34, 24)
        Me._CardTypeTextBoxLabel.Text = "Type:"
        '
        '_CardTypeTextBox
        '
        Me._CardTypeTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me._CardTypeTextBox.Name = "_CardTypeTextBox"
        Me._CardTypeTextBox.Size = New System.Drawing.Size(100, 27)
        Me._CardTypeTextBox.ToolTipText = "Card type"
        '
        '_SettlingTimeNumericLabel
        '
        Me._SettlingTimeNumericLabel.Margin = New System.Windows.Forms.Padding(2, 1, 0, 2)
        Me._SettlingTimeNumericLabel.Name = "_SettlingTimeNumericLabel"
        Me._SettlingTimeNumericLabel.Size = New System.Drawing.Size(50, 24)
        Me._SettlingTimeNumericLabel.Text = "Settling:"
        '
        '_SettlingTimeNumeric
        '
        Me._SettlingTimeNumeric.AutoSize = False
        Me._SettlingTimeNumeric.Margin = New System.Windows.Forms.Padding(0, 1, 0, 1)
        Me._SettlingTimeNumeric.Name = "_SettlingTimeNumeric"
        Me._SettlingTimeNumeric.Size = New System.Drawing.Size(45, 25)
        Me._SettlingTimeNumeric.Text = "0"
        Me._SettlingTimeNumeric.ToolTipText = "Relay settling time"
        Me._SettlingTimeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_SettlingTimeUnitLabel
        '
        Me._SettlingTimeUnitLabel.Name = "_SettlingTimeUnitLabel"
        Me._SettlingTimeUnitLabel.Size = New System.Drawing.Size(23, 24)
        Me._SettlingTimeUnitLabel.Text = "ms"
        '
        'SlotView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "SlotView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(449, 29)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SlotNumberComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SettlingTimeNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SettlingTimeNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _CardTypeTextBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _CardTypeTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _SettlingTimeUnitLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SlotNumberNumeric As Core.Controls.ToolStripNumericUpDown
End Class
