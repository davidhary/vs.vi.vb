''' <summary> Defines a Multimeter Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-15 </para>
''' </remarks>
Public MustInherit Class MultimeterSubsystemBase
    Inherits VI.MultimeterSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem, readingAmounts)
        Me.FunctionModeReadWrites.RemoveAt(VI.MultimeterFunctionModes.ResistanceCommonSide)
        MyBase.SupportedFunctionModes = VI.MultimeterFunctionModes.CurrentAC
        For Each kvp As Pith.EnumReadWrite In MyBase.FunctionModeReadWrites
            MyBase.SupportedFunctionModes = MyBase.SupportedFunctionModes Or CType(kvp.EnumValue, VI.MultimeterFunctionModes)
        Next
    End Sub

#End Region

End Class

