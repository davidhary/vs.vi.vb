'---------------------------------------------------------------------------------------------------
' file:		.\Views\ResourceNameInfoEditor.vb
'
' summary:	Resource name information editor class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.EscapeSequencesExtensions
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> Editor for resource name Information. </summary>
''' <remarks>
''' David, 2020-06-07. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class ResourceNameInfoEditor
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Public Sub New()
        MyBase.New
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._ResourceNameInformations = New VI.Pith.ResourceNameInfoCollection
        Me.ResourceFolderLabel.Text = $"Folder: {Me.ResourceNameInformations.DefaultFolderName}"
        Me.ToolTip.SetToolTip(Me.ResourceNamesListBox, "Click to select a resource")
        Me.ToolTip.SetToolTip(Me.ResourceNameTextBox, "Enter or select a resource from the list")
        Me.ToolTip.SetToolTip(Me.TestResourceButton, "Tests is a session can open to an instrument using the selected resource")
        Me.ToolTip.SetToolTip(Me.AddResourceButton, "Adds a resource")
        Me.ToolTip.SetToolTip(Me.RemoveButton, "Removes a resource")
        Me.ToolTip.SetToolTip(Me.BackupButton, "Saves the resource list to a backup file")
        Me.ToolTip.SetToolTip(Me.RestoreButton, "Restores the resource list from the backup file")
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me.ResourceNameInformations.ReadResources()
            Me.ResourceNamesListBox.DataSource = Me.ResourceNameInformations.ResourceNames
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Tests resource button click. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TestResourceButton_Click(sender As Object, e As EventArgs) Handles TestResourceButton.Click
        Dim resourceName As String = Me.ResourceNameTextBox.Text
        If String.IsNullOrEmpty(resourceName) Then
            Me.MessagesTextBox.Text = "Empty resource name. Please enter a resource name in the text box"
        Else
            Using rm As VI.Pith.SessionBase = VI.SessionFactory.Get.Factory.Session
                Dim r As (Success As Boolean, Details As String) = rm.CanCreateSession(resourceName, TimeSpan.FromMilliseconds(250))
                Me.MessagesTextBox.Text = If(r.Success,
                    $"Success! Resource {resourceName} found",
                    $"Failure! Resource {resourceName} not found
{ r.Details}")
            End Using
        End If

    End Sub

    ''' <summary> Referesh list. </summary>
    Private Sub RefereshList()
        Me.ResourceNamesListBox.DataSource = Nothing
        Me.ResourceNamesListBox.Items.Clear()
        Me.ResourceNamesListBox.DataSource = Me.ResourceNameInformations.ResourceNames
    End Sub

    ''' <summary> Removes the button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub RemoveButton_Click(sender As Object, e As EventArgs) Handles RemoveButton.Click
        Dim resourceName As String = Me.ResourceNameTextBox.Text
        If Me.ResourceNameInformations.Contains(resourceName) Then
            Try
                Me.Cursor = Cursors.WaitCursor
                Me.ResourceNameInformations.Remove(resourceName)
                Me.ResourceNameInformations.WriteResources()
                Me.RefereshList()
                Me.MessagesTextBox.Text = $"{resourceName} was removed"
            Catch ex As Exception
                Me.MessagesTextBox.Text = ex.ToFullBlownString
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub

    ''' <summary> Adds a resource button click to 'e'. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub AddResourceButton_Click(sender As Object, e As EventArgs) Handles AddResourceButton.Click
        Dim resourceName As String = Me.ResourceNameTextBox.Text
        If Not Me.ResourceNameInformations.Contains(resourceName) Then
            Try
                Me.Cursor = Cursors.WaitCursor
                Me.ResourceNameInformations.Add(resourceName)
                Me.ResourceNameInformations.WriteResources()
                Me.RefereshList()
                Me.MessagesTextBox.Text = $"{resourceName} was added"
            Catch ex As Exception
                Me.MessagesTextBox.Text = ex.ToFullBlownString
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub

    ''' <summary> Resource names list box selected value changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResourceNamesListBox_SelectedValueChanged(sender As Object, e As EventArgs) Handles ResourceNamesListBox.SelectedValueChanged
        If Not Me.InitializingComponents Then
            Me.ResourceNameTextBox.Text = Me.ResourceNamesListBox.SelectedItem?.ToString
        End If
    End Sub

    ''' <summary> Restore button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub RestoreButton_Click(sender As Object, e As EventArgs) Handles RestoreButton.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.ResourceNameInformations.RestoreResources()
            Me.ResourceNameInformations.WriteResources()
            Me.RefereshList()
            Me.MessagesTextBox.Text = $"Resources restored from backup {Me.ResourceNameInformations.BackupFullFileName}"
        Catch ex As Exception
            Me.MessagesTextBox.Text = ex.ToFullBlownString
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Backup button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub BackupButton_Click(sender As Object, e As EventArgs) Handles BackupButton.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.ResourceNameInformations.BackupResources()
            Me.MessagesTextBox.Text = $"Resources backed up to {Me.ResourceNameInformations.BackupFullFileName}"
        Catch ex As Exception
            Me.MessagesTextBox.Text = ex.ToFullBlownString
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub


#End Region

#Region " RESOURCE NAME INFO "

    ''' <summary> Gets or sets the resource name informations. </summary>
    ''' <value> The resource name informations. </value>
    Public ReadOnly Property ResourceNameInformations As VI.Pith.ResourceNameInfoCollection

#End Region

End Class
