**Test program for instrument control using VISA**

Installation Information

This program installs a program for communicating with instruments using
the ISR Message-Based VISA Library.

**Table of Contents**
-   Microsoft .NET Framework is Required
-   IVI-VISA Runtime is required
-   Uninstall
-   Contact Information
-   Copyrights and Trademarks

**Microsoft .NET Framework (.NETFX) Version Is Required**

[**[Microsoft .NET
Framework]{.underline}**](https://www.microsoft.com/en-us/download/details.aspx?id=49982)
Version 4.7.1 or above must installed before proceeding with this
installation. For the web installer click
[.Net Web Installer](https://www.microsoft.com/en-us/download/details.aspx?id=49981).

**IVI-VISA Is Required**

[IVI VISA](http://www.ivifoundation.org)
VISA is required for accessing devices. The [Keysight](http://www.Keysight.com) implementation of VISA is recommended.

### Contact Information

support[at]IntegratedScientificResources[.]com

**Uninstalling**

Uninstall the program from the Add/Remove Programs applets of the
Control Panel.

**Contact Information**
support[at]IntegratedScientificResources[.]com
**Copyrights and Trademarks**

The ISR logo is a trademark of Integrated Scientific Resources. Windows
and .NET Framework are trademarks of Microsoft Corporation. Other
trademarks are property of their owners.

\(C\) 2004 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

You are hereby granted permission to add your license notice in
accordance with your agreement with ISR.

This software was developed and tested using Microsoft® [Visual Studio](https://www.visualstudIO.com/) 2019.  

Open source used by this software is described and licensed on the
following sites:

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
