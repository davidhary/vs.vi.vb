'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\DigitalOutputLineView.vb
'
' summary:	Digital Output Line View class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.Core.EnumExtensions
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.Core.SplitExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> An Digital Output Line View. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class DigitalOutputLineView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new DigitalOutputLineView. </summary>
    ''' <returns> A DigitalOutputLineView. </returns>
    Public Shared Function Create() As DigitalOutputLineView
        Dim view As DigitalOutputLineView = Nothing
        Try
            view = New DigitalOutputLineView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    Private _LineIdentity As String

    ''' <summary> Gets or sets the line identity. </summary>
    ''' <value> The line identity. </value>
    Public Property LineIdentity As String
        Get
            Return Me._LineIdentity
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LineIdentity) Then
                Me._LineIdentity = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the name of the line. </summary>
    ''' <value> The name of the line. </value>
    Public Property LineName As String
        Get
            Return Me._SubsystemSplitButton.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LineName) Then
                Me._SubsystemSplitButton.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the line number. </summary>
    ''' <value> The line number. </value>
    Public Property LineNumber As Integer
        Get
            Return Me._LineNumberBox.ValueAsInt32
        End Get
        Set(value As Integer)
            If value <> Me.LineNumber Then
                Me._LineNumberBox.ValueAsInt32 = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the line level. </summary>
    ''' <value> The line level. </value>
    Public Property LineLevel As Integer
        Get
            Return Me._LineLevelNumberBox.ValueAsInt32
        End Get
        Set(value As Integer)
            If value <> Me.LineLevel Then
                Me._LineLevelNumberBox.ValueAsInt32 = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the DigitalOutput source. </summary>
    ''' <value> The DigitalOutput source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ActiveLevel As DigitalActiveLevels
        Get
            Return CType(Me._ActiveLevelComboBox.ComboBox.SelectedValue, DigitalActiveLevels)
        End Get
        Set(value As DigitalActiveLevels)
            If value <> Me.ActiveLevel Then
                Me._ActiveLevelComboBox.ComboBox.SelectedItem = value.ValueNamePair
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the width of the pulse. </summary>
    ''' <value> The width of the pulse. </value>
    Public Property PulseWidth As TimeSpan
        Get
            Return TimeSpan.FromMilliseconds(Me._PulseWidthNumberBox.ValueAsDouble)
        End Get
        Set(value As TimeSpan)
            If value <> Me.PulseWidth Then
                Me._PulseWidthNumberBox.ValueAsDouble = value.TotalMilliseconds
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Toggle line level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    Public Sub ToggleLineLevel()
        Me.ReadActiveLevel()
        Me.WriteLineLevel(If(Me.LineLevel = 0, 1, 0))
    End Sub

    ''' <summary> Reads line level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Sub ReadLineLevel()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading {Me._SubsystemSplitButton.Text}{Me.LineNumber} line level" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me.DigitalOutputSubsystem.QueryLineLevel(Me.LineNumber)
            Me.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Writes the line level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Sub WriteLineLevel(ByVal value As Integer)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} write {Me._SubsystemSplitButton.Text}{Me.LineNumber}  line level" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me.DigitalOutputSubsystem.ApplyLineLevel(Me.LineNumber, value)
            Me.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Pulse line level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="duration"> The duration. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Sub PulseLineLevel(ByVal value As Integer, ByVal duration As TimeSpan)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} output {Me._SubsystemSplitButton.Text}{Me.LineNumber} pulse" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me.DigitalOutputSubsystem.Pulse(Me.LineNumber, value, duration)
            Me.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads active level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Sub ReadActiveLevel()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} read {Me._SubsystemSplitButton.Text}{Me.LineNumber}  active level" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me.DigitalOutputSubsystem.QueryDigitalActiveLevel(Me.LineNumber)
            Me.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Writes the active level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Sub WriteActiveLevel()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} write {Me._SubsystemSplitButton.Text}{Me.LineNumber}  active level" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me.DigitalOutputSubsystem.ApplyDigitalActiveLevel(Me.LineNumber, Me.ActiveLevel)
            Me.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the settings onto the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying {Me._SubsystemSplitButton.Text}{Me.LineNumber}  settings" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me.DigitalOutputSubsystem.ApplyDigitalActiveLevel(Me.LineNumber, Me.ActiveLevel)
            Me.DigitalOutputSubsystem.ApplyLineLevel(Me.LineNumber, Me.LineLevel)
            Me.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading  {Me._SubsystemSplitButton.Text}{Me.LineNumber}  settings" : Me.PublishInfo($"{activity};. ")
            Me.DigitalOutputSubsystem.QueryDigitalActiveLevel(Me.LineNumber)
            Me.DigitalOutputSubsystem.QueryLineLevel(Me.LineNumber)
            Me.ApplyPropertyChanged(Me.DigitalOutputSubsystem)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(DigitalOutputLineView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DIGITAL OUTPUT SUBSYSTEM "

    ''' <summary> Gets or sets the DigitalOutput subsystem. </summary>
    ''' <value> The DigitalOutput subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DigitalOutputSubsystem As DigitalOutputSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-11-13. </remarks>
    ''' <param name="subsystem">  The subsystem. </param>
    ''' <param name="lineNumber"> The line number. </param>
    Public Sub BindSubsystem(ByVal subsystem As DigitalOutputSubsystemBase, ByVal lineNumber As Integer)
        If Me.DigitalOutputSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.DigitalOutputSubsystem)
            Me._DigitalOutputSubsystem = Nothing
        End If
        Me.LineNumber = lineNumber
        Me._DigitalOutputSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.DigitalOutputSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As DigitalOutputSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.DigitalOutputSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(DigitalOutputSubsystemBase.SupportedDigitalActiveLevels))
            DigitalOutputLineView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.DigitalOutputSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As DigitalOutputSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(DigitalOutputSubsystemBase.DigitalOutputLines))
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As DigitalOutputSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryDigitalOutputLines()
        subsystem.StopElapsedStopwatch()
    End Sub

    Private Const _NotSpecified As Integer = -2
    Private Const _NotSet As Integer = -1

    ''' <summary> Handle the Calculate subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As DigitalOutputSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(DigitalOutputSubsystemBase.DigitalOutputLines)
                If subsystem.DigitalOutputLines.Contains(Me.LineNumber) Then
                    Me.LineLevel = subsystem.DigitalOutputLines(Me.LineNumber).LineLevel.GetValueOrDefault(_NotSet)
                    Me.ActiveLevel = subsystem.DigitalOutputLines(Me.LineNumber).ActiveLevel
                Else
                    Me.LineLevel = _NotSpecified
                End If

            Case NameOf(DigitalOutputSubsystemBase.SupportedDigitalActiveLevels)
                Me.InitializingComponents = True
                Me._ActiveLevelComboBox.ComboBox.ListDigitalActiveLevels(subsystem.SupportedDigitalActiveLevels)
                Me.InitializingComponents = False
                If subsystem.DigitalOutputLines.Contains(Me.LineNumber) Then
                    Me.ActiveLevel = subsystem.DigitalOutputLines(Me.LineNumber).ActiveLevel
                Else
                    Me.LineLevel = _NotSpecified
                    Me.ActiveLevel = DigitalActiveLevels.None
                End If
                'If subsystem.ac.HasValue AndAlso Me._SourceComboBox.ComboBox.Items.Count > 0 Then
                ' Me._SourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair
                'End If
        End Select
    End Sub

    ''' <summary> DigitalOutput subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DigitalOutputSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.DigitalOutputSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DigitalOutputSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DigitalOutputSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.DigitalOutputSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Read Button click. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadButton_Click(sender As Object, e As EventArgs) Handles _ReadButton.Click
        Me.ReadLineLevel()
    End Sub

    ''' <summary> Write Button click. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub WriteButton_Click(sender As Object, e As EventArgs) Handles _WriteButton.Click
        Me.WriteLineLevel(Me.LineLevel)
    End Sub

    ''' <summary> Toggle button click. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToggleButton_Click(sender As Object, e As EventArgs) Handles _ToggleButton.Click
        Me.ToggleLineLevel()
    End Sub

    ''' <summary> Pulse button click. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PulseButton_Click(sender As Object, e As EventArgs) Handles _PulseButton.Click
        Me.PulseLineLevel(If(Me.LineLevel = 0, 1, 0), Me.PulseWidth)
    End Sub

    ''' <summary> Applies the settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
