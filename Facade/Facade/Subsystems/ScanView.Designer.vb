<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ScanView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ScanView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplyInternalScanListMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ApplyInternalScanFunctionListMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SelectInternalScanListMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReleaseInternalScanListMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CandidateScanListComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._InternalScanListLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ScanListFunctionLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ClosedChannelsLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._CandidateScanListComboBox, Me._InternalScanListLabel, Me._ScanListFunctionLabel, Me._ClosedChannelsLabel})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(422, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 0
        Me._SubsystemToolStrip.Text = "ChannelToolStrip"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplyInternalScanListMenuItem, Me._ApplyInternalScanFunctionListMenuItem, Me._SelectInternalScanListMenuItem, Me._ReleaseInternalScanListMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(48, 22)
        Me._SubsystemSplitButton.Text = "Scan"
        Me._SubsystemSplitButton.ToolTipText = "Select action"
        '
        '_ApplyInternalScanListMenuItem
        '
        Me._ApplyInternalScanListMenuItem.Name = "_ApplyInternalScanListMenuItem"
        Me._ApplyInternalScanListMenuItem.Size = New System.Drawing.Size(246, 22)
        Me._ApplyInternalScanListMenuItem.Text = "Apply Internal Scan List"
        Me._ApplyInternalScanListMenuItem.ToolTipText = "Applies the list as an internal scan list"
        '
        '_ApplyInternalScanFunctionListMenuItem
        '
        Me._ApplyInternalScanFunctionListMenuItem.Name = "_ApplyInternalScanFunctionListMenuItem"
        Me._ApplyInternalScanFunctionListMenuItem.Size = New System.Drawing.Size(246, 22)
        Me._ApplyInternalScanFunctionListMenuItem.Text = "Apply Internal Scan Function List"
        Me._ApplyInternalScanFunctionListMenuItem.ToolTipText = "Applies the list as an internal function scan list"
        '
        '_SelectInternalScanListMenuItem
        '
        Me._SelectInternalScanListMenuItem.Name = "_SelectInternalScanListMenuItem"
        Me._SelectInternalScanListMenuItem.Size = New System.Drawing.Size(246, 22)
        Me._SelectInternalScanListMenuItem.Text = "Select Internal Scan List"
        Me._SelectInternalScanListMenuItem.ToolTipText = "Select the list as an internal scan list"
        '
        '_ReleaseInternalScanListMenuItem
        '
        Me._ReleaseInternalScanListMenuItem.Name = "_ReleaseInternalScanListMenuItem"
        Me._ReleaseInternalScanListMenuItem.Size = New System.Drawing.Size(246, 22)
        Me._ReleaseInternalScanListMenuItem.Text = "Release Internal Scan List"
        Me._ReleaseInternalScanListMenuItem.ToolTipText = "Releases the internal scan list"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(246, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        '
        '_CandidateScanListComboBox
        '
        Me._CandidateScanListComboBox.AutoSize = False
        Me._CandidateScanListComboBox.Items.AddRange(New Object() {"(@1:3)"})
        Me._CandidateScanListComboBox.Name = "_CandidateScanListComboBox"
        Me._CandidateScanListComboBox.Size = New System.Drawing.Size(140, 23)
        Me._CandidateScanListComboBox.Text = "(@1:3)"
        Me._CandidateScanListComboBox.ToolTipText = "Candidate scan list in the format:  (@<card>:<relay>)"
        '
        '_InternalScanListLabel
        '
        Me._InternalScanListLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._InternalScanListLabel.Name = "_InternalScanListLabel"
        Me._InternalScanListLabel.Size = New System.Drawing.Size(55, 22)
        Me._InternalScanListLabel.Text = "Internal ?"
        Me._InternalScanListLabel.ToolTipText = "Internal scan list"
        '
        '_ScanListFunctionLabel
        '
        Me._ScanListFunctionLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._ScanListFunctionLabel.Name = "_ScanListFunctionLabel"
        Me._ScanListFunctionLabel.Size = New System.Drawing.Size(62, 22)
        Me._ScanListFunctionLabel.Text = "Function ?"
        Me._ScanListFunctionLabel.ToolTipText = "Function scan list"
        '
        '_ClosedChannelsLabel
        '
        Me._ClosedChannelsLabel.Name = "_ClosedChannelsLabel"
        Me._ClosedChannelsLabel.Size = New System.Drawing.Size(51, 22)
        Me._ClosedChannelsLabel.Text = "Closed ?"
        Me._ClosedChannelsLabel.ToolTipText = "Closed channel(s)"
        '
        'ScanView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "ScanView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(424, 27)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _InternalScanListLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ScanListFunctionLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ClosedChannelsLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ReleaseInternalScanListMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplyInternalScanListMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ApplyInternalScanFunctionListMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SelectInternalScanListMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _CandidateScanListComboBox As Windows.Forms.ToolStripComboBox
End Class
