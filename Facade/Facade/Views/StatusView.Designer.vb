Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class StatusView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StatusView))
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ApplicationDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me.EditSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SessionSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._DeviceSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UserInterfaceSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UIBaseSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TraceMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TraceLogLevelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TraceLogLevelComboBox = New isr.Core.Controls.ToolStripComboBox()
        Me._TraceShowLevelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TraceShowLevelComboBox = New isr.Core.Controls.ToolStripComboBox()
        Me._SessionNotificationLevelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SessionNotificationLevelComboBox = New isr.Core.Controls.ToolStripComboBox()
        Me._UsingStatusSubsystemMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._DeviceDropDownButton = New isr.Core.Controls.ToolStripDropDownButton()
        Me._ResetMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearInterfaceMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ClearDeviceMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ResetKnownStateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearExecutionStateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InitKnownStateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearErrorReportMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadDeviceErrorsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadTerminalsStateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SessionDownDownButton = New isr.Core.Controls.ToolStripDropDownButton()
        Me._ReadTerminationMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadTerminationTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._ReadTerminationEnabledMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._WriteTerminationMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._WriteTerminationTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._SessionTimeoutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SessionTimeoutTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._StoreTimeoutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RestoreTimeoutMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SessionReadStatusByteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SessionReadStandardEventRegisterMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SendBusTriggerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PollSplitButton = New isr.Core.Controls.ToolStripDropDownButton()
        Me._PollMessageStatusBitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PollMessageStatusBitTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._PollCommandMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PollWriteCommandTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._PollIntervalMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PollIntervalTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._PollEnabledMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._PollAutoReadMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._PollSendMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ServiceRequestSplitButton = New isr.Core.Controls.ToolStripDropDownButton()
        Me._ServiceRequestBitmaskMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ServiceRequestBitMaskTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._ServiceRequestEnableCommandMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ServiceRequestEnableCommandTextBox = New isr.Core.Controls.ToolStripTextBox()
        Me._ProgramServiceRequestEnableBitmaskMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ToggleVisaEventHandlerMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ServiceRequestHandlerAddRemoveMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ServiceRequestAutoReadMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me.EditResourceNamesMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._ToolStrip.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplicationDropDownButton, Me._DeviceDropDownButton, Me._SessionDownDownButton, Me._PollSplitButton, Me._ServiceRequestSplitButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(474, 31)
        Me._ToolStrip.TabIndex = 12
        '
        '_ApplicationDropDownButton
        '
        Me._ApplicationDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ApplicationDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditSettingsToolStripMenuItem, Me._TraceMenuItem, Me._SessionNotificationLevelMenuItem, Me._UsingStatusSubsystemMenuItem, Me.EditResourceNamesMenuItem})
        Me._ApplicationDropDownButton.Image = CType(resources.GetObject("_ApplicationDropDownButton.Image"), System.Drawing.Image)
        Me._ApplicationDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ApplicationDropDownButton.Name = "_ApplicationDropDownButton"
        Me._ApplicationDropDownButton.Size = New System.Drawing.Size(46, 28)
        Me._ApplicationDropDownButton.Text = "App"
        Me._ApplicationDropDownButton.ToolTipText = "Application options"
        '
        'EditSettingsToolStripMenuItem
        '
        Me.EditSettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SessionSettingsMenuItem, Me._DeviceSettingsMenuItem, Me._UserInterfaceSettingsMenuItem, Me._UIBaseSettingsMenuItem})
        Me.EditSettingsToolStripMenuItem.Name = "EditSettingsToolStripMenuItem"
        Me.EditSettingsToolStripMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.EditSettingsToolStripMenuItem.Text = "Edit Settings"
        '
        '_SessionSettingsMenuItem
        '
        Me._SessionSettingsMenuItem.Name = "_SessionSettingsMenuItem"
        Me._SessionSettingsMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._SessionSettingsMenuItem.Text = "Session..."
        Me._SessionSettingsMenuItem.ToolTipText = "Opens session settings dialog"
        '
        '_DeviceSettingsMenuItem
        '
        Me._DeviceSettingsMenuItem.Name = "_DeviceSettingsMenuItem"
        Me._DeviceSettingsMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._DeviceSettingsMenuItem.Text = "Device..."
        Me._DeviceSettingsMenuItem.ToolTipText = "Opens device settings editor dialog"
        '
        '_UserInterfaceSettingsMenuItem
        '
        Me._UserInterfaceSettingsMenuItem.Name = "_UserInterfaceSettingsMenuItem"
        Me._UserInterfaceSettingsMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._UserInterfaceSettingsMenuItem.Text = "UI..."
        Me._UserInterfaceSettingsMenuItem.ToolTipText = "Opens User Interface settings editor dialog"
        '
        '_UIBaseSettingsMenuItem
        '
        Me._UIBaseSettingsMenuItem.Name = "_UIBaseSettingsMenuItem"
        Me._UIBaseSettingsMenuItem.Size = New System.Drawing.Size(134, 22)
        Me._UIBaseSettingsMenuItem.Text = "UI Base..."
        Me._UIBaseSettingsMenuItem.ToolTipText = "Edits the user interface base settings"
        '
        '_TraceMenuItem
        '
        Me._TraceMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TraceLogLevelMenuItem, Me._TraceShowLevelMenuItem})
        Me._TraceMenuItem.Name = "_TraceMenuItem"
        Me._TraceMenuItem.Size = New System.Drawing.Size(256, 22)
        Me._TraceMenuItem.Text = "Select Trace Levels"
        Me._TraceMenuItem.ToolTipText = "Opens the trace menus"
        '
        '_TraceLogLevelMenuItem
        '
        Me._TraceLogLevelMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TraceLogLevelComboBox})
        Me._TraceLogLevelMenuItem.Name = "_TraceLogLevelMenuItem"
        Me._TraceLogLevelMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._TraceLogLevelMenuItem.Text = "Trace Log Level"
        Me._TraceLogLevelMenuItem.ToolTipText = "Selects trace level for logging"
        '
        '_TraceLogLevelComboBox
        '
        Me._TraceLogLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._TraceLogLevelComboBox.Name = "_TraceLogLevelComboBox"
        Me._TraceLogLevelComboBox.Size = New System.Drawing.Size(100, 22)
        Me._TraceLogLevelComboBox.ToolTipText = "Selects trace level for logging"
        '
        '_TraceShowLevelMenuItem
        '
        Me._TraceShowLevelMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TraceShowLevelComboBox})
        Me._TraceShowLevelMenuItem.Name = "_TraceShowLevelMenuItem"
        Me._TraceShowLevelMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._TraceShowLevelMenuItem.Text = "Display Trace Level"
        Me._TraceShowLevelMenuItem.ToolTipText = "Selects trace level for display"
        '
        '_TraceShowLevelComboBox
        '
        Me._TraceShowLevelComboBox.Name = "_TraceShowLevelComboBox"
        Me._TraceShowLevelComboBox.Size = New System.Drawing.Size(100, 22)
        Me._TraceShowLevelComboBox.ToolTipText = "Selects trace level for display"
        '
        '_SessionNotificationLevelMenuItem
        '
        Me._SessionNotificationLevelMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SessionNotificationLevelComboBox})
        Me._SessionNotificationLevelMenuItem.Name = "_SessionNotificationLevelMenuItem"
        Me._SessionNotificationLevelMenuItem.Size = New System.Drawing.Size(256, 22)
        Me._SessionNotificationLevelMenuItem.Text = "Session Notification Level"
        Me._SessionNotificationLevelMenuItem.ToolTipText = "Shows the session notification level selector"
        '
        '_SessionNotificationLevelComboBox
        '
        Me._SessionNotificationLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SessionNotificationLevelComboBox.Name = "_SessionNotificationLevelComboBox"
        Me._SessionNotificationLevelComboBox.Size = New System.Drawing.Size(100, 22)
        Me._SessionNotificationLevelComboBox.ToolTipText = "Select the session notification level"
        '
        '_UsingStatusSubsystemMenuItem
        '
        Me._UsingStatusSubsystemMenuItem.CheckOnClick = True
        Me._UsingStatusSubsystemMenuItem.Name = "_UsingStatusSubsystemMenuItem"
        Me._UsingStatusSubsystemMenuItem.Size = New System.Drawing.Size(256, 22)
        Me._UsingStatusSubsystemMenuItem.Text = "Using Status Subsystem Only"
        Me._UsingStatusSubsystemMenuItem.ToolTipText = "Toggle to use status or all subsystems"
        '
        '_DeviceDropDownButton
        '
        Me._DeviceDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._DeviceDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ResetMenuItem, Me._ClearErrorReportMenuItem, Me._ReadDeviceErrorsMenuItem, Me._ReadTerminalsStateMenuItem})
        Me._DeviceDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._DeviceDropDownButton.Name = "_DeviceDropDownButton"
        Me._DeviceDropDownButton.Size = New System.Drawing.Size(62, 28)
        Me._DeviceDropDownButton.Text = "Device"
        Me._DeviceDropDownButton.ToolTipText = "Device resets, Service Requests and trace management"
        '
        '_ResetMenuItem
        '
        Me._ResetMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ClearInterfaceMenuItem, Me._ClearDeviceMenuItem, Me._ResetKnownStateMenuItem, Me._ClearExecutionStateMenuItem, Me._InitKnownStateMenuItem})
        Me._ResetMenuItem.Name = "_ResetMenuItem"
        Me._ResetMenuItem.Size = New System.Drawing.Size(205, 22)
        Me._ResetMenuItem.Text = "Reset..."
        Me._ResetMenuItem.ToolTipText = "Opens the reset menus"
        '
        '_ClearInterfaceMenuItem
        '
        Me._ClearInterfaceMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ClearInterfaceMenuItem.Name = "_ClearInterfaceMenuItem"
        Me._ClearInterfaceMenuItem.Size = New System.Drawing.Size(242, 22)
        Me._ClearInterfaceMenuItem.Text = "Clear Interface"
        Me._ClearInterfaceMenuItem.ToolTipText = "Issues an interface clear command"
        '
        '_ClearDeviceMenuItem
        '
        Me._ClearDeviceMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ClearDeviceMenuItem.Name = "_ClearDeviceMenuItem"
        Me._ClearDeviceMenuItem.Size = New System.Drawing.Size(242, 22)
        Me._ClearDeviceMenuItem.Text = "Clear Device (SDC)"
        Me._ClearDeviceMenuItem.ToolTipText = "Issues Selective Device Clear"
        '
        '_ResetKnownStateMenuItem
        '
        Me._ResetKnownStateMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResetKnownStateMenuItem.Name = "_ResetKnownStateMenuItem"
        Me._ResetKnownStateMenuItem.Size = New System.Drawing.Size(242, 22)
        Me._ResetKnownStateMenuItem.Text = "Reset Known State (RST)"
        Me._ResetKnownStateMenuItem.ToolTipText = "Issues *RST"
        '
        '_ClearExecutionStateMenuItem
        '
        Me._ClearExecutionStateMenuItem.Name = "_ClearExecutionStateMenuItem"
        Me._ClearExecutionStateMenuItem.Size = New System.Drawing.Size(242, 22)
        Me._ClearExecutionStateMenuItem.Text = "Clear Execution State (CLS)"
        Me._ClearExecutionStateMenuItem.ToolTipText = "Clears execution state (CLS)"
        '
        '_InitKnownStateMenuItem
        '
        Me._InitKnownStateMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InitKnownStateMenuItem.Name = "_InitKnownStateMenuItem"
        Me._InitKnownStateMenuItem.Size = New System.Drawing.Size(242, 22)
        Me._InitKnownStateMenuItem.Text = "Initialize Known State"
        Me._InitKnownStateMenuItem.ToolTipText = "Issues *RST, CLear and initialize to custom known state"
        '
        '_ClearErrorReportMenuItem
        '
        Me._ClearErrorReportMenuItem.Name = "_ClearErrorReportMenuItem"
        Me._ClearErrorReportMenuItem.Size = New System.Drawing.Size(205, 22)
        Me._ClearErrorReportMenuItem.Text = "Clear Error Report"
        Me._ClearErrorReportMenuItem.ToolTipText = "Clears the error report"
        '
        '_ReadDeviceErrorsMenuItem
        '
        Me._ReadDeviceErrorsMenuItem.Name = "_ReadDeviceErrorsMenuItem"
        Me._ReadDeviceErrorsMenuItem.Size = New System.Drawing.Size(205, 22)
        Me._ReadDeviceErrorsMenuItem.Text = "Read Device Errors"
        Me._ReadDeviceErrorsMenuItem.ToolTipText = "Reads device errors"
        '
        '_ReadTerminalsStateMenuItem
        '
        Me._ReadTerminalsStateMenuItem.Name = "_ReadTerminalsStateMenuItem"
        Me._ReadTerminalsStateMenuItem.Size = New System.Drawing.Size(205, 22)
        Me._ReadTerminalsStateMenuItem.Text = "Read Terminals State"
        '
        '_SessionDownDownButton
        '
        Me._SessionDownDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SessionDownDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadTerminationMenuItem, Me._WriteTerminationMenuItem, Me._SessionTimeoutMenuItem, Me._SessionReadStatusByteMenuItem, Me._SessionReadStandardEventRegisterMenuItem, Me._SendBusTriggerMenuItem})
        Me._SessionDownDownButton.Image = CType(resources.GetObject("_SessionDownDownButton.Image"), System.Drawing.Image)
        Me._SessionDownDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SessionDownDownButton.Name = "_SessionDownDownButton"
        Me._SessionDownDownButton.Size = New System.Drawing.Size(67, 28)
        Me._SessionDownDownButton.Text = "Session"
        Me._SessionDownDownButton.ToolTipText = "Select Session Options"
        '
        '_ReadTerminationMenuItem
        '
        Me._ReadTerminationMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadTerminationTextBox, Me._ReadTerminationEnabledMenuItem})
        Me._ReadTerminationMenuItem.Name = "_ReadTerminationMenuItem"
        Me._ReadTerminationMenuItem.Size = New System.Drawing.Size(257, 22)
        Me._ReadTerminationMenuItem.Text = "Read Termination"
        Me._ReadTerminationMenuItem.ToolTipText = "Read termination. Typically Line Feed (10)"
        '
        '_ReadTerminationTextBox
        '
        Me._ReadTerminationTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadTerminationTextBox.Name = "_ReadTerminationTextBox"
        Me._ReadTerminationTextBox.Size = New System.Drawing.Size(100, 22)
        Me._ReadTerminationTextBox.ToolTipText = "Read termination in Hex"
        '
        '_ReadTerminationEnabledMenuItem
        '
        Me._ReadTerminationEnabledMenuItem.CheckOnClick = True
        Me._ReadTerminationEnabledMenuItem.Name = "_ReadTerminationEnabledMenuItem"
        Me._ReadTerminationEnabledMenuItem.Size = New System.Drawing.Size(160, 22)
        Me._ReadTerminationEnabledMenuItem.Text = "Enabled"
        Me._ReadTerminationEnabledMenuItem.ToolTipText = "Checked to enable read termination"
        '
        '_WriteTerminationMenuItem
        '
        Me._WriteTerminationMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._WriteTerminationTextBox})
        Me._WriteTerminationMenuItem.Name = "_WriteTerminationMenuItem"
        Me._WriteTerminationMenuItem.Size = New System.Drawing.Size(257, 22)
        Me._WriteTerminationMenuItem.Text = "Write Termination"
        Me._WriteTerminationMenuItem.ToolTipText = "Write termination; typically \n (line feed)"
        '
        '_WriteTerminationTextBox
        '
        Me._WriteTerminationTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._WriteTerminationTextBox.Name = "_WriteTerminationTextBox"
        Me._WriteTerminationTextBox.Size = New System.Drawing.Size(100, 22)
        Me._WriteTerminationTextBox.ToolTipText = "Write termination escape sequence, e.g., \n\r"
        '
        '_SessionTimeoutMenuItem
        '
        Me._SessionTimeoutMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SessionTimeoutTextBox, Me._StoreTimeoutMenuItem, Me._RestoreTimeoutMenuItem})
        Me._SessionTimeoutMenuItem.Name = "_SessionTimeoutMenuItem"
        Me._SessionTimeoutMenuItem.Size = New System.Drawing.Size(257, 22)
        Me._SessionTimeoutMenuItem.Text = "Timeout"
        '
        '_SessionTimeoutTextBox
        '
        Me._SessionTimeoutTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SessionTimeoutTextBox.Name = "_SessionTimeoutTextBox"
        Me._SessionTimeoutTextBox.Size = New System.Drawing.Size(100, 22)
        Me._SessionTimeoutTextBox.ToolTipText = "Timeout in seconds"
        '
        '_StoreTimeoutMenuItem
        '
        Me._StoreTimeoutMenuItem.Name = "_StoreTimeoutMenuItem"
        Me._StoreTimeoutMenuItem.Size = New System.Drawing.Size(172, 22)
        Me._StoreTimeoutMenuItem.Text = "Store and Set"
        Me._StoreTimeoutMenuItem.ToolTipText = "Pushes the previous timeout onto the stack and set a new timeout value"
        '
        '_RestoreTimeoutMenuItem
        '
        Me._RestoreTimeoutMenuItem.Name = "_RestoreTimeoutMenuItem"
        Me._RestoreTimeoutMenuItem.Size = New System.Drawing.Size(172, 22)
        Me._RestoreTimeoutMenuItem.Text = "Restore and Set"
        Me._RestoreTimeoutMenuItem.ToolTipText = "Pulls the last timeout from the stack and sets it"
        '
        '_SessionReadStatusByteMenuItem
        '
        Me._SessionReadStatusByteMenuItem.Name = "_SessionReadStatusByteMenuItem"
        Me._SessionReadStatusByteMenuItem.Size = New System.Drawing.Size(257, 22)
        Me._SessionReadStatusByteMenuItem.Text = "Read Status Register"
        Me._SessionReadStatusByteMenuItem.ToolTipText = "Reads the session status byte"
        '
        '_SessionReadStandardEventRegisterMenuItem
        '
        Me._SessionReadStandardEventRegisterMenuItem.Name = "_SessionReadStandardEventRegisterMenuItem"
        Me._SessionReadStandardEventRegisterMenuItem.Size = New System.Drawing.Size(257, 22)
        Me._SessionReadStandardEventRegisterMenuItem.Text = "Read Standard Event Register"
        Me._SessionReadStandardEventRegisterMenuItem.ToolTipText = "Reads the standard event register"
        '
        '_SendBusTriggerMenuItem
        '
        Me._SendBusTriggerMenuItem.Name = "_SendBusTriggerMenuItem"
        Me._SendBusTriggerMenuItem.Size = New System.Drawing.Size(257, 22)
        Me._SendBusTriggerMenuItem.Text = "Send Bus Trigger"
        Me._SendBusTriggerMenuItem.ToolTipText = "Sneds a bus trigger to the instrument"
        '
        '_PollSplitButton
        '
        Me._PollSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._PollSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PollMessageStatusBitMenuItem, Me._PollCommandMenuItem, Me._PollIntervalMenuItem, Me._PollEnabledMenuItem, Me._PollAutoReadMenuItem, Me._PollSendMenuItem})
        Me._PollSplitButton.Image = CType(resources.GetObject("_PollSplitButton.Image"), System.Drawing.Image)
        Me._PollSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PollSplitButton.Name = "_PollSplitButton"
        Me._PollSplitButton.Size = New System.Drawing.Size(45, 28)
        Me._PollSplitButton.Text = "Poll"
        '
        '_PollMessageStatusBitMenuItem
        '
        Me._PollMessageStatusBitMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PollMessageStatusBitTextBox})
        Me._PollMessageStatusBitMenuItem.Name = "_PollMessageStatusBitMenuItem"
        Me._PollMessageStatusBitMenuItem.Size = New System.Drawing.Size(193, 22)
        Me._PollMessageStatusBitMenuItem.Text = "Bitmask:"
        Me._PollMessageStatusBitMenuItem.ToolTipText = "Message is flagged as received when status byte message bit  is on"
        '
        '_PollMessageStatusBitTextBox
        '
        Me._PollMessageStatusBitTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PollMessageStatusBitTextBox.Name = "_PollMessageStatusBitTextBox"
        Me._PollMessageStatusBitTextBox.Size = New System.Drawing.Size(100, 22)
        Me._PollMessageStatusBitTextBox.ToolTipText = "Message status bit used to detect if a message is available for reading"
        '
        '_PollCommandMenuItem
        '
        Me._PollCommandMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PollWriteCommandTextBox})
        Me._PollCommandMenuItem.Name = "_PollCommandMenuItem"
        Me._PollCommandMenuItem.Size = New System.Drawing.Size(193, 22)
        Me._PollCommandMenuItem.Text = "Command:"
        Me._PollCommandMenuItem.ToolTipText = "Click Send Command to send this command to the instrument"
        '
        '_PollWriteCommandTextBox
        '
        Me._PollWriteCommandTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PollWriteCommandTextBox.Name = "_PollWriteCommandTextBox"
        Me._PollWriteCommandTextBox.Size = New System.Drawing.Size(100, 22)
        Me._PollWriteCommandTextBox.Text = "*IDN?\n"
        Me._PollWriteCommandTextBox.ToolTipText = "Command to write"
        '
        '_PollIntervalMenuItem
        '
        Me._PollIntervalMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PollIntervalTextBox})
        Me._PollIntervalMenuItem.Name = "_PollIntervalMenuItem"
        Me._PollIntervalMenuItem.Size = New System.Drawing.Size(193, 22)
        Me._PollIntervalMenuItem.Text = "Interval:"
        '
        '_PollIntervalTextBox
        '
        Me._PollIntervalTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PollIntervalTextBox.Name = "_PollIntervalTextBox"
        Me._PollIntervalTextBox.Size = New System.Drawing.Size(100, 22)
        Me._PollIntervalTextBox.ToolTipText = "Sets the polling interval in seconds"
        '
        '_PollEnabledMenuItem
        '
        Me._PollEnabledMenuItem.Name = "_PollEnabledMenuItem"
        Me._PollEnabledMenuItem.Size = New System.Drawing.Size(193, 22)
        Me._PollEnabledMenuItem.Text = "Press to Start"
        Me._PollEnabledMenuItem.ToolTipText = "Check to enable polling the instrument service request on a timer"
        '
        '_PollAutoReadMenuItem
        '
        Me._PollAutoReadMenuItem.CheckOnClick = True
        Me._PollAutoReadMenuItem.Name = "_PollAutoReadMenuItem"
        Me._PollAutoReadMenuItem.Size = New System.Drawing.Size(193, 22)
        Me._PollAutoReadMenuItem.Text = "Auto Read Enabled"
        Me._PollAutoReadMenuItem.ToolTipText = "Reads upon detecting the message available bit"
        '
        '_PollSendMenuItem
        '
        Me._PollSendMenuItem.Name = "_PollSendMenuItem"
        Me._PollSendMenuItem.Size = New System.Drawing.Size(193, 22)
        Me._PollSendMenuItem.Text = "Send Command"
        Me._PollSendMenuItem.ToolTipText = "Click to send the poll command.  A command can also be sent from the Session pane" &
    "l."
        '
        '_ServiceRequestSplitButton
        '
        Me._ServiceRequestSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ServiceRequestSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServiceRequestBitmaskMenuItem, Me._ServiceRequestEnableCommandMenuItem, Me._ProgramServiceRequestEnableBitmaskMenuItem, Me._ToggleVisaEventHandlerMenuItem, Me._ServiceRequestHandlerAddRemoveMenuItem, Me._ServiceRequestAutoReadMenuItem})
        Me._ServiceRequestSplitButton.Image = CType(resources.GetObject("_ServiceRequestSplitButton.Image"), System.Drawing.Image)
        Me._ServiceRequestSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ServiceRequestSplitButton.Name = "_ServiceRequestSplitButton"
        Me._ServiceRequestSplitButton.Size = New System.Drawing.Size(46, 28)
        Me._ServiceRequestSplitButton.Text = "SRQ"
        '
        '_ServiceRequestBitmaskMenuItem
        '
        Me._ServiceRequestBitmaskMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServiceRequestBitMaskTextBox})
        Me._ServiceRequestBitmaskMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ServiceRequestBitmaskMenuItem.Name = "_ServiceRequestBitmaskMenuItem"
        Me._ServiceRequestBitmaskMenuItem.Size = New System.Drawing.Size(232, 22)
        Me._ServiceRequestBitmaskMenuItem.Text = "Events Enable Bitmask:"
        Me._ServiceRequestBitmaskMenuItem.ToolTipText = "Specifies which instrument events invoke service request"
        '
        '_ServiceRequestBitMaskTextBox
        '
        Me._ServiceRequestBitMaskTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ServiceRequestBitMaskTextBox.Name = "_ServiceRequestBitMaskTextBox"
        Me._ServiceRequestBitMaskTextBox.Size = New System.Drawing.Size(100, 22)
        Me._ServiceRequestBitMaskTextBox.ToolTipText = "Service request Bit Mask in Hex"
        '
        '_ServiceRequestEnableCommandMenuItem
        '
        Me._ServiceRequestEnableCommandMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServiceRequestEnableCommandTextBox})
        Me._ServiceRequestEnableCommandMenuItem.Name = "_ServiceRequestEnableCommandMenuItem"
        Me._ServiceRequestEnableCommandMenuItem.Size = New System.Drawing.Size(232, 22)
        Me._ServiceRequestEnableCommandMenuItem.Text = "Events Enable Command:"
        '
        '_ServiceRequestEnableCommandTextBox
        '
        Me._ServiceRequestEnableCommandTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me._ServiceRequestEnableCommandTextBox.Name = "_ServiceRequestEnableCommandTextBox"
        Me._ServiceRequestEnableCommandTextBox.Size = New System.Drawing.Size(100, 22)
        Me._ServiceRequestEnableCommandTextBox.ToolTipText = "Enter the service request enable command, e.g., *SRE {0:D}"" "
        '
        '_ProgramServiceRequestEnableBitmaskMenuItem
        '
        Me._ProgramServiceRequestEnableBitmaskMenuItem.Name = "_ProgramServiceRequestEnableBitmaskMenuItem"
        Me._ProgramServiceRequestEnableBitmaskMenuItem.Size = New System.Drawing.Size(232, 22)
        Me._ProgramServiceRequestEnableBitmaskMenuItem.Text = "Apply SRQ Bitmask"
        Me._ProgramServiceRequestEnableBitmaskMenuItem.ToolTipText = "Sets the service request enable bitmask"
        '
        '_ToggleVisaEventHandlerMenuItem
        '
        Me._ToggleVisaEventHandlerMenuItem.Name = "_ToggleVisaEventHandlerMenuItem"
        Me._ToggleVisaEventHandlerMenuItem.Size = New System.Drawing.Size(232, 22)
        Me._ToggleVisaEventHandlerMenuItem.Text = "Toggle Visa Events"
        '
        '_ServiceRequestHandlerAddRemoveMenuItem
        '
        Me._ServiceRequestHandlerAddRemoveMenuItem.Name = "_ServiceRequestHandlerAddRemoveMenuItem"
        Me._ServiceRequestHandlerAddRemoveMenuItem.Size = New System.Drawing.Size(232, 22)
        Me._ServiceRequestHandlerAddRemoveMenuItem.Text = "Toggle SRQ Handling"
        Me._ServiceRequestHandlerAddRemoveMenuItem.ToolTipText = "Check to handle device service request"
        '
        '_ServiceRequestAutoReadMenuItem
        '
        Me._ServiceRequestAutoReadMenuItem.CheckOnClick = True
        Me._ServiceRequestAutoReadMenuItem.Name = "_ServiceRequestAutoReadMenuItem"
        Me._ServiceRequestAutoReadMenuItem.Size = New System.Drawing.Size(232, 22)
        Me._ServiceRequestAutoReadMenuItem.Text = "Auto Read Enabled"
        Me._ServiceRequestAutoReadMenuItem.ToolTipText = "Enables reading messages when service request is enabled"
        '
        'EditResourceNamesMenuItem
        '
        Me.EditResourceNamesMenuItem.Name = "EditResourceNamesMenuItem"
        Me.EditResourceNamesMenuItem.Size = New System.Drawing.Size(256, 22)
        Me.EditResourceNamesMenuItem.Text = "Edit Resource Names"
        '
        'StatusView
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._ToolStrip)
        Me.Name = "StatusView"
        Me.Size = New System.Drawing.Size(474, 31)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _DeviceDropDownButton As isr.Core.Controls.ToolStripDropDownButton
    Private WithEvents _ResetMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearInterfaceMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _ClearDeviceMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ResetKnownStateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InitKnownStateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearExecutionStateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SessionDownDownButton As isr.Core.Controls.ToolStripDropDownButton
    Private WithEvents _ReadTerminationEnabledMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _WriteTerminationMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _WriteTerminationTextBox As isr.Core.Controls.ToolStripTextBox
    Private WithEvents _ReadTerminationMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadTerminationTextBox As isr.Core.Controls.ToolStripTextBox
    Private WithEvents _SessionTimeoutMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SessionTimeoutTextBox As isr.Core.Controls.ToolStripTextBox
    Private WithEvents _StoreTimeoutMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PollSplitButton As isr.Core.Controls.ToolStripDropDownButton
    Private WithEvents _PollCommandMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PollWriteCommandTextBox As isr.Core.Controls.ToolStripTextBox
    Private WithEvents _PollIntervalMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PollIntervalTextBox As isr.Core.Controls.ToolStripTextBox
    Private WithEvents _PollMessageStatusBitMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PollMessageStatusBitTextBox As isr.Core.Controls.ToolStripTextBox
    Private WithEvents _SessionReadStatusByteMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RestoreTimeoutMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ApplicationDropDownButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _TraceMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _TraceLogLevelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _TraceLogLevelComboBox As Core.Controls.ToolStripComboBox
    Private WithEvents _TraceShowLevelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _TraceShowLevelComboBox As Core.Controls.ToolStripComboBox
    Private WithEvents _SessionNotificationLevelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SessionNotificationLevelComboBox As Core.Controls.ToolStripComboBox
    Private WithEvents _UsingStatusSubsystemMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _ClearErrorReportMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadDeviceErrorsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SessionReadStandardEventRegisterMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PollSendMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _PollEnabledMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _PollAutoReadMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _ServiceRequestSplitButton As isr.Core.Controls.ToolStripDropDownButton
    Private WithEvents _ServiceRequestBitmaskMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _ServiceRequestBitMaskTextBox As Core.Controls.ToolStripTextBox
    Private WithEvents _ServiceRequestEnableCommandMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ServiceRequestEnableCommandTextBox As Core.Controls.ToolStripTextBox
    Private WithEvents _ProgramServiceRequestEnableBitmaskMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _ServiceRequestHandlerAddRemoveMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _ServiceRequestAutoReadMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _ReadTerminalsStateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ToggleVisaEventHandlerMenuItem As Core.Controls.ToolStripMenuItem
    Private WithEvents _SendBusTriggerMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents EditSettingsToolStripMenuItem As ToolStripMenuItem
    Private WithEvents _SessionSettingsMenuItem As ToolStripMenuItem
    Private WithEvents _DeviceSettingsMenuItem As ToolStripMenuItem
    Private WithEvents _UserInterfaceSettingsMenuItem As ToolStripMenuItem
    Private WithEvents _UIBaseSettingsMenuItem As ToolStripMenuItem
    Private WithEvents EditResourceNamesMenuItem As ToolStripMenuItem
End Class
