<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SessionView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SessionView))
        Me._WriteComboBox = New System.Windows.Forms.ComboBox()
        Me._ReadTextBox = New System.Windows.Forms.TextBox()
        Me._QueryButton = New isr.Core.Controls.ToolStripButton()
        Me._WriteButton = New isr.Core.Controls.ToolStripButton()
        Me._ReadButton = New isr.Core.Controls.ToolStripButton()
        Me._ReadDelayNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._StatusReadDelayNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._MoreOptionsSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._EraseDisplayMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ClearSessionMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ReadStatusMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ShowPollReadingsMenuItem = New isr.Core.Controls.ToolStripMenuItem()
        Me._ShowServiceRequestReadingMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AppendTerminationMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_WriteComboBox
        '
        Me._WriteComboBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._WriteComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._WriteComboBox.FormattingEnabled = True
        Me._WriteComboBox.Location = New System.Drawing.Point(1, 1)
        Me._WriteComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._WriteComboBox.Name = "_WriteComboBox"
        Me._WriteComboBox.Size = New System.Drawing.Size(388, 25)
        Me._WriteComboBox.TabIndex = 53
        '
        '_ReadTextBox
        '
        Me._ReadTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ReadTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReadTextBox.Location = New System.Drawing.Point(1, 54)
        Me._ReadTextBox.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me._ReadTextBox.Multiline = True
        Me._ReadTextBox.Name = "_ReadTextBox"
        Me._ReadTextBox.ReadOnly = True
        Me._ReadTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._ReadTextBox.Size = New System.Drawing.Size(388, 273)
        Me._ReadTextBox.TabIndex = 59
        Me._ReadTextBox.TabStop = False
        '
        '_QueryButton
        '
        Me._QueryButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._QueryButton.Image = CType(resources.GetObject("_QueryButton.Image"), System.Drawing.Image)
        Me._QueryButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._QueryButton.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
        Me._QueryButton.Name = "_QueryButton"
        Me._QueryButton.Size = New System.Drawing.Size(47, 25)
        Me._QueryButton.Text = "&Query"
        Me._QueryButton.ToolTipText = "Write (send) the 'Message to Send' to the instrument, read the reply and display " &
    "in the 'Received data' text box"
        '
        '_WriteButton
        '
        Me._WriteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._WriteButton.Image = CType(resources.GetObject("_WriteButton.Image"), System.Drawing.Image)
        Me._WriteButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._WriteButton.Margin = New System.Windows.Forms.Padding(0, 1, 2, 2)
        Me._WriteButton.Name = "_WriteButton"
        Me._WriteButton.Size = New System.Drawing.Size(43, 25)
        Me._WriteButton.Text = "&Write"
        Me._WriteButton.ToolTipText = "Write (send) the 'Message to Send' to the instrument"
        '
        '_ReadButton
        '
        Me._ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadButton.Image = CType(resources.GetObject("_ReadButton.Image"), System.Drawing.Image)
        Me._ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadButton.Margin = New System.Windows.Forms.Padding(0, 1, 2, 2)
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(42, 25)
        Me._ReadButton.Text = "&Read"
        Me._ReadButton.ToolTipText = "Read a message from the instrument and display in the 'Received data' text box"
        '
        '_ReadDelayNumeric
        '
        Me._ReadDelayNumeric.Name = "_ReadDelayNumeric"
        Me._ReadDelayNumeric.Size = New System.Drawing.Size(45, 25)
        Me._ReadDelayNumeric.Text = "10"
        Me._ReadDelayNumeric.ToolTipText = "Delay before reading in milliseconds"
        Me._ReadDelayNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_StatusReadDelayNumeric
        '
        Me._StatusReadDelayNumeric.Name = "_StatusReadDelayNumeric"
        Me._StatusReadDelayNumeric.Size = New System.Drawing.Size(45, 25)
        Me._StatusReadDelayNumeric.Text = "10"
        Me._StatusReadDelayNumeric.ToolTipText = "Delay time before reading status in milliseconds"
        Me._StatusReadDelayNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_ToolStrip
        '
        Me._ToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._ToolStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._QueryButton, Me._WriteButton, Me._ReadButton, Me._ReadDelayNumeric, Me._StatusReadDelayNumeric, Me._MoreOptionsSplitButton})
        Me._ToolStrip.Location = New System.Drawing.Point(1, 26)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(388, 28)
        Me._ToolStrip.TabIndex = 65
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_MoreOptionsSplitButton
        '
        Me._MoreOptionsSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._MoreOptionsSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._EraseDisplayMenuItem, Me._ClearSessionMenuItem, Me._ReadStatusMenuItem, Me._ShowPollReadingsMenuItem, Me._ShowServiceRequestReadingMenuItem, Me._AppendTerminationMenuItem})
        Me._MoreOptionsSplitButton.Image = CType(resources.GetObject("_MoreOptionsSplitButton.Image"), System.Drawing.Image)
        Me._MoreOptionsSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._MoreOptionsSplitButton.Name = "_MoreOptionsSplitButton"
        Me._MoreOptionsSplitButton.Size = New System.Drawing.Size(79, 25)
        Me._MoreOptionsSplitButton.Text = "&Options..."
        Me._MoreOptionsSplitButton.ToolTipText = "Select from additional options"
        '
        '_EraseDisplayMenuItem
        '
        Me._EraseDisplayMenuItem.Name = "_EraseDisplayMenuItem"
        Me._EraseDisplayMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._EraseDisplayMenuItem.Text = "&Erase Display"
        Me._EraseDisplayMenuItem.ToolTipText = "Clears the display"
        '
        '_ClearSessionMenuItem
        '
        Me._ClearSessionMenuItem.Name = "_ClearSessionMenuItem"
        Me._ClearSessionMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._ClearSessionMenuItem.Text = "&Clear Session"
        Me._ClearSessionMenuItem.ToolTipText = "Clears the session (*CLS)"
        '
        '_ReadStatusMenuItem
        '
        Me._ReadStatusMenuItem.Name = "_ReadStatusMenuItem"
        Me._ReadStatusMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._ReadStatusMenuItem.Text = "&Read Status Byte"
        Me._ReadStatusMenuItem.ToolTipText = "Reads the status byte"
        '
        '_ShowPollReadingsMenuItem
        '
        Me._ShowPollReadingsMenuItem.CheckOnClick = True
        Me._ShowPollReadingsMenuItem.Name = "_ShowPollReadingsMenuItem"
        Me._ShowPollReadingsMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._ShowPollReadingsMenuItem.Text = "Show &Poll Readings"
        Me._ShowPollReadingsMenuItem.ToolTipText = "Displays polled reading when session polling is enabled"
        '
        '_ShowServiceRequestReadingMenuItem
        '
        Me._ShowServiceRequestReadingMenuItem.CheckOnClick = True
        Me._ShowServiceRequestReadingMenuItem.Name = "_ShowServiceRequestReadingMenuItem"
        Me._ShowServiceRequestReadingMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._ShowServiceRequestReadingMenuItem.Text = "Show SR&Q Reading"
        Me._ShowServiceRequestReadingMenuItem.ToolTipText = "Shows service request reading when service request auto read is enabled"
        '
        '_AppendTerminationMenuItem
        '
        Me._AppendTerminationMenuItem.CheckOnClick = True
        Me._AppendTerminationMenuItem.Name = "_AppendTerminationMenuItem"
        Me._AppendTerminationMenuItem.Size = New System.Drawing.Size(194, 22)
        Me._AppendTerminationMenuItem.Text = "&Append Termination"
        Me._AppendTerminationMenuItem.ToolTipText = "Appends termination to instrument commands thus not having the add the terminatio" &
    "n character (\n)"
        '
        'SessionView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ReadTextBox)
        Me.Controls.Add(Me._ToolStrip)
        Me.Controls.Add(Me._WriteComboBox)
        Me.Name = "SessionView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(390, 328)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _WriteComboBox As Windows.Forms.ComboBox
    Private WithEvents _ReadTextBox As Windows.Forms.TextBox
    Private WithEvents _QueryButton As Core.Controls.ToolStripButton
    Private WithEvents _WriteButton As Core.Controls.ToolStripButton
    Private WithEvents _ReadButton As Core.Controls.ToolStripButton
    Private WithEvents _ReadDelayNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _StatusReadDelayNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _MoreOptionsSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _EraseDisplayMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _ClearSessionMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _ReadStatusMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _ShowPollReadingsMenuItem As isr.Core.Controls.ToolStripMenuItem
    Private WithEvents _ShowServiceRequestReadingMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AppendTerminationMenuItem As ToolStripMenuItem
End Class
