''' <summary> K3700 Device unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k3700")>
Public Class SubsystemsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K3700Tests.ResourceSettings.Get.Exists, $"{GetType(K3700Tests.ResourceSettings)} settings not found")
        Assert.IsTrue(K3700Tests.SubsystemsSettings.Get.Exists, $"{GetType(K3700Tests.SubsystemsSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CHECK SUSBSYSTEMS "

    ''' <summary> (Unit Test Method) tests open session. </summary>
    ''' <remarks> Tests opening and closing a VISA session. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValues(device.Session, ResourceSettings.Get, SubsystemsSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> Opens session check status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
    ''' <param name="resourceInfo">     Information describing the resource. </param>
    ''' <param name="subsystemsInfo">   Information describing the subsystems. </param>
    Private Shared Sub OpenSessionCheckStatus(ByVal readErrorEnabled As Boolean, ByVal resourceInfo As ResourceSettings, ByVal subsystemsInfo As SubsystemsSettings)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValues(device.Session, resourceInfo, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertSessionOpenValues(device.Session, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceModel(device.StatusSubsystemBase, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceErrors(device.StatusSubsystemBase, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertTerminationValues(device.Session, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertLineFrequency(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertIntegrationPeriod(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertClearSessionDeviceErrors(device, subsystemsInfo)
                If readErrorEnabled Then isr.VI.DeviceTests.DeviceManager.AssertReadingDeviceErrors(device, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOrphanMessages(device.StatusSubsystemBase)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> A test for Open Session and status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionCheckStatusTest()
        SubsystemsTests.OpenSessionCheckStatus(False, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) tests open session read device errors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionReadDeviceErrorsTest()
        SubsystemsTests.OpenSessionCheckStatus(True, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM: INITIAL VALUES TEST "

    ''' <summary> (Unit Test Method) tests multimeter subsystem initial values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemInitialValuesTest()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.MultimeterSubsystem, SubsystemsSettings.Get)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM: RESISTANCE RANGE TEST "

    ''' <summary> (Unit Test Method) tests multimeter subsystem resistance range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemResistanceRangeTest()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            Try
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                Dim initialRange As Double = 0.1 * SubsystemsSettings.Get.ExpectedResistance
                Dim retryFactor As Double = 2
                Dim retryCount As Integer = 10
                SubsystemsTests.TestCloseChannles(device.ChannelSubsystem, SubsystemsSettings.Get.ResistorChannelList)
                SubsystemsTests.ApplyFunctionMode(device.MultimeterSubsystem, VI.MultimeterFunctionModes.ResistanceFourWire)
                Dim status As VI.MetaStatus = device.MultimeterSubsystem.EstablishRange(initialRange, retryFactor, retryCount)
                Dim expectedRange As Double = SubsystemsSettings.Get.ExpectedResistance
                Assert.IsTrue(device.MultimeterSubsystem.Range.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
                Assert.IsTrue(status.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MeasuredAmount)}.{NameOf(VI.MetaStatus)}")
                Assert.IsTrue(device.MultimeterSubsystem.Range.Value - expectedRange < 0.05 * expectedRange,
                          $"actual {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)} {device.MultimeterSubsystem.Range.Value} too low < {expectedRange}")
                expectedRange *= 10
                Assert.IsTrue(expectedRange - device.MultimeterSubsystem.Range.Value > 0.05 * expectedRange,
                          $"actual {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)} {device.MultimeterSubsystem.Range.Value} too high > {expectedRange}")
            Catch
                Throw
            Finally
                K3700Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM: TEST VOLTAGE MEASUREMENT "

    ''' <summary> Tests close channles. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="channelList"> List of channels. </param>
    Private Shared Sub TestCloseChannles(ByVal subsystem As VI.ChannelSubsystemBase, ByVal channelList As String)

        Dim expectedChannelList As String = String.Empty
        Dim actualChannelList As String = subsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList, $"Open All failed {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)}")

        expectedChannelList = channelList
        actualChannelList = subsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList, $"Close channels failed {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)}")

    End Sub

    ''' <summary> Applies the function mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">            The subsystem. </param>
    ''' <param name="expectedFunctionMode"> The expected function mode. </param>
    Private Shared Sub ApplyFunctionMode(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal expectedFunctionMode As VI.MultimeterFunctionModes)
        Dim actualFunctionMode? As VI.MultimeterFunctionModes = subsystem.ApplyFunctionMode(expectedFunctionMode)
        Assert.IsTrue(actualFunctionMode.HasValue, $"Failed reading {GetType(VI.Tsp.K3700.MultimeterSubsystem)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.FunctionMode)}")
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.FunctionMode)}")
    End Sub

    ''' <summary> Applies the automatic zero once described by subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ApplyAutoZeroOnce(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase)
        subsystem.AutoZeroOnce()
        Dim expectedBoolean As Boolean = False
        Dim actualBoolean As Boolean? = subsystem.QueryAutoZeroEnabled()
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroOnce)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroOnce)} {NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)} still enabled")
    End Sub

    ''' <summary> Applies the automatic zero enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyAutoZeroEnabled(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyAutoZeroEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}")
    End Sub

    ''' <summary> Applies the filter enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyFilterEnabled(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyFilterEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
    End Sub

    ''' <summary> Applies the moving average filter enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyMovingAverageFilterEnabled(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyMovingAverageFilterEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
    End Sub

    ''' <summary> Applies the filter count. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyFilterCount(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Integer)
        Dim expectedInteger As Integer = value
        Dim actualInteger As Integer? = subsystem.ApplyFilterCount(value)
        Assert.IsTrue(actualInteger.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
        Assert.AreEqual(expectedInteger, actualInteger.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
    End Sub

    ''' <summary> Applies the filter window. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyFilterWindow(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyFilterWindow(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
    End Sub

    ''' <summary> Applies the power line cycles. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyPowerLineCycles(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyPowerLineCycles(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
    End Sub

    ''' <summary> Applies the range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyRange(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyRange(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
    End Sub

    ''' <summary> Measures the given subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub Measure(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase)
        Dim expectedTimeSpan As TimeSpan = subsystem.EstimateMeasurementTime
        Dim timeout As Integer = Math.Max(10000, 4 * CInt(expectedTimeSpan.TotalMilliseconds))
        subsystem.Session.StoreCommunicationTimeout(TimeSpan.FromMilliseconds(timeout))
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim value As Double? = subsystem.MeasurePrimaryReading()
        Dim actualTimespan As TimeSpan = sw.Elapsed
        Assert.IsTrue(value.HasValue, $"Failed reading {GetType(VI.Tsp.MultimeterSubsystemBase)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.PrimaryReadingValue)}")
        Assert.IsTrue(value.Value > -0.001, $"Failed reading positive {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PrimaryReadingValue)}")
        Assert.IsTrue(actualTimespan >= expectedTimeSpan, $"Reading too short; expected {expectedTimeSpan} actual {actualTimespan}")
        Dim twiceInterval As TimeSpan = expectedTimeSpan.Add(expectedTimeSpan)
        Assert.IsTrue(actualTimespan < twiceInterval, $"Reading too long; expected {twiceInterval} actual {actualTimespan}")
        subsystem.Session.RestoreCommunicationTimeout()
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests multimeter subsystem voltage measurement timing.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemVoltageMeasurementTimingTest()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                Dim powerLineCycles As Double = 5
                Dim FilterCount As Integer = 20
                Dim window As Double = 0.095
                Dim range As Double = 0.1
                SubsystemsTests.TestCloseChannles(device.ChannelSubsystem, SubsystemsSettings.Get.ShortChannelList)
                SubsystemsTests.ApplyPowerLineCycles(device.MultimeterSubsystem, powerLineCycles)
                SubsystemsTests.ApplyRange(device.MultimeterSubsystem, range)
                SubsystemsTests.ApplyAutoZeroEnabled(device.MultimeterSubsystem, False)
                SubsystemsTests.ApplyAutoZeroOnce(device.MultimeterSubsystem)
                SubsystemsTests.ApplyMovingAverageFilterEnabled(device.MultimeterSubsystem, False) ' use repeat filter.
                SubsystemsTests.ApplyFilterCount(device.MultimeterSubsystem, FilterCount)
                SubsystemsTests.ApplyFilterWindow(device.MultimeterSubsystem, window)
                SubsystemsTests.ApplyFilterEnabled(device.MultimeterSubsystem, True)
                SubsystemsTests.Measure(device.MultimeterSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests multimeter subsystem resistance measurement timing.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemResistanceMeasurementTimingTest()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                Dim powerLineCycles As Double = 5
				Dim FilterCount As Integer = 20
				Dim window As Double = 0.095
				Dim range As Double = 1
				SubsystemsTests.TestCloseChannles(device.ChannelSubsystem, SubsystemsSettings.Get.ShortChannelList)
				SubsystemsTests.ApplyFunctionMode(device.MultimeterSubsystem, VI.MultimeterFunctionModes.ResistanceFourWire)
				SubsystemsTests.ApplyPowerLineCycles(device.MultimeterSubsystem, powerLineCycles)
				SubsystemsTests.ApplyRange(device.MultimeterSubsystem, range)
				SubsystemsTests.ApplyAutoZeroEnabled(device.MultimeterSubsystem, False)
				SubsystemsTests.ApplyAutoZeroOnce(device.MultimeterSubsystem)
				SubsystemsTests.ApplyMovingAverageFilterEnabled(device.MultimeterSubsystem, False) ' use repeat filter.
				SubsystemsTests.ApplyFilterCount(device.MultimeterSubsystem, FilterCount)
				SubsystemsTests.ApplyFilterWindow(device.MultimeterSubsystem, window)
				SubsystemsTests.ApplyFilterEnabled(device.MultimeterSubsystem, True)
				SubsystemsTests.Measure(device.MultimeterSubsystem)
			Catch
				Throw
			Finally
				DeviceManager.CloseSession(TestInfo, device)
			End Try
		End Using
	End Sub

#End Region

End Class
