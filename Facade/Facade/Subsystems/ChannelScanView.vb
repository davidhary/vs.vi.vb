'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\ChannelScanView.vb
'
' summary:	Channel scan view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Channel Scan subsystem user interface. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ChannelScanView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ChannelScanView"/> </summary>
    ''' <returns> A <see cref="ChannelScanView"/>. </returns>
    Public Shared Function Create() As ChannelScanView
        Dim view As ChannelScanView = Nothing
        Try
            view = New ChannelScanView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Gets or sets the closed channels. </summary>
    ''' <value> The closed channels. </value>
    Public Property ClosedChannels() As String
        Get
            Return Me._ChannelView.ClosedChannels
        End Get
        Set(ByVal value As String)
            Me._ChannelView.ClosedChannels = value
        End Set
    End Property

    ''' <summary> Gets or sets the scan list function. </summary>
    ''' <value> The scan list function. </value>
    Public Property ScanListFunction() As String
        Get
            Return Me._ScanView.ScanListFunction
        End Get
        Set(ByVal value As String)
            Me._ScanView.ScanListFunction = value
        End Set
    End Property

    ''' <summary> Gets or sets the scan list of closed channels. </summary>
    ''' <value> The closed channels. </value>
    Public Property InternalScanList() As String
        Get
            Return Me._ScanView.InternalScanList
        End Get
        Set(ByVal value As String)
            Me._ScanView.InternalScanList = value
        End Set
    End Property

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(ChannelScanView).SplitWords}")
        End If
        Me._ChannelView.AssignDevice(value)
        Me._ChannelView.SubsystemName = "Route"
        Me._ScanView.AssignDevice(value)
        Me._ScanView.SubsystemName = "Route"
    End Sub

    ''' <summary> Gets the system subsystem. </summary>
    ''' <value> The system subsystem. </value>
    Private Property SystemSubsystem As SystemSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As SystemSubsystemBase)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.SystemSubsystem = Nothing
        End If
        Me.SystemSubsystem = subsystem
        Me._ChannelView.BindSubsystem(subsystem)
        Me._ScanView.BindSubsystem(subsystem)
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As RouteSubsystemBase)
        If subsystem Is Nothing Then
            Me._ChannelView.BindSubsystem(subsystem)
            Me._ScanView.BindSubsystem(subsystem)
        ElseIf Me.SystemSubsystem.SupportsScanCardOption AndAlso Me.SystemSubsystem.InstalledScanCards.Any Then
            Me._ChannelView.BindSubsystem(subsystem)
            Me._ScanView.BindSubsystem(subsystem)
        ElseIf Not Me.SystemSubsystem.SupportsScanCardOption Then
            Me._InfoTextBox.Text = "No scan card"
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As SenseSubsystemBase)
        If subsystem Is Nothing Then
            Me._ScanView.BindSubsystem(subsystem)
        ElseIf Me.SystemSubsystem.SupportsScanCardOption AndAlso Me.SystemSubsystem.InstalledScanCards.Any Then
            Me._ScanView.BindSubsystem(subsystem)
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        Me._ChannelView.ReadSettings()
        Me._ScanView.ReadSettings()
    End Sub

#End Region

#Region " CONTROLS "

    ''' <summary> Gets the scan view. </summary>
    ''' <value> The scan view. </value>
    Protected ReadOnly Property ScanView As ScanView
        Get
            Return Me._ScanView
        End Get
    End Property


#End Region

#Region " TALKER "

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As isr.Core.ITraceMessageTalker)
        Me._ChannelView.AssignTalker(talker)
        Me._ScanView.AssignTalker(talker)
        ' assigned last as this identifies all talkers.
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
