'---------------------------------------------------------------------------------------------------
' file:		Device\Primitives\AuxiliaryStatusInfo.vb
'
' summary:	Auxiliary status information class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

Imports System.ComponentModel

''' <summary> Information about the Auxiliary Status. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-01-09 </para>
''' </remarks>
Public Class AuxiliaryStatusInfo

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="auxiliaryStatus"> The auxiliary status. </param>
    Public Sub New(ByVal auxiliaryStatus As AuxiliaryStatuses)
        Me.New()
        Me._AuxiliaryStatus = auxiliaryStatus
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As AuxiliaryStatusInfo)
        Me.New()
        If value IsNot Nothing Then
            Me._AuxiliaryStatus = value.AuxiliaryStatus
        End If
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub ClearKnownStateThis()
        Me._AuxiliaryStatus = AuxiliaryStatuses.None
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

#End Region

#Region " VALUE "

    ''' <summary> The auxiliary status. </summary>
    Private _AuxiliaryStatus As AuxiliaryStatuses

    ''' <summary> Gets the auxiliary Status. </summary>
    ''' <value> The auxiliary Status. </value>
    Public ReadOnly Property AuxiliaryStatus As AuxiliaryStatuses
        Get
            Return Me._AuxiliaryStatus
        End Get
    End Property

#End Region

#Region " BIT VALUES "

    ''' <summary>
    ''' Query if the Bits of the <paramref name="bits">Auxiliary Statuses</paramref> are on.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <param name="bits">  The bits. </param>
    ''' <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
    Public Shared Function IsBit(ByVal value As AuxiliaryStatuses, ByVal bits As AuxiliaryStatuses) As Boolean
        Return (value And bits) <> 0
    End Function

    ''' <summary>
    ''' Query if the Bit of the <paramref name="bit">Auxiliary Status</paramref> is on.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="bit"> The bit. </param>
    ''' <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
    Public Function IsBit(ByVal bit As AuxiliaryStatusBit) As Boolean
        Return Me.IsBit(CType(CInt(2 ^ CInt(bit)), AuxiliaryStatuses))
    End Function

    ''' <summary>
    ''' Query if the Bits of the <paramref name="bits">Auxiliary Statuses</paramref> are on.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="bits"> The bits. </param>
    ''' <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
    Public Function IsBit(ByVal bits As AuxiliaryStatuses) As Boolean
        Return (Me.AuxiliaryStatus And bits) <> 0
    End Function

    ''' <summary> Query if this object is dut control. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if dut control, false if not. </returns>
    Public Function IsDutControl() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.DutControlMode)
    End Function

    ''' <summary> Query if this object is heat only mode. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if heat only mode, false if not. </returns>
    Public Function IsHeatOnlyMode() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.HeatOnlyMode)
    End Function

    ''' <summary> Query if this object is ready. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if ready, false if not. </returns>
    Public Function IsReady() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.ReadyStartup)
    End Function

    ''' <summary> Query if this object is manual mode. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if manual mode, false if not. </returns>
    Public Function IsManualMode() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.ManualProgram)
    End Function

    ''' <summary> Query if this object is ramp mode. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if ramp mode, false if not. </returns>
    Public Function IsRampMode() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.RampMode)
    End Function

    ''' <summary> Query if this object is flow on. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if flow on, false if not. </returns>
    Public Function IsFlowOn() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.FlowOnOff)
    End Function

    ''' <summary> Query if this object is head up. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> True if head up, false if not. </returns>
    Public Function IsHeadUp() As Boolean
        Return Me.IsBit(AuxiliaryStatuses.HeadUpDown)
    End Function

    ''' <summary> Gets the description. </summary>
    ''' <value> The description. </value>
    Public ReadOnly Property Description As String
        Get
            Return Me.AuxiliaryStatus.Description
        End Get
    End Property

    ''' <summary> Query if any <see cref="AuxiliaryStatus">bits</see> are. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="included"> The included. </param>
    ''' <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
    Public Function IsIncluded(ByVal included As AuxiliaryStatuses) As Boolean
        Return AuxiliaryStatusInfo.IsIncluded(Me.AuxiliaryStatus, included)
    End Function

#End Region

#Region " HELPERS "

    ''' <summary> Builds Auxiliary Statuses. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="excluded"> The excluded. </param>
    ''' <param name="included"> The included. </param>
    ''' <returns> The AuxiliaryStatuses. </returns>
    Public Shared Function BuildAuxiliaryStatuses(ByVal value As AuxiliaryStatuses, ByVal excluded As AuxiliaryStatuses, ByVal included As AuxiliaryStatuses) As AuxiliaryStatuses
        Return (value Or included) And Not excluded
    End Function

    ''' <summary> Query if 'value' is included. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value">    The value. </param>
    ''' <param name="included"> The included. </param>
    ''' <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
    Public Shared Function IsIncluded(ByVal value As AuxiliaryStatuses, ByVal included As AuxiliaryStatuses) As Boolean
        Return (value And included) <> AuxiliaryStatuses.None
    End Function

#End Region

End Class

''' <summary> Values that represent the auxiliary Status bit. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Enum AuxiliaryStatusBit
    ''' <summary> An enum constant representing the none option. </summary>

    <Description("Not defined")>
    None = 0
    ''' <summary> An enum constant representing the head up down option. </summary>

    <Description("Head Up (1) Down (0)")>
    HeadUpDown = 2
    ''' <summary> An enum constant representing the heat only mode option. </summary>

    <Description("Heat Only (1) Compressor on (0)")>
    HeatOnlyMode = 3
    ''' <summary> An enum constant representing the dut control mode option. </summary>

    <Description("Dut-control (1) or air-control (0) Mode")>
    DutControlMode = 4
    ''' <summary> An enum constant representing the flow on off option. </summary>

    <Description("Flow on (1) or off (0)")>
    FlowOnOff = 5
    ''' <summary> An enum constant representing the ready startup option. </summary>

    <Description("Ready (1) Startup (0)")>
    ReadyStartup = 6
    ''' <summary> An enum constant representing the manual program option. </summary>

    <Description("Short Failure")>
    ManualProgram = 8
    ''' <summary> An enum constant representing the ramp mode option. </summary>

    <Description("Ramp Mode")>
    RampMode = 9
End Enum

''' <summary> A bit field of flags for specifying combination of Auxiliary Statuses. </summary>
''' <remarks> David, 2020-10-11. </remarks>
<Flags()>
Public Enum AuxiliaryStatuses
    ''' <summary> An enum constant representing the none option. </summary>

    <Description("None")>
    None = 0
    ''' <summary> An enum constant representing the head up down option. </summary>

    <Description("Head Up (1) Down (0)")>
    HeadUpDown = CInt(2 ^ CInt(AuxiliaryStatusBit.HeadUpDown))
    ''' <summary> An enum constant representing the heat only mode option. </summary>

    <Description("Heat Only (1) Compressor on (0)")>
    HeatOnlyMode = CInt(2 ^ CInt(AuxiliaryStatusBit.HeatOnlyMode))
    ''' <summary> An enum constant representing the dut control mode option. </summary>

    <Description("Dut-control (1) or air-control (0) Mode")>
    DutControlMode = CInt(2 ^ CInt(AuxiliaryStatusBit.DutControlMode))
    ''' <summary> An enum constant representing the flow on off option. </summary>

    <Description("Flow on (1) or off (0)")>
    FlowOnOff = CInt(2 ^ CInt(AuxiliaryStatusBit.FlowOnOff))
    ''' <summary> An enum constant representing the ready startup option. </summary>

    <Description("Ready (1) Startup (0)")>
    ReadyStartup = CInt(2 ^ CInt(AuxiliaryStatusBit.ReadyStartup))
    ''' <summary> An enum constant representing the manual program option. </summary>

    <Description("Manual (1) Program (0)")>
    ManualProgram = CInt(2 ^ CInt(AuxiliaryStatusBit.ManualProgram))
    ''' <summary> An enum constant representing the ramp mode option. </summary>

    <Description("Ramp Mode")>
    RampMode = CInt(2 ^ CInt(AuxiliaryStatusBit.RampMode))
End Enum
