'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\FormatSubsystemBase.vb
'
' summary:	Format subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary> Defines the contract that must be implemented by a Format Subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-15, 1.0.1841.x. </para>
''' </remarks>
Public MustInherit Class FormatSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FormatSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._OrderedSupportedElements = New List(Of ReadingElementTypes)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> Clears the queues and sets all registers to zero. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Elements = ReadingElementTypes.Reading
    End Sub

#End Region

#Region " ELEMENTS "

    ''' <summary> The ordered supported elements. </summary>
    Private ReadOnly _OrderedSupportedElements As List(Of ReadingElementTypes) = New List(Of ReadingElementTypes)

    ''' <summary> Gets or sets the ordered supported elements. </summary>
    ''' <value> The ordered supported elements. </value>
    Public Property OrderedSupportedElements As IList(Of ReadingElementTypes)
        Get
            ' supported elements for the 2002 are: Reading channel, reading number, units, timestamp and status.
            Return Me._OrderedSupportedElements
        End Get
        Set(value As IList(Of ReadingElementTypes))
            If Not List(Of ReadingElementTypes).Equals(value, Me.OrderedSupportedElements) Then
                Me._OrderedSupportedElements.Clear()
                Me._OrderedSupportedElements.AddRange(value)
                Me.NotifyPropertyChanged()
            End If
            Dim supported As ReadingElementTypes = ReadingElementTypes.None
            For Each element As ReadingElementTypes In Me.OrderedSupportedElements
                supported = supported Or element
            Next
            Me.SupportedElements = supported
        End Set
    End Property

    ''' <summary> The supported elements. </summary>
    Private _SupportedElements As ReadingElementTypes

    ''' <summary> Gets or sets the supported elements. </summary>
    ''' <value> The supported elements. </value>
    Public Property SupportedElements As ReadingElementTypes
        Get
            ' supported elements for the 2002 are: Reading channel, reading number, units, timestamp and status.
            Return Me._SupportedElements
        End Get
        Set(value As ReadingElementTypes)
            If value <> Me.SupportedElements Then
                Me._SupportedElements = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Builds the elements record for the specified reading elements types. </summary>
    ''' <param name="readingElementType"> Reading Element types. </param>
    ''' <returns> The record. </returns>
    Public Shared Function BuildRecord(ByVal readingElementType As ReadingElementTypes) As String
        If readingElementType = ReadingElementTypes.None Then
            Return String.Empty
        Else
            Dim reply As New System.Text.StringBuilder
            For Each code As Integer In [Enum].GetValues(GetType(ReadingElementTypes))
                If (readingElementType And code) <> 0 Then
                    Dim value As String = CType(code, ReadingElementTypes).ExtractBetween()
                    If Not String.IsNullOrWhiteSpace(value) Then
                        If reply.Length > 0 Then reply.Append(","c)
                        reply.Append(value)
                    End If
                End If
            Next
            Return reply.ToString
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="ReadingElementTypes"></see> from the specified value.
    ''' </summary>
    ''' <param name="value"> The Elements. </param>
    ''' <returns> The reading elements. </returns>
    Public Shared Function ParseReadingElement(ByVal value As String) As ReadingElementTypes
        If String.IsNullOrWhiteSpace(value) Then
            Return ReadingElementTypes.None
        Else
            Dim se As New StringEnumerator(Of ReadingElementTypes)
            Return se.ParseContained(value.BuildDelimitedValue)
        End If
    End Function

    ''' <summary>
    ''' Get the composite reading elements based on the message from the instrument.
    ''' </summary>
    ''' <param name="record"> Specifies the comma delimited elements record. </param>
    ''' <returns> The reading elements. </returns>
    Public Shared Function ParseReadingElements(ByVal record As String) As ReadingElementTypes
        Dim parsed As ReadingElementTypes = ReadingElementTypes.None
        If Not String.IsNullOrWhiteSpace(record) Then
            For Each elementValue As String In record.Split(","c)
                parsed = parsed Or ParseReadingElement(elementValue)
            Next
        End If
        Return parsed
    End Function

    ''' <summary> Reading Elements. </summary>
    Private _Elements As ReadingElementTypes

    ''' <summary> Gets or sets the cached Elements. </summary>
    ''' <value> A List of scans. </value>
    Public Overloads Property Elements As ReadingElementTypes
        Get
            Return Me._Elements
        End Get
        Protected Set(ByVal value As ReadingElementTypes)
            If Not Me.Elements.Equals(value) Then
                Me._Elements = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Elements. </summary>
    ''' <param name="value"> The Elements. </param>
    ''' <returns> A List of scans. </returns>
    Public Function ApplyElements(ByVal value As ReadingElementTypes) As ReadingElementTypes
        Me.WriteElements(value)
        Return Me.QueryElements()
    End Function

    ''' <summary> Gets the supports elements. </summary>
    ''' <value> The supports elements. </value>
    Public ReadOnly Property SupportsElements As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.ElementsQueryCommand)
        End Get
    End Property

    ''' <summary> Gets or sets the elements query command. </summary>
    ''' <remarks> SCPI Base Command: ":FORM:ELEM". </remarks>
    ''' <value> The elements query command. </value>
    Protected Overridable Property ElementsQueryCommand As String

    ''' <summary>
    ''' Queries the Elements. Also sets the <see cref="Elements">Format on</see> sentinel.
    ''' </summary>
    ''' <returns> A List of scans. </returns>
    Public Function QueryElements() As ReadingElementTypes
        Dim record As String = FormatSubsystemBase.BuildRecord(Me.Elements)
        record = Me.Query(record, Me.ElementsQueryCommand)
        Me.Elements = FormatSubsystemBase.ParseReadingElements(record)
        Return Me.Elements
    End Function

    ''' <summary>
    ''' Queries the Elements for a single element. Also sets the <see cref="Elements">Format on</see>
    ''' sentinel.
    ''' </summary>
    ''' <returns> A List of scans. </returns>
    Public Function QueryElement() As ReadingElementTypes
        Dim record As String = FormatSubsystemBase.BuildRecord(Me.Elements)
        record = Me.Query(record, Me.ElementsQueryCommand)
        Me.Elements = FormatSubsystemBase.ParseReadingElement(record)
        Return Me.Elements
    End Function

    ''' <summary> Gets or sets the elements command format. </summary>
    ''' <remarks> SCPI Base Command: ":FORM:ELEM {0}". </remarks>
    ''' <value> The elements command format. </value>
    Protected Overridable Property ElementsCommandFormat As String

    ''' <summary>
    ''' Writes the Elements for a single element. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> The Elements. </param>
    ''' <returns> A List of scans. </returns>
    Public Function WriteElement(ByVal value As ReadingElementTypes) As ReadingElementTypes
        Dim record As String = FormatSubsystemBase.BuildRecord(value)
        Me.Write(Me.ElementsCommandFormat, record)
        Me.Elements = FormatSubsystemBase.ParseReadingElement(record)
        Return Me.Elements
    End Function

    ''' <summary> Writes the Elements. Does not read back from the instrument. </summary>
    ''' <param name="value"> The Elements. </param>
    ''' <returns> A List of scans. </returns>
    Public Function WriteElements(ByVal value As ReadingElementTypes) As ReadingElementTypes
        value = value And Me.SupportedElements
        Dim record As String = FormatSubsystemBase.BuildRecord(value)
        Me.Write(Me.ElementsCommandFormat, record)
        Me.Elements = FormatSubsystemBase.ParseReadingElements(record)
        Return Me.Elements
    End Function

#End Region

End Class

