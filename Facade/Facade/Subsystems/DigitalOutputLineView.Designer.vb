<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DigitalOutputLineView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DigitalOutputLineView))
        Me._ToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LineNumberNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._LineNumberBox = New isr.Core.Controls.ToolStripNumberBox()
        Me._ActiveLevelComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ActiveLevelComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ReadButton = New System.Windows.Forms.ToolStripButton()
        Me._LineLevelNumberBox = New isr.Core.Controls.ToolStripNumberBox()
        Me._WriteButton = New System.Windows.Forms.ToolStripButton()
        Me._PulseButton = New System.Windows.Forms.ToolStripButton()
        Me._ToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._PulseWidthNumberBox = New isr.Core.Controls.ToolStripNumberBox()
        Me._ToolStripPanel.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStripPanel
        '
        Me._ToolStripPanel.BackColor = System.Drawing.Color.Transparent
        Me._ToolStripPanel.Controls.Add(Me._SubsystemToolStrip)
        Me._ToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ToolStripPanel.Location = New System.Drawing.Point(1, 1)
        Me._ToolStripPanel.Name = "_ToolStripPanel"
        Me._ToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._ToolStripPanel.RowMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStripPanel.Size = New System.Drawing.Size(451, 28)
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._LineNumberNumericLabel, Me._LineNumberBox, Me._ActiveLevelComboBoxLabel, Me._ActiveLevelComboBox, Me._ReadButton, Me._LineLevelNumberBox, Me._WriteButton, Me._ToggleButton, Me._PulseButton, Me._PulseWidthNumberBox})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(451, 28)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 3
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DoubleClickEnabled = True
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(45, 25)
        Me._SubsystemSplitButton.Text = "Line"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument"
        '
        '_LineNumberNumericLabel
        '
        Me._LineNumberNumericLabel.Name = "_LineNumberNumericLabel"
        Me._LineNumberNumericLabel.Size = New System.Drawing.Size(14, 25)
        Me._LineNumberNumericLabel.Text = "#"
        '
        '_LineNumberBox
        '
        Me._LineNumberBox.BackColor = System.Drawing.SystemColors.Window
        Me._LineNumberBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LineNumberBox.MaxValue = 4.0R
        Me._LineNumberBox.MinValue = 1.0R
        Me._LineNumberBox.Name = "_LineNumberBox"
        Me._LineNumberBox.Prefix = Nothing
        Me._LineNumberBox.Size = New System.Drawing.Size(22, 25)
        Me._LineNumberBox.Snfs = "D"
        Me._LineNumberBox.Suffix = Nothing
        Me._LineNumberBox.Text = "-1"
        Me._LineNumberBox.ValueAsByte = CType(0, Byte)
        Me._LineNumberBox.ValueAsDouble = 0R
        Me._LineNumberBox.ValueAsFloat = 0!
        Me._LineNumberBox.ValueAsInt16 = CType(0, Short)
        Me._LineNumberBox.ValueAsInt32 = 0
        Me._LineNumberBox.ValueAsInt64 = CType(0, Long)
        Me._LineNumberBox.ValueAsSByte = CType(CSByte(0), SByte)
        Me._LineNumberBox.ValueAsUInt16 = CType(0US, UShort)
        Me._LineNumberBox.ValueAsUInt32 = CType(0UI, UInteger)
        Me._LineNumberBox.ValueAsUInt64 = CType(0UL, ULong)
        '
        '_ActiveLevelComboBoxLabel
        '
        Me._ActiveLevelComboBoxLabel.Name = "_ActiveLevelComboBoxLabel"
        Me._ActiveLevelComboBoxLabel.Size = New System.Drawing.Size(43, 25)
        Me._ActiveLevelComboBoxLabel.Text = "Active:"
        '
        '_ActiveLevelComboBox
        '
        Me._ActiveLevelComboBox.AutoSize = False
        Me._ActiveLevelComboBox.Items.AddRange(New Object() {"Low", "High"})
        Me._ActiveLevelComboBox.Name = "_ActiveLevelComboBox"
        Me._ActiveLevelComboBox.Size = New System.Drawing.Size(50, 23)
        Me._ActiveLevelComboBox.Text = "Low"
        Me._ActiveLevelComboBox.ToolTipText = "Selects the active level"
        '
        '_ReadButton
        '
        Me._ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadButton.Image = CType(resources.GetObject("_ReadButton.Image"), System.Drawing.Image)
        Me._ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(37, 25)
        Me._ReadButton.Text = "Read"
        Me._ReadButton.ToolTipText = "Read the digital level"
        '
        '_LineLevelNumberBox
        '
        Me._LineLevelNumberBox.BackColor = System.Drawing.SystemColors.Window
        Me._LineLevelNumberBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LineLevelNumberBox.MaxValue = 1.0R
        Me._LineLevelNumberBox.MinValue = -2.0R
        Me._LineLevelNumberBox.Name = "_LineLevelNumberBox"
        Me._LineLevelNumberBox.Prefix = Nothing
        Me._LineLevelNumberBox.Size = New System.Drawing.Size(17, 25)
        Me._LineLevelNumberBox.Snfs = "D"
        Me._LineLevelNumberBox.Suffix = Nothing
        Me._LineLevelNumberBox.Text = "0"
        Me._LineLevelNumberBox.ToolTipText = "0 = inactive; 1 = active; -1 = not specified; -2 not set."
        Me._LineLevelNumberBox.ValueAsByte = CType(0, Byte)
        Me._LineLevelNumberBox.ValueAsDouble = 0R
        Me._LineLevelNumberBox.ValueAsFloat = 0!
        Me._LineLevelNumberBox.ValueAsInt16 = CType(0, Short)
        Me._LineLevelNumberBox.ValueAsInt32 = 0
        Me._LineLevelNumberBox.ValueAsInt64 = CType(0, Long)
        Me._LineLevelNumberBox.ValueAsSByte = CType(CSByte(0), SByte)
        Me._LineLevelNumberBox.ValueAsUInt16 = CType(0US, UShort)
        Me._LineLevelNumberBox.ValueAsUInt32 = CType(0UI, UInteger)
        Me._LineLevelNumberBox.ValueAsUInt64 = CType(0UL, ULong)
        '
        '_WriteButton
        '
        Me._WriteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._WriteButton.Image = CType(resources.GetObject("_WriteButton.Image"), System.Drawing.Image)
        Me._WriteButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._WriteButton.Name = "_WriteButton"
        Me._WriteButton.Size = New System.Drawing.Size(39, 25)
        Me._WriteButton.Text = "Write"
        Me._WriteButton.ToolTipText = "Output the value"
        '
        '_PulseButton
        '
        Me._PulseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._PulseButton.Image = CType(resources.GetObject("_PulseButton.Image"), System.Drawing.Image)
        Me._PulseButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PulseButton.Name = "_PulseButton"
        Me._PulseButton.Size = New System.Drawing.Size(61, 25)
        Me._PulseButton.Text = "Pulse"
        Me._PulseButton.ToolTipText = "Output a pulse for the specified duration in milliseconds"
        '
        '_ToggleButton
        '
        Me._ToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ToggleButton.Image = CType(resources.GetObject("_ToggleButton.Image"), System.Drawing.Image)
        Me._ToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ToggleButton.Name = "_ToggleButton"
        Me._ToggleButton.Size = New System.Drawing.Size(46, 25)
        Me._ToggleButton.Text = "Toggle"
        Me._ToggleButton.ToolTipText = "Toggle the line level"
        '
        '_PulseWidthNumberBox
        '
        Me._PulseWidthNumberBox.BackColor = System.Drawing.SystemColors.Window
        Me._PulseWidthNumberBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PulseWidthNumberBox.MaxValue = 10000.0R
        Me._PulseWidthNumberBox.MinValue = 1.0R
        Me._PulseWidthNumberBox.Name = "_PulseWidthNumberBox"
        Me._PulseWidthNumberBox.Prefix = Nothing
        Me._PulseWidthNumberBox.Size = New System.Drawing.Size(17, 25)
        Me._PulseWidthNumberBox.Snfs = "N1"
        Me._PulseWidthNumberBox.Suffix = "ms"
        Me._PulseWidthNumberBox.Text = "1"
        Me._PulseWidthNumberBox.ToolTipText = "Pulse width in milliseconds"
        Me._PulseWidthNumberBox.ValueAsByte = CType(0, Byte)
        Me._PulseWidthNumberBox.ValueAsDouble = 0R
        Me._PulseWidthNumberBox.ValueAsFloat = 0!
        Me._PulseWidthNumberBox.ValueAsInt16 = CType(0, Short)
        Me._PulseWidthNumberBox.ValueAsInt32 = 0
        Me._PulseWidthNumberBox.ValueAsInt64 = CType(0, Long)
        Me._PulseWidthNumberBox.ValueAsSByte = CType(CSByte(0), SByte)
        Me._PulseWidthNumberBox.ValueAsUInt16 = CType(0US, UShort)
        Me._PulseWidthNumberBox.ValueAsUInt32 = CType(0UI, UInteger)
        Me._PulseWidthNumberBox.ValueAsUInt64 = CType(0UL, ULong)
        '
        'DigitalOutputLineView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ToolStripPanel)
        Me.Name = "DigitalOutputLineView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(453, 31)
        Me._ToolStripPanel.ResumeLayout(False)
        Me._ToolStripPanel.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private _ToolStripPanel As Windows.Forms.ToolStripPanel
    Private _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private _LineNumberNumericLabel As Windows.Forms.ToolStripLabel
    Private _ActiveLevelComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ActiveLevelComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadButton As ToolStripButton
    Private WithEvents _LineLevelNumberBox As Core.Controls.ToolStripNumberBox
    Private WithEvents _WriteButton As ToolStripButton
    Private WithEvents _LineNumberBox As Core.Controls.ToolStripNumberBox
    Private WithEvents _ToggleButton As ToolStripButton
    Private WithEvents _PulseButton As ToolStripButton
    Private WithEvents _PulseWidthNumberBox As Core.Controls.ToolStripNumberBox
End Class
