''' <summary> A session factory. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-29 </para>
''' </remarks>
Public Class SessionFactory
    Inherits VI.Pith.SessionFactoryBase

    ''' <summary> Creates gpib interface session. </summary>
    ''' <returns> The new gpib interface session. </returns>
    Public Overrides Function GpibInterfaceSession() As VI.Pith.InterfaceSessionBase
        Return New GpibInterfaceSession()
    End Function

    ''' <summary> Creates resources manager. </summary>
    ''' <returns> The new resources manager. </returns>
    Public Overrides Function ResourcesProvider() As Pith.ResourcesProviderBase
        Return New ResourcesProvider() With {.ResourceFinder = New VI.Pith.IntegratedResourceFinder()}
    End Function

    ''' <summary> Creates a session. </summary>
    ''' <returns> The new session. </returns>
    Public Overrides Function Session() As Pith.SessionBase
        Return New Foundation.Session()
    End Function

End Class
