''' <summary> Defines a SCPI Output Subsystem for power supplies. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class OutputSubsystem
    Inherits VI.OutputSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="OutputSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " TERMINAL MODE "

    ''' <summary> Gets or sets the terminals mode query command. </summary>
    ''' <value> The terminals mode query command. </value>
    Protected Overrides Property OutputTerminalsModeQueryCommand As String = ":OUTP:ROUT:TERM?"

    ''' <summary> Gets or sets the terminals mode command format. </summary>
    ''' <value> The terminals mode command format. </value>
    Protected Overrides Property OutputTerminalsModeCommandFormat As String = ":OUTP:ROUT:TERM {0}"

#End Region

#End Region

End Class
