<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ServiceRequestView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ServiceRequestView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ServiceRequestEnabledMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UsingNegativeTransitionsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToggleServiceRequestMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._HexadecimalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ServiceRequestFlagsComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ServiceRequestMaskAddButton = New System.Windows.Forms.ToolStripButton()
        Me._ServiceRequestMaskRemoveButton = New System.Windows.Forms.ToolStripButton()
        Me._ServiceRequestMaskNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ServiceRequestMaskNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._ServiceRequestFlagsComboBox, Me._ServiceRequestMaskAddButton, Me._ServiceRequestMaskRemoveButton, Me._ServiceRequestMaskNumericLabel, Me._ServiceRequestMaskNumeric})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(340, 26)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 1
        Me._SubsystemToolStrip.Text = "Service Request Toolstrip"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServiceRequestEnabledMenuItem, Me._UsingNegativeTransitionsMenuItem, Me._ToggleServiceRequestMenuItem, Me._HexadecimalToolStripMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(45, 23)
        Me._SubsystemSplitButton.Text = "SRQ"
        Me._SubsystemSplitButton.ToolTipText = "SRQ Options and actions"
        '
        '_ServiceRequestEnabledMenuItem
        '
        Me._ServiceRequestEnabledMenuItem.CheckOnClick = True
        Me._ServiceRequestEnabledMenuItem.Name = "_ServiceRequestEnabledMenuItem"
        Me._ServiceRequestEnabledMenuItem.Size = New System.Drawing.Size(213, 22)
        Me._ServiceRequestEnabledMenuItem.Text = "Service Request Enabled"
        Me._ServiceRequestEnabledMenuItem.ToolTipText = "Service request handling enabled when checked"
        '
        '_UsingNegativeTransitionsMenuItem
        '
        Me._UsingNegativeTransitionsMenuItem.CheckOnClick = True
        Me._UsingNegativeTransitionsMenuItem.Name = "_UsingNegativeTransitionsMenuItem"
        Me._UsingNegativeTransitionsMenuItem.Size = New System.Drawing.Size(213, 22)
        Me._UsingNegativeTransitionsMenuItem.Text = "Using Negative transitions"
        Me._UsingNegativeTransitionsMenuItem.ToolTipText = "Uses negative transition when checked"
        '
        '_ToggleServiceRequestMenuItem
        '
        Me._ToggleServiceRequestMenuItem.CheckOnClick = True
        Me._ToggleServiceRequestMenuItem.Name = "_ToggleServiceRequestMenuItem"
        Me._ToggleServiceRequestMenuItem.Size = New System.Drawing.Size(213, 22)
        Me._ToggleServiceRequestMenuItem.Text = "Enable Service Request"
        '
        '_HexadecimalToolStripMenuItem
        '
        Me._HexadecimalToolStripMenuItem.Checked = True
        Me._HexadecimalToolStripMenuItem.CheckOnClick = True
        Me._HexadecimalToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._HexadecimalToolStripMenuItem.Name = "_HexadecimalToolStripMenuItem"
        Me._HexadecimalToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me._HexadecimalToolStripMenuItem.Text = "Hexadecimal"
        Me._HexadecimalToolStripMenuItem.ToolTipText = "Toggles displays of service request max in hex or decimal"
        '
        '_ServiceRequestFlagsComboBox
        '
        Me._ServiceRequestFlagsComboBox.Name = "_ServiceRequestFlagsComboBox"
        Me._ServiceRequestFlagsComboBox.Size = New System.Drawing.Size(141, 26)
        '
        '_ServiceRequestMaskAddButton
        '
        Me._ServiceRequestMaskAddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ServiceRequestMaskAddButton.Image = CType(resources.GetObject("_ServiceRequestMaskAddButton.Image"), System.Drawing.Image)
        Me._ServiceRequestMaskAddButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ServiceRequestMaskAddButton.Name = "_ServiceRequestMaskAddButton"
        Me._ServiceRequestMaskAddButton.Size = New System.Drawing.Size(23, 23)
        Me._ServiceRequestMaskAddButton.Text = "+"
        Me._ServiceRequestMaskAddButton.ToolTipText = "Add to mask"
        '
        '_ServiceRequestMaskRemoveButton
        '
        Me._ServiceRequestMaskRemoveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ServiceRequestMaskRemoveButton.Image = CType(resources.GetObject("_ServiceRequestMaskRemoveButton.Image"), System.Drawing.Image)
        Me._ServiceRequestMaskRemoveButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ServiceRequestMaskRemoveButton.Name = "_ServiceRequestMaskRemoveButton"
        Me._ServiceRequestMaskRemoveButton.Size = New System.Drawing.Size(23, 23)
        Me._ServiceRequestMaskRemoveButton.Text = "-"
        '
        '_ServiceRequestMaskNumericLabel
        '
        Me._ServiceRequestMaskNumericLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ServiceRequestMaskNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ServiceRequestMaskNumericLabel.Name = "_ServiceRequestMaskNumericLabel"
        Me._ServiceRequestMaskNumericLabel.Size = New System.Drawing.Size(19, 23)
        Me._ServiceRequestMaskNumericLabel.Text = "0x"
        '
        '_ServiceRequestMaskNumeric
        '
        Me._ServiceRequestMaskNumeric.Name = "_ServiceRequestMaskNumeric"
        Me._ServiceRequestMaskNumeric.Size = New System.Drawing.Size(41, 23)
        Me._ServiceRequestMaskNumeric.Text = "0"
        Me._ServiceRequestMaskNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'ServiceRequestView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "ServiceRequestView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(342, 28)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ServiceRequestEnabledMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _UsingNegativeTransitionsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ToggleServiceRequestMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ServiceRequestFlagsComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _ServiceRequestMaskAddButton As Windows.Forms.ToolStripButton
    Private WithEvents _ServiceRequestMaskRemoveButton As Windows.Forms.ToolStripButton
    Private WithEvents _ServiceRequestMaskNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ServiceRequestMaskNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _HexadecimalToolStripMenuItem As Windows.Forms.ToolStripMenuItem
End Class
