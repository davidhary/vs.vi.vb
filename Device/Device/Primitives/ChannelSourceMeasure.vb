﻿''' <summary> A channel source measure elements. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-13 </para>
''' </remarks>
Public Class ChannelSourceMeasure

    ''' <summary> Constructor. </summary>
    ''' <param name="title">              The title. </param>
    ''' <param name="sourceChannelList">  A List of source channels. </param>
    ''' <param name="measureChannelList"> A List of measure channels. </param>
    Public Sub New(ByVal title As String, ByVal sourceChannelList As String, ByVal measureChannelList As String)
        MyBase.New()
        Me.Title = title
        Me.SourceChannelList = ChannelSourceMeasure.ToSortedList(sourceChannelList)
        Me.MeasureChannelList = ChannelSourceMeasure.ToSortedList(measureChannelList)
        Me.MergeChannelLists()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="channelSourceMeasure"> The channel source measure. </param>
    Public Sub New(ByVal channelSourceMeasure As ChannelSourceMeasure)
        Me.New(ChannelSourceMeasure.Validated(channelSourceMeasure).Title, channelSourceMeasure.SourceChannelList, channelSourceMeasure.MeasureChannelList)
        Me.Current = channelSourceMeasure.Current
        Me.Voltage = channelSourceMeasure.Voltage
    End Sub

    ''' <summary> Validated the given channel source measure. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channelSourceMeasure"> The channel source measure. </param>
    ''' <returns> A ChannelSourceMeasure. </returns>
    Public Shared Function Validated(ByVal channelSourceMeasure As ChannelSourceMeasure) As ChannelSourceMeasure
        If channelSourceMeasure Is Nothing Then Throw New ArgumentNullException(NameOf(channelSourceMeasure))
        Return channelSourceMeasure
    End Function

    ''' <summary> Converts this object to a sorted list. </summary>
    ''' <param name="list"> The list. </param>
    ''' <returns> The given data converted to a String. </returns>
    Private Shared Function ToSortedList(ByVal list As String) As String
        Return ChannelSourceMeasure.ToSortedList(list, ChannelSourceMeasure.ChannelListDelimiter(list))
    End Function

    ''' <summary> Channel list delimiter. </summary>
    ''' <param name="list"> The list. </param>
    ''' <returns> A Char. </returns>
    Private Shared Function ChannelListDelimiter(ByVal list As String) As Char
        Dim result As Char = ";"c
        If Not list.Contains(result) Then
            result = ","c
        End If
        Return result
    End Function

    ''' <summary> Converts this object to a sorted list. </summary>
    ''' <param name="list">      The list. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> The given data converted to a String. </returns>
    Private Shared Function ToSortedList(ByVal list As String, ByVal delimiter As Char) As String
        Dim result As New System.Text.StringBuilder
        Dim l As New List(Of String)(list.Split(delimiter))
        l.Sort()
        For Each s As String In l
            If result.Length > 0 Then result.Append(delimiter)
            result.Append(s)
        Next
        Return result.ToString
    End Function

    ''' <summary> Gets the title. </summary>
    ''' <value> The title. </value>
    Public ReadOnly Property Title As String

    ''' <summary> Gets a list of source channels. </summary>
    ''' <value> A List of source channels. </value>
    Public ReadOnly Property SourceChannelList As String

    ''' <summary> Gets a list of measure channels. </summary>
    ''' <value> A List of measure channels. </value>
    Public ReadOnly Property MeasureChannelList As String

    ''' <summary> Merge channel lists. </summary>
    Private Sub MergeChannelLists()
        If String.IsNullOrWhiteSpace(Me.SourceChannelList) Then
            Me._ChannelList = If(String.IsNullOrWhiteSpace(Me.MeasureChannelList), String.Empty, ChannelSourceMeasure.ToSortedList(Me.MeasureChannelList))
        Else
            If String.IsNullOrWhiteSpace(Me.MeasureChannelList) Then
                Me._ChannelList = ChannelSourceMeasure.ToSortedList(Me.SourceChannelList)
            Else
                Dim builder As New System.Text.StringBuilder
                builder.Append(Me.SourceChannelList)
                builder.Append(ChannelSourceMeasure.ChannelListDelimiter(builder.ToString))
                builder.Append(Me.MeasureChannelList)
                Me._ChannelList = ChannelSourceMeasure.ToSortedList(builder.ToString)
            End If
        End If
    End Sub

    ''' <summary> Gets a list of channels. </summary>
    ''' <value> A List of channels. </value>
    Public ReadOnly Property ChannelList As String

    ''' <summary> Gets the sentinel indicating if the measure has a non zero current value. </summary>
    ''' <value> The sentinel indicating if the measure has a non zero current value. </value>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.Current <> 0
        End Get
    End Property

    ''' <summary> Gets the resistance. </summary>
    ''' <value>
    ''' The sheet resistance or <see cref="Double.NaN"/> if not <see cref="HasValue"/>.
    ''' </value>
    Public ReadOnly Property Resistance As Double
        Get
            Return If(Me.HasValue, Me.Voltage / Me.Current, Double.NaN)
        End Get
    End Property

    ''' <summary> Gets or sets the voltage. </summary>
    ''' <value> The voltage. </value>
    Public Property Voltage As Double

    ''' <summary> Gets or sets the current. </summary>
    ''' <value> The current. </value>
    Public Property Current As Double
End Class

''' <summary>
''' Channel source measure Collection: an ordered collection of source measures.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-13 </para>
''' </remarks>
Public Class ChannelSourceMeasureCollection
    Inherits ObjectModel.KeyedCollection(Of String, ChannelSourceMeasure)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ChannelSourceMeasure) As String
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.Title
    End Function

    ''' <summary> Adds a new source measure. </summary>
    ''' <param name="title">              The title. </param>
    ''' <param name="sourceChannelList">  List of channels. </param>
    ''' <param name="measureChannelList"> List of measure channels. </param>
    Public Sub AddSourceMeasure(ByVal title As String, ByVal sourceChannelList As String, ByVal measureChannelList As String)
        Me.Add(New ChannelSourceMeasure(title, sourceChannelList, measureChannelList))
    End Sub

End Class

