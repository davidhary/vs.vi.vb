<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResourceNameInfoEditor

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AddResourceButton = New System.Windows.Forms.Button()
        Me.ResourceNamesListBox = New System.Windows.Forms.ListBox()
        Me.ResourceNameTextBox = New System.Windows.Forms.TextBox()
        Me.ResourceNameTextBoxLabel = New System.Windows.Forms.Label()
        Me.TestResourceButton = New System.Windows.Forms.Button()
        Me.MessagesTextBox = New System.Windows.Forms.TextBox()
        Me.LayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.ResourcePanel = New System.Windows.Forms.Panel()
        Me.BackupButton = New System.Windows.Forms.Button()
        Me.RestoreButton = New System.Windows.Forms.Button()
        Me.RemoveButton = New System.Windows.Forms.Button()
        Me.ResourceFolderLabel = New System.Windows.Forms.Label()
        Me.LayoutPanel.SuspendLayout()
        Me.ResourcePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'AddResourceButton
        '
        Me.AddResourceButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AddResourceButton.Location = New System.Drawing.Point(242, 3)
        Me.AddResourceButton.Name = "AddResourceButton"
        Me.AddResourceButton.Size = New System.Drawing.Size(64, 25)
        Me.AddResourceButton.TabIndex = 4
        Me.AddResourceButton.Text = "&Add"
        Me.AddResourceButton.UseVisualStyleBackColor = True
        '
        'ResourceNamesListBox
        '
        Me.ResourceNamesListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ResourceNamesListBox.FormattingEnabled = True
        Me.ResourceNamesListBox.ItemHeight = 17
        Me.ResourceNamesListBox.Location = New System.Drawing.Point(6, 67)
        Me.ResourceNamesListBox.Name = "ResourceNamesListBox"
        Me.ResourceNamesListBox.Size = New System.Drawing.Size(440, 236)
        Me.ResourceNamesListBox.TabIndex = 4
        '
        'ResourceNameTextBox
        '
        Me.ResourceNameTextBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ResourceNameTextBox.Location = New System.Drawing.Point(0, 30)
        Me.ResourceNameTextBox.Name = "ResourceNameTextBox"
        Me.ResourceNameTextBox.Size = New System.Drawing.Size(440, 25)
        Me.ResourceNameTextBox.TabIndex = 1
        '
        'ResourceNameTextBoxLabel
        '
        Me.ResourceNameTextBoxLabel.AutoSize = True
        Me.ResourceNameTextBoxLabel.Location = New System.Drawing.Point(3, 11)
        Me.ResourceNameTextBoxLabel.Name = "ResourceNameTextBoxLabel"
        Me.ResourceNameTextBoxLabel.Size = New System.Drawing.Size(65, 17)
        Me.ResourceNameTextBoxLabel.TabIndex = 0
        Me.ResourceNameTextBoxLabel.Text = "Resource:"
        '
        'TestResourceButton
        '
        Me.TestResourceButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TestResourceButton.Location = New System.Drawing.Point(110, 3)
        Me.TestResourceButton.Name = "TestResourceButton"
        Me.TestResourceButton.Size = New System.Drawing.Size(64, 25)
        Me.TestResourceButton.TabIndex = 2
        Me.TestResourceButton.Text = "&Test"
        Me.TestResourceButton.UseVisualStyleBackColor = True
        '
        'MessagesTextBox
        '
        Me.MessagesTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me.MessagesTextBox.Location = New System.Drawing.Point(6, 309)
        Me.MessagesTextBox.Multiline = True
        Me.MessagesTextBox.Name = "MessagesTextBox"
        Me.MessagesTextBox.ReadOnly = True
        Me.MessagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.MessagesTextBox.Size = New System.Drawing.Size(440, 123)
        Me.MessagesTextBox.TabIndex = 5
        Me.MessagesTextBox.Text = "<messages>"
        '
        'LayoutPanel
        '
        Me.LayoutPanel.ColumnCount = 3
        Me.LayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.LayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.LayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.LayoutPanel.Controls.Add(Me.ResourcePanel, 1, 1)
        Me.LayoutPanel.Controls.Add(Me.MessagesTextBox, 1, 3)
        Me.LayoutPanel.Controls.Add(Me.ResourceNamesListBox, 1, 2)
        Me.LayoutPanel.Controls.Add(Me.ResourceFolderLabel, 1, 4)
        Me.LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me.LayoutPanel.Name = "LayoutPanel"
        Me.LayoutPanel.RowCount = 6
        Me.LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me.LayoutPanel.Size = New System.Drawing.Size(452, 455)
        Me.LayoutPanel.TabIndex = 6
        '
        'ResourcePanel
        '
        Me.ResourcePanel.Controls.Add(Me.BackupButton)
        Me.ResourcePanel.Controls.Add(Me.RestoreButton)
        Me.ResourcePanel.Controls.Add(Me.RemoveButton)
        Me.ResourcePanel.Controls.Add(Me.AddResourceButton)
        Me.ResourcePanel.Controls.Add(Me.TestResourceButton)
        Me.ResourcePanel.Controls.Add(Me.ResourceNameTextBoxLabel)
        Me.ResourcePanel.Controls.Add(Me.ResourceNameTextBox)
        Me.ResourcePanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.ResourcePanel.Location = New System.Drawing.Point(6, 6)
        Me.ResourcePanel.Name = "ResourcePanel"
        Me.ResourcePanel.Size = New System.Drawing.Size(440, 55)
        Me.ResourcePanel.TabIndex = 7
        '
        'BackupButton
        '
        Me.BackupButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BackupButton.Location = New System.Drawing.Point(308, 3)
        Me.BackupButton.Name = "BackupButton"
        Me.BackupButton.Size = New System.Drawing.Size(64, 25)
        Me.BackupButton.TabIndex = 6
        Me.BackupButton.Text = "Backup"
        Me.BackupButton.UseVisualStyleBackColor = True
        '
        'RestoreButton
        '
        Me.RestoreButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RestoreButton.Location = New System.Drawing.Point(374, 3)
        Me.RestoreButton.Name = "RestoreButton"
        Me.RestoreButton.Size = New System.Drawing.Size(64, 25)
        Me.RestoreButton.TabIndex = 5
        Me.RestoreButton.Text = "Restore"
        Me.RestoreButton.UseVisualStyleBackColor = True
        '
        'RemoveButton
        '
        Me.RemoveButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RemoveButton.Location = New System.Drawing.Point(176, 3)
        Me.RemoveButton.Name = "RemoveButton"
        Me.RemoveButton.Size = New System.Drawing.Size(64, 25)
        Me.RemoveButton.TabIndex = 3
        Me.RemoveButton.Text = "&Remove"
        Me.RemoveButton.UseVisualStyleBackColor = True
        '
        'ResourceFolderLabel
        '
        Me.ResourceFolderLabel.AutoSize = True
        Me.ResourceFolderLabel.Location = New System.Drawing.Point(6, 435)
        Me.ResourceFolderLabel.Name = "ResourceFolderLabel"
        Me.ResourceFolderLabel.Size = New System.Drawing.Size(146, 17)
        Me.ResourceFolderLabel.TabIndex = 8
        Me.ResourceFolderLabel.Text = "location of resource file"
        '
        'ResourceNameInfoEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutPanel)
        Me.Name = "ResourceNameInfoEditor"
        Me.Size = New System.Drawing.Size(452, 455)
        Me.LayoutPanel.ResumeLayout(False)
        Me.LayoutPanel.PerformLayout()
        Me.ResourcePanel.ResumeLayout(False)
        Me.ResourcePanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents AddResourceButton As Button
    Private WithEvents ResourceNameTextBox As TextBox
    Private WithEvents ResourceNameTextBoxLabel As Label
    Private WithEvents TestResourceButton As Button
    Private WithEvents ResourceNamesListBox As ListBox
    Private WithEvents MessagesTextBox As TextBox
    Private WithEvents ResourcePanel As Panel
    Private WithEvents RemoveButton As Button
    Private WithEvents LayoutPanel As TableLayoutPanel
    Private WithEvents RestoreButton As Button
    Private WithEvents BackupButton As Button
    Private WithEvents ResourceFolderLabel As Label
End Class
