﻿''' <summary> A trace event identities. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-03-17 </para>
''' </remarks>
<TestClass()>
Public Class TraceEventIdentities

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

#End Region

#Region " SAVE TRACE EVENT IDENTITIES "

    ''' <summary> Save the trace event identities. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="fileName"> Filename of the file. </param>
    ''' <param name="openMode"> The open mode. </param>
    ''' <param name="values">   The values. </param>
    Private Shared Sub SaveTraceEventIdentities(ByVal fileName As String, ByVal openMode As OpenMode,
                                                ByVal values As IEnumerable(Of KeyValuePair(Of String, Integer)))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim fileNo As Integer = Microsoft.VisualBasic.FreeFile()
        If openMode = OpenMode.Output AndAlso System.IO.File.Exists(fileName) Then
            System.IO.File.Delete(fileName)
        End If
        Microsoft.VisualBasic.FileOpen(fileNo, fileName, openMode)
        Microsoft.VisualBasic.WriteLine(fileNo, "hex", "decimal", "description")
        For Each value As KeyValuePair(Of String, Integer) In values
            Microsoft.VisualBasic.WriteLine(fileNo, CInt(value.Value).ToString("X"), value.Value.ToString, value.Key)
        Next
        Microsoft.VisualBasic.FileClose(fileNo)
    End Sub

    ''' <summary> (Unit Test Method) enumerates test event identities. </summary>
    <TestMethod()>
    Public Sub SaveTestEventIdentities()
        Dim fileName As String = TestAssist.TraceEventProjectIdentitiesFileName
        Dim values As New List(Of KeyValuePair(Of String, Integer))
        For Each value As Global.isr.VI.Pith.My.ProjectTraceEventId In [Enum].GetValues(GetType(Global.isr.VI.Pith.My.ProjectTraceEventId))
            values.Add(New KeyValuePair(Of String, Integer)(isr.Core.EnumExtensions.Methods.Description(value), value))
        Next
        TraceEventIdentities.SaveTraceEventIdentities(fileName, OpenMode.Output, values)
    End Sub

#End Region

End Class
