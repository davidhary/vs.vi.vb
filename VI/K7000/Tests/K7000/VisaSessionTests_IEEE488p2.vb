'---------------------------------------------------------------------------------------------------
' file:		.VI\K2002\Resource_VisaSession.vb
'
' summary:	Resource visa session class
'---------------------------------------------------------------------------------------------------

Partial Public Class VisaSessionTests


#Region " OPEN/CLOSE "

    ''' <summary> (Unit Test Method) tests open close visa session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenCloseVisaSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenVisaSession(TestInfo, session, ResourceSettings.Get)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseVisaSession(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open close session base. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenCloseSessionBaseTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenSessionBase(TestInfo, session, ResourceSettings.Get)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseSessionBase(TestInfo, session)
            End Try
        End Using
    End Sub

#End Region

    ''' <summary> (Unit Test Method) tests wait for status bitmask. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub WaitForStatusBitmaskTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenVisaSession(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertWaitsForStatusBitmask(TestInfo, session.Session)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseVisaSession(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests wait for message available. </summary>
    ''' <remarks>
    ''' Initial Service Request Wait Complete Bitmask is 0x00<para>
    ''' Initial Standard Event Enable Bitmask Is 0x00</para><para>
    ''' DMM2002 status Byte 0x50 0x10 bitmask was Set</para>
    ''' </remarks>
    <TestMethod()>
    Public Sub WaitForMessageAvailableTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenVisaSession(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertWaitForMessageAvailable(TestInfo, session.Session)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseVisaSession(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests wait for operation completion. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub WaitForOperationCompletionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenVisaSession(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertWaitForOperationCompletion(TestInfo, session.Session, ":ROUT:OPEN ALL; *OPC")
                isr.VI.DeviceTests.DeviceManager.AssertWaitForServiceRequestOperationCompletion(TestInfo, session.Session, ":ROUT:OPEN ALL; *OPC")
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseVisaSession(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests toggling service request handling. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ToggleServiceRequestHandlingTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenVisaSession(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertToggleServiceRequestHandling(TestInfo, session.Session)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseVisaSession(TestInfo, session)
            End Try
        End Using
    End Sub

End Class
