'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\VisaSession.vb
'
' summary:	Visa session class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A TSP visa session. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-24 </para>
''' </remarks>
Public Class VisaSession
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="VisaSession" /> class. </summary>
    Public Sub New()
        MyBase.New()
        Me.ApplyDefaultSyntax()
    End Sub

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
            End If
            ' release unmanaged-only resources.
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SYNTAX "

    ''' <summary> Applies the default syntax. </summary>
    Private Sub ApplyDefaultSyntax()
        Me.Session.ClearExecutionStateCommand = Tsp.Syntax.Status.ClearExecutionStateCommand
        Me.Session.OperationCompletedQueryCommand = Tsp.Syntax.lua.OperationCompletedQueryCommand
        Me.Session.ResetKnownStateCommand = Tsp.Syntax.Lua.ResetKnownStateCommand
        Me.Session.ServiceRequestEnableCommandFormat = Tsp.Syntax.Status.ServiceRequestEnableCommandFormat
        Me.Session.ServiceRequestEnableQueryCommand = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand
        Me.Session.StandardEventStatusQueryCommand = Tsp.Syntax.Status.StandardEventStatusQueryCommand
        Me.Session.StandardEventEnableQueryCommand = Tsp.Syntax.Status.StandardEventEnableQueryCommand
        Me.Session.StandardServiceEnableCommandFormat = Tsp.Syntax.Status.StandardServiceEnableCommandFormat
        Me.Session.WaitCommand = Tsp.Syntax.Lua.WaitCommand

        Me.Session.ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        Me.Session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        Me.Session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        Me.Session.OperationEventBit = VI.Pith.ServiceRequests.OperationEvent
        Me.Session.QuestionableEventBit = VI.Pith.ServiceRequests.QuestionableEvent
        Me.Session.RequestingServiceBit = VI.Pith.ServiceRequests.RequestingService
        Me.Session.StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
        Me.Session.SystemEventBit = VI.Pith.ServiceRequests.SystemEvent
    End Sub

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Applies the settings. </summary>
    Protected Overrides Sub ApplySettings()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
