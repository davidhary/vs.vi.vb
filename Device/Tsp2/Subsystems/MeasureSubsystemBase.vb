'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Subsystems\MeasureSubsystemBase.vb
'
' summary:	Measure subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary>
''' Defines the contract that must be implemented by a Source Measure Unit Measure Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class MeasureSubsystemBase
    Inherits VI.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MeasureSubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    ''' <param name="readingAmounts">  A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem, readingAmounts)
        Me.DefaultMeasurementUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefaultFunctionRange = VI.Pith.Ranges.NonnegativeFullRange
        Me.DefaultFunctionModeDecimalPlaces = 3
        Me.FunctionModeReadWrites.AddReplace(VI.SenseFunctionModes.VoltageDC, "smu.FUNC_DC_VOLTAGE", VI.SenseFunctionModes.VoltageDC.DescriptionUntil)
        Me.FunctionModeReadWrites.AddReplace(VI.SenseFunctionModes.CurrentDC, "smu.FUNC_DC_CURRENT", VI.SenseFunctionModes.CurrentDC.DescriptionUntil)
        Me.FunctionModeReadWrites.AddReplace(VI.SenseFunctionModes.Resistance, "smu.FUNC_RESISTANCE", VI.SenseFunctionModes.Resistance.DescriptionUntil)
    End Sub

#End Region

#Region " AUTO RANGE STATE "

    ''' <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
    ''' <remarks>
    ''' When this command is set to off, you must set the range. If you do not set the range, the
    ''' instrument remains at the range that was selected by auto range. When this command Is set to
    ''' on, the instrument automatically goes to the most sensitive range to perform the measurement.
    ''' If a range Is manually selected through the front panel Or a remote command, this command Is
    ''' automatically set to off. Auto range selects the best range In which To measure the signal
    ''' that Is applied To the input terminals of the instrument. When auto range Is enabled, the
    ''' range increases at 120 percent of range And decreases occurs When the reading Is less than 10
    ''' percent Of nominal range. For example, If you are On the 1 volt range And auto range Is
    ''' enabled, the instrument auto ranges up To the 10 volt range When the measurement exceeds 1.2
    ''' volts. It auto ranges down To the 100 mV range When the measurement falls below 1 volt.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Overrides Property AutoRangeEnabled As Boolean?
        Get
            Return MyBase.AutoRangeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoRangeEnabled, value) Then
                Me.AutoRangeState = If(value.HasValue, If(value.Value, OnOffState.On, OnOffState.Off), New OnOffState?)
                MyBase.AutoRangeEnabled = value
            End If
        End Set
    End Property

    ''' <summary> State of the automatic range. </summary>
    Private _AutoRangeState As OnOffState?

    ''' <summary> Gets or sets the Auto Range. </summary>
    ''' <value> The automatic range state. </value>
    Public Property AutoRangeState() As OnOffState?
        Get
            Return Me._AutoRangeState
        End Get
        Protected Set(ByVal value As OnOffState?)
            If Not Nullable.Equals(value, Me.AutoRangeState) Then
                Me._AutoRangeState = value
                Me.AutoRangeEnabled = If(value.HasValue, (value.Value = OnOffState.On), New Boolean?)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the AutoRange state. </summary>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> An OnOffState? </returns>
    Public Function ApplyAutoRangeState(ByVal value As OnOffState) As OnOffState?
        Me.WriteAutoRangeState(value)
        Return Me.QueryAutoRangeState()
    End Function

    ''' <summary> Gets or sets the Auto Range state query command. </summary>
    ''' <value> The Auto Range state query command. </value>
    Protected Overridable Property AutoRangeStateQueryCommand As String = "_G.print(_G.smu.measure.autorange)"

    ''' <summary> Queries automatic range state. </summary>
    ''' <returns> The automatic range state. </returns>
    Public Function QueryAutoRangeState() As OnOffState?
        Me.Session.LastAction = Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"Reading {NameOf(AutoRangeState)};. ")
        Me.Session.LastNodeNumber = New Integer?
        Dim mode As String = Me.AutoRangeState.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd(Me.AutoRangeStateQueryCommand)
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = $"Failed fetching {NameOf(AutoRangeState)}"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.AutoRangeState = New OnOffState?
        Else
            Dim se As New StringEnumerator(Of OnOffState)
            Me.AutoRangeState = se.ParseContained(mode.BuildDelimitedValue)
        End If
        Return Me.AutoRangeState
    End Function

    ''' <summary> The Auto Range state command format. </summary>
    ''' <value> The automatic range state command format. </value>
    Protected Overridable Property AutoRangeStateCommandFormat As String = "_G.smu.measure.autorange={0}"

    ''' <summary> Writes an automatic range state. </summary>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> An OnOffState. </returns>
    Public Function WriteAutoRangeState(ByVal value As OnOffState) As OnOffState
        Me.Session.LastAction = Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"Writing {NameOf(AutoRangeState)}={value};. ")
        Me.Session.LastNodeNumber = New Integer?
        Me.Session.WriteLine(Me.AutoRangeStateCommandFormat, value.ExtractBetween)
        Me.AutoRangeState = value
        Return value
    End Function

    #End Region

    #Region " MEASURE UNIT "

    ''' <summary> Converts a a measure unit to a measurement unit. </summary>
    ''' <param name="value"> The Measure Unit. </param>
    ''' <returns> Value as an Arebis.TypedUnits.Unit. </returns>
    Public Function ToMeasurementUnit(ByVal value As Tsp2.MeasureUnits) As Arebis.TypedUnits.Unit
        Dim result As Arebis.TypedUnits.Unit = Me.DefaultFunctionUnit
        Select Case value
            Case Tsp2.MeasureUnits.Ampere
                result = Arebis.StandardUnits.ElectricUnits.Ampere
            Case Tsp2.MeasureUnits.Volt
                result = Arebis.StandardUnits.ElectricUnits.Volt
            Case Tsp2.MeasureUnits.Ohm
                result = Arebis.StandardUnits.ElectricUnits.Ohm
            Case Tsp2.MeasureUnits.Watt
                result = Arebis.StandardUnits.EnergyUnits.Watt
        End Select
        Return result
    End Function

    ''' <summary> Writes and reads back the Measure Unit. </summary>
    ''' <param name="value"> The  Measure Unit. </param>
    ''' <returns> The <see cref="MeasurementUnit">Measure Unit</see> or none if unknown. </returns>
    Public Function ApplyMeasureUnit(ByVal value As MeasureUnits) As MeasureUnits?
        Me.WriteMeasureUnit(value)
        Return Me.QueryMeasureUnit
    End Function

    ''' <summary> The Unit. </summary>
    Private _MeasureUnit As MeasureUnits?

    ''' <summary>
    ''' Gets or sets the cached measure Unit.  This is the actual unit for measurement.
    ''' </summary>
    ''' <value>
    ''' The <see cref="MeasurementUnit">Measure Unit</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property MeasureUnit As MeasureUnits?
        Get
            Return Me._MeasureUnit
        End Get
        Protected Set(ByVal value As MeasureUnits?)
            If Not Nullable.Equals(Me.MeasureUnit, value) Then
                Me._MeasureUnit = value
                If value.HasValue Then
                    Me.MeasurementUnit = Me.ToMeasurementUnit(value.Value)
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Measure Unit query command. </summary>
    ''' <value> The Unit query command. </value>
    Protected Overridable Property MeasureUnitQueryCommand As String = "_G.print(_G.smu.measure.unit)"

    ''' <summary> Queries the Measure Unit. </summary>
    ''' <returns> The <see cref="MeasureUnit">Measure Unit</see> or none if unknown. </returns>
    Public Overridable Function QueryMeasureUnit() As MeasureUnits?
        Dim mode As String = Me.MeasureUnit.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd(Me.MeasureUnitQueryCommand)
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = $"Failed fetching {NameOf(MeasureSubsystemBase)}.{NameOf(MeasureUnit)}"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.MeasureUnit = New MeasureUnits?
        Else
            Dim se As New StringEnumerator(Of MeasureUnits)
            Me.MeasureUnit = se.ParseContained(mode.BuildDelimitedValue)
        End If
        Return Me.MeasureUnit
    End Function

    ''' <summary> Gets or sets the Measure Unit command format. </summary>
    ''' <value> The Unit command format. </value>
    Protected Overridable Property MeasureUnitCommandFormat As String = "_G.smu.measure.unit={0}"

    ''' <summary> Writes the Measure Unit without reading back the value from the device. </summary>
    ''' <param name="value"> The Unit. </param>
    ''' <returns> The <see cref="MeasureUnit">Measure Unit</see> or none if unknown. </returns>
    Public Overridable Function WriteMeasureUnit(ByVal value As MeasureUnits) As MeasureUnits?
        Me.Session.WriteLine(Me.MeasureUnitCommandFormat, value.ExtractBetween())
        Me.MeasureUnit = value
        Return Me.MeasureUnit
    End Function

#End Region

End Class

''' <summary> Specifies the units. </summary>
<Flags>
Public Enum MeasureUnits

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the volt option. </summary>
    <ComponentModel.Description("Volt (smu.UNIT_VOLT)")>
    Volt = 1

    ''' <summary> An enum constant representing the ohm option. </summary>
    <ComponentModel.Description("Ohm (smu.UNIT_OHM)")>
    Ohm = 2

    ''' <summary> An enum constant representing the ampere option. </summary>
    <ComponentModel.Description("Ampere (smu.UNIT_AMP)")>
    Ampere = 4

    ''' <summary> An enum constant representing the watt option. </summary>
    <ComponentModel.Description("Watt (smu.UNIT_WATT)")>
    Watt = 8
End Enum


