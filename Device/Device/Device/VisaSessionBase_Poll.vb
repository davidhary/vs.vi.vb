'---------------------------------------------------------------------------------------------------
' file:		.VI\Device\VisaSessionBase_Poll.vb
'
' summary:	Visa session base poll class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Pith

Partial Public Class VisaSessionBase

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _PollTimer As System.Timers.Timer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the poll synchronizing object. </summary>
    ''' <value> The poll synchronizing object. </value>
    Public Property PollSynchronizingObject As ComponentModel.ISynchronizeInvoke
        Get
            Return Me._PollTimer.SynchronizingObject
        End Get
        Set(value As ComponentModel.ISynchronizeInvoke)
            Me._PollTimer.SynchronizingObject = value
        End Set
    End Property

    ''' <summary> The poll reading. </summary>
    Private _PollReading As String

    ''' <summary> Gets or sets the poll reading. </summary>
    ''' <value> The poll reading. </value>
    Public Property PollReading As String
        Get
            Return Me._PollReading
        End Get
        Set(value As String)
            If Not String.Equals(Me.PollReading, value) Then
                Me._PollReading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True to poll automatic read. </summary>
    Private _PollAutoRead As Boolean

    ''' <summary> Gets or sets the automatic read polling option . </summary>
    ''' <value> The automatic read status. </value>
    Public Property PollAutoRead As Boolean
        Get
            Return Me._PollAutoRead
        End Get
        Set(value As Boolean)
            If Me.PollAutoRead <> value Then
                Me._PollAutoRead = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the poll Timespan. </summary>
    ''' <value> The poll Timespan. </value>
    Public Property PollTimespan As TimeSpan
        Get
            Return TimeSpan.FromMilliseconds(Me._PollTimer.Interval)
        End Get
        Set(value As TimeSpan)
            If Me.PollTimespan <> value Then
                Me._PollTimer.Interval = value.TotalMilliseconds
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The poll enabled. </summary>
    Private _PollEnabled As isr.Core.Constructs.ConcurrentToken(Of Boolean)

    ''' <summary> Gets or sets the poll enabled. </summary>
    ''' <value> The poll enabled. </value>
    Public Property PollEnabled As Boolean
        Get
            Return Me._PollEnabled.Value
        End Get
        Set(value As Boolean)
            If Me.PollEnabled <> value Then
                If value Then
                    ' turn off the poll message available sentinel.
                    Me.PollMessageAvailable = False
                    ' enable only if service request is not enabled. 
                    Me._PollTimer.Enabled = Not Me.Session.ServiceRequestEventEnabled
                End If
                Me._PollEnabled.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The poll message available bitmask. </summary>
    Private _PollMessageAvailableBitmask As Integer = ServiceRequests.MessageAvailable

    ''' <summary> Gets or sets the poll message available bitmask. </summary>
    ''' <value> The message available bitmask. </value>
    Public Property PollMessageAvailableBitmask As Integer
        Get
            Return Me._PollMessageAvailableBitmask
        End Get
        Set(value As Integer)
            If Me.PollMessageAvailableBitmask <> value Then
                Me._PollMessageAvailableBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries if a message is available. </summary>
    ''' <param name="statusByte"> The status byte. </param>
    ''' <returns> <c>true</c> if a message is available; otherwise <c>false</c> </returns>
    Private Function IsPollMessageAvailable(ByVal statusByte As Integer) As Boolean
        Return SessionBase.IsFullyMasked(statusByte, Me.PollMessageAvailableBitmask)
    End Function

    ''' <summary> True if poll message available. </summary>
    Private _PollMessageAvailable As Boolean

    ''' <summary> Gets or sets the poll message available status. </summary>
    ''' <value> The poll message available. </value>
    Public Property PollMessageAvailable As Boolean
        Get
            Return Me._PollMessageAvailable
        End Get
        Set(value As Boolean)
            If Me.PollMessageAvailable <> value Then
                Me._PollMessageAvailable = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Event handler. Called by _PollTimer for tick events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PollTimer.Elapsed
        Dim tmr As Timers.Timer = TryCast(sender, Timers.Timer)
        If tmr Is Nothing OrElse Not Me.IsSessionOpen Then Return
        Try
            ' disable the timer.
            tmr.Stop()
            tmr.Enabled = False
            If Not Me.Session.ServiceRequestEventEnabled Then
                Me.Session.DoEventsStatusReadDelay()
                If Me.IsPollMessageAvailable(Me.Session.ReadStatusRegister) Then
                    If Me.PollAutoRead Then
                        Me.PollReading = String.Empty
                        ' allow other threads to execute
                        Me.Session.DoEventsReadDelay()
                        ' result is also stored in the last message received.
                        Me.PollReading = Me.Session.ReadFreeLineTrimEnd
                        ' read the status register after delay allowing the status to stabilize.
                        Me.Session.DelayReadStatusRegister()
                    End If
                    Me.PollMessageAvailable = True
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            tmr.Enabled = Nullable.Equals(True, Me._PollEnabled?.Value AndAlso Not Me.Session.ServiceRequestEventEnabled)
        End Try
    End Sub

    ''' <summary> Starts awaiting poll reading task. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A Threading.Tasks.Task. </returns>
    Public Function StartAwaitingPollReadingTask(ByVal timeout As TimeSpan) As Threading.Tasks.Task
        Return Threading.Tasks.Task.Factory.StartNew(Sub()
                                                         Dim ticks As Long = timeout.Ticks
                                                         Dim sw As Stopwatch = Stopwatch.StartNew()
                                                         Do While Me.PollEnabled AndAlso Me.PollAutoRead AndAlso
                                                            String.IsNullOrWhiteSpace(Me.PollReading) AndAlso sw.ElapsedTicks < ticks
                                                             Threading.Thread.SpinWait(1)
                                                             isr.Core.ApplianceBase.DoEvents()
                                                         Loop
                                                     End Sub)
    End Function

    ''' <summary> Gets the poll timer enabled. </summary>
    ''' <value> The poll timer enabled. </value>
    Public ReadOnly Property PollTimerEnabled As Boolean
        Get
            Return Me._PollTimer.Enabled
        End Get
    End Property

    ''' <summary> Starts awaiting poll timer disabled task. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A Threading.Tasks.Task. </returns>
    Public Function StartAwaitingPollTimerDisabledTask(ByVal timeout As TimeSpan) As Threading.Tasks.Task
        Return Threading.Tasks.Task.Factory.StartNew(Sub()
                                                         Dim ticks As Long = timeout.Ticks
                                                         Dim sw As Stopwatch = Stopwatch.StartNew()
                                                         Do While Me.PollTimerEnabled AndAlso sw.ElapsedTicks < ticks
                                                             Threading.Thread.SpinWait(1)
                                                             isr.Core.ApplianceBase.DoEvents()
                                                         Loop
                                                     End Sub)
    End Function

End Class
