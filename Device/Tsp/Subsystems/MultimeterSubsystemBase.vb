'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\MultimeterSubsystemBase.vb
'
' summary:	Multimeter subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

''' <summary> Defines a Multimeter Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-15 </para>
''' </remarks>
Public MustInherit Class MultimeterSubsystemBase
    Inherits VI.MultimeterSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem, readingAmounts)
        Me.FunctionModeReadWrites.Clear()
        Me.FunctionModeReadWrites.Add(VI.MultimeterFunctionModes.CurrentAC, "accurrent", VI.MultimeterFunctionModes.CurrentAC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.MultimeterFunctionModes.ResistanceCommonSide, "commonsideohms", VI.MultimeterFunctionModes.ResistanceCommonSide.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.MultimeterFunctionModes.ResistanceFourWire, "fourwireohms", VI.MultimeterFunctionModes.ResistanceFourWire.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.MultimeterFunctionModes.ResistanceTwoWire, "twowireohms", VI.MultimeterFunctionModes.ResistanceTwoWire.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.MultimeterFunctionModes.VoltageAC, "acvolts", VI.MultimeterFunctionModes.VoltageAC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.MultimeterFunctionModes.VoltageDC, "dcvolts", VI.MultimeterFunctionModes.VoltageDC.DescriptionUntil)
        MyBase.SupportedFunctionModes = VI.MultimeterFunctionModes.CurrentAC
        For Each kvp As Pith.EnumReadWrite In MyBase.FunctionModeReadWrites
            Me.SupportedFunctionModes = Me.SupportedFunctionModes Or CType(kvp.EnumValue, VI.MultimeterFunctionModes)
        Next
    End Sub

#End Region

End Class

