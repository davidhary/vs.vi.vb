'---------------------------------------------------------------------------------------------------
' file:		.\Device\VisaSessionBase_Talk.vb
'
' summary:	Visa session base talk class
'---------------------------------------------------------------------------------------------------
Imports isr.Core

Partial Public Class VisaSessionBase

#Region " I TALKER "

    ''' <summary> Assigns a talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(ByVal talker As ITraceMessageTalker)
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identifies talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary> Removes the listener described by listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub RemoveListener(ByVal listener As IMessageListener)
        MyBase.RemoveListener(listener)
    End Sub

    ''' <summary> Adds a listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub AddListener(ByVal listener As IMessageListener)
        MyBase.AddListener(listener)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        MyBase.ApplyTalkerTraceLevel(listenerType, value)
        If listenerType = ListenerType.Logger Then
            Me.NotifyPropertyChanged(NameOf(VisaSessionBase.TraceLogLevel))
        Else
            Me.NotifyPropertyChanged(NameOf(VisaSessionBase.TraceShowLevel))
        End If
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker)
        MyBase.ApplyTalkerTraceLevels(talker)
    End Sub

    ''' <summary> Applies the talker listeners trace levels described by talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker)
        MyBase.ApplyListenerTraceLevels(talker)
    End Sub

#End Region

End Class
