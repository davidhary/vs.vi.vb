''' <summary> K2450 Current Source unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2450")>
Public Class CurrentSourceTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K2450Tests.CurrentSourceSettings.Get.Exists, $"{GetType(K2450Tests.CurrentSourceSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SOURCE CURRENT MEASURE VOLTAGE "

    ''' <summary> Source current measure voltage. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub SourceCurrentMeasureVoltage(ByVal device As VI.Tsp2.K2450.K2450Device)

        Dim expectedPowerLineCycles As Double = CurrentSourceSettings.Get.PowerLineCycles
        Dim actualPowerLineCycles As Double = device.MeasureSubsystem.ApplyPowerLineCycles(expectedPowerLineCycles).GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, device.StatusSubsystem.LineFrequency.Value / TimeSpan.TicksPerSecond,
                    $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.PowerLineCycles)} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}")

        Dim expectedBoolean As Boolean = CurrentSourceSettings.Get.AutoRangeEnabled
        Dim actualBoolean As Boolean = device.MeasureSubsystem.ApplyAutoRangeEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.AutoRangeEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        expectedBoolean = CurrentSourceSettings.Get.AutoZeroEnabled
        actualBoolean = device.MeasureSubsystem.ApplyAutoZeroEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.AutoZeroEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        expectedBoolean = CurrentSourceSettings.Get.FrontTerminalsSelected
        actualBoolean = device.MeasureSubsystem.ApplyFrontTerminalsSelected(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.FrontTerminalsSelected)} is {actualBoolean}; expected {expectedBoolean}")

        Dim expectedFunctionMode As VI.SourceFunctionModes = CurrentSourceSettings.Get.SourceFunction
        Dim SourceFunction As VI.SourceFunctionModes = device.SourceSubsystem.ApplyFunctionMode(expectedFunctionMode).GetValueOrDefault(VI.SourceFunctionModes.None)
        Assert.AreEqual(expectedFunctionMode, SourceFunction, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.FunctionMode)} is {SourceFunction} ; expected {expectedFunctionMode}")

        Dim expectedMeasureFunctionMode As VI.SenseFunctionModes = CurrentSourceSettings.Get.SenseFunction
        Dim measureFunction As VI.SenseFunctionModes = device.MeasureSubsystem.ApplyFunctionMode(expectedMeasureFunctionMode).GetValueOrDefault(VI.SenseFunctionModes.Resistance)
        Assert.AreEqual(expectedMeasureFunctionMode, measureFunction, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.FunctionMode)} is {measureFunction} ; expected {expectedMeasureFunctionMode}")

        expectedBoolean = CurrentSourceSettings.Get.RemoteSenseSelected
        actualBoolean = device.MeasureSubsystem.ApplyRemoteSenseSelected(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.RemoteSenseSelected)} is {actualBoolean}; expected {expectedBoolean}")

        Dim expectedSourceLevel As Double = CurrentSourceSettings.Get.SourceLevel
        Dim actualSourceLevel As Double = device.SourceSubsystem.ApplyLevel(expectedSourceLevel).GetValueOrDefault(0)
        Assert.AreEqual(expectedSourceLevel, actualSourceLevel, 0.000001,
                    $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.Level)} is {actualSourceLevel:G5}; expected {expectedSourceLevel:G5}")

        Dim expectedSourceLimit As Double = 2 * CurrentSourceSettings.Get.SourceLevel * CurrentSourceSettings.Get.LoadResistance
        Dim actualSourceLimit As Double = device.SourceSubsystem.ApplyLimit(expectedSourceLimit).GetValueOrDefault(0)
        Assert.AreEqual(expectedSourceLimit, actualSourceLimit, 0.000001,
                        $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.Limit)} is {actualSourceLimit:G5}; expected {expectedSourceLimit:G5}")

        expectedBoolean = CurrentSourceSettings.Get.SourceReadBackEnabled
        actualBoolean = device.SourceSubsystem.ApplyReadBackEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.ReadBackEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        ' set the reading to display voltage units
        device.MeasureSubsystem.ReadingAmounts.PrimaryReading.ApplyUnit(device.MeasureSubsystem.FunctionUnit)

        ' turn on the output
        isr.VI.DeviceTests.DeviceManager.AssertToggleOutput(device.SourceSubsystem, True)

        For i As Integer = 1 To 2

            Dim measuredVoltage As Double = device.MeasureSubsystem.MeasurePrimaryReading.GetValueOrDefault(-1)
            Dim expectedVoltage As Double = CurrentSourceSettings.Get.SourceLevel * CurrentSourceSettings.Get.LoadResistance
            Dim epsilon As Double = expectedVoltage * CurrentSourceSettings.Get.MeasurementTolerance
            Assert.AreEqual(expectedVoltage, measuredVoltage, epsilon,
                        $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.PrimaryReadingValue)} is {measuredVoltage}; expected {expectedVoltage} within {epsilon}")

            If CurrentSourceSettings.Get.SourceReadBackEnabled Then
                ' read back the last source value
                Dim measuredCurrent As Double = device.SourceSubsystem.ParseReadBackBufferAmount()
                Dim expectedCurrent As Double = CurrentSourceSettings.Get.SourceLevel
                epsilon = expectedCurrent * CurrentSourceSettings.Get.MeasurementTolerance
                Assert.AreEqual(expectedCurrent, measuredCurrent, epsilon,
                        $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.ReadBackAmount)} is {measuredCurrent}; expected {expectedCurrent} within {epsilon}")
            End If

        Next

        ' turn off the output
        isr.VI.DeviceTests.DeviceManager.AssertToggleOutput(device.SourceSubsystem, False)

    End Sub

    ''' <summary> (Unit Test Method) tests source current measure voltage. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SourceCurrentMeasureVoltageTest()
        Using device As VI.Tsp2.K2450.K2450Device = VI.Tsp2.K2450.K2450Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
            Try
                CurrentSourceTests.SourceCurrentMeasureVoltage(device)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertToggleOutput(device.SourceSubsystem, False)
            End Try
            K2450Tests.DeviceManager.CloseSession(TestInfo, device)
        End Using
    End Sub

#End Region

End Class

