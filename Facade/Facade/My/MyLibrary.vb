'---------------------------------------------------------------------------------------------------
' file:		.\My\MyLibrary.vb
'
' summary:	My library class
'---------------------------------------------------------------------------------------------------
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = VI.Pith.My.ProjectTraceEventId.Facade

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "VI Facade Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Facade Virtual Instrument Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "VI.Facade"

        ''' <summary> Provide public access to the shared settings. </summary>
        ''' <remarks>
        ''' Although VS allows setting access to the settings object as Public, the
        ''' <see cref="MySettingsProperty"/> is often set with Friend access when editing the settings in
        ''' the IDE.  This property aims at overcoming this issue.
        ''' </remarks>
        ''' <value> The settings. </value>
        Public Shared ReadOnly Property Settings() As MySettings
            Get
                Return My.Settings
            End Get
        End Property


    End Class

End Namespace


