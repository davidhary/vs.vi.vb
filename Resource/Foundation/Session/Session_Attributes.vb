'---------------------------------------------------------------------------------------------------
' file:		.\Session\Session_Attributes.vb
'
' summary:	Session attributes class
'---------------------------------------------------------------------------------------------------

Partial Public Class Session

#Region " ATTRIBUTES "

    ''' <summary> The vendor unsupported attributes. </summary>
    Private Shared _VendorUnsupportedAttributes As ObjectModel.Collection(Of Ivi.Visa.NativeVisaAttribute)

    ''' <summary> Query if 'value' is supported vendor attribute. </summary>
    ''' <remarks> David, 2020-04-14. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> True if supported vendor attribute, false if not. </returns>
    Private Shared Function IsSupportedVendorAttribute(ByVal value As Ivi.Visa.NativeVisaAttribute) As Boolean
        Return Not Session.VendorUnsupportedAttributes.Contains(value)
    End Function

    ''' <summary> Gets or sets the timeout attribute value. </summary>
    ''' <value> The timeout attribute value. </value>
    Private Property TimeoutAttributeValue As Integer = 2000I

    ''' <summary> Gets or sets the timeout attribute. </summary>
    ''' <value> The timeout attribute. </value>
    Public Property TimeoutAttribute As Integer
        Get
            Me.TimeoutAttributeValue = Me.ReadAttribute(Ivi.Visa.NativeVisaAttribute.TimeoutValue, Me.TimeoutAttributeValue)
            Return Me.TimeoutAttributeValue
        End Get
        Set(value As Integer)
            Me.TimeoutAttributeValue = value
            Me.WriteAttribute(Ivi.Visa.NativeVisaAttribute.TimeoutValue, value)
        End Set
    End Property

    ''' <summary> Gets or sets the default buffer size attribute. </summary>
    ''' <value> The default buffer size attribute. </value>
    Private Property DefaultBufferSizeAttribute As Integer = 1024

    ''' <summary> Gets read buffer size. </summary>
    ''' <remarks> David, 2020-04-14. </remarks>
    ''' <returns> The read buffer size. </returns>
    Private Function ReadBufferSizeAttribute() As Integer
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        Return If(s IsNot Nothing, s.GetAttributeInt32(Ivi.Visa.NativeVisaAttribute.ReadBufferSize), Me.DefaultBufferSizeAttribute)
    End Function

#End Region

#Region " ATTRIBUTE READ WRITE "

    ''' <summary> Query if 'visaSession' 'Attribute' is supported. </summary>
    ''' <param name="nativeVisaSession"> A native visa session. </param>
    ''' <param name="attribute">         The attribute. </param>
    ''' <returns> <c>true</c> if supported; otherwise <c>false</c> </returns>
    Private Shared Function IsSupported(ByVal nativeVisaSession As Ivi.Visa.INativeVisaSession, ByVal attribute As Ivi.Visa.NativeVisaAttribute) As Boolean
        Dim result As Boolean = False
        If nativeVisaSession IsNot Nothing Then
            result = Session.IsSupportedVendorAttribute(attribute)
            If result AndAlso nativeVisaSession.HardwareInterfaceType <> Ivi.Visa.HardwareInterfaceType.Gpib AndAlso
                attribute.ToString.StartsWith(Ivi.Visa.HardwareInterfaceType.Gpib.ToString, StringComparison.OrdinalIgnoreCase) Then
                result = False
            End If
            If result AndAlso nativeVisaSession.HardwareInterfaceType <> Ivi.Visa.HardwareInterfaceType.Pxi AndAlso
                attribute.ToString.StartsWith(Ivi.Visa.HardwareInterfaceType.Pxi.ToString, StringComparison.OrdinalIgnoreCase) Then
                result = False
            End If
            If result AndAlso nativeVisaSession.HardwareInterfaceType <> Ivi.Visa.HardwareInterfaceType.Serial AndAlso
                attribute.ToString.StartsWith(Ivi.Visa.HardwareInterfaceType.Serial.ToString, StringComparison.OrdinalIgnoreCase) Then
                result = False
            End If
            If result AndAlso nativeVisaSession.HardwareInterfaceType <> Ivi.Visa.HardwareInterfaceType.Tcp AndAlso
                attribute.ToString.StartsWith(Ivi.Visa.HardwareInterfaceType.Tcp.ToString, StringComparison.OrdinalIgnoreCase) Then
                result = False
            End If
            If result AndAlso nativeVisaSession.HardwareInterfaceType <> Ivi.Visa.HardwareInterfaceType.Usb AndAlso
                attribute.ToString.StartsWith(Ivi.Visa.HardwareInterfaceType.Usb.ToString, StringComparison.OrdinalIgnoreCase) Then
                result = False
            End If
            If result AndAlso nativeVisaSession.HardwareInterfaceType <> Ivi.Visa.HardwareInterfaceType.Vxi AndAlso
                attribute.ToString.StartsWith(Ivi.Visa.HardwareInterfaceType.Vxi.ToString, StringComparison.OrdinalIgnoreCase) Then
                result = False
            End If
        End If
        Return result
    End Function

    ''' <summary> Reads an attribute. </summary>
    ''' <param name="attribute">    The attribute. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> The attribute. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Function ReadAttribute(ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal defaultValue As Boolean) As Boolean
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        Return If(s IsNot Nothing AndAlso Session.IsSupported(s, attribute), s.GetAttributeBoolean(attribute), defaultValue)
    End Function

    ''' <summary> Writes an attribute. </summary>
    ''' <param name="attribute"> The attribute. </param>
    ''' <param name="value">     The value. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub WriteAttribute(ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal value As Boolean)
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        If s IsNot Nothing AndAlso IsSupported(s, attribute) Then
            s.SetAttributeBoolean(attribute, value)
        End If
    End Sub

    ''' <summary> Reads an attribute. </summary>
    ''' <param name="attribute">    The attribute. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> The attribute. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Function ReadAttribute(ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal defaultValue As Byte) As Byte
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        Return If(s IsNot Nothing AndAlso Session.IsSupported(s, attribute), s.GetAttributeByte(attribute), defaultValue)
    End Function

    ''' <summary> Writes an attribute. </summary>
    ''' <param name="attribute"> The attribute. </param>
    ''' <param name="value">     The value. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub WriteAttribute(ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal value As Byte)
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        If s IsNot Nothing AndAlso Session.IsSupported(s, attribute) Then
            s.SetAttributeByte(attribute, value)
        End If
    End Sub

    ''' <summary> Reads an attribute. </summary>
    ''' <param name="attribute">    The attribute. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> The attribute. </returns>
    Private Function ReadAttribute(ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal defaultValue As Integer) As Integer
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        Return If(s IsNot Nothing AndAlso Session.IsSupported(s, attribute), s.GetAttributeInt32(attribute), defaultValue)
    End Function

    ''' <summary> Writes an attribute. </summary>
    ''' <param name="attribute"> The attribute. </param>
    ''' <param name="value">     The value. </param>
    Private Sub WriteAttribute(ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal value As Integer)
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(Me.VisaSession, Ivi.Visa.INativeVisaSession)
        If s IsNot Nothing AndAlso Session.IsSupported(s, attribute) Then
            s.SetAttributeInt32(attribute, value)
        End If
    End Sub

#End Region

End Class
