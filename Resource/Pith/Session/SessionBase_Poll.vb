﻿Imports isr.VI.Pith.EscapeSequencesExtensions

Partial Public Class SessionBase

    Private WithEvents _PollTimer As System.Timers.Timer

    ''' <summary> Gets or sets the poll synchronizing object. </summary>
    ''' <value> The poll synchronizing object. </value>
    Public Property PollSynchronizingObject As ComponentModel.ISynchronizeInvoke
        Get
            Return Me._PollTimer.SynchronizingObject
        End Get
        Set(value As ComponentModel.ISynchronizeInvoke)
            Me._PollTimer.SynchronizingObject = value
        End Set
    End Property

    Private _PollReading As String

    ''' <summary> Gets or sets the poll reading. </summary>
    ''' <value> The poll reading. </value>

    Public Property PollReading As String
        Get
            Return Me._PollReading
        End Get
        Set(value As String)
            If Not String.Equals(Me.PollReading, value) Then
                Me._PollReading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _PollAutoRead As Boolean

    ''' <summary> Gets or sets the automatic read polling option . </summary>
    ''' <value> The automatic read status. </value>
    Public Property PollAutoRead As Boolean
        Get
            Return Me._PollAutoRead
        End Get
        Set(value As Boolean)
            If Me.PollAutoRead <> value Then
                Me._PollAutoRead = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _PollTimespan As TimeSpan

    ''' <summary> Gets or sets the poll Timespan. </summary>
    ''' <value> The poll Timespan. </value>
    Public Property PollTimespan As TimeSpan
        Get
            Return Me._PollTimespan
        End Get
        Set(value As TimeSpan)
            If Me.PollTimespan <> value Then
                Me._PollTimespan = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the poll enabled. </summary>
    ''' <value> The poll enabled. </value>
    Public Property PollEnabled As Boolean
        Get
            Return Me._PollTimer.Enabled
        End Get
        Set(value As Boolean)
            If Me.PollEnabled <> value Then
                ' turn off the poll message available sentinel.
                If value Then Me.PollMessageAvailable = False
                Me._PollTimer.Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _PollMessageAvailableBitmask As Integer = ServiceRequests.MessageAvailable

    ''' <summary> Gets or sets the poll message available bitmask. </summary>
    ''' <value> The message available bitmask. </value>
    Public Property PollMessageAvailableBitmask As Integer
        Get
            Return Me._PollMessageAvailableBitmask
        End Get
        Set(value As Integer)
            If Me.PollMessageAvailableBitmask <> value Then
                Me._PollMessageAvailableBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries if a message is available. </summary>
    ''' <param name="statusByte"> The status byte. </param>
    ''' <returns> <c>true</c> if a message is available; otherwise <c>false</c> </returns>
    Private Function IsPollMessageAvailable(ByVal statusByte As Integer) As Boolean
        Return SessionBase.IsFullyMasked(statusByte, Me.PollMessageAvailableBitmask)
    End Function

    Private _PollMessageAvailable As Boolean

    ''' <summary> Gets or sets the poll message available status. </summary>
    ''' <value> The poll message available. </value>
    Public Property PollMessageAvailable As Boolean
        Get
            Return Me._PollMessageAvailable
        End Get
        Set(value As Boolean)
            If Me.PollMessageAvailable <> value Then
                Me._PollMessageAvailable = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Event handler. Called by _PollTimer for tick events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PollTimer.Elapsed
        If sender Is Nothing OrElse Not Me.IsSessionOpen OrElse Me._PollTimer Is Nothing Then Return
        Try
            ' disable the timer.
            Me._PollTimer.Stop()
            Me._PollTimer.Enabled = False
            If Me.IsPollMessageAvailable(Me.StatusByte) OrElse Me.IsPollMessageAvailable(Me.ReadStatusRegister) Then
                If Me.PollAutoRead Then
                    Threading.Thread.Sleep(10)
                    ' result is also stored in the last message received.
                    Me.PollReading = Me.ReadFreeLineTrimEnd

                End If
                Me.PollMessageAvailable = True
            End If
        Catch
        Finally
            _PollTimer.Enabled = True
        End Try
    End Sub

End Class
