
Partial Public Class ArmLayerSubsystemBase

#Region " ARM LAYER BYPASS MODE "

    ''' <summary> Define arm layer bypass mode read writes. </summary>
    Private Sub DefineArmLayerBypassModeReadWrites()
        Me._ArmLayerBypassModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As TriggerLayerBypassModes In [Enum].GetValues(GetType(TriggerLayerBypassModes))
            Me._ArmLayerBypassModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets a dictionary of trigger layer bypass mode parses. </summary>
    ''' <value> A Dictionary of trigger layer bypass mode parses. </value>
    Public ReadOnly Property ArmLayerBypassModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary>
    ''' Returns true if bypassing the source Arm using <see cref="TriggerLayerBypassModes.Acceptor"/>
    ''' .
    ''' </summary>
    ''' <value> True if Arm layer is bypassed. </value>
    Public ReadOnly Property IsArmLayerBypass As Boolean?
        Get
            Return If(Me.ArmLayerBypassMode.HasValue, Me.ArmLayerBypassMode.Value = TriggerLayerBypassModes.Acceptor, New Boolean?)
        End Get
    End Property

    ''' <summary> The arm layer bypass mode. </summary>
    Private _ArmLayerBypassMode As TriggerLayerBypassModes?

    ''' <summary> Gets or sets the cached arm layer bypass mode. </summary>
    ''' <value>
    ''' The <see cref="ArmLayerBypassMode">Arm Layer Bypass Mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property ArmLayerBypassMode As TriggerLayerBypassModes?
        Get
            Return Me._ArmLayerBypassMode
        End Get
        Protected Set(ByVal value As TriggerLayerBypassModes?)
            If Not Me.ArmLayerBypassMode.Equals(value) Then
                Me._ArmLayerBypassMode = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(ArmLayerSubsystemBase.IsArmLayerBypass))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Arm Layer Bypass Mode. </summary>
    ''' <param name="value"> The Arm Layer Bypass Mode. </param>
    ''' <returns>
    ''' The <see cref="ArmLayerBypassMode">source  Arm Layer Bypass Mode</see> or none if unknown.
    ''' </returns>
    Public Overloads Function ApplyArmLayerBypassMode(ByVal value As TriggerLayerBypassModes) As TriggerLayerBypassModes?
        Me.WriteArmLayerBypassMode(value)
        Return Me.QueryArmLayerBypassMode()
    End Function

    ''' <summary> Gets or sets the Arm Layer Bypass Mode query command. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:DIR?". </remarks>
    ''' <value> The Arm Layer Bypass Mode query command. </value>
    Protected Overridable Property ArmLayerBypassModeQueryCommand As String

    ''' <summary> Queries the Arm Layer Bypass Mode. </summary>
    ''' <returns>
    ''' The <see cref="ArmLayerBypassMode"> Arm Layer Bypass Mode</see> or none if unknown.
    ''' </returns>
    Public Function QueryArmLayerBypassMode() As TriggerLayerBypassModes?
        Me.ArmLayerBypassMode = Me.Query(Of TriggerLayerBypassModes)(Me.ArmLayerBypassModeQueryCommand, Me.ArmLayerBypassMode)
        Return Me.ArmLayerBypassMode
    End Function

    ''' <summary> Gets or sets the Arm Layer Bypass Mode command format. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:DIR {0}". </remarks>
    ''' <value> The Arm Layer Bypass Mode command format. </value>
    Protected Overridable Property ArmLayerBypassModeCommandFormat As String

    ''' <summary>
    ''' Writes the Arm Layer Bypass Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Arm Layer Bypass Mode. </param>
    ''' <returns>
    ''' The <see cref="ArmLayerBypassMode"> ARM Directio`n</see> or none if unknown.
    ''' </returns>
    Public Overloads Function WriteArmLayerBypassMode(ByVal value As TriggerLayerBypassModes) As TriggerLayerBypassModes?
        Me.ArmLayerBypassMode = Me.Write(Of TriggerLayerBypassModes)(Me.ArmLayerBypassModeCommandFormat, value)
        Return Me.ArmLayerBypassMode
    End Function

#Region " OVERRIDES "

    ''' <summary> Arm Layer Bypass Mode getter. </summary>
    ''' <returns> A Nullable(Of T) </returns>
    Public Function ArmLayerBypassModeGetter() As Integer?
        Return Me.ArmLayerBypassMode
    End Function

    ''' <summary> Arm Layer Bypass Mode setter. </summary>
    ''' <param name="value"> The current ArmCount. </param>
    Public Sub ArmLayerBypassModeSetter(ByVal value As Integer?)
        Me.ArmLayerBypassMode = CType(value, TriggerLayerBypassModes)
    End Sub

    ''' <summary> Arm Layer Bypass Mode reader. </summary>
    ''' <returns> An Integer? </returns>
    Public Function ArmLayerBypassModeReader() As Integer?
        Return Me.QueryArmLayerBypassMode()
    End Function

    ''' <summary> Arm Layer Bypass Mode writer. </summary>
    ''' <param name="value"> The Arm Layer Bypass Mode. </param>
    ''' <returns> An Integer? </returns>
    Public Function ArmLayerBypassModeWriter(ByVal value As Integer) As Integer?
        Return Me.WriteArmLayerBypassMode(CType(value, TriggerLayerBypassModes))
    End Function

#End Region

#End Region

#Region " ARM SOURCE "

    ''' <summary> Define arm source read writes. </summary>
    Private Sub DefineArmSourceReadWrites()
        Me._ArmSourceReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As ArmSources In [Enum].GetValues(GetType(ArmSources))
            Me._ArmSourceReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Arm source parses. </summary>
    ''' <value> A Dictionary of Arm source parses. </value>
    Public ReadOnly Property ArmSourceReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported arm sources. </summary>
    Private _SupportedArmSources As ArmSources

    ''' <summary> Gets or sets the supported Arm sources. </summary>
    ''' <value> The supported Arm sources. </value>
    Public Property SupportedArmSources() As ArmSources
        Get
            Return Me._SupportedArmSources
        End Get
        Set(ByVal value As ArmSources)
            If Not Me.SupportedArmSources.Equals(value) Then
                Me._SupportedArmSources = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The arm source. </summary>
    Private _ArmSource As ArmSources?

    ''' <summary> Gets or sets the cached source ArmSource. </summary>
    ''' <value>
    ''' The <see cref="ArmSource">source Arm Source</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property ArmSource As ArmSources?
        Get
            Return Me._ArmSource
        End Get
        Protected Set(ByVal value As ArmSources?)
            If Not Me.ArmSource.Equals(value) Then
                Me._ArmSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Arm Source. </summary>
    ''' <param name="value"> The  Source Arm Source. </param>
    ''' <returns> The <see cref="ArmSource">source Arm Source</see> or none if unknown. </returns>
    Public Function ApplyArmSource(ByVal value As ArmSources) As ArmSources?
        Me.WriteArmSource(value)
        Return Me.QueryArmSource()
    End Function

    ''' <summary> Gets or sets the Arm source query command. </summary>
    ''' <remarks> SCPI: ":ARM:SOUR?". </remarks>
    ''' <value> The Arm source query command. </value>
    Protected Overridable Property ArmSourceQueryCommand As String

    ''' <summary> Queries the Arm source. </summary>
    ''' <returns> The <see cref="ArmSource">Arm source</see> or none if unknown. </returns>
    Public Function QueryArmSource() As ArmSources?
        Me.ArmSource = Me.Query(Of ArmSources)(Me.ArmSourceQueryCommand,
                                                       Me.ArmSource.GetValueOrDefault(ArmSources.None),
                                                       Me.ArmSourceReadWrites)
        Return Me.ArmSource
    End Function

    ''' <summary> Gets or sets the Arm source command format. </summary>
    ''' <remarks> SCPI: ":ARM:SOUR {0}". </remarks>
    ''' <value> The write Arm source command format. </value>
    Protected Overridable Property ArmSourceCommandFormat As String

    ''' <summary> Writes the Arm Source without reading back the value from the device. </summary>
    ''' <param name="value"> The Arm Source. </param>
    ''' <returns> The <see cref="ArmSource">Arm Source</see> or none if unknown. </returns>
    Public Function WriteArmSource(ByVal value As ArmSources) As ArmSources?
        Me.ArmSource = Me.Write(Of ArmSources)(Me.ArmSourceCommandFormat, value, Me.ArmSourceReadWrites)
        Return Me.ArmSource
    End Function

#End Region

End Class

''' <summary> Enumerates the arm layer control sources. </summary>
<Flags>
Public Enum ArmSources

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the bus option. </summary>
    <ComponentModel.Description("Bus (BUS)")>
    Bus = 1

    ''' <summary> An enum constant representing the external option. </summary>
    <ComponentModel.Description("External (EXT)")>
    External = 2

    ''' <summary> An enum constant representing the hold option. </summary>
    <ComponentModel.Description("Hold operation (HOLD)")>
    Hold = 4

    ''' <summary> An enum constant representing the immediate option. </summary>
    <ComponentModel.Description("Immediate (IMM)")>
    Immediate = 8

    ''' <summary> An enum constant representing the manual option. </summary>
    <ComponentModel.Description("Manual (MAN)")>
    Manual = 16

    ''' <summary> An enum constant representing the timer option. </summary>
    <ComponentModel.Description("Timer (TIM)")>
    Timer = 32

    ''' <summary> Event detection for the arm layer is satisfied when either a positive-going or 
    ''' a negative-going pulse (via the SOT line of the Digital I/O) is received. </summary>
    <ComponentModel.Description("SOT Pulsed High or Low (BSTES)")>
    StartTestBoth = 64

    ''' <summary> Event detection for the arm layer is satisfied when a positive-going pulse 
    ''' (via the SOT line of the Digital I/O) is received.  </summary>
    <ComponentModel.Description("SOT Pulsed High (PSTES)")>
    StartTestHigh = 128

    ''' <summary> Event detection for the arm layer is satisfied when a negative-going pulse 
    ''' (via the SOT line of the Digital I/O) is received. </summary>
    <ComponentModel.Description("SOT Pulsed High (NSTES)")>
    StartTestLow = 256

    ''' <summary> Event detection occurs when an input trigger via the Trigger Link input line is
    ''' received. See “Trigger link,” 2400 manual page 11-19, For more information. With TLINk selected, you
    ''' can Loop around the Arm Event Detector by setting the Event detector bypass. </summary>
    <ComponentModel.Description("Trigger Link (TLIN)")>
    TriggerLink = 512

    ''' <summary> An enum constant representing all option. </summary>
    <ComponentModel.Description("All")>
    All = 1023

End Enum

