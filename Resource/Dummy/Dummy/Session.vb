''' <summary> A Dummy message based session. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-20 </para>
''' </remarks>
Public Class Session
    Inherits VI.Pith.SessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the MessageBasedSession object from the specified resource name.
    ''' </summary>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Disposes the VISA <see cref="T:isr.VI.Pith.SessionBase">Session</see> ending access to the
    ''' instrument.
    ''' </summary>
    Protected Overrides Sub DisposeSession()
    End Sub

    ''' <summary> Gets the sentinel indication that the VISA session is disposed. </summary>
    ''' <value> The is session disposed. </value>
    Public Overrides ReadOnly Property IsSessionDisposed As Boolean
        Get
            Return False
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating whether this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public Overrides ReadOnly Property IsDummy As Boolean = True

    ''' <summary>
    ''' Gets the session open sentinel. When open, the session is capable of addressing the hardware.
    ''' See also <see cref="P:VI.Pith.SessionBase.IsDeviceOpen" />.
    ''' </summary>
    ''' <value> The is session open. </value>
    Public Overrides ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Initializes a dummy session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="timeout">      The open timeout. </param>
    Protected Overrides Sub CreateSession(ByVal resourceName As String, ByVal timeout As TimeSpan)
        Me.ClearLastError()
    End Sub

    ''' <summary> Discards session. </summary>
    Protected Overrides Sub DiscardAllEvents()
    End Sub

    ''' <summary>
    ''' Checks if the candidate resource name exists. If so, assign to the
    ''' <see cref="ValidatedResourceName">validated resource name</see>
    ''' </summary>
    ''' <returns> <c>true</c> if it the resource exists; otherwise <c>false</c> </returns>
    Public Overrides Function ValidateCandidateResourceName() As Boolean
        Using rm As New ResourcesProvider
            Return MyBase.ValidateCandidateResourceName(rm)
        End Using
    End Function

    ''' <summary>
    ''' Gets or sets the sentinel indicating if call backs are performed in a specific
    ''' synchronization context.
    ''' </summary>
    ''' <remarks>
    ''' For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object marshals
    ''' callbacks across threads appropriately.<para>
    ''' DH: 3339 Setting true prevents display.
    ''' </para><para>
    ''' Note that setting to false also breaks display updates.
    ''' </para>
    ''' </remarks>
    ''' <value>
    ''' The sentinel indicating if call backs are performed in a specific synchronization context.
    ''' </value>
    Public Overrides Property SynchronizeCallBacks As Boolean

#End Region

#Region " ATTRIBUTES "

    ''' <summary> Gets read buffer size. </summary>
    ''' <returns> The read buffer size. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Function Get_ReadBufferSize() As Integer
        Return 1024
    End Function

    ''' <summary> The termination character. </summary>
    Private _TerminationCharacter As Byte = isr.Core.EscapeSequencesExtensions.Methods.NewLineValue

    ''' <summary> Gets or sets the ASCII character used to end reading. </summary>
    ''' <value> The termination character. </value>
    Public Overrides Property ReadTerminationCharacter As Byte
        Get
            Return Me._TerminationCharacter
        End Get
        Set(value As Byte)
            If Me.ReadTerminationCharacter <> value Then
                Me._TerminationCharacter = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True to enable, false to disable the termination character. </summary>
    Private _TerminationCharacterEnabled As Boolean = True

    ''' <summary>
    ''' Gets or sets the termination character enabled specifying whether the read operation ends
    ''' when a termination character is received.
    ''' </summary>
    ''' <value> The termination character enabled. </value>
    Public Overrides Property ReadTerminationCharacterEnabled As Boolean
        Get
            Return Me._TerminationCharacterEnabled
        End Get
        Set(value As Boolean)
            If Me.ReadTerminationCharacterEnabled <> value Then
                Me._TerminationCharacterEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The communication timeout. </summary>
    Private _CommunicationTimeout As TimeSpan = TimeSpan.FromSeconds(3)

    ''' <summary> Gets or sets the timeout for I/O communication on this resource session. </summary>
    ''' <value> The communication timeout. </value>
    Public Overrides Property CommunicationTimeout As TimeSpan
        Get
            Return Me._CommunicationTimeout
        End Get
        Set(value As TimeSpan)
            If Me.CommunicationTimeout <> value Then
                Me._CommunicationTimeout = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property


#End Region

#Region " READ/WRITE "

    ''' <summary> Synchronously reads ASCII-encoded string data. </summary>
    ''' <returns> The received message. </returns>
    Public Overrides Function ReadFreeLine() As String
        Me.ClearLastError()
        Me.LastMessageReceived = Me.EmulatedReply
        Return Me.LastMessageReceived
    End Function

    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="ReadTerminationCharacter">termination character</see>.
    ''' </summary>
    ''' <returns> The received message. </returns>
    Public Overrides Function ReadFiniteLine() As String
        Me.ClearLastError()
        Me.LastMessageReceived = Me.EmulatedReply
        Return Me.LastMessageReceived
    End Function

    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
    ''' data with the <see cref="ReadTerminationCharacter">termination character</see>.
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function SyncWriteLine(ByVal dataToWrite As String) As String
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.ClearLastError()
            Me.LastMessageSent = dataToWrite
        End If
        Return dataToWrite
    End Function

    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface.<para>
    ''' Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
    ''' formatted I/O write buffer</para>
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function SyncWrite(ByVal dataToWrite As String) As String
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.ClearLastError()
            Me.LastMessageSent = dataToWrite
        End If
        Return dataToWrite
    End Function

    ''' <summary> Gets the size of the input buffer. </summary>
    ''' <value> The size of the input buffer. </value>
    Public Overrides ReadOnly Property InputBufferSize As Integer
        Get
            Return 4096
        End Get
    End Property

#End Region

#Region " REGISTERS "

    ''' <summary> Thread unsafe read status byte. </summary>
    ''' <returns> The VI.Pith.ServiceRequests. </returns>
    Protected Overrides Function ThreadUnsafeReadStatusByte() As VI.Pith.ServiceRequests
        Me.ClearLastError()
        Me.StatusByte = Me.EmulatedStatusByte
        Return Me.StatusByte
    End Function

    ''' <summary> Clears the device. </summary>
    Protected Overrides Sub Clear()
        Me.ClearLastError()
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> True to enable, false to disable the service request event handler. </summary>
    Private _ServiceRequestEventHandlerEnabled As Boolean

    ''' <summary>
    ''' Gets or sets or set (protected) the sentinel indication if a service request event handler
    ''' was enabled and registered.
    ''' </summary>
    ''' <value>
    ''' <c>True</c> if service request event is enabled and registered; otherwise, <c>False</c>.
    ''' </value>
    Public Overrides Property ServiceRequestEventEnabled As Boolean
        Get
            Return Me._ServiceRequestEventHandlerEnabled
        End Get
        Set(value As Boolean)
            If Me.ServiceRequestEventEnabled <> value Then
                Me._ServiceRequestEventHandlerEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Enables the service request. </summary>
    Public Overrides Sub EnableServiceRequestEventHandler()
        If Not Me.ServiceRequestEventEnabled Then
            Me.ClearLastError()
            Me.ServiceRequestEventEnabled = True
        End If
    End Sub

    ''' <summary> Disables the service request. </summary>
    Public Overrides Sub DisableServiceRequestEventHandler()
        If Me.ServiceRequestEventEnabled Then
            Me.ClearLastError()
            Me.ServiceRequestEventEnabled = False
        End If
    End Sub

    ''' <summary> Discard pending service requests. </summary>
    Public Overrides Sub DiscardServiceRequests()
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary>
    ''' Asserts a software or hardware trigger depending on the interface; Sends a bus trigger.
    ''' </summary>
    Public Overrides Sub AssertTrigger()
        Me.ClearLastError()
    End Sub

#End Region

#Region " INTERFACE "

    ''' <summary> Clears the interface. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Protected Overrides Sub ImplementClearHardwareInterface()
        If Me.SupportsClearInterface AndAlso Me.IsSessionOpen Then
            Using gi As GpibInterfaceSession = New GpibInterfaceSession()
                gi.OpenSession(Me.ResourceNameInfo.InterfaceResourceName)
                If gi.IsOpen Then
                    gi.SelectiveDeviceClear(Me.OpenResourceName)
                Else
                    Throw New isr.Core.OperationFailedException($"Failed opening GPIB Interface Session {Me.ResourceNameInfo.InterfaceResourceName}")
                End If
            End Using
        End If
    End Sub

    ''' <summary> Clears the device (SDC). </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Protected Overrides Sub ClearDevice()
        Me.Clear()
        If Me.SupportsClearInterface AndAlso Me.IsSessionOpen Then
            Using gi As GpibInterfaceSession = New GpibInterfaceSession()
                gi.OpenSession(Me.ResourceNameInfo.InterfaceResourceName)
                If gi.IsOpen Then
                    gi.SelectiveDeviceClear(Me.OpenResourceName)
                Else
                    Throw New isr.Core.OperationFailedException($"Failed opening GPIB Interface Session {Me.ResourceNameInfo.InterfaceResourceName}")
                End If
            End Using
        End If
    End Sub

#End Region

End Class
