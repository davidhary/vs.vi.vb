﻿''' <summary> Status subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits isr.VI.Tsp.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> The session. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(session)
        StatusSubsystem.InitializeSession(session)
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region

#Region " SESSION "

    ''' <summary> Initializes the session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="Session">message based TSP session</see>. </param>
    Private Shared Sub InitializeSession(ByVal session As VI.Pith.SessionBase)
        session.ClearExecutionStateCommand = TspSyntax.ClearExecutionStateCommand
        session.ResetKnownStateCommand = TspSyntax.ResetKnownStateCommand
        session.ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        session.StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
        session.OperationCompletedQueryCommand = TspSyntax.OperationCompletedQueryCommand
        session.StandardServiceEnableCommandFormat = TspSyntax.StandardServiceEnableCommandFormat
        session.StandardServiceEnableCompleteCommandFormat = TspSyntax.StandardServiceEnableCompleteCommandFormat
        session.ServiceRequestEnableCommandFormat = TspSyntax.ServiceRequestEnableCommandFormat
        session.ServiceRequestEnableQueryCommand = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand
        session.StandardEventStatusQueryCommand = TspSyntax.StandardEventStatusQueryCommand
        session.StandardEventEnableQueryCommand = TspSyntax.StandardEventEnableQueryCommand
        session.WaitCommand = VI.Pith.Ieee488.Syntax.WaitCommand
    End Sub

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = TspSyntax.ClearErrorQueueCommand

    ''' <summary> Gets or sets the error queue query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = TspSyntax.ErrorQueueQueryCommand

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the 'Next Error' query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property DequeueErrorQueryCommand As String = String.Empty

#End Region

#Region " MEASUREMENT REGISTER EVENTS "

    ''' <summary> Gets or sets the measurement status query command. </summary>
    ''' <value> The measurement status query command. </value>
    Protected Overrides Property MeasurementStatusQueryCommand As String = TspSyntax.MeasurementEventQueryCommand

    ''' <summary> Gets or sets the measurement event condition query command. </summary>
    ''' <value> The measurement event condition query command. </value>
    Protected Overrides Property MeasurementEventConditionQueryCommand As String = TspSyntax.MeasurementEventConditionQueryCommand

#End Region

#Region " OPERATION REGISTER EVENTS "

    ''' <summary> Gets or sets the operation event enable Query command. </summary>
    ''' <value> The operation event enable Query command. </value>
    Protected Overrides Property OperationEventEnableQueryCommand As String = TspSyntax.OperationEventEnableQueryCommand

    ''' <summary> Gets or sets the operation event enable command format. </summary>
    ''' <value> The operation event enable command format. </value>
    Protected Overrides Property OperationEventEnableCommandFormat As String = TspSyntax.OperationEventEnableCommandFormat

    ''' <summary> Gets or sets the operation event status query command. </summary>
    ''' <value> The operation event status query command. </value>
    Protected Overrides Property OperationEventStatusQueryCommand As String = TspSyntax.OperationEventQueryCommand

#End Region

#Region " QUESTIONABLE REGISTER "

    ''' <summary> Gets or sets the questionable status query command. </summary>
    ''' <value> The questionable status query command. </value>
    Protected Overrides Property QuestionableStatusQueryCommand As String = TspSyntax.QuestionableEventQueryCommand

#End Region

#Region " IDENTITIY "

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = VI.Pith.Ieee488.Syntax.IdentityQueryCommand & " " & VI.Pith.Ieee488.Syntax.WaitCommand

#End Region

End Class
