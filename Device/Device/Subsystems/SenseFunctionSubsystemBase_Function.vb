'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SenseFunctionSubsystemBase_Function.vb
'
' summary:	Sense function subsystem base function class
'---------------------------------------------------------------------------------------------------

Partial Public MustInherit Class SenseFunctionSubsystemBase

#Region " RANGE "

    ''' <summary> Define function mode ranges. </summary>
    Private Sub DefineFunctionModeRanges()
        Me._FunctionModeRanges = New RangeDictionary
        SenseSubsystemBase.DefineFunctionModeRanges(Me.FunctionModeRanges, Me.DefaultFunctionRange)
    End Sub

    ''' <summary> Gets or sets the function mode ranges. </summary>
    ''' <value> The function mode ranges. </value>
    Public ReadOnly Property FunctionModeRanges As RangeDictionary

    ''' <summary> Gets or sets the default function range. </summary>
    ''' <value> The default function range. </value>
    Public Property DefaultFunctionRange As isr.Core.Constructs.RangeR

    ''' <summary> Converts a functionMode to a range. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> FunctionMode as an isr.Core.Constructs.RangeR. </returns>
    Public Overridable Function ToRange(ByVal functionMode As Integer) As isr.Core.Constructs.RangeR
        Return Me.FunctionModeRanges(functionMode)
    End Function

    ''' <summary> The function range. </summary>
    Private _FunctionRange As Core.Constructs.RangeR

    ''' <summary> The Range of the range. </summary>
    ''' <value> The function range. </value>
    Public Property FunctionRange As Core.Constructs.RangeR
        Get
            Return Me._FunctionRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.FunctionRange <> value Then
                Me._FunctionRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " DECIMAL PLACES "

    ''' <summary> Gets or sets the default decimal places. </summary>
    ''' <value> The default decimal places. </value>
    Public Property DefaultFunctionModeDecimalPlaces As Integer

    ''' <summary> Define function mode decimal places. </summary>
    Private Sub DefineFunctionModeDecimalPlaces()
        Me._FunctionModeDecimalPlaces = New IntegerDictionary
        SenseSubsystemBase.DefineFunctionModeDecimalPlaces(Me.FunctionModeDecimalPlaces, Me.DefaultFunctionModeDecimalPlaces)
    End Sub

    ''' <summary> Gets or sets the function mode decimal places. </summary>
    ''' <value> The function mode decimal places. </value>
    Public ReadOnly Property FunctionModeDecimalPlaces As IntegerDictionary

    ''' <summary> Converts a function Mode to a decimal places. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> FunctionMode as an Integer. </returns>
    Public Overridable Function ToDecimalPlaces(ByVal functionMode As Integer) As Integer
        Return Me.FunctionModeDecimalPlaces(functionMode)
    End Function

    ''' <summary> The function range decimal places. </summary>
    Private _FunctionRangeDecimalPlaces As Integer

    ''' <summary> Gets or sets the function range decimal places. </summary>
    ''' <value> The function range decimal places. </value>
    Public Property FunctionRangeDecimalPlaces As Integer
        Get
            Return Me._FunctionRangeDecimalPlaces
        End Get
        Set(value As Integer)
            If Me.FunctionRangeDecimalPlaces <> value Then
                Me._FunctionRangeDecimalPlaces = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " UNIT "

    ''' <summary> Gets or sets the default unit. </summary>
    ''' <value> The default unit. </value>
    Public Property DefaultFunctionUnit As Arebis.TypedUnits.Unit

    ''' <summary> Define function mode units. </summary>
    Private Sub DefineFunctionModeUnits()
        Me._FunctionModeUnits = New UnitDictionary
        SenseSubsystemBase.DefineFunctionModeUnits(Me._FunctionModeUnits)
    End Sub

    ''' <summary> Gets or sets the function mode decimal places. </summary>
    ''' <value> The function mode decimal places. </value>
    Public ReadOnly Property FunctionModeUnits As UnitDictionary

    ''' <summary> Parse units. </summary>
    ''' <param name="functionMode"> The  Multimeter Function Mode. </param>
    ''' <returns> An Arebis.TypedUnits.Unit. </returns>
    Public Overridable Function ToUnit(ByVal functionMode As Integer) As Arebis.TypedUnits.Unit
        Return Me.FunctionModeUnits(functionMode)
    End Function

    ''' <summary> Gets or sets the function unit. </summary>
    ''' <value> The function unit. </value>
    Public Property FunctionUnit As Arebis.TypedUnits.Unit
        Get
            Return Me.PrimaryReading.Amount.Unit
        End Get
        Set(value As Arebis.TypedUnits.Unit)
            If Me.FunctionUnit <> value Then
                Me.PrimaryReading.ApplyUnit(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Define function clear known state. </summary>
    Protected Overridable Sub DefineFunctionClearKnownState()
        If Me.ReadingAmounts.HasReadingElements Then
            Me.ReadingAmounts.ActiveReadingAmount.ApplyUnit(Me.FunctionUnit)
        End If
        Me.ReadingAmounts.Reset()
        Me.ParsePrimaryReading(String.Empty)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Me.FunctionUnit)
        Me.ReadingAmounts.TryParse(String.Empty)
        Me.NotifyPropertyChanged(NameOf(MeasureSubsystemBase.ReadingAmounts))
    End Sub

    ''' <summary> Define function mode read writes. </summary>
    Private Sub DefineFunctionModeReadWrites()
        Me._FunctionModeReadWrites = New Pith.EnumReadWriteCollection
        SenseSubsystemBase.DefineFunctionModeReadWrites(Me.FunctionModeReadWrites)
    End Sub

    ''' <summary> Define function mode read writes. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="readValueDecorator">  The read value decorator. </param>
    ''' <param name="writeValueDecorator"> The write value decorator. </param>
    Public Sub DefineFunctionModeReadWrites(ByVal readValueDecorator As String, ByVal writeValueDecorator As String)
        Me._FunctionModeReadWrites = New Pith.EnumReadWriteCollection With {.ReadValueDecorator = readValueDecorator,
            .WriteValueDecorator = writeValueDecorator}
        SenseSubsystemBase.DefineFunctionModeReadWrites(Me.FunctionModeReadWrites)
    End Sub

    ''' <summary> Gets or sets a dictionary of Sense function mode parses. </summary>
    ''' <value> A Dictionary of Sense function mode parses. </value>
    Public ReadOnly Property FunctionModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported function modes. </summary>
    Private _SupportedFunctionModes As SenseFunctionModes

    ''' <summary>
    ''' Gets or sets the supported Function Modes. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported Sense function modes. </value>
    Public Property SupportedFunctionModes() As SenseFunctionModes
        Get
            Return Me._SupportedFunctionModes
        End Get
        Set(ByVal value As SenseFunctionModes)
            If Not Me.SupportedFunctionModes.Equals(value) Then
                Me._SupportedFunctionModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The function mode. </summary>
    Private _FunctionMode As SenseFunctionModes?

    ''' <summary> Gets or sets the cached Sense function mode. </summary>
    ''' <value>
    ''' The <see cref="FunctionMode">Sense function mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property FunctionMode As SenseFunctionModes?
        Get
            Return Me._FunctionMode
        End Get
        Protected Set(ByVal value As SenseFunctionModes?)
            If Not Me.FunctionMode.Equals(value) Then
                Me._FunctionMode = value
                If value.HasValue Then
                    Me.FunctionRange = Me.ToRange(value.Value)
                    Me.FunctionUnit = Me.ToUnit(value.Value)
                    Me.FunctionRangeDecimalPlaces = Me.ToDecimalPlaces(value.Value)
                Else
                    Me.FunctionRange = Me.DefaultFunctionRange
                    Me.FunctionUnit = Me.DefaultFunctionUnit
                    Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
                End If
                Me.DefineFunctionClearKnownState()
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sense function mode. </summary>
    ''' <param name="value"> The  Sense function mode. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">source Sense function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function ApplyFunctionMode(ByVal value As SenseFunctionModes) As SenseFunctionModes?
        Me.WriteFunctionMode(value)
        Return Me.QueryFunctionMode()
    End Function

    ''' <summary> Gets or sets the Sense function mode query command. </summary>
    ''' <value> The Sense function mode query command. </value>
    Protected MustOverride ReadOnly Property FunctionModeQueryCommand As String

    ''' <summary> Queries the Sense function mode. </summary>
    ''' <returns> The <see cref="FunctionMode">Sense function mode</see> or none if unknown. </returns>
    Public Overridable Function QueryFunctionMode() As SenseFunctionModes?
        Return Me.QueryFunctionMode(Me.FunctionModeQueryCommand)
    End Function

    ''' <summary> Queries print function mode. </summary>
    ''' <returns> The print function mode. </returns>
    Public Overridable Function QueryPrintFunctionMode() As SenseFunctionModes?
        Return Me.QueryFunctionMode(Me.Session.BuildQueryPrintCommand(Me.FunctionModeQueryCommand))
    End Function

    ''' <summary> Queries the Sense function mode. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns> The <see cref="FunctionMode">Sense function mode</see> or none if unknown. </returns>
    Public Overridable Function QueryFunctionMode(ByVal queryCommand As String) As SenseFunctionModes?
        Me.FunctionMode = Me.Query(Of SenseFunctionModes)(queryCommand,
                                                          Me.FunctionMode.GetValueOrDefault(SenseFunctionModes.None),
                                                          Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

    ''' <summary> Gets or sets the Sense function mode command format. </summary>
    ''' <value> The Sense function mode command format. </value>
    Protected MustOverride ReadOnly Property FunctionModeCommandFormat As String

    ''' <summary>
    ''' Writes the Sense function mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Sense function mode. </param>
    ''' <returns> The <see cref="FunctionMode">Sense function mode</see> or none if unknown. </returns>
    Public Overridable Function WriteFunctionMode(ByVal value As SenseFunctionModes) As SenseFunctionModes?
        Me.FunctionMode = Me.Write(Of SenseFunctionModes)(Me.FunctionModeCommandFormat, value, Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

#End Region

End Class

