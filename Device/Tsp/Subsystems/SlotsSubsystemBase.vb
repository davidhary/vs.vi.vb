''' <summary> A slots subsystem base. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-02-15 </para>
''' </remarks>
Public MustInherit Class SlotsSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <param name="maxSlotCount">    Number of maximum slots. </param>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal maxSlotCount As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._MaximumSlotCount = maxSlotCount
        Me._Slots = New SlotCollection
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        For Each s As SlotSubsystemBase In Me.Slots
            s.QuerySlotExists()
            If s.IsSlotExists Then
                s.QuerySupportsInterlock()
                s.QueryInterlocksState()
            End If
        Next
    End Sub

#End Region

#Region " EXISTS "

    ''' <summary> The slots. </summary>
    ''' <value> The slots. </value>
    Public ReadOnly Property Slots As SlotCollection

    ''' <summary> Gets or sets the number of maximum slots. </summary>
    ''' <value> The number of maximum slots. </value>
    Public ReadOnly Property MaximumSlotCount As Integer

#End Region

End Class

''' <summary> Collection of slots. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-02-15 </para>
''' </remarks>
Public Class SlotCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of Integer, SlotSubsystemBase)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As SlotSubsystemBase) As Integer
        If item IsNot Nothing Then
            Return item.SlotNumber
        End If
    End Function

End Class