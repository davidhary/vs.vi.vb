''' <summary> Defines the SCPI Compliance Limit subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para> 
''' David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
''' </remarks>
Public MustInherit Class ComplianceLimitBase
    Inherits NumericLimitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ComplianceLimitBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(1, statusSubsystem)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="limitNumber">     The limit number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal limitNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(limitNumber, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Enabled = False
        Me.IncomplianceCondition = True
        Me.FailureBits = 15
    End Sub

#End Region

#Region " FAILURE BITS "

    ''' <summary> The failure bits. </summary>
    Private _FailureBits As Integer?

    ''' <summary> Gets or sets the cached Failure Bits. </summary>
    ''' <value> The Failure Bits or none if not set or unknown. </value>
    Public Overloads Property FailureBits As Integer?
        Get
            Return Me._FailureBits
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.FailureBits, value) Then
                Me._FailureBits = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Failure Bits. </summary>
    ''' <param name="value"> The current Failure Bits. </param>
    ''' <returns> The Failure Bits or none if unknown. </returns>
    Public Function ApplyFailureBits(ByVal value As Integer) As Integer?
        Me.WriteFailureBits(value)
        Return Me.QueryFailureBits()
    End Function

    ''' <summary> Gets or sets Failure Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM:COMP:SOUR2?". </remarks>
    ''' <value> The Failure Bits query command. </value>
    Protected Overridable Property FailureBitsQueryCommand As String

    ''' <summary> Queries the current Failure Bits. </summary>
    ''' <returns> The Failure Bits or none if unknown. </returns>
    Public Function QueryFailureBits() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.FailureBitsQueryCommand) Then
            Me.FailureBits = Me.Session.Query(0I, Me.BuildCommand(Me.FailureBitsQueryCommand))
        End If
        Return Me.FailureBits
    End Function

    ''' <summary> Gets or sets Failure Bits command format. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM:COMP:SOUR2 {0}". </remarks>
    ''' <value> The Failure Bits command format. </value>
    Protected Overridable Property FailureBitsCommandFormat As String

    ''' <summary> Write the Failure Bits without reading back the value from the device. </summary>
    ''' <param name="value"> The current Failure Bits. </param>
    ''' <returns> The Failure Bits or none if unknown. </returns>
    Public Function WriteFailureBits(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.FailureBitsCommandFormat) Then
            Me.Session.WriteLine(Me.BuildCommand(Me.FailureBitsCommandFormat), value)
        End If
        Me.FailureBits = value
        Return Me.FailureBits
    End Function

#End Region

#Region " IN COMPLIANCE FAILURE CONDITION "

    ''' <summary> The incompliance condition. </summary>
    Private _IncomplianceCondition As Boolean?

    ''' <summary> Gets or sets the cached In Compliance Condition sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if In Compliance Condition is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property IncomplianceCondition As Boolean?
        Get
            Return Me._IncomplianceCondition
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.IncomplianceCondition, value) Then
                Me._IncomplianceCondition = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the In Compliance Condition sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Condition; otherwise <c>False</c>. </returns>
    Public Function ApplyIncomplianceCondition(ByVal value As Boolean) As Boolean?
        Me.WriteIncomplianceCondition(value)
        Return Me.QueryIncomplianceCondition()
    End Function

    ''' <summary> Gets or sets the In compliance Condition query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM:COMP:FAIL?". </remarks>
    ''' <value> The In-compliance Condition query command. </value>
    Protected Overridable Property IncomplianceConditionQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Enabled sentinel. Also sets the
    ''' <see cref="InComplianceCondition">Condition</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if in compliance; otherwise <c>False</c>. </returns>
    Public Function QueryIncomplianceCondition() As Boolean?
        Me.IncomplianceCondition = Me.Query(Me.IncomplianceCondition, Me.BuildCommand(Me.IncomplianceConditionQueryCommand))
        Return Me.IncomplianceCondition
    End Function

    ''' <summary>
    ''' Gets or sets the In-compliance Condition command Format.
    ''' <see cref="InComplianceCondition">Condition</see> sentinel.
    ''' </summary>
    ''' <remarks> SCPI: ":CALC2:LIM:COMP:FAIL {0:'IN';'IN';'OUT'}". </remarks>
    ''' <value> The incompliance condition command format. </value>
    Protected Overridable Property IncomplianceConditionCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if in compliance; otherwise <c>False</c>. </returns>
    Public Function WriteIncomplianceCondition(ByVal value As Boolean) As Boolean?
        Me.IncomplianceCondition = Me.Write(value, Me.BuildCommand(Me.IncomplianceConditionCommandFormat))
        Return Me.IncomplianceCondition
    End Function

#End Region

End Class
