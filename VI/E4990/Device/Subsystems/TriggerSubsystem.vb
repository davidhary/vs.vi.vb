''' <summary> Defines a Trigger Subsystem for a Keithley 7500 Meter. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TriggerSubsystem
    Inherits VI.TriggerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.AveragingEnabled = False
        Me.TriggerSource = VI.TriggerSources.Internal
        Me.SupportedTriggerSources = VI.TriggerSources.Bus Or VI.TriggerSources.External Or VI.TriggerSources.Immediate Or VI.TriggerSources.Internal
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " ABORT / INIT COMMANDS "

    ''' <summary> Gets or sets the Abort command. </summary>
    ''' <value> The Abort command. </value>
    Protected Overrides Property AbortCommand As String = ":ABOR"

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <value> The initiate command. </value>
    Protected Overrides Property InitiateCommand As String = ":TRIG"

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <remarks>
    ''' This command generates a trigger immediately and executes a measurement, regardless of the
    ''' setting of the trigger mode. This command Is different from :TRIG as the execution of the
    ''' object finishes when the measurement (all of the sweep) initiated with this object Is
    ''' complete. In other words, you can wait for the end of the measurement using the *OPC object.
    ''' If you execute this Object When the trigger system Is Not In the trigger wait state (trigger
    ''' Event detection state), an Error occurs When executed And the Object Is ignored.
    ''' </remarks>
    ''' <value> The Immediate command. </value>
    Protected Overrides Property ImmediateCommand As String = ":TRIG:SING"

#End Region

#Region " AVERAGING ENABLED "

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AveragingEnabledQueryCommand As String = ":TRIG:AVER?"

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AveragingEnabledCommandFormat As String = ":TRIG:AVER {0:1;1;0}"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = ":TRIG:EXT:DEL {0:s\.FFFFFFF}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = ":TRIG:EXT:DEL?"

#End Region

#Region " SOURCE "

    ''' <summary> Gets or sets the Trigger source query command. </summary>
    ''' <value> The Trigger source query command. </value>
    Protected Overrides Property TriggerSourceQueryCommand As String = ":TRIG:SOUR?"

    ''' <summary> Gets or sets the Trigger source command format. </summary>
    ''' <remarks> SCPI: "{0}". </remarks>
    ''' <value> The write Trigger source command format. </value>
    Protected Overrides Property TriggerSourceCommandFormat As String = ":TRIG:SOUR {0}"

#End Region

#End Region

End Class
