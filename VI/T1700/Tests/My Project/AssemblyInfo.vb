﻿Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Tegam 1700 VI Tests")>
<Assembly: AssemblyDescription("Tegam 1700 Virtual Instrument Unit Tests Library")>
<Assembly: AssemblyProduct("isr.VI.T1700.Tests")>
<Assembly: CLSCompliant(True)>
<Assembly: ComVisible(False)>

