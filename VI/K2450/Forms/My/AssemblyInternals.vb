'---------------------------------------------------------------------------------------------------
' file:		Forms\My\MyLibrary_Internals.vb
'
' summary:	My library internals class
'---------------------------------------------------------------------------------------------------
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.Tsp2.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.Tsp2.Forms.K2450FormsTests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.Cinco.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.Morphe.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Verrzzano.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.StrainPublicKey)>
