'---------------------------------------------------------------------------------------------------
' file:		.\Views\SessionStatusView.vb
'
' summary:	Session status view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> ASession Status view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SessionStatusView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SessionStatusView"/> </summary>
    ''' <returns> A <see cref="SessionStatusView"/>. </returns>
    Public Shared Function Create() As SessionStatusView
        Dim view As SessionStatusView = Nothing
        Try
            view = New SessionStatusView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                ' TO_DO: Doe we need this? Me.BindVisaSessionBase(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " visa session base (device base) "

    ''' <summary> Gets the visa session base. </summary>
    ''' <value> The visa session base. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase

    ''' <summary> Binds the visa session to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session view model. </param>
    Public Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            Me._SelectorOpener.AssignSelectorViewModel(Nothing)
            Me._SelectorOpener.AssignOpenerViewModel(Nothing)
            Me._VisaSessionBase = Nothing
        End If
        If visaSessionBase IsNot Nothing Then
            Me._VisaSessionBase = visaSessionBase
            Me._SelectorOpener.AssignSelectorViewModel(visaSessionBase.SessionFactory)
            Me._SelectorOpener.AssignOpenerViewModel(visaSessionBase)
        End If
        Me._StatusView.BindVisaSessionBase(visaSessionBase)
        Me._SessionView.BindVisaSessionBase(visaSessionBase)
    End Sub

#End Region

#Region " EXPOSED VIEWS "

    ''' <summary> Gets the status view. </summary>
    ''' <value> The status view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property StatusView As StatusView
        Get
            Return Me._StatusView
        End Get
    End Property

    ''' <summary> Gets the session view. </summary>
    ''' <value> The session view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SessionView As SessionView
        Get
            Return Me._SessionView
        End Get
    End Property


#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
