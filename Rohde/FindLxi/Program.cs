using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RohdeSchwarz.Visa;

namespace RsVISA.NET_FindLxi
{
    class Program
    {
        /// <summary>
        /// This example uses Rohde & Schwartz-specific method of ResourceManager called FindTcpIpResources() to search for all the devices on the TCP-IP Network.
        /// Compared to the standard Ivi.Visa method Find(), the FindTcpIpResources() allows for using VXI-11 broadcast and mDNS Bonjour mechanism.
        /// The entire process is perform without opening a session to any instrument (viOpen).
        /// Therefore, the FindTcpIpResources() does not interfere with any other instrument's operations
        /// </summary>
        static void Main()
        {
            var rm = new ResourceManager();
            var instruments = rm.FindTcpIpResources("?*", true, true);

            // Displaying all the instruments
            Console.WriteLine("All found instrument:");
            foreach (var instrument in instruments)
            {
                Console.WriteLine(instrument);
            }

            // Displaying only the LXI instruments
            var lxiInstruments = instruments.ToList().FindAll(s => s.LxiInfo.IsValid());
            Console.WriteLine("\n\nFound LXI instruments:");
            if (lxiInstruments.Count > 0)
            {
                foreach (var instrument in lxiInstruments)
                {
                    Console.WriteLine(instrument);
                }

                // Displaying grouped LXI instruments
                var lxiInstrumentGroups = from p in lxiInstruments
                    group p by p.LxiInfo.HostName
                    into g
                    select new
                    {
                        g.ElementAt(0).LxiInfo,
                        ResourceNames = g.Select(c => c.OriginalResourceName)
                    };

                Console.WriteLine("\n\nLXI instruments grouped:");
                foreach (var lxiGroup in lxiInstrumentGroups)
                {
                    Console.WriteLine("{0}:", lxiGroup.LxiInfo);
                    Console.WriteLine("    {0}\n", string.Join("\n    ", lxiGroup.ResourceNames));
                }
            }
            else
            {
                Console.WriteLine("None");
            }

            Console.WriteLine("\n\nPress any key to finish.");
            Console.ReadKey();
        }
    }
}
