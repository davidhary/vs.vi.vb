'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SubsystemCollection.vb
'
' summary:	Subsystem collection class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.VI.ExceptionExtensions

''' <summary> Collection of subsystems. </summary>
''' <remarks>
''' The synchronization context is captured as part of the property change and other event
''' handlers and is no longer needed here. <para>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved., Inc. All rights
''' reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-12-21 </para>
''' </remarks>
Public Class SubsystemCollection
    Inherits Collections.ObjectModel.Collection(Of SubsystemBase)
    Implements ITalker

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._Talker = New TraceMessageTalker
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the clear execution state (CLS) by setting system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    ''' <remarks> Clears the queues and sets all registers to zero. </remarks>
    Public Sub DefineClearExecutionState()
        For Each element As SubsystemBase In Me.Items
            element.DefineClearExecutionState()
        Next
    End Sub

    ''' <summary>
    ''' Performs a reset and additional custom setting for the subsystem:<para>
    ''' </para>
    ''' </summary>
    Public Sub InitKnownState()
        For Each element As SubsystemBase In Me.Items
            element.InitKnownState()
        Next
    End Sub

    ''' <summary>
    ''' Gets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Sub PresetKnownState()
        For Each element As SubsystemBase In Me.Items
            element.PresetKnownState()
        Next
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> Clears the queues and sets all registers to zero. </remarks>
    Public Sub DefineKnownResetState()
        For Each element As SubsystemBase In Me.Items
            element.DefineKnownResetState()
        Next
    End Sub

#End Region

#Region " ADD "

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
    Public Overloads Sub Add(ByVal item As SubsystemBase)
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        MyBase.Add(item)
        If Me.Talker IsNot Nothing Then item.AssignTalker(Me.Talker)
    End Sub

#End Region

#Region " CLEAR/DISPOSE "

    ''' <summary> Disposes items and clear. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shadows Sub Clear()
        For Each element As SubsystemBase In Me.Items
            Try
                TryCast(element, IDisposable)?.Dispose()
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
        MyBase.Clear()
    End Sub

#End Region

End Class

