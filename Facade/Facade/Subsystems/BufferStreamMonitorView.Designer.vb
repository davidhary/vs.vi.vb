<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BufferStreamMonitorView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BufferStreamMonitorView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DisplayMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearBufferDisplayMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._DisplayBufferMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TriggerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InitiateTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortStartTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MonitorActiveTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InitMonitorReadRepeatMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RepeatMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StreamMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ToggleBufferStreamMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortButton = New System.Windows.Forms.ToolStripButton()
        Me._BufferCountLabel = New isr.Core.Controls.ToolStripLabel()
        Me._LastPointNumberLabel = New isr.Core.Controls.ToolStripLabel()
        Me._FirstPointNumberLabel = New isr.Core.Controls.ToolStripLabel()
        Me._BufferSizeNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BufferSizeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._TriggerStateLabel = New isr.Core.Controls.ToolStripLabel()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._AbortButton, Me._BufferCountLabel, Me._LastPointNumberLabel, Me._FirstPointNumberLabel, Me._BufferSizeNumericLabel, Me._BufferSizeNumeric, Me._TriggerStateLabel})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(356, 28)
        Me._SubsystemToolStrip.TabIndex = 11
        Me._SubsystemToolStrip.Text = "Buffer"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DisplayMenuItem, Me._TriggerMenuItem, Me._StreamMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(55, 25)
        Me._SubsystemSplitButton.Text = "Buffer"
        '
        '_DisplayMenuItem
        '
        Me._DisplayMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ClearBufferDisplayMenuItem, Me._DisplayBufferMenuItem})
        Me._DisplayMenuItem.Name = "_DisplayMenuItem"
        Me._DisplayMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._DisplayMenuItem.Text = "Display"
        '
        '_ClearBufferDisplayMenuItem
        '
        Me._ClearBufferDisplayMenuItem.Name = "_ClearBufferDisplayMenuItem"
        Me._ClearBufferDisplayMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ClearBufferDisplayMenuItem.Text = "Clear Buffer Display"
        Me._ClearBufferDisplayMenuItem.ToolTipText = "Clears the display"
        '
        '_DisplayBufferMenuItem
        '
        Me._DisplayBufferMenuItem.Name = "_DisplayBufferMenuItem"
        Me._DisplayBufferMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._DisplayBufferMenuItem.Text = "Display Buffer"
        Me._DisplayBufferMenuItem.ToolTipText = "Displays the buffer"
        '
        '_TriggerMenuItem
        '
        Me._TriggerMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InitiateTriggerPlanMenuItem, Me._AbortStartTriggerPlanMenuItem, Me._MonitorActiveTriggerPlanMenuItem, Me._InitMonitorReadRepeatMenuItem, Me._RepeatMenuItem})
        Me._TriggerMenuItem.Name = "_TriggerMenuItem"
        Me._TriggerMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._TriggerMenuItem.Text = "Trigger Monitor"
        '
        '_InitiateTriggerPlanMenuItem
        '
        Me._InitiateTriggerPlanMenuItem.Name = "_InitiateTriggerPlanMenuItem"
        Me._InitiateTriggerPlanMenuItem.Size = New System.Drawing.Size(219, 22)
        Me._InitiateTriggerPlanMenuItem.Text = "Initiate"
        Me._InitiateTriggerPlanMenuItem.ToolTipText = "Sends the Initiate command to the instrument"
        '
        '_AbortStartTriggerPlanMenuItem
        '
        Me._AbortStartTriggerPlanMenuItem.Name = "_AbortStartTriggerPlanMenuItem"
        Me._AbortStartTriggerPlanMenuItem.Size = New System.Drawing.Size(219, 22)
        Me._AbortStartTriggerPlanMenuItem.Text = "Abort, Clear, Initiate"
        Me._AbortStartTriggerPlanMenuItem.ToolTipText = "Aborts, clears buffer and starts the trigger plan"
        '
        '_MonitorActiveTriggerPlanMenuItem
        '
        Me._MonitorActiveTriggerPlanMenuItem.Name = "_MonitorActiveTriggerPlanMenuItem"
        Me._MonitorActiveTriggerPlanMenuItem.Size = New System.Drawing.Size(219, 22)
        Me._MonitorActiveTriggerPlanMenuItem.Text = "Monitor Active Trigger State"
        Me._MonitorActiveTriggerPlanMenuItem.ToolTipText = "Monitors active trigger state. Exits if trigger plan inactive"
        '
        '_InitMonitorReadRepeatMenuItem
        '
        Me._InitMonitorReadRepeatMenuItem.Name = "_InitMonitorReadRepeatMenuItem"
        Me._InitMonitorReadRepeatMenuItem.Size = New System.Drawing.Size(219, 22)
        Me._InitMonitorReadRepeatMenuItem.Text = "Init, Monitor, Read, Repeat"
        Me._InitMonitorReadRepeatMenuItem.ToolTipText = "Initiates a trigger plan, monitors it, reads and displays buffer and repeats if a" &
    "uto repeat is checked"
        '
        '_RepeatMenuItem
        '
        Me._RepeatMenuItem.CheckOnClick = True
        Me._RepeatMenuItem.Name = "_RepeatMenuItem"
        Me._RepeatMenuItem.Size = New System.Drawing.Size(219, 22)
        Me._RepeatMenuItem.Text = "Repeat"
        Me._RepeatMenuItem.ToolTipText = "Repeat initiating the trigger plan"
        '
        '_StreamMenuItem
        '
        Me._StreamMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ToggleBufferStreamMenuItem})
        Me._StreamMenuItem.Name = "_StreamMenuItem"
        Me._StreamMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._StreamMenuItem.Text = "Stream"
        '
        '_ToggleBufferStreamMenuItem
        '
        Me._ToggleBufferStreamMenuItem.CheckOnClick = False
        Me._ToggleBufferStreamMenuItem.Name = "_ToggleBufferStreamMenuItem"
        Me._ToggleBufferStreamMenuItem.Size = New System.Drawing.Size(174, 22)
        Me._ToggleBufferStreamMenuItem.Text = "Start Buffer Stream"
        Me._ToggleBufferStreamMenuItem.ToolTipText = "Toggles streaming values"
        '
        '_AbortButton
        '
        Me._AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AbortButton.Image = CType(resources.GetObject("_AbortButton.Image"), System.Drawing.Image)
        Me._AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(41, 25)
        Me._AbortButton.Text = "Abort"
        Me._AbortButton.ToolTipText = "Aborts triggering"
        '
        '_BufferCountLabel
        '
        Me._BufferCountLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._BufferCountLabel.Name = "_BufferCountLabel"
        Me._BufferCountLabel.Size = New System.Drawing.Size(13, 25)
        Me._BufferCountLabel.Text = "0"
        Me._BufferCountLabel.ToolTipText = "Buffer count"
        '
        '_LastPointNumberLabel
        '
        Me._LastPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._LastPointNumberLabel.Name = "_LastPointNumberLabel"
        Me._LastPointNumberLabel.Size = New System.Drawing.Size(13, 25)
        Me._LastPointNumberLabel.Text = "2"
        Me._LastPointNumberLabel.ToolTipText = "Number of last buffer reading"
        '
        '_FirstPointNumberLabel
        '
        Me._FirstPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._FirstPointNumberLabel.Name = "_FirstPointNumberLabel"
        Me._FirstPointNumberLabel.Size = New System.Drawing.Size(13, 25)
        Me._FirstPointNumberLabel.Text = "1"
        Me._FirstPointNumberLabel.ToolTipText = "Number of the first buffer reading"
        '
        '_BufferSizeNumericLabel
        '
        Me._BufferSizeNumericLabel.Name = "_BufferSizeNumericLabel"
        Me._BufferSizeNumericLabel.Size = New System.Drawing.Size(30, 25)
        Me._BufferSizeNumericLabel.Text = "Size:"
        '
        '_BufferSizeNumeric
        '
        Me._BufferSizeNumeric.AutoSize = False
        Me._BufferSizeNumeric.Name = "_BufferSizeNumeric"
        Me._BufferSizeNumeric.Size = New System.Drawing.Size(71, 25)
        Me._BufferSizeNumeric.Text = "0"
        Me._BufferSizeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_TriggerStateLabel
        '
        Me._TriggerStateLabel.Name = "_TriggerStateLabel"
        Me._TriggerStateLabel.Size = New System.Drawing.Size(26, 25)
        Me._TriggerStateLabel.Text = "idle"
        Me._TriggerStateLabel.ToolTipText = "Trigger state"
        '
        'BufferStreamMonitorView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "BufferStreamMonitorView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(358, 27)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _BufferCountLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _LastPointNumberLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _FirstPointNumberLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _BufferSizeNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _InitiateTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AbortStartTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _MonitorActiveTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InitMonitorReadRepeatMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RepeatMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ToggleBufferStreamMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _BufferSizeNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _AbortButton As Windows.Forms.ToolStripButton
    Private WithEvents _TriggerStateLabel As Core.Controls.ToolStripLabel
    Private WithEvents _TriggerMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _DisplayMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearBufferDisplayMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _DisplayBufferMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _StreamMenuItem As Windows.Forms.ToolStripMenuItem
End Class
