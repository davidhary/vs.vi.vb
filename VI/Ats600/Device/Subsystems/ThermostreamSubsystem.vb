''' <summary>
''' Defines a SCPI Sense Current Subsystem for a InTest Thermo Stream instrument.
''' </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2014-03-01, 3.0.5173. </para>
''' </remarks>
Public Class ThermostreamSubsystem
    Inherits VI.ThermostreamSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ThermostreamSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._TemperatureEventInfo = New TemperatureEventInfo(TemperatureEvents.None)
        Me._AuxiliaryStatusInfo = New AuxiliaryStatusInfo(AuxiliaryStatuses.None)
        Me._ThermalProfile = New ThermalProfileCollection
        Me._AmbientTemperature = 25
    End Sub
#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.CycleCountRange = New isr.Core.Constructs.RangeI(1, 9999)
        Me.DeviceThermalConstantRange = New isr.Core.Constructs.RangeI(20, 500)
        Me.LowRampRateRange = New isr.Core.Constructs.RangeR(0, 99.99)
        Me.HighRampRateRange = New isr.Core.Constructs.RangeR(100, 9999)
        Me.SetpointRange = New isr.Core.Constructs.RangeR(-99.9, 225)
        Me.SetpointNumberRange = New isr.Core.Constructs.RangeI(0, 11)
        Me.SetpointWindowRange = New isr.Core.Constructs.RangeR(0.1, 9.9)
        Me.SoakTimeRange = New isr.Core.Constructs.RangeI(0, 9999)
        Me.MaximumTestTimeRange = New isr.Core.Constructs.RangeI(0, 9999)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.QuerySystemScreen()
        If Not Me.OperatorScreen.GetValueOrDefault(False) Then
            Me.ApplyOperatorScreen()
        End If
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " READ TEMPERATURE "

    ''' <summary> Reads current set point values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ReadCurrentSetpointValues()
        Me.QuerySetpointNumber()
        Me.QuerySetpointWindow()
        Me.QuerySoakTime()
        Me.QuerySetpoint()
        Me.QueryRampRate()
    End Sub

    ''' <summary> Searches for the zero-based last setpoint number. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> The found setpoint number. </returns>
    Public Function FindLastSetpointNumber() As Integer
        Dim currentSetpoint As Integer = Me.SetpointNumber.GetValueOrDefault(-1)
        Dim setPointNumber As Integer = -1
        For i As Integer = Me.SetpointNumberRange.Min To Me.SetpointNumberRange.Max
            Me.ApplySetpointNumber(i)
            If Me.Session.IsSessionOpen Then
                isr.Core.ApplianceBase.DoEventsWait(Me.NextSetpointRefractoryTimeSpan)
            End If
            Me.QueryRampRate()
            If Me.RampRate.HasValue AndAlso Math.Abs(Me.RampRate.Value) > 0.01 Then
                setPointNumber = i
            Else
                Exit For
            End If
        Next
        If currentSetpoint >= 0 Then Me.ApplySetpointNumber(currentSetpoint)
        Return setPointNumber
    End Function

#End Region

#Region " DEVICE ERROR "

    ''' <summary> Gets or sets the DeviceError query command. </summary>
    ''' <value> The DeviceError query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String = "EROR?"

#End Region

#Region " CYCLE COUNT "

    ''' <summary> Gets or sets the Cycle Count command format. </summary>
    ''' <value> the Cycle Count command format. </value>
    Protected Overrides Property CycleCountCommandFormat As String = "CYCC {0}"

    ''' <summary> Gets or sets the Cycle Count query command. </summary>
    ''' <value> the Cycle Count query command. </value>
    Protected Overrides Property CycleCountQueryCommand As String = "CYCC?"

#End Region

#Region " CYCLE NUMBER "

    ''' <summary> Gets or sets the Cycle Number query command. </summary>
    ''' <value> the Cycle Number query command. </value>
    Protected Overrides Property CycleNumberQueryCommand As String = "CYCL?"

#End Region

#Region " CYCLING: START / STOP "

    ''' <summary> Gets or sets the command execution refractory time span. </summary>
    ''' <value> The command execution refractory time span. </value>
    Public Overrides ReadOnly Property CommandRefractoryTimeSpan As TimeSpan = TimeSpan.FromMilliseconds(5)

    ''' <summary> Gets or sets the start cycling command. </summary>
    ''' <value> The start cycling command. </value>
    Protected Overrides Property StartCyclingCommand As String = "CYCL 1"

    ''' <summary> Gets or sets the stop cycling command. </summary>
    ''' <value> The stop cycling command. </value>
    Protected Overrides Property StopCyclingCommand As String = "CYCL 0"

    ''' <summary> true if use cycles completed. </summary>
    Public Const UseCyclesCompleted As Boolean = False

    ''' <summary> Start cycling and wait till confirmed. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="countdown"> The countdown. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Overloads Function StartCycling(ByVal countdown As Integer) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        ' send the start command
        Me.StartCycling()
        Dim cycleCompleted As Boolean = True
        Dim setpointNumber As Integer = -1
        Dim cycleNumber As Integer
        Do
            countdown -= 1
            isr.Core.ApplianceBase.DoEventsWait(Me.CommandRefractoryTimeSpan)
            Me.QueryCycleNumber()
            cycleNumber = Me.CycleNumber.GetValueOrDefault(-1)
            If cycleNumber > 0 Then
                Me.QueryTemperatureEventStatus()
                cycleCompleted = Me.IsCycleCompleted OrElse (ThermostreamSubsystem.UseCyclesCompleted AndAlso Me.IsCyclesCompleted)
                Me.QuerySetpointNumber()
                setpointNumber = Me.SetpointNumber.GetValueOrDefault(-1)
            End If
        Loop Until countdown = 0 OrElse (cycleNumber > 0 AndAlso Not cycleCompleted AndAlso setpointNumber = 0)
        If Me.IsCyclingStopped Then
            result = (False, $"Thermo-Stream cycle stopped on error. Testing will stop.")
        ElseIf Not cycleCompleted Then
            result = (False, $"Thermo-Stream failed starting - cycle is tagged as completed after sending start. Testing will stop.")
        ElseIf cycleNumber <= 0 Then
            result = (False, $"Thermo-Stream failed starting - cycle number = {cycleNumber}. Testing will stop.")
        ElseIf setpointNumber > 0 Then
            result = (False, $"Thermo-Stream failed starting - set point = {setpointNumber}. Testing will stop.")
        Else
            ' got a start
        End If
        Return result
    End Function

    ''' <summary> Stop cycling and wait till confirmed. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="countdown"> The countdown. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Overloads Function StopCycling(ByVal countdown As Integer) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        ' send the start command
        Me.StopCycling()
        Dim headUp As Boolean = False
        Dim cycleNumber As Integer
        Do
            countdown -= 1
            isr.Core.ApplianceBase.DoEventsWait(Me.CommandRefractoryTimeSpan)
            Me.QueryCycleNumber()
            cycleNumber = Me.CycleNumber.GetValueOrDefault(-1)
            If cycleNumber > 0 Then
                Me.QueryAuxiliaryEventStatus()
                headUp = Me.IsHeadUp
            End If
        Loop Until countdown = 0 OrElse cycleNumber = 0 AndAlso headUp
        If Not headUp Then
            result = (False, $"Thermo-Stream failed stopping--head is not up. Testing will stop.")
        ElseIf cycleNumber > 0 Then
            result = (False, $"Thermo-Stream failed starting--cycle number = {cycleNumber}. Testing will stop.")
        Else
            ' got a stop
        End If
        Return result
    End Function

#End Region

#Region " DEVICE CONTROL "

    ''' <summary> Gets or sets the Device Control command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property DeviceControlCommandFormat As String = "DUTM {0:        '1';'1';'0'}"

    ''' <summary> Gets or sets the Device Control query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property DeviceControlQueryCommand As String = "DUTM?"

#End Region

#Region " DEVICE SENSOR TYPE "

    ''' <summary> Gets or sets the Device Sensor Type command format. </summary>
    ''' <value> the Device Sensor Type command format. </value>
    Protected Overrides Property DeviceSensorTypeCommandFormat As String = "DSNS {0}"

    ''' <summary> Gets or sets the Device Sensor Type query command. </summary>
    ''' <value> the Device Sensor Type query command. </value>
    Protected Overrides Property DeviceSensorTypeQueryCommand As String = "DSNS?"

#End Region

#Region " DEVICE THERMAL CONSTANT "

    ''' <summary> Gets or sets the Device Thermal Constant command format. </summary>
    ''' <value> the Device Thermal Constant command format. </value>
    Protected Overrides Property DeviceThermalConstantCommandFormat As String = "DUTC {0}"

    ''' <summary> Gets or sets the Device Thermal Constant query command. </summary>
    ''' <value> the Device Thermal Constant query command. </value>
    Protected Overrides Property DeviceThermalConstantQueryCommand As String = "DUTC?"

#End Region

#Region " HEAD STATUS "

    ''' <summary> Gets or sets the head down command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property HeadDownCommandFormat As String = "HEAD {0:'1';'1';'0'}"

    ''' <summary> Gets or sets the head down query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property HeadDownQueryCommand As String = "HEAD?"

#End Region

#Region " RESET OPERATOR / CYCLE MODE "

    ''' <summary>
    ''' Gets or sets the refractory time span for resetting to Cycle (manual) mode.
    ''' </summary>
    ''' <value> The command execution refractory time span. </value>
    Public Overrides ReadOnly Property ResetCycleScreenRefractoryTimeSpan As TimeSpan = TimeSpan.FromMilliseconds(4000)

    ''' <summary> Gets or sets the Reset Cycle Screen command. </summary>
    ''' <value> The Reset Cycle Screen command. </value>
    Protected Overrides Property ResetCycleScreenCommand As String = "*RST"

    ''' <summary> Cycle Screen. </summary>
    Private _CycleScreen As Boolean?

    ''' <summary> Gets or sets the cached Cycle Screen sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Cycle Screen is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property CycleScreen As Boolean?
        Get
            Return Me._CycleScreen
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.CycleScreen, value) Then
                Me._CycleScreen = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Cycle screen value. </summary>
    ''' <value> The Cycle screen value. </value>
    Public Property CycleScreenValue As Integer = 10

    ''' <summary>
    ''' Queries the Operator Screen sentinel. Also sets the
    ''' <see cref="OperatorScreen">Operator Screen</see> sentinel.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Overrides Function QuerySystemScreen() As Integer?
        MyBase.QuerySystemScreen()
        If Me.SystemScreen.HasValue Then
            Me.OperatorScreen = Me.SystemScreen.Value = Me.OperatorScreenValue
            Me.CycleScreen = Me.SystemScreen.Value = Me.CycleScreenValue
        Else
            Me.OperatorScreen = New Boolean?
            Me.CycleScreen = New Boolean?
        End If
    End Function

    ''' <summary>
    ''' Queries the Cycle Screen sentinel. Also sets the
    ''' <see cref="CycleScreen">Cycle Screen</see> sentinel.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryCycleScreen() As Boolean?
        Me.QuerySystemScreen()
        Return Me.CycleScreen
    End Function

    ''' <summary> Applies the operator screen. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ApplyCyclesScreen()
        Me.ResetCycleScreen()
        Me.PublishVerbose("Awaiting reset to Cycles (manual mode) screen;. ")
        isr.Core.ApplianceBase.DoEventsWait(Me.ResetCycleScreenRefractoryTimeSpan)
        Me.PublishVerbose("Querying system screen;. ")
        Me.QuerySystemScreen()
    End Sub

    ''' <summary> Gets or sets the refractory time span for resetting to Operator Mode. </summary>
    ''' <value> The command execution refractory time span. </value>
    Public Overrides ReadOnly Property ResetOperatorScreenRefractoryTimeSpan As TimeSpan = TimeSpan.FromMilliseconds(1000)

    ''' <summary> Gets or sets the Reset Operator Screen command. </summary>
    ''' <value> The Reset Operator Screen command. </value>
    Protected Overrides Property ResetOperatorScreenCommand As String = "RSTO"

    ''' <summary> Operator Screen. </summary>
    Private _OperatorScreen As Boolean?

    ''' <summary> Gets or sets the cached Operator Screen sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Operator Screen is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property OperatorScreen As Boolean?
        Get
            Return Me._OperatorScreen
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.OperatorScreen, value) Then
                Me._OperatorScreen = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the operator screen value. </summary>
    ''' <value> The operator screen value. </value>
    Public Property OperatorScreenValue As Integer = 10

    ''' <summary>
    ''' Queries the Operator Screen sentinel. Also sets the
    ''' <see cref="OperatorScreen">Operator Screen</see> sentinel.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryOperatorScreen() As Boolean?
        Me.QuerySystemScreen()
        Return Me.OperatorScreen
    End Function

    ''' <summary> Applies the operator screen. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ApplyOperatorScreen()
        Me.ResetOperatorScreen()
        Me.PublishVerbose("Awaiting reset to Operator screen;. ")
        isr.Core.ApplianceBase.DoEventsWait(Me.ResetOperatorScreenRefractoryTimeSpan)
        Me.PublishVerbose("Querying system screen;. ")
        Me.QuerySystemScreen()
    End Sub

#End Region

#Region " RAMP RATE "

    ''' <summary> Gets Low the Ramp Rate command format. </summary>
    ''' <value> the Low Ramp Rate command format. </value>
    Protected Overrides Property LowRampRateCommandFormat As String = "RAMP {0:0.0}"

    ''' <summary> Gets High the Ramp Rate command format. </summary>
    ''' <value> the High Ramp Rate command format. </value>
    Protected Overrides Property HighRampRateCommandFormat As String = "RAMP {0:0}"

    ''' <summary> Gets the Ramp Rate query command. </summary>
    ''' <value> the Ramp Rate query command. </value>
    Protected Overrides Property RampRateQueryCommand As String = "RAMP?"

#End Region

#Region " SET POINT "

    ''' <summary> Gets The Set Point command format. </summary>
    ''' <value> The Set Point command format. </value>
    Protected Overrides Property SetpointCommandFormat As String = "SETP {0}"

    ''' <summary> Gets The Set Point query command. </summary>
    ''' <value> The Set Point query command. </value>
    Protected Overrides Property SetpointQueryCommand As String = "SETP?"

#End Region

#Region " SET POINT: NEXT "

    ''' <summary> Gets the next setpoint refractory time span. </summary>
    ''' <value> The next setpoint refractory time span. </value>
    Public Overrides ReadOnly Property NextSetpointRefractoryTimeSpan As TimeSpan = TimeSpan.FromMilliseconds(100)

    ''' <summary> Gets the Next Set Point command. </summary>
    ''' <value> The Next Set Point command. </value>
    Protected Overrides Property NextSetpointCommand As String = "NEXT"

    ''' <summary> True to cycle completed cache. </summary>
    Private _CycleCompletedCache As Boolean

    ''' <summary> Gets a value indicating whether the cycle completed cache. </summary>
    ''' <value> <c>true</c> if cycle completed cache; otherwise <c>false</c> </value>
    Public ReadOnly Property CycleCompletedCache As Boolean
        Get
            Return Me._CycleCompletedCache
        End Get
    End Property

    ''' <summary> Query if this object is cycle ended. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> True if cycle completed; otherwise, false. </returns>
    Public Function QueryCycleCompleted() As Boolean
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me.QueryTemperatureEventStatus()
        Dim affirmative As Boolean = Me.IsCycleCompleted OrElse (ThermostreamSubsystem.UseCyclesCompleted AndAlso Me.IsCyclesCompleted)
        If Not affirmative Then
            Me.QueryAuxiliaryEventStatus()
            affirmative = Me.IsHeadUp
            If Not affirmative Then
                Me.QueryRampRate()
                affirmative = Me.RampRate.GetValueOrDefault(0) < 0.1
            End If
        End If
        Return affirmative
    End Function

    ''' <summary> Move next setpoint. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="countdown"> The countdown. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function MoveNextSetpoint(ByVal countdown As Integer) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        ' otherwise, save the current setpoint number
        Me.QuerySetpointNumber()
        If Me.SetpointNumber.HasValue Then
            Dim setpointNumber As Integer = Me.SetpointNumber.Value
            ' move next.
            Me.NextSetpoint()
            Me._CycleCompletedCache = False
            Do
                countdown -= 1
                isr.Core.ApplianceBase.DoEventsWait(Me.NextSetpointRefractoryTimeSpan)
                Me._CycleCompletedCache = Me.QueryCycleCompleted()
                If Not Me._CycleCompletedCache Then Me.QuerySetpointNumber()
            Loop Until countdown = 0 OrElse Me._CycleCompletedCache OrElse Me.SetpointNumber.GetValueOrDefault(setpointNumber) <> setpointNumber
            If Me.IsCyclingStopped Then
                result = (False, "Thermo-Stream cycle stopped on error. Testing will stop.")
            ElseIf Me._CycleCompletedCache Then
                ' done with this cycle, no need to check the new setpoint number
            ElseIf Me.SetpointNumber.GetValueOrDefault(setpointNumber) = setpointNumber Then
                result = (False, $"Thermo-Stream failed advancing the setpoint number from {setpointNumber}. Testing will stop.")
            Else
                ' got a new setpoint number
            End If
        Else
            result = (False, "Thermo-Stream failed reading the setpoint number--value is empty. Testing will stop.")
            isr.Core.ApplianceBase.DoEvents()
        End If
        Return result
    End Function

    ''' <summary>
    ''' Queries if cycles stopped or terminated; Otherwise, steps to the next temperature.
    ''' </summary>
    ''' <remarks> This is the same as indicating that the start of test signal was received. </remarks>
    ''' <param name="countdown">      The countdown. </param>
    ''' <param name="additionalInfo"> Information describing the additional. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ConditionalCycleMoveNext(ByVal countdown As Integer, ByVal additionalInfo As String) As (Success As Boolean, Details As String)
        If String.IsNullOrEmpty(additionalInfo) Then additionalInfo = "waiting for At-Temp"
        Me.QueryTemperatureEventStatus()
        If Me.IsCycleCompleted Then
            Return (False, $"Thermo-Stream cycle completed while {additionalInfo}")
        ElseIf ThermostreamSubsystem.UseCyclesCompleted AndAlso Me.IsCyclesCompleted Then
            Return (False, $"Thermo-Stream completed all cycles while  {additionalInfo}")
        ElseIf Me.IsCyclingStopped Then
            Return (False, $"Thermo-Stream cycle was stopped while  {additionalInfo}")
        Else
            Dim r As (Affirmative As Boolean, Success As Boolean, Details As String) = Me.ValidateHandlerHeadDown()
            Return If(r.Success, Me.MoveNextSetpoint(countdown), (False, $"Unable to proceed {additionalInfo};. Details: { r.Details}"))
        End If
    End Function

#End Region

#Region " SET POINT NUMBER "

    ''' <summary> Gets the Set Point Number command format. </summary>
    ''' <value> the Set Point Number command format. </value>
    Protected Overrides Property SetpointNumberCommandFormat As String = "SETN {0}"

    ''' <summary> Gets the Set Point Number query command. </summary>
    ''' <value> the Set Point Number query command. </value>
    Protected Overrides Property SetpointNumberQueryCommand As String = "SETN?"

#End Region

#Region " SET POINT WINDOW "

    ''' <summary> Gets the Set Point Window command format. </summary>
    ''' <value> the Set Point Window command format. </value>
    Protected Overrides Property SetpointWindowCommandFormat As String = "WNDW {0}"

    ''' <summary> Gets the Set Point Window query command. </summary>
    ''' <value> the Set Point Window query command. </value>
    Protected Overrides Property SetpointWindowQueryCommand As String = "WNDW?"

#End Region

#Region " SOAK TIME "

    ''' <summary> Gets the Soak Time command format. </summary>
    ''' <value> the Soak Time command format. </value>
    Protected Overrides Property SoakTimeCommandFormat As String = "SOAK {0}"

    ''' <summary> Gets the Soak Time query command. </summary>
    ''' <value> the Soak Time query command. </value>
    Protected Overrides Property SoakTimeQueryCommand As String = "SOAK?"

#End Region

#Region " TEMPERATURE "

    ''' <summary> Gets the Temperature query command. </summary>
    ''' <value> The Temperature query command. </value>
    Protected Overrides Property TemperatureQueryCommand As String = "TEMP?"

#End Region

#Region " MAXIMUM TEST TIME "

    ''' <summary> Gets the Maximum Test Time command format. </summary>
    ''' <value> the Maximum Test Time command format. </value>
    Protected Overrides Property MaximumTestTimeCommandFormat As String = "TTIM {0}"

    ''' <summary> Gets the Maximum Test Time query command. </summary>
    ''' <value> the Maximum Test Time query command. </value>
    Protected Overrides Property MaximumTestTimeQueryCommand As String = "TTIM?"

#End Region

#Region " SYSTEM SCREEN "

    ''' <summary> Gets the System Screen query command. </summary>
    ''' <value> The System Screen query command. </value>
    Protected Overrides Property SystemScreenQueryCommand As String = "WHAT?"

#End Region

#End Region

#Region " PROFILE "

    ''' <summary> The thermal profile. </summary>
    Private _ThermalProfile As ThermalProfileCollection

    ''' <summary> Thermal profile. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A ThermalProfileCollection. </returns>
    Public Function ThermalProfile() As ThermalProfileCollection
        Return Me._ThermalProfile
    End Function

    ''' <summary> Creates new profile. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub CreateNewProfile()
        Me._ThermalProfile = New ThermalProfileCollection
    End Sub

    ''' <summary> The active thermal setpoint. </summary>
    Private _ActiveThermalSetpoint As ThermalSetpoint

    ''' <summary> Gets the active thermal profile setpoint. </summary>
    ''' <value> The active thermal setpoint. </value>
    Public ReadOnly Property ActiveThermalSetpoint As ThermalSetpoint
        Get
            Return Me._ActiveThermalSetpoint
        End Get
    End Property

    ''' <summary> Adds a new setpoint. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="number"> Number of. </param>
    ''' <returns> A ThermalSetpoint. </returns>
    Public Function AddNewSetpoint(ByVal number As Integer) As ThermalSetpoint
        Return Me._ThermalProfile.AddNewSetpoint(number)
    End Function

    ''' <summary> Gets or sets the ambient temperature. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The ambient temperature. </value>
    Public Property AmbientTemperature As Integer

    ''' <summary> Gets or sets the hot setpoint number. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The hot setpoint number. </value>
    Public Property HotSetpointNumber As Integer = 0

    ''' <summary> Gets or sets the ambient setpoint number. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The ambient setpoint number. </value>
    Public Property AmbientSetpointNumber As Integer = 1

    ''' <summary> Gets or sets the cold setpoint number. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The cold setpoint number. </value>
    Public Property ColdSetpointNumber As Integer = 2

    ''' <summary> Parse setpoint number. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="temperature"> The temperature. </param>
    ''' <returns> An Integer. </returns>
    Public Function ParseSetpointNumber(ByVal temperature As Double) As Integer
        If temperature < Me.AmbientTemperature Then
            Return Me.ColdSetpointNumber
        ElseIf temperature > Me.AmbientTemperature Then
            Return Me.HotSetpointNumber
        Else
            Return Me.AmbientSetpointNumber
        End If
    End Function

    ''' <summary> Builds a command. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="setpoint"> The setpoint. </param>
    ''' <returns> A String. </returns>
    Public Function BuildCommand(ByVal setpoint As ThermalSetpoint) As String
        If setpoint Is Nothing Then Throw New ArgumentNullException(NameOf(setpoint))
        Dim builder As New Text.StringBuilder()
        Dim delimiter As String = "; "
        builder.AppendFormat(Me.SetpointNumberCommandFormat, Me.ParseSetpointNumber(setpoint.Temperature))
        builder.Append(delimiter)
        builder.AppendFormat(Me.SetpointCommandFormat, setpoint.Temperature)
        builder.Append(delimiter)
        ' moded out -- must be issued separately.
        ' Dim value As Double = 60 * setpoint.RampRate
        ' If Me.LowRampRateRange.Contains(value) Then
        '     builder.AppendFormat(Me.LowRampRateCommandFormat, value)
        ' ElseIf Me.HighRampRateRange.Contains(value) Then
        '     builder.AppendFormat(Me.HighRampRateCommandFormat, value)
        ' Else
        '     Throw New InvalidOperationException($"Ramp range {value} is outside both the low {Me.LowRampRateRange.ToString} and high {Me.HighRampRateRange.ToString} ranges")
        ' End If
        ' builder.Append(delimiter)
        builder.AppendFormat(Me.SoakTimeCommandFormat, Math.Max(Me.SoakTimeRange.Min, Math.Min(Me.SoakTimeRange.Max, setpoint.SoakSeconds)))
        builder.Append(delimiter)
        builder.AppendFormat(Me.SetpointWindowCommandFormat, Math.Max(Me.SetpointWindowRange.Min, Math.Min(Me.SetpointWindowRange.Max, setpoint.Window)))
        Return builder.ToString
    End Function

    ''' <summary> Applies the active thermal setpoint described by setpoint. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ApplyActiveThermalSetpoint()
        Me.Session.Execute(Me.BuildCommand(Me.ActiveThermalSetpoint))
        ' must issue this separately.
        Me.ApplyRampRate(60 * Me.ActiveThermalSetpoint.RampRate)
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Select active thermal setpoint. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function SelectActiveThermalSetpoint(ByVal thermalProfileSetpointNumber As Integer) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If Me.ThermalProfile Is Nothing Then
            result = (False, "Thermo-Stream thermal profile not set")
        ElseIf Me.ThermalProfile.Count = 0 Then
            result = (False, "Thermo-Stream thermal profile is empty")
        ElseIf thermalProfileSetpointNumber < 1 Then
            result = (False, $"Thermo-Stream thermal profile setpoint number {thermalProfileSetpointNumber} must be positive")
        ElseIf thermalProfileSetpointNumber > Me.ThermalProfile.Count Then
            result = (False, $"Thermo-Stream thermal profile setpoint number {thermalProfileSetpointNumber} is out of range [1,{Me.ThermalProfile.Count}]")
        Else
            Me._ActiveThermalSetpoint = Me.ThermalProfile.Item(thermalProfileSetpointNumber)
        End If
        Return result
    End Function

    ''' <summary> Validates the thermal setpoint. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="setpoint"> The setpoint. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ValidateThermalSetpoint(ByVal setpoint As ThermalSetpoint) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If setpoint Is Nothing Then Throw New ArgumentNullException(NameOf(setpoint))
        Me.QuerySetpointNumber()
        If Me.SetpointNumber.HasValue Then
            Me.QuerySetpoint()
            If Me.Setpoint.HasValue Then
                Me.QueryRampRate()
                If Me.RampRate.HasValue Then
                    Me.QuerySoakTime()
                    If Me.SoakTime.HasValue Then
                        Me.QuerySetpointWindow()
                        If Me.SetpointWindow.HasValue Then
                            If Me.SetpointNumber.Value = Me.ParseSetpointNumber(setpoint.Temperature) Then
                                If Math.Abs(Me.Setpoint.Value - setpoint.Temperature) < 0.1F Then
                                    If Math.Abs(Me.RampRate.Value - 60 * setpoint.RampRate) < 1.1F Then
                                        If Math.Abs(Me.SoakTime.Value - setpoint.SoakSeconds) < 1.1F Then
                                            If Math.Abs(Me.SetpointWindow.Value - setpoint.Window) < 0.1F Then
                                            Else
                                                result = (False, $"Thermo-Stream failed setting the setpoint window {Me.SetpointWindow.Value} to {setpoint.Window} {Arebis.StandardUnits.TemperatureUnits.DegreeCelsius.Symbol}.")
                                            End If
                                        Else
                                            result = (False, $"Thermo-Stream failed setting the soak time {Me.SoakTime.Value} to {setpoint.SoakSeconds} s.")
                                        End If
                                    Else
                                        result = (False, $"Thermo-Stream failed setting the ramp rate {Me.RampRate.Value} to {60 * setpoint.RampRate} {Me.RampRateUnit.Symbol}.")
                                    End If
                                Else
                                    result = (False, $"Thermo-Stream failed setting the setpoint {Me.Setpoint.Value} to {setpoint.Temperature} {Arebis.StandardUnits.TemperatureUnits.DegreeCelsius.Symbol}.")
                                End If
                            Else
                                result = (False, $"Thermo-Stream failed setting the setpoint number {Me.SetpointNumber.Value} to {Me.ParseSetpointNumber(setpoint.Temperature)}.")
                            End If
                        Else
                            result = (False, $"Thermo-Stream failed reading the setpoint window--value is empty.")
                        End If
                    Else
                        result = (False, $"Thermo-Stream failed reading the soak time--value is empty.")
                    End If
                Else
                    result = (False, $"Thermo-Stream failed reading the ramp rate--value is empty.")
                End If
            Else
                result = (False, $"Thermo-Stream failed reading the setpoint--value is empty.")
            End If
        Else
            result = (False, $"Thermo-Stream failed reading the setpoint number--value is empty.")
        End If
        isr.Core.ApplianceBase.DoEvents()
        Return result
    End Function

    ''' <summary> Conditional apply setpoint. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
    ''' <param name="countdown">                    The countdown. </param>
    ''' <param name="additionalInfo">               Information describing the additional. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ConditionalApplySetpoint(ByVal thermalProfileSetpointNumber As Integer, ByVal countdown As Integer,
                                        ByVal additionalInfo As String) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(additionalInfo) Then additionalInfo = $"applying setpoint {thermalProfileSetpointNumber};"
        Dim result As (Success As Boolean, Details As String) = Me.SelectActiveThermalSetpoint(thermalProfileSetpointNumber)
        If result.Success Then
            Me.ApplyActiveThermalSetpoint()
            Do
                isr.Core.ApplianceBase.DoEventsWait(Me.CommandRefractoryTimeSpan)
                result = Me.ValidateThermalSetpoint(Me.ThermalProfile.Item(thermalProfileSetpointNumber))
                countdown -= 1
            Loop Until result.Success OrElse countdown = 0
        End If
        If Not result.Success Then result = (False, $"Thermo-Stream failed {additionalInfo};. Details: {result.Details}")
        Return result
    End Function

    ''' <summary> Conditional move first. This assumes the head is still up. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
    ''' <param name="countdown">                    The countdown. </param>
    ''' <param name="additionalInfo">               Information describing the additional. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ConditionalMoveFirst1(ByVal thermalProfileSetpointNumber As Integer, ByVal countdown As Integer,
                                          ByVal additionalInfo As String) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(additionalInfo) Then additionalInfo = "selecting first profile @ head up; clear to start"
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = Me.ValidateHandlerHeadUp()
        Return If(result.Success,
                        Me.ConditionalApplySetpoint(thermalProfileSetpointNumber, countdown, additionalInfo),
                        (False, $"Thermo-Stream is unable to start while {additionalInfo};. Details: {result.Details}"))
    End Function

    ''' <summary> Conditional move next. Expects the head to be down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
    ''' <param name="countdown">                    The countdown. </param>
    ''' <param name="additionalInfo">               Information describing the additional. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ConditionalMoveNext1(ByVal thermalProfileSetpointNumber As Integer, ByVal countdown As Integer,
                                   ByVal additionalInfo As String) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(additionalInfo) Then additionalInfo = "moving to next profile"
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = Me.ValidateHandlerHeadDown()
        Return If(result.Success,
                    Me.ConditionalApplySetpoint(thermalProfileSetpointNumber, countdown, additionalInfo),
                    (False, $"Thermo-Stream is unable to start while {additionalInfo};. Details: {result.Details}"))
    End Function

    ''' <summary> Query if 'setpointNumber' exceeds the profile count. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="setpointNumber"> The setpoint number. </param>
    ''' <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
    Public Function IsProfileCompleted(ByVal setpointNumber As Integer) As (Affirmative As Boolean, Success As Boolean, Details As String)
        If Me.ThermalProfile Is Nothing Then
            Return (False, False, $"Thermo-Stream thermal profile not set")
        ElseIf Me.ThermalProfile.Count = 0 Then
            Return (False, False, $"Thermo-Stream thermal profile is empty")
        ElseIf setpointNumber < 1 Then
            Return (False, False, $"Thermo-Stream thermal profile setpoint number {setpointNumber} must be positive")
        ElseIf setpointNumber > Me.ThermalProfile.Count Then
            Return (True, True, String.Empty)
        Else
            Return (False, False, $"Unknown case")
        End If
    End Function
#End Region

#Region " REGISTERS "

#Region " TEMPERATURE EVENT "

    ''' <summary> Gets or sets the Temperature event condition query command. </summary>
    ''' <value> The Temperature event condition query command. </value>
    Protected Overrides Property TemperatureEventConditionQueryCommand As String = "TECR?"

    ''' <summary> Gets or sets the temperature event enable mask command format. </summary>
    ''' <value> The temperature event enable mask command format. </value>
    Protected Overrides Property TemperatureEventEnableMaskCommandFormat As String = "TESE {0:D}"

    ''' <summary> Gets or sets the temperature event enable mask query command. </summary>
    ''' <value> The temperature event enable mask query command. </value>
    Protected Overrides Property TemperatureEventEnableMaskQueryCommand As String = "TESE?"

    ''' <summary> Gets or sets the Temperature Event status query command. </summary>
    ''' <value> The Temperature status query command. </value>
    Protected Overrides Property TemperatureEventStatusQueryCommand As String = "TESR?"

    ''' <summary> Gets or sets the cached Temperature Event sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Temperature Event is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Overrides Property TemperatureEventStatus As Integer?
        Get
            Return MyBase.TemperatureEventStatus
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Integer?.Equals(Me.TemperatureEventStatus, value) Then
                MyBase.TemperatureEventStatus = value
                If value.GetValueOrDefault(0) > 0 Then
                    ' update the event only if having a new value -- this way the event info gets latched.
                    Me.TemperatureEventInfo = New TemperatureEventInfo(CType(value.Value, TemperatureEvents))
                End If
            End If
        End Set
    End Property

    ''' <summary> Information describing the temperature event. </summary>
    Private _TemperatureEventInfo As TemperatureEventInfo

    ''' <summary> Gets or sets information describing the temperature event. </summary>
    ''' <value> Information describing the temperature event. </value>
    Public Property TemperatureEventInfo As TemperatureEventInfo
        Get
            Return Me._TemperatureEventInfo
        End Get
        Protected Set(value As TemperatureEventInfo)
            If value IsNot Nothing AndAlso Me.TemperatureEventInfo.TemperatureEvent <> value.TemperatureEvent Then
                Me._TemperatureEventInfo = value
                Me.IsAtTemperature = value.IsAtTemperature
                Me.IsNotAtTemperature = value.IsNotAtTemperature
                Me.IsCycleCompleted = value.IsEndOfCycle
                Me.IsCyclesCompleted = value.IsEndOfCycles
                Me.IsCyclingStopped = value.IsCyclingStopped
                Me.IsTestTimeElapsed = value.IsTestTimeElapsed
            End If
        End Set
    End Property

    ''' <summary> True if is cycle completed, false if not. </summary>
    Private _IsCycleCompleted As Boolean

    ''' <summary> Gets or sets a value indicating whether one cycle completed. </summary>
    ''' <value> <c>true</c> if one cycle completed; otherwise <c>false</c> </value>
    Public Property IsCycleCompleted As Boolean
        Get
            Return Me._IsCycleCompleted
        End Get
        Set(value As Boolean)
            If value <> Me.IsCycleCompleted Then
                Me._IsCycleCompleted = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is cycles completed, false if not. </summary>
    Private _IsCyclesCompleted As Boolean

    ''' <summary> Gets or sets a value indicating whether all Cycles completed. </summary>
    ''' <value> <c>true</c> if all Cycles completed; otherwise <c>false</c> </value>
    Public Property IsCyclesCompleted As Boolean
        Get
            Return Me._IsCyclesCompleted
        End Get
        Set(value As Boolean)
            If value <> Me.IsCyclesCompleted Then
                Me._IsCyclesCompleted = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is test time elapsed, false if not. </summary>
    Private _IsTestTimeElapsed As Boolean

    ''' <summary> Gets or sets a value indicating whether test time elapsed. </summary>
    ''' <value> <c>true</c> if this cycling Stopped; otherwise <c>false</c> </value>
    Public Property IsTestTimeElapsed As Boolean
        Get
            Return Me._IsTestTimeElapsed
        End Get
        Set(value As Boolean)
            If value <> Me.IsTestTimeElapsed Then
                Me._IsTestTimeElapsed = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is cycling stopped, false if not. </summary>
    Private _IsCyclingStopped As Boolean

    ''' <summary> Gets or sets a value indicating whether cycling stopped. </summary>
    ''' <value> <c>true</c> if this cycling Stopped; otherwise <c>false</c> </value>
    Public Property IsCyclingStopped As Boolean
        Get
            Return Me._IsCyclingStopped
        End Get
        Set(value As Boolean)
            If value <> Me.IsCyclingStopped Then
                Me._IsCyclingStopped = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is at temperature, false if not. </summary>
    Private _IsAtTemperature As Boolean

    ''' <summary> Gets or sets a value indicating whether this object is at temperature. </summary>
    ''' <remarks>
    ''' Use <see cref="IsNotAtTemperature">not at temp</see> as this is on when the instrument turns
    ''' on.
    ''' </remarks>
    ''' <value> <c>true</c> if this object is at temperature; otherwise <c>false</c> </value>
    Public Property IsAtTemperature As Boolean
        Get
            Return Me._IsAtTemperature
        End Get
        Set(value As Boolean)
            If value <> Me.IsAtTemperature Then
                Me._IsAtTemperature = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True if is not at temperature, false if not. </summary>
    Private _IsNotAtTemperature As Boolean

    ''' <summary> Gets or sets a value indicating whether this object is not at temperature. </summary>
    ''' <value> <c>true</c> if this object is not at temperature; otherwise <c>false</c> </value>
    Public Property IsNotAtTemperature As Boolean
        Get
            Return Me._IsNotAtTemperature
        End Get
        Set(value As Boolean)
            If value <> Me.IsNotAtTemperature Then
                Me._IsNotAtTemperature = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Query if cycles stopped or terminated; Otherwise, queries if reached setpoint.
    ''' </summary>
    ''' <remarks> This is the same as indicating that the start of test signal was received. </remarks>
    ''' <returns> The cycling at temperature. </returns>
    Public Function QueryCyclingAtTemperature() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String)
        Me.QueryTemperatureEventStatus()
        If Me.IsCycleCompleted Then
            result = (False, False, "Thermo-Stream cycle completed while waiting for At-Temp")
        ElseIf ThermostreamSubsystem.UseCyclesCompleted AndAlso Me.IsCyclesCompleted Then
            result = (False, False, "Thermo-Stream completed all cycles while waiting for At-Temp")
        ElseIf Me.IsCyclingStopped Then
            result = (False, False, "Thermo-Stream cycle was stopped while waiting for At-Temp")
        Else
            result = Me.ValidateHandlerHeadDown()
            If Not result.Success Then
                result = (False, False, $"Thermo-Stream is unable to wait for At-Temp;. Details: {result.Details}")
            Else
                ' problem is: at temp may falsely show at temp.
                result = (Me.IsAtTemperature AndAlso Not Me.IsNotAtTemperature, True, String.Empty)
            End If
        End If
        Return result
    End Function

    ''' <summary>
    ''' Query if cycles stopped or terminated; Otherwise, queries if reached setpoint.
    ''' </summary>
    ''' <remarks> This is the same as indicating that the start of test signal was received. </remarks>
    ''' <returns> at temperature. </returns>
    Public Function QueryAtTemperature() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = Me.ValidateHandlerHeadDown()
        If Not result.Success Then
            result = (False, False, $"Thermo-Stream is unable to wait for At-Temp;. Details: {result.Details}")
        Else
            Me.QueryTemperatureEventStatus()
            ' problem is: at temp may falsely show at temp.
            result = (Me.IsAtTemperature AndAlso Not Me.IsNotAtTemperature, True, String.Empty)
        End If
        Return result
    End Function

    ''' <summary> Queries head down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> The cycling head down. </returns>
    Public Overloads Function QueryCyclingHeadDown() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Me.QueryTemperatureEventStatus()
        If Me.IsCycleCompleted Then
            Return (False, False, "Thermo-Stream cycle completed while waiting for head down")
        ElseIf ThermostreamSubsystem.UseCyclesCompleted AndAlso Me.IsCyclesCompleted Then
            Return (False, False, "Thermo-Stream completed all cycles while waiting for head down")
        ElseIf Me.IsCyclingStopped Then
            Return (False, False, "Thermo-Stream cycle was stopped while waiting for head down")
        Else
            Return Me.QueryHeadDown()
        End If
    End Function

    ''' <summary> Queries head down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> The head down. </returns>
    Public Overloads Function QueryHeadDown() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Me.QueryAuxiliaryEventStatus()
        Return If(Not Me.IsReady,
                (False, False, $"Thermo-Stream is no longer ready while waiting for head down;. Ready={CInt(AuxiliaryStatuses.ReadyStartup)}.and.{CInt(Me.AuxiliaryStatusInfo.AuxiliaryStatus)}"),
                (Not Me.IsHeadUp, True, String.Empty))
    End Function

    ''' <summary> Queries head down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> The head up. </returns>
    Public Overloads Function QueryHeadUp() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Me.QueryAuxiliaryEventStatus()
        Return If(Not Me.IsReady,
                    (False, False, $"Thermo-Stream is no longer ready while waiting for head up;. Ready={CInt(AuxiliaryStatuses.ReadyStartup)}.and.{CInt(Me.AuxiliaryStatusInfo.AuxiliaryStatus)}"),
                    (Me.IsHeadUp, True, String.Empty))
    End Function

    ''' <summary> Check if the handler is ready and cycle not completed and head is down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
    Public Function ValidateCyclingHandlerReady() As (Affirmative As Boolean, Success As Boolean, Details As String)
        ' check if handler is ready and head is up
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = Me.ValidateHandlerHeadUp()
        If result.Success Then
            Me.QueryTemperatureEventStatus()
            If Not Me.IsCycleCompleted Then
                result = (False, False, "Thermo-Stream seems busy--cycle not completed--stop and try again")
            ElseIf ThermostreamSubsystem.UseCyclesCompleted AndAlso Not Me.IsCyclesCompleted Then
                result = (False, False, "Thermo-Stream seems busy--cycles not all completed--stop and try again")
            End If
        End If
        Return result
    End Function

    ''' <summary> Check if the handler is ready and cycle not completed and head is down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
    Public Function ValidateHandlerHeadUp() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = (True, True, String.Empty)
        ' check if handler is ready
        Me.QueryAuxiliaryEventStatus()
        If Not Me.IsReady Then
            result = (False, False,
                $"Thermo-Stream is not ready--wait for startup operation to complete and try again;. Ready={CInt(AuxiliaryStatuses.ReadyStartup)}.and.{ CInt(Me.AuxiliaryStatusInfo.AuxiliaryStatus)}")
            ' check if head is up.
        ElseIf Not Me.IsHeadUp Then
            result = (False, False, $"Thermo-Stream head is down;. Up={CInt(AuxiliaryStatuses.HeadUpDown)}.and.{CInt(Me.AuxiliaryStatusInfo.AuxiliaryStatus)}")
        End If
        Return result
    End Function

    ''' <summary> Check if the handler is ready and cycle not completed and head is down. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
    Public Function ValidateHandlerHeadDown() As (Affirmative As Boolean, Success As Boolean, Details As String)
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = (True, True, String.Empty)
        ' check if handler is ready
        Me.QueryAuxiliaryEventStatus()
        If Not Me.IsReady Then
            result = (False, False,
                $"Thermo-Stream is not ready--wait for startup operation to complete and try again;. Ready={CInt(AuxiliaryStatuses.ReadyStartup)}.and.{ CInt(Me.AuxiliaryStatusInfo.AuxiliaryStatus)}")
        ElseIf Me.IsHeadUp Then
            result = (False, False, $"Thermo-Stream head is up;. Up={CInt(AuxiliaryStatuses.HeadUpDown)}.and.{CInt(Me.AuxiliaryStatusInfo.AuxiliaryStatus)}")
        End If
        Return result
    End Function

    ''' <summary> Conditional reset operator mode. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ConditionalResetOperatorMode(ByVal repeatCount As Integer) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Me.QuerySystemScreen()
        If Not Me.OperatorScreen.GetValueOrDefault(False) Then
            result = Me.ResetOperatorMode(repeatCount)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Check if the handler is ready and head is up and switch to operator screen mode then wait
    ''' tile mode established.
    ''' </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function ResetOperatorMode(ByVal repeatCount As Integer) As (Success As Boolean, Details As String)
        Dim result As (Affirmative As Boolean, Success As Boolean, Details As String) = Me.ValidateHandlerHeadUp()
        If result.Success Then
            Me.ResetOperatorScreen()
            isr.Core.ApplianceBase.DoEventsWait(Me.CommandRefractoryTimeSpan)
            Dim opMode As Boolean
            Do
                Me.QuerySystemScreen()
                opMode = Me.OperatorScreen.GetValueOrDefault(False)
                If Not opMode Then
                    isr.Core.ApplianceBase.DoEventsWait(Me.CommandRefractoryTimeSpan)
                    repeatCount -= 1
                End If
            Loop Until opMode OrElse repeatCount = 0
            Return If(opMode, (True, String.Empty), (False, $"Thermo-Stream seems failed switching to operator mode--stop and try again"))
        Else
            Return (False, $"Unable to reset operator mode;. Details: {result.Details}")
        End If
    End Function

#End Region

#Region " AUXILIARY EVENT "

    ''' <summary> Gets or sets the Auxiliary Event status query command. </summary>
    ''' <value> The Auxiliary status query command. </value>
    Protected Overrides Property AuxiliaryEventStatusQueryCommand As String = "AUXC?"

    ''' <summary> Gets or sets the cached Auxiliary Event sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auxiliary Event is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Overrides Property AuxiliaryEventStatus As Integer?
        Get
            Return MyBase.AuxiliaryEventStatus
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Integer?.Equals(Me.AuxiliaryEventStatus, value) Then
                MyBase.AuxiliaryEventStatus = value
                Me.AuxiliaryStatusInfo = New AuxiliaryStatusInfo(CType(value.Value, AuxiliaryStatuses))
                If value.GetValueOrDefault(0) > 0 Then
                    ' update the event only if having a new value -- this way the event info gets latched.
                End If
            End If
        End Set
    End Property

    ''' <summary> Information describing the auxiliary status. </summary>
    Private _AuxiliaryStatusInfo As AuxiliaryStatusInfo

    ''' <summary> Gets or sets information describing the Auxiliary event. </summary>
    ''' <value> Information describing the Auxiliary event. </value>
    Public Property AuxiliaryStatusInfo As AuxiliaryStatusInfo
        Get
            Return Me._AuxiliaryStatusInfo
        End Get
        Protected Set(value As AuxiliaryStatusInfo)
            If value IsNot Nothing AndAlso
                    Me.AuxiliaryStatusInfo.AuxiliaryStatus <> value.AuxiliaryStatus Then
                Me._AuxiliaryStatusInfo = value
                Me.IsDutControl = value.IsDutControl
                Me.IsFlowOn = value.IsFlowOn
                Me.IsHeadUp = value.IsHeadUp
                Me.IsCyclingStopped = value.IsHeatOnlyMode
                Me.IsManualMode = value.IsManualMode
                Me.IsRampMode = value.IsRampMode
                Me.IsReady = value.IsReady
            End If
        End Set
    End Property

    ''' <summary> True if is ramp mode, false if not. </summary>
    Private _IsRampMode As Boolean

    ''' <summary> Gets or sets a value indicating whether the instrument is in ramp mode. </summary>
    ''' <value> <c>true</c> if in ramp mode; otherwise <c>false</c> </value>
    Public Property IsRampMode As Boolean
        Get
            Return Me._IsRampMode
        End Get
        Set(value As Boolean)
            If value <> Me.IsRampMode Then
                Me._IsRampMode = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is ready, false if not. </summary>
    Private _IsReady As Boolean

    ''' <summary>
    ''' Gets or sets a value indicating whether the instrument is ready for operation or in startup.
    ''' </summary>
    ''' <value>
    ''' <c>true</c> if instrument is ready for operation or in startup; otherwise <c>false</c>
    ''' </value>
    Public Property IsReady As Boolean
        Get
            Return Me._IsReady
        End Get
        Set(value As Boolean)
            If value <> Me.IsReady Then
                Me._IsReady = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is head up, false if not. </summary>
    Private _IsHeadUp As Boolean

    ''' <summary> Gets or sets a value indicating whether head is up. </summary>
    ''' <value> <c>true</c> if head is up; otherwise <c>false</c> </value>
    Public Property IsHeadUp As Boolean
        Get
            Return Me._IsHeadUp
        End Get
        Set(value As Boolean)
            If value <> Me.IsHeadUp Then
                Me._IsHeadUp = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is manual mode, false if not. </summary>
    Private _IsManualMode As Boolean

    ''' <summary> Gets or sets a value indicating whether manual mode is on. </summary>
    ''' <value> <c>true</c> if manual mode is on; otherwise <c>false</c> </value>
    Public Property IsManualMode As Boolean
        Get
            Return Me._IsManualMode
        End Get
        Set(value As Boolean)
            If value <> Me.IsManualMode Then
                Me._IsManualMode = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is heat only mode, false if not. </summary>
    Private _IsHeatOnlyMode As Boolean

    ''' <summary> Gets or sets a value indicating whether heat-only mode. </summary>
    ''' <value> <c>true</c> if this heat-only mode; otherwise <c>false</c> </value>
    Public Property IsHeatOnlyMode As Boolean
        Get
            Return Me._IsHeatOnlyMode
        End Get
        Set(value As Boolean)
            If value <> Me.IsHeatOnlyMode Then
                Me._IsHeatOnlyMode = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> True if is dut control, false if not. </summary>
    Private _IsDutControl As Boolean

    ''' <summary> Gets or sets a value indicating whether dut control is used. </summary>
    ''' <value> <c>true</c> if dut control is used; otherwise <c>false</c> </value>
    Public Property IsDutControl As Boolean
        Get
            Return Me._IsDutControl
        End Get
        Set(value As Boolean)
            If value <> Me.IsDutControl Then
                Me._IsDutControl = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True if is flow on, false if not. </summary>
    Private _IsFlowOn As Boolean

    ''' <summary> Gets or sets a value indicating whether flow is on. </summary>
    ''' <value> <c>true</c> if flow is on; otherwise <c>false</c> </value>
    Public Property IsFlowOn As Boolean
        Get
            Return Me._IsFlowOn
        End Get
        Set(value As Boolean)
            If value <> Me.IsFlowOn Then
                Me._IsFlowOn = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#End Region

End Class
