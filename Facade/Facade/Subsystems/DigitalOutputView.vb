'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\TriggerView.vb
'
' summary:	Trigger view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A Digital Output Subsystem user interface. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class DigitalOutputView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._DigitalOutputLine1View.LineIdentity = _StrobeLineIdentity
        Me._DigitalOutputLine1View.LineName = _StrobeLineIdentity
        Me._DigitalOutputLine1View.LineNumber = My.Settings.StrobeLineNumber
        Me._DigitalOutputLine1View.PulseWidth = My.Settings.StrobeDuration
        Me._DigitalOutputLine1View.ActiveLevel = DigitalActiveLevels.Low
        Me._DigitalOutputLine2View.LineIdentity = _BinLineIdentity
        Me._DigitalOutputLine2View.LineName = _BinLineIdentity
        Me._DigitalOutputLine2View.LineNumber = My.Settings.BinLineNumber
        Me._DigitalOutputLine2View.PulseWidth = My.Settings.BinDuration
        Me._DigitalOutputLine2View.ActiveLevel = DigitalActiveLevels.Low
        Me._DigitalOutputLine3View.LineIdentity = _ThirdLineIdentity
        Me._DigitalOutputLine3View.LineName = "Line"

    End Sub

    ''' <summary> Creates a new <see cref="TriggerView"/> </summary>
    ''' <returns> A <see cref="TriggerView"/>. </returns>
    Public Shared Function Create() As TriggerView
        Dim view As TriggerView = Nothing
        Try
            view = New TriggerView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Adds a menu item. </summary>
    ''' <param name="item"> The item. </param>
    Public Sub AddMenuItem(ByVal item As ToolStripMenuItem)
        Me._SubsystemSplitButton.DropDownItems.Add(item)
    End Sub

    ''' <summary> Applies the trigger plan settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} applying digital output 1 subsystem instrument settings"
            Me._DigitalOutputLine1View.ApplySettings()
            activity = $"{Me.Device.ResourceNameCaption} applying digital output 2 subsystem instrument settings"
            Me._DigitalOutputLine2View.ApplySettings()
            activity = $"{Me.Device.ResourceNameCaption} applying digital output 3 Subsystem instrument settings"
            Me._DigitalOutputLine3View.ApplySettings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads instrument settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading digital output 1 subsystem instrument settings"
            Me._DigitalOutputLine1View.ReadSettings()
            activity = $"{Me.Device.ResourceNameCaption} reading digital output 2 subsystem instrument settings"
            Me._DigitalOutputLine2View.ReadSettings()
            activity = $"{Me.Device.ResourceNameCaption} reading digital output 3 Subsystem instrument settings"
            Me._DigitalOutputLine3View.ReadSettings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the strobe line number. </summary>
    ''' <value> The strobe line number. </value>
    Public Property StrobeLineNumber As Integer
        Get
            Return Me._DigitalOutputLine1View.LineNumber
        End Get
        Set(value As Integer)
            Me._DigitalOutputLine1View.LineNumber = value
        End Set
    End Property

    ''' <summary> Gets or sets the strobe pulse width. </summary>
    ''' <value> The strobe pulse width. </value>
    Public Property StrobeDuration As TimeSpan
        Get
            Return Me._DigitalOutputLine1View.PulseWidth
        End Get
        Set(value As TimeSpan)
            Me._DigitalOutputLine1View.PulseWidth = value
        End Set
    End Property

    ''' <summary> Gets or sets the bin line number. </summary>
    ''' <value> The bin line number. </value>
    Public Property BinLineNumber As Integer
        Get
            Return Me._DigitalOutputLine1View.LineNumber
        End Get
        Set(value As Integer)
            Me._DigitalOutputLine1View.LineNumber = value
        End Set
    End Property

    ''' <summary> Gets or sets the bin pulse width. </summary>
    ''' <value> The bin pulse width. </value>
    Public Property BinDuration As TimeSpan
        Get
            Return Me._DigitalOutputLine1View.PulseWidth
        End Get
        Set(value As TimeSpan)
            Me._DigitalOutputLine1View.PulseWidth = value
        End Set
    End Property

    ''' <summary> Outputs a strobe pulse. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub Strobe()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me._DigitalOutputLine3View.DigitalOutputSubsystem.StartElapsedStopwatch()
            Me._DigitalOutputLine3View.DigitalOutputSubsystem.Strobe(Me.StrobeLineNumber, Me.StrobeDuration, Me.BinLineNumber, Me.BinDuration)
            Me._DigitalOutputLine3View.DigitalOutputSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        Me._DigitalOutputLine1View.AssignDevice(value)
        Me._DigitalOutputLine2View.AssignDevice(value)
        Me._DigitalOutputLine3View.AssignDevice(value)
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As DigitalOutputSubsystemBase)
        Dim lineNumber As Integer
        Dim pulseWidth As TimeSpan
        Dim activeLevel As DigitalActiveLevels
        Dim views As DigitalOutputLineView() = New DigitalOutputLineView() {Me._DigitalOutputLine1View, Me._DigitalOutputLine2View, Me._DigitalOutputLine3View}
        For Each digitalLineView As DigitalOutputLineView In views
            Select Case digitalLineView.LineIdentity
                Case _StrobeLineIdentity
                    lineNumber = My.Settings.StrobeLineNumber
                    pulseWidth = My.Settings.StrobeDuration
                    activeLevel = DigitalActiveLevels.Low
                Case _BinLineIdentity
                    lineNumber = My.Settings.BinLineNumber
                    pulseWidth = My.Settings.BinDuration
                    activeLevel = DigitalActiveLevels.Low
                Case _ThirdLineIdentity
                    lineNumber = 2
                    pulseWidth = TimeSpan.FromMilliseconds(10)
                    activeLevel = DigitalActiveLevels.Low
            End Select
            digitalLineView.BindSubsystem(subsystem, lineNumber)
            If subsystem Is Nothing Then
                RemoveHandler digitalLineView.PropertyChanged, AddressOf Me.DigitalOutputLineViewPropertyChanged
            Else
                AddHandler digitalLineView.PropertyChanged, AddressOf Me.DigitalOutputLineViewPropertyChanged
                digitalLineView.LineNumber = lineNumber
                digitalLineView.PulseWidth = pulseWidth
                digitalLineView.ActiveLevel = activeLevel
                Me.HandlePropertyChanged(digitalLineView, NameOf(isr.VI.Facade.DigitalOutputLineView.LineNumber))
            End If
        Next
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DIGITAL OUTPUT LINE VIEW "

    ''' <summary> The strobe line identity. </summary>
    Private Const _StrobeLineIdentity As String = "Strobe"

    ''' <summary> The bin line identity. </summary>
    Private Const _BinLineIdentity As String = "Bin"

    Private Const _ThirdLineIdentity As String = "3"

    ''' <summary> Handle the Digital Output Line view property changed event. </summary>
    ''' <remarks> David, 2020-11-13. </remarks>
    ''' <param name="view">         The view. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Protected Overridable Sub HandlePropertyChanged(ByVal view As VI.Facade.DigitalOutputLineView, ByVal propertyName As String)
        If view Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Facade.DigitalOutputLineView.LineNumber)
                Select Case view.LineIdentity
                    Case _StrobeLineIdentity
                        My.Settings.StrobeLineNumber = view.LineNumber
                    Case _BinLineIdentity
                        My.Settings.BinLineNumber = view.LineNumber
                    Case _ThirdLineIdentity
                End Select
            Case NameOf(VI.Facade.DigitalOutputLineView.PulseWidth)
                Select Case view.LineIdentity
                    Case _StrobeLineIdentity
                        My.Settings.StrobeDuration = view.PulseWidth
                    Case _BinLineIdentity
                        My.Settings.BinDuration = view.PulseWidth
                    Case _ThirdLineIdentity
                End Select
        End Select
    End Sub

    ''' <summary> Digital output line view property changed. </summary>
    ''' <remarks> David, 2020-11-13. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DigitalOutputLineViewPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim digitalLineView As DigitalOutputLineView = TryCast(sender, DigitalOutputLineView)
        Dim activity As String = $"handling {NameOf(isr.VI.Facade.DigitalOutputLineView)}.{e.PropertyName} change"
        Try
            If digitalLineView IsNot Nothing AndAlso e IsNot Nothing Then
            ElseIf Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DigitalOutputLineViewPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DigitalOutputLineViewPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(digitalLineView, e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Applies the settings tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Initiate click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InitiateButton_Click(sender As Object, e As EventArgs) Handles _StrobeButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.Strobe()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As isr.Core.ITraceMessageTalker)
        Me._DigitalOutputLine1View.AssignTalker(talker)
        Me._DigitalOutputLine2View.AssignTalker(talker)
        Me._DigitalOutputLine3View.AssignTalker(talker)
        ' assigned last as this identifies all talkers.
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
