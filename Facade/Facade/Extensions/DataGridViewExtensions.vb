'---------------------------------------------------------------------------------------------------
' file:		.\Extensions\DataGridViewExtensions.vb
'
' summary:	Data grid view extensions class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices
Imports System.Drawing

Namespace DataGridViewExtensions

    Public Module Methods

#Region " BUFFER READINGS "

        ''' <summary> Configure the display of <see cref="VI.BufferReading"/>. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The buffer readings. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Private Function ConfigureDisplay(ByVal grid As DataGridView, ByVal values As isr.VI.BufferReadingBindingList) As Integer
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            Dim wasEnabled As Boolean = grid.Enabled

            values.Synchronizer = grid
            grid.Enabled = False
            grid.AllowUserToAddRows = False
            grid.AutoGenerateColumns = False
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGreen
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
            grid.BorderStyle = BorderStyle.Fixed3D
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
            grid.EnableHeadersVisualStyles = True
            grid.MultiSelect = True
            grid.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised
            grid.ScrollBars = Windows.Forms.ScrollBars.Both

            grid.Columns.Clear()
            grid.DataSource = values

            Dim displayIndex As Integer = 0
            Dim column As New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(VI.BufferReading.Reading),
                .Name = NameOf(VI.BufferReading.Reading),
                .Visible = True,
                .DisplayIndex = displayIndex,
                .HeaderText = "Value"
            }

            grid.Columns.Add(column)

            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(VI.BufferReading.UnitReading),
                .Name = NameOf(VI.BufferReading.UnitReading),
                .Visible = True,
                .DisplayIndex = displayIndex,
                .HeaderText = "Unit"
            }
            grid.Columns.Add(column)

            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(VI.BufferReading.FractionalSecond),
                .Name = NameOf(VI.BufferReading.FractionalSecond),
                .Visible = True,
                .DisplayIndex = displayIndex,
                .HeaderText = "Time"
            }
            column.DefaultCellStyle.Format = "0.000"
            grid.Columns.Add(column)

            displayIndex += 1
            column = New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(VI.BufferReading.StatusReading),
                .Name = NameOf(VI.BufferReading.StatusReading),
                .Visible = True,
                .DisplayIndex = displayIndex,
                .HeaderText = "Status"
            }
            grid.Columns.Add(column)

            grid.Enabled = wasEnabled
            Return If(grid.Columns IsNot Nothing, grid.Columns.Count, 0)

        End Function

        ''' <summary>
        ''' Binds the <see cref="isr.VI.BufferReadingBindingList">readings</see> on the grid.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="values">      The buffer readings. </param>
        ''' <param name="reconfigure"> True to reconfigure. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function Bind(ByVal grid As DataGridView, ByVal values As isr.VI.BufferReadingBindingList, ByVal reconfigure As Boolean) As Integer
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            Dim wasEnabled As Boolean = grid.Enabled
            Try
                grid.Enabled = False
                ' 2017-02-24: had to configure each time otherwise, the grid would not display
                ' when called from the thread.
                ' 20190124: not sure if this limitations still applies when using a binding list.
                If grid.DataSource Is Nothing OrElse reconfigure Then
                    grid.ConfigureDisplay(values)
                    ' grid.DataSource = Me
                End If
                For Each c As DataGridViewColumn In grid.Columns
                    c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Next
                grid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells)
                grid.Invalidate()
                Return grid.Columns.Count
            Catch
                Throw
            Finally
                grid.Enabled = wasEnabled
            End Try
        End Function

#End Region

#Region " CHANNEL RESISTANCE "

        ''' <summary> Configure display of <see cref="ChannelResistorCollection"/> values. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel resistors. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function ConfigureDisplayValues(ByVal grid As DataGridView, ByVal values As ChannelResistorCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            grid.Enabled = False
            grid.DataSource = Nothing
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSalmon
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
            grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            grid.AutoGenerateColumns = False
            grid.RowHeadersVisible = False
            grid.ReadOnly = True
            grid.DataSource = values

            grid.Columns.Clear()
            grid.Refresh()
            Dim displayIndex As Integer = 0
            Dim width As Integer = 0
            Dim column As DataGridViewTextBoxColumn = Nothing
            Try
                displayIndex = 0
                column = New DataGridViewTextBoxColumn With {
                    .DataPropertyName = NameOf(ChannelResistor.Title),
                    .Name = NameOf(ChannelSourceMeasure.Title),
                    .Visible = True,
                    .Width = 50,
                    .DisplayIndex = displayIndex
                }
                grid.Columns.Add(column)
                width += column.Width
            Catch
                If column IsNot Nothing Then column.Dispose()
                Throw
            End Try

            Try
                displayIndex += 1
                column = New DataGridViewTextBoxColumn With {
                    .DataPropertyName = NameOf(ChannelResistor.Resistance),
                    .Name = NameOf(ChannelResistor.Resistance),
                    .Visible = True,
                    .DisplayIndex = displayIndex,
                    .Width = grid.Width - width - grid.Columns.Count
                }
                column.DefaultCellStyle.Format = "G5"
                grid.Columns.Add(column)
            Catch
                If column IsNot Nothing Then column.Dispose()
                Throw
            End Try

            grid.Enabled = True
            Return If(grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0, grid.Columns.Count, 0)

        End Function

        ''' <summary> Displays <see cref="ChannelResistorCollection"/> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel resistors. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function DisplayValues(ByVal grid As DataGridView, ByVal values As ChannelResistorCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            If grid.DataSource Is Nothing Then
                Methods.ConfigureDisplayValues(grid, values)
                Windows.Forms.Application.DoEvents()
            End If

            grid.DataSource = values.ToList
            Windows.Forms.Application.DoEvents()
            Return grid.Columns.Count

        End Function

        ''' <summary> Configure display of <see cref="ChannelResistorCollection"/> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel resistors. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function ConfigureDisplay(ByVal grid As DataGridView, ByVal values As ChannelResistorCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            grid.Enabled = False
            grid.DataSource = Nothing
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSalmon
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
            grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            grid.AutoGenerateColumns = True
            grid.RowHeadersVisible = False
            grid.ReadOnly = True
            grid.DataSource = values
            grid.Enabled = True
            Return If(grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0, grid.Columns.Count, 0)

        End Function

        ''' <summary> Displays <see cref="ChannelResistorCollection"/> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel resistors. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function Display(ByVal grid As DataGridView, ByVal values As ChannelResistorCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            If grid.DataSource Is Nothing Then
                Methods.ConfigureDisplay(grid, values)
                Windows.Forms.Application.DoEvents()
            End If

            grid.DataSource = values.ToList
            Windows.Forms.Application.DoEvents()
            Return grid.Columns.Count

        End Function


#End Region

#Region " CHANNEL SOURCE MEASURE "

        ''' <summary> Configure display of the <see cref="ChannelSourceMeasureCollection"/> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel source measures. </param>
        ''' <returns> the column count. </returns>
        <Extension>
        Public Function ConfigureDisplayValues(ByVal grid As DataGridView, ByVal values As ChannelSourceMeasureCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            grid.Enabled = False
            grid.DataSource = Nothing
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSalmon
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
            grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            grid.AutoGenerateColumns = False
            grid.RowHeadersVisible = False
            grid.ReadOnly = True
            grid.DataSource = values

            grid.Columns.Clear()
            grid.Refresh()
            Dim displayIndex As Integer = 0
            Dim width As Integer = 0
            Dim column As DataGridViewTextBoxColumn = Nothing

            Try
                displayIndex = 0
                column = New DataGridViewTextBoxColumn With {
                    .DataPropertyName = NameOf(ChannelSourceMeasure.Title),
                    .Name = NameOf(ChannelSourceMeasure.Title),
                    .Visible = True,
                    .Width = 50,
                    .DisplayIndex = displayIndex
                }
                grid.Columns.Add(column)
                width += column.Width
            Catch
                If column IsNot Nothing Then column.Dispose()
                Throw
            End Try

            column = Nothing
            Try
                displayIndex += 1
                column = New DataGridViewTextBoxColumn With {
                    .DataPropertyName = NameOf(ChannelSourceMeasure.Voltage),
                    .Name = "Volt",
                    .Visible = True,
                    .DisplayIndex = displayIndex,
                    .Width = 80
                }
                column.DefaultCellStyle.Format = "G5"
                grid.Columns.Add(column)
                width += column.Width
            Catch
                If column IsNot Nothing Then column.Dispose()
                Throw
            End Try

            column = Nothing
            Try
                displayIndex += 1
                column = New DataGridViewTextBoxColumn With {
                    .DataPropertyName = NameOf(ChannelSourceMeasure.Current),
                    .Name = "Ampere",
                    .Visible = True,
                    .DisplayIndex = displayIndex,
                    .Width = 80
                }
                column.DefaultCellStyle.Format = "G5"
                grid.Columns.Add(column)
                width += column.Width
            Catch
                If column IsNot Nothing Then column.Dispose()
                Throw
            End Try

            column = Nothing
            Try
                displayIndex += 1
                column = New DataGridViewTextBoxColumn With {
                    .DataPropertyName = NameOf(ChannelSourceMeasure.Resistance),
                    .Name = "Ohm",
                    .Visible = True,
                    .DisplayIndex = displayIndex,
                    .Width = grid.Width - width - grid.Columns.Count
                }
                column.DefaultCellStyle.Format = "G5"
                grid.Columns.Add(column)
            Catch
                If column IsNot Nothing Then column.Dispose()
                Throw
            End Try

            column = Nothing
            grid.Enabled = True
            Return If(grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0, grid.Columns.Count, 0)

        End Function

        ''' <summary> Displays the <see cref="ChannelSourceMeasureCollection"/> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel source measures. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function DisplayValues(ByVal grid As DataGridView, ByVal values As ChannelSourceMeasureCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            If grid.DataSource Is Nothing Then
                Methods.ConfigureDisplayValues(grid, values)
                Windows.Forms.Application.DoEvents()
            End If

            grid.DataSource = values.ToList
            Windows.Forms.Application.DoEvents()
            Return grid.Columns.Count


        End Function

        ''' <summary> Configure display of <see cref="ChannelSourceMeasureCollection"/> . </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel source measures. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function ConfigureDisplay(ByVal grid As DataGridView, ByVal values As ChannelSourceMeasureCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            grid.Enabled = False
            grid.DataSource = Nothing
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSalmon
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
            grid.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            grid.AutoGenerateColumns = True
            grid.RowHeadersVisible = False
            grid.ReadOnly = True
            grid.DataSource = values
            grid.Enabled = True
            Return If(grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0, grid.Columns.Count, 0)

        End Function

        ''' <summary>
        ''' Displays the <see cref="ChannelSourceMeasureCollection"/> using default configuration.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The channel source measures. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function Display(ByVal grid As DataGridView, ByVal values As ChannelSourceMeasureCollection) As Integer

            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

            Dim wasEnabled As Boolean = grid.Enabled
            grid.Enabled = False
            grid.Enabled = wasEnabled

            If grid.DataSource Is Nothing Then
                Methods.ConfigureDisplay(grid, values)
                Windows.Forms.Application.DoEvents()
            End If

            grid.DataSource = values.ToList
            Windows.Forms.Application.DoEvents()
            Return grid.Columns.Count


        End Function


#End Region

#Region " READING AMOUNTS "

        ''' <summary> Configure the display of <see cref="VI.ReadingAmounts"/>. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">   The grid. </param>
        ''' <param name="values"> The values. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function ConfigureDisplay(ByVal grid As DataGridView, ByVal values As IEnumerable(Of ReadingAmounts)) As Integer
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            Dim wasEnabled As Boolean = grid.Enabled

            grid.Enabled = False
            grid.AllowUserToAddRows = False
            grid.AutoGenerateColumns = False
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGreen
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
            grid.BorderStyle = BorderStyle.Fixed3D
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
            grid.EnableHeadersVisualStyles = True
            grid.MultiSelect = True
            grid.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised
            grid.ScrollBars = Windows.Forms.ScrollBars.Both

            grid.Columns.Clear()
            grid.DataSource = values

            Dim displayIndex As Integer = 0
            Dim column As New DataGridViewTextBoxColumn With {
                .DataPropertyName = NameOf(VI.ReadingAmounts.RawReading),
                .Name = NameOf(VI.ReadingAmounts.RawReading),
                .Visible = True,
                .DisplayIndex = displayIndex,
                .HeaderText = "Reading"
            }
            grid.Columns.Add(column)

            For Each c As Windows.Forms.DataGridViewColumn In grid.Columns
                c.AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
            Next

            grid.Enabled = wasEnabled
            Return If(grid.Columns IsNot Nothing, grid.Columns.Count, 0)

        End Function

        ''' <summary>
        ''' Displays <see cref="ChannelResistorCollection"/> updating the data source on each call.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="grid">        The grid. </param>
        ''' <param name="values">      The values. </param>
        ''' <param name="reconfigure"> True to reconfigure. </param>
        ''' <returns> The column count. </returns>
        <Extension>
        Public Function Display(ByVal grid As DataGridView, ByVal values As IEnumerable(Of ReadingAmounts), ByVal reconfigure As Boolean) As Integer
            If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))
            Dim wasEnabled As Boolean = grid.Enabled
            Try
                grid.Enabled = False
                ' 2017-02-24: had to configure each time otherwise, the grid would not display
                ' when called from the thread.
                ' 20190124: not sure if this limitations still applies when using a binding list.
                If grid.DataSource Is Nothing OrElse reconfigure Then
                    grid.ConfigureDisplay(values)
                    ' grid.DataSource = Me
                End If
                'For Each c As DataGridViewColumn In grid.Columns
                'c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                'Next
                grid.ScrollBars = Windows.Forms.ScrollBars.Both
                grid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells)
                grid.DataSource = values.ToList
                grid.Invalidate()
                Return grid.Columns.Count
            Catch
                Throw
            Finally
                grid.Enabled = wasEnabled
            End Try
        End Function

#End Region

    End Module

End Namespace
