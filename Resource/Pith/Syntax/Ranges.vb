﻿'---------------------------------------------------------------------------------------------------
' file:		Pith\Syntax\Ranges.vb
'
' summary:	Ranges class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.Constructs

''' <summary> A ranges. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Module Ranges

#Region " STANDARD RANGES "


    ''' <summary>
    ''' Gets a new instance of the default full range bound by the minimum decimal value and the
    ''' maximum decimal value minus 1E+14.
    ''' </summary>
    ''' <value> The full range. </value>
    Public ReadOnly Property FullRange() As RangeR
        Get
            ' round off of the decimal range cause an exception when converting back. 
            Return New RangeR(Decimal.MinValue, Decimal.MaxValue - 10000000000000.0)
        End Get
    End Property

    ''' <summary> Gets the non-negative range bound by maximum decimal value minus 1E+14. </summary>
    ''' <value> The non-negative full range. </value>
    Public ReadOnly Property NonnegativeFullRange() As RangeR
        Get
            Return New RangeR(0, Decimal.MaxValue - 10000000000000.0)
        End Get
    End Property

    ''' <summary> Gets the Standard aperture range. </summary>
    ''' <value> The Standard aperture range. </value>
    Public ReadOnly Property StandardApertureRange As RangeR
        Get
            Return New RangeR(0.0001, 1)
        End Get
    End Property

    ''' <summary> Gets the Standard filter window range. </summary>
    ''' <value> The Standard filter window range. </value>
    Public ReadOnly Property StandardFilterWindowRange As RangeR
        Get
            Return New RangeR(0.001, 0.1)
        End Get
    End Property

    ''' <summary> Gets the default filter count range. </summary>
    ''' <value> The default filter count range. </value>
    Public ReadOnly Property StandardFilterCountRange As RangeI
        Get
            Return New RangeI(0, 100)
        End Get
    End Property

    ''' <summary> Gets the Standard power line cycles range. </summary>
    ''' <value> The Standard power line cycles range. </value>
    Public ReadOnly Property StandardPowerLineCyclesRange As RangeR
        Get
            Const lineFrequency As Double = 50
            Return New RangeR(lineFrequency * Ranges.StandardApertureRange.Min, lineFrequency * Ranges.StandardApertureRange.Max)
        End Get
    End Property

#End Region

End Module
