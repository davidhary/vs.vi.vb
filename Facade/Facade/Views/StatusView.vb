'---------------------------------------------------------------------------------------------------
' file:		.\Views\StatusView.vb
'
' summary:	Status view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.Core.Models
Imports isr.Core
Imports isr.Core.Forma
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A control for handling status display and control. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2018-12-20, 6.0.6928.x. </para>
''' </remarks>
<Description("Status display control")>
<System.Drawing.ToolboxBitmap(GetType(StatusView), "StatusView"), ToolboxItem(True)>
Public Class StatusView
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me.ToolTip.IsBalloon = True
        Me._ReadTerminalsStateMenuItem.ToolTipText = My.Resources.TerminalsHint
        Me._UsingStatusSubsystemMenuItem.ToolTipText = My.Resources.StatusSubsystemOnlyHint
        Me._TraceMenuItem.ToolTipText = My.Resources.TraceHint
        Me._TraceShowLevelComboBox.ToolTipText = My.Resources.TraceHint
        Me._TraceLogLevelComboBox.ToolTipText = My.Resources.TraceHint
        Me._ToggleVisaEventHandlerMenuItem.ToolTipText = My.Resources.TraceHint
        Me._ServiceRequestAutoReadMenuItem.ToolTipText = My.Resources.ServiceRequestAutoReadHint
        Me._PollAutoReadMenuItem.ToolTipText = My.Resources.PollAutoReadHint
    End Sub

    ''' <summary> Creates a new <see cref="StatusView"/> </summary>
    ''' <returns> A <see cref="StatusView"/>. </returns>
    Public Shared Function Create() As StatusView
        Dim statusView As StatusView = Nothing
        Try
            statusView = New StatusView
            Return statusView
        Catch
            statusView?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                ' TO_DO: Doe we need this? Me.BindVisaSessionBase(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SESSION BASE: ASSIGNMENT "

    ''' <summary> Gets the session. </summary>
    ''' <value> The session. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SessionBase As isr.VI.Pith.SessionBase

    ''' <summary> Gets the sentinel indication having an open session. </summary>
    ''' <value> The is open. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.SessionBase IsNot Nothing AndAlso Me.SessionBase.IsSessionOpen
        End Get
    End Property

    ''' <summary> Binds the Session base to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session base. </param>
    Private Sub BindSessionBase(ByVal visaSessionBase As isr.VI.VisaSessionBase)
        If visaSessionBase Is Nothing Then
            Me.BindSessionBaseThis(Nothing)
        Else
            Me.BindSessionBaseThis(visaSessionBase.Session)
        End If
    End Sub

    ''' <summary> Bind session base. </summary>
    ''' <param name="sessionBase"> The session. </param>
    Private Sub BindSessionBaseThis(ByVal sessionBase As isr.VI.Pith.SessionBase)
        If Me.SessionBase IsNot Nothing Then
            Me.BindSessionViewModel(False, Me.SessionBase)
            Me._SessionBase = Nothing
        End If
        Me.BindStatusSubsystemBaseThis(Nothing)
        If sessionBase IsNot Nothing Then
            Me._SessionBase = sessionBase
            Me.BindSessionViewModel(True, Me.SessionBase)
        End If
    End Sub

    ''' <summary> Binds the Session view model to its controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The status subsystem view model. </param>
    Private Sub BindSessionViewModel(ByVal add As Boolean, ByVal viewModel As isr.VI.Pith.SessionBase)

        Dim binding As Binding = Me.AddRemoveBinding(Me._SessionDownDownButton, add, NameOf(Control.Enabled), viewModel,
                                                                    NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._ClearInterfaceMenuItem, add, NameOf(Control.Visible), viewModel,
                                                                    NameOf(isr.VI.Pith.SessionBase.SupportsClearInterface))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._SessionTimeoutTextBox, add, NameOf(Control.Text), viewModel,
                                                                    NameOf(isr.VI.Pith.SessionBase.TimeoutCandidate))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation
        binding.FormatString = "s\.fff"

        binding = Me.AddRemoveBinding(Me._ProgramServiceRequestEnableBitmaskMenuItem, add, NameOf(ToolStripMenuItem.Checked), viewModel,
                                                                    NameOf(isr.VI.Pith.SessionBase.ServiceRequestEventEnabled))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._ServiceRequestEnableCommandTextBox, add, NameOf(Control.Text), viewModel,
                                                                    NameOf(isr.VI.Pith.SessionBase.ServiceRequestEnableCommandFormat))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation

        binding = Me.AddRemoveBinding(Me._ServiceRequestBitMaskTextBox, add, NameOf(Control.Text), viewModel,
                                                                    NameOf(isr.VI.Pith.SessionBase.ServiceRequestEnabledBitmask))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation
        ' this format string is not accepted
        ' binding .FormatString = "0x{0:X2}"

        ' this works
        ' binding .FormatString = "X"

        ' this does not work
        ' binding .FormatString = "X2"

        ' this does not work
        ' binding .FormatInfo = New Pith.RegisterValueFormatProvider

        ' this works
        If add Then
            AddHandler binding.Format, BindingEventHandlers.DisplayRegisterEventHandler
        Else
            RemoveHandler binding.Format, BindingEventHandlers.DisplayRegisterEventHandler
        End If

        If add Then
            AddHandler binding.Parse, BindingEventHandlers.ParseStatusRegisterEventHandler
        Else
            RemoveHandler binding.Parse, BindingEventHandlers.ParseStatusRegisterEventHandler
        End If

        binding = Me.AddRemoveBinding(Me._ServiceRequestSplitButton, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        binding = Me.AddRemoveBinding(Me._PollSplitButton, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        ' termination
        binding = Me.AddRemoveBinding(Me._ReadTerminationEnabledMenuItem, add, NameOf(ToolStripMenuItem.Checked), viewModel, NameOf(isr.VI.Pith.SessionBase.ReadTerminationCharacterEnabled))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation

        binding = Me.AddRemoveBinding(Me._ReadTerminationTextBox, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.ReadTerminationCharacter))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation

        binding = Me.AddRemoveBinding(Me._WriteTerminationTextBox, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.TerminationSequence))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation

        ' notification
        Me._SessionNotificationLevelComboBox.Enabled = True
        If add Then
            Me._SessionNotificationLevelComboBox.ComboBox.DataSource = viewModel.NotifySyncLevels.ToList
            Me._SessionNotificationLevelComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            Me._SessionNotificationLevelComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            Me._SessionNotificationLevelComboBox.ComboBox.BindingContext = Me.BindingContext
            ' this is necessary because the combo box binding does not set the data source value on it item change event
            AddHandler Me._SessionNotificationLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.HandleNotificationLevelComboBoxValueChanged
        Else
            Me._SessionNotificationLevelComboBox.ComboBox.DataSource = Nothing
            RemoveHandler Me._SessionNotificationLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.HandleNotificationLevelComboBoxValueChanged
        End If

        Me.AddRemoveBinding(Me._SessionNotificationLevelComboBox, add, NameOf(ToolStripComboBox.SelectedItem), viewModel, NameOf(isr.VI.Pith.SessionBase.MessageNotificationEvent))
        If add Then
            Me._SessionNotificationLevelComboBox.ComboBox.SelectValue(viewModel.MessageNotificationLevel)
        End If

        Me.AddRemoveBinding(Me._ToggleVisaEventHandlerMenuItem, add, NameOf(CheckBox.Checked), viewModel, NameOf(Pith.SessionBase.ServiceRequestEventEnabled), DataSourceUpdateMode.Never)

    End Sub

    ''' <summary> Handles the notification level combo box value changed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleNotificationLevelComboBoxValueChanged(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing OrElse Me.SessionBase Is Nothing Then Return
        Dim combo As Windows.Forms.ToolStripComboBox = TryCast(sender, Windows.Forms.ToolStripComboBox) ' _SessionNotificationLevelComboBox
        Dim activity As String = String.Empty
        Try
            If combo IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"Setting session notification level to {combo.Text}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.MessageNotificationLevel = combo.SelectedEnumValue(isr.VI.Pith.NotifySyncLevel.None)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SESSION BASE: STATUS AND STANDARD REGISTERS "

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadStatusRegister()
        Dim activity As String = String.Empty
        Try
            If Me.IsSessionOpen Then
                activity = $"reading status byte after {Me.SessionBase.StatusReadDelay.TotalMilliseconds}ms delay" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.DelayReadStatusRegister()
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Session read status byte menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionReadStatusByteMenuItem_Click(sender As Object, e As EventArgs) Handles _SessionReadStatusByteMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} reading status byte" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.ReadStatusRegister()
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Session read standard event register menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionReadStandardEventRegisterMenuItem_Click(sender As Object, e As EventArgs) Handles _SessionReadStandardEventRegisterMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} reading status register" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.ReadStatusRegister()
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
                If Me.SessionBase.ErrorAvailable Then
                    activity = $"{Me.VisaSessionBase.ResourceNameCaption} error; not reading standard event registers" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                ElseIf Me.SessionBase.MessageAvailable Then
                    activity = $"{Me.VisaSessionBase.ResourceNameCaption} message to read; not reading standard event registers" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Else
                    activity = $"{Me.VisaSessionBase.ResourceNameCaption} reading standard event registers" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                    Me.VisaSessionBase.StartElapsedStopwatch()
                    Me.VisaSessionBase.Session.ReadStandardEventRegisters()
                    Me.VisaSessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
                End If
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SESSION BASE: TIMEOUT "

    ''' <summary> Applies and stores the timeout. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StoreTimeoutMenuItem_Click(sender As Object, e As EventArgs) Handles _StoreTimeoutMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} storing current and setting a new timeout" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.StoreCommunicationTimeout(Me.SessionBase.TimeoutCandidate)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Restores timeout menu item click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RestoreTimeoutMenuItem_Click(sender As Object, e As EventArgs) Handles _RestoreTimeoutMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} restoring previous timeout" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.RestoreCommunicationTimeout()
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SESSION BASE ACTIONS "

    ''' <summary> Sends the bus trigger click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SendBusTrigger_Click(sender As Object, e As EventArgs) Handles _SendBusTriggerMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} sending bus trigger" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.AssertTrigger()
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SESSION BASE: DEBUG TEST CODE "

#If DEBUG Then

    ''' <summary> Increments the given value. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A TraceEventType. </returns>
    Private Shared Function Increment(ByVal value As TraceEventType) As TraceEventType
        Select Case value
            Case TraceEventType.Critical
                value = TraceEventType.Error
            Case TraceEventType.Error
                value = TraceEventType.Warning
            Case TraceEventType.Warning
                value = TraceEventType.Information
            Case TraceEventType.Information
                value = TraceEventType.Verbose
            Case TraceEventType.Verbose
                value = TraceEventType.Critical
        End Select
        Windows.Forms.Application.DoEvents()
        Return value
    End Function

    ''' <summary>
    ''' Tool strip double click for testing the trace and notification event binding.
    ''' </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToolStrip_DoubleClick(sender As Object, e As EventArgs) Handles _ToolStrip.DoubleClick
        Me.VisaSessionBase.TraceShowLevel = StatusView.Increment(Me.VisaSessionBase.TraceShowLevel)
        Me.VisaSessionBase.TraceLogLevel = StatusView.Increment(Me.VisaSessionBase.TraceLogLevel)
        Select Case Me.SessionBase.MessageNotificationLevel
            Case isr.VI.Pith.NotifySyncLevel.None
                Me.SessionBase.MessageNotificationLevel = isr.VI.Pith.NotifySyncLevel.Sync
            Case isr.VI.Pith.NotifySyncLevel.Sync
                Me.SessionBase.MessageNotificationLevel = isr.VI.Pith.NotifySyncLevel.Async
            Case isr.VI.Pith.NotifySyncLevel.Async
                Me.SessionBase.MessageNotificationLevel = isr.VI.Pith.NotifySyncLevel.None
        End Select
    End Sub

#End If

#End Region

#Region " STATUS SUBSYSTEM BASE: ASSIGNMENT  "

    ''' <summary> Gets the status subsystem. </summary>
    ''' <value> The status subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property StatusSubsystemBase As StatusSubsystemBase

    ''' <summary> Bind status subsystem view model. </summary>
    ''' <param name="visaSessionBase"> The visa session view model. </param>
    Private Sub BindStatusSubsystemBase(ByVal visaSessionBase As VI.VisaSessionBase)
        If visaSessionBase Is Nothing Then
            Me.BindStatusSubsystemBaseThis(Nothing)
        Else
            Me.BindStatusSubsystemBaseThis(visaSessionBase.StatusSubsystemBase)
        End If
    End Sub

    ''' <summary> Binds the StatusSubsystem view model to its controls. </summary>
    ''' <param name="statusSubsystemBase"> The status subsystem view model. </param>
    Private Sub BindStatusSubsystemBaseThis(ByVal statusSubsystemBase As StatusSubsystemBase)
        If Me.StatusSubsystemBase IsNot Nothing Then
            Me._StatusSubsystemBase = Nothing
        End If
        Me._ClearErrorReportMenuItem.Enabled = statusSubsystemBase IsNot Nothing
        Me._ReadDeviceErrorsMenuItem.Enabled = statusSubsystemBase IsNot Nothing
        Me._StatusSubsystemBase = statusSubsystemBase
    End Sub

#End Region

#Region " STATUS SUBSYSTEM BASE: DEVICE ERRORS "

    ''' <summary> Clears the error report menu item click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearErrorReportMenuItem_Click(sender As Object, e As EventArgs) Handles _ClearErrorReportMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} clearing error report" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.StatusSubsystemBase.ClearErrorReport()
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads device errors menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadDeviceErrorsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadDeviceErrorsMenuItem.Click
        Dim activity As String = $"{Me.StatusSubsystemBase.ResourceNameCaption} reading device errors"
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If menuItem IsNot Nothing Then
                activity = $"{Me.StatusSubsystemBase.ResourceNameCaption} reading device errors" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                If Me.SessionBase.IsErrorBitSet Then
                    Dim r As (Success As Boolean, Details As String) = Me.StatusSubsystemBase.TryQueryExistingDeviceErrors()
                    If Not r.Success Then Me.PublishWarning($"Failed {activity}; { r.Details}")
                End If
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE (VISA SESSION BASE): ASSIGNMENT "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _VisaSessionBase As VisaSessionBase
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the visa session base. </summary>
    ''' <value> The visa session base. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase
        Get
            Return Me._VisaSessionBase
        End Get
    End Property

    ''' <summary> Binds the visa session to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session view model. </param>
    Public Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            'RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed
            Me.BindVisaSessionViewModel(False, Me.VisaSessionBase)
            Me.SessionSettings = Nothing
            Me.DeviceSettings = Nothing
            Me.UserInterfaceSettings = Nothing
            Me._VisaSessionBase = Nothing
        End If
        If visaSessionBase IsNot Nothing Then
            Me._VisaSessionBase = visaSessionBase
            'AddHandler visaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            AddHandler visaSessionBase.Closed, AddressOf Me.HandleDeviceClosed
            Me.SessionSettings = isr.VI.Pith.My.MySettings.Default
            Me.DeviceSettings = Nothing
            Me.UserInterfaceSettings = Nothing
            Me.BindVisaSessionViewModel(True, Me.VisaSessionBase)
        End If
        Me.BindSessionBase(visaSessionBase)
        Me.BindStatusSubsystemBase(visaSessionBase)
    End Sub

    ''' <summary> Handles the property changed. </summary>
    ''' <param name="session">      The session. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(session As VisaSessionBase, ByVal propertyName As String)
        If session Is Nothing OrElse propertyName Is Nothing Then Return
        Select Case propertyName
            Case NameOf(VisaSessionBase.SubsystemSupportMode)
                Me.UsingStatusSubsystemOnly = session.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
        End Select
    End Sub

    ''' <summary> Visa session base property changed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub VisaSessionBase_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _VisaSessionBase.PropertyChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing OrElse Me.SessionBase Is Nothing Then Return
        Dim activity As String = String.Empty
        Dim session As VisaSessionBase = TryCast(sender, VisaSessionBase)
        Try
            Me.HandlePropertyChanged(session, e?.PropertyName)
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> True to using status subsystem only. </summary>
    Private _UsingStatusSubsystemOnly As Boolean

    ''' <summary> Gets or sets the using status subsystem only. </summary>
    ''' <value> The using status subsystem only. </value>
    Public Property UsingStatusSubsystemOnly As Boolean
        Get
            Return Me._UsingStatusSubsystemOnly
        End Get
        Set(value As Boolean)
            If value <> Me.UsingStatusSubsystemOnly Then
                Me._UsingStatusSubsystemOnly = value
                Me.NotifyPropertyChanged()
                Me._VisaSessionBase.SubsystemSupportMode = If(value, SubsystemSupportMode.StatusOnly, SubsystemSupportMode.Full)
            End If
        End Set
    End Property

    ''' <summary> Binds the visa session view model to its controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The device view model. </param>
    Private Sub BindVisaSessionViewModel(ByVal add As Boolean, ByVal viewModel As VisaSessionBase)

        ' this could be enabled when the device opens
        Me.ReadTerminalsState = Nothing
        ' menus
        Me._DeviceDropDownButton.Enabled = viewModel.IsSessionOpen

        Dim binding As Binding = Me.AddRemoveBinding(Me._DeviceDropDownButton, add, NameOf(Control.Enabled), viewModel, NameOf(VI.VisaSessionBase.IsSessionOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        ' open options
        Me.AddRemoveBinding(Me._UsingStatusSubsystemMenuItem, add, NameOf(ToolStripMenuItem.Checked), Me, NameOf(StatusView.UsingStatusSubsystemOnly))

        ' trace show level
        binding = Me.AddRemoveBinding(Me._TraceShowLevelComboBox.ComboBox, add, NameOf(ComboBox.Enabled), viewModel, NameOf(isr.VI.VisaSessionBase.IsSessionOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        If add Then
            Me._TraceShowLevelComboBox.ComboBox.DataSource = ViewModelTalkerBase.TraceEvents.ToArray
            Me._TraceShowLevelComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            Me._TraceShowLevelComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            Me._TraceShowLevelComboBox.ComboBox.BindingContext = Me.BindingContext
            ' this is necessary because the combo box binding does not set the data source value on it item change event
            AddHandler Me._TraceShowLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.HandleTraceShowLevelComboBoxValueChanged
        Else
            Me._TraceShowLevelComboBox.ComboBox.DataSource = Nothing
            RemoveHandler Me._TraceShowLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.HandleTraceShowLevelComboBoxValueChanged
        End If

        Me.AddRemoveBinding(Me._TraceShowLevelComboBox, add, NameOf(ToolStripComboBox.SelectedItem), viewModel, NameOf(VI.VisaSessionBase.TraceShowEvent))
        ModelViewTalkerBase.SelectItem(Me._TraceShowLevelComboBox, viewModel.TraceShowLevel)

        ' trace log level
        binding = Me.AddRemoveBinding(Me._TraceLogLevelComboBox.ComboBox, add, NameOf(ComboBox.Enabled), viewModel, NameOf(VI.VisaSessionBase.IsSessionOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never

        If add Then
            Me._TraceLogLevelComboBox.ComboBox.DataSource = isr.Core.Models.ViewModelTalkerBase.TraceEvents.ToList
            Me._TraceLogLevelComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            Me._TraceLogLevelComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            Me._TraceLogLevelComboBox.ComboBox.BindingContext = Me.BindingContext
            ' this is necessary because the combo box binding does not set the data source value on it item change event
            AddHandler Me._TraceLogLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.HandleTraceLogLevelComboBoxValueChanged
        Else
            Me._TraceLogLevelComboBox.ComboBox.DataSource = Nothing
            RemoveHandler Me._TraceLogLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.HandleTraceLogLevelComboBoxValueChanged
        End If

        Me.AddRemoveBinding(Me._TraceLogLevelComboBox, add, NameOf(ToolStripComboBox.SelectedItem), viewModel, NameOf(VI.VisaSessionBase.TraceLogEvent))
        ModelViewTalkerBase.SelectItem(Me._TraceLogLevelComboBox, viewModel.TraceLogLevel)

        binding = Me.AddRemoveBinding(Me._UsingStatusSubsystemMenuItem, add, NameOf(ToolStripMenuItem.Enabled), viewModel, NameOf(isr.VI.VisaSessionBase.IsSessionOpen))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
        If add Then
            AddHandler binding.Format, BindingEventHandlers.InvertDisplayHandler
        Else
            RemoveHandler binding.Format, BindingEventHandlers.InvertDisplayHandler
        End If

        ' SRQ
        Me.AddRemoveBinding(Me._ServiceRequestHandlerAddRemoveMenuItem, add, NameOf(CheckBox.Checked), viewModel, NameOf(isr.VI.VisaSessionBase.ServiceRequestHandlerAssigned), DataSourceUpdateMode.Never)
        Me.AddRemoveBinding(Me._ServiceRequestAutoReadMenuItem, add, NameOf(ToolStripMenuItem.Checked), viewModel, NameOf(VisaSessionBase.ServiceRequestAutoRead))

        ' POLL
        binding = Me.AddRemoveBinding(Me._PollMessageStatusBitTextBox, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.VisaSessionBase.PollMessageAvailableBitmask))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation

        binding = Me.AddRemoveBinding(Me._PollIntervalTextBox, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.VisaSessionBase.PollTimespan))
        binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation
        binding.FormatString = "s\.fff"

        Me.AddRemoveBinding(Me._PollEnabledMenuItem, add, NameOf(ToolStripMenuItem.Checked), viewModel, NameOf(isr.VI.VisaSessionBase.PollEnabled), DataSourceUpdateMode.Never)
        Me.AddRemoveBinding(Me._PollAutoReadMenuItem, add, NameOf(ToolStripMenuItem.Checked), viewModel, NameOf(isr.VI.VisaSessionBase.PollAutoRead))

    End Sub

#Region " visa session base (device base) EVENT HANDLERS "

    ''' <summary> Handles the device Close. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device closed"
            ' TO_DO: Doe we need this? Me.BindStatusSubsystemBase(VisaSessionBase)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the device opened. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device opened"
            ' TO_DO: Doe we need this? Me.BindStatusSubsystemBase(VisaSessionBase)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#End Region

#Region " VISA SESSION EVENT HANDLERS: RESET "

    ''' <summary> Clears interface. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearInterfaceMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ClearInterfaceMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} clearing interface" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                Me.VisaSessionBase.ClearInterface()
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears device (SDC). </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearDeviceMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ClearDeviceMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} clearing device active state" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                Me.VisaSessionBase.ClearActiveState()
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears (CLS) the execution state menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearExecutionStateMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ClearExecutionStateMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} clearing execution state" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                Me.VisaSessionBase.ClearExecutionState()
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Resets (RST) the known state menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ResetKnownStateMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResetKnownStateMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} resetting known state" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                Me.VisaSessionBase.ResetKnownState()
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initializes to known state menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitKnownStateMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InitKnownStateMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} initializing known state" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                Me.VisaSessionBase.InitKnownState()
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE/SESSION: SERVICE REQUEST "

    ''' <summary> Program service request enable bitmask menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ProgramServiceRequestEnableBitmaskMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ProgramServiceRequestEnableBitmaskMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                Dim bitmask As Pith.ServiceRequests = If(Me.SessionBase.ServiceRequestEnabledBitmask, Me.SessionBase.DefaultServiceRequestEnableBitmask)
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} applying service request enable bitmask 0x{CInt(bitmask):x2}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.ApplyServiceRequestEnableBitmask(bitmask)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Toggle visa event handler menu item check state changed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ToggleVisaEventHandlerMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _ToggleVisaEventHandlerMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} turning {If(Me.SessionBase.ServiceRequestEventEnabled, "OFF", "ON")}  VISA event handling"
                Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                If Me.SessionBase.ServiceRequestEventEnabled Then
                    Me.SessionBase.DisableServiceRequestEventHandler()
                Else
                    Me.SessionBase.EnableServiceRequestEventHandler()
                End If
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Toggles the session service request handler . </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ServiceRequestHandlerAddRemoveMenuItem_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ServiceRequestHandlerAddRemoveMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} {If(Me.VisaSessionBase.ServiceRequestHandlerAssigned, "REMOVING", "ADDING")} VISA SRQ event handler"
                Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                If Me.VisaSessionBase.ServiceRequestHandlerAssigned Then
                    Me.VisaSessionBase.RemoveServiceRequestEventHandler()
                Else
                    Me.VisaSessionBase.AddServiceRequestEventHandler()
                End If
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " POLL "

    ''' <summary> Poll enabled menu item check state changed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PollEnabledMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _PollEnabledMenuItem.CheckStateChanged
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem IsNot Nothing Then
            menuItem.Text = If(menuItem.Checked, "Press to Stop", "Press to Start")
        End If
    End Sub

    ''' <summary> Poll enabled menu item check state changed. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PollEnabledMenuItem_Click(sender As Object, e As EventArgs) Handles _PollEnabledMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                If Me.VisaSessionBase.PollEnabled AndAlso Me.VisaSessionBase.ServiceRequestHandlerAssigned Then
                    Me.InfoProvider.Annunciate(sender, InfoProviderLevel.Info, "Service request handler must be removed before polling")
                Else
                    activity = $"{Me.VisaSessionBase.ResourceNameCaption} {If(Me.VisaSessionBase.PollEnabled, "STOPPING", "STARTING")} status byte polling" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                    Me.VisaSessionBase.StartElapsedStopwatch()
                    Me.VisaSessionBase.PollSynchronizingObject = Me
                    Me.VisaSessionBase.PollEnabled = Not Me.VisaSessionBase.PollEnabled
                    Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
                End If
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Poll send menu item click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PollSendMenuItem_Click(sender As Object, e As EventArgs) Handles _PollSendMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} sending {Me._PollWriteCommandTextBox.Text}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.WriteEscapedLine(Me._PollWriteCommandTextBox.Text)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE: TERMINALS "

    ''' <summary> State of the read terminals. </summary>
    Private _ReadTerminalsState As Action

    ''' <summary> Gets or sets the delegate for reading the terminals state. </summary>
    ''' <value> The delegate for reading the terminals state. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ReadTerminalsState As Action
        Get
            Return Me._ReadTerminalsState
        End Get
        Set(value As Action)
            Me._ReadTerminalsState = value
            Me._ReadTerminalsStateMenuItem.Enabled = value IsNot Nothing
        End Set
    End Property

    ''' <summary> Read terminals state menu item click. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadTerminalsStateMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadTerminalsStateMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            If menuItem IsNot Nothing AndAlso Not Me.ReadTerminalsState Is Nothing Then
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.VisaSessionBase.ResourceNameCaption} checking terminals state" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.VisaSessionBase.StartElapsedStopwatch()
                Me.ReadTerminalsState.DynamicInvoke()
                Me.VisaSessionBase.ElapsedTime = Me.VisaSessionBase.ReadElapsedTime(True)
            End If
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " APP MENU: SETTINGS of SESSION BASE, DEVICE, UI "

    ''' <summary> Shows the settings hint. </summary>
    Private Sub ShowSettingsHint()
        Dim toolTip As New ToolTip With {.IsBalloon = True, .ToolTipIcon = ToolTipIcon.Info, .ToolTipTitle = "Settings hint"}
        ' call twice per https://stackoverflow.com/questions/8716917/how-to-show-a-net-balloon-tooltip
        toolTip.Show(String.Empty, Me._ToolStrip)
        toolTip.Show("Opens the settings dialog", Me._ToolStrip)
    End Sub

    ''' <summary> The session settings. </summary>
    Private _SessionSettings As Configuration.ApplicationSettingsBase

    ''' <summary> Gets or sets the session settings. </summary>
    ''' <value> The session settings. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SessionSettings As Configuration.ApplicationSettingsBase
        Get
            Return Me._SessionSettings
        End Get
        Set(value As Configuration.ApplicationSettingsBase)
            Me._SessionSettings = value
            Me._SessionSettingsMenuItem.Enabled = value IsNot Nothing
        End Set
    End Property

    ''' <summary> Session settings menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SessionSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _SessionSettingsMenuItem.Click
        If Me.SessionSettings Is Nothing Then
            Me.SessionSettings = isr.VI.Pith.My.MySettings.Default
        End If
        If Me.SessionSettings IsNot Nothing Then
            isr.Core.WindowsForms.EditConfiguration("Session Settings", Me.SessionSettings)
        End If
    End Sub

    ''' <summary> The device settings. </summary>
    Private _DeviceSettings As Configuration.ApplicationSettingsBase

    ''' <summary> Gets or sets the Device settings. </summary>
    ''' <value> The Device settings. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DeviceSettings As Configuration.ApplicationSettingsBase
        Get
            Return Me._DeviceSettings
        End Get
        Set(value As Configuration.ApplicationSettingsBase)
            Me._DeviceSettings = value
            Me._DeviceSettingsMenuItem.Enabled = value IsNot Nothing
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Device tool strip menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _DeviceSettingsMenuItem.Click
        If Me.DeviceSettings IsNot Nothing Then
            isr.Core.WindowsForms.EditConfiguration("Device Settings", Me.DeviceSettings)
        End If
    End Sub

    ''' <summary> The user interface settings. </summary>
    Private _UserInterfaceSettings As Configuration.ApplicationSettingsBase

    ''' <summary> Gets or sets the UserInterface settings. </summary>
    ''' <value> The UserInterface settings. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property UserInterfaceSettings As Configuration.ApplicationSettingsBase
        Get
            Return Me._UserInterfaceSettings
        End Get
        Set(value As Configuration.ApplicationSettingsBase)
            Me._UserInterfaceSettings = value
            Me._UserInterfaceSettingsMenuItem.Enabled = value IsNot Nothing
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> UserInterface tool strip menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UserInterfaceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _UserInterfaceSettingsMenuItem.Click
        If Me.UserInterfaceSettings IsNot Nothing Then
            isr.Core.WindowsForms.EditConfiguration("User Interface Settings", Me.UserInterfaceSettings)
        End If
    End Sub

    ''' <summary> Base settings menu item click. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UIBaseSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _UIBaseSettingsMenuItem.Click
        If My.Settings IsNot Nothing Then
            isr.Core.WindowsForms.EditConfiguration("UI Base Settings", My.Settings)
        End If
    End Sub

    ''' <summary> Opens status subsystem menu item check state changed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenStatusSubsystemMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _UsingStatusSubsystemMenuItem.CheckStateChanged
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem IsNot Nothing Then
            menuItem.Text = If(menuItem.Checked, "Using status subsystem only", "Using all subsystems")
        End If
    End Sub

    ''' <summary> Edit resource names menu item click. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EditResourceNamesMenuItem_Click(sender As Object, e As EventArgs) Handles EditResourceNamesMenuItem.Click
        Using editorForm As ResourceNameInfoEditorForm = New ResourceNameInfoEditorForm
            editorForm.ShowDialog(Me)
        End Using
    End Sub

#End Region

#Region " APP MENU: TRACE LEVEL "

    ''' <summary>
    ''' Event handler. Called by _DisplayTraceLevelComboBox for selected value changed events.
    ''' </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleTraceShowLevelComboBoxValueChanged(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} selecting trace show level"
            Me.VisaSessionBase.ApplyTalkerTraceLevel(ListenerType.Display, CType(Me._TraceShowLevelComboBox.ComboBox.SelectedValue, TraceEventType))
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by _LogTraceLevelComboBox for selected value changed events.
    ''' </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleTraceLogLevelComboBoxValueChanged(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} selecting trace log level" : Me.PublishInfo($"{activity};. ")
            Me.VisaSessionBase.ApplyTalkerTraceLevel(Core.ListenerType.Logger,
                                                 ModelViewTalkerBase.SelectedValue(Me._TraceLogLevelComboBox, TraceEventType.Information))
        Catch ex As Exception
            Me.SessionBase.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

