<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BinningView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BinningView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PreformLimitTestMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadLimitTestStateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._PassBitPatternNumericButton = New System.Windows.Forms.ToolStripButton()
        Me._PassBitPatternNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._BinningStobeToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._LimitFailedButton = New System.Windows.Forms.ToolStripButton()
        Me._BinningStrobeDurationNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BinningStrobeDurationNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._BinningStrobeDurationNumericUnitLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Limit2View = New isr.VI.Facade.LimitView()
        Me._Limit1View = New isr.VI.Facade.LimitView()
        Me._ToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._DigitalOutputActiveHighMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SubsystemToolStrip.SuspendLayout()
        Me._ToolStripPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.AutoSize = False
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._PassBitPatternNumericButton, Me._PassBitPatternNumeric, Me._BinningStobeToggleButton, Me._LimitFailedButton, Me._BinningStrobeDurationNumericLabel, Me._BinningStrobeDurationNumeric, Me._BinningStrobeDurationNumericUnitLabel})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(403, 28)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 0
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem, Me._PreformLimitTestMenuItem, Me._ReadLimitTestStateMenuItem, Me._DigitalOutputActiveHighMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(42, 25)
        Me._SubsystemSplitButton.Text = "Bin"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(215, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies the limit settings to the instrument"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(215, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Read current settings from the instrument"
        '
        '_PreformLimitTestMenuItem
        '
        Me._PreformLimitTestMenuItem.Name = "_PreformLimitTestMenuItem"
        Me._PreformLimitTestMenuItem.Size = New System.Drawing.Size(215, 22)
        Me._PreformLimitTestMenuItem.Text = "Preform Limit Test"
        Me._PreformLimitTestMenuItem.ToolTipText = "Performs the limit test"
        '
        '_ReadLimitTestStateMenuItem
        '
        Me._ReadLimitTestStateMenuItem.Name = "_ReadLimitTestStateMenuItem"
        Me._ReadLimitTestStateMenuItem.Size = New System.Drawing.Size(215, 22)
        Me._ReadLimitTestStateMenuItem.Text = "Read Limit Test State"
        Me._ReadLimitTestStateMenuItem.ToolTipText = "Performs and reads limit test results"
        '
        '_PassBitPatternNumericButton
        '
        Me._PassBitPatternNumericButton.CheckOnClick = True
        Me._PassBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._PassBitPatternNumericButton.Image = CType(resources.GetObject("_PassBitPatternNumericButton.Image"), System.Drawing.Image)
        Me._PassBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PassBitPatternNumericButton.Name = "_PassBitPatternNumericButton"
        Me._PassBitPatternNumericButton.Size = New System.Drawing.Size(52, 25)
        Me._PassBitPatternNumericButton.Text = "Pass: 0x"
        '
        '_PassBitPatternNumeric
        '
        Me._PassBitPatternNumeric.Name = "_PassBitPatternNumeric"
        Me._PassBitPatternNumeric.Size = New System.Drawing.Size(41, 25)
        Me._PassBitPatternNumeric.Text = "2"
        Me._PassBitPatternNumeric.ToolTipText = "Pass bit pattern"
        Me._PassBitPatternNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_BinningStobeToggleButton
        '
        Me._BinningStobeToggleButton.Checked = True
        Me._BinningStobeToggleButton.CheckOnClick = True
        Me._BinningStobeToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._BinningStobeToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._BinningStobeToggleButton.Image = CType(resources.GetObject("_BinningStobeToggleButton.Image"), System.Drawing.Image)
        Me._BinningStobeToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._BinningStobeToggleButton.Name = "_BinningStobeToggleButton"
        Me._BinningStobeToggleButton.Size = New System.Drawing.Size(52, 25)
        Me._BinningStobeToggleButton.Text = "Stobe: ?"
        Me._BinningStobeToggleButton.ToolTipText = "Toggle to enable or disable binning strobe"
        '
        '_LimitFailedButton
        '
        Me._LimitFailedButton.Checked = True
        Me._LimitFailedButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._LimitFailedButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._LimitFailedButton.Image = CType(resources.GetObject("_LimitFailedButton.Image"), System.Drawing.Image)
        Me._LimitFailedButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LimitFailedButton.Name = "_LimitFailedButton"
        Me._LimitFailedButton.Size = New System.Drawing.Size(47, 25)
        Me._LimitFailedButton.Text = "Failed?"
        Me._LimitFailedButton.ToolTipText = "Displays limit state"
        '
        '_BinningStrobeDurationNumericLabel
        '
        Me._BinningStrobeDurationNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._BinningStrobeDurationNumericLabel.Name = "_BinningStrobeDurationNumericLabel"
        Me._BinningStrobeDurationNumericLabel.Size = New System.Drawing.Size(56, 25)
        Me._BinningStrobeDurationNumericLabel.Text = "Duration:"
        '
        '_BinningStrobeDurationNumeric
        '
        Me._BinningStrobeDurationNumeric.Name = "_BinningStrobeDurationNumeric"
        Me._BinningStrobeDurationNumeric.Size = New System.Drawing.Size(41, 25)
        Me._BinningStrobeDurationNumeric.Text = "0"
        Me._BinningStrobeDurationNumeric.ToolTipText = "Duration of binning strobe to use for timing the trigger plan"
        Me._BinningStrobeDurationNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_BinningStrobeDurationNumericUnitLabel
        '
        Me._BinningStrobeDurationNumericUnitLabel.Name = "_BinningStrobeDurationNumericUnitLabel"
        Me._BinningStrobeDurationNumericUnitLabel.Size = New System.Drawing.Size(23, 25)
        Me._BinningStrobeDurationNumericUnitLabel.Text = "ms"
        '
        '_Limit2View
        '
        Me._Limit2View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._Limit2View.Dock = System.Windows.Forms.DockStyle.Top
        Me._Limit2View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Limit2View.Location = New System.Drawing.Point(1, 111)
        Me._Limit2View.Name = "_Limit2View"
        Me._Limit2View.Padding = New System.Windows.Forms.Padding(1)
        Me._Limit2View.Size = New System.Drawing.Size(403, 82)
        Me._Limit2View.TabIndex = 3
        '
        '_Limit1View
        '
        Me._Limit1View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._Limit1View.Dock = System.Windows.Forms.DockStyle.Top
        Me._Limit1View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Limit1View.Location = New System.Drawing.Point(1, 29)
        Me._Limit1View.Name = "_Limit1View"
        Me._Limit1View.Padding = New System.Windows.Forms.Padding(1)
        Me._Limit1View.Size = New System.Drawing.Size(403, 82)
        Me._Limit1View.TabIndex = 1
        '
        '_ToolStripPanel
        '
        Me._ToolStripPanel.BackColor = System.Drawing.Color.Transparent
        Me._ToolStripPanel.Controls.Add(Me._SubsystemToolStrip)
        Me._ToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ToolStripPanel.Location = New System.Drawing.Point(1, 1)
        Me._ToolStripPanel.Name = "_ToolStripPanel"
        Me._ToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._ToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me._ToolStripPanel.Size = New System.Drawing.Size(403, 28)
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InfoTextBox.Location = New System.Drawing.Point(1, 193)
        Me._InfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ReadOnly = True
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InfoTextBox.Size = New System.Drawing.Size(403, 147)
        Me._InfoTextBox.TabIndex = 26
        Me._InfoTextBox.Text = "<info>"
        '
        '_DigitalOutputActiveHighMenuItem
        '
        Me._DigitalOutputActiveHighMenuItem.CheckOnClick = True
        Me._DigitalOutputActiveHighMenuItem.Name = "_DigitalOutputActiveHighMenuItem"
        Me._DigitalOutputActiveHighMenuItem.Size = New System.Drawing.Size(215, 22)
        Me._DigitalOutputActiveHighMenuItem.Text = "Digital Output Active High"
        Me._DigitalOutputActiveHighMenuItem.ToolTipText = "Toggles binning digital output polarity"
        '
        'BinningView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._InfoTextBox)
        Me.Controls.Add(Me._Limit2View)
        Me.Controls.Add(Me._Limit1View)
        Me.Controls.Add(Me._ToolStripPanel)
        Me.Name = "BinningView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(405, 341)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me._ToolStripPanel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _PassBitPatternNumericButton As Windows.Forms.ToolStripButton
    Private WithEvents _PassBitPatternNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _Limit2View As LimitView
    Private WithEvents _BinningStobeToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _LimitFailedButton As Windows.Forms.ToolStripButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadLimitTestStateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _Limit1View As LimitView
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _BinningStrobeDurationNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _BinningStrobeDurationNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _BinningStrobeDurationNumericUnitLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ToolStripPanel As Windows.Forms.ToolStripPanel
    Private WithEvents _PreformLimitTestMenuItem As ToolStripMenuItem
    Private WithEvents _InfoTextBox As TextBox
    Private WithEvents _DigitalOutputActiveHighMenuItem As ToolStripMenuItem
End Class
