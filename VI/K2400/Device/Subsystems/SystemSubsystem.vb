''' <summary> Defines a SCPI System Subsystem such as the Keithley 2400. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class SystemSubsystem
    Inherits SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks>
    ''' Additional Actions: <para>
    ''' Clears Error Queue.
    ''' </para>
    ''' </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Dim activity As String = String.Empty
        Try
            activity = "Reading options" : Me.PublishVerbose($"{activity};. ")
            Me.QueryOptions()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.FourWireSenseEnabled = False
        Me.AutoZeroEnabled = True
        Me.ContactCheckEnabled = False
        Me.ContactCheckResistance = 50
    End Sub

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Gets or sets the Auto Zero enabled query command. </summary>
    ''' <value> The Auto Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = ":SYST:AZER?"

    ''' <summary> Gets or sets the Auto Zero enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:AZERO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Auto Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = ":SYST:AZER {0:'ON';'ON';'OFF'}"

#End Region

#Region " CONTACT CHECK ENABLED "

    ''' <summary> Gets or sets the Contact Check enabled query command. </summary>
    ''' <value> The Contact Check enabled query command. </value>
    Protected Overrides Property ContactCheckEnabledQueryCommand As String = ":SYST:CCH?"

    ''' <summary> Gets or sets the Contact Check enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:CCH {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Contact Check enabled query command. </value>
    Protected Overrides Property ContactCheckEnabledCommandFormat As String = ":SYST:CCH {0:'ON';'ON';'OFF'}"

#End Region

#Region " CONTACT CHECK RESISTANCE "

    ''' <summary> Gets or sets the ContactCheck Resistance query command. </summary>
    ''' <value> the ContactCheck Resistance query command. </value>
    Protected Overrides Property ContactCheckResistanceQueryCommand As String = "SYST:CCH:RES?"

    ''' <summary> Gets or sets the ContactCheck Resistance command format. </summary>
    ''' <value> the ContactCheck Resistance command format. </value>
    Protected Overrides Property ContactCheckResistanceCommandFormat As String = "SYST:CCH:RES {0}"

#End Region

#Region " CONTACT CHECK SUPPORTED "

    ''' <summary> Gets or sets the contact check option value. </summary>
    ''' <value> The contact check option value. </value>
    Protected Overrides Property ContactCheckOptionValue As String = "CONTACT-CHECK"

#End Region

#Region " FOUR WIRE SENSE ENABLED "

    ''' <summary> Gets or sets the Four Wire Sense enabled query command. </summary>
    ''' <value> The Four Wire Sense enabled query command. </value>
    Protected Overrides Property FourWireSenseEnabledQueryCommand As String = ":SYST:RSEN?"

    ''' <summary> Gets or sets the Four Wire Sense enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:RSEN {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Four Wire Sense enabled query command. </value>
    Protected Overrides Property FourWireSenseEnabledCommandFormat As String = ":SYST:RSEN {0:'ON';'ON';'OFF'}"

#End Region

#Region " OPTIONS "

    ''' <summary> Gets or sets the option query command. </summary>
    ''' <value> The option query command. </value>
    Protected Overrides Property OptionQueryCommand As String = "*OPT?"

#End Region

End Class
