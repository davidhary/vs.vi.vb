
Partial Public Class DigitalOutputSubsystemBase

#Region " OUTPUT MODE "

    ''' <summary> Define output mode read writes. </summary>
    Private Sub DefineOutputModeReadWrites()
        Me._OutputModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As OutputModes In [Enum].GetValues(GetType(OutputModes))
            Me._OutputModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Output mode parses. </summary>
    ''' <value> A Dictionary of Output mode parses. </value>
    Public ReadOnly Property OutputModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported output modes. </summary>
    Private _SupportedOutputModes As OutputModes

    ''' <summary>
    ''' Gets or sets the supported Function Mode. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported Output modes. </value>
    Public Property SupportedOutputModes() As OutputModes
        Get
            Return Me._SupportedOutputModes
        End Get
        Set(ByVal value As OutputModes)
            If Not Me.SupportedOutputModes.Equals(value) Then
                Me._SupportedOutputModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The output mode. </summary>
    Private _OutputMode As OutputModes?

    ''' <summary> Gets or sets the cached Output mode. </summary>
    ''' <value> The <see cref="OutputMode">Output mode</see> or none if not set or unknown. </value>
    Public Overloads Property OutputMode As OutputModes?
        Get
            Return Me._OutputMode
        End Get
        Protected Set(ByVal value As OutputModes?)
            If Not Me.OutputMode.Equals(value) Then
                Me._OutputMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Output mode. </summary>
    ''' <param name="value"> The  Output mode. </param>
    ''' <returns> The <see cref="OutputMode">source Output mode</see> or none if unknown. </returns>
    Public Function ApplyOutputMode(ByVal value As OutputModes) As OutputModes?
        Me.WriteOutputMode(value)
        Return Me.QueryOutputMode()
    End Function

    ''' <summary> Gets or sets the Output mode query command. </summary>
    ''' <value> The Output mode query command. </value>
    Protected Overridable Property OutputModeQueryCommand As String

    ''' <summary> Queries the Output mode. </summary>
    ''' <returns> The <see cref="OutputMode">Output mode</see> or none if unknown. </returns>
    Public Function QueryOutputMode() As OutputModes?
        Me.OutputMode = Me.Query(Of OutputModes)(Me.OutputModeQueryCommand,
                                                                         Me.OutputMode.GetValueOrDefault(OutputModes.None),
                                                                         Me.OutputModeReadWrites)
        Return Me.OutputMode
    End Function

    ''' <summary> Gets or sets the Output mode command format. </summary>
    ''' <value> The Output mode command format. </value>
    Protected Overridable Property OutputModeCommandFormat As String

    ''' <summary> Writes the Output mode without reading back the value from the device. </summary>
    ''' <param name="value"> The Output mode. </param>
    ''' <returns> The <see cref="OutputMode">Output mode</see> or none if unknown. </returns>
    Public Function WriteOutputMode(ByVal value As OutputModes) As OutputModes?
        Me.OutputMode = Me.Write(Of OutputModes)(Me.OutputModeCommandFormat, value, Me.OutputModeReadWrites)
        Return Me.OutputMode
    End Function

#End Region

#Region " DIGITAL OUTPUT ACTIVE LEVEL "

    ''' <summary> Define the active level read writes. </summary>
    Private Sub DefineDigitalActiveLevelReadWrites()
        Me._DigitalActiveLevelReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As DigitalActiveLevels In [Enum].GetValues(GetType(DigitalActiveLevels))
            Me._DigitalActiveLevelReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of digital active level parses. </summary>
    ''' <value> A Dictionary of digital active level parses. </value>
    Public ReadOnly Property DigitalActiveLevelReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported digital active levels. </summary>
    Private _SupportedDigitalActiveLevels As DigitalActiveLevels

    ''' <summary>
    ''' Gets or sets the supported digital active levels. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported digital active levels. </value>
    Public Property SupportedDigitalActiveLevels() As DigitalActiveLevels
        Get
            Return Me._SupportedDigitalActiveLevels
        End Get
        Set(ByVal value As DigitalActiveLevels)
            If Not Me.SupportedDigitalActiveLevels.Equals(value) Then
                Me._SupportedDigitalActiveLevels = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The current digital active level. </summary>
    Private _CurrentDigitalActiveLevel As DigitalActiveLevels?

    ''' <summary> Gets or sets the current digital active level common to all digital lines. </summary>
    ''' <value>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if not set or
    ''' unknown.
    ''' </value>
    Public Overloads Property CurrentDigitalActiveLevel As DigitalActiveLevels?
        Get
            Return Me._CurrentDigitalActiveLevel
        End Get
        Protected Set(ByVal value As DigitalActiveLevels?)
            If Not Me.CurrentDigitalActiveLevel.Equals(value) Then
                Me._CurrentDigitalActiveLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the current digital active level. </summary>
    ''' <param name="value"> The digital active level. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">source digital active level</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalActiveLevel(ByVal value As DigitalActiveLevels) As DigitalActiveLevels?
        Me.WriteDigitalActiveLevel(value)
        Return Me.QueryDigitalActiveLevel()
    End Function

    ''' <summary> Gets or sets the current digital active level query command. </summary>
    ''' <value> The digital active level query command. </value>
    Protected Overridable Property DigitalActiveLevelQueryCommand As String

    ''' <summary> Queries the current digital active level. </summary>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if unknown.
    ''' </returns>
    Public Function QueryDigitalActiveLevel() As DigitalActiveLevels?
        Me.CurrentDigitalActiveLevel = Me.Query(Of DigitalActiveLevels)(Me.DigitalActiveLevelQueryCommand,
                                                                      Me.CurrentDigitalActiveLevel.GetValueOrDefault(DigitalActiveLevels.None),
                                                                      Me.DigitalActiveLevelReadWrites)
        Return Me.CurrentDigitalActiveLevel
    End Function

    ''' <summary> Gets or sets the current digital active level command format. </summary>
    ''' <value> The digital active level command format. </value>
    Protected Overridable Property DigitalActiveLevelCommandFormat As String

    ''' <summary>
    ''' Writes the current digital active level without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The digital active level. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if unknown.
    ''' </returns>
    Public Function WriteDigitalActiveLevel(ByVal value As DigitalActiveLevels) As DigitalActiveLevels?
        Me.CurrentDigitalActiveLevel = Me.Write(Of DigitalActiveLevels)(Me.DigitalActiveLevelCommandFormat, value, Me.DigitalActiveLevelReadWrites)
        Return Me.CurrentDigitalActiveLevel
    End Function

    ''' <summary> Writes and reads back the current digital active level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The  digital active level. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">source digital active level</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalActiveLevel(ByVal lineNumber As Integer, ByVal value As DigitalActiveLevels) As DigitalActiveLevels?
        Me.WriteDigitalActiveLevel(lineNumber, value)
        Return Me.QueryDigitalActiveLevel(lineNumber)
    End Function

    ''' <summary> Queries the current digital active level. Also updates the <see cref="CommonDigitalActiveLevel"/>. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if unknown.
    ''' </returns>
    Public Function QueryDigitalActiveLevel(ByVal lineNumber As Integer) As DigitalActiveLevels?
        Me.CurrentDigitalActiveLevel = Me.Query(Of DigitalActiveLevels)(String.Format(Me.DigitalActiveLevelQueryCommand, lineNumber),
                                                                      Me.CurrentDigitalActiveLevel.GetValueOrDefault(DigitalActiveLevels.None),
                                                                      Me.DigitalActiveLevelReadWrites)
        If Me.CurrentDigitalActiveLevel.HasValue Then
            Me.DigitalOutputLines.Update(lineNumber, Me.CurrentDigitalActiveLevel.Value)
            Me.CommonDigitalActiveLevel = Me.DigitalOutputLines.DigitalActiveLevel
        Else
            Me.DigitalOutputLines.Update(lineNumber, DigitalActiveLevels.None)
        End If
        Me.NotifyPropertyChanged(NameOf(DigitalOutputSubsystemBase.DigitalOutputLines))
        Return Me.CurrentDigitalActiveLevel
    End Function

    ''' <summary>
    ''' Writes the digital active level without reading back the value from the device. Also updates the <see cref="CommonDigitalActiveLevel"/> />
    ''' </summary>
    ''' <param name="lineNumber"> The output line number. </param>
    ''' <param name="value">            The digital active level. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if unknown.
    ''' </returns>
    Public Function WriteDigitalActiveLevel(ByVal lineNumber As Integer, ByVal value As DigitalActiveLevels) As DigitalActiveLevels?
        Me.CurrentDigitalActiveLevel = Me.Write(Of DigitalActiveLevels)(String.Format(Me.DigitalActiveLevelCommandFormat, lineNumber),
                                                                                      value, Me.DigitalActiveLevelReadWrites)
        If Me.CurrentDigitalActiveLevel.HasValue Then
            Me.DigitalOutputLines.Update(lineNumber, Me.CurrentDigitalActiveLevel.Value)
            Me.CommonDigitalActiveLevel = Me.DigitalOutputLines.DigitalActiveLevel
        Else
            Me.DigitalOutputLines.Update(lineNumber, DigitalActiveLevels.None)
        End If
        Me.NotifyPropertyChanged(NameOf(DigitalOutputSubsystemBase.DigitalOutputLines))
        Return Me.CurrentDigitalActiveLevel
    End Function

    ''' <summary> The common digital active level. </summary>
    Private _CommonDigitalActiveLevel As DigitalActiveLevels?

    ''' <summary> Gets or sets the cached digital active level of all lines. </summary>
    ''' <value>
    ''' The <see cref="CommonDigitalActiveLevel">digital active level</see> or none if not set or not
    ''' the same for all lines.
    ''' </value>
    Public Overloads Property CommonDigitalActiveLevel As DigitalActiveLevels?
        Get
            Return Me._CommonDigitalActiveLevel
        End Get
        Protected Set(ByVal value As DigitalActiveLevels?)
            If Not Me.CommonDigitalActiveLevel.Equals(value) Then
                Me._CommonDigitalActiveLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the digital output lines. </summary>
    ''' <value> The digital output lines. </value>
    Public ReadOnly Property DigitalOutputLines As DigitalOutputLineCollection

    ''' <summary> Applies the output level. </summary>
    ''' <param name="lineNumbers"> The output lines. </param>
    ''' <param name="value">       The  digital active level. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">source digital active level</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalActiveLevel(ByVal lineNumbers As IEnumerable(Of Integer), ByVal value As DigitalActiveLevels) As DigitalActiveLevels?
        For Each lineNumber As Integer In lineNumbers
            Me.ApplyDigitalActiveLevel(lineNumber, value)
        Next
        Return Me.CommonDigitalActiveLevel
    End Function

    ''' <summary> Writes and reads back the output levels. </summary>
    ''' <param name="outputLines"> The output lines. </param>
    ''' <param name="values">      The values. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">source digital active level</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalActiveLevels(ByVal outputLines As IEnumerable(Of Integer), ByVal values As IEnumerable(Of DigitalActiveLevels)) As IEnumerable(Of DigitalActiveLevels?)
        Dim result As New List(Of DigitalActiveLevels?)
        For i As Integer = 0 To outputLines.Count - 1
            result.Add(Me.ApplyDigitalActiveLevel(outputLines(i), values(i)))
        Next
        Return result
    End Function

    ''' <summary> Queries the digital active levels. </summary>
    ''' <param name="outputLines"> The output lines. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if unknown.
    ''' </returns>
    Public Function QueryDigitalActiveLevels(ByVal outputLines As IEnumerable(Of Integer)) As DigitalActiveLevels?
        If Not String.IsNullOrEmpty(Me.DigitalActiveLevelQueryCommand) Then
            For Each lineNumber As Integer In outputLines
                Me.QueryDigitalActiveLevel(lineNumber)
            Next
        End If
        Return Me.CommonDigitalActiveLevel
    End Function

    ''' <summary> Queries the polarities and levels of all signal lines. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    Public Sub QueryDigitalOutputLines()
        Me.QueryDigitalActiveLevels(Me.DigitalOutputLines.DigitalOutputLineNumbers)
        For Each lineNumber As Integer In Me.DigitalOutputLines.DigitalOutputLineNumbers
            Me.QueryLineLevel(lineNumber)
        Next
    End Sub

    ''' <summary>
    ''' Writes the digital active level without reading back the value from the device.
    ''' </summary>
    ''' <param name="outputLines"> The output lines. </param>
    ''' <param name="value">       The digital active level. </param>
    ''' <returns>
    ''' The <see cref="CurrentDigitalActiveLevel">digital active level</see> or none if unknown.
    ''' </returns>
    Public Function WriteDigitalActiveLevel(ByVal outputLines As IEnumerable(Of Integer), ByVal value As DigitalActiveLevels) As DigitalActiveLevels?
        If Not String.IsNullOrEmpty(Me.DigitalActiveLevelQueryCommand) Then
            For Each lineNumber As Integer In outputLines
                Me.WriteDigitalActiveLevel(lineNumber, value)
            Next
        End If
        Return Me.CommonDigitalActiveLevel
    End Function

#End Region


End Class

''' <summary> A digital output line. </summary>
Public Class DigitalOutputLine

    ''' <summary> Gets the line number. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The line number. </value>
    Public Property LineNumber As Integer

    ''' <summary> Gets the digital active level. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The digital active. </value>
    Public Property ActiveLevel As DigitalActiveLevels

    ''' <summary> Gets the line level. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The line level. </value>
    Public Property LineLevel As Integer?
End Class

''' <summary> Collection of digital output lines. </summary>
Public Class DigitalOutputLineCollection
    Inherits ObjectModel.KeyedCollection(Of Integer, DigitalOutputLine)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As DigitalOutputLine) As Integer
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(DigitalOutputLine))
        Return item.LineNumber
    End Function

    ''' <summary> Updates the digital active level. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="activeLevel">   The active level. </param>
    Public Sub Update(ByVal lineNumber As Integer, ByVal activeLevel As DigitalActiveLevels)
        If Not Me.Contains(lineNumber) Then
            Me.Add(New DigitalOutputLine With {.LineNumber = lineNumber})
        End If
        Me.Item(lineNumber).ActiveLevel = activeLevel
    End Sub

    ''' <summary> Updates the line level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="lineLevel">  The line level. </param>
    Public Sub Update(ByVal lineNumber As Integer, ByVal lineLevel As Integer)
        If Not Me.Contains(lineNumber) Then
            Me.Add(New DigitalOutputLine With {.LineNumber = lineNumber})
        End If
        Me.Item(lineNumber).LineLevel = lineLevel
    End Sub

    ''' <summary> Updates the digital active level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="lineLevel">  The line level. </param>
    Public Sub Update(ByVal lineNumber As Integer, ByVal lineLevel As Integer?)
        If Not Me.Contains(lineNumber) Then
            Me.Add(New DigitalOutputLine With {.LineNumber = lineNumber})
        End If
        Me.Item(lineNumber).LineLevel = lineLevel
    End Sub

    ''' <summary> Updates the digital active level. </summary>
    ''' <remarks> David, 2020-11-12. </remarks>
    ''' <param name="lineNumber">  The line number. </param>
    ''' <param name="activeLevel"> The active level. </param>
    ''' <param name="lineLevel">   The line level. </param>
    Public Sub Update(ByVal lineNumber As Integer, ByVal activeLevel As DigitalActiveLevels, ByVal lineLevel As Integer)
        If Not Me.Contains(lineNumber) Then
            Me.Add(New DigitalOutputLine With {.LineNumber = lineNumber})
        End If
        Me.Item(lineNumber).LineLevel = lineLevel
        Me.Item(lineNumber).ActiveLevel = activeLevel
    End Sub

    ''' <summary> Gets the digital output line numbers. </summary>
    ''' <value> The digital output line numbers. </value>
    Public ReadOnly Property DigitalOutputLineNumbers As IEnumerable(Of Integer)
        Get
            Dim l As New List(Of Integer)
            For Each dol As DigitalOutputLine In Me
                l.Add(dol.LineNumber)
            Next
            Return l
        End Get
    End Property

    ''' <summary> Gets the active levels. </summary>
    ''' <value> The active levels. </value>
    Public ReadOnly Property ActiveLevels As IEnumerable(Of DigitalActiveLevels)
        Get
            Dim l As New List(Of DigitalActiveLevels)
            For Each dol As DigitalOutputLine In Me
                l.Add(dol.ActiveLevel)
            Next
            Return l
        End Get
    End Property

    ''' <summary> Gets the line levels. </summary>
    ''' <value> The line levels. </value>
    Public ReadOnly Property LineLevels As IEnumerable(Of Integer?)
        Get
            Dim l As New List(Of Integer?)
            For Each dol As DigitalOutputLine In Me
                l.Add(dol.LineLevel)
            Next
            Return l
        End Get
    End Property

    ''' <summary> Gets the polarity. Return nothing if values are not all the same. </summary>
    ''' <value>
    ''' The digital active level common to all line. Nothing if digital active level is not the same
    ''' for all lines.
    ''' </value>
    Public ReadOnly Property DigitalActiveLevel As DigitalActiveLevels?
        Get
            Dim result As New DigitalActiveLevels?
            If Me.Any Then
                For Each dol As DigitalOutputLine In Me
                    If result.HasValue Then
                        If result.Value <> dol.ActiveLevel Then
                            result = Nothing
                            Exit For
                        End If
                    Else
                        result = dol.ActiveLevel
                    End If
                Next
            End If
            Return result
        End Get
    End Property

End Class


''' <summary> Enumerates the digital output mode. Defined
''' with the <see cref="System.FlagsAttribute"/> for defining the
''' <see cref="VI.DigitalOutputSubsystemBase.SupportedOutputModes"/></summary>
<Flags>
Public Enum OutputModes

    ''' <summary> the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> Using line 4 (2400) as end of test signal. </summary>
    <ComponentModel.Description("End of test (EOT)")>
    EndTest = 1

    ''' <summary> Using line 4 (2400) as busy signal. </summary>
    <ComponentModel.Description("Busy (BUSY)")>
    Busy = 2

End Enum

''' <summary>
''' Enumerates the digital active level (high or low) of the end of test or busy signals. Defined
''' with the <see cref="System.FlagsAttribute"/> for defining the
''' <see cref="VI.DigitalOutputSubsystemBase.SupportedDigitalActiveLevels"/>
''' </summary>
''' <remarks> David, 2020-11-12. </remarks>
<Flags>
Public Enum DigitalActiveLevels

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the active high option. </summary>
    <ComponentModel.Description("High (HI)")>
    High = 1

    ''' <summary> An enum constant representing the active low option. </summary>
    <ComponentModel.Description("Low (LO)")>
    Low = 2
End Enum
