''' <summary> Defines a SCPI Compensation Channel Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class CompensateChannelSubsystem
    Inherits VI.CompensateChannelSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="compensationType"> Type of the compensation. </param>
    ''' <param name="channelNumber">    A reference to a <see cref="StatusSubsystemBase">message
    '''                                 based session</see>. </param>
    ''' <param name="statusSubsystem">  The status subsystem. </param>
    Public Sub New(ByVal compensationType As VI.CompensationTypes, ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(compensationType, channelNumber, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Enabled = False
        If Me.CompensationType = VI.CompensationTypes.Load Then
            Me.ModelResistance = 50
            Me.ModelInductance = 0
            Me.ModelCapacitance = 0
        ElseIf Me.CompensationType = VI.CompensationTypes.OpenCircuit Then
            Me.ModelInductance = 0
            Me.ModelConductance = 0
        ElseIf Me.CompensationType = VI.CompensationTypes.ShortCircuit Then
            Me.ModelResistance = 0
            Me.ModelCapacitance = 0
        End If
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " COMMANDS "

    ''' <summary> Gets the Acquire measurements command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:CORR2:COLL:ACQ:&lt;type&gt;". </remarks>
    ''' <value> The clear measurements command. </value>
    Protected Overrides Property AcquireMeasurementsCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:COLL:ACQ:{Me.CompensationTypeCode}"
        End Get
    End Property

    ''' <summary> Gets the clear measured data command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:CORR2:&lt;type&gt;:COLL:CLE". </remarks>
    ''' <value> The clear measurements command. </value>
    Protected Overrides Property ClearMeasurementsCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2{Me.CompensationTypeCode}:COLL:CLE"
        End Get
    End Property

#End Region

#Region " ENABLED "

    ''' <summary> Gets the compensation enabled query command. </summary>
    ''' <value> The compensation enabled query command. </value>
    Protected Overrides Property EnabledQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:{Me.CompensationTypeCode}:STAT?"
        End Get
    End Property

    ''' <summary> Gets the compensation enabled command Format. </summary>
    ''' <value> The compensation enabled query command. </value>
    Protected Overrides Property EnabledCommandFormat As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:{Me.CompensationTypeCode}:STAT {{0:1;1;0}}"
        End Get
    End Property

#End Region

#Region " FREQUENCY STIMULUS POINTS "

    ''' <summary> Gets the Frequency Stimulus Points query command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:CORR2:ZME:&lt;type&gt;:POIN?". </remarks>
    ''' <value> The Frequency Stimulus Points query command. </value>
    Protected Overrides Property FrequencyStimulusPointsQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:ZME:{Me.CompensationTypeCode}:POIN?"
        End Get
    End Property

#End Region

#Region " FREQUENCY ARRAY "

    ''' <summary> Gets the Frequency Array query command. </summary>
    ''' <value> The Frequency Array query command. </value>
    Protected Overrides Property FrequencyArrayQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:ZME:{Me.CompensationTypeCode}:FREQ?"
        End Get
    End Property


#End Region

#Region " IMPEDANCE ARRAY "

    ''' <summary> Gets the Impedance Array query command. </summary>
    ''' <value> The Impedance Array query command. </value>
    Protected Overrides Property ImpedanceArrayQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:ZME:{Me.CompensationTypeCode}:DATA?"
        End Get
    End Property

    ''' <summary> Gets the Impedance Array command Format. </summary>
    ''' <value> The Impedance Array query command. </value>
    Protected Overrides Property ImpedanceArrayCommandFormat As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:ZME:{Me.CompensationTypeCode}:DATA {{0}}"
        End Get
    End Property

#End Region

#Region " MODEL RESISTANCE "

    ''' <summary> Gets the Model Resistance query command. </summary>
    ''' <value> The Model Resistance query command. </value>
    Protected Overrides Property ModelResistanceQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:R?"
        End Get
    End Property

    ''' <summary> Gets the Model Resistance command Format. </summary>
    ''' <value> The Model Resistance query command. </value>
    Protected Overrides Property ModelResistanceCommandFormat As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:R {{0}}"
        End Get
    End Property

#End Region

#Region " MODEL CONDUCTANCE "

    ''' <summary> Gets the Model Conductance query command. </summary>
    ''' <value> The Model Conductance query command. </value>
    Protected Overrides Property ModelConductanceQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:G?"
        End Get
    End Property

    ''' <summary> Gets the Model Conductance command Format. </summary>
    ''' <value> The Model Conductance query command. </value>
    Protected Overrides Property ModelConductanceCommandFormat As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:G {{0}}"
        End Get
    End Property

#End Region

#Region " MODEL CAPACITANCE "

    ''' <summary> Gets the Model Capacitance query command. </summary>
    ''' <value> The Model Capacitance query command. </value>
    Protected Overrides Property ModelCapacitanceQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:C?"
        End Get
    End Property

    ''' <summary> Gets the Model Capacitance command Format. </summary>
    ''' <value> The Model Capacitance query command. </value>
    Protected Overrides Property ModelCapacitanceCommandFormat As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:C {{0}}"
        End Get
    End Property

#End Region

#Region " MODEL INDUCTANCE "

    ''' <summary> Gets the Model Inductance query command. </summary>
    ''' <value> The Model Inductance query command. </value>
    Protected Overrides Property ModelInductanceQueryCommand As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:L?"
        End Get
    End Property

    ''' <summary> Gets the Model Inductance command Format. </summary>
    ''' <value> The Model Inductance query command. </value>
    Protected Overrides Property ModelInductanceCommandFormat As String
        Get
            Return $":SENS{Me.ChannelNumber}:CORR2:CKIT:{Me.CompensationTypeCode}:L {{0}}"
        End Get
    End Property

#End Region

#End Region

End Class
