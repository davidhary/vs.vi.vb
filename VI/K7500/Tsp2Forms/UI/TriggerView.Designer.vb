﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TriggerView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TriggerView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._TriggerToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._TriggerDelayToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TriggerDelaysToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._StartTriggerDelayNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._StartTriggerDelayNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._EndTriggerDelayNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._EndTriggerDelayNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._Limit1ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._Limits1Label = New System.Windows.Forms.ToolStripLabel()
        Me._Limit1DecimalsNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._LowerLimit1NumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._LowerLimit1Numeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._UpperLimit1NumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._UpperLimit1Numeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._GradeBinningToolStrip = New System.Windows.Forms.ToolStrip()
        Me._BinningToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PassBitPatternNumericButton = New System.Windows.Forms.ToolStripButton()
        Me._PassBitPatternNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._OpenLeadsBitPatternNumericButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenLeadsBitPatternNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._FailBitPatternNumericButton = New System.Windows.Forms.ToolStripButton()
        Me._FailLimit1BitPatternNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._TriggerLayerToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TriggerLayerToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TriggerCountNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TriggerCountNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._TriggerSourceComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._TriggerSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ContinuousTriggerEnabledMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadTriggerStateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TriggerModelsToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TriggerModelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ApplyTriggerModelSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._LoadGradeBinTriggerModelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._LoadSimpleLoopMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._RunSimpleLoopMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearTriggerModelMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MeterCompleterFirstGradingBinningMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        Me._TriggerToolStripPanel.SuspendLayout()
        Me._TriggerDelayToolStrip.SuspendLayout()
        Me._Limit1ToolStrip.SuspendLayout()
        Me._GradeBinningToolStrip.SuspendLayout()
        Me._TriggerLayerToolStrip.SuspendLayout()
        Me._TriggerModelsToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.Size = New System.Drawing.Size(379, 322)
        Me._Layout.TabIndex = 0
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._TriggerToolStripPanel)
        Me._Panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Panel.Location = New System.Drawing.Point(6, 6)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(367, 310)
        Me._Panel.TabIndex = 0
        '
        '_TriggerToolStripPanel
        '
        Me._TriggerToolStripPanel.Controls.Add(Me._TriggerDelayToolStrip)
        Me._TriggerToolStripPanel.Controls.Add(Me._Limit1ToolStrip)
        Me._TriggerToolStripPanel.Controls.Add(Me._GradeBinningToolStrip)
        Me._TriggerToolStripPanel.Controls.Add(Me._TriggerLayerToolStrip)
        Me._TriggerToolStripPanel.Controls.Add(Me._TriggerModelsToolStrip)
        Me._TriggerToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TriggerToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me._TriggerToolStripPanel.Name = "_TriggerToolStripPanel"
        Me._TriggerToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._TriggerToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me._TriggerToolStripPanel.Size = New System.Drawing.Size(367, 137)
        '
        '_TriggerDelayToolStrip
        '
        Me._TriggerDelayToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TriggerDelayToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TriggerDelayToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._TriggerDelayToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TriggerDelaysToolStripLabel, Me._StartTriggerDelayNumericLabel, Me._StartTriggerDelayNumeric, Me._EndTriggerDelayNumericLabel, Me._EndTriggerDelayNumeric})
        Me._TriggerDelayToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._TriggerDelayToolStrip.Name = "_TriggerDelayToolStrip"
        Me._TriggerDelayToolStrip.Size = New System.Drawing.Size(230, 28)
        Me._TriggerDelayToolStrip.TabIndex = 1
        '
        '_TriggerDelaysToolStripLabel
        '
        Me._TriggerDelaysToolStripLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerDelaysToolStripLabel.Name = "_TriggerDelaysToolStripLabel"
        Me._TriggerDelaysToolStripLabel.Size = New System.Drawing.Size(43, 25)
        Me._TriggerDelaysToolStripLabel.Text = "DELAY"
        '
        '_StartTriggerDelayNumericLabel
        '
        Me._StartTriggerDelayNumericLabel.Name = "_StartTriggerDelayNumericLabel"
        Me._StartTriggerDelayNumericLabel.Size = New System.Drawing.Size(27, 25)
        Me._StartTriggerDelayNumericLabel.Text = "Pre:"
        '
        '_StartTriggerDelayNumeric
        '
        Me._StartTriggerDelayNumeric.AutoSize = False
        Me._StartTriggerDelayNumeric.Name = "_StartTriggerDelayNumeric"
        Me._StartTriggerDelayNumeric.Size = New System.Drawing.Size(62, 25)
        Me._StartTriggerDelayNumeric.Text = "0"
        Me._StartTriggerDelayNumeric.ToolTipText = "Start delay in seconds"
        Me._StartTriggerDelayNumeric.Value = New Decimal(New Integer() {20, 0, 0, 196608})
        '
        '_EndTriggerDelayNumericLabel
        '
        Me._EndTriggerDelayNumericLabel.Name = "_EndTriggerDelayNumericLabel"
        Me._EndTriggerDelayNumericLabel.Size = New System.Drawing.Size(33, 25)
        Me._EndTriggerDelayNumericLabel.Text = "Post:"
        '
        '_EndTriggerDelayNumeric
        '
        Me._EndTriggerDelayNumeric.AutoSize = False
        Me._EndTriggerDelayNumeric.Name = "_EndTriggerDelayNumeric"
        Me._EndTriggerDelayNumeric.Size = New System.Drawing.Size(62, 23)
        Me._EndTriggerDelayNumeric.Text = "0"
        Me._EndTriggerDelayNumeric.ToolTipText = "End delay in seconds"
        Me._EndTriggerDelayNumeric.Value = New Decimal(New Integer() {1, 0, 0, 196608})
        '
        '_Limit1ToolStrip
        '
        Me._Limit1ToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._Limit1ToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._Limit1ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._Limit1ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._Limits1Label, Me._Limit1DecimalsNumeric, Me._LowerLimit1NumericLabel, Me._LowerLimit1Numeric, Me._UpperLimit1NumericLabel, Me._UpperLimit1Numeric})
        Me._Limit1ToolStrip.Location = New System.Drawing.Point(3, 28)
        Me._Limit1ToolStrip.Name = "_Limit1ToolStrip"
        Me._Limit1ToolStrip.Size = New System.Drawing.Size(251, 28)
        Me._Limit1ToolStrip.TabIndex = 2
        '
        '_Limits1Label
        '
        Me._Limits1Label.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Limits1Label.Name = "_Limits1Label"
        Me._Limits1Label.Size = New System.Drawing.Size(44, 25)
        Me._Limits1Label.Text = "LIMIT1:"
        '
        '_Limit1DecimalsNumeric
        '
        Me._Limit1DecimalsNumeric.Name = "_Limit1DecimalsNumeric"
        Me._Limit1DecimalsNumeric.Size = New System.Drawing.Size(41, 25)
        Me._Limit1DecimalsNumeric.Text = "3"
        Me._Limit1DecimalsNumeric.ToolTipText = "Number of decimal places for setting the limits"
        Me._Limit1DecimalsNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_LowerLimit1NumericLabel
        '
        Me._LowerLimit1NumericLabel.Name = "_LowerLimit1NumericLabel"
        Me._LowerLimit1NumericLabel.Size = New System.Drawing.Size(42, 25)
        Me._LowerLimit1NumericLabel.Text = "Lower:"
        '
        '_LowerLimit1Numeric
        '
        Me._LowerLimit1Numeric.Name = "_LowerLimit1Numeric"
        Me._LowerLimit1Numeric.Size = New System.Drawing.Size(41, 25)
        Me._LowerLimit1Numeric.Text = "0"
        Me._LowerLimit1Numeric.ToolTipText = "Lower limit"
        Me._LowerLimit1Numeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_UpperLimit1NumericLabel
        '
        Me._UpperLimit1NumericLabel.Name = "_UpperLimit1NumericLabel"
        Me._UpperLimit1NumericLabel.Size = New System.Drawing.Size(39, 25)
        Me._UpperLimit1NumericLabel.Text = "Upper"
        '
        '_UpperLimit1Numeric
        '
        Me._UpperLimit1Numeric.Name = "_UpperLimit1Numeric"
        Me._UpperLimit1Numeric.Size = New System.Drawing.Size(41, 25)
        Me._UpperLimit1Numeric.Text = "0"
        Me._UpperLimit1Numeric.ToolTipText = "Upper limit"
        Me._UpperLimit1Numeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_GradeBinningToolStrip
        '
        Me._GradeBinningToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._GradeBinningToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._GradeBinningToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._GradeBinningToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._BinningToolStripLabel, Me._PassBitPatternNumericButton, Me._PassBitPatternNumeric, Me._OpenLeadsBitPatternNumericButton, Me._OpenLeadsBitPatternNumeric, Me._FailBitPatternNumericButton, Me._FailLimit1BitPatternNumeric})
        Me._GradeBinningToolStrip.Location = New System.Drawing.Point(3, 56)
        Me._GradeBinningToolStrip.Name = "_GradeBinningToolStrip"
        Me._GradeBinningToolStrip.Size = New System.Drawing.Size(300, 28)
        Me._GradeBinningToolStrip.TabIndex = 0
        '
        '_BinningToolStripLabel
        '
        Me._BinningToolStripLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BinningToolStripLabel.Name = "_BinningToolStripLabel"
        Me._BinningToolStripLabel.Size = New System.Drawing.Size(26, 25)
        Me._BinningToolStripLabel.Text = "BIN"
        '
        '_PassBitPatternNumericButton
        '
        Me._PassBitPatternNumericButton.CheckOnClick = True
        Me._PassBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._PassBitPatternNumericButton.Image = CType(resources.GetObject("_PassBitPatternNumericButton.Image"), System.Drawing.Image)
        Me._PassBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._PassBitPatternNumericButton.Name = "_PassBitPatternNumericButton"
        Me._PassBitPatternNumericButton.Size = New System.Drawing.Size(49, 25)
        Me._PassBitPatternNumericButton.Text = "Pass 0x"
        '
        '_PassBitPatternNumeric
        '
        Me._PassBitPatternNumeric.Name = "_PassBitPatternNumeric"
        Me._PassBitPatternNumeric.Size = New System.Drawing.Size(41, 25)
        Me._PassBitPatternNumeric.Text = "2"
        Me._PassBitPatternNumeric.ToolTipText = "Pass bit pattern"
        Me._PassBitPatternNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_OpenLeadsBitPatternNumericButton
        '
        Me._OpenLeadsBitPatternNumericButton.CheckOnClick = True
        Me._OpenLeadsBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OpenLeadsBitPatternNumericButton.Image = CType(resources.GetObject("_OpenLeadsBitPatternNumericButton.Image"), System.Drawing.Image)
        Me._OpenLeadsBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenLeadsBitPatternNumericButton.Name = "_OpenLeadsBitPatternNumericButton"
        Me._OpenLeadsBitPatternNumericButton.Size = New System.Drawing.Size(55, 25)
        Me._OpenLeadsBitPatternNumericButton.Text = "Open 0x"
        '
        '_OpenLeadsBitPatternNumeric
        '
        Me._OpenLeadsBitPatternNumeric.Name = "_OpenLeadsBitPatternNumeric"
        Me._OpenLeadsBitPatternNumeric.Size = New System.Drawing.Size(41, 25)
        Me._OpenLeadsBitPatternNumeric.Text = "0"
        Me._OpenLeadsBitPatternNumeric.ToolTipText = "Open leads bit pattern"
        Me._OpenLeadsBitPatternNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_FailBitPatternNumericButton
        '
        Me._FailBitPatternNumericButton.CheckOnClick = True
        Me._FailBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._FailBitPatternNumericButton.Image = CType(resources.GetObject("_FailBitPatternNumericButton.Image"), System.Drawing.Image)
        Me._FailBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FailBitPatternNumericButton.Name = "_FailBitPatternNumericButton"
        Me._FailBitPatternNumericButton.Size = New System.Drawing.Size(44, 25)
        Me._FailBitPatternNumericButton.Text = "Fail 0x"
        '
        '_FailLimit1BitPatternNumeric
        '
        Me._FailLimit1BitPatternNumeric.Name = "_FailLimit1BitPatternNumeric"
        Me._FailLimit1BitPatternNumeric.Size = New System.Drawing.Size(41, 25)
        Me._FailLimit1BitPatternNumeric.Text = "1"
        Me._FailLimit1BitPatternNumeric.ToolTipText = "Failed bit pattern"
        Me._FailLimit1BitPatternNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_TriggerLayerToolStrip
        '
        Me._TriggerLayerToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TriggerLayerToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TriggerLayerToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._TriggerLayerToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TriggerLayerToolStripLabel, Me._TriggerCountNumericLabel, Me._TriggerCountNumeric, Me._TriggerSourceComboBox, Me._TriggerSplitButton})
        Me._TriggerLayerToolStrip.Location = New System.Drawing.Point(3, 84)
        Me._TriggerLayerToolStrip.Name = "_TriggerLayerToolStrip"
        Me._TriggerLayerToolStrip.Size = New System.Drawing.Size(250, 28)
        Me._TriggerLayerToolStrip.TabIndex = 7
        Me._TriggerLayerToolStrip.Text = "ToolStrip1"
        '
        '_TriggerLayerToolStripLabel
        '
        Me._TriggerLayerToolStripLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerLayerToolStripLabel.Name = "_TriggerLayerToolStripLabel"
        Me._TriggerLayerToolStripLabel.Size = New System.Drawing.Size(34, 25)
        Me._TriggerLayerToolStripLabel.Text = "TRIG:"
        '
        '_TriggerCountNumericLabel
        '
        Me._TriggerCountNumericLabel.Name = "_TriggerCountNumericLabel"
        Me._TriggerCountNumericLabel.Size = New System.Drawing.Size(19, 25)
        Me._TriggerCountNumericLabel.Text = "N:"
        '
        '_TriggerCountNumeric
        '
        Me._TriggerCountNumeric.Name = "_TriggerCountNumeric"
        Me._TriggerCountNumeric.Size = New System.Drawing.Size(41, 25)
        Me._TriggerCountNumeric.Text = "0"
        Me._TriggerCountNumeric.ToolTipText = "Trigger Count"
        Me._TriggerCountNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_TriggerSourceComboBox
        '
        Me._TriggerSourceComboBox.Name = "_TriggerSourceComboBox"
        Me._TriggerSourceComboBox.Size = New System.Drawing.Size(91, 28)
        Me._TriggerSourceComboBox.Text = "Immediate"
        Me._TriggerSourceComboBox.ToolTipText = "Trigger Source"
        '
        '_TriggerSplitButton
        '
        Me._TriggerSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ContinuousTriggerEnabledMenuItem, Me._ReadTriggerStateMenuItem})
        Me._TriggerSplitButton.Name = "_TriggerSplitButton"
        Me._TriggerSplitButton.Size = New System.Drawing.Size(60, 25)
        Me._TriggerSplitButton.Text = "More..."
        Me._TriggerSplitButton.ToolTipText = "Continuous On/Off"
        '
        '_ContinuousTriggerEnabledMenuItem
        '
        Me._ContinuousTriggerEnabledMenuItem.CheckOnClick = True
        Me._ContinuousTriggerEnabledMenuItem.Name = "_ContinuousTriggerEnabledMenuItem"
        Me._ContinuousTriggerEnabledMenuItem.Size = New System.Drawing.Size(136, 22)
        Me._ContinuousTriggerEnabledMenuItem.Text = "Continuous"
        Me._ContinuousTriggerEnabledMenuItem.ToolTipText = "Check to enable"
        '
        '_ReadTriggerStateMenuItem
        '
        Me._ReadTriggerStateMenuItem.Name = "_ReadTriggerStateMenuItem"
        Me._ReadTriggerStateMenuItem.Size = New System.Drawing.Size(136, 22)
        Me._ReadTriggerStateMenuItem.Text = "Read State"
        Me._ReadTriggerStateMenuItem.ToolTipText = "Reads trigger state"
        '
        '_TriggerModelsToolStrip
        '
        Me._TriggerModelsToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TriggerModelsToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TriggerModelsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._TriggerModelsToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TriggerModelLabel, Me._ApplyTriggerModelSplitButton})
        Me._TriggerModelsToolStrip.Location = New System.Drawing.Point(5, 112)
        Me._TriggerModelsToolStrip.Name = "_TriggerModelsToolStrip"
        Me._TriggerModelsToolStrip.Size = New System.Drawing.Size(142, 25)
        Me._TriggerModelsToolStrip.TabIndex = 3
        '
        '_TriggerModelLabel
        '
        Me._TriggerModelLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerModelLabel.Name = "_TriggerModelLabel"
        Me._TriggerModelLabel.Size = New System.Drawing.Size(45, 22)
        Me._TriggerModelLabel.Text = "APPLY:"
        '
        '_ApplyTriggerModelSplitButton
        '
        Me._ApplyTriggerModelSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ApplyTriggerModelSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LoadGradeBinTriggerModelMenuItem, Me._LoadSimpleLoopMenuItem, Me._RunSimpleLoopMenuItem, Me._ClearTriggerModelMenuItem, Me._MeterCompleterFirstGradingBinningMenuItem})
        Me._ApplyTriggerModelSplitButton.Image = CType(resources.GetObject("_ApplyTriggerModelSplitButton.Image"), System.Drawing.Image)
        Me._ApplyTriggerModelSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ApplyTriggerModelSplitButton.Name = "_ApplyTriggerModelSplitButton"
        Me._ApplyTriggerModelSplitButton.Size = New System.Drawing.Size(63, 22)
        Me._ApplyTriggerModelSplitButton.Text = "Select..."
        Me._ApplyTriggerModelSplitButton.ToolTipText = "Select option:"
        '
        '_LoadGradeBinTriggerModelMenuItem
        '
        Me._LoadGradeBinTriggerModelMenuItem.Name = "_LoadGradeBinTriggerModelMenuItem"
        Me._LoadGradeBinTriggerModelMenuItem.Size = New System.Drawing.Size(352, 22)
        Me._LoadGradeBinTriggerModelMenuItem.Text = "Grading/Binning Trigger Loop"
        Me._LoadGradeBinTriggerModelMenuItem.ToolTipText = "Loads the grading binning trigger model"
        '
        '_LoadSimpleLoopMenuItem
        '
        Me._LoadSimpleLoopMenuItem.Name = "_LoadSimpleLoopMenuItem"
        Me._LoadSimpleLoopMenuItem.Size = New System.Drawing.Size(352, 22)
        Me._LoadSimpleLoopMenuItem.Text = "Load Simple Loop"
        Me._LoadSimpleLoopMenuItem.ToolTipText = "Loads a simple loop trigger model."
        '
        '_RunSimpleLoopMenuItem
        '
        Me._RunSimpleLoopMenuItem.Name = "_RunSimpleLoopMenuItem"
        Me._RunSimpleLoopMenuItem.Size = New System.Drawing.Size(352, 22)
        Me._RunSimpleLoopMenuItem.Text = "Run Simple Loop"
        Me._RunSimpleLoopMenuItem.ToolTipText = "Initiates a single loop, waits for completion and displays the values"
        '
        '_ClearTriggerModelMenuItem
        '
        Me._ClearTriggerModelMenuItem.Name = "_ClearTriggerModelMenuItem"
        Me._ClearTriggerModelMenuItem.Size = New System.Drawing.Size(352, 22)
        Me._ClearTriggerModelMenuItem.Text = "Clear Trigger Model"
        Me._ClearTriggerModelMenuItem.ToolTipText = "Clears the instrument trigger model"
        '
        '_MeterCompleterFirstGradingBinningMenuItem
        '
        Me._MeterCompleterFirstGradingBinningMenuItem.Name = "_MeterCompleterFirstGradingBinningMenuItem"
        Me._MeterCompleterFirstGradingBinningMenuItem.Size = New System.Drawing.Size(352, 22)
        Me._MeterCompleterFirstGradingBinningMenuItem.Text = "Meter Complete First Grading/Binning Trigger Model"
        Me._MeterCompleterFirstGradingBinningMenuItem.ToolTipText = "Applies a trigger model where meter complete is output first."
        '
        'TriggerView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "TriggerView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me._TriggerToolStripPanel.ResumeLayout(False)
        Me._TriggerToolStripPanel.PerformLayout()
        Me._TriggerDelayToolStrip.ResumeLayout(False)
        Me._TriggerDelayToolStrip.PerformLayout()
        Me._Limit1ToolStrip.ResumeLayout(False)
        Me._Limit1ToolStrip.PerformLayout()
        Me._GradeBinningToolStrip.ResumeLayout(False)
        Me._GradeBinningToolStrip.PerformLayout()
        Me._TriggerLayerToolStrip.ResumeLayout(False)
        Me._TriggerLayerToolStrip.PerformLayout()
        Me._TriggerModelsToolStrip.ResumeLayout(False)
        Me._TriggerModelsToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _TriggerToolStripPanel As Windows.Forms.ToolStripPanel
    Private WithEvents _TriggerDelayToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _TriggerDelaysToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _StartTriggerDelayNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _StartTriggerDelayNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _EndTriggerDelayNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _EndTriggerDelayNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _Limit1ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _Limits1Label As Windows.Forms.ToolStripLabel
    Private WithEvents _Limit1DecimalsNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _LowerLimit1NumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _LowerLimit1Numeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _UpperLimit1NumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _UpperLimit1Numeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _GradeBinningToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _BinningToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _PassBitPatternNumericButton As Windows.Forms.ToolStripButton
    Private WithEvents _PassBitPatternNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _OpenLeadsBitPatternNumericButton As Windows.Forms.ToolStripButton
    Private WithEvents _OpenLeadsBitPatternNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _FailBitPatternNumericButton As Windows.Forms.ToolStripButton
    Private WithEvents _FailLimit1BitPatternNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _TriggerLayerToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _TriggerLayerToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _TriggerCountNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _TriggerCountNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _TriggerSourceComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _TriggerSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ContinuousTriggerEnabledMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadTriggerStateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _TriggerModelsToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _TriggerModelLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ApplyTriggerModelSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _LoadGradeBinTriggerModelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _LoadSimpleLoopMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _RunSimpleLoopMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearTriggerModelMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _MeterCompleterFirstGradingBinningMenuItem As Windows.Forms.ToolStripMenuItem
End Class
