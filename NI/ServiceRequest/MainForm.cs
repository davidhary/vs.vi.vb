//==================================================================================================
//
// Title      : MainForm.cs
// Purpose    : This application illustrates how to use the service request event and
//              the service request status byte to determine when generated data is ready
//              and how to read it.
//
//==================================================================================================

using System;
using System.Windows.Forms;
using Ivi.Visa;

namespace NationalInstruments.Examples.ServiceRequest
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form
    {
        private IMessageBasedSession _MbSession;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _SelectResourceLabel;
        private System.Windows.Forms.GroupBox _ConfiguringGroupBox;
        private System.Windows.Forms.GroupBox _WritingGroupBox;
        private System.Windows.Forms.GroupBox _ReadingGroupBox;
        private System.Windows.Forms.Button _ClearButton;
        private System.Windows.Forms.Label _ResourceNameLabel;
        private System.Windows.Forms.Button _OpenButton;
        private System.Windows.Forms.Button _CloseButton;
        private System.Windows.Forms.TextBox _CommandTextBox;
        private System.Windows.Forms.Label _CommandLabel;
        private System.Windows.Forms.Button _EnableSRQButton;
        private System.Windows.Forms.TextBox _WriteTextBox;
        private System.Windows.Forms.Button _WriteButton;
        private System.Windows.Forms.TextBox _ReadTextBox;
        private System.Windows.Forms.ComboBox _ResourceNameComboBox;
        private System.ComponentModel.IContainer _Components;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            this.InitializeComponent();
            this.InitializeUI();
            this._ToolTip.SetToolTip( this._EnableSRQButton, "Enable the instrument's SRQ event on MAV by sending the following command (varies by instrument):");
            this._ToolTip.SetToolTip( this._WriteButton, "Send string to device");
            this._ToolTip.SetToolTip( this._CloseButton, "Causes the control to release its handle to the device");
            this._ToolTip.SetToolTip( this._OpenButton, "The resource name of the device is set and the control attempts to connect to the device");

            try
            {
                // This example uses an instance of the NationalInstruments.Visa.ResourceManager class to find resources on the system.
                // Alternatively, static methods provided by the Ivi.Visa.ResourceManager class may be used when an application
                // requires additional VISA .NET implementations.
                var validResources = Ivi.Visa.GlobalResourceManager.Find( "(GPIB|TCPIP|USB)?*INSTR" );
                foreach ( var resource in validResources )
                {
                    _ = this._ResourceNameComboBox.Items.Add( resource );
                }
            }
            catch (Exception)
            {
                _ = this._ResourceNameComboBox.Items.Add( "No 488.2 INSTR resource found on the system" );
                this.UpdateResourceNameControls(false);
                this._CloseButton.Enabled = false;
            }
            this._ResourceNameComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if ( this._Components != null)
                {
                    this._Components.Dispose();
                }
                if ( this._MbSession != null)
                {
                    this._MbSession.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._Components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._ResourceNameLabel = new System.Windows.Forms.Label();
            this._OpenButton = new System.Windows.Forms.Button();
            this._CloseButton = new System.Windows.Forms.Button();
            this._CommandLabel = new System.Windows.Forms.Label();
            this._CommandTextBox = new System.Windows.Forms.TextBox();
            this._EnableSRQButton = new System.Windows.Forms.Button();
            this._ConfiguringGroupBox = new System.Windows.Forms.GroupBox();
            this._ResourceNameComboBox = new System.Windows.Forms.ComboBox();
            this._SelectResourceLabel = new System.Windows.Forms.Label();
            this._WritingGroupBox = new System.Windows.Forms.GroupBox();
            this._WriteTextBox = new System.Windows.Forms.TextBox();
            this._WriteButton = new System.Windows.Forms.Button();
            this._ReadingGroupBox = new System.Windows.Forms.GroupBox();
            this._ClearButton = new System.Windows.Forms.Button();
            this._ReadTextBox = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this._Components);
            this._ConfiguringGroupBox.SuspendLayout();
            this._WritingGroupBox.SuspendLayout();
            this._ReadingGroupBox.SuspendLayout();
            this.SuspendLayout();
            //
            // resourceNameLabel
            //
            this._ResourceNameLabel.Location = new System.Drawing.Point(16, 80);
            this._ResourceNameLabel.Name = "resourceNameLabel";
            this._ResourceNameLabel.Size = new System.Drawing.Size(112, 16);
            this._ResourceNameLabel.TabIndex = 1;
            this._ResourceNameLabel.Text = "Resource Name:";
            //
            // openButton
            //
            this._OpenButton.Location = new System.Drawing.Point(16, 128);
            this._OpenButton.Name = "openButton";
            this._OpenButton.Size = new System.Drawing.Size(104, 23);
            this._OpenButton.TabIndex = 2;
            this._OpenButton.Text = "Open Session";
            this._OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            //
            // closeButton
            //
            this._CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CloseButton.Location = new System.Drawing.Point(160, 64);
            this._CloseButton.Name = "closeButton";
            this._CloseButton.Size = new System.Drawing.Size(104, 23);
            this._CloseButton.TabIndex = 3;
            this._CloseButton.Text = "Close Session";
            this._CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            //
            // commandLabel
            //
            this._CommandLabel.Location = new System.Drawing.Point(16, 160);
            this._CommandLabel.Name = "commandLabel";
            this._CommandLabel.Size = new System.Drawing.Size(256, 32);
            this._CommandLabel.TabIndex = 4;
            this._CommandLabel.Text = "Type the command to enable the instrument\'s SRQ event on MAV:";
            //
            // commandTextBox
            //
            this._CommandTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._CommandTextBox.Location = new System.Drawing.Point(16, 200);
            this._CommandTextBox.Name = "commandTextBox";
            this._CommandTextBox.Size = new System.Drawing.Size(152, 20);
            this._CommandTextBox.TabIndex = 5;
            this._CommandTextBox.Text = "*SRE 16";
            //
            // enableSRQButton
            //
            this._EnableSRQButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._EnableSRQButton.Location = new System.Drawing.Point(168, 200);
            this._EnableSRQButton.Name = "enableSRQButton";
            this._EnableSRQButton.Size = new System.Drawing.Size(104, 24);
            this._EnableSRQButton.TabIndex = 6;
            this._EnableSRQButton.Text = "Enable SRQ";
            this._EnableSRQButton.Click += new System.EventHandler(this.EnableSRQButton_Click);
            //
            // configuringGroupBox
            //
            this._ConfiguringGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ConfiguringGroupBox.Controls.Add(this._ResourceNameComboBox);
            this._ConfiguringGroupBox.Controls.Add(this._CloseButton);
            this._ConfiguringGroupBox.Location = new System.Drawing.Point(8, 64);
            this._ConfiguringGroupBox.Name = "configuringGroupBox";
            this._ConfiguringGroupBox.Size = new System.Drawing.Size(272, 168);
            this._ConfiguringGroupBox.TabIndex = 7;
            this._ConfiguringGroupBox.TabStop = false;
            this._ConfiguringGroupBox.Text = "Configuring";
            //
            // resourceNameComboBox
            //
            this._ResourceNameComboBox.FormattingEnabled = true;
            this._ResourceNameComboBox.Location = new System.Drawing.Point(11, 35);
            this._ResourceNameComboBox.Name = "resourceNameComboBox";
            this._ResourceNameComboBox.Size = new System.Drawing.Size(255, 21);
            this._ResourceNameComboBox.TabIndex = 4;
            //
            // selectResourceLabel
            //
            this._SelectResourceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._SelectResourceLabel.Location = new System.Drawing.Point(8, 8);
            this._SelectResourceLabel.Name = "selectResourceLabel";
            this._SelectResourceLabel.Size = new System.Drawing.Size(272, 56);
            this._SelectResourceLabel.TabIndex = 8;
            this._SelectResourceLabel.Text = "Select the Resource Name associated with your device and press the Configure Devi" +
    "ce button. Then enter the command string that enables SRQ and click the Enable S" +
    "RQ button.";
            //
            // writingGroupBox
            //
            this._WritingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._WritingGroupBox.Controls.Add(this._WriteTextBox);
            this._WritingGroupBox.Controls.Add(this._WriteButton);
            this._WritingGroupBox.Location = new System.Drawing.Point(8, 240);
            this._WritingGroupBox.Name = "writingGroupBox";
            this._WritingGroupBox.Size = new System.Drawing.Size(272, 56);
            this._WritingGroupBox.TabIndex = 9;
            this._WritingGroupBox.TabStop = false;
            this._WritingGroupBox.Text = "Writing";
            //
            // writeTextBox
            //
            this._WriteTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._WriteTextBox.Location = new System.Drawing.Point(8, 24);
            this._WriteTextBox.Name = "writeTextBox";
            this._WriteTextBox.Size = new System.Drawing.Size(152, 20);
            this._WriteTextBox.TabIndex = 2;
            this._WriteTextBox.Text = "*IDN?\\n";
            //
            // writeButton
            //
            this._WriteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._WriteButton.Location = new System.Drawing.Point(160, 24);
            this._WriteButton.Name = "writeButton";
            this._WriteButton.Size = new System.Drawing.Size(104, 23);
            this._WriteButton.TabIndex = 1;
            this._WriteButton.Text = "Write";
            this._WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
            //
            // readingGroupBox
            //
            this._ReadingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadingGroupBox.Controls.Add(this._ClearButton);
            this._ReadingGroupBox.Controls.Add(this._ReadTextBox);
            this._ReadingGroupBox.Location = new System.Drawing.Point(8, 312);
            this._ReadingGroupBox.Name = "readingGroupBox";
            this._ReadingGroupBox.Size = new System.Drawing.Size(272, 120);
            this._ReadingGroupBox.TabIndex = 10;
            this._ReadingGroupBox.TabStop = false;
            this._ReadingGroupBox.Text = "Reading";
            //
            // clearButton
            //
            this._ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._ClearButton.Location = new System.Drawing.Point(8, 88);
            this._ClearButton.Name = "clearButton";
            this._ClearButton.Size = new System.Drawing.Size(104, 23);
            this._ClearButton.TabIndex = 1;
            this._ClearButton.Text = "Clear";
            this._ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            //
            // readTextBox
            //
            this._ReadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadTextBox.Location = new System.Drawing.Point(8, 24);
            this._ReadTextBox.Multiline = true;
            this._ReadTextBox.Name = "readTextBox";
            this._ReadTextBox.ReadOnly = true;
            this._ReadTextBox.Size = new System.Drawing.Size(256, 56);
            this._ReadTextBox.TabIndex = 0;
            //
            // MainForm
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(288, 448);
            this.Controls.Add(this._ReadingGroupBox);
            this.Controls.Add(this._WritingGroupBox);
            this.Controls.Add(this._SelectResourceLabel);
            this.Controls.Add(this._EnableSRQButton);
            this.Controls.Add(this._CommandTextBox);
            this.Controls.Add(this._CommandLabel);
            this.Controls.Add(this._OpenButton);
            this.Controls.Add(this._ResourceNameLabel);
            this.Controls.Add(this._ConfiguringGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(296, 482);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service Request";
            this._ConfiguringGroupBox.ResumeLayout(false);
            this._WritingGroupBox.ResumeLayout(false);
            this._WritingGroupBox.PerformLayout();
            this._ReadingGroupBox.ResumeLayout(false);
            this._ReadingGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.Run(new MainForm());
        }

        private void UpdateResourceNameControls(bool enable)
        {
            this._ResourceNameComboBox.Enabled = enable;
            this._OpenButton.Enabled = enable;
            this._CloseButton.Enabled = !enable;
            if (enable)
            {
                _ = this._OpenButton.Focus();
            }
        }

        private void UpdateSRQControls(bool enable)
        {
            this._CommandTextBox.Enabled = enable;
            this._EnableSRQButton.Enabled = enable;
            if (enable)
            {
                _ = this._EnableSRQButton.Focus();
            }
        }

        private void UpdateWriteControls(bool enable)
        {
            this._WriteTextBox.Enabled = enable;
            this._WriteButton.Enabled = enable;
            if (enable)
            {
                _ = this._WriteButton.Focus();
            }
        }

        private void InitializeUI()
        {
            this.UpdateResourceNameControls(true);
            this.UpdateSRQControls(false);
            this.UpdateWriteControls(false);
        }


        // When the Open Session button is pressed, the resource name of the
        // device is set and the control attempts to connect to the device
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void OpenButton_Click(object sender, System.EventArgs e)
        {
            try
            {
                this._MbSession = ( Ivi.Visa.IMessageBasedSession ) Ivi.Visa.GlobalResourceManager.Open( this._ResourceNameComboBox.Text );
                // Use SynchronizeCallbacks to specify that the object marshals callbacks across threads appropriately.
                this._MbSession.SynchronizeCallbacks = true;
                this.UpdateResourceNameControls( false );
                this.UpdateSRQControls( true );
            }
            catch (Exception exp)
            {
                _ = MessageBox.Show( exp.Message );
            }
        }


        // The Enable SRQ button writes the string that tells the instrument to
        // enable the SRQ bit
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void EnableSRQButton_Click(object sender, System.EventArgs e)
        {
            try
            {   // Registering a handler for an event automatically enables that event.
                this._MbSession.ServiceRequest += this.OnServiceRequest;
                this.WriteToSession( this._CommandTextBox.Text);
                this.UpdateSRQControls(false);
                this.UpdateWriteControls(true);
            }
            catch(Exception exp)
            {
                _ = MessageBox.Show( exp.Message );
            }
        }

        // Pressing Close Session causes the control to release its handle to the device
        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            this._MbSession.ServiceRequest -= this.OnServiceRequest;
            this._MbSession.Dispose();
            this.InitializeUI();
        }

        // Clicking the Write Button causes the Send String to be written to the device
        private void WriteButton_Click(object sender, System.EventArgs e)
        {
            this.WriteToSession( this._WriteTextBox.Text);
        }

        // Pressing the Clear button clears the read text box
        private void ClearButton_Click(object sender, System.EventArgs e)
        {
            this._ReadTextBox.Clear();
        }

        private string ReplaceCommonEscapeSequences(string s)
        {
            return s.Replace("\\n", "\n").Replace("\\r", "\r");
        }

        private string InsertCommonEscapeSequences(string s)
        {
            return s.Replace("\n", "\\n").Replace("\r", "\\r");
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void WriteToSession(string txtWrite)
        {
            try
            {
                string textToWrite = this.ReplaceCommonEscapeSequences(txtWrite);
                this._MbSession.RawIO.Write(textToWrite);
            }
            catch(Exception exp)
            {
                _ = MessageBox.Show( exp.Message );
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void OnServiceRequest(object sender, VisaEventArgs e)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    _ = this.Invoke( new Action<object, VisaEventArgs>( this.OnServiceRequest ), new object[] { sender, e } );
                }
                else
                {
                    var mbs = ( IMessageBasedSession ) sender;
                    StatusByteFlags sb = mbs.ReadStatusByte();

                    if ( (sb & StatusByteFlags.MessageAvailable) != 0 )
                    {
                        string textRead = mbs.RawIO.ReadString();
                        this._ReadTextBox.Text = this.InsertCommonEscapeSequences( textRead );
                    }
                    else
                    {
                        _ = MessageBox.Show( "MAV in status register is not set, which means that message is not available. Make sure the command to enable SRQ is correct, and the instrument is 488.2 compatible." );
                    }
                }
            }
            catch(Exception exp)
            {
                _ = MessageBox.Show( exp.Message );
            }
        }
    }
}
