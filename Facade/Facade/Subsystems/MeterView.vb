'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\MeterView.vb
'
' summary:	Meter view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.Facade.DataGridViewExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.Core.Constructs

''' <summary> A DMM meter view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class MeterView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._ToolStripPanel.SuspendLayout()
        Dim topLocation As Integer = 0
        For Each ts As ToolStrip In New ToolStrip() {Me._SubsystemToolStrip, Me._ReadingToolStrip, Me._FunctionConfigurationToolStrip, Me._FilterToolStrip, Me._InfoToolStrip}
            ts.Dock = DockStyle.None
            ts.Location = New Drawing.Point(0, topLocation)
            topLocation += ts.Height
        Next
        Me._ToolStripPanel.ResumeLayout()
        Me.InitializingComponents = False

        Me._ApertureNumeric.NumericUpDownControl.Increment = 0.1D
        Me._ApertureNumeric.NumericUpDownControl.Minimum = 0.001D
        Me._ApertureNumeric.NumericUpDownControl.Maximum = 25
        Me._ApertureNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._ApertureNumeric.NumericUpDownControl.Value = 1

        Me._SenseRangeNumeric.NumericUpDownControl.Increment = 1D
        Me._SenseRangeNumeric.NumericUpDownControl.Minimum = 0D
        Me._SenseRangeNumeric.NumericUpDownControl.Maximum = 1010
        Me._SenseRangeNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._SenseRangeNumeric.NumericUpDownControl.Value = 0.105D
        Me._SenseRangeNumeric.NumericUpDownControl.MaximumSize = New Drawing.Size(Me._SenseRangeNumeric.Size.Width + Me._SenseRangeNumeric.Size.Width, Me._SenseRangeNumeric.Size.Height + 2)

        Me._FilterCountNumeric.NumericUpDownControl.Increment = 1D
        Me._FilterCountNumeric.NumericUpDownControl.Minimum = 0D
        Me._FilterCountNumeric.NumericUpDownControl.Maximum = 100
        Me._FilterCountNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._FilterCountNumeric.NumericUpDownControl.Value = 1D

        Me._FilterWindowNumeric.NumericUpDownControl.Increment = 1D
        Me._FilterWindowNumeric.NumericUpDownControl.Minimum = 0D
        Me._FilterWindowNumeric.NumericUpDownControl.Maximum = 10
        Me._FilterWindowNumeric.NumericUpDownControl.DecimalPlaces = 2
        Me._FilterWindowNumeric.NumericUpDownControl.Value = 10D

        Me._ResolutionDigitsNumeric.NumericUpDownControl.Increment = 1D
        Me._ResolutionDigitsNumeric.NumericUpDownControl.Minimum = 4D
        Me._ResolutionDigitsNumeric.NumericUpDownControl.Maximum = 9D
        Me._ResolutionDigitsNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._ResolutionDigitsNumeric.NumericUpDownControl.Value = 7D

    End Sub

    ''' <summary> Creates a new <see cref="MeterView"/> </summary>
    ''' <returns> A <see cref="MeterView"/>. </returns>
    Public Shared Function Create() As MeterView
        Dim view As MeterView = Nothing
        Try
            view = New MeterView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Gets or sets the supports measurement events. </summary>
    ''' <value> The supports measurement events. </value>
    Public Property SupportsMeasurementEvents As Boolean
        Get
            Return Me._MeasureOptionsMenuItem.Enabled
        End Get
        Set(value As Boolean)
            If value <> Me.SupportsMeasurementEvents Then
                Me._MeasureOptionsMenuItem.Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DataGridView As DataGridView
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets or set the data grid view. </summary>
    ''' <value> The data grid view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DataGridView As DataGridView
        Get
            Return Me._DataGridView
        End Get
        Set(value As DataGridView)
            Me._DataGridView = value
        End Set
    End Property

    ''' <summary> Gets or sets the selected multimeter subsystem function mode. </summary>
    ''' <value> The selected multimeter function mode. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedMultimeterFunctionMode() As VI.MultimeterFunctionModes
        Get
            Return Me._SenseFunctionComboBox.SelectedEnumValue(MultimeterFunctionModes.CurrentAC)
        End Get
        Set(value As VI.MultimeterFunctionModes)
            If value <> Me.SelectedMultimeterFunctionMode AndAlso Me._SenseFunctionComboBox.ComboBox.Items.Count > 0 Then
                Me.InitializingComponents = Not If(Me.Device?.IsDeviceOpen, False, True)
                Me._SenseFunctionComboBox.SelectValue(value)
                Me.NotifyPropertyChanged()
                Me.InitializingComponents = False
            End If
        End Set
    End Property

    ''' <summary> Gets the selected sense function mode. </summary>
    ''' <value> The selected sense function mode. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SelectedSenseFunctionMode() As VI.SenseFunctionModes
        Get
            Return Me._SenseFunctionComboBox.SelectedEnumValue(SenseFunctionModes.CurrentAC)
        End Get
    End Property

    ''' <summary> Gets or sets the name of the subsystem. </summary>
    ''' <value> The name of the subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SubsystemName As String
        Get
            Return Me._ReadSplitButton.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SubsystemName) Then
                Me._ReadSplitButton.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the function mode. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplyFunctionMode()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying {Me.SubsystemName} function mode" : Me.PublishInfo($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing AndAlso
                Not Nullable.Equals(Me.SelectedMultimeterFunctionMode, Me.MultimeterSubsystem.FunctionMode) Then
                Me.MultimeterSubsystem.StartElapsedStopwatch()
                Me.MultimeterSubsystem.ApplyFunctionMode(Me.SelectedMultimeterFunctionMode)
                Me.MultimeterSubsystem.StopElapsedStopwatch()
            ElseIf Me.SenseSubsystem IsNot Nothing Then
                If Me.SelectedSenseFunctionMode <> Me.SenseSubsystem.FunctionMode.Value Then
                    Me.SenseSubsystem.StartElapsedStopwatch()
                    Me.SenseSubsystem.ApplyFunctionMode(Me.SelectedSenseFunctionMode)
                    Me.SenseSubsystem.StopElapsedStopwatch()
                ElseIf Me.SenseFunctionSubsystem Is Nothing OrElse
                    Not Nullable.Equals(Me.SenseFunctionSubsystem.FunctionMode, Me.SenseSubsystem.FunctionMode) Then
                    Me.HandleFunctionModesChanged(Me.SenseSubsystem)
                End If
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads function mode. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadFunctionMode()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} Reading {Me.SubsystemName} function mode" : Me.PublishInfo($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                Me.MultimeterSubsystem.StartElapsedStopwatch()
                Me.MultimeterSubsystem.QueryFunctionMode()
                Me.MultimeterSubsystem.StopElapsedStopwatch()
            ElseIf Me.SenseSubsystem IsNot Nothing Then
                Me.SenseSubsystem.StartElapsedStopwatch()
                Me.SenseSubsystem.QueryFunctionMode()
                Me.SenseSubsystem.StopElapsedStopwatch()
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the settings onto the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying {Me.SubsystemName} settings" : Me.PublishInfo($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                If Me.SelectedMultimeterFunctionMode <> Me.MultimeterSubsystem.FunctionMode.Value Then
                    Me.InfoProvider.Annunciate(Me._ApplyFunctionModeButton, Core.Forma.InfoProviderLevel.Info, "Set function first")
                Else
                    Me.ApplySettings(Me.MultimeterSubsystem)
                End If
            ElseIf Me.SenseSubsystem IsNot Nothing Then
                If Me.SelectedSenseFunctionMode <> Me.SenseFunctionSubsystem.FunctionMode.Value Then
                    Me.InfoProvider.Annunciate(Me._ApplyFunctionModeButton, Core.Forma.InfoProviderLevel.Info, "Set function first")
                Else
                    Me.ApplySettings(Me.SenseFunctionSubsystem)
                End If
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading {Me.SubsystemName} settings" : Me.PublishInfo($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                If Me.MultimeterSubsystem Is Nothing OrElse Me.MultimeterSubsystem.FunctionMode Is Nothing OrElse
                    Me.SelectedMultimeterFunctionMode <> Me.MultimeterSubsystem.FunctionMode.Value Then
                    Me.InfoProvider.Annunciate(Me._ApplyFunctionModeButton, Core.Forma.InfoProviderLevel.Info, "Set function first")
                Else
                    MeterView.ReadSettings(Me.MultimeterSubsystem)
                    Me.ApplyPropertyChanged(Me.MultimeterSubsystem)
                End If
            ElseIf Me.SenseSubsystem IsNot Nothing Then
                ' this should select the sense function subsystem.
                MeterView.ReadSettings(Me.SenseSubsystem)
                Me.ApplyPropertyChanged(Me.SenseSubsystem)
                MeterView.ReadSettings(Me.FormatSubsystem)
                Me.ApplyPropertyChanged(Me.FormatSubsystem)
                MeterView.ReadSettings(Me.SystemSubsystem)
                Me.ApplyPropertyChanged(Me.SystemSubsystem)
                If Me.SenseFunctionSubsystem Is Nothing OrElse Me.SenseFunctionSubsystem.FunctionMode Is Nothing OrElse
                    Me.SelectedSenseFunctionMode <> Me.SenseFunctionSubsystem.FunctionMode.Value Then
                    Me.HandleFunctionModesChanged(Me.SenseSubsystem)
                Else
                    MeterView.ReadSettings(Me.SenseFunctionSubsystem)
                    Me.ApplyPropertyChanged(Me.SenseFunctionSubsystem)
                End If
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads terminal state. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadTerminalState()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} Reading {Me.SubsystemName} terminals state" : Me.PublishInfo($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                Me.MultimeterSubsystem.QueryFrontTerminalsSelected()
            ElseIf Me.SystemSubsystem IsNot Nothing Then
                Me.SystemSubsystem.QueryFrontTerminalsSelected()
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Measure value. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub MeasureValue()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} querying terminal mode" : Me.PublishVerbose($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                Me.MultimeterSubsystem.QueryFrontTerminalsSelected()
            ElseIf Me.SystemSubsystem IsNot Nothing Then
                Me.SystemSubsystem.QueryFrontTerminalsSelected()
            End If
            activity = $"{Me.Device.ResourceNameCaption} measuring" : Me.PublishVerbose($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                MeterView.Read(Me.MultimeterSubsystem)
            Else
                MeterView.Read(Me.MeasureSubsystem)
            End If
            Me.Device.StatusSubsystemBase.QueryMeasurementEventStatus()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the type of the selected reading element. </summary>
    ''' <value> The type of the selected reading element. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedReadingElementType() As VI.ReadingElementTypes
        Get
            Return Me._ReadingElementTypesComboBox.SelectedEnumValue(VI.ReadingElementTypes.Reading)
        End Get
        Set(value As VI.ReadingElementTypes)
            If value <> Me.SelectedReadingElementType AndAlso Me._ReadingElementTypesComboBox.ComboBox.Items.Count > 0 Then
                Me._ReadingElementTypesComboBox.SelectValue(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a the types of reading elements. </summary>
    ''' <value> A list of types of the reading elements. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ReadingElementTypes As ReadingElementTypes

    ''' <summary> Reading element types setter. </summary>
    ''' <param name="candidateReadingElementTypes"> The candidates of reading element type. </param>
    Public Sub ReadingElementTypesSetter(ByVal candidateReadingElementTypes As VI.ReadingElementTypes)
        If candidateReadingElementTypes <> Me.ReadingElementTypes Then
            Me._ReadingElementTypes = candidateReadingElementTypes And Not VI.ReadingElementTypes.Units
            Try
                Me.InitializingComponents = True
                Me._ReadingElementTypesComboBox.ComboBox.ListEnumDescriptions(Of ReadingElementTypes)(candidateReadingElementTypes, VI.ReadingElementTypes.Units)
            Catch
                Throw
            Finally
                Me.InitializingComponents = False
            End Try
            Me.NotifyPropertyChanged(NameOf(MeterView.ReadingElementTypes))
        End If
    End Sub

    ''' <summary> Select active reading. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub SelectActiveReading()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} selecting a reading to display"
            Me.PublishInfo($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                Me.MultimeterSubsystem.SelectActiveReading(Me.SelectedReadingElementType)
            ElseIf Me.MeasureSubsystem IsNot Nothing Then
                Me.MeasureSubsystem.SelectActiveReading(Me.SelectedReadingElementType)
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(MeterView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS: BUFFER DISPLAY "

    ''' <summary> Gets or sets the buffer readings. </summary>
    ''' <value> The buffer readings. </value>
    Private ReadOnly Property BufferReadings As New VI.BufferReadingBindingList

    ''' <summary> Clears the buffer display. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearBufferDisplay()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing buffer display" : Me.PublishVerbose($"{activity};. ")
            If Me.BufferReadings Is Nothing Then
                Me._BufferReadings = New VI.BufferReadingBindingList()
                Me.DataGridView?.Bind(Me.BufferReadings, True)
            End If
            Me.BufferReadings.Clear()
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " BUFFER SUBSYSTEM "

    ''' <summary> Gets or sets the Buffer subsystem. </summary>
    ''' <value> The Buffer subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BufferSubsystem As BufferSubsystemBase

    ''' <summary> Bind Buffer subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As BufferSubsystemBase)
        If Me.BufferSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.BufferSubsystem)
            Me._BufferSubsystem = Nothing
        End If
        Me._BufferSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.BufferSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As BufferSubsystemBase)
        If add Then Me.DataGridView?.Bind(subsystem.BufferReadingsBindingList, True)
    End Sub

    ''' <summary> Reads a buffer. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadBuffer(ByVal subsystem As BufferSubsystemBase)
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            subsystem.StartElapsedStopwatch()
            Me.ClearBufferDisplay()
            Me.BufferReadings.Add(subsystem.QueryBufferReadings)
            subsystem.LastReading = Me.BufferReadings.LastReading
            subsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " FORMAT SUBSYSTEM "

    ''' <summary> Gets or sets the Format subsystem. </summary>
    ''' <value> The Format subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property FormatSubsystem As VI.FormatSubsystemBase

    ''' <summary> Bind format subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.FormatSubsystemBase)
        If Me.FormatSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.FormatSubsystem)
            Me._FormatSubsystem = Nothing
        End If
        Me._FormatSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.FormatSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As FormatSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            MeterView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As FormatSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(FormatSubsystemBase.Elements))
    End Sub

    ''' <summary> Reads the selected measurements settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As FormatSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryElements()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the format subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As FormatSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            ' removed: Elements are set from either measure  or multimeter subsystem
            ' Case NameOf(FormatSubsystemBase.Elements)
            ' Me.ReadingElementTypesSetter(subsystem.Elements)
        End Select
    End Sub

    ''' <summary> Format subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FormatSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(FormatSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.FormatSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.FormatSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, FormatSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MEASURE SUBSYSTEM "

    ''' <summary> Gets or sets the Measure subsystem. </summary>
    ''' <value> The Measure subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property MeasureSubsystem As VI.MeasureSubsystemBase

    ''' <summary> Bind Measure subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.MeasureSubsystemBase)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        Me._MeasureSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.MeasureSubsystem)
            Me.HandlePropertyChanged(subsystem, NameOf(MeasureSubsystemBase.ReadingElementTypes))
            Me.SelectActiveReading()
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Measure subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MeasureSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(MeasureSubsystemBase.PrimaryReadingValue)
            Case NameOf(MeasureSubsystemBase.ReadingElementTypes)
                Me.ReadingElementTypesSetter(subsystem.ReadingElementTypes)
        End Select
    End Sub

    ''' <summary> Measure subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inMeasureion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(MeasureSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, MeasureSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Reads the given subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub Read(ByVal subsystem As MeasureSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.Read()
        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " MUTLIMETER SUBSYSTEM "

    ''' <summary> Gets or sets the Multimeter subsystem. </summary>
    ''' <value> The Multimeter subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MultimeterSubsystem As VI.MultimeterSubsystemBase

    ''' <summary> Bind Multimeter subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As MultimeterSubsystemBase)
        If Me.MultimeterSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MultimeterSubsystem)
            Me._MultimeterSubsystem = Nothing
        End If
        Me._MultimeterSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.MultimeterSubsystem)
        End If
        Me._ResolutionDigitsNumeric.Visible = False
        Me.SubsystemName = "DMM"
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MultimeterSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MultimeterSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.SupportedFunctionModes))
            Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.ReadingElementTypes))
            MeterView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
            Me.SelectActiveReading()
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MultimeterSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As MultimeterSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FilterCountRange))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FilterWindowRange))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FunctionRange))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FunctionRangeDecimalPlaces))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.PowerLineCyclesRange))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.PowerLineCyclesDecimalPlaces))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.AutoDelayMode))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FunctionMode))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.AutoDelayEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.AutoRangeEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.AutoZeroEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FilterCount))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FilterEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FilterWindow))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.MovingAverageFilterEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FrontTerminalsSelected))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.FunctionUnit))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.OpenDetectorEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.PowerLineCycles))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.Range))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.ReadingAmounts))
        Me.HandlePropertyChanged(subsystem, NameOf(MultimeterSubsystemBase.ReadingElementTypes))
    End Sub

    ''' <summary> Reads the selected measurements settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As MultimeterSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFunctionMode()
        subsystem.QueryAutoDelayMode()
        subsystem.QueryAutoRangeEnabled()
        subsystem.QueryAutoZeroEnabled()
        subsystem.QueryFilterCount()
        subsystem.QueryFilterEnabled()
        subsystem.QueryFilterWindow()
        subsystem.QueryMovingAverageFilterEnabled()
        subsystem.QueryFrontTerminalsSelected()
        subsystem.QueryOpenDetectorEnabled()
        subsystem.QueryMultimeterMeasurementUnit()
        subsystem.QueryOpenDetectorEnabled()
        subsystem.QueryPowerLineCycles()
        subsystem.QueryRange()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handles the Multimeter subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MultimeterSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName

            Case NameOf(MultimeterSubsystemBase.AutoDelayMode)
                ' TO_DO: Handle auto delay mode; might be just on or off.
                ' If subsystem.AutoDelayMode.HasValue Then Me.AutoDelayMode = subsystem.AutoDelayMode.Value
                ' 
            Case NameOf(VI.MultimeterSubsystemBase.AutoDelayEnabled)
                Me._AutoDelayToggleButton.CheckState = subsystem.AutoDelayEnabled.ToCheckState

            Case NameOf(MultimeterSubsystemBase.AutoRangeEnabled)
                Me._AutoRangeToggleButton.CheckState = subsystem.AutoRangeEnabled.ToCheckState

            Case NameOf(MultimeterSubsystemBase.AutoZeroEnabled)
                Me._AutoZeroToggleButton.CheckState = subsystem.AutoZeroEnabled.ToCheckState

            Case NameOf(MultimeterSubsystemBase.FilterCount)
                If subsystem.FilterCount.HasValue Then Me._FilterCountNumeric.Value = subsystem.FilterCount.Value

            Case NameOf(MultimeterSubsystemBase.FilterCountRange)
                Me._FilterCountNumeric.NumericUpDownControl.RangeSetter(subsystem.FilterCountRange.Min, subsystem.FilterCountRange.Max)
                Me._FilterCountNumeric.NumericUpDownControl.DecimalPlaces = 0

            Case NameOf(MultimeterSubsystemBase.FilterEnabled)
                Me._FilterEnabledToggleButton.CheckState = subsystem.FilterEnabled.ToCheckState

            Case NameOf(MultimeterSubsystemBase.FilterWindow)
                If subsystem.FilterWindow.HasValue Then Me._FilterWindowNumeric.Value = CDec(100 * subsystem.FilterWindow.Value)

            Case NameOf(MultimeterSubsystemBase.FilterWindowRange)
                Dim range As RangeR = subsystem.FilterWindowRange.TransposedRange(0, 100)
                Me._FilterWindowNumeric.NumericUpDownControl.RangeSetter(range.Min, range.Max)
                Me._FilterWindowNumeric.NumericUpDownControl.DecimalPlaces = 0

            Case NameOf(MultimeterSubsystemBase.MovingAverageFilterEnabled)
                Me._WindowTypeToggleButton.CheckState = subsystem.MovingAverageFilterEnabled.ToCheckState

            Case NameOf(MultimeterSubsystemBase.FrontTerminalsSelected)
                ' To_Do: bind front terminal state to a status label with visible if has value and F, R: use binding formating functions
                ' Me._ReadTerminalStateButton.Checked = subsystem.FrontTerminalsSelected.GetValueOrDefault(False)
                Me._TerminalStateReadButton.CheckState = subsystem.FrontTerminalsSelected.ToCheckState

            Case NameOf(MultimeterSubsystemBase.FunctionMode)
                Me.SelectedMultimeterFunctionMode = subsystem.FunctionMode.GetValueOrDefault(VI.MultimeterFunctionModes.VoltageDC)

            Case NameOf(MultimeterSubsystemBase.FunctionRange)
                Me._SenseRangeNumeric.NumericUpDownControl.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)

            Case NameOf(MultimeterSubsystemBase.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.NumericUpDownControl.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces

            Case NameOf(MultimeterSubsystemBase.FunctionUnit)
                Me._FunctionLabel.Text = subsystem.FunctionUnit.ToString

            Case NameOf(MultimeterSubsystemBase.OpenDetectorEnabled)
                Me._OpenDetectorToggleButton.CheckState = subsystem.OpenDetectorEnabled.ToCheckState

            Case NameOf(MultimeterSubsystemBase.PowerLineCycles)
                If subsystem.PowerLineCycles.HasValue Then Me._ApertureNumeric.Value = CDec(subsystem.PowerLineCycles.Value)

            Case NameOf(MultimeterSubsystemBase.PowerLineCyclesRange)
                Me._ApertureNumeric.NumericUpDownControl.RangeSetter(subsystem.PowerLineCyclesRange.Min, subsystem.PowerLineCyclesRange.Max)
                Me._ApertureNumeric.NumericUpDownControl.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces

            Case NameOf(MultimeterSubsystemBase.PowerLineCyclesDecimalPlaces)
                Me._ApertureNumeric.NumericUpDownControl.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces

            Case NameOf(MultimeterSubsystemBase.Range)
                If subsystem.Range.HasValue Then Me._SenseRangeNumeric.NumericUpDownControl.ValueSetter(subsystem.Range.Value)

            Case NameOf(MultimeterSubsystemBase.ReadingAmounts)

            Case NameOf(MultimeterSubsystemBase.ReadingElementTypes)
                Me.ReadingElementTypesSetter(subsystem.ReadingElementTypes)

            Case NameOf(MultimeterSubsystemBase.SupportedFunctionModes)
                Dim init As Boolean = Me.InitializingComponents
                Me.InitializingComponents = True
                Me._SenseFunctionComboBox.ComboBox.ListEnumDescriptions(subsystem.SupportedFunctionModes, Not subsystem.SupportedFunctionModes)
                Me.InitializingComponents = False
                Me.SelectedMultimeterFunctionMode = subsystem.FunctionMode.GetValueOrDefault(VI.MultimeterFunctionModes.VoltageDC)

        End Select
    End Sub

    ''' <summary> Multimeter subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MultimeterSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MultimeterSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                activity = $"invoking {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MultimeterSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, MultimeterSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Applies the selected measurements settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplySettings(ByVal subsystem As MultimeterSubsystemBase)
        subsystem.StartElapsedStopwatch()
        If Not Nullable.Equals(subsystem.PowerLineCycles, Me._ApertureNumeric.Value) Then
            subsystem.ApplyPowerLineCycles(Me._ApertureNumeric.Value)
        End If

        ' If Not Nullable.Equals(.AutoDelayMode, Me._AutoDelayMode) Then
        '     .ApplyAutoDelayMode(Me._AutoDelayMode)
        ' End If

        If Not Nullable.Equals(subsystem.AutoRangeEnabled, Me._AutoRangeToggleButton.Checked) Then
            subsystem.ApplyAutoRangeEnabled(Me._AutoRangeToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.AutoZeroEnabled, Me._AutoZeroToggleButton.Checked) Then
            subsystem.ApplyAutoZeroEnabled(Me._AutoZeroToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.FilterEnabled, Me._FilterEnabledToggleButton.Checked) Then
            subsystem.ApplyFilterEnabled(Me._FilterEnabledToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.FilterCount, Me._FilterCountNumeric.Value) Then
            subsystem.ApplyFilterCount(CInt(Me._FilterCountNumeric.Value))
        End If

        If Not Nullable.Equals(subsystem.MovingAverageFilterEnabled, Me._WindowTypeToggleButton.Checked) Then
            subsystem.ApplyMovingAverageFilterEnabled(Me._WindowTypeToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.OpenDetectorEnabled, Me._OpenDetectorToggleButton.Checked) Then
            subsystem.ApplyOpenDetectorEnabled(Me._OpenDetectorToggleButton.Checked)
        End If

        Me.MultimeterSubsystem.QueryMultimeterMeasurementUnit()
        Me.MultimeterSubsystem.QueryOpenDetectorEnabled()

        If subsystem.AutoRangeEnabled Then
            subsystem.QueryRange()
        ElseIf Not Nullable.Equals(subsystem.Range, Me._SenseRangeNumeric.Value) Then
            subsystem.ApplyRange(CInt(Me._SenseRangeNumeric.Value))
        End If


        If Not Nullable.Equals(subsystem.FilterWindow, 0.01 * Me._FilterWindowNumeric.Value) Then
            subsystem.ApplyFilterWindow(0.01 * Me._FilterWindowNumeric.Value)
        End If
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the given subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub Read(ByVal subsystem As MultimeterSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.MeasurePrimaryReading()
        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " SENSE SUBSYSTEM "

    ''' <summary> Gets or sets the Sense subsystem. </summary>
    ''' <value> The Sense subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseSubsystem As VI.SenseSubsystemBase

    ''' <summary> Bind Sense subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As SenseSubsystemBase)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        Me._SenseSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.SenseSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(SenseSubsystemBase.SupportedFunctionModes))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseSubsystemBase.FunctionRange))
            MeterView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.BindSubsystem(CType(Nothing, SenseFunctionSubsystemBase), "DMM")
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As SenseSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(SenseSubsystemBase.FunctionMode))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseSubsystemBase.FunctionRange))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseSubsystemBase.FunctionRangeDecimalPlaces))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseSubsystemBase.FunctionUnit))
    End Sub

    ''' <summary> Reads the selected measurements settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As SenseSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFunctionMode()
        'subsystem.QueryAutoRangeEnabled()
        'subsystem.QueryPowerLineCycles()
        'subsystem.QueryRange()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub HandleFunctionModesChanged(ByVal subsystem As SenseSubsystemBase)
        If subsystem IsNot Nothing AndAlso subsystem.FunctionMode.HasValue Then
            Dim value As VI.SenseFunctionModes = subsystem.FunctionMode.Value
            If value <> VI.SenseFunctionModes.None Then
                Dim valueChanged As Boolean = Me.SelectedSenseFunctionMode <> subsystem.FunctionMode.Value
                ' this is done at the device level by applying the function mode to the measure subsystem
                ' Me.MeasureSubsystem.Readings.Reading.ApplyUnit(subsystem.ToUnit(value))
                Me._SenseFunctionComboBox.SelectValue(subsystem.FunctionMode.Value)
                Me.NotifyPropertyChanged(NameOf(MeterView.SelectedSenseFunctionMode))
            End If
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SenseSubsystemBase, ByVal propertyName As String)
        If Me.InitializingComponents OrElse subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(SenseSubsystemBase.SupportedFunctionModes)
                Dim init As Boolean = Me.InitializingComponents
                Me.InitializingComponents = True
                Me._SenseFunctionComboBox.ComboBox.ListEnumDescriptions(subsystem.SupportedFunctionModes, Not subsystem.SupportedFunctionModes)
                Me.InitializingComponents = init
            Case NameOf(SenseSubsystemBase.FunctionMode)
                Me.HandleFunctionModesChanged(subsystem)
            Case NameOf(SenseSubsystemBase.FunctionRange)
                Me._SenseRangeNumeric.NumericUpDownControl.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)
            Case NameOf(SenseSubsystemBase.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.NumericUpDownControl.DecimalPlaces = subsystem.FunctionRangeDecimalPlaces
            Case NameOf(SenseSubsystemBase.FunctionUnit)
                Me._FunctionLabel.Text = subsystem.FunctionUnit.ToString
        End Select
    End Sub

    ''' <summary> Sense subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE FUNCTION SUBSYSTEM "

    ''' <summary> Gets or sets the sense function subsystem. </summary>
    ''' <value> The sense function subsystem. </value>
    Public ReadOnly Property SenseFunctionSubsystem As VI.SenseFunctionSubsystemBase

    ''' <summary> Bind Sense function subsystem. </summary>
    ''' <param name="subsystem">     The subsystem. </param>
    ''' <param name="subsystemName"> The name of the subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.SenseFunctionSubsystemBase, ByVal subsystemName As String)
        If Me.SenseFunctionSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseFunctionSubsystem)
            Me._SenseFunctionSubsystem = Nothing
        End If
        Me._SenseFunctionSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.SenseFunctionSubsystem)
            Me._AutoZeroToggleButton.Visible = subsystem.SupportsAutoZero
            Me._AutoDelayToggleButton.Visible = subsystem.SupportsAutoDelay
            Me._OpenDetectorToggleButton.Visible = subsystem.SupportsOpenLeadDetector
        End If
        Me.SubsystemName = If(String.IsNullOrEmpty(subsystemName), "DMM", subsystemName)
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseFunctionSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseFunctionSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AverageCountRange))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AveragePercentWindowRange))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.FunctionRangeDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.PowerLineCyclesRange))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.PowerLineCyclesDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.ResolutionDigitsRange))
            MeterView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseFunctionSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As SenseFunctionSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.FunctionMode))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AutoRangeEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AutoZeroEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AverageCount))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AverageEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AverageFilterType))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.AveragePercentWindow))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.FunctionUnit))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.PowerLineCycles))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.Range))
        Me.HandlePropertyChanged(subsystem, NameOf(SenseFunctionSubsystemBase.ResolutionDigits))
    End Sub

    ''' <summary> Reads the selected measurements settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As SenseFunctionSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFunctionMode()
        subsystem.QueryAutoRangeEnabled()
        subsystem.QueryAutoZeroEnabled()
        subsystem.QueryAverageCount()
        subsystem.QueryAverageEnabled()
        subsystem.QueryAverageFilterType()
        subsystem.QueryAveragePercentWindow()
        subsystem.QueryPowerLineCycles()
        subsystem.QueryRange()
        subsystem.QueryResolutionDigits()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.SenseFunctionSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName

            Case NameOf(SenseFunctionSubsystemBase.AutoRangeEnabled)
                Me._AutoRangeToggleButton.CheckState = subsystem.AutoRangeEnabled.ToCheckState

            Case NameOf(SenseFunctionSubsystemBase.AutoZeroEnabled)
                Me._AutoZeroToggleButton.CheckState = subsystem.AutoZeroEnabled.ToCheckState

            Case NameOf(SenseFunctionSubsystemBase.AverageCount)
                If subsystem.AverageCount.HasValue Then Me._FilterCountNumeric.Value = subsystem.AverageCount.Value

            Case NameOf(SenseFunctionSubsystemBase.AverageCountRange)
                Me._FilterCountNumeric.NumericUpDownControl.RangeSetter(subsystem.AverageCountRange.Min, subsystem.AverageCountRange.Max)
                Me._FilterCountNumeric.NumericUpDownControl.DecimalPlaces = 0

            Case NameOf(SenseFunctionSubsystemBase.AverageEnabled)
                Me._FilterEnabledToggleButton.CheckState = subsystem.AverageEnabled.ToCheckState

            Case NameOf(SenseFunctionSubsystemBase.AveragePercentWindow)
                If subsystem.AveragePercentWindow.HasValue Then Me._FilterWindowNumeric.Value = CDec(subsystem.AveragePercentWindow.Value)

            Case NameOf(SenseFunctionSubsystemBase.AveragePercentWindowRange)
                Dim range As RangeR = subsystem.AveragePercentWindowRange.TransposedRange(0, 100)
                Me._FilterWindowNumeric.NumericUpDownControl.RangeSetter(range.Min, range.Max)
                Me._FilterWindowNumeric.NumericUpDownControl.DecimalPlaces = 0

            Case NameOf(SenseFunctionSubsystemBase.AverageFilterType)
                Me._WindowTypeToggleButton.CheckState = If(subsystem.AverageFilterType.HasValue,
                                                                If(subsystem.AverageFilterType.Value = AverageFilterTypes.Moving, CheckState.Checked, CheckState.Unchecked),
                                                           CheckState.Indeterminate)

            Case NameOf(SenseFunctionSubsystemBase.FunctionRange)
                Me._SenseRangeNumeric.NumericUpDownControl.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)

            Case NameOf(SenseFunctionSubsystemBase.FunctionRangeDecimalPlaces)
                Dim range As Double = subsystem.Range.GetValueOrDefault(0.105)
                Me._SenseRangeNumeric.NumericUpDownControl.DecimalPlaces = CInt(Math.Max(subsystem.FunctionRangeDecimalPlaces, Math.Min(0, subsystem.FunctionRangeDecimalPlaces - Math.Log(range))))

            Case NameOf(SenseFunctionSubsystemBase.FunctionUnit)
                Me._FunctionLabel.Text = subsystem.FunctionUnit.ToString

            Case NameOf(SenseFunctionSubsystemBase.PowerLineCycles)
                If subsystem.PowerLineCycles.HasValue Then Me._ApertureNumeric.Value = CDec(subsystem.PowerLineCycles.Value)

            Case NameOf(SenseFunctionSubsystemBase.PowerLineCyclesRange)
                Me._ApertureNumeric.NumericUpDownControl.RangeSetter(subsystem.PowerLineCyclesRange.Min, subsystem.PowerLineCyclesRange.Max)
                Me._ApertureNumeric.NumericUpDownControl.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces

            Case NameOf(SenseFunctionSubsystemBase.PowerLineCyclesDecimalPlaces)
                Me._ApertureNumeric.NumericUpDownControl.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces

            Case NameOf(SenseFunctionSubsystemBase.Range)
                If subsystem.Range.HasValue Then Me._SenseRangeNumeric.NumericUpDownControl.ValueSetter(subsystem.Range.Value)

            Case NameOf(SenseFunctionSubsystemBase.ResolutionDigits)
                If subsystem.ResolutionDigits.HasValue Then Me._ResolutionDigitsNumeric.NumericUpDownControl.Value = CDec(subsystem.ResolutionDigits.Value)

            Case NameOf(SenseFunctionSubsystemBase.ResolutionDigitsRange)
                Me._ResolutionDigitsNumeric.NumericUpDownControl.RangeSetter(subsystem.ResolutionDigitsRange.Min, subsystem.ResolutionDigitsRange.Max)
                Me._ResolutionDigitsNumeric.NumericUpDownControl.DecimalPlaces = 0

        End Select
    End Sub

    ''' <summary> Sense function subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseFunctionSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.SenseFunctionSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseFunctionSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseFunctionSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SenseFunctionSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Applies the selected measurements settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplySettings(ByVal subsystem As SenseFunctionSubsystemBase)
        subsystem.StartElapsedStopwatch()
        If Not Nullable.Equals(subsystem.PowerLineCycles, Me._ApertureNumeric.Value) Then
            subsystem.ApplyPowerLineCycles(Me._ApertureNumeric.Value)
        End If

        ' If Not Nullable.Equals(.subsystemAutoDelayMode, Me._AutoDelayMode) Then
        '     subsystem.ApplyAutoDelayMode(Me._AutoDelayMode)
        ' End If

        If Not Nullable.Equals(subsystem.AutoRangeEnabled, Me._AutoRangeToggleButton.Checked) Then
            subsystem.ApplyAutoRangeEnabled(Me._AutoRangeToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.AutoZeroEnabled, Me._AutoZeroToggleButton.Checked) Then
            subsystem.ApplyAutoZeroEnabled(Me._AutoZeroToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.AverageEnabled, Me._FilterEnabledToggleButton.Checked) Then
            subsystem.ApplyAverageEnabled(Me._FilterEnabledToggleButton.Checked)
        End If

        If Not Nullable.Equals(subsystem.AverageCount, Me._FilterCountNumeric.Value) Then
            subsystem.ApplyAverageCount(CInt(Me._FilterCountNumeric.Value))
        End If

        Dim filterType As AverageFilterTypes = If(Me._WindowTypeToggleButton.CheckState = CheckState.Checked, AverageFilterTypes.Moving,
                                                        If(Me._WindowTypeToggleButton.CheckState = CheckState.Unchecked, AverageFilterTypes.Repeat, AverageFilterTypes.None))
        If subsystem.AverageFilterType <> filterType AndAlso filterType <> AverageFilterTypes.None Then
            subsystem.ApplyAverageFilterType(filterType)
        End If

        If subsystem.AutoRangeEnabled Then
            subsystem.QueryRange()
        ElseIf Not Nullable.Equals(subsystem.Range, Me._SenseRangeNumeric.Value) Then
            subsystem.ApplyRange(CInt(Me._SenseRangeNumeric.Value))
        End If

        If Not Nullable.Equals(subsystem.AveragePercentWindow, Me._FilterWindowNumeric.Value) Then
            subsystem.ApplyAveragePercentWindow(Me._FilterWindowNumeric.Value)
        End If

        If Not Nullable.Equals(subsystem.ResolutionDigits, Me._ResolutionDigitsNumeric.Value) Then
            subsystem.ApplyResolutionDigits(Me._ResolutionDigitsNumeric.Value)
        End If

        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " SYSTEM SUBSYSTEM "

    ''' <summary> Gets or sets the sense function subsystem. </summary>
    ''' <value> The sense function subsystem. </value>
    Public ReadOnly Property SystemSubsystem As VI.SystemSubsystemBase

    ''' <summary> Bind system subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.SystemSubsystemBase)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.SystemSubsystem)
        End If
        Me._AutoDelayToggleButton.Visible = False
        Me._OpenDetectorToggleButton.Visible = False
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SystemSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            MeterView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As SystemSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(SystemSubsystemBase.FrontTerminalsSelected))
    End Sub

    ''' <summary> Reads the <see cref="SystemSubsystemBase"/> settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As SystemSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFrontTerminalsSelected()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handles the Multimeter subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SystemSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(SystemSubsystemBase.FrontTerminalsSelected)
                Me._TerminalStateReadButton.CheckState = subsystem.FrontTerminalsSelected.ToCheckState
        End Select

    End Sub

    ''' <summary> System subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.SystemSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SystemSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SystemSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SystemSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TRACE SUBSYSTEM "

    ''' <summary> Gets or sets the trace subsystem. </summary>
    ''' <value> The trace subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TraceSubsystem As TraceSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As TraceSubsystemBase)
        If Me.TraceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TraceSubsystem)
            Me._TraceSubsystem = Nothing
        End If
        Me._TraceSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.TraceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TraceSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
            MeterView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As TraceSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.PointsCount))
    End Sub

    ''' <summary> Reads the <see cref="TraceSubsystemBase"/> settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As TraceSubsystemBase)
        subsystem.StartElapsedStopwatch()
        ' subsystem.QueryReadingElementTypes()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the Trace subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TraceSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(TraceSubsystemBase.PointsCount)
        End Select
    End Sub

    ''' <summary> Trace subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inTraceion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TraceSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TraceSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TraceSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TraceSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TRIGGER SUBSYSTEM "

    ''' <summary> Gets or sets the trigger subsystem. </summary>
    ''' <value> The trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As TriggerSubsystemBase)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " MEASUREMENT: EVENT DRIVEN "

    ''' <summary> Gets or sets the trace readings. </summary>
    ''' <value> The trace readings. </value>
    Private ReadOnly Property TraceReadings As List(Of ReadingAmounts)

    ''' <summary> Fetches buffered readings. </summary>
    ''' <remarks> David, 2020-07-27. </remarks>
    ''' <param name="values"> The values. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DisplayBufferedReadings(ByVal values As IList(Of ReadingAmounts))
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} adding buffered readings" : Me.PublishVerbose($"{activity};. ")
            If Me.TraceReadings Is Nothing Then Me._TraceReadings = New List(Of ReadingAmounts)
            Me.TraceReadings.AddRange(values)
            If Me.DataGridView Is Nothing Then
                activity = $"{Me.Device.ResourceNameCaption} data grid view not used in this instance" : Me.PublishVerbose($"{activity};. ")
            Else
                activity = $"{Me.Device.ResourceNameCaption} updating the display" : Me.PublishVerbose($"{activity};. ")
                If Me.TraceReadings.Count = values.Count Then
                    Me.DataGridView.Display(values, False)
                Else
                    Me.DataGridView.Invalidate()
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Fetches and displays buffered readings. </summary>
    Protected Overridable Sub FetchAndDisplayBufferedReadings()
    End Sub

    ''' <summary> Handles the measurement completed request. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementCompletedRequest(ByVal subsystem As MeasureSubsystemBase)
        Dim activity As String = $"{Me.Device.ResourceNameCaption} handling measurement event"
        Try
            Me.PublishVerbose($"{activity};. ")
            Me.PublishVerbose($"{Me.Device.ResourceTitleCaption} SRQ: {Me.Device.Session.ServiceRequestStatus:X};. ")
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.Device.StatusSubsystemBase.HasMeasurementEvent Then
                If Me.TriggerSubsystem.TriggerCount.Value = 1 Then
                    If Me.TraceSubsystem.FeedSource = VI.FeedSources.None Then
                        activity = $"{Me.Device.ResourceNameCaption} fetching a single readings"
                        Me.PublishVerbose($"{activity};. ")
                        subsystem.Fetch()
                    Else
                        Me.FetchAndDisplayBufferedReadings()
                    End If
                Else
                    Me.FetchAndDisplayBufferedReadings()
                End If
                Me.TraceSubsystem.StopElapsedStopwatch()

                If Me._AutoInitiateMenuItem.Checked Then
                    activity = $"{Me.Device.ResourceNameCaption} initiating next measurement(s)"
                    Me.PublishVerbose($"{activity};. ")
                    Me.TraceSubsystem.StartElapsedStopwatch()
                    Me.TraceSubsystem.ClearBuffer() ' ?@#  17-7-6
                    Me.TriggerSubsystem.Initiate()
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} measurement not available--is error?"
                Me.PublishVerbose($"{activity};. ")
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Handles the measurement completed request. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementCompletedRequest(ByVal subsystem As MultimeterSubsystemBase)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} handling SRQ: {Me.Device.Session.ServiceRequestStatus:X}" : Me.PublishVerbose($"{activity};. ")
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} kludge: reading buffer count"
            Me.PublishVerbose($"{activity};. ")

            ' this assume buffer is cleared upon each new cycle
            Dim newBufferCount As Integer = Me.BufferSubsystem.QueryActualPointCount.GetValueOrDefault(0)

            If newBufferCount > 0 Then

                activity = $"{Me.Device.ResourceNameCaption} kludge: buffer has data..."
                Me.PublishVerbose($"{activity};. ")

                activity = $"{Me.Device.ResourceNameCaption} handling measurement available"
                Me.PublishVerbose($"{activity};. ")

                If Me.TriggerSubsystem.TriggerCount = 1 Then
                    activity = $"{Me.Device.ResourceNameCaption} fetching a single reading"
                    Me.PublishVerbose($"{activity};. ")
                    subsystem.MeasurePrimaryReading()
                Else
                    activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
                    Me.PublishVerbose($"{activity};. ")
                    Me.BufferReadings.Add(Me.BufferSubsystem.QueryBufferReadings())

                    activity = $"{Me.Device.ResourceNameCaption} updating the display" : Me.PublishVerbose($"{activity};. ")
                    Me.DataGridView?.Invalidate()
                    Me.BufferSubsystem.StopElapsedStopwatch()
                End If
                If Me._AutoInitiateMenuItem.Checked Then
                    activity = $"{Me.Device.ResourceNameCaption} initiating next measurement(s)" : Me.PublishVerbose($"{activity};. ")
                    Me.BufferSubsystem.StartElapsedStopwatch()
                    Me.BufferSubsystem.ClearBuffer() ' ?@3 removed 17-7-6
                    Me.TriggerSubsystem.Initiate()
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} trigger plan started; buffer empty"
                Me.PublishVerbose($"{activity};. ")
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Handles the measurement completed request. </summary>
    ''' <param name="sender"> <see cref="Object"/>
    '''                                             instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HandleMeasurementCompletedRequest(sender As Object, e As EventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.HandleMeasurementCompletedRequest(Me.MeasureSubsystem)
        ElseIf Me.MultimeterSubsystem IsNot Nothing Then
            Me.HandleMeasurementCompletedRequest(Me.MultimeterSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the measurement complete handler added. </summary>
    ''' <value> The measurement complete handler added. </value>
    Private Property MeasurementCompleteHandlerAdded As Boolean

    ''' <summary> Adds measurement complete event handler. </summary>
    Private Sub AddMeasurementCompleteEventHandler()

        Dim activity As String = String.Empty
        If Not Me.MeasurementCompleteHandlerAdded Then

            ' clear execution state before enabling events
            activity = $"{Me.Device.ResourceNameCaption} Clearing execution state"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} Enabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.EnableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Adding device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.AddServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning on measurement events"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.StatusSubsystemBase.ApplyMeasurementEventEnableBitmask(Me.Device.StatusSubsystemBase.MeasurementEventsBitmasks.All)
            ' 
            ' if handling buffer full, use the 4917 event to detect buffer full. 

            activity = $"{Me.Device.ResourceNameCaption} Turning on status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)

            activity = $"{Me.Device.ResourceNameCaption} Adding re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            AddHandler Me.Device.ServiceRequested, AddressOf Me.HandleMeasurementCompletedRequest
            Me.MeasurementCompleteHandlerAdded = True
        End If
    End Sub

    ''' <summary> Removes the measurement complete event handler. </summary>
    Private Sub RemoveMeasurementCompleteEventHandler()

        Dim activity As String = String.Empty
        If Me.MeasurementCompleteHandlerAdded Then

            activity = $"{Me.Device.ResourceNameCaption} Disabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.DisableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Removing device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.RemoveServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning off measurement events"
            Me.PublishVerbose($"{activity};. ")
            'Me.Device.StatusSubsystemBase.ApplyMeasurementEventEnableBitmask(0)

            activity = $"{Me.Device.ResourceNameCaption} Turning off status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.None)

            activity = $"{Me.Device.ResourceNameCaption} Removing re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.HandleMeasurementCompletedRequest

            Me.MeasurementCompleteHandlerAdded = False

        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by HandleMeasurementEventMenuItem for check state changed events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementEventMenuItem_CheckStateChanged(sender As Object, e As EventArgs)

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishInfo($"{activity};. ")
            Me.AbortTriggerPlan()

            If menuItem.Checked Then

                activity = $"{Me.Device.ResourceNameCaption} Adding measurement completion handler"
                Me.PublishInfo($"{activity};. ")
                Me.AddMeasurementCompleteEventHandler()
                Me._MeasureValueButton.Text = "Initiated"

            Else

                activity = $"{Me.Device.ResourceNameCaption} Removing measurement completion handler"
                Me.PublishInfo($"{activity};. ")
                Me.RemoveMeasurementCompleteEventHandler()
                Me._MeasureValueButton.Text = "Measure"

            End If

        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Aborts trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortTriggerPlan()

        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.Abort()

        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Starts trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartTriggerPlan()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} clearing buffer and display"
            Me.PublishVerbose($"{activity};. ")

            Me.TraceSubsystem.ClearBuffer()

            activity = $"{Me.Device.ResourceNameCaption} initiating single trigger measurements(s)"
            Me.PublishVerbose($"{activity};. ")
            Me.MeasureSubsystem.StartElapsedStopwatch()
            Me.TriggerSubsystem.Initiate()

        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Initiates this object. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Initiate()

        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishInfo($"{activity};. ")
            Me.AbortTriggerPlan()

            activity = $"{Me.Device.ResourceNameCaption} Starting trigger plan"
            Me.PublishInfo($"{activity};. ")
            Me.StartTriggerPlan()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Aborts this object. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Abort()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishInfo($"{activity};. ")
            Me.AbortTriggerPlan()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Applies the function mode button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplyFunctionModeButton_Click(sender As Object, e As EventArgs) Handles _ApplyFunctionModeButton.Click, _ApplyFunctionModeMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplyFunctionMode()
    End Sub

    ''' <summary> Automatic delay toggle button Check State Changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoDelayToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _AutoDelayToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Delay: {button.CheckState.ToCheckStateCaption("auto", "~auto", "?")}"
        End If
    End Sub

    ''' <summary> Automatic range toggle button Check State Changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoRangeToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _AutoRangeToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"{button.CheckState.ToCheckStateCaption(String.Empty, "~", "?")}auto"
        End If
    End Sub

    ''' <summary> Automatic zero toggle button Check State Changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoZeroToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _AutoZeroToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"{button.CheckState.ToCheckStateCaption(String.Empty, "~", "?")}auto zero"
        End If
    End Sub

    ''' <summary> Filter enabled toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FilterEnabledToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _FilterEnabledToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"{button.CheckState.ToCheckStateCaption("on", "off", "?on")}"
        End If
    End Sub

    ''' <summary> Opens detector toggle button Check State Changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDetectorToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _OpenDetectorToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Open Detector: {button.CheckState.ToCheckStateCaption("On", "Off", "?on")}"
        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by _SenseFunctionComboBox for selected index changed events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SenseFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SenseFunctionComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplyFunctionMode()
    End Sub

    ''' <summary> Displays terminal state. </summary>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TerminalStateReadButton_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TerminalStateReadButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Terminals: {button.CheckState.ToCheckStateCaption("Front", "Rear", "?")}"
        End If
    End Sub

    ''' <summary> Terminal state read button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TerminalStateReadButton_Click(sender As Object, e As EventArgs) Handles _TerminalStateReadButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            Me.ReadTerminalState()
        End If
    End Sub

    ''' <summary> Window type toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub WindowTypeToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _WindowTypeToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"{button.CheckState.ToCheckStateCaption("Moving", "Repeating", "?filter")}"
        End If
    End Sub

    ''' <summary> Reads function mode menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadFunctionModeMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadFunctionModeMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ReadFunctionMode()
    End Sub

    ''' <summary> Applies the settings tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Starts stop measure value. </summary>
    Private Sub StartStopMeasureValue()
        If Me._FetchOnMeasurementEventMenuItem.Checked Then
            If Me.TriggerSubsystem.TriggerState = TriggerState.Idle Then
                Me.Initiate()
                Me._MeasureValueButton.Text = "Abort"
            Else
                Me.Abort()
                Me._MeasureValueButton.Text = "Initiate"
            End If
        Else
            Me.MeasureValue()
        End If
    End Sub

    ''' <summary> Measure immediate menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeasureImmediateMenuItem_Click(sender As Object, e As EventArgs) Handles _MeasureImmediateMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.MeasureValue()
    End Sub

    ''' <summary> Measure value button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeasureValueButton_Click(sender As Object, e As EventArgs) Handles _MeasureValueButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.StartStopMeasureValue()
    End Sub

    ''' <summary> Reading combo box selected index changed. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadingElementTypesComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _ReadingElementTypesComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.SelectActiveReading()
    End Sub

    ''' <summary> Handles the DataError event of the _dataGridView control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
    '''                       event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DataGridView_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles _DataGridView.DataError
        Try
            ' prevent error reporting when adding a new row or editing a cell
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid IsNot Nothing Then
                If grid.CurrentRow IsNot Nothing AndAlso grid.CurrentRow.IsNewRow Then Return
                If grid.IsCurrentCellInEditMode Then Return
                If grid.IsCurrentRowDirty Then Return
                Dim activity As String = $"{Me.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString}"

                Me.PublishVerbose(activity)
                Me.InfoProvider.Annunciate(grid, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End If
        Catch
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
