'---------------------------------------------------------------------------------------------------
' file:		Tsp2Tests\K7510\Subsystems_Device.vb
'
' summary:	Subsystems device class
'---------------------------------------------------------------------------------------------------

''' <summary> Static class for managing the common functions. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Partial Friend NotInheritable Class DeviceManager

End Class

