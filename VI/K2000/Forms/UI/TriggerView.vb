'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\TriggerView.vb
'
' summary:	Trigger view class
'---------------------------------------------------------------------------------------------------
Imports System.Windows.Forms

Imports isr.Core.SplitExtensions

''' <summary>
''' A Keithley 2002 edition of the basic <see cref="Facade.TriggerView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-01-11 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class TriggerView
    Inherits isr.VI.Facade.TriggerView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
        If Not Me.DesignMode Then
            Me.ExternalScanMakeMenu()
        End If
    End Sub

    ''' <summary> Creates a new <see cref="TriggerView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="TriggerView"/>. </returns>
    Public Overloads Shared Function Create() As TriggerView
        Dim view As TriggerView = Nothing
        Try
            view = New TriggerView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K2002Device As K2002Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K2002Device)
        MyBase.AssignDevice(value)
        Me._K2002Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 1)
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 2)
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase), 1)
        Else
            Me.BindSubsystem(Me.K2002Device.ArmLayer1Subsystem, 1)
            Me.BindSubsystem(Me.K2002Device.ArmLayer2Subsystem, 2)
            Me.BindSubsystem(Me.K2002Device.TriggerSubsystem, 1)
        End If
    End Sub

#End Region

#Region " INITIATE WAIT FETCH "

    ''' <summary> Initiate wait read. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Sub InitiateWaitRead()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} clearing execution state"
            Me.K2002Device.ClearExecutionState()

            activity = $"{Me.K2002Device.ResourceNameCaption} initiating wait read"

            ' Me.k2002device.ClearExecutionState()
            ' set the service request
            ' Me.k2002device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
            ' Me.k2002device.StatusSubsystem.EnableServiceRequest(Me.k2002device.Session.DefaultOperationServiceRequestEnableBitmask)
            ' Me.k2002device.Session.Write("*SRE 1") ' Set MSB bit of SRE register
            ' Me.k2002device.Session.Write("stat:meas:ptr 32767; ntr 0; enab 512") ' Set all PTR bits and clear all NTR bits for measurement events Set Buffer Full bit of Measurement
            ' Me.k2002device.Session.Write(":trac:feed calc") ' Select Calculate as reading source
            ' Me.k2002device.Session.Write(":trac:poin 10")   ' Set buffer size to 10 points 
            ' Me.k2002device.Session.Write(":trac:egr full")  ' Select Full element group

            ' trigger the initiation of the measurement letting the triggering or service request do the rest.
            activity = $"{Me.K2002Device.ResourceNameCaption} Initiating meter" : Me.PublishVerbose($"{activity};. ")
            Me.K2002Device.TriggerSubsystem.Initiate()
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " EXTERNAL TRIGGER SCAN "

    ''' <summary> External scan make menu. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ExternalScanMakeMenu()
        Dim infoMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "ExternalScanInfoMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Info",
            .ToolTipText = "Displays information about the external scan"
        }
        AddHandler infoMenuItem.Click, AddressOf Me.ExternalScanInfoMenuItem_Click

        Dim configureMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "ExternalScanConfigureMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Configure",
            .ToolTipText = "Configures external scan"
        }
        AddHandler configureMenuItem.Click, AddressOf Me.ExternalScanConfigureMenuItem_Click

        Dim executeMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "ExternalScanExecuteMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Execute",
            .ToolTipText = "Executes the external scan"
        }
        AddHandler executeMenuItem.Click, AddressOf Me.ExternalScanExecuteMenuItem_Click

        Dim mainMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "ExternalScanMenuItem",
            .Size = New System.Drawing.Size(192, 22),
            .Text = "external scan...",
            .ToolTipText = "Selects external scan options"
        }
        mainMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {infoMenuItem, configureMenuItem, executeMenuItem})
        Me.AddMenuItem(mainMenuItem)
    End Sub

    ''' <summary> Event handler. Called by ExternalScanInfoMenuItem for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExternalScanInfoMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} displaying {NameOf(isr.VI.K2002.TriggerSubsystem.BuildExternalScanHelpMessage).SplitWords}"
            Core.Controls.PopupContainer.PopupInfo(Me, isr.VI.K2002.TriggerSubsystem.BuildExternalScanHelpMessage(Me.K2002Device.TriggerSubsystem), Me.Location, Me.Size)
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by ExternalScanConfigureMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExternalScanConfigureMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.TriggerSubsystem.ConfigureExternalScan)}"
            Dim r As (Success As Boolean, Details As String) = isr.VI.K2002.TriggerSubsystem.ConfigureExternalScan(Me.K2002Device, Me.TriggerCount,
                                                                                                                     Me.TriggerLayerSource, Me.ArmLayer1Source)
            If Not r.Success Then
                activity = Me.PublishWarning($"failed {activity};. {r.Details}")
                Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End If
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by ExternalScanExecuteMenuItem for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExternalScanExecuteMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.TriggerSubsystem.ExecuteExternalScan)}"
            Dim result As (Status As String, Value As String) = isr.VI.K2002.TriggerSubsystem.ExecuteExternalScan(Me.K2002Device.Session)
            Core.Controls.PopupContainer.PopupInfo(Me, $"{result.Status}{Environment.NewLine}{result.Value}", Me.Location, Me.Size)
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

End Class
