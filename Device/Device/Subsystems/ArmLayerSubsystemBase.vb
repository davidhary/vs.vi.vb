''' <summary> Defines a SCPI arm layer base Subsystem. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-05, . based on SCPI 5.1 library. </para>
''' </remarks>
Public MustInherit Class ArmLayerSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ArmLayerSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        Me.New(1, statusSubsystem)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ArmLayerSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="layerNumber">     The arm layer number. </param>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="StatusSubsystemBase">status subsystem</see>. </param>
    Protected Sub New(ByVal layerNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._LayerNumber = layerNumber
        Me.DefineArmLayerBypassModeReadWrites()
        Me.DefineArmSourceReadWrites()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ArmCount = 1
        Me.Delay = TimeSpan.Zero
        Me.InputLineNumber = 2
        Me.OutputLineNumber = 1
        Me.TimerInterval = TimeSpan.FromSeconds(1)
        Me.ArmSource = VI.ArmSources.Immediate
        Me.ArmLayerBypassMode = VI.TriggerLayerBypassModes.Acceptor
        Me.MaximumArmCount = 99999
        Me.MaximumDelay = TimeSpan.FromSeconds(999999.999)
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:IMM". </remarks>
    ''' <value> The Immediate command. </value>
    Protected Overridable Property ImmediateCommand As String

    ''' <summary> Immediately move tot he next layer. </summary>
    Public Sub Immediate()
        Me.Session.Execute(Me.ImmediateCommand)
    End Sub

#End Region

#Region " LAYER NUMBER "

    ''' <summary> Gets or sets the arm layer number. </summary>
    ''' <value> The arm layer number. </value>
    Public ReadOnly Property LayerNumber As Integer

#End Region

#Region " ARM COUNT "

    ''' <summary> Number of maximum arms. </summary>
    Private _MaximumArmCount As Integer

    ''' <summary> Gets or sets the cached Maximum Arm Count. </summary>
    ''' <remarks>
    ''' Specifies how many times an operation is performed in the specified layer of the Arm model.
    ''' </remarks>
    ''' <value> The Arm MaximumArmCount or none if not set or unknown. </value>
    Public Property MaximumArmCount As Integer
        Get
            Return Me._MaximumArmCount
        End Get
        Protected Set(ByVal value As Integer)
            If Not Nullable.Equals(Me.MaximumArmCount, value) Then
                Me._MaximumArmCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Returns <c>True</c> if using an infinite Arm count. </summary>
    ''' <value> The number of is infinite Arms. </value>
    Public ReadOnly Property IsArmCountInfinite As Boolean?
        Get
            Return If(Me.ArmCount.HasValue, Me.ArmCount >= Integer.MaxValue, New Boolean?)
        End Get
    End Property

    ''' <summary> Number of arms. </summary>
    Private _ArmCount As Integer?

    ''' <summary> Gets or sets the cached Arm Count. </summary>
    ''' <remarks>
    ''' Specifies how many times an operation is performed in the specified layer of the Arm model.
    ''' </remarks>
    ''' <value> The Arm ArmCount or none if not set or unknown. </value>
    Public Property ArmCount As Integer?
        Get
            Return Me._ArmCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.ArmCount, value) Then
                Me._ArmCount = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(ArmLayerSubsystemBase.IsArmCountInfinite))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Arm ArmCount. </summary>
    ''' <param name="value"> The current ArmCount. </param>
    ''' <returns> The ArmCount or none if unknown. </returns>
    Public Function ApplyArmCount(ByVal value As Integer) As Integer?
        Me.WriteArmCount(value)
        Return Me.QueryArmCount()
    End Function

    ''' <summary> Gets or sets Arm ArmCount query command. </summary>
    ''' <remarks> SCPI: ":ARM:COUN?". </remarks>
    ''' <value> The Arm ArmCount query command. </value>
    Protected Overridable Property ArmCountQueryCommand As String

    ''' <summary> Queries the current PointsArmCount. </summary>
    ''' <returns> The PointsArmCount or none if unknown. </returns>
    Public Function QueryArmCount() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.ArmCountQueryCommand) Then
            Me.ArmCount = Me.Session.Query(0I, Me.ArmCountQueryCommand)
        End If
        Return Me.ArmCount
    End Function

    ''' <summary> Gets or sets Arm ArmCount command format. </summary>
    ''' <remarks> SCPI: ":ARM:COUN {0}". </remarks>
    ''' <value> The Arm ArmCount command format. </value>
    Protected Overridable Property ArmCountCommandFormat As String

    ''' <summary> Write the arm count without reading back the value from the device. </summary>
    ''' <param name="value"> The current arm count. </param>
    ''' <returns> The arm count or none if unknown. </returns>
    Public Function WriteArmCount(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.ArmCountCommandFormat) Then
            Me.Session.WriteLine(Me.ArmCountCommandFormat, value)
        End If
        Me.ArmCount = value
        Return Me.ArmCount
    End Function

#End Region

#Region " DELAY "

    ''' <summary> The maximum delay. </summary>
    Private _MaximumDelay As TimeSpan

    ''' <summary> Gets or sets the maximum delay. </summary>
    ''' <value> The maximum delay. </value>
    Public Property MaximumDelay As TimeSpan
        Get
            Return Me._MaximumDelay
        End Get
        Protected Set(ByVal value As TimeSpan)
            If Not Nullable.Equals(Me.MaximumDelay, value) Then
                Me._MaximumDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The delay. </summary>
    Private _Delay As TimeSpan?

    ''' <summary> Gets or sets the cached Arm Delay. </summary>
    ''' <remarks>
    ''' The delay is used to delay operation in the Arm layer. After the programmed Arm event occurs,
    ''' the instrument waits until the delay period expires before performing the Device Action.
    ''' </remarks>
    ''' <value> The Arm Delay or none if not set or unknown. </value>
    Public Overloads Property Delay As TimeSpan?
        Get
            Return Me._Delay
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Nullable.Equals(Me.Delay, value) Then
                Me._Delay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Arm Delay. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Arm Delay or none if unknown. </returns>
    Public Function ApplyDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteDelay(value)
        Return Me.QueryDelay()
    End Function

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:DEL?". </remarks>
    ''' <value> The delay query command. </value>
    Protected Overridable Property DelayQueryCommand As String

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Delay query command. </value>
    Protected Overridable Property DelayFormat As String

    ''' <summary> Queries the Delay. </summary>
    ''' <returns> The Delay or none if unknown. </returns>
    Public Function QueryDelay() As TimeSpan?
        Me.Delay = Me.Query(Me.Delay, Me.DelayFormat, Me.DelayQueryCommand)
        Return Me.Delay
    End Function

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:DEL {0:s\.FFFFFFF}". </remarks>
    ''' <value> The delay command format. </value>
    Protected Overridable Property DelayCommandFormat As String

    ''' <summary> Writes the Arm Delay without reading back the value from the device. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Arm Delay or none if unknown. </returns>
    Public Function WriteDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.Delay = Me.Write(value, Me.DelayCommandFormat)
        Return Me.Delay
    End Function

#End Region

#Region " INPUT LINE NUMBER "

    ''' <summary> The input line number. </summary>
    Private _InputLineNumber As Integer?

    ''' <summary> Gets or sets the cached Arm Input Line Number. </summary>
    ''' <value> The Arm InputLineNumber or none if not set or unknown. </value>
    Public Overloads Property InputLineNumber As Integer?
        Get
            Return Me._InputLineNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.InputLineNumber, value) Then
                Me._InputLineNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Input Line Number. </summary>
    ''' <param name="value"> The current Input Line Number. </param>
    ''' <returns> The Input Line Number or none if unknown. </returns>
    Public Function ApplyInputLineNumber(ByVal value As Integer) As Integer?
        Me.WriteInputLineNumber(value)
        Return Me.QueryInputLineNumber()
    End Function

    ''' <summary> Gets or sets the Input Line Number query command. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:ILIN?". </remarks>
    ''' <value> The Input Line Number query command. </value>
    Protected Overridable Property InputLineNumberQueryCommand As String

    ''' <summary> Queries the InputLineNumber. </summary>
    ''' <returns> The Input Line Number or none if unknown. </returns>
    Public Function QueryInputLineNumber() As Integer?
        Me.InputLineNumber = Me.Query(Me.InputLineNumber, Me.InputLineNumberQueryCommand)
        Return Me.InputLineNumber
    End Function

    ''' <summary> Gets or sets the Input Line Number command format. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:ILIN {1}". </remarks>
    ''' <value> The Input Line Number command format. </value>
    Protected Overridable Property InputLineNumberCommandFormat As String

    ''' <summary>
    ''' Writes the Arm Input Line Number without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current InputLineNumber. </param>
    ''' <returns> The Arm Input Line Number or none if unknown. </returns>
    Public Function WriteInputLineNumber(ByVal value As Integer) As Integer?
        Me.InputLineNumber = Me.Write(value, Me.InputLineNumberCommandFormat)
        Return Me.InputLineNumber
    End Function

#End Region

#Region " OUTPUT LINE NUMBER "

    ''' <summary> The output line number. </summary>
    Private _OutputLineNumber As Integer?

    ''' <summary> Gets or sets the cached Output Line Number. </summary>
    ''' <value> The Arm OutputLineNumber or none if not set or unknown. </value>
    Public Overloads Property OutputLineNumber As Integer?
        Get
            Return Me._OutputLineNumber
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.OutputLineNumber, value) Then
                Me._OutputLineNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Arm Output Line Number. </summary>
    ''' <param name="value"> The current Output Line Number. </param>
    ''' <returns> The Output Line Number or none if unknown. </returns>
    Public Function ApplyOutputLineNumber(ByVal value As Integer) As Integer?
        Me.WriteOutputLineNumber(value)
        Return Me.QueryOutputLineNumber()
    End Function

    ''' <summary> Gets or sets the Output Line Number query command. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:OLIN?". </remarks>
    ''' <value> The Output Line Number query command. </value>
    Protected Overridable Property OutputLineNumberQueryCommand As String

    ''' <summary> Queries the OutputLineNumber. </summary>
    ''' <returns> The Output Line Number or none if unknown. </returns>
    Public Function QueryOutputLineNumber() As Integer?
        Me.OutputLineNumber = Me.Query(Me.OutputLineNumber, Me.OutputLineNumberQueryCommand)
        Return Me.OutputLineNumber
    End Function

    ''' <summary> Gets or sets the Output Line Number command format. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:OLIN {0}". </remarks>
    ''' <value> The Output Line Number command format. </value>
    Protected Overridable Property OutputLineNumberCommandFormat As String

    ''' <summary>
    ''' Writes the Arm Output Line Number without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current OutputLineNumber. </param>
    ''' <returns> The Arm Output Line Number or none if unknown. </returns>
    Public Function WriteOutputLineNumber(ByVal value As Integer) As Integer?
        Me.OutputLineNumber = Me.Write(value, Me.OutputLineNumberCommandFormat)
        Return Me.OutputLineNumber
    End Function

#End Region

#Region " TIMER TIME SPAN "

    ''' <summary> The timer interval. </summary>
    Private _TimerInterval As TimeSpan?

    ''' <summary> Gets or sets the cached Arm Timer Interval. </summary>
    ''' <remarks>
    ''' The Timer Interval is used to Timer Interval operation in the Arm layer. After the programmed
    ''' Arm event occurs, the instrument waits until the Timer Interval period expires before
    ''' performing the Device Action.
    ''' </remarks>
    ''' <value> The Arm Timer Interval or none if not set or unknown. </value>
    Public Overloads Property TimerInterval As TimeSpan?
        Get
            Return Me._TimerInterval
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Me.TimerInterval.Equals(value) Then
                Me._TimerInterval = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Arm Timer Interval. </summary>
    ''' <param name="value"> The current TimerTimeSpan. </param>
    ''' <returns> The Arm Timer Interval or none if unknown. </returns>
    Public Function ApplyTimerTimeSpan(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteTimerTimeSpan(value)
        Return Me.QueryTimerTimeSpan()
    End Function

    ''' <summary> Gets or sets the Timer Interval query command. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:TIM?". </remarks>
    ''' <value> The Timer Interval query command. </value>
    Protected Overridable Property TimerIntervalQueryCommand As String

    ''' <summary>
    ''' Gets or sets the Timer Interval format for converting the query to time span.
    ''' </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Timer Interval query command. </value>
    Protected Overridable Property TimerIntervalFormat As String

    ''' <summary> Queries the Timer Interval. </summary>
    ''' <returns> The Timer Interval or none if unknown. </returns>
    Public Function QueryTimerTimeSpan() As TimeSpan?
        Me.TimerInterval = Me.Query(Me.TimerInterval, Me.TimerIntervalFormat, Me.TimerIntervalQueryCommand)
        Return Me.TimerInterval
    End Function

    ''' <summary> Gets or sets the Timer Interval command format. </summary>
    ''' <remarks> SCPI: ":ARM:LAYx:TIM {0:s\.FFFFFFF}". </remarks>
    ''' <value> The query command format. </value>
    Protected Overridable Property TimerIntervalCommandFormat As String

    ''' <summary>
    ''' Writes the Arm Timer Interval without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The current TimerTimeSpan. </param>
    ''' <returns> The Arm Timer Interval or none if unknown. </returns>
    Public Function WriteTimerTimeSpan(ByVal value As TimeSpan) As TimeSpan?
        Me.TimerInterval = Me.Write(value, Me.TimerIntervalCommandFormat)
        Return Me.TimerInterval
    End Function

#End Region

End Class

