﻿'---------------------------------------------------------------------------------------------------
' file:		.\Tsp\Syntax\SlotSyntax.vb
'
' summary:	Slot syntax class
'---------------------------------------------------------------------------------------------------
Namespace TspSyntax.Slot

    ''' <summary> Defines the TSP Slot syntax. Modified for TSP2. </summary>
    ''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2005-01-15, 1.0.1841.x. </para></remarks>
    Public Module SlotSyntax

        Public Const SubsystemNameFormat As String = "_G.slot[{0}]"

        ''' <summary> The interlock state format. </summary>
        Public Const InterlockStateFormat As String = "_G.slot[{0}].interlock.state"

        ''' <summary> The interlock state print command format. </summary>
        Public Const InterlockStatePrintCommandFormat As String = "_G.print(_G.string.format('%d',_G.slot[{0}].interlock.state))"

    End Module

End Namespace
