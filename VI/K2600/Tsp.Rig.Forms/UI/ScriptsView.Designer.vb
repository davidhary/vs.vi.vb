﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ScriptsView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._UserScriptsList = New System.Windows.Forms.ListBox()
        Me._RemoveScriptButton = New System.Windows.Forms.Button()
        Me._TspScriptSelector = New isr.Core.Controls.FileSelector()
        Me._ScriptNameTextBox = New System.Windows.Forms.TextBox()
        Me._LoadScriptButton = New System.Windows.Forms.Button()
        Me._LoadAndRunButton = New System.Windows.Forms.Button()
        Me._RunScriptButton = New System.Windows.Forms.Button()
        Me._ScriptsTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._ScriptsPanel4 = New System.Windows.Forms.Panel()
        Me._UserScriptsListLabel = New System.Windows.Forms.Label()
        Me._RefreshUserScriptsListButton = New System.Windows.Forms.Button()
        Me._ScriptsPanel3 = New System.Windows.Forms.Panel()
        Me._TspScriptSelectorLabel = New System.Windows.Forms.Label()
        Me._ScriptsPanel2 = New System.Windows.Forms.Panel()
        Me._RetainCodeOutlineToggle = New System.Windows.Forms.CheckBox()
        Me._ScriptNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._ScriptsPanel1 = New System.Windows.Forms.Panel()
        Me._ScriptsTableLayoutPanel.SuspendLayout()
        Me._ScriptsPanel4.SuspendLayout()
        Me._ScriptsPanel3.SuspendLayout()
        Me._ScriptsPanel2.SuspendLayout()
        Me._ScriptsPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_UserScriptsList
        '
        Me._UserScriptsList.BackColor = System.Drawing.SystemColors.Window
        Me._UserScriptsList.Cursor = System.Windows.Forms.Cursors.Default
        Me._UserScriptsList.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._UserScriptsList.ForeColor = System.Drawing.SystemColors.WindowText
        Me._UserScriptsList.ItemHeight = 17
        Me._UserScriptsList.Location = New System.Drawing.Point(4, 28)
        Me._UserScriptsList.Name = "_UserScriptsList"
        Me._UserScriptsList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._UserScriptsList.Size = New System.Drawing.Size(173, 140)
        Me._UserScriptsList.TabIndex = 2
        '
        '_RemoveScriptButton
        '
        Me._RemoveScriptButton.BackColor = System.Drawing.SystemColors.Control
        Me._RemoveScriptButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RemoveScriptButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._RemoveScriptButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RemoveScriptButton.Location = New System.Drawing.Point(4, 176)
        Me._RemoveScriptButton.Name = "_RemoveScriptButton"
        Me._RemoveScriptButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RemoveScriptButton.Size = New System.Drawing.Size(84, 33)
        Me._RemoveScriptButton.TabIndex = 0
        Me._RemoveScriptButton.Text = "&REMOVE"
        Me._RemoveScriptButton.UseVisualStyleBackColor = True
        '
        '_TspScriptSelector
        '
        Me._TspScriptSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._TspScriptSelector.BackColor = System.Drawing.SystemColors.Window
        Me._TspScriptSelector.DefaultExtension = ".TSP"
        Me._TspScriptSelector.DialogFilter = "TSP Files (*.tsp; *.lua)|*.tsp;*.lua|Script Files (*.dbg)|*.dbg|All Files (*.*)|*" &
    ".*"
        Me._TspScriptSelector.DialogTitle = "SELECT TSP SCRIPT FILE"
        Me._TspScriptSelector.Dock = System.Windows.Forms.DockStyle.Top
        Me._TspScriptSelector.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TspScriptSelector.Location = New System.Drawing.Point(3, 22)
        Me._TspScriptSelector.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._TspScriptSelector.Name = "_TspScriptSelector"
        Me._TspScriptSelector.Padding = New System.Windows.Forms.Padding(3)
        Me._TspScriptSelector.Size = New System.Drawing.Size(594, 27)
        Me._TspScriptSelector.TabIndex = 0
        '
        '_ScriptNameTextBox
        '
        Me._ScriptNameTextBox.AcceptsReturn = True
        Me._ScriptNameTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._ScriptNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._ScriptNameTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ScriptNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ScriptNameTextBox.Location = New System.Drawing.Point(4, 27)
        Me._ScriptNameTextBox.MaxLength = 0
        Me._ScriptNameTextBox.Name = "_ScriptNameTextBox"
        Me._ScriptNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ScriptNameTextBox.Size = New System.Drawing.Size(269, 25)
        Me._ScriptNameTextBox.TabIndex = 1
        Me._ScriptNameTextBox.Text = "product.tsp"
        '
        '_LoadScriptButton
        '
        Me._LoadScriptButton.BackColor = System.Drawing.SystemColors.Control
        Me._LoadScriptButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._LoadScriptButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._LoadScriptButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LoadScriptButton.Location = New System.Drawing.Point(15, 14)
        Me._LoadScriptButton.Name = "_LoadScriptButton"
        Me._LoadScriptButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LoadScriptButton.Size = New System.Drawing.Size(145, 33)
        Me._LoadScriptButton.TabIndex = 0
        Me._LoadScriptButton.Text = "&LOAD"
        Me._LoadScriptButton.UseVisualStyleBackColor = True
        '
        '_LoadAndRunButton
        '
        Me._LoadAndRunButton.BackColor = System.Drawing.SystemColors.Control
        Me._LoadAndRunButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._LoadAndRunButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._LoadAndRunButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LoadAndRunButton.Location = New System.Drawing.Point(15, 86)
        Me._LoadAndRunButton.Name = "_LoadAndRunButton"
        Me._LoadAndRunButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LoadAndRunButton.Size = New System.Drawing.Size(145, 33)
        Me._LoadAndRunButton.TabIndex = 2
        Me._LoadAndRunButton.Text = "L&OAD  AND  RUN"
        Me._LoadAndRunButton.UseVisualStyleBackColor = True
        '
        '_RunScriptButton
        '
        Me._RunScriptButton.BackColor = System.Drawing.SystemColors.Control
        Me._RunScriptButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RunScriptButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._RunScriptButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RunScriptButton.Location = New System.Drawing.Point(15, 50)
        Me._RunScriptButton.Name = "_RunScriptButton"
        Me._RunScriptButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RunScriptButton.Size = New System.Drawing.Size(145, 33)
        Me._RunScriptButton.TabIndex = 1
        Me._RunScriptButton.Text = "&RUN"
        Me._RunScriptButton.UseVisualStyleBackColor = True
        '
        '_ScriptsTableLayoutPanel
        '
        Me._ScriptsTableLayoutPanel.ColumnCount = 1
        Me._ScriptsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ScriptsTableLayoutPanel.Controls.Add(Me._ScriptsPanel4, 0, 5)
        Me._ScriptsTableLayoutPanel.Controls.Add(Me._ScriptsPanel3, 0, 3)
        Me._ScriptsTableLayoutPanel.Controls.Add(Me._ScriptsPanel2, 0, 1)
        Me._ScriptsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ScriptsTableLayoutPanel.Location = New System.Drawing.Point(1, 1)
        Me._ScriptsTableLayoutPanel.Name = "_ScriptsTableLayoutPanel"
        Me._ScriptsTableLayoutPanel.RowCount = 7
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ScriptsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ScriptsTableLayoutPanel.Size = New System.Drawing.Size(606, 466)
        Me._ScriptsTableLayoutPanel.TabIndex = 43
        '
        '_ScriptsPanel4
        '
        Me._ScriptsPanel4.Controls.Add(Me._UserScriptsListLabel)
        Me._ScriptsPanel4.Controls.Add(Me._UserScriptsList)
        Me._ScriptsPanel4.Controls.Add(Me._RefreshUserScriptsListButton)
        Me._ScriptsPanel4.Controls.Add(Me._RemoveScriptButton)
        Me._ScriptsPanel4.Location = New System.Drawing.Point(3, 191)
        Me._ScriptsPanel4.Name = "_ScriptsPanel4"
        Me._ScriptsPanel4.Size = New System.Drawing.Size(191, 220)
        Me._ScriptsPanel4.TabIndex = 40
        '
        '_UserScriptsListLabel
        '
        Me._UserScriptsListLabel.BackColor = System.Drawing.Color.Transparent
        Me._UserScriptsListLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._UserScriptsListLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._UserScriptsListLabel.Location = New System.Drawing.Point(4, 8)
        Me._UserScriptsListLabel.Name = "_UserScriptsListLabel"
        Me._UserScriptsListLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._UserScriptsListLabel.Size = New System.Drawing.Size(173, 16)
        Me._UserScriptsListLabel.TabIndex = 18
        Me._UserScriptsListLabel.Text = "User Scripts: "
        '
        '_RefreshUserScriptsListButton
        '
        Me._RefreshUserScriptsListButton.BackColor = System.Drawing.SystemColors.Control
        Me._RefreshUserScriptsListButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RefreshUserScriptsListButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._RefreshUserScriptsListButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RefreshUserScriptsListButton.Location = New System.Drawing.Point(93, 176)
        Me._RefreshUserScriptsListButton.Name = "_RefreshUserScriptsListButton"
        Me._RefreshUserScriptsListButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RefreshUserScriptsListButton.Size = New System.Drawing.Size(84, 33)
        Me._RefreshUserScriptsListButton.TabIndex = 0
        Me._RefreshUserScriptsListButton.Text = "REFRES&H"
        Me._RefreshUserScriptsListButton.UseVisualStyleBackColor = True
        '
        '_ScriptsPanel3
        '
        Me._ScriptsPanel3.Controls.Add(Me._TspScriptSelector)
        Me._ScriptsPanel3.Controls.Add(Me._TspScriptSelectorLabel)
        Me._ScriptsPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ScriptsPanel3.Location = New System.Drawing.Point(3, 108)
        Me._ScriptsPanel3.Name = "_ScriptsPanel3"
        Me._ScriptsPanel3.Padding = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me._ScriptsPanel3.Size = New System.Drawing.Size(600, 57)
        Me._ScriptsPanel3.TabIndex = 39
        '
        '_TspScriptSelectorLabel
        '
        Me._TspScriptSelectorLabel.BackColor = System.Drawing.Color.Transparent
        Me._TspScriptSelectorLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._TspScriptSelectorLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TspScriptSelectorLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._TspScriptSelectorLabel.Location = New System.Drawing.Point(3, 6)
        Me._TspScriptSelectorLabel.Name = "_TspScriptSelectorLabel"
        Me._TspScriptSelectorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TspScriptSelectorLabel.Size = New System.Drawing.Size(594, 16)
        Me._TspScriptSelectorLabel.TabIndex = 17
        Me._TspScriptSelectorLabel.Text = "Script File: "
        '
        '_ScriptsPanel2
        '
        Me._ScriptsPanel2.Controls.Add(Me._RetainCodeOutlineToggle)
        Me._ScriptsPanel2.Controls.Add(Me._ScriptNameTextBoxLabel)
        Me._ScriptsPanel2.Controls.Add(Me._ScriptNameTextBox)
        Me._ScriptsPanel2.Location = New System.Drawing.Point(3, 23)
        Me._ScriptsPanel2.Name = "_ScriptsPanel2"
        Me._ScriptsPanel2.Size = New System.Drawing.Size(577, 59)
        Me._ScriptsPanel2.TabIndex = 38
        '
        '_RetainCodeOutlineToggle
        '
        Me._RetainCodeOutlineToggle.AutoSize = True
        Me._RetainCodeOutlineToggle.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._RetainCodeOutlineToggle.Location = New System.Drawing.Point(320, 21)
        Me._RetainCodeOutlineToggle.Name = "_RetainCodeOutlineToggle"
        Me._RetainCodeOutlineToggle.Size = New System.Drawing.Size(151, 21)
        Me._RetainCodeOutlineToggle.TabIndex = 2
        Me._RetainCodeOutlineToggle.Text = "Retain Code Outline"
        Me._RetainCodeOutlineToggle.UseVisualStyleBackColor = True
        '
        '_ScriptNameTextBoxLabel
        '
        Me._ScriptNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ScriptNameTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ScriptNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ScriptNameTextBoxLabel.Location = New System.Drawing.Point(4, 7)
        Me._ScriptNameTextBoxLabel.Name = "_ScriptNameTextBoxLabel"
        Me._ScriptNameTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ScriptNameTextBoxLabel.Size = New System.Drawing.Size(80, 16)
        Me._ScriptNameTextBoxLabel.TabIndex = 0
        Me._ScriptNameTextBoxLabel.Text = "Script Name: "
        '
        '_ScriptsPanel1
        '
        Me._ScriptsPanel1.Controls.Add(Me._LoadScriptButton)
        Me._ScriptsPanel1.Controls.Add(Me._LoadAndRunButton)
        Me._ScriptsPanel1.Controls.Add(Me._RunScriptButton)
        Me._ScriptsPanel1.Dock = System.Windows.Forms.DockStyle.Right
        Me._ScriptsPanel1.Location = New System.Drawing.Point(607, 1)
        Me._ScriptsPanel1.Name = "_ScriptsPanel1"
        Me._ScriptsPanel1.Size = New System.Drawing.Size(174, 466)
        Me._ScriptsPanel1.TabIndex = 42
        '
        'ScriptsView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._ScriptsTableLayoutPanel)
        Me.Controls.Add(Me._ScriptsPanel1)
        Me.Name = "ScriptsView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(782, 468)
        Me._ScriptsTableLayoutPanel.ResumeLayout(False)
        Me._ScriptsPanel4.ResumeLayout(False)
        Me._ScriptsPanel3.ResumeLayout(False)
        Me._ScriptsPanel2.ResumeLayout(False)
        Me._ScriptsPanel2.PerformLayout()
        Me._ScriptsPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ScriptsTableLayoutPanel As Windows.Forms.TableLayoutPanel
    Private WithEvents _ScriptsPanel4 As Windows.Forms.Panel
    Private WithEvents _UserScriptsListLabel As Windows.Forms.Label
    Private WithEvents _UserScriptsList As Windows.Forms.ListBox
    Private WithEvents _RefreshUserScriptsListButton As Windows.Forms.Button
    Private WithEvents _RemoveScriptButton As Windows.Forms.Button
    Private WithEvents _ScriptsPanel3 As Windows.Forms.Panel
    Private WithEvents _TspScriptSelector As Core.Controls.FileSelector
    Private WithEvents _TspScriptSelectorLabel As Windows.Forms.Label
    Private WithEvents _ScriptsPanel2 As Windows.Forms.Panel
    Private WithEvents _RetainCodeOutlineToggle As Windows.Forms.CheckBox
    Private WithEvents _ScriptNameTextBoxLabel As Windows.Forms.Label
    Private WithEvents _ScriptNameTextBox As Windows.Forms.TextBox
    Private WithEvents _ScriptsPanel1 As Windows.Forms.Panel
    Private WithEvents _LoadScriptButton As Windows.Forms.Button
    Private WithEvents _LoadAndRunButton As Windows.Forms.Button
    Private WithEvents _RunScriptButton As Windows.Forms.Button
End Class
