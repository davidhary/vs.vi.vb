'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SenseChannelSubsystemBase.vb
'
' summary:	Sense channel subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary>
''' Defines the contract that must be implemented by a Sense Channel Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific ReSenses, Inc.<para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public MustInherit Class SenseChannelSubsystemBase
    Inherits SenseFunctionSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="channelNumber">   The channel number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem, readingAmounts)
        Me.ChannelNumber = channelNumber
        Me.ApertureRange = VI.Pith.Ranges.StandardApertureRange
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Gets or sets the channel number. </summary>
    ''' <value> The channel number. </value>
    Public ReadOnly Property ChannelNumber As Integer

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:CORR2:CLE". </remarks>
    ''' <value> The clear compensations command. </value>
    Protected Overridable Property ClearCompensationsCommand As String

    ''' <summary> Clears the Compensations. </summary>
    Public Sub ClearCompensations()
        Me.Write(Me.ClearCompensationsCommand)
    End Sub


#End Region

#Region " SWEEP POINTS "

    ''' <summary> The sweep points. </summary>
    Private _SweepPoints As Integer?

    ''' <summary> Gets or sets the cached Sweep Points. </summary>
    ''' <value> The Sweep Points or none if not set or unknown. </value>
    Public Overloads Property SweepPoints As Integer?
        Get
            Return Me._SweepPoints
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.SweepPoints, value) Then
                Me._SweepPoints = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Points. </summary>
    ''' <param name="value"> The current Sweep Points. </param>
    ''' <returns> The SweepPoints or none if unknown. </returns>
    Public Function ApplySweepPoints(ByVal value As Integer) As Integer?
        Me.WriteSweepPoints(value)
        Return Me.QuerySweepPoints()
    End Function

    ''' <summary> Gets or sets Sweep Points query command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:SWE:POIN?". </remarks>
    ''' <value> The Sweep Points query command. </value>
    Protected Overridable Property SweepPointsQueryCommand As String

    ''' <summary> Queries the current Sweep Points. </summary>
    ''' <returns> The Sweep Points or none if unknown. </returns>
    Public Function QuerySweepPoints() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.SweepPointsQueryCommand) Then
            Me.SweepPoints = Me.Session.Query(0I, Me.SweepPointsQueryCommand)
        End If
        Return Me.SweepPoints
    End Function

    ''' <summary> Gets or sets Sweep Points command format. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:SWE:POIN {0}". </remarks>
    ''' <value> The Sweep Points command format. </value>
    Protected Overridable Property SweepPointsCommandFormat As String

    ''' <summary> Write the Sweep Points without reading back the value from the device. </summary>
    ''' <param name="value"> The current PointsSweepPoints. </param>
    ''' <returns> The PointsSweepPoints or none if unknown. </returns>
    Public Function WriteSweepPoints(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.SweepPointsCommandFormat) Then
            Me.Session.WriteLine(Me.SweepPointsCommandFormat, value)
        End If
        Me.SweepPoints = value
        Return Me.SweepPoints
    End Function

#End Region

#Region " SWEEP START "

    ''' <summary> The sweep start. </summary>
    Private _SweepStart As Double?

    ''' <summary> Gets or sets the cached Sweep Start. </summary>
    ''' <value> The Sweep Start or none if not set or unknown. </value>
    Public Overloads Property SweepStart As Double?
        Get
            Return Me._SweepStart
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.SweepStart, value) Then
                Me._SweepStart = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Start. </summary>
    ''' <param name="value"> The current Sweep Start. </param>
    ''' <returns> The SweepStart or none if unknown. </returns>
    Public Function ApplySweepStart(ByVal value As Double) As Double?
        Me.WriteSweepStart(value)
        Return Me.QuerySweepStart()
    End Function

    ''' <summary> Gets or sets Sweep Start query command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:FREQ:STAR?". </remarks>
    ''' <value> The Sweep Start query command. </value>
    Protected Overridable Property SweepStartQueryCommand As String

    ''' <summary> Queries the current Sweep Start. </summary>
    ''' <returns> The Sweep Start or none if unknown. </returns>
    Public Function QuerySweepStart() As Double?
        If Not String.IsNullOrWhiteSpace(Me.SweepStartQueryCommand) Then
            Me.SweepStart = Me.Session.Query(0I, Me.SweepStartQueryCommand)
        End If
        Return Me.SweepStart
    End Function

    ''' <summary> Gets or sets Sweep Start command format. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:FREQ:STAR {0}". </remarks>
    ''' <value> The Sweep Start command format. </value>
    Protected Overridable Property SweepStartCommandFormat As String

    ''' <summary> Write the Sweep Start without reading back the value from the device. </summary>
    ''' <param name="value"> The current StartSweepStart. </param>
    ''' <returns> The StartSweepStart or none if unknown. </returns>
    Public Function WriteSweepStart(ByVal value As Double) As Double?
        If Not String.IsNullOrWhiteSpace(Me.SweepStartCommandFormat) Then
            Me.Session.WriteLine(Me.SweepStartCommandFormat, value)
        End If
        Me.SweepStart = value
        Return Me.SweepStart
    End Function

#End Region

#Region " SWEEP STOP "

    ''' <summary> The sweep stop. </summary>
    Private _SweepStop As Double?

    ''' <summary> Gets or sets the cached Sweep Stop. </summary>
    ''' <value> The Sweep Stop or none if not set or unknown. </value>
    Public Overloads Property SweepStop As Double?
        Get
            Return Me._SweepStop
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.SweepStop, value) Then
                Me._SweepStop = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Stop. </summary>
    ''' <param name="value"> The current Sweep Stop. </param>
    ''' <returns> The SweepStop or none if unknown. </returns>
    Public Function ApplySweepStop(ByVal value As Double) As Double?
        Me.WriteSweepStop(value)
        Return Me.QuerySweepStop()
    End Function

    ''' <summary> Gets or sets Sweep Stop query command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:FREQ:STOP?". </remarks>
    ''' <value> The Sweep Stop query command. </value>
    Protected Overridable Property SweepStopQueryCommand As String

    ''' <summary> Queries the current Sweep Stop. </summary>
    ''' <returns> The Sweep Stop or none if unknown. </returns>
    Public Function QuerySweepStop() As Double?
        If Not String.IsNullOrWhiteSpace(Me.SweepStopQueryCommand) Then
            Me.SweepStop = Me.Session.Query(0I, Me.SweepStopQueryCommand)
        End If
        Return Me.SweepStop
    End Function

    ''' <summary> Gets or sets Sweep Stop command format. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:FREQ:STOP {0}". </remarks>
    ''' <value> The Sweep Stop command format. </value>
    Protected Overridable Property SweepStopCommandFormat As String

    ''' <summary> Write the Sweep Stop without reading back the value from the device. </summary>
    ''' <param name="value"> The current StopSweepStop. </param>
    ''' <returns> The StopSweepStop or none if unknown. </returns>
    Public Function WriteSweepStop(ByVal value As Double) As Double?
        If Not String.IsNullOrWhiteSpace(Me.SweepStopCommandFormat) Then
            Me.Session.WriteLine(Me.SweepStopCommandFormat, value)
        End If
        Me.SweepStop = value
        Return Me.SweepStop
    End Function

#End Region

#Region " ADAPTER TYPE "

    ''' <summary> List of types of the supported adapters. </summary>
    Private _SupportedAdapterTypes As AdapterTypes

    ''' <summary>
    ''' Gets or sets the supported Adapter Type. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> A list of types of the supported adapters. </value>
    Public Property SupportedAdapterTypes() As AdapterTypes
        Get
            Return Me._SupportedAdapterTypes
        End Get
        Set(ByVal value As AdapterTypes)
            If Not Me.SupportedAdapterTypes.Equals(value) Then
                Me._SupportedAdapterTypes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Adapter Type. </summary>
    Private _AdapterType As AdapterTypes?

    ''' <summary> Gets or sets the cached Adapter Type. </summary>
    ''' <value> The <see cref="AdapterType">Adapter Type</see> or none if not set or unknown. </value>
    Public Overridable Property AdapterType As AdapterTypes?
        Get
            Return Me._AdapterType
        End Get
        Protected Set(ByVal value As AdapterTypes?)
            If Not Nullable.Equals(Me.AdapterType, value) Then
                Me._AdapterType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Adapter Type. </summary>
    ''' <param name="value"> The Adapter Type. </param>
    ''' <returns> The <see cref="AdapterType">Adapter Type</see> or none if unknown. </returns>
    Public Function ApplyAdapterType(ByVal value As AdapterTypes) As AdapterTypes?
        Me.WriteAdapterType(value)
        Return Me.QueryAdapterType()
    End Function

    ''' <summary> Gets or sets the Adapter Type query command. </summary>
    ''' <value> The Adapter Type query command, e.g., :SENS:ADAPT? </value>
    Protected Overridable Property AdapterTypeQueryCommand As String

    ''' <summary> Queries the Adapter Type. </summary>
    ''' <returns> The <see cref="AdapterType">Adapter Type</see> or none if unknown. </returns>
    Public Function QueryAdapterType() As AdapterTypes?
        If String.IsNullOrWhiteSpace(Me.AdapterTypeQueryCommand) Then
            Me.AdapterType = New AdapterTypes?
        Else
            Dim mode As String = Me.AdapterType.ToString
            Me.Session.MakeEmulatedReplyIfEmpty(mode)
            mode = Me.Session.QueryTrimEnd(Me.AdapterTypeQueryCommand)
            If String.IsNullOrWhiteSpace(mode) Then
                Dim message As String = "Failed fetching Adapter Type"
                Debug.Assert(Not Debugger.IsAttached, message)
                Me.AdapterType = New AdapterTypes?
            Else
                Dim se As New StringEnumerator(Of AdapterTypes)
                Me.AdapterType = se.ParseContained(mode.BuildDelimitedValue)
            End If
        End If
        Return Me.AdapterType
    End Function

    ''' <summary> Gets or sets the Adapter Type command. </summary>
    ''' <value> The Adapter Type command, e.g., :SENS:ADAPT {0}. </value>
    Protected Overridable Property AdapterTypeCommandFormat As String

    ''' <summary> Writes the Adapter Type without reading back the value from the device. </summary>
    ''' <param name="value"> The Adapter Type. </param>
    ''' <returns> The <see cref="AdapterType">Adapter Type</see> or none if unknown. </returns>
    Public Function WriteAdapterType(ByVal value As AdapterTypes) As AdapterTypes?
        If Not String.IsNullOrWhiteSpace(Me.AdapterTypeCommandFormat) Then
            Me.Session.WriteLine(Me.AdapterTypeCommandFormat, value.ExtractBetween())
        End If
        Me.AdapterType = value
        Return Me.AdapterType
    End Function

#End Region

#Region " FREQUENCY POINTS TYPE "

    ''' <summary> The Frequency Points Type. </summary>
    Private _FrequencyPointsType As FrequencyPointsTypes?

    ''' <summary> Gets or sets the cached Frequency Points Type. </summary>
    ''' <value>
    ''' The <see cref="FrequencyPointsType">Frequency Points Type</see> or none if not set or unknown.
    ''' </value>
    Public Overridable Property FrequencyPointsType As FrequencyPointsTypes?
        Get
            Return Me._FrequencyPointsType
        End Get
        Protected Set(ByVal value As FrequencyPointsTypes?)
            If Not Nullable.Equals(Me.FrequencyPointsType, value) Then
                Me._FrequencyPointsType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Frequency Points Type. </summary>
    ''' <param name="value"> The Frequency Points Type. </param>
    ''' <returns>
    ''' The <see cref="FrequencyPointsType">Frequency Points Type</see> or none if unknown.
    ''' </returns>
    Public Function ApplyFrequencyPointsType(ByVal value As FrequencyPointsTypes) As FrequencyPointsTypes?
        Me.WriteFrequencyPointsType(value)
        Return Me.QueryFrequencyPointsType()
    End Function

    ''' <summary> Gets or sets the Frequency Points Type query command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:COLL:FPO?". </remarks>
    ''' <value> The 'frequency points type query' command. </value>
    Protected Overridable Property FrequencyPointsTypeQueryCommand As String

    ''' <summary> Queries the Frequency Points Type. </summary>
    ''' <returns>
    ''' The <see cref="FrequencyPointsType">Frequency Points Type</see> or none if unknown.
    ''' </returns>
    Public Function QueryFrequencyPointsType() As FrequencyPointsTypes?
        If String.IsNullOrWhiteSpace(Me.FrequencyPointsTypeQueryCommand) Then
            Me.FrequencyPointsType = New FrequencyPointsTypes?
        Else
            Dim mode As String = Me.FrequencyPointsType.ToString
            Me.Session.MakeEmulatedReplyIfEmpty(mode)
            mode = Me.Session.QueryTrimEnd(Me.FrequencyPointsTypeQueryCommand)
            If String.IsNullOrWhiteSpace(mode) Then
                Dim message As String = "Failed fetching Frequency Points Type"
                Debug.Assert(Not Debugger.IsAttached, message)
                Me.FrequencyPointsType = New FrequencyPointsTypes?
            Else
                Dim se As New StringEnumerator(Of FrequencyPointsTypes)
                Me.FrequencyPointsType = se.ParseContained(mode.BuildDelimitedValue)
            End If
        End If
        Return Me.FrequencyPointsType
    End Function

    ''' <summary> Gets or sets the Frequency Points Type command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:COLL:FPO {0}". </remarks>
    ''' <value> The frequency points type command format. </value>
    Protected Overridable Property FrequencyPointsTypeCommandFormat As String

    ''' <summary>
    ''' Writes the Frequency Points Type without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Frequency Points Type. </param>
    ''' <returns>
    ''' The <see cref="FrequencyPointsType">Frequency Points Type</see> or none if unknown.
    ''' </returns>
    Public Function WriteFrequencyPointsType(ByVal value As FrequencyPointsTypes) As FrequencyPointsTypes?
        If Not String.IsNullOrWhiteSpace(Me.FrequencyPointsTypeCommandFormat) Then
            Me.Session.WriteLine(Me.FrequencyPointsTypeCommandFormat, value.ExtractBetween())
        End If
        Me.FrequencyPointsType = value
        Return Me.FrequencyPointsType
    End Function

#End Region

#Region " SWEEP TYPE "

    ''' <summary> List of types of the supported sweeps. </summary>
    Private _SupportedSweepTypes As SweepTypes

    ''' <summary>
    ''' Gets or sets the supported Sweep Type. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> A list of types of the supported sweeps. </value>
    Public Property SupportedSweepTypes() As SweepTypes
        Get
            Return Me._SupportedSweepTypes
        End Get
        Set(ByVal value As SweepTypes)
            If Not Me.SupportedSweepTypes.Equals(value) Then
                Me._SupportedSweepTypes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Sweep Type. </summary>
    Private _SweepType As SweepTypes?

    ''' <summary> Gets or sets the cached Sweep Type. </summary>
    ''' <value> The <see cref="SweepType">Sweep Type</see> or none if not set or unknown. </value>
    Public Overridable Property SweepType As SweepTypes?
        Get
            Return Me._SweepType
        End Get
        Protected Set(ByVal value As SweepTypes?)
            If Not Nullable.Equals(Me.SweepType, value) Then
                Me._SweepType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Type. </summary>
    ''' <param name="value"> The Sweep Type. </param>
    ''' <returns> The <see cref="SweepType">Sweep Type</see> or none if unknown. </returns>
    Public Function ApplySweepType(ByVal value As SweepTypes) As SweepTypes?
        Me.WriteSweepType(value)
        Return Me.QuerySweepType()
    End Function

    ''' <summary> Gets or sets the Sweep Type query command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:SWE:TYPE? </remarks>
    ''' <value> The 'sweep type query' command. </value>
    Protected Overridable Property SweepTypeQueryCommand As String

    ''' <summary> Queries the Sweep Type. </summary>
    ''' <returns> The <see cref="SweepType">Sweep Type</see> or none if unknown. </returns>
    Public Function QuerySweepType() As SweepTypes?
        If String.IsNullOrWhiteSpace(Me.SweepTypeQueryCommand) Then
            Me.SweepType = New SweepTypes?
        Else
            Dim mode As String = Me.SweepType.ToString
            Me.Session.MakeEmulatedReplyIfEmpty(mode)
            mode = Me.Session.QueryTrimEnd(Me.SweepTypeQueryCommand)
            If String.IsNullOrWhiteSpace(mode) Then
                Dim message As String = "Failed fetching Sweep Type"
                Debug.Assert(Not Debugger.IsAttached, message)
                Me.SweepType = New SweepTypes?
            Else
                Dim se As New StringEnumerator(Of SweepTypes)
                Me.SweepType = se.ParseContained(mode.BuildDelimitedValue)
            End If
        End If
        Return Me.SweepType
    End Function

    ''' <summary> Gets or sets the Sweep Type command. </summary>
    ''' <remarks> SCPI: ":SENS&lt;ch#&gt;:SWE:TYPE {0} </remarks>
    ''' <value> The sweep type command format. </value>
    Protected Overridable Property SweepTypeCommandFormat As String

    ''' <summary> Writes the Sweep Type without reading back the value from the device. </summary>
    ''' <param name="value"> The Sweep Type. </param>
    ''' <returns> The <see cref="SweepType">Sweep Type</see> or none if unknown. </returns>
    Public Function WriteSweepType(ByVal value As SweepTypes) As SweepTypes?
        If Not String.IsNullOrWhiteSpace(Me.SweepTypeCommandFormat) Then
            Me.Session.WriteLine(Me.SweepTypeCommandFormat, value.ExtractBetween())
        End If
        Me.SweepType = value
        Return Me.SweepType
    End Function

#End Region

End Class

''' <summary> Specifies the adapter types. </summary>
<Flags>
Public Enum AdapterTypes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None (NONE)")>
    None = 0

    ''' <summary> An enum constant representing the 4 m 1 option. </summary>
    <ComponentModel.Description("4TP 1m (E4M1)")>
    E4M1 = 1

    ''' <summary> An enum constant representing the 4 m 2 option. </summary>
    <ComponentModel.Description("4TP 2m (E4M2)")>
    E4M2 = 2
End Enum

''' <summary> A bit-field of flags for specifying sweep types. </summary>
<Flags>
Public Enum SweepTypes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not set ()")>
    None = 0

    ''' <summary> An enum constant representing the linear option. </summary>
    <ComponentModel.Description("Linear (LIN)")>
    Linear = 1

    ''' <summary> An enum constant representing the logarithmic option. </summary>
    <ComponentModel.Description("Logarithmic (LOG)")>
    Logarithmic = 2

    ''' <summary> An enum constant representing the segment option. </summary>
    <ComponentModel.Description("Segment (SEGM)")>
    Segment = 4

    ''' <summary> An enum constant representing the power option. </summary>
    <ComponentModel.Description("Power (POW)")>
    Power = 8

    ''' <summary> An enum constant representing the linear bias option. </summary>
    <ComponentModel.Description("Linear Bias (BIAS)")>
    LinearBias = 16

    ''' <summary> An enum constant representing the logarithmic bias option. </summary>
    <ComponentModel.Description("Logarithmic Bias (LBI)")>
    LogarithmicBias = 32
End Enum

''' <summary> A bit-field of flags for specifying frequency points types. </summary>
<Flags>
Public Enum FrequencyPointsTypes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not set ()")>
    None = 0

    ''' <summary> An enum constant representing the fixed option. </summary>
    <ComponentModel.Description("Fixed (FIX)")>
    Fixed = 1

    ''' <summary> An enum constant representing the user option. </summary>
    <ComponentModel.Description("User (USER)")>
    User = 2
End Enum
