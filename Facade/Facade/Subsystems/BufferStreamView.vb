'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\BufferStreamView.vb
'
' summary:	Buffer stream view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.VI.Facade.DataGridViewExtensions
Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Buffer Stream view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class BufferStreamView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._BufferSizeNumeric.NumericUpDownControl.CausesValidation = True
        Me._BufferSizeNumeric.NumericUpDownControl.Minimum = 0
        Me._BufferSizeNumeric.NumericUpDownControl.Maximum = 27500000
        Me._StartBufferStreamMenuItem.Enabled = False
        Me._AssertBusTriggerButton.Enabled = False
    End Sub

    ''' <summary> Creates a new <see cref="BufferStreamView"/> </summary>
    ''' <returns> A <see cref="BufferStreamView"/>. </returns>
    Public Shared Function Create() As BufferStreamView
        Dim view As BufferStreamView = Nothing
        Try
            view = New BufferStreamView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS: BUFFER "

    ''' <summary> Applies the buffer capacity. </summary>
    ''' <param name="capacity"> The capacity. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplyBufferCapacity(ByVal capacity As Integer)
        Dim activity As String = String.Empty
        Try
            If Me.Device.IsDeviceOpen Then
                Dim value As Integer = CInt(Me._BufferSizeNumeric.Value)
                If Me.BufferSubsystem IsNot Nothing Then
                    activity = $"{Me.Device.ResourceNameCaption} setting {GetType(TraceSubsystemBase)}.{NameOf(BufferSubsystemBase.Capacity)} to {value}"
                    Me.PublishVerbose(activity)
                    BufferStreamView.ApplyBufferCapacity(Me.BufferSubsystem, value)
                ElseIf Me.TraceSubsystem IsNot Nothing Then
                    activity = $"{Me.Device.ResourceNameCaption} setting {GetType(TraceSubsystemBase)}.{NameOf(TraceSubsystemBase.PointsCount)} to {value}"
                    Me.PublishVerbose(activity)
                    BufferStreamView.ApplyBufferCapacity(Me.TraceSubsystem, value)
                End If
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        End Try
    End Sub

    ''' <summary> Reads the buffer. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadBuffer()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            If Me.BufferSubsystem IsNot Nothing Then
                Me.ReadBuffer(Me.BufferSubsystem)
            ElseIf Me.TraceSubsystem IsNot Nothing Then
                Me.ReadBuffer(Me.TraceSubsystem)
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS: BUFFER DISPLAY "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DataGridView As DataGridView
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the data grid view. </summary>
    ''' <value> The data grid view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DataGridView As DataGridView
        Get
            Return Me._DataGridView
        End Get
        Set(value As DataGridView)
            Me._DataGridView = value
        End Set
    End Property

    ''' <summary> Gets the buffer readings. </summary>
    ''' <value> The buffer readings. </value>
    Private ReadOnly Property BufferReadings As New VI.BufferReadingBindingList

    ''' <summary> Displays a buffer readings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub DisplayBufferReadings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            Me.BufferSubsystem.StartElapsedStopwatch()
            Me.ClearBufferDisplay()
            Me.BufferReadings.Add(Me.BufferSubsystem.QueryBufferReadings)
            Me.BufferSubsystem.LastReading = Me.BufferReadings.LastReading
            Me.BufferSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the buffer display. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearBufferDisplay()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing buffer display" : Me.PublishVerbose($"{activity};. ")
            If Me.BufferReadings Is Nothing Then
                Me._BufferReadings = New VI.BufferReadingBindingList()
                Me.DataGridView?.Bind(Me.BufferReadings, True)
            End If
            Me.BufferReadings.Clear()
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me.DataGridView = Nothing
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(BufferStreamView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " BUFFER SUBSYSTEM "

    ''' <summary> Gets the Buffer subsystem. </summary>
    ''' <value> The Buffer subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BufferSubsystem As BufferSubsystemBase

    ''' <summary> Bind Buffer subsystem. </summary>
    ''' <param name="subsystem">  The subsystem. </param>
    ''' <param name="bufferName"> Name of the buffer. </param>
    Public Sub BindSubsystem(ByVal subsystem As BufferSubsystemBase, ByVal bufferName As String)
        If Me.BufferSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.BufferSubsystem)
            Me._BufferSubsystem = Nothing
        End If
        Me._BufferSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.BufferSubsystem)
            Me._SubsystemSplitButton.Text = bufferName
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As BufferSubsystemBase)
        If add Then Me.DataGridView?.Bind(subsystem.BufferReadingsBindingList, True)
        Dim binding As Binding = Me.AddRemoveBinding(Me._BufferSizeNumeric, add, NameOf(NumericUpDown.Value), subsystem, NameOf(BufferSubsystemBase.Capacity))
        ' has to apply the value.
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
        Me.AddRemoveBinding(Me._BufferCountLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.ActualPointCount))
        Me.AddRemoveBinding(Me._FirstPointNumberLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.FirstPointNumber))
        Me.AddRemoveBinding(Me._LastPointNumberLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.LastPointNumber))
        BufferStreamView.ReadSettings(subsystem)
        If add Then
            AddHandler Me.BufferSubsystem.PropertyChanged, AddressOf Me.HandleBufferSubsystemPropertyChange
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler Me.BufferSubsystem.PropertyChanged, AddressOf Me.HandleBufferSubsystemPropertyChange
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As BufferSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(BufferSubsystemBase.BufferStreamingEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(BufferSubsystemBase.BufferStreamingActive))
        Me.HandlePropertyChanged(subsystem, NameOf(BufferSubsystemBase.BufferReadingsCount))
    End Sub

    ''' <summary> Handles the Buffer subsystem property change. </summary>
    ''' <param name="subsystem">    The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As VI.BufferSubsystemBase, ByVal propertyName As String)
        If subsystem IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.BufferSubsystemBase.BufferReadingsCount)
                    If subsystem.BufferReadingsCount > 0 Then
                        Dim message As String = Me.PublishInfo($"Streaming reading #{subsystem.BufferReadingsCount}: {subsystem.LastReading.Amount} 0x{subsystem.LastReading.StatusWord:X4}")
                        subsystem.Session.StatusPrompt = message
                    End If
                Case NameOf(VI.BufferSubsystemBase.BufferStreamingEnabled)
                    Me.NotifyPropertyChanged(NameOf(BufferSubsystemBase.BufferStreamingEnabled))
                Case NameOf(VI.BufferSubsystemBase.BufferStreamingActive)
                    Me._ConfigureStreamingMenuItem.Enabled = Not subsystem.BufferStreamingActive
                    If subsystem.BufferReadingsCount > 0 AndAlso Not subsystem.BufferStreamingActive Then
                        Dim message As String = Me.PublishInfo($"Streaming ended reading #{subsystem.BufferReadingsCount}: {subsystem.LastReading.Amount} 0x{subsystem.LastReading.StatusWord:X4}")
                        subsystem.Session.StatusPrompt = message
                    End If
                    Me.NotifyPropertyChanged(NameOf(BufferSubsystemBase.BufferStreamingActive))
            End Select
        End If
    End Sub

    ''' <summary> Handles the Buffer subsystem property change. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleBufferSubsystemPropertyChange(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(BufferSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.HandleBufferSubsystemPropertyChange), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.HandleBufferSubsystemPropertyChange), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, BufferSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As BufferSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryCapacity()
        subsystem.QueryFirstPointNumber()
        subsystem.QueryLastPointNumber()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Applies the buffer capacity. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="capacity">  The capacity. </param>
    Private Shared Sub ApplyBufferCapacity(ByVal subsystem As BufferSubsystemBase, ByVal capacity As Integer)
        ' overrides and set to the minimum size: Me._TraceSizeNumeric.Value = Me._TraceSizeNumeric.NumericUpDownControl.Minimum
        subsystem.StartElapsedStopwatch()
        subsystem.ApplyCapacity(capacity)
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the buffer. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadBuffer(ByVal subsystem As BufferSubsystemBase)
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            subsystem.StartElapsedStopwatch()
            Me.ClearBufferDisplay()
            Me.BufferReadings.Add(subsystem.QueryBufferReadings)
            subsystem.LastReading = Me.BufferReadings.LastReading
            subsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TRACE SUBSYSTEM "

    ''' <summary> Gets the Trace subsystem. </summary>
    ''' <value> The Trace subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TraceSubsystem As TraceSubsystemBase

    ''' <summary> Bind Trace subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As TraceSubsystemBase)
        If Me.TraceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TraceSubsystem)
            Me._TraceSubsystem = Nothing
        End If
        Me._TraceSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me._SubsystemSplitButton.Text = "Trace"
            Me.BindSubsystem(True, Me.TraceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TraceSubsystemBase)
        If add Then Me.DataGridView?.Bind(subsystem.BufferReadingsBindingList, True)
        Dim binding As Binding = Me.AddRemoveBinding(Me._BufferSizeNumeric, add, NameOf(NumericUpDown.Value), subsystem, NameOf(TraceSubsystemBase.PointsCount))
        ' has to apply the value.
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
        Me.AddRemoveBinding(Me._BufferCountLabel, add, NameOf(Control.Text), subsystem, NameOf(TraceSubsystemBase.ActualPointCount))
        Me._FirstPointNumberLabel.Visible = False
        Me._LastPointNumberLabel.Visible = False
        BufferStreamView.ReadSettings(subsystem)
        If add Then
            AddHandler Me.TraceSubsystem.PropertyChanged, AddressOf Me.HandleTraceSubsystemPropertyChange
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler Me.TraceSubsystem.PropertyChanged, AddressOf Me.HandleTraceSubsystemPropertyChange
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As TraceSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.BufferStreamingEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.BufferStreamingActive))
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.BufferReadingsCount))
    End Sub

    ''' <summary> Handles the trace subsystem property change. </summary>
    ''' <param name="subsystem">    The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As VI.TraceSubsystemBase, ByVal propertyName As String)
        If subsystem IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.TraceSubsystemBase.BufferReadingsCount)
                    If subsystem.BufferReadingsCount > 0 Then
                        Dim message As String = Me.PublishInfo($"Streaming reading #{subsystem.BufferReadingsCount}: {subsystem.LastBufferReading.Amount} 0x{subsystem.LastBufferReading.StatusWord:X4}")
                        subsystem.Session.StatusPrompt = message
                    End If
                Case NameOf(VI.TraceSubsystemBase.BufferStreamingEnabled)
                    Me.NotifyPropertyChanged(NameOf(TraceSubsystemBase.BufferStreamingEnabled))
                Case NameOf(VI.TraceSubsystemBase.BufferStreamingActive)
                    Me._ConfigureStreamingMenuItem.Enabled = Not subsystem.BufferStreamingActive
                    If subsystem.BufferReadingsCount > 0 AndAlso Not subsystem.BufferStreamingActive Then
                        Dim message As String = Me.PublishInfo($"Streaming ended reading #{subsystem.BufferReadingsCount}: {subsystem.LastBufferReading.Amount} 0x{subsystem.LastBufferReading.StatusWord:X4}")
                        subsystem.Session.StatusPrompt = message
                    End If
                    Me.NotifyPropertyChanged(NameOf(TraceSubsystemBase.BufferStreamingActive))
            End Select
        End If
    End Sub

    ''' <summary> Handles the trace subsystem property change. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleTraceSubsystemPropertyChange(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TraceSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.HandleTraceSubsystemPropertyChange), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TraceSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As TraceSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryPointsCount()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Applies the buffer capacity. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="capacity">  The capacity. </param>
    Private Shared Sub ApplyBufferCapacity(ByVal subsystem As TraceSubsystemBase, ByVal capacity As Integer)
        ' overrides and set to the minimum size: Me._TraceSizeNumeric.Value = Me._TraceSizeNumeric.NumericUpDownControl.Minimum
        subsystem.StartElapsedStopwatch()
        subsystem.ApplyPointsCount(capacity)
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the buffer. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ReadBuffer(ByVal subsystem As TraceSubsystemBase)
        subsystem.StartElapsedStopwatch()
        Me.ClearBufferDisplay()
        Me.BufferReadings.Add(subsystem.QueryBufferReadings)
        subsystem.LastBufferReading = Me.BufferReadings.LastReading
        subsystem.StopElapsedStopwatch()
    End Sub


#End Region

#Region " TRIGGER "

    ''' <summary> Gets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystemBase

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As TriggerSubsystemBase)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As TriggerSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(TriggerSubsystemBase.TriggerState))
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(TriggerSubsystemBase.TriggerState)
                ' TO_DO: Bind trigger state label caption to the Subsystem reading label in the display view.
                ' TO_DO: Move functionality (e.g., handler trigger plan state change) from the user interface to the subsystem
                Me._TriggerStateLabel.Visible = subsystem.TriggerState.HasValue
                If subsystem.SupportsTriggerState AndAlso subsystem.TriggerState.HasValue Then
                    Me._TriggerStateLabel.Text = subsystem.TriggerState.Value.ToString
                    If Not subsystem.IsTriggerStateActive AndAlso Me._StartBufferStreamMenuItem.Checked Then
                        Me._StartBufferStreamMenuItem.Checked = False
                    End If
                End If
                ' ?? this causes a cross thread exception. 
                ' Me._TriggerStateLabel.Invalidate()
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Initiate trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitiateTriggerPlan()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.StartElapsedStopwatch()
            Me.TriggerSubsystem.Initiate()
            Me.TriggerSubsystem.QueryTriggerState()
            Me.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Aborts trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub AbortTriggerPlan()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.Abort()
            Me.Device.Session.QueryOperationCompleted(TimeSpan.FromMilliseconds(100))
            Me.TriggerSubsystem.QueryTriggerState()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SYSTEM SUBSYSTEM "

    ''' <summary> Gets the System subsystem. </summary>
    ''' <value> The System subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SystemSubsystem As SystemSubsystemBase

    ''' <summary> Bind System subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As SystemSubsystemBase)
        If Me.SystemSubsystem IsNot Nothing Then
            RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.HandleSystemSubsystemPropertyChange
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If subsystem IsNot Nothing Then
            BufferStreamView.ReadSettings(subsystem)
            ' Me.ApplyPropertyChanged(subsystem)
            AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.HandleSystemSubsystemPropertyChange
        End If
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As SystemSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFrontTerminalsSelected()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As VI.SystemSubsystemBase, ByVal propertyName As String)
        If subsystem IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.SystemSubsystemBase.SupportsScanCardOption)
                    Me._UsingScanCardMenuItem.Enabled = subsystem.SupportsScanCardOption
                    If Not subsystem.SupportsScanCardOption Then
                        Me._UsingScanCardMenuItem.Checked = False
                    End If
            End Select
        End If
    End Sub

    ''' <summary> Handles the system subsystem property change. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleSystemSubsystemPropertyChange(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.SystemSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.HandleSystemSubsystemPropertyChange), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.HandleSystemSubsystemPropertyChange), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SystemSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " ARM LAYER 1 SUBSYSTEM "

    ''' <summary> Gets the ArmLayer1 subsystem. </summary>
    ''' <value> The ArmLayer1 subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayer1Subsystem As ArmLayerSubsystemBase

    ''' <summary> Bind ArmLayer1 subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem1(ByVal subsystem As ArmLayerSubsystemBase)
        If Me.ArmLayer1Subsystem IsNot Nothing Then
            Me._ArmLayer1Subsystem = Nothing
        End If
        Me._ArmLayer1Subsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " ARM LAYER 2 SUBSYSTEM "

    ''' <summary> Gets the ArmLayer2 subsystem. </summary>
    ''' <value> The ArmLayer2 subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayer2Subsystem As ArmLayerSubsystemBase

    ''' <summary> Bind ArmLayer2 subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem2(ByVal subsystem As ArmLayerSubsystemBase)
        If Me.ArmLayer2Subsystem IsNot Nothing Then
            Me._ArmLayer2Subsystem = Nothing
        End If
        Me._ArmLayer2Subsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " DIGITAL OUTPUT SUBSYSTEM "

    ''' <summary> Gets the digital output subsystem. </summary>
    ''' <value> The digital output subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DigitalOutputSubsystem As DigitalOutputSubsystemBase

    ''' <summary> Bind ArmLayer2 subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As DigitalOutputSubsystemBase)
        If Me.DigitalOutputSubsystem IsNot Nothing Then
            Me._DigitalOutputSubsystem = Nothing
        End If
        Me._DigitalOutputSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " SENSE SUBSYSTEM "

    ''' <summary> Gets the Sense subsystem. </summary>
    ''' <value> The Sense subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseSubsystem As VI.SenseSubsystemBase

    ''' <summary> Bind Sense subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As SenseSubsystemBase)
        If Me.SenseSubsystem IsNot Nothing Then
            RemoveHandler Me.SenseSubsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me._SenseSubsystem = Nothing
        End If
        Me._SenseSubsystem = subsystem
        If subsystem IsNot Nothing Then
            AddHandler Me.SenseSubsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Gets the function mode changed. </summary>
    ''' <value> The function mode changed. </value>
    Public ReadOnly Property FunctionModeChanged As Boolean
        Get
            Return Not Nullable.Equals(Me.SenseFunctionSubsystem?.FunctionMode, Me.SenseSubsystem?.FunctionMode)
        End Get
    End Property

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub HandleFunctionModesChanged(ByVal subsystem As SenseSubsystemBase)
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As SenseSubsystemBase, ByVal propertyName As String)
        If Me.InitializingComponents OrElse subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(SenseSubsystemBase.FunctionMode)
                If Me.FunctionModeChanged Then Me.HandleFunctionModesChanged(Me.SenseSubsystem)
        End Select
    End Sub

    ''' <summary> Sense subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE FUNCTION SUBSYSTEM "

    ''' <summary> Gets the sense function subsystem. </summary>
    ''' <value> The sense function subsystem. </value>
    Public ReadOnly Property SenseFunctionSubsystem As VI.SenseFunctionSubsystemBase

    ''' <summary> Bind Sense function subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.SenseFunctionSubsystemBase)
        If Me.SenseFunctionSubsystem IsNot Nothing Then
            Me._SenseFunctionSubsystem = Nothing
        End If
        Me._SenseFunctionSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub


#End Region

#Region " BINNING SUBSYSTEM "

    ''' <summary> Gets the <see cref="VI.BinningSubsystemBase"/>. </summary>
    ''' <value> The <see cref="VI.BinningSubsystemBase"/>. </value>
    Public ReadOnly Property BinningSubsystem As VI.BinningSubsystemBase

    ''' <summary> Bind <see cref="VI.BinningSubsystemBase"/>. </summary>
    ''' <param name="subsystem"> The <see cref="VI.BinningSubsystemBase"/>. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.BinningSubsystemBase)
        If Me.BinningSubsystem IsNot Nothing Then
            Me._BinningSubsystem = Nothing
        End If
        Me._BinningSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Gets the Route subsystem. </summary>
    ''' <value> The Route subsystem. </value>
    Public ReadOnly Property RouteSubsystem As VI.RouteSubsystemBase

    ''' <summary> Bind Route subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As VI.RouteSubsystemBase)
        If Me.RouteSubsystem IsNot Nothing Then
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

    ''' <summary>
    ''' Configures four wire resistance scan using virtual instrument library functions.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="scanList">      List of scans. </param>
    ''' <param name="sampleCount">   Number of samples. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <returns> A String. </returns>
    Public Function ConfigureFourWireResistanceScan(ByVal scanList As String, ByVal sampleCount As Integer,
                                                    ByVal triggerSource As ArmSources, ByVal triggerCount As Integer) As String
        Dim result As String = String.Empty
        ' this is required for getting the correct function mode when fetching buffers.
        Me.SenseSubsystem.ApplyFunctionMode(SenseFunctionModes.ResistanceFourWire)
        Me.TriggerSubsystem.ApplyContinuousEnabled(False)
        Me.RouteSubsystem.ApplySelectedScanListType(ScanListType.None)
        Me.RouteSubsystem.ApplyScanListFunction("(@1:10)", SenseFunctionModes.None, Me.SenseSubsystem.FunctionModeReadWrites)
        Me.RouteSubsystem.ApplyScanListFunction(scanList, SenseFunctionModes.ResistanceFourWire, Me.SenseSubsystem.FunctionModeReadWrites)
        Me.TriggerSubsystem.ApplyTriggerCount(sampleCount)
        Me.TriggerSubsystem.ApplyTriggerSource(TriggerSources.Immediate)
        Me.ArmLayer2Subsystem.ApplyArmCount(triggerCount)
        Me.ArmLayer2Subsystem.ApplyArmSource(triggerSource)
        If ArmSources.Timer = triggerSource Then
            Me.ArmLayer2Subsystem.ApplyTimerTimeSpan(TimeSpan.FromMilliseconds(600))
        End If
        Me.TraceSubsystem.ClearBuffer()
        Me.TraceSubsystem.ApplyPointsCount(sampleCount)
        Me.TraceSubsystem.ApplyFeedSource(FeedSources.Sense)
        Me.TraceSubsystem.ApplyFeedControl(FeedControls.Next)
        Me.RouteSubsystem.ApplySelectedScanListType(ScanListType.Internal)
        Return result
    End Function


#End Region

#Region " CONFIGURE BUFFER STREAM "

    ''' <summary> Restore Trigger State. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RestoreState()

        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Dim title As String = Me.Device.OpenResourceTitle
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{title} aborting trigger plan" : Me.PublishInfo($"{activity};. ")
            Me.TriggerSubsystem.Abort()
            Me.Device.Session.QueryOperationCompleted()
            activity = $"{title} restoring device state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ResetKnownState()
            Me.Device.Session.QueryOperationCompleted()
            Me.Device.ClearExecutionState()
            Me.Device.Session.QueryOperationCompleted()
            Me.TriggerSubsystem.ApplyContinuousEnabled(False)
            Me.Device.Session.EnableServiceRequestWaitComplete()
            Me.Device.Session.StatusPrompt = "Instrument state restored"
            Me._StartBufferStreamMenuItem.Enabled = False
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Configure trigger plan. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="sampleCount">   Number of samples. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    Protected Overridable Sub ConfigureTriggerPlan(ByVal triggerCount As Integer, ByVal sampleCount As Integer, ByVal triggerSource As isr.VI.TriggerSources)

        Me.TriggerSubsystem.ApplyContinuousEnabled(False)
        Me.TriggerSubsystem.Abort()
        Me.TriggerSubsystem.ClearTriggerModel()
        Me.Device.Session.Wait()
        Me.Device.Session.ReadStatusRegister()

        Me.TriggerSubsystem.ApplyAutoDelayEnabled(False)
        Me.TriggerSubsystem.Session.EnableServiceRequestWaitComplete()
        Me.TriggerSubsystem.Session.ReadStatusRegister()

        Me.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        Me.ArmLayer1Subsystem.ApplyArmCount(1)
        Me.ArmLayer1Subsystem.ApplyArmLayerBypassMode(TriggerLayerBypassModes.Acceptor)

        Me.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        Me.ArmLayer2Subsystem.ApplyArmCount(sampleCount)
        Me.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)
        Me.ArmLayer2Subsystem.ApplyArmLayerBypassMode(TriggerLayerBypassModes.Acceptor)

        Me.TriggerSubsystem.ApplyTriggerSource(triggerSource)
        Me.TriggerSubsystem.ApplyTriggerCount(triggerCount)
        Me.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
        Me.TriggerSubsystem.ApplyTriggerLayerBypassMode(TriggerLayerBypassModes.Acceptor)

    End Sub

    ''' <summary> Configure measurement. </summary>
    ''' <remarks> David, 2020-07-25. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Private Sub ConfigureMeasurement()
        Me.SystemSubsystem.ApplyAutoZeroEnabled(True)
        Me.SenseSubsystem.ApplyFunctionMode(My.Settings.StreamBufferSenseFunctionMode)
        Me.Device.Session.QueryOperationCompleted()
        ' verify that function mode change occurred
        If Me.FunctionModeChanged Then
            Throw New Core.OperationFailedException($"Failed settings new {NameOf(SenseFunctionSubsystemBase).SplitWords} to {My.Settings.StreamBufferSenseFunctionMode} from {Me.SenseFunctionSubsystem.FunctionMode}")
        End If
        Me.SenseFunctionSubsystem.ApplyPowerLineCycles(1)
        Dim autoRangeEnabled As Boolean = True
        Dim actuaAutoRangeEnabled As Boolean? = Me.SenseFunctionSubsystem.ApplyAutoRangeEnabled(autoRangeEnabled)
        If Not Nullable.Equals(actuaAutoRangeEnabled, autoRangeEnabled) Then
            Throw New Core.OperationFailedException($"Failed settings {NameOf(SenseFunctionSubsystemBase.AutoRangeEnabled).SplitWords} to {autoRangeEnabled}")
        End If
        Me.SenseFunctionSubsystem.ApplyResolutionDigits(9)

        Me.SenseFunctionSubsystem.ApplyOpenLeadDetectorEnabled(True)
        'Me.SenseFunctionSubsystem.ApplyPowerLineCycles(1)
        'Me.SenseFunctionSubsystem.ApplyRange(My.Settings.NominalResistance)

    End Sub

    ''' <summary> Configure digital output. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub ConfigureDigitalOutput(ByVal subsystem As DigitalOutputSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.ApplyDigitalActiveLevels(New Integer() {1, 2, 3, 4},
                                       New DigitalActiveLevels() {DigitalActiveLevels.Low, DigitalActiveLevels.Low, DigitalActiveLevels.Low, DigitalActiveLevels.Low})
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Configure limit binning. </summary>
    Protected Overridable Sub ConfigureLimitBinning()

        Dim expectedResistance As Double = My.Settings.NominalResistance
        Dim resistanceTolerance As Double = My.Settings.ResistanceTolerance
        Dim openLimit As Double = My.Settings.OpenLimit
        Dim passOutputValue As Integer = My.Settings.PasstBitmask
        Dim failOutputValue As Integer = My.Settings.FailBitmask
        Dim overflowOutputValue As Integer = My.Settings.OverflowBitmask

        ' sets the expected duration of the binning probe, which is used to wait before 
        ' enabling the next measurement cycle. 
        Me.BinningSubsystem.BinningStrobeDuration = My.Settings.BinningStrobeDuration

        Me.BinningSubsystem.ApplyPassSource(passOutputValue)

        ' limit 2 is set for the nominal values
        Dim expectedValue As Double = expectedResistance * (1 - resistanceTolerance)
        Me.BinningSubsystem.ApplyLimit2LowerLevel(expectedValue)

        expectedValue = expectedResistance * (1 + resistanceTolerance)
        Me.BinningSubsystem.ApplyLimit2UpperLevel(expectedValue)

        Me.BinningSubsystem.ApplyLimit2AutoClear(True)
        Me.BinningSubsystem.ApplyLimit2Enabled(True)

        Me.BinningSubsystem.ApplyLimit2LowerSource(failOutputValue)
        Me.BinningSubsystem.ApplyLimit2UpperSource(failOutputValue)

        ' limit 1 is set for the overflow
        expectedValue = expectedResistance * resistanceTolerance
        Me.BinningSubsystem.ApplyLimit1LowerLevel(expectedValue)
        Me.BinningSubsystem.ApplyLimit1UpperLevel(openLimit)

        Me.BinningSubsystem.ApplyLimit1AutoClear(True)

        Me.BinningSubsystem.ApplyLimit1Enabled(True)

        Me.BinningSubsystem.ApplyLimit1LowerSource(overflowOutputValue)
        Me.BinningSubsystem.ApplyLimit1UpperSource(overflowOutputValue)

        Me.BinningSubsystem.ApplyBinningStrobeEnabled(True)

    End Sub

    ''' <summary> Configure trace for fetching only. </summary>
    ''' <param name="binningStrokeDuration"> Duration of the binning stroke. </param>
    Protected Overridable Sub ConfigureTrace(ByVal binningStrokeDuration As TimeSpan)
        Me.TraceSubsystem.ClearBuffer()
        Me.TraceSubsystem.BinningDuration = binningStrokeDuration
        Me.TraceSubsystem.ApplyFeedSource(FeedSources.Sense)
        Me.TraceSubsystem.ApplyFeedControl(FeedControls.Never)
    End Sub

    ''' <summary> Builds buffer streaming description. </summary>
    ''' <returns> A String. </returns>
    Public Overridable Function BuildBufferStreamingDescription() As String
        If Me.InitializingComponents Then Return String.Empty
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine($"Buffer streaming plan:")
        builder.AppendLine($"Measurement: {My.Settings.StreamBufferSenseFunctionMode}; auto zero; {1} NPLC; auto range; 8.5 digits.")
        builder.AppendLine($"Limits, Ω: Pass: {My.Settings.NominalResistance}±{(My.Settings.NominalResistance * My.Settings.ResistanceTolerance)}; Open: {My.Settings.OpenLimit}.")
        builder.AppendLine($"Binning Bitmasks: Pass={My.Settings.PasstBitmask}; Fail={My.Settings.FailBitmask}; Open={My.Settings.OverflowBitmask}.")
        builder.AppendLine($"Using Scan Card: {Me._UsingScanCardMenuItem.Checked}")
        If Me._UsingScanCardMenuItem.Checked Then
            builder.AppendLine($"Arm layer 1: {1} count; {VI.ArmSources.Immediate}; {0} delay.")
            builder.AppendLine($"Arm layer 2: {My.Settings.StreamTriggerCount} count; {My.Settings.StreamBufferArmSource}; {0} delay.")
            builder.AppendLine($"Trigger: {VI.ArmSources.Immediate}; {My.Settings.ScanCardSampleCount} count; {0} delay; {VI.TriggerLayerBypassModes.Acceptor} bypass.")
        Else
            builder.AppendLine($"Arm layer 1: {1} count; {VI.ArmSources.Immediate}; {0} delay.")
            builder.AppendLine($"Arm layer 2: {1} count; {VI.ArmSources.Immediate}; {0} delay.")
            builder.AppendLine($"Trigger: {My.Settings.StreamBufferTriggerSource}; {My.Settings.StreamTriggerCount} count; {0} delay; {VI.TriggerLayerBypassModes.Acceptor} bypass.")
        End If
        Return builder.ToString
    End Function

    ''' <summary> Configure buffer stream. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureBufferStream()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} Configure trigger plan" : Me.PublishInfo($"{activity};. ")
            Me.ConfigureTriggerPlan(My.Settings.StreamTriggerCount, 1, My.Settings.StreamBufferTriggerSource)
            If Me.SenseSubsystem IsNot Nothing AndAlso
                (Not Me.SenseSubsystem.FunctionMode.HasValue OrElse
                        Me.SenseSubsystem.FunctionMode <> My.Settings.StreamBufferSenseFunctionMode) Then Me.ConfigureMeasurement()

            If Me.BinningSubsystem IsNot Nothing AndAlso
                (Me.BinningSubsystem.Limit1AutoClear.GetValueOrDefault(False) = False OrElse
                Me.BinningSubsystem.Limit1Enabled.GetValueOrDefault(False) = False) Then
                Me.ConfigureLimitBinning()
                Me.ConfigureDigitalOutput(Me.DigitalOutputSubsystem)
            End If

            If Me.TraceSubsystem IsNot Nothing AndAlso
                Me.TraceSubsystem.FeedSource.GetValueOrDefault(VI.FeedSources.None) <> FeedSources.Sense Then
                Me.ConfigureTrace(If(Me.BinningSubsystem IsNot Nothing, Me.BinningSubsystem.BinningStrobeDuration, My.Settings.BinningStrobeDuration))
            End If

            If Me._UsingScanCardMenuItem.Checked Then
                ' the fetched buffer includes only reading values.
                Me.TraceSubsystem.OrderedReadingElementTypes = New List(Of ReadingElementTypes) From {VI.ReadingElementTypes.Reading}
                Me.ConfigureFourWireResistanceScan(My.Settings.ScanCardScanList, My.Settings.ScanCardSampleCount,
                                                   My.Settings.StreamBufferArmSource, My.Settings.StreamTriggerCount)
            End If
            If Me.BufferSubsystem IsNot Nothing Then
                Me.DataGridView?.Bind(Me.BufferSubsystem.BufferReadingsBindingList, True)
            Else
                Me.DataGridView?.Bind(Me.TraceSubsystem.BufferReadingsBindingList, True)
            End If
            Me.Device.Session.StatusPrompt = "Ready to commence buffer stream"
            Me._StartBufferStreamMenuItem.Enabled = True
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " COMMENCE BUFFER STREAM "

    ''' <summary> Gets the buffer streaming enabled. </summary>
    ''' <value> The buffer streaming enabled. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BufferStreamingEnabled As Boolean
        Get
            If Me.BufferSubsystem IsNot Nothing Then
                Return Me.BufferSubsystem.BufferStreamingEnabled
            ElseIf Me.TraceSubsystem IsNot Nothing Then
                Return Me.TraceSubsystem.BufferStreamingEnabled
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary> Gets the buffer streaming Active. </summary>
    ''' <value> The buffer streaming Active. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BufferStreamingActive As Boolean
        Get
            If Me.BufferSubsystem IsNot Nothing Then
                Return Me.BufferSubsystem.BufferStreamingActive
            ElseIf Me.TraceSubsystem IsNot Nothing Then
                Return Me.TraceSubsystem.BufferStreamingActive
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary> Stops buffer streaming. </summary>
    Private Sub StopBufferStreaming()
        Dim timeout As TimeSpan = VI.TraceSubsystemBase.EstimateStreamStopTimeoutInterval(Me.TraceSubsystem.StreamCycleDuration,
                                                                                          isr.VI.Facade.My.Settings.BufferStreamPollInterval, 1.5)
        If Me.BufferSubsystem IsNot Nothing Then RemoveHandler Me.BufferSubsystem.BufferStreamTasker.AsyncCompleted, AddressOf Me.BufferStreamTasker_AsyncCompleted
        If Me.TraceSubsystem IsNot Nothing Then RemoveHandler Me.TraceSubsystem.BufferStreamTasker.AsyncCompleted, AddressOf Me.BufferStreamTasker_AsyncCompleted
        Dim result As (Success As Boolean, Details As String) = If(Me.BufferSubsystem IsNot Nothing,
                                                                        Me.BufferSubsystem.StopBufferStream(timeout),
                                                                        Me.TraceSubsystem.StopBufferStream(timeout))
        If result.Success Then
            Me.TriggerSubsystem.Abort()
            Me.ReadStatusRegister()
            Me.TriggerSubsystem.QueryTriggerState()
            Me.ReadStatusRegister()
        Else
            Me.PublishWarning($"buffer streaming failed {result.Details}")
        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by BufferStreamTasker for asynchronous completed events.
    ''' </summary>
    ''' <remarks> David, 2020-08-06. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Asynchronous completed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub BufferStreamTasker_AsyncCompleted(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        Dim activity As String = "Handling buffer stream completed event"
        If sender Is Nothing OrElse e Is Nothing Then Return
        Try
            If e.Error IsNot Nothing Then
                Me.PublishException(activity, e.Error)
                Me.StopBufferStreaming()
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Starts buffer streaming. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub StartBufferStreaming(ByVal subsystem As TraceSubsystemBase)
        Me.Device.ClearExecutionState()
        Me.TriggerSubsystem.Initiate()
        isr.Core.ApplianceBase.DoEvents()
        AddHandler subsystem.BufferStreamTasker.AsyncCompleted, AddressOf Me.BufferStreamTasker_AsyncCompleted
        subsystem.StartBufferStream(Me.TriggerSubsystem, My.Settings.BufferStreamPollInterval, Me.SenseSubsystem.FunctionUnit)
    End Sub

    ''' <summary> Starts buffer streaming. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub StartBufferStreaming(ByVal subsystem As BufferSubsystemBase)
        Me.Device.ClearExecutionState()
        subsystem.BufferReadingUnit = Me.SenseSubsystem.FunctionUnit
        Me.TriggerSubsystem.Initiate()
        isr.Core.ApplianceBase.DoEvents()
        AddHandler subsystem.BufferStreamTasker.AsyncCompleted, AddressOf Me.BufferStreamTasker_AsyncCompleted
        subsystem.StartBufferStream(Me.TriggerSubsystem, My.Settings.BufferStreamPollInterval)
    End Sub

    ''' <summary> Commence buffer stream. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CommenceBufferStream()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Commencing buffer stream" : Me.PublishInfo($"{activity};. ")
            If Me.BufferSubsystem IsNot Nothing Then
                Me.StartBufferStreaming(Me.BufferSubsystem)
            Else
                Me.StartBufferStreaming(Me.TraceSubsystem)
            End If
            If Me.Device.StatusSubsystemBase.ErrorAvailable Then
                Me.StopBufferStreaming()
                Me.PublishWarning(Me.Device.StatusSubsystemBase.DeviceErrorReport)
                Me.Device.Session.StatusPrompt = "Streaming aborted"
            Else
                Me.Device.Session.StatusPrompt = If(Me._UsingScanCardMenuItem.Checked,
                    $"Streaming started--awaiting {My.Settings.StreamBufferArmSource} triggers",
                    $"Streaming started--awaiting {My.Settings.StreamBufferTriggerSource} triggers")
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: STREAM BUFFER "

    ''' <summary> Starts buffer stream. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub StartBufferStream()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me._StartBufferStreamMenuItem.Checked Then
                Me.CommenceBufferStream()
            Else
                activity = $"{Me.Device.ResourceNameCaption} stopping buffer streaming" : Me.PublishInfo($"{activity};. ")
                Me.StopBufferStreaming()
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Stream buffer menu item check state changed. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StartBufferStreamMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _StartBufferStreamMenuItem.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.StartBufferStream()
        Me._AssertBusTriggerButton.Enabled = Me._StartBufferStreamMenuItem.Checked AndAlso
                                                If(Me._UsingScanCardMenuItem.Checked,
                                                   VI.ArmSources.Bus = My.Settings.StreamBufferArmSource,
                                                   VI.TriggerSources.Bus = My.Settings.StreamBufferTriggerSource)
    End Sub

    ''' <summary> Configure streaming menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConfigureStreamingMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _ConfigureStreamingMenuItem.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        If Me._ConfigureStreamingMenuItem.Checked Then
            If Me._UsingScanCardMenuItem.Checked AndAlso Me.SystemSubsystem.QueryFrontTerminalsSelected.Value Then
                Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, "Set Rear Terminal")
                Return
            End If
            Me.ConfigureBufferStream()
        Else
            Me.RestoreState()
        End If
    End Sub

    ''' <summary> Explain buffer streaming menu item click. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExplainBufferStreamingMenuItem_Click(sender As Object, e As EventArgs) Handles _ExplainBufferStreamingMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} building external scan trigger plan description"
        Try
            Core.Controls.PopupContainer.PopupInfo(Me, Me.BuildBufferStreamingDescription, Me._SubsystemToolStrip.Location, Me.Size)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.SetIconPadding(Me, -15)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
        End Try
    End Sub

    ''' <summary> Assert bus trigger button click. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AssertBusTriggerButton_Click(sender As Object, e As EventArgs) Handles _AssertBusTriggerButton.Click
        Me.TraceSubsystem.BusTriggerRequested = True
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Buffer size text box validating. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub BufferSizeNumeric_Validating(sender As Object, e As CancelEventArgs) Handles _BufferSizeNumeric.Validating
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplyBufferCapacity(CInt(Me._BufferSizeNumeric.Value))
    End Sub

    ''' <summary> Handles the DataError event of the _dataGridView control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
    '''                       event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DataGridView_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles _DataGridView.DataError
        Try
            ' prevent error reporting when adding a new row or editing a cell
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid IsNot Nothing Then
                If grid.CurrentRow IsNot Nothing AndAlso grid.CurrentRow.IsNewRow Then Return
                If grid.IsCurrentCellInEditMode Then Return
                If grid.IsCurrentRowDirty Then Return
                Dim activity As String = $"{Me.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString}"

                Me.PublishVerbose(activity)
                Me.InfoProvider.Annunciate(grid, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End If
        Catch
        End Try
    End Sub

    ''' <summary> Displays a buffer menu item click. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DisplayBufferMenuItem_Click(sender As Object, e As EventArgs) Handles _DisplayBufferMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.DisplayBufferReadings()
    End Sub

    ''' <summary> Clears the buffer display menu item click. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearBufferDisplayMenuItem_Click(sender As Object, e As EventArgs) Handles _ClearBufferDisplayMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ClearBufferDisplay()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

