'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\MultimeterSubsystemBase_Function.vb
'
' summary:	Multimeter subsystem base function class
'---------------------------------------------------------------------------------------------------

Partial Public MustInherit Class MultimeterSubsystemBase

#Region " RANGE "

    ''' <summary> Define function mode ranges. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functionModeRanges">   The function mode ranges. </param>
    ''' <param name="defaultFunctionRange"> The default function range. </param>
    Public Shared Sub DefineFunctionModeRanges(ByVal functionModeRanges As RangeDictionary, ByVal defaultFunctionRange As Core.Constructs.RangeR)
        If functionModeRanges Is Nothing Then Throw New ArgumentNullException(NameOf(functionModeRanges))
        functionModeRanges.Clear()
        For Each functionMode As VI.MultimeterFunctionModes In [Enum].GetValues(GetType(VI.MultimeterFunctionModes))
            functionModeRanges.Add(functionMode, New Core.Constructs.RangeR(defaultFunctionRange))
        Next
    End Sub

    ''' <summary> Define function mode ranges. </summary>
    Private Sub DefineFunctionModeRanges()
        Me._FunctionModeRanges = New RangeDictionary
        MultimeterSubsystemBase.DefineFunctionModeRanges(Me.FunctionModeRanges, Me.DefaultFunctionRange)
    End Sub

    ''' <summary> Gets or sets the function mode ranges. </summary>
    ''' <value> The function mode ranges. </value>
    Public ReadOnly Property FunctionModeRanges As RangeDictionary

    ''' <summary> Gets or sets the default function range. </summary>
    ''' <value> The default function range. </value>
    Public Property DefaultFunctionRange As isr.Core.Constructs.RangeR

    ''' <summary> Converts a functionMode to a range. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> FunctionMode as an isr.Core.Constructs.RangeR. </returns>
    Public Overridable Function ToRange(ByVal functionMode As Integer) As isr.Core.Constructs.RangeR
        Return Me.FunctionModeRanges(functionMode)
    End Function

    ''' <summary> The function range. </summary>
    Private _FunctionRange As Core.Constructs.RangeR

    ''' <summary> The Range of the range. </summary>
    ''' <value> The function range. </value>
    Public Property FunctionRange As Core.Constructs.RangeR
        Get
            Return Me._FunctionRange
        End Get
        Protected Set(value As Core.Constructs.RangeR)
            If Me.FunctionRange <> value Then
                Me._FunctionRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Establish range. </summary>
    ''' <param name="initialValue">     The initial value. </param>
    ''' <param name="rangeScaleFactor"> The range scale factor. </param>
    ''' <param name="countOut">         The count out. </param>
    ''' <returns> The MetaStatus. </returns>
    Public Function EstablishRange(ByVal initialValue As Double, ByVal rangeScaleFactor As Double, ByVal countOut As Integer) As MetaStatus

        countOut -= 1
        If Me.AutoRangeEnabled.GetValueOrDefault(True) Then
            ' disabling auto range
            Me.ApplyAutoRangeEnabled(False)
        End If

        ' set range candidate
        Me.ApplyRange(initialValue)

        ' take a measurement
        Dim value As Double? = Me.MeasureReadingAmounts

        ' get the meta status
        Dim result As MetaStatus = Me.PrimaryReading.MetaStatus

        ' check if success
        If value.HasValue AndAlso Me.PrimaryReading.MetaStatus.IsSet Then
            If Me.PrimaryReading.MetaStatus.Infinity OrElse Me.PrimaryReading.MetaStatus.NegativeInfinity Then
                ' if infinity, increase the range
                initialValue *= rangeScaleFactor
                If countOut > 0 Then
                    result = Me.EstablishRange(initialValue, rangeScaleFactor, countOut)
                End If
            Else
                ' if has value then done.
            End If
        ElseIf countOut > 0 Then
            ' 2-wire range measurement failed --trying again
            result = Me.EstablishRange(initialValue, rangeScaleFactor, countOut)
        Else
            ' if count out, done
        End If
        Return result
    End Function

#End Region

#Region " DECIMAL PLACES "

    ''' <summary> Define function mode decimal places. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functionModeDecimalPlaces">        The function mode decimal places. </param>
    ''' <param name="defaultFunctionModeDecimalPlaces"> The default decimal places. </param>
    Public Shared Sub DefineFunctionModeDecimalPlaces(ByVal functionModeDecimalPlaces As IntegerDictionary, ByVal defaultFunctionModeDecimalPlaces As Integer)
        If functionModeDecimalPlaces Is Nothing Then Throw New ArgumentNullException(NameOf(functionModeDecimalPlaces))
        functionModeDecimalPlaces.Clear()
        For Each functionMode As VI.MultimeterFunctionModes In [Enum].GetValues(GetType(VI.MultimeterFunctionModes))
            functionModeDecimalPlaces.Add(functionMode, defaultFunctionModeDecimalPlaces)
        Next
    End Sub

    ''' <summary> Define function mode decimal places. </summary>
    Private Sub DefineFunctionModeDecimalPlaces()
        Me._FunctionModeDecimalPlaces = New IntegerDictionary
        MultimeterSubsystemBase.DefineFunctionModeDecimalPlaces(Me.FunctionModeDecimalPlaces, Me.DefaultFunctionModeDecimalPlaces)
    End Sub

    ''' <summary> Gets or sets the default decimal places. </summary>
    ''' <value> The default decimal places. </value>
    Public Property DefaultFunctionModeDecimalPlaces As Integer

    ''' <summary> Gets or sets the function mode decimal places. </summary>
    ''' <value> The function mode decimal places. </value>
    Public ReadOnly Property FunctionModeDecimalPlaces As IntegerDictionary

    ''' <summary> Converts a function Mode to a decimal places. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> FunctionMode as an Integer. </returns>
    Public Overridable Function ToDecimalPlaces(ByVal functionMode As Integer) As Integer
        Return Me.FunctionModeDecimalPlaces(functionMode)
    End Function

    ''' <summary> The function range decimal places. </summary>
    Private _FunctionRangeDecimalPlaces As Integer

    ''' <summary> Gets or sets the function range decimal places. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="KeyNotFoundException">  Thrown when a Key Not Found error condition occurs. </exception>
    ''' <value> The function range decimal places. </value>
    Public Property FunctionRangeDecimalPlaces As Integer
        Get
            Return Me._FunctionRangeDecimalPlaces
        End Get
        Protected Set(value As Integer)
            If Me.FunctionRangeDecimalPlaces <> value Then
                Me._FunctionRangeDecimalPlaces = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " UNIT "

    ''' <summary> Define function mode units. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="KeyNotFoundException">  Thrown when a Key Not Found error condition occurs. </exception>
    ''' <param name="functionModeUnits"> The function mode decimal places. </param>
    Public Shared Sub DefineFunctionModeUnits(ByVal functionModeUnits As UnitDictionary)
        If functionModeUnits Is Nothing Then Throw New ArgumentNullException(NameOf(functionModeUnits))
        functionModeUnits.Item(MultimeterFunctionModes.CurrentAC) = Arebis.StandardUnits.ElectricUnits.Ampere
        functionModeUnits.Item(MultimeterFunctionModes.CurrentDC) = Arebis.StandardUnits.ElectricUnits.Ampere
        functionModeUnits.Item(MultimeterFunctionModes.ResistanceTwoWire) = Arebis.StandardUnits.ElectricUnits.Ohm
        functionModeUnits.Item(MultimeterFunctionModes.ResistanceFourWire) = Arebis.StandardUnits.ElectricUnits.Ohm
        functionModeUnits.Item(MultimeterFunctionModes.VoltageAC) = Arebis.StandardUnits.ElectricUnits.Volt
        functionModeUnits.Item(MultimeterFunctionModes.VoltageDC) = Arebis.StandardUnits.ElectricUnits.Volt
        functionModeUnits.Item(MultimeterFunctionModes.Capacitance) = Arebis.StandardUnits.ElectricUnits.Farad
        functionModeUnits.Item(MultimeterFunctionModes.Continuity) = Arebis.StandardUnits.ElectricUnits.Ohm
        functionModeUnits.Item(MultimeterFunctionModes.Diode) = Arebis.StandardUnits.ElectricUnits.Volt
        functionModeUnits.Item(MultimeterFunctionModes.Frequency) = Arebis.StandardUnits.FrequencyUnits.Hertz
        functionModeUnits.Item(MultimeterFunctionModes.Period) = Arebis.StandardUnits.TimeUnits.Second
        functionModeUnits.Item(MultimeterFunctionModes.Ratio) = Arebis.StandardUnits.UnitlessUnits.Ratio
        functionModeUnits.Item(MultimeterFunctionModes.ResistanceCommonSide) = Arebis.StandardUnits.ElectricUnits.Ohm
        functionModeUnits.Item(MultimeterFunctionModes.Temperature) = Arebis.StandardUnits.TemperatureUnits.Kelvin
        For Each functionMode As VI.MultimeterFunctionModes In [Enum].GetValues(GetType(VI.MultimeterFunctionModes))
            If functionMode <> VI.MultimeterFunctionModes.None AndAlso Not functionModeUnits.ContainsKey(functionMode) Then
                Throw New System.Collections.Generic.KeyNotFoundException($"Unit not specified for multimeter function mode {functionMode}")
            End If
        Next
    End Sub

    ''' <summary> Define function mode units. </summary>
    Private Sub DefineFunctionModeUnits()
        Me._FunctionModeUnits = New UnitDictionary
        MultimeterSubsystemBase.DefineFunctionModeUnits(Me._FunctionModeUnits)
    End Sub

    ''' <summary> Gets or sets the default unit. </summary>
    ''' <value> The default unit. </value>
    Public Property DefaultFunctionUnit As Arebis.TypedUnits.Unit

    ''' <summary> Gets or sets the function mode decimal places. </summary>
    ''' <value> The function mode decimal places. </value>
    Public ReadOnly Property FunctionModeUnits As UnitDictionary

    ''' <summary> Parse units. </summary>
    ''' <param name="functionMode"> The  Multimeter Function Mode. </param>
    ''' <returns> An Arebis.TypedUnits.Unit. </returns>
    Public Overridable Function ToUnit(ByVal functionMode As Integer) As Arebis.TypedUnits.Unit
        Return Me.FunctionModeUnits(functionMode)
    End Function

    ''' <summary> Gets or sets the function unit. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The function unit. </value>
    Public Property FunctionUnit As Arebis.TypedUnits.Unit
        Get
            Return Me.PrimaryReading.Amount.Unit
        End Get
        Protected Set(value As Arebis.TypedUnits.Unit)
            If Me.FunctionUnit <> value Then
                Me.PrimaryReading.ApplyUnit(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Define function clear known state. </summary>
    Protected Overridable Sub DefineFunctionClearKnownState()
        If Me.ReadingAmounts.HasReadingElements Then
            Me.ReadingAmounts.ActiveReadingAmount.ApplyUnit(Me.FunctionUnit)
        End If
        Me.ReadingAmounts.Reset()
        Me.NotifyPropertyChanged(NameOf(MeasureSubsystemBase.ReadingAmounts))
        Me.ParsePrimaryReading(String.Empty)
    End Sub

    ''' <summary> Define function mode read writes. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functionModeReadWrites"> A Dictionary of multimeter function mode parses. </param>
    Public Shared Sub DefineFunctionModeReadWrites(ByVal functionModeReadWrites As Pith.EnumReadWriteCollection)
        If functionModeReadWrites Is Nothing Then Throw New ArgumentNullException(NameOf(functionModeReadWrites))
        functionModeReadWrites.Clear()
        For Each functionMode As VI.MultimeterFunctionModes In [Enum].GetValues(GetType(VI.MultimeterFunctionModes))
            functionModeReadWrites.Add(functionMode)
        Next
    End Sub

    ''' <summary> Define function mode read writes. </summary>
    Private Sub DefineFunctionModeReadWrites()
        Me._FunctionModeReadWrites = New Pith.EnumReadWriteCollection
        MultimeterSubsystemBase.DefineFunctionModeReadWrites(Me.FunctionModeReadWrites)
    End Sub

    ''' <summary> Gets or sets a dictionary of multimeter function mode parses. </summary>
    ''' <value> A Dictionary of multimeter function mode parses. </value>
    Public ReadOnly Property FunctionModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported function modes. </summary>
    Private _SupportedFunctionModes As MultimeterFunctionModes

    ''' <summary>
    ''' Gets or sets the supported Function Modes. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported multimeter function modes. </value>
    Public Property SupportedFunctionModes() As MultimeterFunctionModes
        Get
            Return Me._SupportedFunctionModes
        End Get
        Set(ByVal value As MultimeterFunctionModes)
            If Not Me.SupportedFunctionModes.Equals(value) Then
                Me._SupportedFunctionModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The function mode. </summary>
    Private _FunctionMode As MultimeterFunctionModes?

    ''' <summary> Gets or sets the cached multimeter function mode. </summary>
    ''' <value>
    ''' The <see cref="FunctionMode">multimeter function mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property FunctionMode As MultimeterFunctionModes?
        Get
            Return Me._FunctionMode
        End Get
        Protected Set(ByVal value As MultimeterFunctionModes?)
            If Not Me.FunctionMode.Equals(value) Then
                Me._FunctionMode = value
                If value.HasValue Then
                    Me.FunctionRange = Me.ToRange(value.Value)
                    Me.FunctionUnit = Me.ToUnit(value.Value)
                    Me.FunctionRangeDecimalPlaces = Me.ToDecimalPlaces(value.Value)
                    Me.FunctionOpenDetectorCapable = Me.IsOpenDetectorCapable(value.Value)
                Else
                    Me.FunctionRange = Me.DefaultFunctionRange
                    Me.FunctionUnit = Me.DefaultFunctionUnit
                    Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
                    Me.FunctionOpenDetectorCapable = Me.DefaultOpenDetectorCapable
                End If
                If Not Me.FunctionOpenDetectorCapable Then Me.OpenDetectorEnabled = False
                Me.DefineFunctionClearKnownState()
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the multimeter function mode. </summary>
    ''' <param name="value"> The  multimeter function mode. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">source multimeter function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function ApplyFunctionMode(ByVal value As MultimeterFunctionModes) As MultimeterFunctionModes?
        Me.WriteFunctionMode(value)
        Return Me.QueryFunctionMode()
    End Function

    ''' <summary> Gets or sets the multimeter function mode query command. </summary>
    ''' <value> The multimeter function mode query command. </value>
    Protected MustOverride ReadOnly Property FunctionModeQueryCommand As String

    ''' <summary> Queries the multimeter function mode. </summary>
    ''' <returns>
    ''' The <see cref="FunctionMode">multimeter function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryFunctionMode() As MultimeterFunctionModes?
        Return Me.QueryFunctionMode(Me.FunctionModeQueryCommand)
    End Function

    ''' <summary> Queries print function mode. </summary>
    ''' <returns> The print function mode. </returns>
    Public Overridable Function QueryPrintFunctionMode() As MultimeterFunctionModes?
        Return Me.QueryFunctionMode(Me.Session.BuildQueryPrintCommand(Me.FunctionModeQueryCommand))
    End Function

    ''' <summary> Queries the multimeter function mode. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">multimeter function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryFunctionMode(ByVal queryCommand As String) As MultimeterFunctionModes?
        Me.FunctionMode = Me.Query(Of MultimeterFunctionModes)(queryCommand,
                                                               Me.FunctionMode.GetValueOrDefault(MultimeterFunctionModes.None),
                                                               Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

    ''' <summary> Gets or sets the multimeter function mode command format. </summary>
    ''' <value> The multimeter function mode command format. </value>
    Protected MustOverride ReadOnly Property FunctionModeCommandFormat As String

    ''' <summary>
    ''' Writes the multimeter function mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The multimeter function mode. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">multimeter function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteFunctionMode(ByVal value As MultimeterFunctionModes) As MultimeterFunctionModes?
        Me.FunctionMode = Me.Write(Of MultimeterFunctionModes)(Me.FunctionModeCommandFormat, value, Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

#End Region

#Region " INPUT IMPEDANCE MODE "

    ''' <summary> Define input impedance mode read writes. </summary>
    Private Sub DefineInputImpedanceModeReadWrites()
        Me._InputImpedanceModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As InputImpedanceModes In [Enum].GetValues(GetType(InputImpedanceModes))
            Me._InputImpedanceModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of multimeter Input Impedance Mode parses. </summary>
    ''' <value> A Dictionary of multimeter Input Impedance Mode parses. </value>
    Public ReadOnly Property InputImpedanceModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported input impedance modes. </summary>
    Private _SupportedInputImpedanceModes As InputImpedanceModes

    ''' <summary>
    ''' Gets or sets the supported Input Impedance Modes. This is a subset of the InputImpedances
    ''' supported by the instrument.
    ''' </summary>
    ''' <value> The supported multimeter Input Impedance Modes. </value>
    Public Property SupportedInputImpedanceModes() As InputImpedanceModes
        Get
            Return Me._SupportedInputImpedanceModes
        End Get
        Set(ByVal value As InputImpedanceModes)
            If Not Me.SupportedInputImpedanceModes.Equals(value) Then
                Me._SupportedInputImpedanceModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The input impedance mode. </summary>
    Private _InputImpedanceMode As InputImpedanceModes?

    ''' <summary> Gets or sets the cached multimeter Input Impedance Mode. </summary>
    ''' <value>
    ''' The <see cref="InputImpedanceMode">multimeter Input Impedance Mode</see> or none if not set
    ''' or unknown.
    ''' </value>
    Public Overloads Property InputImpedanceMode As InputImpedanceModes?
        Get
            Return Me._InputImpedanceMode
        End Get
        Protected Set(ByVal value As InputImpedanceModes?)
            If Not Me.InputImpedanceMode.Equals(value) Then
                Me._InputImpedanceMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the multimeter Input Impedance Mode. </summary>
    ''' <param name="value"> The  multimeter Input Impedance Mode. </param>
    ''' <returns>
    ''' The <see cref="InputImpedanceMode">source multimeter Input Impedance Mode</see> or none if
    ''' unknown.
    ''' </returns>
    Public Overridable Function ApplyInputImpedanceMode(ByVal value As InputImpedanceModes) As InputImpedanceModes?
        Me.WriteInputImpedanceMode(value)
        Return Me.QueryInputImpedanceMode()
    End Function

    ''' <summary> Gets or sets the multimeter Input Impedance Mode query command. </summary>
    ''' <value> The multimeter Input Impedance Mode query command. </value>
    Protected Overridable Property InputImpedanceModeQueryCommand As String

    ''' <summary> Queries the multimeter Input Impedance Mode. </summary>
    ''' <returns>
    ''' The <see cref="InputImpedanceMode">multimeter Input Impedance Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryInputImpedanceMode() As InputImpedanceModes?
        Return Me.QueryInputImpedanceMode(Me.InputImpedanceModeQueryCommand)
    End Function

    ''' <summary> Queries print Input Impedance Mode. </summary>
    ''' <returns> The print Input Impedance Mode. </returns>
    Public Overridable Function QueryPrintInputImpedanceMode() As InputImpedanceModes?
        Return Me.QueryInputImpedanceMode(Me.Session.BuildQueryPrintCommand(Me.InputImpedanceModeQueryCommand))
    End Function

    ''' <summary> Queries the multimeter Input Impedance Mode. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="InputImpedanceMode">multimeter Input Impedance Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryInputImpedanceMode(ByVal queryCommand As String) As InputImpedanceModes?
        Me.InputImpedanceMode = Me.Query(Of InputImpedanceModes)(queryCommand,
                                                               Me.InputImpedanceMode.GetValueOrDefault(InputImpedanceModes.None),
                                                               Me.InputImpedanceModeReadWrites)
        Return Me.InputImpedanceMode
    End Function

    ''' <summary> Gets or sets the multimeter Input Impedance Mode command format. </summary>
    ''' <value> The multimeter Input Impedance Mode command format. </value>
    Protected Overridable Property InputImpedanceModeCommandFormat As String

    ''' <summary>
    ''' Writes the multimeter Input Impedance Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The multimeter Input Impedance Mode. </param>
    ''' <returns>
    ''' The <see cref="InputImpedanceMode">multimeter Input Impedance Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteInputImpedanceMode(ByVal value As InputImpedanceModes) As InputImpedanceModes?
        Me.InputImpedanceMode = Me.Write(Of InputImpedanceModes)(Me.InputImpedanceModeCommandFormat, value, Me.InputImpedanceModeReadWrites)
        Return Me.InputImpedanceMode
    End Function

#End Region

#Region " MEASUREMENT UNIT "

    ''' <summary> Define multimeter measurement unit read writes. </summary>
    Private Sub DefineMultimeterMeasurementUnitReadWrites()
        Me._MultimeterMeasurementUnitReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As MultimeterMeasurementUnits In [Enum].GetValues(GetType(MultimeterMeasurementUnits))
            Me._MultimeterMeasurementUnitReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Converts a a Multimeter unit to a measurement unit. </summary>
    ''' <param name="value"> The Multimeter Unit. </param>
    ''' <returns> Value as an Arebis.TypedUnits.Unit. </returns>
    Public Function ToMeasurementUnit(ByVal value As MultimeterMeasurementUnits) As Arebis.TypedUnits.Unit
        Dim result As Arebis.TypedUnits.Unit ' = Me.DefaultFunctionUnit
        Select Case value
            Case MultimeterMeasurementUnits.Celsius
                result = Arebis.StandardUnits.TemperatureUnits.DegreeCelsius
            Case MultimeterMeasurementUnits.Decibel
                result = Arebis.StandardUnits.UnitlessUnits.Decibel
            Case MultimeterMeasurementUnits.Fahrenheit
                result = Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit
            Case MultimeterMeasurementUnits.Kelvin
                result = Arebis.StandardUnits.TemperatureUnits.Kelvin
            Case MultimeterMeasurementUnits.Volt
                result = Arebis.StandardUnits.ElectricUnits.Volt
            Case Else
                result = Me.FunctionUnit
        End Select
        Return result
    End Function

    ''' <summary> Gets or sets a dictionary of multimeter Input Impedance Mode parses. </summary>
    ''' <value> A Dictionary of multimeter Input Impedance Mode parses. </value>
    Public ReadOnly Property MultimeterMeasurementUnitReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported multimeter measurement units. </summary>
    Private _SupportedMultimeterMeasurementUnits As MultimeterMeasurementUnits

    ''' <summary>
    ''' Gets or sets the supported Input Impedance Modes. This is a subset of the InputImpedances
    ''' supported by the instrument.
    ''' </summary>
    ''' <value> The supported multimeter Input Impedance Modes. </value>
    Public Property SupportedMultimeterMeasurementUnits() As MultimeterMeasurementUnits
        Get
            Return Me._SupportedMultimeterMeasurementUnits
        End Get
        Set(ByVal value As MultimeterMeasurementUnits)
            If Not Me.SupportedMultimeterMeasurementUnits.Equals(value) Then
                Me._SupportedMultimeterMeasurementUnits = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The multimeter measurement unit. </summary>
    Private _MultimeterMeasurementUnit As MultimeterMeasurementUnits?

    ''' <summary> Gets or sets the cached multimeter Input Impedance Mode. </summary>
    ''' <value>
    ''' The <see cref="MultimeterMeasurementUnit">multimeter Input Impedance Mode</see> or none if
    ''' not set or unknown.
    ''' </value>
    Public Overloads Property MultimeterMeasurementUnit As MultimeterMeasurementUnits?
        Get
            Return Me._MultimeterMeasurementUnit
        End Get
        Protected Set(ByVal value As MultimeterMeasurementUnits?)
            If Not Me.MultimeterMeasurementUnit.Equals(value) Then
                Me._MultimeterMeasurementUnit = value
                If value.HasValue Then
                    Me.FunctionUnit = Me.ToMeasurementUnit(value.Value)
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the multimeter Input Impedance Mode. </summary>
    ''' <param name="value"> The  multimeter Input Impedance Mode. </param>
    ''' <returns>
    ''' The <see cref="MultimeterMeasurementUnit">source multimeter Input Impedance Mode</see> or
    ''' none if unknown.
    ''' </returns>
    Public Overridable Function ApplyMultimeterMeasurementUnit(ByVal value As MultimeterMeasurementUnits) As MultimeterMeasurementUnits?
        Me.WriteMultimeterMeasurementUnit(value)
        Return Me.QueryMultimeterMeasurementUnit()
    End Function

    ''' <summary> Gets or sets the multimeter Input Impedance Mode query command. </summary>
    ''' <value> The multimeter Input Impedance Mode query command. </value>
    Protected Overridable Property MultimeterMeasurementUnitQueryCommand As String

    ''' <summary> Queries the multimeter Input Impedance Mode. </summary>
    ''' <returns>
    ''' The <see cref="MultimeterMeasurementUnit">multimeter Input Impedance Mode</see> or none if
    ''' unknown.
    ''' </returns>
    Public Overridable Function QueryMultimeterMeasurementUnit() As MultimeterMeasurementUnits?
        Return Me.QueryMultimeterMeasurementUnit(Me.MultimeterMeasurementUnitQueryCommand)
    End Function

    ''' <summary> Queries print Input Impedance Mode. </summary>
    ''' <returns> The print Input Impedance Mode. </returns>
    Public Overridable Function QueryPrintMultimeterMeasurementUnit() As MultimeterMeasurementUnits?
        Return Me.QueryMultimeterMeasurementUnit(Me.Session.BuildQueryPrintCommand(Me.MultimeterMeasurementUnitQueryCommand))
    End Function

    ''' <summary> Queries the multimeter Input Impedance Mode. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="MultimeterMeasurementUnit">multimeter Input Impedance Mode</see> or none if
    ''' unknown.
    ''' </returns>
    Public Overridable Function QueryMultimeterMeasurementUnit(ByVal queryCommand As String) As MultimeterMeasurementUnits?
        Me.MultimeterMeasurementUnit = Me.Query(Of MultimeterMeasurementUnits)(queryCommand,
                                                               Me.MultimeterMeasurementUnit.GetValueOrDefault(MultimeterMeasurementUnits.None),
                                                               Me.MultimeterMeasurementUnitReadWrites)
        Return Me.MultimeterMeasurementUnit
    End Function

    ''' <summary> Gets or sets the multimeter Input Impedance Mode command format. </summary>
    ''' <value> The multimeter Input Impedance Mode command format. </value>
    Protected Overridable Property MultimeterMeasurementUnitCommandFormat As String

    ''' <summary>
    ''' Writes the multimeter Input Impedance Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The multimeter Input Impedance Mode. </param>
    ''' <returns>
    ''' The <see cref="MultimeterMeasurementUnit">multimeter Input Impedance Mode</see> or none if
    ''' unknown.
    ''' </returns>
    Public Overridable Function WriteMultimeterMeasurementUnit(ByVal value As MultimeterMeasurementUnits) As MultimeterMeasurementUnits?
        Me.MultimeterMeasurementUnit = Me.Write(Of MultimeterMeasurementUnits)(Me.MultimeterMeasurementUnitCommandFormat, value, Me.MultimeterMeasurementUnitReadWrites)
        Return Me.MultimeterMeasurementUnit
    End Function

#End Region

End Class

''' <summary> Values that represent input impedance modes. </summary>
<Flags>
Public Enum InputImpedanceModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the automatic option. </summary>
    <ComponentModel.Description("Automatic (dmm.IMPEDANCE_AUTO)")>
    Automatic = 1

    ''' <summary> An enum constant representing the ten mega ohm option. </summary>
    <ComponentModel.Description("Ten Mega Ohm (dmm.IMPEDANCE_10M)")>
    TenMegaOhm = 2
End Enum

''' <summary> Specifies the units. </summary>
<Flags>
Public Enum MultimeterMeasurementUnits

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the volt option. </summary>
    <ComponentModel.Description("Volt (dmm.UNIT_VOLT)")>
    Volt = 1

    ''' <summary> An enum constant representing the decibel option. </summary>
    <ComponentModel.Description("Decibel (dmm.UNIT_DB)")>
    Decibel = 2

    ''' <summary> An enum constant representing the Celsius option. </summary>
    <ComponentModel.Description("Celsius (dmm.UNIT_CELCIUS)")>
    Celsius = 4

    ''' <summary> An enum constant representing the kelvin option. </summary>
    <ComponentModel.Description("Kelvin (dmm.UNIT_KELVIN)")>
    Kelvin = 8

    ''' <summary> An enum constant representing the Fahrenheit option. </summary>
    <ComponentModel.Description("Fahrenheit (dmm.UNIT_FAHRENHEIT)")>
    Fahrenheit = 16
End Enum

''' <summary> Specifies the function modes. </summary>
<Flags>
Public Enum MultimeterFunctionModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the voltage Device-context option. </summary>
    <ComponentModel.Description("DC Voltage (dmm.FUNC_DC_VOLTAGE)")>
    VoltageDC = 1

    ''' <summary> An enum constant representing the voltage a c option. </summary>
    <ComponentModel.Description("AC Voltage (dmm.FUNC_AC_VOLTAGE)")>
    VoltageAC = 2

    ''' <summary> An enum constant representing the current Device-context option. </summary>
    <ComponentModel.Description("DC Current (dmm.FUNC_DC_CURRENT)")>
    CurrentDC = 4

    ''' <summary> An enum constant representing the current a c option. </summary>
    <ComponentModel.Description("AC Current (dmm.FUNC_AC_CURRENT)")>
    CurrentAC = 8

    ''' <summary> An enum constant representing the temperature option. </summary>
    <ComponentModel.Description("Temperature (dmm.FUNC_TEMPERATURE)")>
    Temperature = 16

    ''' <summary> An enum constant representing the resistance common side option. </summary>
    <ComponentModel.Description("Resistance Common Side (commonsideohms)")>
    ResistanceCommonSide = 32

    ''' <summary> An enum constant representing the resistance two wire option. </summary>
    <ComponentModel.Description("Resistance 2-Wire (dmm.FUNC_RESISTANCE)")>
    ResistanceTwoWire = 64

    ''' <summary> An enum constant representing the resistance four wire option. </summary>
    <ComponentModel.Description("Resistance 4-Wire (dmm.FUNC_4W_RESISTANCE)")>
    ResistanceFourWire = 128

    ''' <summary> An enum constant representing the diode option. </summary>
    <ComponentModel.Description("Diode (dmm.FUNC_DIODE)")>
    Diode = 256

    ''' <summary> An enum constant representing the capacitance option. </summary>
    <ComponentModel.Description("Capacitance (dmm.FUNC_CAPACITANCE)")>
    Capacitance = 512

    ''' <summary> An enum constant representing the continuity option. </summary>
    <ComponentModel.Description("Continuity (dmm.FUNC_CONTINUITY)")>
    Continuity = 1024

    ''' <summary> An enum constant representing the frequency option. </summary>
    <ComponentModel.Description("Frequency (dmm.FUNC_ACV_FREQUENCY)")>
    Frequency = 2048

    ''' <summary> An enum constant representing the period option. </summary>
    <ComponentModel.Description("Period (dmm.FUNC_ACV_PERIOD)")>
    Period = 4096

    ''' <summary> An enum constant representing the ratio option. </summary>
    <ComponentModel.Description("Ratio (dmm.FUNC_DCV_RATIO)")>
    Ratio = 8192
End Enum

