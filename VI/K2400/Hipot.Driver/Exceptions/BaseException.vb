''' <summary>
''' An inheritable exception for use by ISR framework classes and applications.
''' </summary>
''' <remarks>
''' Inherits from System.Exception per FxCop design rule CA1958 which specifies that "Types do
''' not extend inheritance vulnerable types" and further explains that "This [Application
''' Exception] base exception type does not provide any additional value for framework classes."
''' <para>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<Serializable()>
Public Class BaseException
    Inherits System.Exception

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The args. </param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="innerException"> Specifies the InnerException. </param>
    ''' <param name="format">         Specifies the exception formatting. </param>
    ''' <param name="args">           Specifies the message arguments. </param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), innerException)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
    ''' data.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        Me._MachineName = info.GetString("machineName")
        Me._CreatedDateTime = info.GetDateTime("createdDateTime")
        Me._AppDomainName = info.GetString("appDomainName")
        Me._ThreadIdentityName = info.GetString("threadIdentity")
        Me._WindowsIdentityName = info.GetString("windowsIdentity")
        Me._OSVersion = info.GetString("OSVersion")
        Me._AdditionalInformation = CType(info.GetValue("additionalInformation",
                                                        GetType(System.Collections.Specialized.NameValueCollection)),
                                                    System.Collections.Specialized.NameValueCollection)
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData" /> method to serialize custom values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="info">    The <see cref="Runtime.Serialization.SerializationInfo">serialization
    '''                        information</see>. </param>
    ''' <param name="context"> The <see cref="Runtime.Serialization.StreamingContext">streaming
    '''                        context</see> for the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue("machineName", Me.MachineName, GetType(String))
        info.AddValue("createdDateTime", Me.CreatedDateTime)
        info.AddValue("appDomainName", Me.AppDomainName, GetType(String))
        info.AddValue("threadIdentity", Me.ThreadIdentityName, GetType(String))
        info.AddValue("windowsIdentity", Me.WindowsIdentityName, GetType(String))
        info.AddValue("OSVersion", Me.OSVersion, GetType(String))
        info.AddValue("additionalInformation", Me.AdditionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

#Region " ADDITIONAL INFORMATION "

    ''' <summary> Information describing the additional. </summary>
    Private _AdditionalInformation As New System.Collections.Specialized.NameValueCollection

    ''' <summary> Collection allowing additional information to be added to the exception. </summary>
    ''' <value> The additional information. </value>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            Return Me._AdditionalInformation
        End Get
    End Property

    ''' <summary> Name of the application domain. </summary>
    Private _AppDomainName As String

    ''' <summary> AppDomain name where the Exception occurred. </summary>
    ''' <value> The name of the application domain. </value>
    Public ReadOnly Property AppDomainName() As String
        Get
            Return Me._AppDomainName
        End Get
    End Property

    ''' <summary> The created date time. </summary>
    Private _CreatedDateTime As DateTimeOffset = DateTimeOffset.Now

    ''' <summary> Date and Time the exception was created. </summary>
    ''' <value> The created date time. </value>
    Public ReadOnly Property CreatedDateTime() As DateTimeOffset
        Get
            Return Me._CreatedDateTime
        End Get
    End Property

    ''' <summary> Name of the machine. </summary>
    Private _MachineName As String

    ''' <summary> Machine name where the Exception occurred. </summary>
    ''' <value> The name of the machine. </value>
    Public ReadOnly Property MachineName() As String
        Get
            Return Me._MachineName
        End Get
    End Property

    ''' <summary> The operating system version. </summary>
    Private _OSVersion As String

    ''' <summary> Gets the OS version. </summary>
    ''' <value> The OS version. </value>
    Public ReadOnly Property OSVersion() As String
        Get
            Return Me._OSVersion
        End Get
    End Property

    ''' <summary> Name of the thread identity. </summary>
    Private _ThreadIdentityName As String

    ''' <summary> Identity of the executing thread on which the exception was created. </summary>
    ''' <value> The name of the thread identity. </value>
    Public ReadOnly Property ThreadIdentityName() As String
        Get
            Return Me._ThreadIdentityName
        End Get
    End Property

    ''' <summary> Name of the windows identity. </summary>
    Private _WindowsIdentityName As String

    ''' <summary> Windows identity under which the code was running. </summary>
    ''' <value> The name of the windows identity. </value>
    Public ReadOnly Property WindowsIdentityName() As String
        Get
            Return Me._WindowsIdentityName
        End Get
    End Property

    ''' <summary> Function That gathers environment information safely. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ObtainEnvironmentInformation()

        Const unknown As String = "N/A"
        Me._CreatedDateTime = DateTimeOffset.Now
        Me._CreatedDateTime = DateTimeOffset.Now

        Me._MachineName = Environment.MachineName
        If Me._MachineName.Length = 0 Then
            Me._MachineName = unknown
        End If

        Me._ThreadIdentityName = System.Threading.Thread.CurrentPrincipal.Identity.Name
        If Me._ThreadIdentityName.Length = 0 Then
            Me._ThreadIdentityName = unknown
        End If

        Me._WindowsIdentityName = System.Security.Principal.WindowsIdentity.GetCurrent().Name
        If Me._WindowsIdentityName.Length = 0 Then
            Me._WindowsIdentityName = unknown
        End If

        Me._AppDomainName = AppDomain.CurrentDomain.FriendlyName
        If Me._AppDomainName.Length = 0 Then
            Me._AppDomainName = unknown
        End If

        Me._OSVersion = Environment.OSVersion.ToString
        If Me._OSVersion.Length = 0 Then
            Me._OSVersion = unknown
        End If

        Me._AdditionalInformation = New System.Collections.Specialized.NameValueCollection From {
            {AdditionalInfoItem.MachineName.ToString, My.Computer.Name},
            {AdditionalInfoItem.Timestamp.ToString, DateTimeOffset.Now.ToString(System.Globalization.CultureInfo.CurrentCulture)},
            {AdditionalInfoItem.FullName.ToString, Reflection.Assembly.GetExecutingAssembly().FullName},
            {AdditionalInfoItem.AppDomainName.ToString, AppDomain.CurrentDomain.FriendlyName},
            {AdditionalInfoItem.ThreadIdentity.ToString, My.User.Name}, ' Thread.CurrentPrincipal.Identity.Name
            {AdditionalInfoItem.WindowsIdentity.ToString, My.Computer.Info.OSFullName},
            {AdditionalInfoItem.OSVersion.ToString, My.Computer.Info.OSVersion}
        }

    End Sub

#End Region

    ''' <summary> Specifies the contents of the additional information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Enum AdditionalInfoItem

        ''' <summary>
        ''' The none
        ''' </summary>
        None

        ''' <summary>
        ''' The machine name
        ''' </summary>
        MachineName

        ''' <summary>
        ''' The timestamp
        ''' </summary>
        Timestamp

        ''' <summary>
        ''' The full name
        ''' </summary>
        FullName

        ''' <summary>
        ''' The application domain name
        ''' </summary>
        AppDomainName

        ''' <summary>
        ''' The thread identity
        ''' </summary>
        ThreadIdentity

        ''' <summary>
        ''' The windows identity
        ''' </summary>
        WindowsIdentity

        ''' <summary>
        ''' The OS version
        ''' </summary>
        OSVersion
    End Enum

End Class
