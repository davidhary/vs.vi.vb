'---------------------------------------------------------------------------------------------------
' file:		.\UI\VisaTreeViewForm.vb
'
' summary:	Visa tree view Windows Form
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions
Imports isr.Core.SplitExtensions

''' <summary> Form for viewing the visa tree view. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class VisaTreeViewForm
    Inherits isr.VI.Facade.VisaTreeViewForm

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        Me.New(CommandLineInfo.VirtualInstrument.GetValueOrDefault(VirtualInstrument.ScpiSession))
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="instrumentOption"> The instrument option. </param>
    Public Sub New(ByVal instrumentOption As VirtualInstrument)
        MyBase.New()
        Me.InstrumentOption = instrumentOption
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.VisaTreeView IsNot Nothing Then
            Dim session As VisaSessionBase = Me.VisaTreeView.VisaSessionBase
            Me.VisaTreeView.Dispose() : Me.VisaTreeView = Nothing
            ' the session needs to be disposed after the device releases the session.
            If session IsNot Nothing Then session.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        If Not Me.VisaTreeViewEnabled Then Return
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim inst As VirtualInstrument = CommandLineInfo.VirtualInstrument.GetValueOrDefault(VirtualInstrument.ScpiSession)
            If inst = VirtualInstrument.Switchboard Then inst = VirtualInstrument.ScpiSession
            Select Case inst

                Case VirtualInstrument.Ats600
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ATS600ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ATS600ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.EG2000
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.EG2000ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.EG2000ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2002
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2002ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2002ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2400
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2400ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2400ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2450
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2450ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2450ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K2600
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                    End If
                    If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                    End If
                Case VirtualInstrument.K2700
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2700ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                    End If
                    If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2700ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                    End If
                Case VirtualInstrument.K3700rs
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K3700
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K3700ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K7000
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K7000ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K7000ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K7500
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.K7500S
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K7500ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.T1750
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.T1750ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.T1750ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.TspRig
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.K2600ResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case VirtualInstrument.ScpiSession
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case VirtualInstrument.TspSession
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.TspSessionResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.TspSessionResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case VirtualInstrument.Tsp2Session
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.Tsp2SessionResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.Tsp2SessionResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If

                Case Else
                    If Me.VisaTreeView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceName = Me.VisaTreeView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceTitle = Me.VisaTreeView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
            End Select

        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Gets the visa tree view enabled. </summary>
    ''' <value> The visa tree view enabled. </value>
    Private ReadOnly Property VisaTreeViewEnabled As Boolean
        Get
            Return Me.VisaTreeView IsNot Nothing
        End Get
    End Property

    ''' <summary> Gets or sets the instrument option. </summary>
    ''' <value> The instrument option. </value>
    Public Property InstrumentOption As VirtualInstrument

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try
            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)
            If Not Me.DesignMode Then
                ' add listeners before the first talker publish command
                Me.AddPrivateListeners()
                Me.AddListener(My.Application.Logger)
                Me.LoadInstrumentView(Me.InstrumentOption)
            End If
            ' set the form caption
            Dim suffix As String = If(My.MyApplication.InDesignMode, ".IDE", "")

            ' #If rc Then
            '            suffix = If(My.MyApplication.InDesignMode, ".IDE.RC", ".RC")
            ' #End If            '
#If rc Then
            suffix = If(My.MyApplication.InDesignMode, ".IDE.RC", ".RC")
#End If            ' 
            Me.Text = isr.Core.ApplicationInfo.BuildApplicationDescriptionCaption($"{NameOf(VisaTreeViewForm).SplitWords}{suffix}")
            Windows.Forms.Application.DoEvents()
        Catch ex As Exception
            ex.Data.Add("@isr", "Exception loading the form.")
            My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
            If isr.Core.MyDialogResult.Abort = isr.Core.WindowsForms.ShowDialogAbortIgnore(ex, isr.Core.MyMessageBoxIcon.Error) Then
                Application.Exit()
            End If
        Finally
            Try
                MyBase.OnLoad(e)
                Trace.CorrelationManager.StopLogicalOperation()
            Finally
            End Try
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Loads instrument view. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="instrumentOption"> The instrument option. </param>
    Public Sub LoadInstrumentView(ByVal instrumentOption As VirtualInstrument)
        If instrumentOption = VirtualInstrument.Switchboard Then instrumentOption = VirtualInstrument.ScpiSession
        Select Case instrumentOption

            Case VirtualInstrument.Ats600
                Dim device As New VI.Ats600.Ats600Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.ATS600ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ATS600ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Ats600.Forms.Ats600TreeView(device)

            Case VirtualInstrument.EG2000
                Dim device As New VI.EG2000.EG2000Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.EG2000ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.EG2000ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.EG2000.Forms.EG2000TreeView(device)
            Case VirtualInstrument.K2002
                Dim device As New VI.K2002.K2002Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K2002ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2002ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.K2002.Forms.TreeView(device)
            Case VirtualInstrument.K2400
                Dim device As New VI.K2400.K2400Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K2400ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2400ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.K2400.Forms.K2400TreeView(device)

            Case VirtualInstrument.K2450
                Dim device As New VI.Tsp2.K2450.K2450Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K2450ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2450ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Tsp2.K2450.Forms.K2450TreeView(device)

            Case VirtualInstrument.K2600
                Dim device As New VI.Tsp.K2600.K2600Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K2600ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2600ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Tsp.K2600.Forms.K2600TreeView(device)

            Case VirtualInstrument.K2700
                Dim device As New VI.K2700.K2700Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K2700ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2700ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.K2700.Forms.K2700TreeView(device)

            Case VirtualInstrument.K3700rs
                Dim device As New VI.Tsp.K3700.K3700Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K3700ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K3700ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Tsp.K3700.Forms.K3700TreeView(device) With {.WithResistancesMetersView = True}

            Case VirtualInstrument.K3700
                Dim device As New VI.Tsp.K3700.K3700Device With {
                    .CandidateResourceTitle = My.MySettings.Default.K3700ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K3700ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Tsp.K3700.Forms.K3700TreeView(device) With {.WithResistancesMetersView = False}

            Case VirtualInstrument.K7500
                Dim device As New VI.Tsp2.K7500.K7500Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K7500ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K7500ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Tsp2.K7500.Forms.K7500TreeView(device)

            Case VirtualInstrument.T1750
                Dim device As New VI.T1700.T1700Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.T1750ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.T1750ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.T1700.Forms.T1700TreeView(device)

            Case VirtualInstrument.TspRig
                Dim device As New VI.Tsp.Rig.TspDevice With {
                    .SubsystemSupportMode = SubsystemSupportMode.Native,
                    .CandidateResourceTitle = My.MySettings.Default.K2600ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K2600ResourceName
                }
                Me.VisaTreeView = New VI.Tsp.Rig.Forms.RigTreeView(device)
            Case VirtualInstrument.K7000
                Dim device As New VI.K7000.K7000Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K7000ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K7000ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.K7000.Forms.TreeView(device)

            Case VirtualInstrument.K7500S
                Dim device As New VI.K7500.K7500Device With {
                    .SubsystemSupportMode = SubsystemSupportMode.Full,
                    .CandidateResourceTitle = My.MySettings.Default.K7500ResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.K7500ResourceName
                }
                device.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.K7500.Forms.TreeView(device)

            Case VirtualInstrument.ScpiSession

                Dim session As New VisaSession With {
                    .SubsystemSupportMode = SubsystemSupportMode.Native,
                    .CandidateResourceTitle = My.MySettings.Default.ScpiSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ScpiSessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Facade.VisaTreeView(session)

            Case VirtualInstrument.TspSession

                Dim session As New VisaSession With {
                    .SubsystemSupportMode = SubsystemSupportMode.Native,
                    .CandidateResourceTitle = My.MySettings.Default.TspSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.TspSessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Facade.VisaTreeView(session)

            Case VirtualInstrument.Tsp2Session

                Dim session As New VisaSession With {
                    .SubsystemSupportMode = SubsystemSupportMode.Native,
                    .CandidateResourceTitle = My.MySettings.Default.Tsp2SessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.Tsp2SessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Facade.VisaTreeView(session)

            Case Else

                Dim session As New VisaSession With {
                    .SubsystemSupportMode = SubsystemSupportMode.Native,
                    .CandidateResourceTitle = My.MySettings.Default.ScpiSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ScpiSessionResourceName
                }
                session.AddListeners(Me.Talker)
                Me.VisaTreeView = New VI.Facade.VisaTreeView(session)

        End Select
        Me.VisaTreeViewDisposeEnabled = True
        If Me.VisaTreeViewEnabled Then
            Me.VisaTreeView.ApplyTalkerTraceLevel(Core.ListenerType.Display, TraceEventType.Verbose)
            Me.VisaTreeView.ApplyTalkerTraceLevel(Core.ListenerType.Logger, TraceEventType.Verbose)
        End If
    End Sub



#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
