'---------------------------------------------------------------------------------------------------
' file:		.\Extensions\BindingEventHandlers.vb
'
' summary:	Binding event handlers class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.WinForms.BindingExtensions

''' <summary> A binding event handlers. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Module BindingEventHandlers

    ''' <summary> The hexadecimal format provider. </summary>
    Private ReadOnly HexFormatProvider As Pith.HexFormatProvider = Pith.HexFormatProvider.FormatProvider(2)

    ''' <summary> The display x coordinate 2 event handler. </summary>
    Friend DisplayX2EventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                              e.Value = HexFormatProvider.Format(CInt(e.Value))
                                                              ' e.Value = If(e.Value Is Nothing, String.Empty, String . Format("{0:X2}", CInt(e.Value)))
                                                              ' e.Value = String . Format("0x{0:X2}", CInt(e.Value))
                                                          End Sub

    ''' <summary> The display register event handler. </summary>
    Friend DisplayRegisterEventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                                    e.Value = HexFormatProvider.Format(e.Value)
                                                                    ' this works: 
                                                                    ' e.Value = Pith.RegisterValueFormatProvider.Format(Pith.RegisterValueFormatProvider.DefaultFormatString,
                                                                    '               Pith.RegisterValueFormatProvider.DefaultNullValueCaption,
                                                                    '              e.Value)
                                                                    ' this works: e.Value = String .Format("0x{0:X2}", CInt(e.Value))
                                                                    ' this works: e.DisplayEnumValue("0x{0:X2}")
                                                                End Sub

    ''' <summary> The parse status register event handler. </summary>
    Friend ParseStatusRegisterEventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                                        e.Value = HexFormatProvider.ToServiceRequests(e.Value.ToString)
                                                                    End Sub

    ''' <summary> The parse standard register event handler. </summary>
    Friend ParseStandardRegisterEventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                                          e.Value = HexFormatProvider.ToStandardEvents(e.Value.ToString)
                                                                      End Sub

    ''' <summary> The invert display handler. </summary>
    Friend InvertDisplayHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                             e.WriteInverted
                                                         End Sub

End Module
