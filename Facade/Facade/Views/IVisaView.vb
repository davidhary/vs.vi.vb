﻿''' <summary> Interface for visa view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IVisaView
    Inherits IDisposable, Core.ITalker

    ''' <summary> Gets or sets the visa session base. </summary>
    ''' <value> The visa session base. </value>
    ReadOnly Property VisaSessionBase As VisaSessionBase

    ''' <summary> Gets or sets the display view. </summary>
    ''' <value> The display view. </value>
    ReadOnly Property DisplayView As DisplayView

    ''' <summary> Gets or sets the status view. </summary>
    ''' <value> The status view. </value>
    ReadOnly Property StatusView As StatusView

    ''' <summary> Gets or sets the number of views. </summary>
    ''' <value> The number of views. </value>
    ReadOnly Property ViewCount As Integer

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <param name="view"> The control. </param>
    Sub AddView(ByVal view As VisaViewControl)

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <param name="view">        The View control. </param>
    ''' <param name="viewIndex">   Zero-based index of the view. </param>
    ''' <param name="viewName">    Name of the view. </param>
    ''' <param name="viewCaption"> The view caption. </param>
    Sub AddView(ByVal view As Windows.Forms.Control, ByVal viewIndex As Integer, ByVal viewName As String, ByVal viewCaption As String)

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <param name="view">        The control. </param>
    ''' <param name="viewIndex">   Zero-based index of the view. </param>
    ''' <param name="viewName">    The name of the view. </param>
    ''' <param name="viewCaption"> The caption of the view. </param>
    Sub AddView(ByVal view As Core.Forma.ModelViewTalkerBase, ByVal viewIndex As Integer, ByVal viewName As String, ByVal viewCaption As String)

    ''' <summary> Gets or sets the number of internal resource names. </summary>
    ''' <value> The number of internal resource names. </value>
    ReadOnly Property InternalResourceNamesCount As Integer

    ''' <summary> Gets or sets the name of the internal selected resource. </summary>
    ''' <value> The name of the internal selected resource. </value>
    ReadOnly Property InternalSelectedResourceName As String

End Interface

''' <summary> A visa view control. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure VisaViewControl

    ''' <summary> Constructor. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="index">   Zero-based index of the control. </param>
    ''' <param name="name">    Name of the control. </param>
    ''' <param name="caption"> The control caption. </param>
    Public Sub New(ByVal control As Windows.Forms.Control, ByVal index As Integer, ByVal name As String, ByVal caption As String)
        Me.Control = control
        Me.Index = index
        Me.Name = name
        Me.Caption = caption
    End Sub

    ''' <summary> Gets or sets the control. </summary>
    ''' <value> The control. </value>
    Public ReadOnly Property Control As Windows.Forms.Control

    ''' <summary> Gets or sets the zero-based index of this object. </summary>
    ''' <value> The index. </value>
    Public ReadOnly Property Index As Integer

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public ReadOnly Property Name As String

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    Public ReadOnly Property Caption As String
End Structure

