'---------------------------------------------------------------------------------------------------
' file:		.\Views\SessionView.vb
'
' summary:	Session view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.EscapeSequencesExtensions
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> A simple read write control. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-12-29 </para>
''' </remarks>
Public Class SessionView
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Public Sub New()
        MyBase.New
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.BindSettings()
        Me.RefreshCommandListThis()
        Me.OnConnectionChanged()
        Me.InitializingComponents = False
        Me.AppendTermination = True
        Me._ReadTextBox.Text = $"'Append Termination' is {If(Me.AppendTermination, "", "un")}checked--Termination characters are {If(Me.AppendTermination, "not", "")}required;
Write *ESE 255{My.Settings.Termination} if opening a stand alone VISA Session"
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " BIND SETTINGS "

    ''' <summary> Bind settings. </summary>
    Private Sub BindSettings()

        Me._StatusReadDelayNumeric.NumericUpDownControl.Minimum = 0
        Me._StatusReadDelayNumeric.NumericUpDownControl.Maximum = My.Settings.MaximumDelay
        Me._StatusReadDelayNumeric.NumericUpDownControl.DataBindings.Add(NameOf(NumericUpDown.Maximum), My.Settings, NameOf(My.Settings.MaximumDelay))
        Me._StatusReadDelayNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._StatusReadDelayNumeric.NumericUpDownControl.Value = My.Settings.StatusReadDelay
        Me._StatusReadDelayNumeric.NumericUpDownControl.DataBindings.Add(NameOf(NumericUpDown.Value), My.Settings, NameOf(My.Settings.StatusReadDelay))

        Me._ReadDelayNumeric.NumericUpDownControl.Minimum = 0
        Me._ReadDelayNumeric.NumericUpDownControl.Maximum = My.Settings.MaximumDelay
        Me._ReadDelayNumeric.NumericUpDownControl.DataBindings.Add(NameOf(NumericUpDown.Maximum), My.Settings, NameOf(My.Settings.MaximumDelay))
        Me._ReadDelayNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._ReadDelayNumeric.NumericUpDownControl.Value = My.Settings.ReadDelay
        Me._ReadDelayNumeric.NumericUpDownControl.DataBindings.Add(NameOf(NumericUpDown.Value), My.Settings, NameOf(My.Settings.ReadDelay))

        ' Me._Termination = isr.Core.Agnostic.EscapeSequencesExtensions.NewLineEscape
        Me._Termination = My.Settings.Termination

    End Sub

#End Region

#Region " VISA SESSION BASE (DEVICE BASE) "

    ''' <summary> Gets the visa session base. </summary>
    ''' <value> The visa session base. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase

    ''' <summary> Binds the visa session to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session view model. </param>
    Public Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            RemoveHandler Me.VisaSessionBase.PropertyChanged, AddressOf Me.VisaSessionPropertyChanged
            Me._VisaSessionBase = Nothing
        End If
        If visaSessionBase IsNot Nothing Then
            Me._VisaSessionBase = visaSessionBase
            AddHandler Me.VisaSessionBase.PropertyChanged, AddressOf Me.VisaSessionPropertyChanged
        End If
        Me.BindSessionBase(visaSessionBase)
    End Sub

    ''' <summary> Handles the session property changed action. </summary>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Sub HandlePropertyChanged(ByVal sender As isr.VI.VisaSessionBase, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(isr.VI.VisaSessionBase.ServiceRequestReading)
                If Me._ShowServiceRequestReadingMenuItem.Checked Then
                    Me.UpdateReadMessage(sender.ServiceRequestReading)
                End If
            Case NameOf(VI.VisaSessionBase.PollReading)
                If Me._ShowPollReadingsMenuItem.Checked Then
                    Me.UpdateReadMessage(sender.PollReading)
                End If
        End Select
    End Sub

    ''' <summary> Handles the Session property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub VisaSessionPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.VisaSessionBase)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, VI.VisaSessionBase), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SESSION BASE "

    ''' <summary> Gets the session. </summary>
    ''' <value> The session. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property SessionBase As VI.Pith.SessionBase

    ''' <summary> Binds the Session base to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session base. </param>
    Private Sub BindSessionBase(ByVal visaSessionBase As isr.VI.VisaSessionBase)
        If visaSessionBase Is Nothing Then
            Me.BindSessionBaseThis(Nothing)
        Else
            Me.BindSessionBaseThis(visaSessionBase.Session)
        End If
    End Sub

    ''' <summary> Bind session base. </summary>
    ''' <param name="sessionBase"> The session. </param>
    Private Sub BindSessionBaseThis(ByVal sessionBase As isr.VI.Pith.SessionBase)
        If Me.SessionBase IsNot Nothing Then
            Me.SessionBase.StatusPrompt = $"Releasing session {Me.SessionBase.ResourceNameCaption}"
            Me.BindSessionViewModel(False, Me.SessionBase)
            Me._SessionBase = Nothing
        End If
        If sessionBase IsNot Nothing Then
            Me._SessionBase = sessionBase
            Me.BindSessionViewModel(True, Me.SessionBase)
        End If
        Me.OnConnectionChanged()
    End Sub

    ''' <summary> Gets the sentinel indication having an open session. </summary>
    ''' <value> The is open. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.SessionBase IsNot Nothing AndAlso Me.SessionBase.IsSessionOpen
        End Get
    End Property

    ''' <summary> Executes the connection changed action. </summary>
    Private Sub OnConnectionChanged()
        Me._ClearSessionMenuItem.Enabled = Me.IsSessionOpen
        Me._QueryButton.Enabled = Me.IsSessionOpen
        Me._ReadButton.Enabled = Me.IsSessionOpen
        Me._WriteButton.Enabled = Me.IsSessionOpen
        Me._ReadStatusMenuItem.Enabled = Me.IsSessionOpen
        Me._EraseDisplayMenuItem.Enabled = True
        If Me.IsSessionOpen Then
            Me.SessionBase.StatusPrompt = "Session Open"
            Me.ReadStatusRegister()
        ElseIf Me.SessionBase IsNot Nothing Then
            Me.SessionBase.StatusPrompt = "Session Not open; open session"
        End If
    End Sub

    ''' <summary> Bind session view model. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The session view model. </param>
    Private Sub BindSessionViewModel(ByVal add As Boolean, ByVal viewModel As isr.VI.Pith.SessionBase)
        Me.AddRemoveBinding(Me._ClearSessionMenuItem, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        Me.AddRemoveBinding(Me._QueryButton, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        Me.AddRemoveBinding(Me._ReadButton, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        Me.AddRemoveBinding(Me._WriteButton, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        Me.AddRemoveBinding(Me._ReadStatusMenuItem, add, NameOf(Control.Enabled), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen))
        Me._EraseDisplayMenuItem.Enabled = True
    End Sub

    ''' <summary> Reads delay numeric value changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadDelayNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _ReadDelayNumeric.ValueChanged
        If Me.SessionBase IsNot Nothing Then
            Me.SessionBase.ReadDelay = TimeSpan.FromMilliseconds(Me._ReadDelayNumeric.Value)
        End If
    End Sub

    ''' <summary> Status read delay numeric value changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StatusReadDelayNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _StatusReadDelayNumeric.ValueChanged
        If Me.SessionBase IsNot Nothing Then
            Me.SessionBase.StatusReadDelay = TimeSpan.FromMilliseconds(Me._StatusReadDelayNumeric.Value)
        End If
    End Sub


#End Region

#Region " COMMANDS "

    ''' <summary> The termination. </summary>
    Private _Termination As String

    ''' <summary> Gets or sets the termination. </summary>
    ''' <value> The termination. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Termination As String
        Get
            Return Me._Termination
        End Get
        Set(value As String)
            If Not String.Equals(Me.Termination, value) Then
                Me._Termination = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Dictionary of commands. </summary>
    Private _CommandDictionary As Dictionary(Of String, String)

    ''' <summary> Command dictionary. </summary>
    ''' <returns> A Dictionary(Of String, String) </returns>
    Private Function CommandDictionary() As IDictionary(Of String, String)
        If Me._CommandDictionary Is Nothing Then
            Me._CommandDictionary = New Dictionary(Of String, String)
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand)
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.IdentityQueryCommand)
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.OperationCompleteCommand)
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.OperationCompletedQueryCommand)
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.ResetKnownStateCommand)
            Me.AddCommandThis(String.Format(Globalization.CultureInfo.CurrentCulture, VI.Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat, 255))
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand)
            Me.AddCommandThis(String.Format(Globalization.CultureInfo.CurrentCulture, VI.Pith.Ieee488.Syntax.StandardEventEnableCommandFormat, 255))
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand)
            Me.AddCommandThis(VI.Pith.Ieee488.Syntax.WaitCommand)
        End If
        Return Me._CommandDictionary
    End Function

    ''' <summary> Clears the commands. </summary>
    Public Sub ClearCommands()
        Me._CommandDictionary.Clear()
    End Sub

    ''' <summary> Inserts a command. </summary>
    ''' <param name="value">       The value. </param>
    ''' <param name="termination"> The termination. </param>
    Private Sub InsertCommandThis(ByVal value As String, ByVal termination As String)
        If Not Me.CommandDictionary.ContainsKey(value) Then
            Dim dix As New Dictionary(Of String, String)(Me.CommandDictionary)
            Me.CommandDictionary.Clear()
            Me.CommandDictionary.Add(value, value & termination)
            For Each key As String In dix.Keys
                Me.CommandDictionary.Add(key, dix.Item(key))
            Next
        End If
    End Sub

    ''' <summary> Inserts a command. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub InsertCommandThis(ByVal value As String)
        Me.InsertCommandThis(value, My.Settings.Termination)
    End Sub

    ''' <summary> Inserts a command described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub InsertCommand(ByVal value As String)
        Me.InsertCommandThis(value)
    End Sub

    ''' <summary> Adds a command. </summary>
    ''' <param name="value">       The value. </param>
    ''' <param name="termination"> The termination. </param>
    Private Sub AddCommandThis(ByVal value As String, ByVal termination As String)
        If Not Me.CommandDictionary.ContainsKey(value) Then
            Me.CommandDictionary.Add(value, value & termination)
        End If
    End Sub

    ''' <summary> Adds a command. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AddCommandThis(ByVal value As String)
        Me.AddCommandThis(value, My.Settings.Termination)
    End Sub

    ''' <summary> Adds a command. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub AddCommand(ByVal value As String)
        Me.AddCommandThis(value)
    End Sub

    ''' <summary> Refresh command list. </summary>
    Private Sub RefreshCommandListThis()
        Me._WriteComboBox.Items.Clear()
        Me._WriteComboBox.Items.AddRange(Me.CommandDictionary.Values.ToArray)
        Me._WriteComboBox.SelectedIndex = 1
    End Sub

    ''' <summary> Refresh command list. </summary>
    Public Sub RefreshCommandList()
        Me.RefreshCommandListThis()
    End Sub

#End Region

#Region " READ And WRITE "

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadStatusRegister()
        Dim activity As String = String.Empty
        Try
            If Me.IsSessionOpen Then
                activity = $"reading status byte after {Me.SessionBase.StatusReadDelay.TotalMilliseconds}ms delay" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.DelayReadStatusRegister()
            End If
        Catch ex As Exception
            Me._ReadTextBox.Text = ex.ToFullBlownString
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Gets or sets the append termination to the entered commands. </summary>
    ''' <value> The append termination sentinel. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property AppendTermination As Boolean
        Get
            Return Me._AppendTerminationMenuItem.Checked
        End Get
        Set(value As Boolean)
            Me._AppendTerminationMenuItem.Checked = value
        End Set
    End Property

    ''' <summary> Queries. </summary>
    ''' <param name="textToWrite"> The text to write. </param>
    Public Sub Query(ByVal textToWrite As String)
        Me.Write(textToWrite)
        Me.Read()
    End Sub

    ''' <summary> Writes. </summary>
    ''' <param name="textToWrite"> The text to write. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Write(ByVal textToWrite As String)
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Dim activity As String = String.Empty
        Try
            activity = $"writing {textToWrite}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
            Me.SessionBase.StartElapsedStopwatch()
            If Me.AppendTermination Then
                textToWrite = textToWrite.RemoveCommonEscapeSequences()
                Me.SessionBase.WriteLine(textToWrite)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime()
            Else
                textToWrite = textToWrite.ReplaceCommonEscapeSequences
                Me.SessionBase.Write(textToWrite)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime()
            End If
            activity = $"done {activity}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
        Catch ex As Exception
            Me._ReadTextBox.Text = ex.ToFullBlownString
            Me.PublishException(activity, ex)
            Me.SessionBase.StatusPrompt = $"failed {activity}"
        Finally
            Me.SessionBase.ReadStatusRegister()
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads a message from the session. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Async Sub Read()
        Dim activity As String = String.Empty
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            activity = "Awaiting read delay" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
            Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
            Windows.Forms.Application.DoEvents()
            Me.SessionBase.StartElapsedStopwatch()
            Await Threading.Tasks.Task.Delay(Me.SessionBase.ReadDelay)
            activity = "Reading status register" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
            Me.SessionBase.ReadStatusRegister()
            If Me.SessionBase.MessageAvailable Then
                activity = "reading" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Dim responseString As String = Me.SessionBase.ReadLine()
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
                Dim message As String = responseString.InsertCommonEscapeSequences
                activity = $"received: {message}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.UpdateReadMessage(message)
            Else
                activity = "nothing to read" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
            End If
        Catch ex As Exception
            Me._ReadTextBox.Text = ex.ToFullBlownString
            Me.PublishException(activity, ex)
            Me.SessionBase.StatusPrompt = $"failed {activity}"
        Finally
            Me.ReadStatusRegister()
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Erases the text boxes. </summary>
    Public Sub [Erase]()
        Me._ReadTextBox.Text = String.Empty
    End Sub

    ''' <summary> Clears the session. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearSession()
        Dim activity As String = String.Empty
        Try
            If Me._SessionBase IsNot Nothing Then
                activity = "clearing active state" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
                Me.SessionBase.StartElapsedStopwatch()
                Me.SessionBase.ClearActiveState(Me.SessionBase.DeviceClearRefractoryPeriod)
                Me.SessionBase.ElapsedTime = Me.SessionBase.ReadElapsedTime(True)
                activity = $"done {activity}" : Me.SessionBase.StatusPrompt = activity : Me.PublishInfo($"{activity};. ")
            End If
        Catch ex As Exception
            Me._ReadTextBox.Text = ex.ToFullBlownString
            Me.PublishException(activity, ex)
            Me.SessionBase.StatusPrompt = $"failed {activity}"
        Finally
            Me.ReadStatusRegister()
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Reads status button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadStatusMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadStatusMenuItem.Click
        Me.ReadStatusRegister()
    End Sub

    ''' <summary> Queries (write and then reads) the instrument. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub QueryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _QueryButton.Click
        Me.Query(Me._WriteComboBox.Text)
    End Sub

    ''' <summary> Writes a message to the instrument. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub WriteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _WriteButton.Click
        Me.Write(Me._WriteComboBox.Text)
    End Sub

    ''' <summary> Updates the read message described by message. </summary>
    ''' <param name="message"> The message. </param>
    Private Sub UpdateReadMessage(ByVal message As String)
        Dim builder As New System.Text.StringBuilder()
        If Me._ReadTextBox.Text.Length > 0 Then
            builder.AppendLine(Me._ReadTextBox.Text)
        End If
        builder.Append(message)
        Me._ReadTextBox.Text = builder.ToString
    End Sub

    ''' <summary> Reads a message from the instrument. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
        Me.Read()
    End Sub

    ''' <summary> Event handler. Called by _ClearSessionButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearSessionMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ClearSessionMenuItem.Click
        Me.ClearSession()
    End Sub

    ''' <summary> Event handler. Called by clear for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EraseDisplayMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EraseDisplayMenuItem.Click
        Me.Erase()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

