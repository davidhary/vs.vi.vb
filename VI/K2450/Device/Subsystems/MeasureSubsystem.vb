''' <summary> Measure subsystem. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-03 </para>
''' </remarks>
Public Class MeasureSubsystem
    Inherits VI.Tsp2.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ampere)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere
        Me.SupportedFunctionModes = SenseFunctionModes.CurrentDC Or SenseFunctionModes.VoltageDC Or SenseFunctionModes.Resistance
        MyBase.FunctionModeRanges.Item(VI.SenseFunctionModes.CurrentDC).SetRange(0, 1)
        MyBase.FunctionModeRanges.Item(VI.SenseFunctionModes.VoltageDC).SetRange(0, 200)
        MyBase.FunctionModeRanges.Item(VI.SenseFunctionModes.Resistance).SetRange(0, 200000000.0)
        MyBase.FunctionModeRanges.Item(VI.SenseFunctionModes.ResistanceFourWire).SetRange(0, 200000000.0)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.ApertureRange = New isr.Core.Constructs.RangeR(0.000166667, 0.166667)
        Me.FilterCountRange = New isr.Core.Constructs.RangeI(1, 100)
        Me.FilterWindowRange = New isr.Core.Constructs.RangeR(0, 0.1)
        Me.PowerLineCyclesRange = New isr.Core.Constructs.RangeR(0.01, 10)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PowerLineCycles = 1
        Me.AutoRangeState = OnOffState.On
        Me.AutoZeroEnabled = True
        Me.FilterCount = 10
        Me.FilterEnabled = False
        Me.MovingAverageFilterEnabled = False
        Me.OpenDetectorEnabled = False
        Me.FilterWindow = 0.001
        Me.FunctionMode = VI.SenseFunctionModes.CurrentDC
        Me.Range = 0.000001
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " APERTURE "

    ''' <summary> Gets or sets the Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property ApertureQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overrides Property ApertureCommandFormat As String = String.Empty

#End Region

#Region " AUTO RANGE STATE "

    ''' <summary> The Auto Range state command format. </summary>
    ''' <value> The automatic range state command format. </value>
    Protected Overrides Property AutoRangeStateCommandFormat As String = "_G.smu.measure.autorange={0}"

    ''' <summary> Gets or sets the Auto Range state query command. </summary>
    ''' <value> The AutoRange state query command. </value>
    Protected Overrides Property AutoRangeStateQueryCommand As String = "_G.smu.measure.autorange"

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = "_G.smu.measure.autorange={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = "_G.print(_G.smu.measure.autorange==smu.ON)"

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = "_G.smu.measure.autozero.enable={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = "_G.print(_G.smu.measure.autozero.enable==smu.ON)"

#End Region

#Region " AUTO ZERO ONCE "

    ''' <summary> Gets or sets the automatic zero once command. </summary>
    ''' <value> The automatic zero once command. </value>
    Protected Overrides Property AutoZeroOnceCommand As String = "_G.smu.measure.autozero.once()"

#End Region

#Region " FILTER "

#Region " FILTER COUNT "

    ''' <summary> Gets or sets the Filter Count query command. </summary>
    ''' <value> The FilterCount query command. </value>
    Protected Overrides Property FilterCountQueryCommand As String = "_G.print(_G.smu.measure.filter.count)"

    ''' <summary> Gets or sets the Filter Count command format. </summary>
    ''' <value> The FilterCount command format. </value>
    Protected Overrides Property FilterCountCommandFormat As String = "_G.smu.measure.filter.count={0}"

#End Region

#Region " FILTER ENABLED "

    ''' <summary> Gets or sets the Filter enabled command Format. </summary>
    ''' <value> The Filter enabled query command. </value>
    Protected Overrides Property FilterEnabledCommandFormat As String = "_G.smu.measure.filter.enable={0:'smu.ON';'smu.ON';'smu.OFF'}"

    ''' <summary> Gets or sets the Filter enabled query command. </summary>
    ''' <value> The Filter enabled query command. </value>
    Protected Overrides Property FilterEnabledQueryCommand As String = "_G.print(_G.smu.measure.filter.enable==smu.ON)"

#End Region

#Region " MOVING AVERAGE ENABLED "

    ''' <summary> Gets or sets the moving average filter enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property MovingAverageFilterEnabledCommandFormat As String = "_G.smu.measure.filter.type={0:'smu.FILTER_MOVING_AVG';'smu.FILTER_MOVING_AVG';'smu.FILTER_REPEAT_AVG'}"

    ''' <summary> Gets or sets the moving average filter enabled query command. </summary>
    ''' <value> The moving average filter enabled query command. </value>
    Protected Overrides Property MovingAverageFilterEnabledQueryCommand As String = "_G.print(_G.smu.measure.filter.type==smu.FILTER_MOVING_AVG)"

#End Region

#Region " FILTER Window "

    ''' <summary> Gets or sets the Filter Window query command. </summary>
    ''' <value> The FilterWindow query command. </value>
    Protected Overrides Property FilterWindowQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the Filter Window command format. </summary>
    ''' <value> The FilterWindow command format. </value>
    Protected Overrides Property FilterWindowCommandFormat As String = String.Empty

#End Region

#End Region

#Region " FRONT TERMINALS SELECTED "

    ''' <summary> Gets or sets the front terminals selected command format. </summary>
    ''' <value> The front terminals selected command format. </value>
    Protected Overrides Property FrontTerminalsSelectedCommandFormat As String = "_G.smu.measure.terminals={0:'smu.TERMINALS_FRONT';'smu.TERMINALS_FRONT';'smu.TERMINALS_REAR'}"

    ''' <summary> Gets or sets the front terminals selected query command. </summary>
    ''' <value> The front terminals selected query command. </value>
    Protected Overrides Property FrontTerminalsSelectedQueryCommand As String = "_G.print(_G.smu.measure.terminals==smu.TERMINALS_FRONT)"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the function mode query command. </summary>
    ''' <value> The function mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = "_G.smu.measure.func"

    ''' <summary> Gets or sets the function mode command format. </summary>
    ''' <value> The function mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = "_G.smu.measure.func={0}"

#End Region

#Region " MEASURE "

    ''' <summary> Gets or sets the Measure query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property MeasureQueryCommand As String = "_G.print(_G.smu.measure.read())"

#End Region

#Region " OPEN DETECTOR ENABLED "

    ''' <summary> Gets or sets the open detector enabled command Format. </summary>
    ''' <value> The open detector enabled query command. </value>
    Protected Overrides Property OpenDetectorEnabledCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the open detector enabled query command. </summary>
    ''' <value> The open detector enabled query command. </value>
    Protected Overrides Property OpenDetectorEnabledQueryCommand As String = String.Empty

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = "_G.smu.measure.nplc={0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = "_G.print(_G.smu.measure.nplc)"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the Range query command. </summary>
    ''' <value> The Range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = "_G.print(_G.smu.measure.range)"

    ''' <summary> Gets or sets the Range command format. </summary>
    ''' <value> The Range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = "_G.smu.measure.range={0}"

#End Region

#Region " REMOTE SENSE SELECTED "

    ''' <summary> Gets or sets the remote sense selected command format. </summary>
    ''' <value> The remote sense selected command format. </value>
    Protected Overrides Property RemoteSenseSelectedCommandFormat As String = "_G.smu.measure.sense={0:'smu.SENSE_4WIRE';'smu.SENSE_4WIRE';'smu.SENSE_2WIRE'}"

    ''' <summary> Gets or sets the remote sense selected query command. </summary>
    ''' <value> The remote sense selected query command. </value>
    Protected Overrides Property RemoteSenseSelectedQueryCommand As String = "_G.print(_G.smu.measure.sense==smu.SENSE_4WIRE)"

#End Region

#Region " UNIT "

    ''' <summary> Gets or sets the unit command. </summary>
    ''' <value> The unit query command. </value>
    Protected Overrides Property MeasureUnitQueryCommand As String = "_G.smu.measure.unit"

    ''' <summary> Gets or sets the unit command format. </summary>
    ''' <value> The unit command format. </value>
    Protected Overrides Property MeasureUnitCommandFormat As String = "_G.smu.measure.unit={0}"

#End Region

#End Region

#Region " MEASURE "

    ''' <summary> Queries readings into the reading amounts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The reading or none if unknown. </returns>
    Public Overrides Function MeasureReadingAmounts() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return MyBase.MeasureReadingAmounts()
    End Function

#End Region

End Class
