'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\ScannerView.vb
'
' summary:	Scanner view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A scanner view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ScannerView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ScannerView"/> </summary>
    ''' <returns> A <see cref="ScannerView"/>. </returns>
    Public Shared Function Create() As ScannerView
        Dim view As ScannerView = Nothing
        Try
            view = New ScannerView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Aborts the trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Abort()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} aborting trigger plan" : Me.PublishInfo($"{activity};. ")
            Me.TriggerSubsystem.StartElapsedStopwatch()
            Me.TriggerSubsystem.Abort()
            Me.TriggerSubsystem.StopElapsedStopwatch()
            Me.Device.Session.QueryOperationCompleted()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initiates the trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Initiate()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.QueryOperationCompleted()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan" : Me.PublishInfo($"{activity};. ")
            Me.TriggerSubsystem.StartElapsedStopwatch()
            Me.TriggerSubsystem.Initiate()
            Me.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
            Try
                activity = $"{Me.Device.ResourceNameCaption} aborting trigger plan"
                Me.TriggerSubsystem.Abort()
            Catch
                Me.Device.Session.StatusPrompt = $"failed {activity}"
                activity = Me.PublishException(activity, ex)
                Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End Try
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Sends the bus trigger. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub SendBusTrigger()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} sending bus trigger" : Me.PublishInfo($"{activity};. ")
            Me.Device.Session.AssertTrigger()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Builds bus trigger description. </summary>
    ''' <returns> A String. </returns>
    Public Function BuildBusTriggerDescription() As String
        If Me.InitializingComponents Then Return String.Empty
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine($"Scan plan with bus triggering:")
        builder.AppendLine($"Scan list: {Me._ScanListView.ScanList}")
        builder.AppendLine($"Arm layer 1: Count {1} {VI.ArmSources.Immediate} delay {0}")
        builder.AppendLine($"Arm layer 2: Count {1} {VI.ArmSources.Immediate} delay {0}")
        builder.AppendLine($"Trigger layer: Source {VI.TriggerSources.Bus} count {9999} delay {0} bypass {VI.TriggerLayerBypassModes.Acceptor}")
        Return builder.ToString
    End Function

    ''' <summary> Configure bus trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ConfigureBusTriggerPlan()
        If Me.InitializingComponents Then Return
        Dim title As String = Me.Device.OpenResourceTitle
        Dim propertyName As String = String.Empty
        Dim activity As String = $"{title} Configuring bus scan trigger plan"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.PublishInfo($"{activity};. ")

            activity = $"{title} aborting trigger plan" : Me.PublishInfo($"{activity};. ")
            Me.TriggerSubsystem.Abort() : Me.Device.Session.QueryOperationCompleted()

            activity = $"{title} clearing execution state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState() : Me.Device.Session.QueryOperationCompleted()
            propertyName = $"{NameOf(VI.TriggerSubsystemBase).SplitWords}.{NameOf(VI.TriggerSubsystemBase.ContinuousEnabled).SplitWords}"
            activity = $"{title} setting [{propertyName}]=False" : Me.PublishInfo($"{activity};. ")
            Me.TriggerSubsystem.ApplyContinuousEnabled(False)
            Me.TriggerSubsystem.Abort()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            Me.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
            Me.RouteSubsystem.QueryClosedChannels()
            Me.RouteSubsystem.ApplyScanList(Me._ScanListView.ScanList)

            Me.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
            Me.ArmLayer1Subsystem.ApplyArmCount(1)
            Me.ArmLayer1Subsystem.ApplyArmLayerBypassMode(TriggerLayerBypassModes.Acceptor)

            Me.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
            Me.ArmLayer2Subsystem.ApplyArmCount(1)
            Me.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)

            Me.TriggerSubsystem.ApplyTriggerSource(TriggerSources.Bus)
            Me.TriggerSubsystem.ApplyTriggerCount(9999) ' in place of infinite
            Me.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
            Me.TriggerSubsystem.ApplyTriggerLayerBypassMode(TriggerLayerBypassModes.Acceptor)
            Me.Device.Session.StatusPrompt = "Ready: Initiate (1) Meter; (2) 7001r"
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Builds external trigger description. </summary>
    ''' <returns> A String. </returns>
    Public Function BuildExternalTriggerDescription() As String
        If Me.InitializingComponents Then Return String.Empty
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine("Scan plan with external triggering handshake with a meter:")
        builder.AppendLine($"Scan list: {Me._ScanListView.ScanList}")
        builder.AppendLine($"Arm layer 1: Count {1} {VI.ArmSources.Immediate} delay {0}")
        builder.AppendLine($"Arm layer 2: Count {1} {VI.ArmSources.Immediate} delay {0}")
        builder.AppendLine($"Trigger layer: Source {VI.TriggerSources.External} count {9999} delay {0} bypass {VI.TriggerLayerBypassModes.Source}")
        Return builder.ToString
    End Function

    ''' <summary> Configure external trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ConfigureExternalTriggerPlan()
        If Me.InitializingComponents Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} Configuring external scan trigger plan"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.ClearExecutionState()
            ' enable service requests
            'Me.EnableServiceRequestEventHandler()
            'Me.Device.StatusSubsystem.EnableServiceRequest(Me.Device.Session.DefaultServiceRequestEnableBitmask)

            Me.PublishInfo($"{activity};. ")
            Me.TriggerSubsystem.ApplyContinuousEnabled(False)
            Me.TriggerSubsystem.Abort()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            Me.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
            Me.RouteSubsystem.QueryClosedChannels()
            Me.RouteSubsystem.ApplyScanList(Me._ScanListView.ScanList) ' "(@1!1:1!10)"
            Me.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
            Me.ArmLayer1Subsystem.ApplyArmCount(1)
            Me.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
            Me.ArmLayer2Subsystem.ApplyArmCount(1)
            Me.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)
            Me.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.External)
            Me.TriggerSubsystem.ApplyTriggerCount(9999) ' in place of infinite
            Me.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
            Me.TriggerSubsystem.ApplyTriggerLayerBypassMode(VI.TriggerLayerBypassModes.Source)
            Me.Device.Session.StatusPrompt = "Ready: Initiate (1) 7001; (2) Meter"
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            RemoveHandler Me._Device.PropertyChanged, AddressOf Me.VisaSessionBasePropertyChanged
            Me._Device = Nothing
        End If
        Me._Device = value
        Me._SlotView.AssignDevice(value)
        Me._ServiceRequestView.AssignDevice(value)
        Me._ScanListView.AssignDevice(value)
        If value IsNot Nothing Then
            AddHandler Me._Device.PropertyChanged, AddressOf Me.VisaSessionBasePropertyChanged
            Me.HandlePropertyChanged(Me.Device, NameOf(VisaSessionBase.OpenResourceTitle))
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handle the visa session base property changed event. </summary>
    ''' <param name="device">       The visa session base. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal device As isr.VI.VisaSessionBase, ByVal propertyName As String)
        If device Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(isr.VI.VisaSessionBase.OpenResourceTitle)
                Me._SubsystemSplitButton.Text = device.OpenResourceTitle
        End Select
    End Sub

    ''' <summary> visa session base  property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub VisaSessionBasePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(isr.VI.VisaSessionBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.VisaSessionBasePropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.VisaSessionBasePropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.VisaSessionBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " RELAY VIEW "

    ''' <summary> Handle the relay view property changed event. </summary>
    ''' <param name="view">         The view. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal view As VI.Facade.RelayView, ByVal propertyName As String)
        If view Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Facade.RelayView.ChannelList)
                Me._InfoTextBox.Text = $"Channel List: {view.ChannelList}"
        End Select
    End Sub

    ''' <summary> Relay view property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RelayViewPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(RouteSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.RelayViewPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.RelayViewPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Facade.RelayView), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SCAN LIST VIEW "

    ''' <summary> Handle the ScanList view property changed event. </summary>
    ''' <param name="view">         The view. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal view As VI.Facade.ScanListView, ByVal propertyName As String)
        If view Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Facade.ScanListView.ScanList)
                Me._InfoTextBox.Text = $"Scan List: {view.ScanList}"
        End Select
    End Sub

    ''' <summary> ScanList view property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ScanListViewPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(Facade.ScanListView)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ScanListViewPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ScanListViewPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Facade.ScanListView), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the route subsystem. </summary>
    ''' <value> The route  subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property RouteSubsystem As RouteSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As RouteSubsystemBase)
        Me._SlotView.BindSubsystem(subsystem)
        Me._ScanListView.BindSubsystem(subsystem)
        If subsystem Is Nothing Then
            Me._RouteSubsystem = Nothing
            RemoveHandler Me._RelayView.PropertyChanged, AddressOf Me.RelayViewPropertyChanged
            RemoveHandler Me._ScanListView.PropertyChanged, AddressOf Me.ScanListViewPropertyChanged
        Else
            Me._RouteSubsystem = subsystem
            AddHandler Me._RelayView.PropertyChanged, AddressOf Me.RelayViewPropertyChanged
            AddHandler Me._ScanListView.PropertyChanged, AddressOf Me.ScanListViewPropertyChanged
            Me.HandlePropertyChanged(Me._RelayView, NameOf(isr.VI.Facade.RelayView.ChannelList))
        End If
    End Sub

#End Region

#Region " ARM LAYER SUBSYSTEMs "

    ''' <summary> Gets or sets the arm layer 1 subsystem. </summary>
    ''' <value> The arm layer 1 subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayer1Subsystem As ArmLayerSubsystemBase

    ''' <summary> Gets or sets the arm layer 2 subsystem. </summary>
    ''' <value> The arm layer 2 subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayer2Subsystem As ArmLayerSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="layerNumber"> The layer number. </param>
    Protected Sub BindSubsystem(ByVal subsystem As ArmLayerSubsystemBase, ByVal layerNumber As Integer)
        If layerNumber = 1 Then
            Me.BindSubsystem1(subsystem)
        Else
            Me.BindSubsystem2(subsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem 1. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Sub BindSubsystem1(ByVal subsystem As ArmLayerSubsystemBase)
        If Me.ArmLayer1Subsystem IsNot Nothing Then
            Me._ArmLayer1Subsystem = Nothing
        End If
        Me._ArmLayer1Subsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

    ''' <summary> Bind subsystem 2. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Sub BindSubsystem2(ByVal subsystem As ArmLayerSubsystemBase)
        If Me.ArmLayer2Subsystem IsNot Nothing Then
            Me._ArmLayer2Subsystem = Nothing
        End If
        Me._ArmLayer2Subsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " TRIGGER SUBSYSTEM "

    ''' <summary> Gets or sets the trigger subsystem. </summary>
    ''' <value> The trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Sub BindSubsystem(ByVal subsystem As TriggerSubsystemBase)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Initiate click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InitiateButton_Click(sender As Object, e As EventArgs) Handles _InitiateButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.Initiate()
    End Sub

    ''' <summary> Abort button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AbortButton_Click(sender As Object, e As EventArgs) Handles _AbortButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.Abort()
    End Sub

    ''' <summary> Sends the bus trigger button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SendBusTriggerButton_Click(sender As Object, e As EventArgs) Handles _SendBusTriggerButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.SendBusTrigger()
    End Sub

    ''' <summary> Explain external trigger plan menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExplainExternalTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _ExplainExternalTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} building external scan trigger plan description"
        Dim description As String = String.Empty
        Try
            description = Me.BuildExternalTriggerDescription
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me._InfoTextBox.Text = description
        End Try
    End Sub

    ''' <summary> Configure external trigger plan menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConfigureExternalTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _ConfigureExternalTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ConfigureExternalTriggerPlan()
    End Sub

    ''' <summary> Explain bus trigger plan menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExplainBusTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _ExplainBusTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} building bus trigger plan description"
        Dim description As String = String.Empty
        Try
            description = Me.BuildBusTriggerDescription
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me._InfoTextBox.Text = description
        End Try
    End Sub

    ''' <summary> Configure bus trigger plan menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConfigureBusTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _ConfigureBusTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ConfigureBusTriggerPlan()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As isr.Core.ITraceMessageTalker)
        Me._SlotView.AssignTalker(talker)
        Me._ServiceRequestView.AssignTalker(talker)
        Me._RelayView.AssignTalker(talker)
        Me._ScanListView.AssignTalker(talker)
        ' assigned last as this identifies all talkers.
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
