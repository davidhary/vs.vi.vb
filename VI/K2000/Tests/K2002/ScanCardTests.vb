'---------------------------------------------------------------------------------------------------
' file:		Tests\K2002\ScanCard_Tests.vb
'
' summary:	Scan card tests class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

''' <summary> K2002 Scan Card unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2001ScanCard")>
Public Class ScanCardTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(K2002ResourceInfo.Exists, $"{GetType(K2002Tests.ResourceSettings)} settings not found")
        Assert.IsTrue(K2002SubsystemsInfo.Exists, $"{GetType(K2002Tests.SubsystemsSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SCAN CARD TESTS "

    ''' <summary> (Unit Test Method) scans the card internal scan function test. </summary>
    <TestMethod()>
    Public Sub ScanCardInternalScanFunctionTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertScanCardInstalled(device.SystemSubsystem, K2002SubsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertScanCardInternalScan(device.RouteSubsystem, device.SystemSubsystem, K2002SubsystemsInfo)
                device.Session.ReadStatusRegister()
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> Assert scan card four wire resistance scan result. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="result">           The result. </param>
    ''' <param name="elapsedTimeRange"> The elapsed time range. </param>
    ''' <param name="errorBitmask">     The error bitmask. </param>
    Private Shared Sub AssertScanCardKelvinScanResult(ByVal result As (Status As String, Reading As String, Elapsed As TimeSpan),
                                                      ByVal elapsedTimeRange As isr.Core.Constructs.RangeTimeSpan,
                                                      ByVal errorBitmask As Integer)
        Dim expectedStatus As Integer = 0
        Dim actualStatus As Integer = Integer.Parse(result.Status)
        Assert.AreEqual(expectedStatus, actualStatus And errorBitmask, $"{NameOf(result.Status)} errors bits {errorBitmask:X2} should be off")
        Assert.IsFalse(String.IsNullOrWhiteSpace(result.Reading), $"{NameOf(result.Reading)} must not be empty")
        Dim values As IEnumerable(Of String) = result.Reading.Split(","c)
        Dim expectedCount As Integer = 3
        Assert.AreEqual(expectedCount, values.Count, $"{NameOf(result.Reading)} must have the expected number of elements")
        Assert.IsTrue(elapsedTimeRange.Contains(result.Elapsed), $"Elapsed time {result.Elapsed.TotalMilliseconds:0}ms should be within {elapsedTimeRange.ToString("ss\.fff")}")
    End Sub

    ''' <summary> (Unit Test Method) tests the scan card four wire resistance scan. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    <TestMethod()>
    Public Sub ScanCardKelvinScanTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                If device.SystemSubsystem.QueryFrontTerminalsSelected.Value Then Assert.Inconclusive($"Rear terminal must be selected")
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(device.Session, "IMM", 1, TimeSpan.FromMilliseconds(800))
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(device.Session, device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                Dim result As (Status As String, Reading As String, Elapsed As TimeSpan) =
                                                        VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(device.Session,
                                                                         device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                ScanCardTests.TestInfo.TraceMessage($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                Dim errorBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.FailuresSummary)
                ScanCardTests.AssertScanCardKelvinScanResult(result,
                                                             New isr.Core.Constructs.RangeTimeSpan(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(1000)),
                                                             errorBitmask)
                device.Session.ReadStatusRegister()
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) scans the card four wire resistance timer scan test. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    <TestMethod()>
    Public Sub ScanCardKelvinTimerScanTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                If device.SystemSubsystem.QueryFrontTerminalsSelected.Value Then Assert.Inconclusive($"Rear terminal must be selected")
                Dim triggerCount As Integer = 10
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(device.Session, "TIM", triggerCount, TimeSpan.FromMilliseconds(800))
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(device.Session, device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                Do While triggerCount > 0
                    Dim result As (Status As String, Reading As String, Elapsed As TimeSpan) =
                                                        VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(device.Session,
                                                                         device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                    ScanCardTests.TestInfo.TraceMessage($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                    Dim errorBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.FailuresSummary)
                    ScanCardTests.AssertScanCardKelvinScanResult(result,
                                                                 New isr.Core.Constructs.RangeTimeSpan(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(1000)),
                                                                 errorBitmask)
                    triggerCount -= 1
                    VI.K2002.RouteSubsystem.ClearBuffer(device.Session)
                Loop
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) scans the card four wire resistance bus trigger scan test.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    <TestMethod()>
    Public Sub ScanCardKelvinBusTriggerScanTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                If device.SystemSubsystem.QueryFrontTerminalsSelected.Value Then Assert.Inconclusive($"Rear terminal must be selected")
                Dim triggerCount As Integer = 10
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(device.Session, "BUS", triggerCount, TimeSpan.FromMilliseconds(800))
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(device.Session, device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                Do While triggerCount > 0
                    device.Session.WriteLine("*TRG")
                    Dim result As (Status As String, Reading As String, Elapsed As TimeSpan) =
                                                        VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(device.Session,
                                                                         device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                    VI.K2002.RouteSubsystem.ClearBuffer(device.Session)
                    ScanCardTests.TestInfo.TraceMessage($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                    Dim errorBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.FailuresSummary)
                    ScanCardTests.AssertScanCardKelvinScanResult(result,
                                                                 New isr.Core.Constructs.RangeTimeSpan(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(1000)),
                                                                 errorBitmask)
                    triggerCount -= 1
                    isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(200))
                Loop
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) scans the card four wire resistance scan library test. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    <TestMethod()>
    Public Sub ScanCardKelvinScanLibraryTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                If device.SystemSubsystem.QueryFrontTerminalsSelected.Value Then Assert.Inconclusive($"Rear terminal must be selected")
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(device, VI.ArmSources.Immediate, 1, TimeSpan.FromMilliseconds(600))
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(device, device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                Dim result As (Status As String, Reading As String, Elapsed As TimeSpan) =
                                                        VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(device)
                ScanCardTests.TestInfo.TraceMessage($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                Dim errorBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.FailuresSummary)
                ScanCardTests.AssertScanCardKelvinScanResult(result,
                                                             New isr.Core.Constructs.RangeTimeSpan(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(1000)),
                                                             errorBitmask)
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) scans the card four wire resistance timer library scan test.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    <TestMethod()>
    Public Sub ScanCardKelvinTimerLibraryScanTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                If device.SystemSubsystem.QueryFrontTerminalsSelected.Value Then Assert.Inconclusive($"Rear terminal must be selected")
                Dim triggerCount As Integer = 10
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(device, ArmSources.Timer, triggerCount, TimeSpan.FromMilliseconds(600))
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(device, device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                Do While triggerCount > 0
                    Dim result As (Status As String, Reading As String, Elapsed As TimeSpan) =
                                                        VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(device)
                    ScanCardTests.TestInfo.TraceMessage($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                    Dim errorBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.FailuresSummary)
                    ScanCardTests.AssertScanCardKelvinScanResult(result,
                                                                 New isr.Core.Constructs.RangeTimeSpan(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(1000)),
                                                                 errorBitmask)
                    device.Session.ReadStatusRegister()
                    triggerCount -= 1
                    VI.K2002.RouteSubsystem.ClearBuffer(device.Session)
                Loop
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) scans the card four wire resistance bus trigger library scan test.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    <TestMethod()>
    Public Sub ScanCardKelvinBusTriggerLibraryScanTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(ScanCardTests.TestInfo, device, K2002ResourceInfo)
                If device.SystemSubsystem.QueryFrontTerminalsSelected.Value Then Assert.Inconclusive($"Rear terminal must be selected")
                Dim triggerCount As Integer = 10
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(device, ArmSources.Bus, triggerCount, TimeSpan.FromMilliseconds(600))
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(device, device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull))
                Do While triggerCount > 0
                    device.Session.WriteLine("*TRG")
                    Dim result As (Status As String, Reading As String, Elapsed As TimeSpan) =
                                                        VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(device)
                    ScanCardTests.TestInfo.TraceMessage($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                    Dim errorBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.FailuresSummary)
                    ScanCardTests.AssertScanCardKelvinScanResult(result,
                                                                 New isr.Core.Constructs.RangeTimeSpan(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(1000)),
                                                                 errorBitmask)
                    device.Session.ReadStatusRegister()
                    isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(250))
                    triggerCount -= 1
                    VI.K2002.RouteSubsystem.ClearBuffer(device.Session)
                Loop
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(ScanCardTests.TestInfo, device)
            End Try
        End Using
    End Sub


#End Region

End Class
