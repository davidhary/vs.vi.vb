''' <summary> An Hex format provider. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-11-21 </para>
''' </remarks>
Public Class HexFormatProvider
    Inherits isr.Core.DigitalValueFormatProvider

    ''' <summary> Converts a value to a service requests enum value. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a ServiceRequests? </returns>
    Public Function ToServiceRequests(ByVal value As String) As ServiceRequests?
        Return HexFormatProvider.ParseEnumValue(Of ServiceRequests)(Me.Prefix, value, Globalization.NumberStyles.HexNumber)
    End Function

    ''' <summary> Converts a value to a standard events enum value. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a StandardEvents? </returns>
    Public Function ToStandardEvents(ByVal value As String) As StandardEvents?
        Return HexFormatProvider.ParseEnumValue(Of StandardEvents)(Me.Prefix, value, Globalization.NumberStyles.HexNumber)
    End Function

    ''' <summary> Hexadecimal value format provider. </summary>
    ''' <param name="byteCount"> Number of bytes. </param>
    ''' <returns> A RegisterValueFormatProvider. </returns>
    Public Shared Function FormatProvider(ByVal byteCount As Integer) As HexFormatProvider
        Return FormatProvider(HexFormatProvider.DefaultHexPrefix, byteCount)
    End Function

    ''' <summary> Hexadecimal value format provider. </summary>
    ''' <param name="prefix">    The prefix. </param>
    ''' <param name="byteCount"> Number of bytes. </param>
    ''' <returns> A RegisterValueFormatProvider. </returns>
    Public Shared Function FormatProvider(ByVal prefix As String, ByVal byteCount As Integer) As HexFormatProvider
        Dim result As New HexFormatProvider With {
            .Prefix = prefix,
            .FormatString = $"{prefix}{{0:X{byteCount}}}",
            .NullValueCaption = $"{prefix}{StrDup(byteCount, ".")}"
        }
        Return result
    End Function

End Class

