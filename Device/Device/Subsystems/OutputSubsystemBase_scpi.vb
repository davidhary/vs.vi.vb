
Partial Public Class OutputSubsystemBase

#Region " OUTPUT OFF MODE "

    ''' <summary> Define output off mode read writes. </summary>
    Private Sub DefineOutputOffModeReadWrites()
        Me._OutputOffModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As OutputOffModes In [Enum].GetValues(GetType(OutputOffModes))
            Me._OutputOffModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of output Off Mode parses. </summary>
    ''' <value> A Dictionary of output Off Mode parses. </value>
    Public ReadOnly Property OutputOffModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported output off modes. </summary>
    Private _SupportedOutputOffModes As OutputOffModes

    ''' <summary> Gets or sets the supported Off Modes. </summary>
    ''' <value> The supported Off Modes. </value>
    Public Property SupportedOutputOffModes() As OutputOffModes
        Get
            Return Me._SupportedOutputOffModes
        End Get
        Set(ByVal value As OutputOffModes)
            If Not Me.SupportedOutputOffModes.Equals(value) Then
                Me._SupportedOutputOffModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The output off mode. </summary>
    Private _OutputOffMode As OutputOffModes?

    ''' <summary> Gets or sets the cached output off mode. </summary>
    ''' <value>
    ''' The <see cref="OutputOffMode">output off mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property OutputOffMode As OutputOffModes?
        Get
            Return Me._OutputOffMode
        End Get
        Protected Set(ByVal value As OutputOffModes?)
            If Not Me.OutputOffMode.Equals(value) Then
                Me._OutputOffMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the output off mode. </summary>
    ''' <param name="value"> The  output off mode. </param>
    ''' <returns> The <see cref="OutputOffMode">output off mode</see> or none if unknown. </returns>
    Public Function ApplyOutputOffMode(ByVal value As OutputOffModes) As OutputOffModes?
        Me.WriteOutputOffMode(value)
        Return Me.QueryOutputOffMode()
    End Function

    ''' <summary> Gets or sets the Output Off Mode query command. </summary>
    ''' <remarks> SCPI: "?". </remarks>
    ''' <value> The Off Mode query command. </value>
    Protected Overridable Property OutputOffModeQueryCommand As String

    ''' <summary> Queries the output Off Mode. </summary>
    ''' <returns> The <see cref="OutputOffMode">Off Mode</see> or none if unknown. </returns>
    Public Function QueryOutputOffMode() As OutputOffModes?
        Me.OutputOffMode = Me.Query(Of OutputOffModes)(Me.OutputOffModeQueryCommand,
                                                       Me.OutputOffMode.GetValueOrDefault(OutputOffModes.None),
                                                       Me.OutputOffModeReadWrites)
        Return Me.OutputOffMode
    End Function

    ''' <summary> Gets or sets the output Off Mode command format. </summary>
    ''' <remarks> SCPI: " {0}". </remarks>
    ''' <value> The write Off Mode command format. </value>
    Protected Overridable Property OutputOffModeCommandFormat As String

    ''' <summary> Writes the output Off Mode without reading back the value from the device. </summary>
    ''' <param name="value"> The Off Mode. </param>
    ''' <returns> The <see cref="OutputOffMode">Off Mode</see> or none if unknown. </returns>
    Public Function WriteOutputOffMode(ByVal value As OutputOffModes) As OutputOffModes?
        Me.OutputOffMode = Me.Write(Of OutputOffModes)(Me.OutputOffModeCommandFormat, value, Me.OutputOffModeReadWrites)
        Return Me.OutputOffMode
    End Function

#End Region

#Region " OUTPUT TERMINALS MODE "

    ''' <summary> Define output terminals mode read writes. </summary>
    Private Sub DefineOutputTerminalsModeReadWrites()
        Me._OutputTerminalsModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As OutputTerminalsModes In [Enum].GetValues(GetType(OutputTerminalsModes))
            Me._OutputTerminalsModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of output Terminals Mode parses. </summary>
    ''' <value> A Dictionary of output Terminals Mode parses. </value>
    Public ReadOnly Property OutputTerminalsModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported output terminals modes. </summary>
    Private _SupportedOutputTerminalsModes As OutputTerminalsModes

    ''' <summary> Gets or sets the supported Terminals Modes. </summary>
    ''' <value> The supported Terminals Modes. </value>
    Public Property SupportedOutputTerminalsModes() As OutputTerminalsModes
        Get
            Return Me._SupportedOutputTerminalsModes
        End Get
        Set(ByVal value As OutputTerminalsModes)
            If Not Me.SupportedOutputTerminalsModes.Equals(value) Then
                Me._SupportedOutputTerminalsModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The output terminals mode. </summary>
    Private _OutputTerminalsMode As OutputTerminalsModes?

    ''' <summary> Gets or sets the cached output Terminals mode. </summary>
    ''' <value>
    ''' The <see cref="OutputTerminalsMode">output Terminals mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property OutputTerminalsMode As OutputTerminalsModes?
        Get
            Return Me._OutputTerminalsMode
        End Get
        Protected Set(ByVal value As OutputTerminalsModes?)
            If Not Me.OutputTerminalsMode.Equals(value) Then
                Me._OutputTerminalsMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the output Terminals mode. </summary>
    ''' <param name="value"> The  output Terminals mode. </param>
    ''' <returns>
    ''' The <see cref="OutputTerminalsMode">output Terminals mode</see> or none if unknown.
    ''' </returns>
    Public Function ApplyOutputTerminalsMode(ByVal value As OutputTerminalsModes) As OutputTerminalsModes?
        Me.WriteOutputTerminalsMode(value)
        Return Me.QueryOutputTerminalsMode()
    End Function

    ''' <summary> Gets or sets the Output Terminals Mode query command. </summary>
    ''' <remarks> SCPI: ":OUTP:ROUT:TERM?". </remarks>
    ''' <value> The Terminals Mode query command. </value>
    Protected Overridable Property OutputTerminalsModeQueryCommand As String = ":OUTP:ROUT:TERM?"

    ''' <summary> Queries the output Terminals Mode. </summary>
    ''' <returns>
    ''' The <see cref="OutputTerminalsMode">Terminals Mode</see> or none if unknown.
    ''' </returns>
    Public Function QueryOutputTerminalsMode() As OutputTerminalsModes?
        Me.OutputTerminalsMode = Me.Query(Of OutputTerminalsModes)(Me.OutputTerminalsModeQueryCommand,
                                                       Me.OutputTerminalsMode.GetValueOrDefault(OutputTerminalsModes.None),
                                                       Me.OutputTerminalsModeReadWrites)
        Return Me.OutputTerminalsMode
    End Function

    ''' <summary> Gets or sets the output Terminals Mode command format. </summary>
    ''' <remarks> SCPI: ":OUTP:ROUT:TERM {0}". </remarks>
    ''' <value> The write Terminals Mode command format. </value>
    Protected Overridable Property OutputTerminalsModeCommandFormat As String = ":OUTP:ROUT:TERM {0}"

    ''' <summary>
    ''' Writes the output Terminals Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Terminals Mode. </param>
    ''' <returns>
    ''' The <see cref="OutputTerminalsMode">Terminals Mode</see> or none if unknown.
    ''' </returns>
    Public Function WriteOutputTerminalsMode(ByVal value As OutputTerminalsModes) As OutputTerminalsModes?
        Me.OutputTerminalsMode = Me.Write(Of OutputTerminalsModes)(Me.OutputTerminalsModeCommandFormat, value, Me.OutputTerminalsModeReadWrites)
        Return Me.OutputTerminalsMode
    End Function

#End Region

End Class

''' <summary> Specifies the output terminals mode. </summary>
<Flags>
Public Enum OutputTerminalsModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not set ()")>
    None = 0

    ''' <summary> An enum constant representing the front option. </summary>
    <ComponentModel.Description("Front (FRON)")>
    Front = 1

    ''' <summary> An enum constant representing the rear option. </summary>
    <ComponentModel.Description("Rear (REAR)")>
    Rear = 2
End Enum

''' <summary> Specifies the output off mode. </summary>
<Flags>
Public Enum OutputOffModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not set ()")>
    None = 0

    ''' <summary> An enum constant representing the guard option. </summary>
    <ComponentModel.Description("Guard (GUAR)")>
    Guard = 1

    ''' <summary> An enum constant representing the high impedance option. </summary>
    <ComponentModel.Description("High Impedance (HIMP)")>
    HighImpedance = 2

    ''' <summary> An enum constant representing the normal option. </summary>
    <ComponentModel.Description("Normal (NORM)")>
    Normal = 4

    ''' <summary> An enum constant representing the zero option. </summary>
    <ComponentModel.Description("Zero (ZERO)")>
    Zero = 8
End Enum

