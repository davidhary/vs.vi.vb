'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\BinningView.vb
'
' summary:	Binning view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

''' <summary>
''' A Keithley 2002 edition of the base <see cref="Facade.BinningView"/> user interface.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class BinningView
    Inherits isr.VI.Facade.BinningView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="BinningView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="BinningView"/>. </returns>
    Public Overloads Shared Function Create() As BinningView
        Dim view As BinningView = Nothing
        Try
            view = New BinningView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property K2002Device As K2002Device

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Shadows Sub AssignDevice(ByVal value As K2002Device)
        MyBase.AssignDevice(value)
        Me._K2002Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, BinningSubsystemBase))
            Me.BindSubsystem(CType(Nothing, DigitalOutputSubsystemBase))
        Else
            Me.BindSubsystem(Me.K2002Device.BinningSubsystem)
            Me.BindSubsystem(Me.K2002Device.DigitalOutputSubsystem)
        End If
    End Sub

#End Region

End Class

