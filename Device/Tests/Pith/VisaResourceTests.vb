'---------------------------------------------------------------------------------------------------
' file:		Tests\Pith\VisaResourceTests.vb
'
' summary:	Visa resource tests class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Pith

''' <summary> VISA Resource Settings tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-11 </para>
''' </remarks>
<TestClass()>
Public Class VisaResourceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()

        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()

        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Overridable Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " VISA VERSION TESTS "

    ''' <summary> (Unit Test Method) validates the visa version test. </summary>
    ''' <remarks>
    ''' To test the 32bit version, change the default processor architecture from the Test, Test
    ''' Settings menu.e.
    ''' </remarks>
    <TestMethod()>
    Public Sub ValidateVisaVersionTest()
        Dim r As (Success As Boolean, Details As String) = VI.SessionFactory.ValidateFunctionalVisaVersions()
        Assert.IsTrue(r.Success, r.Details)
    End Sub

#End Region

#Region " VISA RESOURCE TEST "

    ''' <summary> (Unit Test Method) tests locating visa resources. </summary>
    <TestMethod()>
    Public Sub VisaResourcesTest()
        'resourcesFilter = resourcesFilter.Replace("(", "[")
        'resourcesFilter = resourcesFilter.Replace(")", "]")
        'resourcesFilter = "[TCPIP|GPIB]*INSTR"
        'resourcesFilter = "(TCPIP|GPIB)*INSTR"
        Dim resources As String()
        Using rm As VI.Pith.ResourcesProviderBase = VI.SessionFactory.Get.Factory.ResourcesProvider()
            Dim resourcesFilter As String = rm.ResourceFinder.BuildMinimalResourcesFilter
            resources = rm.FindResources(resourcesFilter).ToArray
        End Using
        Assert.IsTrue(resources.Any, $"VISA Resources {If(resources.Any, String.Empty, "not")} found")
    End Sub

#End Region

#Region " RESOURCE NAME INFO "

    ''' <summary> (Unit Test Method) tests visa resource name information. </summary>
    <TestMethod()>
    Public Sub VisaResourceNameInfoTest()
        Dim resourceNameInfoCollection As New ResourceNameInfoCollection
        Dim expectedCount As Integer
        Dim actualCount As Integer
        If Not resourceNameInfoCollection.IsFileExists Then
            ' if a new file, add a known resource.
            Dim resourceName As String = "TCPIP0::192.168.0.254::gpib0,15::INSTR"
            resourceNameInfoCollection.Add(resourceName)
            resourceNameInfoCollection.WriteResources()
            Assert.IsTrue(resourceNameInfoCollection.IsFileExists())
            expectedCount = resourceNameInfoCollection.Count
            resourceNameInfoCollection.ReadResources()
            actualCount = resourceNameInfoCollection.Count
            Assert.AreEqual(expectedCount, actualCount, $"{NameOf(VI.Pith.ResourceNameInfoCollection)}.count should match")
            Assert.IsTrue(resourceNameInfoCollection.Contains(resourceName), $"{NameOf(VI.Pith.ResourceNameInfoCollection)} should contain {resourceName}")
        End If
        resourceNameInfoCollection.ReadResources()
        Assert.IsTrue(resourceNameInfoCollection.Any, $"{NameOf(VI.Pith.ResourceNameInfoCollection)} should have items")
        resourceNameInfoCollection.WriteResources()
    End Sub

#End Region

End Class
