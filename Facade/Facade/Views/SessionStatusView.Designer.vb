<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SessionStatusView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SessionStatusView))
        Me._StatusView = New isr.VI.Facade.StatusView()
        Me._SessionView = New isr.VI.Facade.SessionView()
        Me._SelectorOpener = New isr.Core.Controls.SelectorOpener()
        Me.SuspendLayout()
        '
        '_StatusView
        '
        Me._StatusView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._StatusView.BackColor = System.Drawing.Color.Transparent
        Me._StatusView.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._StatusView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatusView.Location = New System.Drawing.Point(0, 266)
        Me._StatusView.Margin = New System.Windows.Forms.Padding(0)
        Me._StatusView.Name = "_StatusView"
        Me._StatusView.PublishBindingSuccessEnabled = False
        Me._StatusView.Size = New System.Drawing.Size(383, 31)
        Me._StatusView.TabIndex = 0
        Me._StatusView.TraceLogEvent = CType(resources.GetObject("_StatusView.TraceLogEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._StatusView.TraceLogLevel = System.Diagnostics.TraceEventType.Information
        Me._StatusView.TraceShowEvent = CType(resources.GetObject("_StatusView.TraceShowEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._StatusView.TraceShowLevel = System.Diagnostics.TraceEventType.Information
        '
        '_SessionView
        '
        Me._SessionView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SessionView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SessionView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SessionView.Location = New System.Drawing.Point(0, 0)
        Me._SessionView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SessionView.Name = "_SessionView"
        Me._SessionView.PublishBindingSuccessEnabled = False
        Me._SessionView.Size = New System.Drawing.Size(383, 266)
        Me._SessionView.TabIndex = 1
        Me._SessionView.Termination = "\n"
        Me._SessionView.TraceLogEvent = CType(resources.GetObject("_SessionView.TraceLogEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._SessionView.TraceLogLevel = System.Diagnostics.TraceEventType.Information
        Me._SessionView.TraceShowEvent = CType(resources.GetObject("_SessionView.TraceShowEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._SessionView.TraceShowLevel = System.Diagnostics.TraceEventType.Information
        '
        '_SelectorOpener
        '
        Me._SelectorOpener.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SelectorOpener.BackColor = System.Drawing.Color.Transparent
        Me._SelectorOpener.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._SelectorOpener.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectorOpener.Location = New System.Drawing.Point(0, 297)
        Me._SelectorOpener.Margin = New System.Windows.Forms.Padding(0)
        Me._SelectorOpener.Name = "_SelectorOpener"
        Me._SelectorOpener.PublishBindingSuccessEnabled = False
        Me._SelectorOpener.Size = New System.Drawing.Size(383, 29)
        Me._SelectorOpener.TabIndex = 2
        Me._SelectorOpener.TraceLogEvent = CType(resources.GetObject("_SelectorOpener.TraceLogEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._SelectorOpener.TraceLogLevel = System.Diagnostics.TraceEventType.Information
        Me._SelectorOpener.TraceShowEvent = CType(resources.GetObject("_SelectorOpener.TraceShowEvent"), System.Collections.Generic.KeyValuePair(Of System.Diagnostics.TraceEventType, String))
        Me._SelectorOpener.TraceShowLevel = System.Diagnostics.TraceEventType.Information
        '
        'SessionStatusView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SessionView)
        Me.Controls.Add(Me._StatusView)
        Me.Controls.Add(Me._SelectorOpener)
        Me.Name = "SessionStatusView"
        Me.Size = New System.Drawing.Size(383, 326)
        Me.ResumeLayout(False)
    End Sub
    Private WithEvents _StatusView As StatusView
    Private WithEvents _SessionView As SessionView
    Private WithEvents _SelectorOpener As Core.Controls.SelectorOpener
End Class
