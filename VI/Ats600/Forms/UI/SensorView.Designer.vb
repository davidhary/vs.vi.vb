﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SensorView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SensorView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._DeviceSensorTypeSelectorLabel = New System.Windows.Forms.Label()
        Me._DeviceThermalConstantSelectorLabel = New System.Windows.Forms.Label()
        Me._DeviceSensorTypeSelector = New isr.Core.Controls.SelectorComboBox()
        Me._DeviceThermalConstantSelector = New isr.Core.Controls.SelectorNumeric()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(379, 322)
        Me._Layout.TabIndex = 0
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._DeviceSensorTypeSelectorLabel)
        Me._Panel.Controls.Add(Me._DeviceThermalConstantSelectorLabel)
        Me._Panel.Controls.Add(Me._DeviceSensorTypeSelector)
        Me._Panel.Controls.Add(Me._DeviceThermalConstantSelector)
        Me._Panel.Location = New System.Drawing.Point(24, 114)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(330, 93)
        Me._Panel.TabIndex = 0
        '
        '_DeviceSensorTypeSelectorLabel
        '
        Me._DeviceSensorTypeSelectorLabel.AutoSize = True
        Me._DeviceSensorTypeSelectorLabel.Location = New System.Drawing.Point(41, 48)
        Me._DeviceSensorTypeSelectorLabel.Name = "_DeviceSensorTypeSelectorLabel"
        Me._DeviceSensorTypeSelectorLabel.Size = New System.Drawing.Size(82, 17)
        Me._DeviceSensorTypeSelectorLabel.TabIndex = 7
        Me._DeviceSensorTypeSelectorLabel.Text = "Sensor Type:"
        '
        '_DeviceThermalConstantSelectorLabel
        '
        Me._DeviceThermalConstantSelectorLabel.AutoSize = True
        Me._DeviceThermalConstantSelectorLabel.Location = New System.Drawing.Point(11, 15)
        Me._DeviceThermalConstantSelectorLabel.Name = "_DeviceThermalConstantSelectorLabel"
        Me._DeviceThermalConstantSelectorLabel.Size = New System.Drawing.Size(113, 17)
        Me._DeviceThermalConstantSelectorLabel.TabIndex = 6
        Me._DeviceThermalConstantSelectorLabel.Text = "Thermal Constant:"
        '
        '_DeviceSensorTypeSelector
        '
        Me._DeviceSensorTypeSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DeviceSensorTypeSelector.DirtyBackColor = System.Drawing.Color.Orange
        Me._DeviceSensorTypeSelector.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._DeviceSensorTypeSelector.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DeviceSensorTypeSelector.Location = New System.Drawing.Point(126, 44)
        Me._DeviceSensorTypeSelector.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceSensorTypeSelector.Name = "_DeviceSensorTypeSelector"
        Me._DeviceSensorTypeSelector.SelectorIcon = CType(resources.GetObject("_DeviceSensorTypeSelector.SelectorIcon"), System.Drawing.Image)
        Me._DeviceSensorTypeSelector.Size = New System.Drawing.Size(182, 25)
        Me._DeviceSensorTypeSelector.TabIndex = 5
        Me._DeviceSensorTypeSelector.Watermark = Nothing
        '
        '_DeviceThermalConstantSelector
        '
        Me._DeviceThermalConstantSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DeviceThermalConstantSelector.DirtyBackColor = System.Drawing.Color.Orange
        Me._DeviceThermalConstantSelector.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._DeviceThermalConstantSelector.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DeviceThermalConstantSelector.Location = New System.Drawing.Point(126, 12)
        Me._DeviceThermalConstantSelector.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceThermalConstantSelector.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me._DeviceThermalConstantSelector.Minimum = New Decimal(New Integer() {20, 0, 0, 0})
        Me._DeviceThermalConstantSelector.Name = "_DeviceThermalConstantSelector"
        Me._DeviceThermalConstantSelector.SelectorIcon = CType(resources.GetObject("_DeviceThermalConstantSelector.SelectorIcon"), System.Drawing.Image)
        Me._DeviceThermalConstantSelector.Size = New System.Drawing.Size(74, 25)
        Me._DeviceThermalConstantSelector.TabIndex = 4
        Me._DeviceThermalConstantSelector.Value = New Decimal(New Integer() {20, 0, 0, 0})
        Me._DeviceThermalConstantSelector.Watermark = Nothing
        '
        'SensorView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "SensorView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _DeviceSensorTypeSelectorLabel As Windows.Forms.Label
    Private WithEvents _DeviceThermalConstantSelectorLabel As Windows.Forms.Label
    Private WithEvents _DeviceSensorTypeSelector As Core.Controls.SelectorComboBox
    Private WithEvents _DeviceThermalConstantSelector As Core.Controls.SelectorNumeric
End Class
