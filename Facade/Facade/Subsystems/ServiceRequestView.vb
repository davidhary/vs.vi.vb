'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\ServiceRequestView.vb
'
' summary:	Service request view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A ServiceRequest view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ServiceRequestView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._ServiceRequestFlagsComboBox.ComboBox.DataSource = Nothing
        Me._ServiceRequestFlagsComboBox.ComboBox.Items.Clear()
        Me._ServiceRequestFlagsComboBox.ComboBox.DataSource = [Enum].GetNames(GetType(isr.VI.Pith.ServiceRequests)).ToList

        Me._ServiceRequestMaskNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._ServiceRequestMaskNumeric.NumericUpDownControl.Minimum = 0
        Me._ServiceRequestMaskNumeric.NumericUpDownControl.Maximum = 255
        Me._ServiceRequestMaskNumeric.NumericUpDownControl.Hexadecimal = True
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ServiceRequestView"/> </summary>
    ''' <returns> A <see cref="ServiceRequestView"/>. </returns>
    Public Shared Function Create() As ServiceRequestView
        Dim view As ServiceRequestView = Nothing
        Try
            view = New ServiceRequestView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Adds service request mask. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub AddServiceRequestMask()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} adding service request mask"
            Dim selectedFlag As VI.Pith.ServiceRequests = CType(Me._ServiceRequestFlagsComboBox.SelectedItem, isr.VI.Pith.ServiceRequests)
            Me._ServiceRequestMaskNumeric.Value = CInt(Me._ServiceRequestMaskNumeric.Value) Or CInt(selectedFlag)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Removes the service request mask. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveServiceRequestMask()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} removing a service request mask"
            Dim selectedFlag As VI.Pith.ServiceRequests = CType(Me._ServiceRequestFlagsComboBox.SelectedItem, isr.VI.Pith.ServiceRequests)
            Me._ServiceRequestMaskNumeric.Value = CInt(Me._ServiceRequestMaskNumeric.Value) And Not CInt(selectedFlag)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Toggle end of settling request enabled. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ToggleEndOfSettlingRequestEnabled()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} enabling end of settling time request"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.StatusSubsystemBase.ToggleEndOfScanService(Me._ServiceRequestEnabledMenuItem.Checked,
                                                                 Me._UsingNegativeTransitionsMenuItem.Checked,
                                                                 CType(CInt(Me._ServiceRequestMaskNumeric.Value), isr.VI.Pith.ServiceRequests))
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(ServiceRequestView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SRQ "

    ''' <summary> Event handler. Called by ServiceRequestMaskAddButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ServiceRequestMaskAddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.AddServiceRequestMask()
    End Sub

    ''' <summary>
    ''' Event handler. Called by ServiceRequestMaskRemoveButton for click events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ServiceRequestMaskRemoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.RemoveServiceRequestMask()
    End Sub

    ''' <summary> Toggle service request menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToggleServiceRequestMenuItem_Click(sender As Object, e As EventArgs) Handles _ToggleServiceRequestMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ToggleEndOfSettlingRequestEnabled()
    End Sub

    ''' <summary> Hexadecimal tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub HexadecimalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _HexadecimalToolStripMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me._ServiceRequestMaskNumeric.NumericUpDownControl.Hexadecimal = Me._HexadecimalToolStripMenuItem.Checked
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
