﻿''' <summary> Part Cold resistance. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-23 </para>
''' </remarks>
Public Class ColdResistance
    Inherits ColdResistanceBase
    Implements System.ICloneable

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clones an existing measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ColdResistance)
        MyBase.New(value)
        If value IsNot Nothing Then
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return New ColdResistance(Me)
    End Function

#End Region

End Class
