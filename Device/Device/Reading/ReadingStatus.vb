''' <summary> Defines a <see cref="System.Int32">Status</see> reading. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-01 </para>
''' </remarks>
Public Class ReadingStatus
    Inherits ReadingValue

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    Public Sub New(ByVal readingType As ReadingElementTypes)
        MyBase.New(readingType)
    End Sub

    ''' <summary> Constructs a copy of an existing value. </summary>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As ReadingStatus)
        MyBase.New(model)
    End Sub

#End Region

#Region " VALUES "

    ''' <summary> Gets the Status Value. </summary>
    ''' <remarks> Handles the case where the status value was saved as infinite. </remarks>
    ''' <value> The Status Value. </value>
    Public ReadOnly Property StatusValue As Long?
        Get
            Return If(Me.Value.HasValue,
                CLng(If(Me.Value.Value < 0, 0, If(Me.Value.Value > Long.MaxValue, Long.MaxValue, Me.Value.Value))),
                New Long?)
        End Get
    End Property

    ''' <summary> Query if 'bit' is bit. </summary>
    ''' <param name="bit"> The bit. </param>
    ''' <returns> <c>true</c> if bit; otherwise <c>false</c> </returns>
    Public Function IsBit(ByVal bit As Integer) As Boolean
        Return Me.StatusValue.HasValue AndAlso (1 And (Me.StatusValue.Value >> bit)) = 1
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return If(Me.StatusValue.HasValue, $"0x{Me.StatusValue.Value:X}", "empty")
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <param name="nibbleCount"> Number of nibbles. </param>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString(ByVal nibbleCount As Integer) As String
        Return If(Me.StatusValue.HasValue, String.Format(String.Format("0x{{0:X{0}}}", nibbleCount), Me.StatusValue.Value), Me.ToString)
    End Function

#End Region

End Class

