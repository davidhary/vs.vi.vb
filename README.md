# VI Core libraries

Virtual Instruments (VI) libraries using the [IVI Foundation](https://www.ivifoundation.org) VISA.

* [Runtime Pre-Requisites](#Runtime-Pre-Requisites)
* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

## Runtime Pre-Requisites[](#){name=Runtime-Pre-Requisites}

### .Net Framework 4.7.2
[Microsoft /.NET Framework](https://dotnet.microsoft.com/download)
.NET Framework must installed before proceeding with this installation.

### IVI Visa
[IVI VISA](http://www.ivifoundation.org) 5.11 and above is required for accessing devices.
The IVI VISA implementation can be obtained from either one of the following vendors: 
* Compiled using VISA Shared Components version: 5.12.0

#### Keysight I/O Suite
The [Keysight](https://www.keysight.com/en/pd-1985909/io-libraries-suite) I/O Suite 18.1 and above is recommended.
* Compiled using I/O Library Suite revision: 18.1.26209.5 released 2020-10-15.

#### NI VISA 
[NI VISA](https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210)
revision 19.0 and above can be used.

## Source Code[](#){name=Source-Code}
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
* [Typed Units](https://www.bitbucket.org/davidhary/vs.core) - Typed Units Project
* [VI](https://www.bitbucket.org/davidhary/vs.vi.core) - VI Core Libraries.

```
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.vi.core.git
git clone git@bitbucket.org:davidhary/arebis.typedunits.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Core\Core
.\Libraries\VS\Core\TypedUnits
.\Libraries\VS\IO\VI
```
### Thermal Transient Meter Repositories
The Thermal Transient Meter requires cloning:
* [Numerical](https://www.bitbucket.org/davidhary/vs.numerical) - Numerical Libraries

```
git clone git@bitbucket.org:davidhary/vs.core.git
```
into:
```
.\Libraries\VS\Algorithms\Numerical
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

## Facilitated By[](#){name=FacilitatedBy}
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows
* [IVI VISA](http://www.ivifoundation.org) - IVI Foundation VISA
* [Keysight](https://www.keysight.com/find/iosuiteproductcounter) - I/O Libraris
* [Test Script Builder](https://www.tek.com/keithley-test-script-builder) - Test Script Builder

## Authors[](#){name=Authors}
* [ATE Coder](https://www.IntegratedScientificResources.com)

## Acknowledgments[](#){name=Acknowledgments}
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](https://www.tek.com/keithley-test-script-builder)  
[Keysight](https://www.keysight.com/find/iosuiteproductcounter)
