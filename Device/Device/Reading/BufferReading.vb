'---------------------------------------------------------------------------------------------------
' file:		.\Reading\BufferReading.vb
'
' summary:	Buffer reading class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.ParseExtensions

''' <summary> A buffer reading. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-07-23 </para>
''' </remarks>
Public Class BufferReading

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.ClearThis()
        Me._BuildTimestamp = DateTimeOffset.Now
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="data">                       The value. </param>
    ''' <param name="orderedReadingElementTypes"> List of types of the reading elements. </param>
    Public Sub New(ByVal data As Queue(Of String), ByVal orderedReadingElementTypes As IEnumerable(Of ReadingElementTypes))
        Me.New()
        Me._OrderedReadingElementTypes = orderedReadingElementTypes
        Me._ReadingElementTypes = BufferReading.JoinReadingElementTypes(orderedReadingElementTypes)
        Me.ParseThis(data, orderedReadingElementTypes)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="measuredElementtype">        The type of the measured element. </param>
    ''' <param name="data">                       The value. </param>
    ''' <param name="orderedReadingElementTypes"> The ordered list of types of the readings. </param>
    Public Sub New(ByVal measuredElementtype As ReadingElementTypes, ByVal data As Queue(Of String), ByVal orderedReadingElementTypes As IEnumerable(Of ReadingElementTypes))
        Me.New(data, orderedReadingElementTypes)
        Me.MeasuredElementType = measuredElementtype
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="data">                       The value. </param>
    ''' <param name="firstReading">               The first buffer reading. </param>
    ''' <param name="orderedReadingElementTypes"> The ordered list of types of the readings. </param>
    Public Sub New(ByVal data As Queue(Of String), ByVal firstReading As BufferReading, ByVal orderedReadingElementTypes As IEnumerable(Of ReadingElementTypes))
        Me.New()
        Me._OrderedReadingElementTypes = orderedReadingElementTypes
        Me._ReadingElementTypes = BufferReading.JoinReadingElementTypes(Me.OrderedReadingElementTypes)
        Me.ParseThis(data, firstReading)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="reading"> The reading. </param>
    Public Sub New(ByVal reading As BufferReading)
        Me.New()
        If reading IsNot Nothing Then
            Me._OrderedReadingElementTypes = reading.OrderedReadingElementTypes
            Me._ReadingElementTypes = reading.ReadingElementTypes
            Me._Reading = reading.Reading
            Me._Value = reading.Value
            Me._StatusReading = reading.StatusReading
            Me.StatusWord = reading.StatusWord
            Me._TimestampReading = reading.TimestampReading
            Me._Timestamp = reading.Timestamp
            Me._BuildTimestamp = reading.BuildTimestamp
            Me._FractionalSecond = reading.FractionalSecond
            Me._FractionalTimespan = reading.FractionalTimespan
            Me._RelativeTimespan = reading.RelativeTimespan
            Me._UnitReading = reading.UnitReading
            If reading.Amount Is Nothing Then
                If Not String.IsNullOrEmpty(Me._UnitReading) Then
                    Me._Amount = BufferReading.BuildAmount(reading.Value, reading.UnitReading)
                End If
            Else
                Me._Amount = New Arebis.TypedUnits.Amount(reading.Amount)
            End If
        End If
    End Sub

    ''' <summary> Validated the given buffer reading. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bufferReading"> The buffer reading. </param>
    ''' <returns> A BufferReading. </returns>
    Public Shared Function Validated(ByVal bufferReading As BufferReading) As BufferReading
        If bufferReading Is Nothing Then Throw New ArgumentNullException(NameOf(bufferReading))
        Return bufferReading
    End Function

    ''' <summary> Clears this object to its blank/initial state. </summary>
    Private Sub ClearThis()
        Me._Reading = String.Empty
        Me._StatusReading = String.Empty
        Me._StatusWord = 0
        Me._TimestampReading = String.Empty
        Me._Timestamp = DateTimeOffset.MinValue
        Me._BuildTimestamp = DateTimeOffset.MinValue
        Me._FractionalSecond = 0
        Me._FractionalTimespan = TimeSpan.Zero
        Me._RelativeTimespan = TimeSpan.Zero
        Me._UnitReading = String.Empty
        Me._Amount = Nothing
    End Sub

    ''' <summary> Gets a list of types of the readings. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> A list of types of the readings. </value>
    Public ReadOnly Property OrderedReadingElementTypes As IEnumerable(Of ReadingElementTypes)

    ''' <summary> Join reading element types. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The Reading Element Types. </returns>
    Private Shared Function JoinReadingElementTypes(ByVal values As IEnumerable(Of ReadingElementTypes)) As ReadingElementTypes
        Dim result As ReadingElementTypes
        For Each v As VI.ReadingElementTypes In values
            result = result Or v
        Next
        Return result
    End Function

    ''' <summary> Gets the type of the measured element. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The type of the measured element. </value>
    Public Property MeasuredElementType As ReadingElementTypes

    ''' <summary> Gets a list of types of the reading elements. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> A list of types of the reading elements. </value>
    Public ReadOnly Property ReadingElementTypes As ReadingElementTypes

    ''' <summary> Gets the reading. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The reading. </value>
    Public ReadOnly Property Reading As String

    ''' <summary> Gets the timestamp reading. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The timestamp reading. </value>
    Public ReadOnly Property TimestampReading As String

    ''' <summary> Parses the given value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data">                       The value. </param>
    ''' <param name="orderedReadingElementTypes"> The ordered list of types of the readings elements. </param>
    Private Sub ParseThis(ByVal data As Queue(Of String), ByVal orderedReadingElementTypes As IEnumerable(Of ReadingElementTypes))
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        Me.ClearThis()
        Me._BuildTimestamp = DateTimeOffset.UtcNow
        For Each readingElementType As ReadingElementTypes In orderedReadingElementTypes
            Select Case readingElementType
                Case VI.ReadingElementTypes.Reading
                    If data.Any Then
                        Me._Reading = data.Dequeue
                    End If
                Case VI.ReadingElementTypes.Channel
                    If data.Any Then
                        Me.ParseChannel(data.Dequeue)
                    End If
                Case VI.ReadingElementTypes.Status
                    If data.Any Then
                        Me.ParseStatus(data.Dequeue)
                    End If
                Case VI.ReadingElementTypes.Timestamp
                    If data.Any Then
                        Me.ParseTimestamp(data.Dequeue)
                    End If
                Case VI.ReadingElementTypes.Time
                    If data.Any Then
                        Me.ParseTimestamp(data.Dequeue)
                    End If
                Case VI.ReadingElementTypes.Units
                    If data.Any Then
                        Me.BuildAmount(data.Dequeue)
                    End If
            End Select
        Next
    End Sub

    ''' <summary> Parses the given value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data">         The value. </param>
    ''' <param name="firstReading"> The first buffer reading. </param>
    Private Sub ParseThis(ByVal data As Queue(Of String), ByVal firstReading As BufferReading)
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        If firstReading Is Nothing Then Throw New ArgumentNullException(NameOf(firstReading))
        Me.ParseThis(data, firstReading.OrderedReadingElementTypes)
        If Not String.IsNullOrWhiteSpace(Me._TimestampReading) Then
            Me.AdjustRelativeTimespanThis(firstReading)
        End If
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets the status reading. </summary>
    ''' <value> The status reading. </value>
    Public ReadOnly Property StatusReading As String

    ''' <summary> Gets the has status. </summary>
    ''' <value> The has status. </value>
    Public ReadOnly Property HasStatus As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.StatusReading)
        End Get
    End Property

    ''' <summary> Gets the status word. </summary>
    ''' <value> The status word. </value>
    Public ReadOnly Property StatusWord As Integer

    ''' <summary> Parse status. </summary>
    ''' <param name="reading"> The reading. </param>
    Public Sub ParseStatus(ByVal reading As String)
        Me._StatusReading = reading
        If String.IsNullOrWhiteSpace(reading) Then
            Me._StatusWord = 0
        Else
            Dim value As Double = Double.Parse(reading, Globalization.NumberStyles.AllowDecimalPoint Or Globalization.NumberStyles.AllowExponent)
            Me._StatusWord = CInt(value)
        End If
    End Sub

    ''' <summary> Applies the status described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub ApplyStatus(ByVal value As Integer)
        Me._StatusReading = value.ToString
        Me._StatusWord = value
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Gets the has channel reading. </summary>
    ''' <value> The has channel reading. </value>
    Public ReadOnly Property HasChannelReading As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ChannelReading)
        End Get
    End Property

    ''' <summary> Gets the Channel reading. </summary>
    ''' <value> The Channel reading. </value>
    Public ReadOnly Property ChannelReading As String

    ''' <summary> Gets the Channel number. </summary>
    ''' <value> The Channel number. </value>
    Public ReadOnly Property ChannelNumber As Integer

    ''' <summary> Parse Channel. </summary>
    ''' <param name="reading"> The reading. </param>
    Private Sub ParseChannel(ByVal reading As String)
        Me._ChannelReading = reading
        Dim value As Double = Double.Parse(reading, Globalization.NumberStyles.AllowDecimalPoint Or Globalization.NumberStyles.AllowExponent)
        Me._ChannelNumber = CInt(value)
    End Sub

#End Region

#Region " AMOUNT "

    ''' <summary>
    ''' Gets the indication that the buffer readign has a reading.  The buffer reading could consist
    ''' of status values alone.
    ''' </summary>
    ''' <value> The has reading. </value>
    Public ReadOnly Property HasReading As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.Reading)
        End Get
    End Property

    ''' <summary> Gets the reading caption. </summary>
    ''' <value> The reading caption. </value>
    Public ReadOnly Property ReadingCaption As String
        Get
            Return If(Me.Amount Is Nothing,
                If(String.IsNullOrEmpty(Me.UnitReading), $"-.---- ", $"-.---- {Me.UnitReading}"),
                $"{Me.Amount} {Me.Amount.Unit}")
        End Get
    End Property

    ''' <summary> Gets the unit reading. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The unit reading. </value>
    Public ReadOnly Property UnitReading As String

    ''' <summary> The amount. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The amount. </value>
    Public ReadOnly Property Amount As Arebis.TypedUnits.Amount

    ''' <summary> Gets the value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The value. </value>
    Public ReadOnly Property Value As Double

    ''' <summary> Builds an amount. </summary>
    ''' <param name="unit"> The reading. </param>
    Public Sub BuildAmount(ByVal unit As String)
        Me._UnitReading = unit
        If Me.HasReading Then
            If Not Double.TryParse(Me.Reading, Me._Value) Then
                Me._Value = Double.NaN
            End If
        Else
            Me._Value = 0
        End If
        Me._Amount = BuildAmount(Me.Value, Me._UnitReading)
    End Sub

    ''' <summary> Builds an amount. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="unit"> The reading. </param>
    Public Sub BuildAmount(ByVal unit As Arebis.TypedUnits.Unit)
        If unit Is Nothing Then Throw New ArgumentNullException(NameOf(unit))
        Me._UnitReading = unit.ToString
        If Me.HasReading Then
            If Not Double.TryParse(Me.Reading, Me._Value) Then
                Me._Value = Double.NaN
            End If
        Else
            Me._Value = 0
        End If
        Me._Amount = New Arebis.TypedUnits.Amount(Me.Value, unit)
    End Sub

    ''' <summary> Return an amount form a reading containing value and unit. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reading"> The reading. </param>
    ''' <returns> Reading as an Arebis.TypedUnits.Amount. </returns>
    Public Shared Function ToAmount(ByVal reading As String) As Arebis.TypedUnits.Amount
        If String.IsNullOrWhiteSpace(reading) Then Throw New ArgumentNullException(NameOf(reading))
        ' strip out the non-numeric part of the number
        Dim valueReading As String = reading.ExtractNumberPart()
        Dim unitReading As String = reading.Substring(valueReading.Length)
        Dim value As Double = 0
        If Not Double.TryParse(valueReading, value) Then
            value = Double.NaN
        End If
        Return BuildAmount(value, unitReading)
    End Function

    ''' <summary> Dictionary of units. </summary>
    Private Shared _UnitDictionary As Dictionary(Of String, Arebis.TypedUnits.Unit)

    ''' <summary> Gets a dictionary of units. </summary>
    ''' <value> A Dictionary of units. </value>
    Public Shared ReadOnly Property UnitDictionary() As IDictionary(Of String, Arebis.TypedUnits.Unit)
        Get
            If BufferReading._UnitDictionary Is Nothing Then
                BufferReading._UnitDictionary = New Dictionary(Of String, Arebis.TypedUnits.Unit) From {
                    {"OHM4W", Arebis.StandardUnits.ElectricUnits.Ohm},
                    {"OHM", Arebis.StandardUnits.ElectricUnits.Ohm},
                    {"VOLT", Arebis.StandardUnits.ElectricUnits.Volt},
                    {"VDC", Arebis.StandardUnits.ElectricUnits.Volt},
                    {"VAC", Arebis.StandardUnits.ElectricUnits.Volt},
                    {"AMP", Arebis.StandardUnits.ElectricUnits.Ampere},
                    {"ADC", Arebis.StandardUnits.ElectricUnits.Ampere},
                    {"AAC", Arebis.StandardUnits.ElectricUnits.Ampere},
                    {"KELV", Arebis.StandardUnits.TemperatureUnits.Kelvin},
                    {"FAHR", Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit},
                    {"CELS", Arebis.StandardUnits.TemperatureUnits.DegreeCelsius},
                    {"DB", Arebis.StandardUnits.UnitlessUnits.Decibel},
                    {"HZ", Arebis.StandardUnits.FrequencyUnits.Hertz},
                    {"C", Arebis.StandardUnits.TemperatureUnits.DegreeCelsius},
                    {"F", Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit},
                    {"K", Arebis.StandardUnits.TemperatureUnits.Kelvin}
                }
            End If
            Return BufferReading._UnitDictionary
        End Get
    End Property

    ''' <summary> Parse unit. </summary>
    ''' <param name="unitReading"> The reading. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ParseUnit(ByVal unitReading As String) As (Value As String, Unit As Arebis.TypedUnits.Unit)
        Dim unit As New KeyValuePair(Of String, Arebis.TypedUnits.Unit)("VOLT", Arebis.StandardUnits.ElectricUnits.Volt)
        If String.IsNullOrWhiteSpace(unitReading) Then Return (unit.Key, unit.Value)
        For Each unit In BufferReading.UnitDictionary
            If unitReading.Contains(unit.Key) Then
                Exit For
            End If
        Next
        Return (unit.Key, unit.Value)
    End Function

    ''' <summary> Builds an amount. </summary>
    ''' <param name="value">       The value. </param>
    ''' <param name="unitReading"> The reading. </param>
    ''' <returns> An Arebis.TypedUnits.Amount. </returns>
    Public Shared Function BuildAmount(ByVal value As Double, ByVal unitReading As String) As Arebis.TypedUnits.Amount
        Return New Arebis.TypedUnits.Amount(value, BufferReading.ParseUnit(unitReading).Unit)
    End Function

#End Region

#Region " TIMESTAMP "

    ''' <summary> Gets the has timestamp. </summary>
    ''' <value> The has timestamp. </value>
    Public ReadOnly Property HasTimestamp As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.TimestampReading)
        End Get
    End Property

    ''' <summary> Parse timestamp. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timestamp"> The time stamp rounded down to the second. </param>
    Private Sub ParseTimestamp(ByVal timestamp As String)
        If String.IsNullOrWhiteSpace(timestamp) Then Throw New ArgumentNullException(NameOf(timestamp))
        Me._TimestampReading = timestamp
        Dim q As New Queue(Of String)(timestamp.Split("."c))
        Me._Timestamp = DateTimeOffset.Parse(q.Dequeue)
        Me._FractionalSecond = Double.Parse($".{q.Dequeue}")
        Me._FractionalTimespan = TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * Me._FractionalSecond))
        Me._MeterTime = Me._Timestamp.Add(Me.FractionalTimespan)
    End Sub

    ''' <summary> Parse timestamp. </summary>
    ''' <param name="firstTimestamp">  The first time stamp. </param>
    ''' <param name="elapsedTimespan"> The elapsed timespan. </param>
    Public Sub ParseTimestamp(ByVal firstTimestamp As DateTimeOffset, ByVal elapsedTimespan As TimeSpan)
        Me._BuildTimestamp = firstTimestamp.Add(elapsedTimespan)
        Me._Timestamp = Me._BuildTimestamp
        Me._TimestampReading = Me.Timestamp.ToString("O")
        Me._FractionalSecond = elapsedTimespan.Ticks / TimeSpan.TicksPerSecond
        Me._FractionalTimespan = elapsedTimespan
        Me._MeterTime = Me._Timestamp
    End Sub

    ''' <summary> Gets or sets the local date time offset when the reading was constructed. </summary>
    ''' <value> The local date time offset  when the reading was constructed. </value>
    Public ReadOnly Property BuildTimestamp As DateTimeOffset

    ''' <summary> Gets or sets the meter time. </summary>
    ''' <value> The meter time. </value>
    Public ReadOnly Property MeterTime As DateTimeOffset

    ''' <summary> Gets or sets the time stamp rounded down to the second. </summary>
    ''' <value> The time stamp rounded down to the second. </value>
    Public ReadOnly Property Timestamp As DateTimeOffset

    ''' <summary> Gets or sets the fractional second. </summary>
    ''' <value> The fractional second. </value>
    Public ReadOnly Property FractionalSecond As Double

    ''' <summary> Gets or sets the fractional timestamp. </summary>
    ''' <remarks> Converted from the fractional second of the instrument timestamp f. </remarks>
    ''' <value> The fractional timestamp. </value>
    Public ReadOnly Property FractionalTimespan As TimeSpan

    ''' <summary> Gets or sets the timespan relative to the first reading. </summary>
    ''' <value> The relative timespan. </value>
    Public ReadOnly Property RelativeTimespan As TimeSpan

    ''' <summary> Parses the given value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data">         The value. </param>
    ''' <param name="firstReading"> The first buffer reading. </param>
    Public Sub Parse(ByVal data As Queue(Of String), ByVal firstReading As BufferReading)
        If data Is Nothing Then Throw New ArgumentNullException(NameOf(data))
        If firstReading Is Nothing Then Throw New ArgumentNullException(NameOf(firstReading))
        Me.ParseThis(data, firstReading.OrderedReadingElementTypes)
        Me.AdjustRelativeTimespanThis(firstReading)
    End Sub

    ''' <summary> Adjust relative timespan. </summary>
    ''' <param name="firstReading"> The first buffer reading. </param>
    Private Sub AdjustRelativeTimespanThis(ByVal firstReading As BufferReading)
        Me._RelativeTimespan = If(firstReading Is Nothing,
            TimeSpan.Zero,
            Me.Timestamp.Subtract(firstReading.Timestamp).Add(Me.FractionalTimespan).Subtract(firstReading.FractionalTimespan))
    End Sub

    ''' <summary> Adjust relative timespan. </summary>
    ''' <param name="firstReading"> The first buffer reading. </param>
    Public Sub AdjustRelativeTimespan(ByVal firstReading As BufferReading)
        Me.AdjustRelativeTimespanThis(firstReading)
    End Sub

#End Region

End Class

''' <summary> A buffer readings binding list. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-07-23 </para>
''' </remarks>
Public Class BufferReadingBindingList
    Inherits isr.Core.Constructs.InvokingBindingList(Of BufferReading)

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    Public Sub New()
        MyBase.New
        Me.NewThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
    '''                             delegate. </param>
    Public Sub New(synchronizer As ISynchronizeInvoke)
        MyBase.New(synchronizer)
        Me.NewThis()
    End Sub

    ''' <summary> New s this object. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    Private Sub NewThis()
        Me._FirstReading = New BufferReading()
        Me._LastReading = New BufferReading()
    End Sub

#End Region

#Region " ADD "

    ''' <summary> Parses the reading and adds values to the collection. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="data">             The data. </param>
    ''' <param name="referenceReading"> The reference reading, which is required for parsing the
    '''                                 data. </param>
    Public Overloads Sub Add(ByVal data As String, ByVal referenceReading As BufferReading)
        If String.IsNullOrWhiteSpace(data) Then Throw New ArgumentNullException(NameOf(data))
        Dim raiseListChangedEventsWasEnabled As Boolean = Me.RaiseListChangedEvents
        Try
            Dim q As New Queue(Of String)(data.Split(","c))
            Me.RaiseListChangedEvents = False
            Do While q.Any
                Me.Add(q, referenceReading)
            Loop
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        Catch
            Throw
        Finally
            Me.RaiseListChangedEvents = raiseListChangedEventsWasEnabled
        End Try

    End Sub

    ''' <summary> Parses the reading and adds values to the collection. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="readingTimestampQueue"> The reading plus timestamp pair of values to add. </param>
    ''' <param name="referenceReading">      The reference reading, which is required for parsing the
    '''                                      data. </param>
    Public Overloads Sub Add(ByVal readingTimestampQueue As Queue(Of String), ByVal referenceReading As BufferReading)
        If readingTimestampQueue Is Nothing Then Throw New ArgumentNullException(NameOf(readingTimestampQueue))
        Me.Add(New BufferReading(readingTimestampQueue, referenceReading, New ReadingElementTypes() {VI.ReadingElementTypes.Reading, VI.ReadingElementTypes.Timestamp}))
    End Sub

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' This does not notify of list changes.
    ''' </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As BufferReading)
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        If Not Me.Any Then Me._FirstReading = item
        Me._LastReading = item
        MyBase.Add(item)
    End Sub

    ''' <summary> Adds and notify of binding change. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub AddAndResetBinding(ByVal item As BufferReading)
        Dim raiseListChangedEventsWasEnabled As Boolean = Me.RaiseListChangedEvents
        Try
            Me.RaiseListChangedEvents = False
            Me.Add(item)
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        Catch
            Throw
        Finally
            Me.RaiseListChangedEvents = raiseListChangedEventsWasEnabled
        End Try
    End Sub

    ''' <summary> Parses the reading and adds values to the collection. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The values to add. </param>
    Public Overloads Sub Add(ByVal values As BufferReading())
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim raiseListChangedEventsWasEnabled As Boolean = Me.RaiseListChangedEvents
        Try
            Me.RaiseListChangedEvents = False
            For Each br As BufferReading In values
                Me.Add(New BufferReading(br))
            Next
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        Catch
            Throw
        Finally
            Me.RaiseListChangedEvents = raiseListChangedEventsWasEnabled
        End Try
    End Sub

    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    Public Overloads Sub Clear()
        Dim raiseListChangedEventsWasEnabled As Boolean = Me.RaiseListChangedEvents
        Try
            Me.RaiseListChangedEvents = False
            MyBase.Clear()
            Me._FirstReading = New BufferReading()
            Me._LastReading = New BufferReading()
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        Catch
            Throw
        Finally
            Me.RaiseListChangedEvents = raiseListChangedEventsWasEnabled
        End Try
    End Sub

#End Region

#Region " FIRST AND LAST VALUES "

    ''' <summary> Gets or sets the first reading. </summary>
    ''' <value> The first reading. </value>
    Public ReadOnly Property FirstReading As BufferReading

    ''' <summary> Gets or sets the last reading. </summary>
    ''' <value> The last reading. </value>
    Public ReadOnly Property LastReading As BufferReading

#End Region

End Class

