''' <summary> Defines the SCPI Contact Check Limit subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para> 
''' David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
''' </remarks>
Public MustInherit Class ContactCheckLimitBase
    Inherits NumericLimitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ContactCheckLimitBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(4, statusSubsystem)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="limitNumber">     The limit number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal limitNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(limitNumber, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Enabled = False
        Me.FailureBits = 15
    End Sub

#End Region

#Region " FAILURE BITS "

    ''' <summary> The failure bits. </summary>
    Private _FailureBits As Integer?

    ''' <summary> Gets or sets the cached Failure Bits. </summary>
    ''' <value> The Failure Bits or none if not set or unknown. </value>
    Public Overloads Property FailureBits As Integer?
        Get
            Return Me._FailureBits
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.FailureBits, value) Then
                Me._FailureBits = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Failure Bits. </summary>
    ''' <param name="value"> The current Failure Bits. </param>
    ''' <returns> The Failure Bits or none if unknown. </returns>
    Public Function ApplyFailureBits(ByVal value As Integer) As Integer?
        Me.WriteFailureBits(value)
        Return Me.QueryFailureBits()
    End Function

    ''' <summary> Gets or sets Failure Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM:COMP:SOUR2?". </remarks>
    ''' <value> The Failure Bits query command. </value>
    Protected Overridable Property FailureBitsQueryCommand As String

    ''' <summary> Queries the current Failure Bits. </summary>
    ''' <returns> The Failure Bits or none if unknown. </returns>
    Public Function QueryFailureBits() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.FailureBitsQueryCommand) Then
            Me.FailureBits = Me.Session.Query(0I, Me.BuildCommand(Me.FailureBitsQueryCommand))
        End If
        Return Me.FailureBits
    End Function

    ''' <summary> Gets or sets Failure Bits command format. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM:COMP:SOUR2 {0}". </remarks>
    ''' <value> The Failure Bits command format. </value>
    Protected Overridable Property FailureBitsCommandFormat As String

    ''' <summary> Write the Failure Bits without reading back the value from the device. </summary>
    ''' <param name="value"> The current Failure Bits. </param>
    ''' <returns> The Failure Bits or none if unknown. </returns>
    Public Function WriteFailureBits(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.FailureBitsCommandFormat) Then
            Me.Session.WriteLine(Me.BuildCommand(Me.FailureBitsCommandFormat), value)
        End If
        Me.FailureBits = value
        Return Me.FailureBits
    End Function

#End Region

End Class
