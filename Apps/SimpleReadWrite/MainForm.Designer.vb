
Partial Public Class MainForm

    Private _WriteTextBox As System.Windows.Forms.TextBox
    Private _ReadTextBox As System.Windows.Forms.TextBox
    Private WithEvents QueryButton As System.Windows.Forms.Button
    Private WithEvents WriteButton As System.Windows.Forms.Button
    Private WithEvents ReadButton As System.Windows.Forms.Button
    Private WithEvents OpenSessionButton As System.Windows.Forms.Button
    Private WithEvents ClearButton As System.Windows.Forms.Button
    Private WithEvents CloseSessionButton As System.Windows.Forms.Button
    Private _WraiteTextBoxLabel As System.Windows.Forms.Label
    Private _ReadTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents ResourcesComboBox As ComboBox
    Private WithEvents ResourcesComboBoxLabel As Label
    Private WithEvents MultipleResourcesCheckBox As CheckBox

    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private _Components As System.ComponentModel.Container = Nothing

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.QueryButton = New System.Windows.Forms.Button()
        Me.WriteButton = New System.Windows.Forms.Button()
        Me.ReadButton = New System.Windows.Forms.Button()
        Me.OpenSessionButton = New System.Windows.Forms.Button()
        Me._WriteTextBox = New System.Windows.Forms.TextBox()
        Me._ReadTextBox = New System.Windows.Forms.TextBox()
        Me.ClearButton = New System.Windows.Forms.Button()
        Me.CloseSessionButton = New System.Windows.Forms.Button()
        Me._WraiteTextBoxLabel = New System.Windows.Forms.Label()
        Me._ReadTextBoxLabel = New System.Windows.Forms.Label()
        Me.ResourcesComboBox = New System.Windows.Forms.ComboBox()
        Me.ResourcesComboBoxLabel = New System.Windows.Forms.Label()
        Me.MultipleResourcesCheckBox = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'queryButton
        '
        Me.QueryButton.Location = New System.Drawing.Point(71, 129)
        Me.QueryButton.Name = "queryButton"
        Me.QueryButton.Size = New System.Drawing.Size(67, 23)
        Me.QueryButton.TabIndex = 3
        Me.QueryButton.Text = "Query"
        '
        'writeButton
        '
        Me.WriteButton.Location = New System.Drawing.Point(143, 129)
        Me.WriteButton.Name = "writeButton"
        Me.WriteButton.Size = New System.Drawing.Size(67, 23)
        Me.WriteButton.TabIndex = 4
        Me.WriteButton.Text = "Write"
        '
        'readButton
        '
        Me.ReadButton.Location = New System.Drawing.Point(214, 129)
        Me.ReadButton.Name = "readButton"
        Me.ReadButton.Size = New System.Drawing.Size(67, 23)
        Me.ReadButton.TabIndex = 5
        Me.ReadButton.Text = "Read"
        '
        '_OpenSessionButton
        '
        Me.OpenSessionButton.Location = New System.Drawing.Point(5, 5)
        Me.OpenSessionButton.Name = "_OpenSessionButton"
        Me.OpenSessionButton.Size = New System.Drawing.Size(92, 22)
        Me.OpenSessionButton.TabIndex = 0
        Me.OpenSessionButton.Text = "Open Session"
        '
        'writeTextBox
        '
        Me._WriteTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._WriteTextBox.Location = New System.Drawing.Point(5, 101)
        Me._WriteTextBox.Name = "writeTextBox"
        Me._WriteTextBox.Size = New System.Drawing.Size(277, 20)
        Me._WriteTextBox.TabIndex = 2
        Me._WriteTextBox.Text = "*IDN?\n"
        '
        '_ReadTextBox
        '
        Me._ReadTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ReadTextBox.Location = New System.Drawing.Point(5, 157)
        Me._ReadTextBox.Multiline = True
        Me._ReadTextBox.Name = "_ReadTextBox"
        Me._ReadTextBox.ReadOnly = True
        Me._ReadTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._ReadTextBox.Size = New System.Drawing.Size(277, 124)
        Me._ReadTextBox.TabIndex = 6
        Me._ReadTextBox.TabStop = False
        '
        'clearButton
        '
        Me.ClearButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ClearButton.Location = New System.Drawing.Point(6, 288)
        Me.ClearButton.Name = "clearButton"
        Me.ClearButton.Size = New System.Drawing.Size(276, 24)
        Me.ClearButton.TabIndex = 7
        Me.ClearButton.Text = "Clear"
        '
        '_CloseSessionButton
        '
        Me.CloseSessionButton.Location = New System.Drawing.Point(97, 5)
        Me.CloseSessionButton.Name = "_CloseSessionButton"
        Me.CloseSessionButton.Size = New System.Drawing.Size(92, 22)
        Me.CloseSessionButton.TabIndex = 1
        Me.CloseSessionButton.Text = "Close Session"
        '
        'stringToWriteLabel
        '
        Me._WraiteTextBoxLabel.AutoSize = True
        Me._WraiteTextBoxLabel.Location = New System.Drawing.Point(5, 85)
        Me._WraiteTextBoxLabel.Name = "stringToWriteLabel"
        Me._WraiteTextBoxLabel.Size = New System.Drawing.Size(77, 13)
        Me._WraiteTextBoxLabel.TabIndex = 8
        Me._WraiteTextBoxLabel.Text = "String to Write:"
        '
        '_ReadTextBoxLabel
        '
        Me._ReadTextBoxLabel.AutoSize = True
        Me._ReadTextBoxLabel.Location = New System.Drawing.Point(5, 140)
        Me._ReadTextBoxLabel.Name = "_ReadTextBoxLabel"
        Me._ReadTextBoxLabel.Size = New System.Drawing.Size(56, 13)
        Me._ReadTextBoxLabel.TabIndex = 9
        Me._ReadTextBoxLabel.Text = "Received:"
        '
        '_ResourcesComboBox
        '
        Me.ResourcesComboBox.FormattingEnabled = True
        Me.ResourcesComboBox.Location = New System.Drawing.Point(5, 56)
        Me.ResourcesComboBox.Name = "_ResourcesComboBox"
        Me.ResourcesComboBox.Size = New System.Drawing.Size(277, 21)
        Me.ResourcesComboBox.TabIndex = 10
        '
        '_ResourcesComboBoxLabel
        '
        Me.ResourcesComboBoxLabel.AutoSize = True
        Me.ResourcesComboBoxLabel.Location = New System.Drawing.Point(5, 40)
        Me.ResourcesComboBoxLabel.Name = "_ResourcesComboBoxLabel"
        Me.ResourcesComboBoxLabel.Size = New System.Drawing.Size(92, 13)
        Me.ResourcesComboBoxLabel.TabIndex = 11
        Me.ResourcesComboBoxLabel.Text = "Resource Names:"
        '
        '_MultipleResourcesCheckBox
        '
        Me.MultipleResourcesCheckBox.AutoSize = True
        Me.MultipleResourcesCheckBox.Location = New System.Drawing.Point(195, 8)
        Me.MultipleResourcesCheckBox.Name = "_MultipleResourcesCheckBox"
        Me.MultipleResourcesCheckBox.Size = New System.Drawing.Size(90, 17)
        Me.MultipleResourcesCheckBox.TabIndex = 12
        Me.MultipleResourcesCheckBox.Text = "> 1 Resource"
        Me.MultipleResourcesCheckBox.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(287, 317)
        Me.Controls.Add(Me.MultipleResourcesCheckBox)
        Me.Controls.Add(Me.ResourcesComboBoxLabel)
        Me.Controls.Add(Me.ResourcesComboBox)
        Me.Controls.Add(Me._ReadTextBoxLabel)
        Me.Controls.Add(Me._WraiteTextBoxLabel)
        Me.Controls.Add(Me.CloseSessionButton)
        Me.Controls.Add(Me.ClearButton)
        Me.Controls.Add(Me._ReadTextBox)
        Me.Controls.Add(Me._WriteTextBox)
        Me.Controls.Add(Me.OpenSessionButton)
        Me.Controls.Add(Me.ReadButton)
        Me.Controls.Add(Me.WriteButton)
        Me.Controls.Add(Me.QueryButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(295, 316)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Simple Read/Write"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region

End Class
