'---------------------------------------------------------------------------------------------------
' file:		Pith\Code\ResourcesProviderBase.vb
'
' summary:	Resources provider base class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

''' <summary> The resources provider base class. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-21 </para>
''' </remarks>
Public MustInherit Class ResourcesProviderBase
    Implements IDisposable

#Region " RESOURCE FINDER "

    ''' <summary> Gets or sets the resource finder. </summary>
    ''' <value> The resource finder. </value>
    Public Property ResourceFinder As VI.Pith.ResourceFinderBase

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Gets or sets the sentinel indicating weather this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public MustOverride ReadOnly Property IsDummy As Boolean

    ''' <summary> Parse resource. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> A <see cref="ResourceNameInfo"/>. </returns>
    Public MustOverride Function ParseResource(ByVal resourceName As String) As ResourceNameInfo

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <returns> List of all resources. </returns>
    Public MustOverride Function FindResources() As IEnumerable(Of String)

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> List of all resources. </returns>
    Public MustOverride Function FindResources(ByVal filter As String) As IEnumerable(Of String)

    ''' <summary> Tries to find resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindResources() As (Success As Boolean, Details As String)

    ''' <summary> Tries to find resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindResources(ByVal filter As String) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

    ''' <summary> Returns true if the specified resource exists in the resource names cache. </summary>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    Public MustOverride Function Exists(ByVal resourceName As String) As Boolean

#End Region

#Region " INTERFACES "

    ''' <summary> Searches for the interface in the resource names cache. </summary>
    ''' <param name="resourceName"> The interface resource name. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    Public MustOverride Function InterfaceExists(ByVal resourceName As String) As Boolean

    ''' <summary> Searches for all interfaces in the resource names cache. </summary>
    ''' <returns> The found interface resource names. </returns>
    Public MustOverride Function FindInterfaces() As IEnumerable(Of String)

    ''' <summary> Try find interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindInterfaces() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

    ''' <summary> Searches for the interfaces in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public MustOverride Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)

    ''' <summary> Try find interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

#End Region

#Region " INSTRUMENTS  "

    ''' <summary> Searches for the instrument in the resource names cache. </summary>
    ''' <param name="resourceName"> The instrument resource name. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    Public MustOverride Function FindInstrument(ByVal resourceName As String) As Boolean

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments() As IEnumerable(Of String)

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindInstruments() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    Public MustOverride Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                                    ByVal interfaceNumber As Integer) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

    ''' <summary> Searches for the first resource in the resource names cache. </summary>
    ''' <param name="resourceNames"> List of names of the resources. </param>
    ''' <returns> The found resource. </returns>
    Public Function FindResource(ByVal resourceNames As IEnumerable(Of String)) As String
        Dim result As String = String.Empty
        If resourceNames IsNot Nothing AndAlso resourceNames.Any Then
            For Each rn As String In resourceNames
                If Me.FindInstrument(rn) Then
                    result = rn
                    Exit For
                End If
            Next
        End If
        Return result
    End Function

#End Region

#Region " REVISION MANANGEMENT "

    ''' <summary> Determine if the elements of the two versions are equal. </summary>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <param name="actualVersion">   The actual version. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal expectedVersion As String, actualVersion As String) As Boolean
        Dim delimiter As Char = "."c
        Return ResourcesProviderBase.AreEqual(expectedVersion.Split(delimiter), actualVersion.Split(delimiter))
    End Function

    ''' <summary> Determine if the elements of the two versions are equal. </summary>
    ''' <param name="expectedValues"> The expected values. </param>
    ''' <param name="actualValues">   The actual values. </param>
    ''' <returns> <c>true</c> if equal; otherwise <c>false</c> </returns>
    Public Shared Function AreEqual(ByVal expectedValues As String(), actualValues As String()) As Boolean
        Dim result As Boolean = True
        For i As Integer = 0 To Math.Min(expectedValues.Count, actualValues.Count) - 1
            result = result AndAlso String.Equals(expectedValues(i), actualValues(i), StringComparison.OrdinalIgnoreCase)
        Next
        Return result
    End Function


    ''' <summary>
    ''' Validates the implementation and specification visa versions against settings values.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public MustOverride Function ValidateFunctionalVisaVersions() As (Success As Boolean, Details As String)

    ''' <summary> Validates the visa assembly versions against settings values. </summary>
    ''' <remarks> David, 2020-04-13. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public MustOverride Function ValidateVisaAssemblyVersions() As (Success As Boolean, Details As String)

#End Region

#Region " Disposable Support "

    ''' <summary> Gets or sets the disposed sentinel. </summary>
    ''' <value> The disposed. </value>
    Protected Property IsDisposed As Boolean


    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
    ''' unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub


    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncommented because Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class

''' <summary> Values that represent resource open states. </summary>
''' <remarks> David, 2020-04-10. </remarks>
Public Enum ResourceOpenState

    ''' <summary> An enum constant representing the unknown option. 
    ''' Used as an initial value but will never be returned for an open operation. </summary>
    <Description("Unknown")>
    Unknown

    ''' <summary> An enum constant representing the success. 
    ''' The session opened successfully. </summary>
    <Description("Success")>
    Success

    ''' <summary> An enum constant representing the device not responding. 
    ''' The session opened successfully but the device at the other end did not respond. </summary>
    <Description("Device Not Responding")>
    DeviceNotResponding

    ''' <summary> An enum constant representing the configuration not loaded. 
    ''' The session opened successfully but the specified configuration either does not exists
    ''' or could not be loaded. The session will use VISA-specified defaults. </summary>
    <Description("Configuration Not Loaded")>
    ConfigurationNotLoaded
End Enum
