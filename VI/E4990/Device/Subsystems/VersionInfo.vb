﻿''' <summary> Information about the version of a Keithley 2700 instrument. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class VersionInfo
    Inherits isr.VI.VersionInfoBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Parses the instrument firmware revision. </summary>
    ''' <remarks>
    ''' Agilent\sTechnologies,E4990A,MY54100800,A.02.12\n<para>
    ''' where; MY54100800 Is the serial number</para><para>
    ''' A Is the firmware revision top level revision</para><para>
    ''' 02.12 Is the firmware revision</para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="revision"> Specifies the instrument <see cref="FirmwareRevisionElements">board
    '''                         revisions</see>
    '''                         e.g., <c>xxxxx,/yyyyy/zzzzz</c> for the digital and display boards. 
    ''' </param>
    Protected Overrides Sub ParseFirmwareRevision(ByVal revision As String)

        If revision Is Nothing Then
            Throw New ArgumentNullException(NameOf(revision))
        ElseIf String.IsNullOrWhiteSpace(revision) Then
            MyBase.ParseFirmwareRevision(revision)
        Else
            MyBase.ParseFirmwareRevision(revision)

            ' get the revision sections
            Dim revSections As Queue(Of String) = New Queue(Of String)(revision.Split("."c))

            ' Rev: A.02.12
            If revSections.Any Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.Digital.ToString, revSections.Dequeue.Trim)
            If revSections.Any Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.Display.ToString, revSections.Dequeue.Trim)
            If revSections.Any Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.LedDisplay.ToString, revSections.Dequeue.Trim)

        End If

    End Sub

End Class

