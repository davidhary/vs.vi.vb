''' <summary> Defines the Calculations SCPI subsystem (often CALC2. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para> 
''' David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
''' </remarks>
Public MustInherit Class CalculateSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CalculateSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Return the average of the buffer contents. </summary>
    ''' <returns> The calculated buffer average. </returns>
    Public Function CalculateBufferAverage() As Double

        ' select average
        Me.Session.WriteLine("CALC2:FORM:MEAN")

        ' turn status on.
        Me.Session.WriteLine("CALC2:STAT:ON")

        ' do the calculation.
        Me.Session.WriteLine("CALC2:IMM")

        ' get the result
        Return Me.Session.Query(0.0F, ":CALC2:DATA")

    End Function

#End Region

End Class

