﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigureView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._RangeComboBox = New System.Windows.Forms.ComboBox()
        Me._TriggerCombo = New System.Windows.Forms.ComboBox()
        Me._TriggerComboBoxLabel = New System.Windows.Forms.Label()
        Me._ConfigureButton = New System.Windows.Forms.Button()
        Me._RangeComboBoxLabel = New System.Windows.Forms.Label()
        Me._ConfigureGroupBox = New System.Windows.Forms.GroupBox()
        Me._Layout.SuspendLayout()
        Me._ConfigureGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001!))
        Me._Layout.Controls.Add(Me._ConfigureGroupBox, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.00001!))
        Me._Layout.Size = New System.Drawing.Size(383, 326)
        Me._Layout.TabIndex = 0
        '
        '_RangeComboBox
        '
        Me._RangeComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)

        Me._RangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._RangeComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._RangeComboBox.Location = New System.Drawing.Point(11, 41)
        Me._RangeComboBox.Name = "_RangeComboBox"
        Me._RangeComboBox.Size = New System.Drawing.Size(310, 25)
        Me._RangeComboBox.TabIndex = 1
        '
        '_TriggerCombo
        '
        Me._TriggerCombo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._TriggerCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._TriggerCombo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._TriggerCombo.Items.AddRange(New Object() {"I", "V"})
        Me._TriggerCombo.Location = New System.Drawing.Point(11, 92)
        Me._TriggerCombo.Name = "_TriggerCombo"
        Me._TriggerCombo.Size = New System.Drawing.Size(310, 25)
        Me._TriggerCombo.TabIndex = 3
        '
        '_TriggerComboBoxLabel
        '
        Me._TriggerComboBoxLabel.Location = New System.Drawing.Point(9, 69)
        Me._TriggerComboBoxLabel.Name = "_TriggerComboBoxLabel"
        Me._TriggerComboBoxLabel.Size = New System.Drawing.Size(62, 21)
        Me._TriggerComboBoxLabel.TabIndex = 2
        Me._TriggerComboBoxLabel.Text = "Trigger: "
        Me._TriggerComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_ConfigureButton
        '
        Me._ConfigureButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ConfigureButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ConfigureButton.Location = New System.Drawing.Point(263, 132)
        Me._ConfigureButton.Name = "_ConfigureButton"
        Me._ConfigureButton.Size = New System.Drawing.Size(58, 30)
        Me._ConfigureButton.TabIndex = 4
        Me._ConfigureButton.Text = "&Apply"
        Me._ConfigureButton.UseVisualStyleBackColor = True
        '
        '_RangeComboBoxLabel
        '
        Me._RangeComboBoxLabel.AutoSize = True
        Me._RangeComboBoxLabel.Location = New System.Drawing.Point(9, 21)
        Me._RangeComboBoxLabel.Name = "_RangeComboBoxLabel"
        Me._RangeComboBoxLabel.Size = New System.Drawing.Size(52, 17)
        Me._RangeComboBoxLabel.TabIndex = 0
        Me._RangeComboBoxLabel.Text = "Range: "
        Me._RangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_ConfigureGroupBox
        '
        Me._ConfigureGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ConfigureGroupBox.Controls.Add(Me._RangeComboBox)
        Me._ConfigureGroupBox.Controls.Add(Me._TriggerCombo)
        Me._ConfigureGroupBox.Controls.Add(Me._TriggerComboBoxLabel)
        Me._ConfigureGroupBox.Controls.Add(Me._ConfigureButton)
        Me._ConfigureGroupBox.Controls.Add(Me._RangeComboBoxLabel)
        Me._ConfigureGroupBox.Location = New System.Drawing.Point(25, 78)
        Me._ConfigureGroupBox.Name = "_ConfigureGroupBox"
        Me._ConfigureGroupBox.Size = New System.Drawing.Size(332, 169)
        Me._ConfigureGroupBox.TabIndex = 9
        Me._ConfigureGroupBox.TabStop = False
        Me._ConfigureGroupBox.Text = "Configure Measurement"
        '
        'ConfigureView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "ConfigureView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(383, 326)
        Me._Layout.ResumeLayout(False)
        CType(Me.InfoProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ConfigureGroupBox.ResumeLayout(False)
        Me._ConfigureGroupBox.PerformLayout()

        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _ConfigureGroupBox As Windows.Forms.GroupBox
    Private WithEvents _RangeComboBox As Windows.Forms.ComboBox
    Private WithEvents _TriggerCombo As Windows.Forms.ComboBox
    Private WithEvents _TriggerComboBoxLabel As Windows.Forms.Label
    Private WithEvents _ConfigureButton As Windows.Forms.Button
    Private WithEvents _RangeComboBoxLabel As Windows.Forms.Label
End Class
