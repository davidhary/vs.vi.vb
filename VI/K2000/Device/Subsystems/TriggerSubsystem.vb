''' <summary> Defines a Trigger Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TriggerSubsystem
    Inherits VI.TriggerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.SupportedTriggerSources = VI.TriggerSources.Bus Or VI.TriggerSources.External Or
                                     VI.TriggerSources.Hold Or VI.TriggerSources.Immediate Or
                                     VI.TriggerSources.Manual Or VI.TriggerSources.Timer Or
                                     VI.TriggerSources.TriggerLink
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-08-06. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.MaximumTriggerCount = 99999
        Me.MaximumDelay = TimeSpan.FromSeconds(999999.999)
    End Sub

#End Region

#Region " ABORT / INIT COMMANDS "

    ''' <summary> Gets or sets the Abort command. </summary>
    ''' <value> The Abort command. </value>
    Protected Overrides Property AbortCommand As String = ":ABOR"

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <value> The initiate command. </value>
    Protected Overrides Property InitiateCommand As String = ":INIT"

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearCommand As String = String.Empty ' ":TRIG:CLE"

#End Region

#Region " AUTO DELAY "

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledCommandFormat As String = String.Empty '  ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledQueryCommand As String = String.Empty '  ":TRIG:DEL:AUTO?"

#End Region

#Region " CONTINUOUS "

    ''' <summary> Gets or sets the Continuous enabled command Format. </summary>
    ''' <value> The Continuous enabled query command. </value>
    Protected Overrides Property ContinuousEnabledCommandFormat As String = ":INIT:CONT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Continuous enabled query command. </summary>
    ''' <value> The Continuous enabled query command. </value>
    Protected Overrides Property ContinuousEnabledQueryCommand As String = ":INIT:CONT?"

#End Region

#Region " TRIGGER COUNT "

    ''' <summary> Gets or sets trigger count query command. </summary>
    ''' <value> The trigger count query command. </value>
    Protected Overrides Property TriggerCountQueryCommand As String = ":TRIG:COUN?"

    ''' <summary> Gets or sets trigger count command format. </summary>
    ''' <value> The trigger count command format. </value>
    Protected Overrides Property TriggerCountCommandFormat As String = ":TRIG:COUN {0}"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = ":TRIG:DEL {0:s\.FFFFFFF}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = ":TRIG:DEL?"

#End Region

#Region " DIRECTION (BYPASS) "

    ''' <summary> Gets or sets the Trigger Direction query command. </summary>
    ''' <value> The Trigger Direction query command. </value>
    Protected Overrides Property TriggerLayerBypassModeQueryCommand As String = ":TRIG:TCON:DIR?"

    ''' <summary> Gets or sets the Trigger Direction command format. </summary>
    ''' <value> The Trigger Direction command format. </value>
    Protected Overrides Property TriggerLayerBypassModeCommandFormat As String = ":TRIG:TCON:DIR {0}"

#End Region

#Region " INPUT LINE NUMBER--TRIGGER LINK ASYNC "

    ''' <summary> Gets or sets the Input Line Number command format. </summary>
    ''' <value> The Input Line Number command format. </value>
    Protected Overrides Property InputLineNumberCommandFormat As String = ":TRIG:TCON:ASYN:ILIN {0}"

    ''' <summary> Gets or sets the Input Line Number query command. </summary>
    ''' <value> The Input Line Number query command. </value>
    Protected Overrides Property InputLineNumberQueryCommand As String = ":TRIG:TCON:ASYN:ILIN?"

#End Region

#Region " OUTPUT LINE NUMBER--TRIGGER LINK ASYNC "

    ''' <summary> Gets or sets the Output Line Number command format. </summary>
    ''' <value> The Output Line Number command format. </value>
    Protected Overrides Property OutputLineNumberCommandFormat As String = ":TRIG:TCON:ASYN:OLIN {0}"

    ''' <summary> Gets or sets the Output Line Number query command. </summary>
    ''' <value> The Output Line Number query command. </value>
    Protected Overrides Property OutputLineNumberQueryCommand As String = ":TRIG:TCON:ASYN:OLIN?"

#End Region

#Region " SOURCE "

    ''' <summary> Gets or sets the Trigger Source query command. </summary>
    ''' <value> The Trigger Source query command. </value>
    Protected Overrides Property TriggerSourceQueryCommand As String = ":TRIG:SOUR?"

    ''' <summary> Gets or sets the Trigger Source command format. </summary>
    ''' <value> The Trigger Source command format. </value>
    Protected Overrides Property TriggerSourceCommandFormat As String = ":TRIG:SOUR {0}"

#End Region

#Region " TIMER TIME SPAN "

    ''' <summary> Gets or sets the Timer Interval command format. </summary>
    ''' <value> The query command format. </value>
    Protected Overrides Property TimerIntervalCommandFormat As String = ":TRIG:TIM {0:s\.FFFFFFF}"

    ''' <summary>
    ''' Gets or sets the Timer Interval format for converting the query to time span.
    ''' </summary>
    ''' <value> The Timer Interval query command. </value>
    Protected Overrides Property TimerIntervalFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the Timer Interval query command. </summary>
    ''' <value> The Timer Interval query command. </value>
    Protected Overrides Property TimerIntervalQueryCommand As String = ":TRIG:TIM?"

#End Region

#Region " TRIGGER STATE "

    ''' <summary> Queries the Trigger State. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' The <see cref="P:isr.VI.TriggerSubsystemBase.TriggerState">Trigger State</see> or none if
    ''' unknown.
    ''' </returns>
    Public Overrides Function QueryTriggerState() As TriggerState?
        Me.StatusSubsystem.OperationEventsBitmasks.Status = Me.StatusSubsystem.QueryOperationEventCondition.GetValueOrDefault(0)
        Select Case True
            Case Me.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Arming)
                Me.TriggerState = VI.TriggerState.Waiting
            Case Me.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Idle)
                If Me.IsTriggerStateActive Then
                    ' the first detection go to aborting
                    Me.TriggerState = VI.TriggerState.Aborting
                ElseIf Me.IsTriggerStateAborting Then
                    Me.TriggerState = VI.TriggerState.Aborted
                ElseIf Me.TriggerState = VI.TriggerState.Aborted Then
                    Me.TriggerState = VI.TriggerState.Idle
                End If
                Me.TriggerState = VI.TriggerState.Idle
            Case Me.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Measuring)
                Me.TriggerState = VI.TriggerState.Running
            Case Me.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn(OperationEventBitmaskKey.Triggering)
                Me.TriggerState = VI.TriggerState.Running
            Case Else
                If Me.IsTriggerStateIdle Then
                    ' if state was idle and no event was detected, state is running or waiting.
                    Me.TriggerState = VI.TriggerState.Running
                End If
        End Select
        Return Me.TriggerState
    End Function
#End Region

#Region " TRIGGER SUBSYSTEM TEST FUNCTIONS "

    ''' <summary> Builds external scan help message. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildExternalScanHelpMessage(ByVal triggerSubsystem As VI.TriggerSubsystemBase) As String
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine("Configure external scan taking a reading on each trigger input.")
        builder.AppendLine($"Trigger: Count = {triggerSubsystem.TriggerCount}; Source: {triggerSubsystem.TriggerSource}")
        builder.AppendLine("The meter takes ten sets Of readings, with each set spaced 1 seconds apart.")
        builder.AppendLine("Each of the three readings in each group taken as fast as possible. The")
        builder.AppendLine("The readings are stored in the buffer.")
        builder.AppendLine("SRQ is asserted SRQ When the buffer Is full.")
        builder.AppendLine("The program waits For the SRQ, then reads the readings from the buffer. ")
        Return builder.ToString
    End Function

    ''' <summary> Configure external scan. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="device">             The device. </param>
    ''' <param name="triggerCount">       Number of triggers. </param>
    ''' <param name="triggerLayerSource"> The trigger layer source. </param>
    ''' <param name="armLayer1Source">    The arm layer 1 source. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ConfigureExternalScan(ByVal device As K2002.K2002Device,
                                                 ByVal triggerCount As Integer,
                                                 ByVal triggerLayerSource As VI.TriggerSources,
                                                 ByVal armLayer1Source As VI.ArmSources) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If device Is Nothing Then
            result = (False, $"{NameOf(device)} is nothing")
        End If
        If Not result.Success Then Return result

        Dim activity As String = $"{device.ResourceNameCaption} configuring external scan"
        ' device.ResetKnownState()
        ' device.ClearExecutionState()

        Dim count As Integer = triggerCount
        ' TO_DO: Add service request handler to trace when done. 
        ' enable service requests
        ' device.Session.EnableServiceRequest()
        ' device.StatusSubsystem.EnableServiceRequest(device.Session.DefaultServiceRequestEnableBitmask)
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.TriggerSubsystem.Abort()

        device.SystemSubsystem.ApplyAutoZeroEnabled(True)
        device.FormatSubsystem.ApplyElements(ReadingElementTypes.Reading)
        device.SenseSubsystem.ApplyFunctionMode(VI.SenseFunctionModes.ResistanceFourWire)
        device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled(False)

        If count <= 1 Then
            ' setting the feed source to none causes -109,"Missing parameter" error
            ' device.TraceSubsystem.WriteFeedSource(Scpi.FeedSource.None)
            device.TriggerSubsystem.ApplyTriggerSource(triggerLayerSource)
            device.TriggerSubsystem.ApplyTriggerCount(count)

            If device.TriggerSubsystem.TriggerSource.Value = VI.TriggerSources.External Then
                device.Session.StatusPrompt = "Ready: Initiate meter and then scanner"
            ElseIf device.TriggerSubsystem.TriggerSource.Value = VI.TriggerSources.Immediate Then
                device.Session.StatusPrompt = "Ready: Initiate meter to take a reading"
            ElseIf device.TriggerSubsystem.TriggerSource.Value = VI.TriggerSources.Bus Then
                device.Session.StatusPrompt = "Ready: Click the Trigger button to take a reading"
            Else
                result = (False, $"Invalid trigger source {device.TriggerSubsystem.TriggerSource.Value}")
            End If
        Else
            ' use external if the meter waits for the scanner or immediate if the scanner waits for the meter.
            device.ArmLayer1Subsystem.ApplyArmSource(armLayer1Source)
            device.ArmLayer1Subsystem.ApplyArmCount(1)
            device.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
            device.ArmLayer2Subsystem.ApplyArmCount(1)
            device.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)
            device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.External)
            device.TriggerSubsystem.ApplyTriggerCount(count)
            device.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
            device.TriggerSubsystem.ApplyTriggerLayerBypassMode(VI.TriggerLayerBypassModes.Source)
            If count > 1 Then
                device.TraceSubsystem.WriteFeedSource(VI.FeedSources.Sense)
                ' device.TraceSubsystem.ApplyPointsCount(count)
                device.Session.WriteLine(":TRAC:POIN:AUTO 1")
                device.TraceSubsystem.WriteFeedControl(VI.FeedControls.Next)
            Else
                ' This does not work. Getting device errors:
                ' -109,"Missing parameter"
                ' -221,"Settings conflict", ""
                ' -222,"Parameter data out of range", ""
                ' -102,"Syntax error", ""
                ' -109,"Missing parameter", ""
                'device.Session.WriteLine(":TRAC:POIN:AUTO 1")
                'device.TraceSubsystem.WriteFeedControl(Scpi.FeedControl.Never)
                'device.TraceSubsystem.WriteFeedSource(Scpi.FeedSource.None)
            End If
            If device.ArmLayer1Subsystem.ArmSource.Value = VI.ArmSources.External Then
                device.Session.StatusPrompt = "Ready: Initiate meter and then scanner"
            ElseIf device.ArmLayer1Subsystem.ArmSource.Value = VI.ArmSources.Immediate Then
                device.Session.StatusPrompt = "Ready: Initiate scanner and then meter"
            Else
                result = (False, $"Invalid arms source {device.ArmLayer1Subsystem.ArmSource.Value}")
            End If
        End If
        Return result
    End Function

    ''' <summary> Executes the external scan operation. </summary>
    ''' <remarks> David, 2020-04-06. </remarks>
    ''' <param name="session"> The session. </param>
    ''' <returns> The (Status As String, Value As String) </returns>
    Public Shared Function ExecuteExternalScan(ByVal session As VI.Pith.SessionBase) As (Status As String, Value As String)
        session.WriteLine("STAT:PRES;*CLS")
        session.WriteLine("STAT:MEAS:ENAB 512")
        session.WriteLine("*SRE 1")

        session.WriteLine("INIT")                       ' Trigger the scan
        '                                               ' wait for the buffer to fill or some other event that shows up in the status register>>>
        Dim srq As Pith.ServiceRequests = session.ReadStatusRegister()
        '                                               ' wait for service request
        Do Until Pith.ServiceRequests.RequestingService = (srq And Pith.ServiceRequests.RequestingService)
            srq = session.ReadStatusRegister(TimeSpan.FromMilliseconds(10))
        Loop
        session.WriteLine("STAT:MEAS?")                 ' get measurement status
        Dim status As String = session.ReadFreeLineTrimEnd()
        session.WriteLine("FORM:ELEM READ,UNIT")        ' set reading format.

        session.WriteLine("TRAC:DATA?")                 ' Extract the data from the buffer
        Return (status, session.ReadFreeLine())
    End Function

#End Region

End Class
