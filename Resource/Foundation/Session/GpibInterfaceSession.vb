'---------------------------------------------------------------------------------------------------
' file:		.\Session\GpibInterfaceSession.vb
'
' summary:	Gpib interface session class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Foundation.ExceptionExtensions
Imports isr.VI.Foundation.GpibInterfaceExtensions

''' <summary> A gpib interface session. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-21 </para>
''' </remarks>
Public Class GpibInterfaceSession
    Inherits VI.Pith.InterfaceSessionBase

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support"

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Try
                    Me.CloseSessionThis()
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Failed discarding enabled events.",
                                 $"Failed discarding enabled events. {ex.ToFullBlownString}")
                End Try
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region

    #End Region

    #Region " SESSION "

    ''' <summary> Gets the sentinel indicating whether this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public Overrides ReadOnly Property IsDummy As Boolean = False

    ''' <summary> Gets the gpib interface. </summary>
    ''' <value> The gpib interface. </value>
    Private ReadOnly Property GpibInterface As Ivi.Visa.IGpibInterfaceSession

    ''' <summary> Opens a session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="timeout">      The timeout. </param>
    Public Overrides Sub OpenSession(ByVal resourceName As String, ByVal timeout As TimeSpan)
        Me.OpenSessionThis(resourceName, timeout)
        MyBase.OpenSession(resourceName, timeout)
    End Sub

    ''' <summary> Opens a session. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="timeout">      The timeout. </param>
    ''' <returns> The Ivi.Visa.ResourceOpenStatus. </returns>
    Private Function OpenSessionThis(ByVal resourceName As String, ByVal timeout As TimeSpan) As Ivi.Visa.ResourceOpenStatus
        Dim openStatus As Ivi.Visa.ResourceOpenStatus
        Me._GpibInterface = TryCast(Ivi.Visa.GlobalResourceManager.Open(resourceName, Ivi.Visa.AccessModes.None, CInt(timeout.TotalMilliseconds), openStatus),
                                        Ivi.Visa.IGpibInterfaceSession)
        Return openStatus
    End Function

    ''' <summary> Opens a session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Overrides Sub OpenSession(ByVal resourceName As String)
        Me.OpenSessionThis(resourceName, Me.DefaultOpenTimeout)
        MyBase.OpenSession(resourceName)
    End Sub

    ''' <summary> Closes the session. </summary>
    Public Overrides Sub CloseSession()
        Me.CloseSessionThis()
        MyBase.CloseSession()
    End Sub

    ''' <summary> Closes the session. </summary>
    Private Sub CloseSessionThis()
        If Me._GpibInterface IsNot Nothing Then
            Me._GpibInterface.DiscardEvents(Ivi.Visa.EventType.AllEnabled)
            Me._GpibInterface.Dispose()
        End If
    End Sub

#End Region

#Region " GPIB INTERFACE "

    ''' <summary> Sends the interface clear. </summary>
    Public Overrides Sub SendInterfaceClear()
        Me.GpibInterface.SendInterfaceClear()
    End Sub

    ''' <summary> Gets the type of the hardware interface. </summary>
    ''' <value> The type of the hardware interface. </value>
    Public Overrides ReadOnly Property HardwareInterfaceType As VI.Pith.HardwareInterfaceType
        Get
            Return VI.Pith.HardwareInterfaceType.Gpib
        End Get
    End Property

    ''' <summary> Gets the hardware interface number. </summary>
    ''' <value> The hardware interface number. </value>
    Public Overrides ReadOnly Property HardwareInterfaceNumber As Integer
        Get
            Return Me.GpibInterface.HardwareInterfaceNumber
        End Get
    End Property

    ''' <summary> Returns all instruments to some default state. </summary>
    Public Overrides Sub ClearDevices()
        Me.GpibInterface.ClearDevices
    End Sub

    ''' <summary> Clears the specified device. </summary>
    ''' <param name="gpibAddress"> The instrument address. </param>
    Public Overrides Sub SelectiveDeviceClear(ByVal gpibAddress As Integer)
        Me.GpibInterface.SelectiveDeviceClear(gpibAddress)
    End Sub

    ''' <summary> Clears the specified device. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Overrides Sub SelectiveDeviceClear(ByVal resourceName As String)
        Me.GpibInterface.SelectiveDeviceClear(resourceName)
    End Sub

    ''' <summary> Clears the interface. </summary>
    Public Overrides Sub ClearInterface()
        Me.GpibInterface.ClearInterface()
    End Sub

#End Region

End Class

