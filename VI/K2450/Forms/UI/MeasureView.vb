'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\MeasureView.vb
'
' summary:	Measure view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Facade.ComboBoxExtensions

''' <summary> A measure view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class MeasureView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="MeasureView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As MeasureView
        Dim view As MeasureView = Nothing
        Try
            view = New MeasureView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K2450Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K2450Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K2450Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindMeasureSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K2450Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Gets the resistance sense subsystem . </summary>
    ''' <value> The Resistance Sense subsystem . </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Bind Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMeasureSubsystem(ByVal device As K2450Device)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._MeasureSubsystem = device.MeasureSubsystem
            Me.BindSubsystem(True, Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.SupportedFunctionModes))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.AutoRangeEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.AutoZeroEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FilterCount))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FilterCountRange))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FilterWindow))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FilterWindowRange))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.MovingAverageFilterEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FunctionMode))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FunctionRangeDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.FunctionUnit))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.OpenDetectorEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.PowerLineCycles))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.PowerLineCyclesDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.PowerLineCyclesRange))
            Me.HandlePropertyChanged(subsystem, NameOf(K2450.MeasureSubsystem.Range))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the supported function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub HandleSupportedFunctionModesChanged(ByVal subsystem As MeasureSubsystem)
        If subsystem IsNot Nothing AndAlso subsystem.SupportedFunctionModes <> VI.SenseFunctionModes.None Then
            Dim init As Boolean = Me.InitializingComponents
            Try
                Me.InitializingComponents = True
                Me._SenseFunctionComboBox.ListSupportedSenseFunctionModes(subsystem.SupportedFunctionModes)
            Catch
                Throw
            Finally
                Me.InitializingComponents = init
            End Try
        End If
    End Sub

    ''' <summary> Handles the function modes changed described by subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub HandleFunctionModesChanged(ByVal subsystem As MeasureSubsystem)
        Me._SenseFunctionComboBox.SelectSenseFunctionModes(subsystem.FunctionMode.GetValueOrDefault(VI.SenseFunctionModes.VoltageDC))
        Me.Device.MeasureSubsystem.QueryMeasureUnit()
        Me.Device.MeasureSubsystem.QueryOpenDetectorEnabled()
    End Sub

    ''' <summary> Handles the measure subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)

        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName

            Case NameOf(K2450.MeasureSubsystem.AutoRangeEnabled)
                If subsystem.AutoRangeEnabled.HasValue Then Me._AutoRangeCheckBox.Checked = subsystem.AutoRangeEnabled.Value

            Case NameOf(K2450.MeasureSubsystem.AutoZeroEnabled)
                If subsystem.AutoZeroEnabled.HasValue Then Me._AutoZeroCheckBox.Checked = subsystem.AutoZeroEnabled.Value

            Case NameOf(K2450.MeasureSubsystem.FilterCount)
                If subsystem.FilterCount.HasValue Then Me._FilterCountNumeric.Value = subsystem.FilterCount.Value

            Case NameOf(K2450.MeasureSubsystem.FilterCountRange)
                Me._FilterCountNumeric.Maximum = CDec(subsystem.FilterCountRange.Max)
                Me._FilterCountNumeric.Minimum = CDec(subsystem.FilterCountRange.Min)

            Case NameOf(K2450.MeasureSubsystem.FilterEnabled)
                If subsystem.FilterEnabled.HasValue Then Me._FilterEnabledCheckBox.Checked = subsystem.FilterEnabled.Value
                If Me._FilterEnabledCheckBox.Checked <> Me._FilterGroupBox.Enabled Then Me._FilterGroupBox.Enabled = Me._FilterEnabledCheckBox.Checked

            Case NameOf(K2450.MeasureSubsystem.FilterWindow)
                If subsystem.FilterWindow.HasValue Then Me._FilterWindowNumeric.Value = CDec(100 * subsystem.FilterWindow.Value)

            Case NameOf(K2450.MeasureSubsystem.FilterWindowRange)
                Me._FilterWindowNumeric.Maximum = 100 * CDec(subsystem.FilterWindowRange.Max)
                Me._FilterWindowNumeric.Minimum = 100 * CDec(subsystem.FilterWindowRange.Min)

            Case NameOf(K2450.MeasureSubsystem.MovingAverageFilterEnabled)
                If subsystem.MovingAverageFilterEnabled.HasValue Then Me._MovingAverageRadioButton.Checked = subsystem.MovingAverageFilterEnabled.Value
                If subsystem.MovingAverageFilterEnabled.HasValue Then Me._RepeatingAverageRadioButton.Checked = Not subsystem.MovingAverageFilterEnabled.Value

            Case NameOf(K2450.MeasureSubsystem.SupportedFunctionModes)
                Me.HandleSupportedFunctionModesChanged(subsystem)

            Case NameOf(K2450.MeasureSubsystem.FunctionMode)
                Me.HandleFunctionModesChanged(subsystem)

            Case NameOf(K2450.MeasureSubsystem.FunctionRange)
                Me._SenseRangeNumeric.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)

            Case NameOf(K2450.MeasureSubsystem.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces

            Case NameOf(K2450.MeasureSubsystem.FunctionUnit)
                Me._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:"
                Me._SenseRangeNumericLabel.Left = Me._SenseRangeNumeric.Left - Me._SenseRangeNumericLabel.Width

            Case NameOf(K2450.MeasureSubsystem.OpenDetectorEnabled)
                If subsystem.OpenDetectorEnabled.HasValue Then Me._OpenDetectorCheckBox.Checked = subsystem.OpenDetectorEnabled.Value

            Case NameOf(K2450.MeasureSubsystem.PowerLineCycles)
                If subsystem.PowerLineCycles.HasValue Then Me._PowerLineCyclesNumeric.Value = CDec(subsystem.PowerLineCycles.Value)

            Case NameOf(K2450.MeasureSubsystem.PowerLineCyclesRange)
                Me._PowerLineCyclesNumeric.Maximum = CDec(subsystem.PowerLineCyclesRange.Max)
                Me._PowerLineCyclesNumeric.Minimum = CDec(subsystem.PowerLineCyclesRange.Min)
                Me._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces

            Case NameOf(K2450.MeasureSubsystem.Range)
                If subsystem.Range.HasValue Then Me.SenseRangeSetter(subsystem.Range.Value)
        End Select
    End Sub

    ''' <summary> measure subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(MeasureSubsystem)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(MeasureSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, MeasureSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " DEVICE SETTINGS: FUNCTION MODE "

    ''' <summary> Gets the selected function mode. </summary>
    ''' <value> The selected function mode. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedFunctionMode() As VI.SenseFunctionModes
        Get
            Return Me._SenseFunctionComboBox.SelectedSenseFunctionModes
        End Get
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS: MEASURE "

    ''' <summary> Sense range setter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub SenseRangeSetter(ByVal value As Double)
        If value <= Me._SenseRangeNumeric.Maximum AndAlso value >= Me._SenseRangeNumeric.Minimum Then Me._SenseRangeNumeric.Value = CDec(value)
    End Sub

    ''' <summary>
    ''' Event handler. Called by _SenseFunctionComboBox for selected index changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SenseFunctionComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} checking function mode"
            Dim mode As VI.SenseFunctionModes = Me.SelectedFunctionMode
            If mode = VI.SenseFunctionModes.None Then Return
            activity = $"{Me.Device.ResourceNameCaption} applying selected function mode {mode}"
            Me._Device.MeasureSubsystem.ApplyFunctionMode(mode)
            activity = $"{Me.Device.ResourceNameCaption} reading "
            Me._Device.MeasureSubsystem.QueryPowerLineCycles()
            Me._Device.MeasureSubsystem.QueryAutoRangeEnabled()
            Me._Device.MeasureSubsystem.QueryRange()
            Me._Device.MeasureSubsystem.QueryMeasureUnit()
            Me._Device.MeasureSubsystem.QueryAutoZeroEnabled()
            Me._Device.MeasureSubsystem.QueryFilterEnabled()
            Me._Device.MeasureSubsystem.QueryFilterCount()
            Me._Device.MeasureSubsystem.QueryFilterWindow()
            Me._Device.MeasureSubsystem.QueryMovingAverageFilterEnabled()

            Me._Device.MeasureSubsystem.QueryOpenDetectorEnabled()
            Me.ReadStatusRegister()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the selected measurements settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ApplySenseSettings()
        If Not Nullable.Equals(Me.Device.MeasureSubsystem.PowerLineCycles, Me._PowerLineCyclesNumeric.Value) Then
            Me.Device.MeasureSubsystem.ApplyPowerLineCycles(Me._PowerLineCyclesNumeric.Value)
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.AutoRangeEnabled, Me._AutoRangeCheckBox.Checked) Then
            Me.Device.MeasureSubsystem.ApplyAutoRangeEnabled(Me._AutoRangeCheckBox.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.AutoZeroEnabled, Me._AutoZeroCheckBox.Checked) Then
            Me.Device.MeasureSubsystem.ApplyAutoZeroEnabled(Me._AutoZeroCheckBox.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.FilterEnabled, Me._FilterEnabledCheckBox.Checked) Then
            Me.Device.MeasureSubsystem.ApplyFilterEnabled(Me._FilterEnabledCheckBox.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.FilterCount, Me._FilterCountNumeric.Value) Then
            Me.Device.MeasureSubsystem.ApplyFilterCount(CInt(Me._FilterCountNumeric.Value))
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.MovingAverageFilterEnabled, Me._MovingAverageRadioButton.Checked) Then
            Me.Device.MeasureSubsystem.ApplyMovingAverageFilterEnabled(Me._MovingAverageRadioButton.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.OpenDetectorEnabled, Me._OpenDetectorCheckBox.Checked) Then
            Me.Device.MeasureSubsystem.ApplyOpenDetectorEnabled(Me._OpenDetectorCheckBox.Checked)
        End If

        If Me.Device.MeasureSubsystem.AutoRangeEnabled Then
            Me.Device.MeasureSubsystem.QueryRange()
        ElseIf Not Nullable.Equals(Me.Device.MeasureSubsystem.Range, Me._SenseRangeNumeric.Value) Then
            Me.Device.MeasureSubsystem.ApplyRange(CInt(Me._SenseRangeNumeric.Value))
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.FilterWindow, 0.01 * Me._FilterWindowNumeric.Value) Then
            Me.Device.MeasureSubsystem.ApplyFilterWindow(0.01 * Me._FilterWindowNumeric.Value)
        End If
    End Sub

    ''' <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ApplySenseSettingsButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying sense settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.SelectedFunctionMode <> Me.Device.MeasureSubsystem.FunctionMode.Value Then
                Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Info, "Set function first")
            Else
                Me.ApplySenseSettings()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Filter enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FilterEnabledCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _FilterEnabledCheckBox.CheckedChanged
        Me._FilterGroupBox.Enabled = Me._FilterEnabledCheckBox.Checked
    End Sub

    ''' <summary> Opens detector check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDetectorCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _OpenDetectorCheckBox.CheckedChanged
        Me._OpenDetectorCheckBox.Text = $"Open Detector {Me._OpenDetectorCheckBox.Checked.GetHashCode:'ON';'ON';'OFF'}"
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
