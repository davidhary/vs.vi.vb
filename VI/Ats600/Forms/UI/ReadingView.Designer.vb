﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ReadingView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._ServiceRequestValuesTextBox = New System.Windows.Forms.TextBox()
        Me._SrqRefratoryTimeNumericLabel = New System.Windows.Forms.Label()
        Me._SrqRefratoryTimeNumeric = New isr.Core.Controls.NumericUpDown()
        Me._SendComboBox = New System.Windows.Forms.ComboBox()
        Me._ReceiveTextBox = New System.Windows.Forms.TextBox()
        Me._ReceiveButton = New System.Windows.Forms.Button()
        Me._SendButton = New System.Windows.Forms.Button()
        Me._StatusByteLabel = New System.Windows.Forms.Label()
        Me._ReadStatusByteButton = New System.Windows.Forms.Button()
        Me._ClearErrorQueueButton = New System.Windows.Forms.Button()
        Me._ReadLastErrorButton = New System.Windows.Forms.Button()
        Me._ReadButton = New System.Windows.Forms.Button()
        Me._InitializeButton = New System.Windows.Forms.Button()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel.SuspendLayout()
        CType(Me._SrqRefratoryTimeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._ServiceRequestValuesTextBox)
        Me._Panel.Controls.Add(Me._SrqRefratoryTimeNumericLabel)
        Me._Panel.Controls.Add(Me._SrqRefratoryTimeNumeric)
        Me._Panel.Controls.Add(Me._SendComboBox)
        Me._Panel.Controls.Add(Me._ReceiveTextBox)
        Me._Panel.Controls.Add(Me._ReceiveButton)
        Me._Panel.Controls.Add(Me._SendButton)
        Me._Panel.Controls.Add(Me._StatusByteLabel)
        Me._Panel.Controls.Add(Me._ReadStatusByteButton)
        Me._Panel.Controls.Add(Me._ClearErrorQueueButton)
        Me._Panel.Controls.Add(Me._ReadLastErrorButton)
        Me._Panel.Controls.Add(Me._ReadButton)
        Me._Panel.Controls.Add(Me._InitializeButton)
        Me._Panel.Location = New System.Drawing.Point(13, 41)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(352, 240)
        Me._Panel.TabIndex = 0
        '
        '_ServiceRequestValuesTextBox
        '
        Me._ServiceRequestValuesTextBox.Location = New System.Drawing.Point(127, 8)
        Me._ServiceRequestValuesTextBox.Multiline = True
        Me._ServiceRequestValuesTextBox.Name = "_ServiceRequestValuesTextBox"
        Me._ServiceRequestValuesTextBox.ReadOnly = True
        Me._ServiceRequestValuesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._ServiceRequestValuesTextBox.Size = New System.Drawing.Size(216, 62)
        Me._ServiceRequestValuesTextBox.TabIndex = 26
        Me._ServiceRequestValuesTextBox.Text = "bit  Value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  2  Device Specific Error" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  3  Temperature Event" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  4  Message Avai" &
    "lable" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  5  Standard Event Status" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  6  Master Summery Status" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  7  Ready"
        '
        '_SrqRefratoryTimeNumericLabel
        '
        Me._SrqRefratoryTimeNumericLabel.AutoSize = True
        Me._SrqRefratoryTimeNumericLabel.Location = New System.Drawing.Point(223, 84)
        Me._SrqRefratoryTimeNumericLabel.Name = "_SrqRefratoryTimeNumericLabel"
        Me._SrqRefratoryTimeNumericLabel.Size = New System.Drawing.Size(72, 17)
        Me._SrqRefratoryTimeNumericLabel.TabIndex = 25
        Me._SrqRefratoryTimeNumericLabel.Text = "Delay [ms]:"
        '
        '_SrqRefratoryTimeNumeric
        '
        Me._SrqRefratoryTimeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SrqRefratoryTimeNumeric.Location = New System.Drawing.Point(298, 80)
        Me._SrqRefratoryTimeNumeric.Name = "_SrqRefratoryTimeNumeric"
        Me._SrqRefratoryTimeNumeric.NullValue = New Decimal(New Integer() {5, 0, 0, 0})
        Me._SrqRefratoryTimeNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._SrqRefratoryTimeNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._SrqRefratoryTimeNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._SrqRefratoryTimeNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._SrqRefratoryTimeNumeric.Size = New System.Drawing.Size(45, 25)
        Me._SrqRefratoryTimeNumeric.TabIndex = 24
        Me._SrqRefratoryTimeNumeric.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        '_SendComboBox
        '
        Me._SendComboBox.FormattingEnabled = True
        Me._SendComboBox.Items.AddRange(New Object() {"*CLS ", "*ESE 255", "*ESE?", "*ESR?", "*IDN? ", "*RST ", "*SRE 255", "*SRE?", "EROR?", "FLWR?", "FLRL?", "HEAD?", "LLIM?", "NEXT", "RAMP?", "SETD?", "TEMP?"})
        Me._SendComboBox.Location = New System.Drawing.Point(70, 149)
        Me._SendComboBox.Name = "_SendComboBox"
        Me._SendComboBox.Size = New System.Drawing.Size(273, 25)
        Me._SendComboBox.TabIndex = 23
        Me._SendComboBox.Text = "*IDN? "
        '
        '_ReceiveTextBox
        '
        Me._ReceiveTextBox.Location = New System.Drawing.Point(70, 182)
        Me._ReceiveTextBox.Multiline = True
        Me._ReceiveTextBox.Name = "_ReceiveTextBox"
        Me._ReceiveTextBox.ReadOnly = True
        Me._ReceiveTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._ReceiveTextBox.Size = New System.Drawing.Size(273, 50)
        Me._ReceiveTextBox.TabIndex = 22
        '
        '_ReceiveButton
        '
        Me._ReceiveButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReceiveButton.Location = New System.Drawing.Point(8, 181)
        Me._ReceiveButton.Name = "_ReceiveButton"
        Me._ReceiveButton.Size = New System.Drawing.Size(56, 30)
        Me._ReceiveButton.TabIndex = 21
        Me._ReceiveButton.Text = "&Read:"
        Me._ReceiveButton.UseVisualStyleBackColor = True
        '
        '_SendButton
        '
        Me._SendButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SendButton.Location = New System.Drawing.Point(8, 146)
        Me._SendButton.Name = "_SendButton"
        Me._SendButton.Size = New System.Drawing.Size(56, 30)
        Me._SendButton.TabIndex = 20
        Me._SendButton.Text = "&Send:"
        Me._SendButton.UseVisualStyleBackColor = True
        '
        '_StatusByteLabel
        '
        Me._StatusByteLabel.AutoSize = True
        Me._StatusByteLabel.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatusByteLabel.Location = New System.Drawing.Point(146, 85)
        Me._StatusByteLabel.Name = "_StatusByteLabel"
        Me._StatusByteLabel.Size = New System.Drawing.Size(63, 15)
        Me._StatusByteLabel.TabIndex = 19
        Me._StatusByteLabel.Text = "00000000"
        '
        '_ReadStatusByteButton
        '
        Me._ReadStatusByteButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadStatusByteButton.Location = New System.Drawing.Point(8, 77)
        Me._ReadStatusByteButton.Name = "_ReadStatusByteButton"
        Me._ReadStatusByteButton.Size = New System.Drawing.Size(135, 30)
        Me._ReadStatusByteButton.TabIndex = 18
        Me._ReadStatusByteButton.Text = "Read Status Byte:"
        Me._ReadStatusByteButton.UseVisualStyleBackColor = True
        '
        '_ClearErrorQueueButton
        '
        Me._ClearErrorQueueButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ClearErrorQueueButton.Location = New System.Drawing.Point(176, 112)
        Me._ClearErrorQueueButton.Name = "_ClearErrorQueueButton"
        Me._ClearErrorQueueButton.Size = New System.Drawing.Size(147, 30)
        Me._ClearErrorQueueButton.TabIndex = 16
        Me._ClearErrorQueueButton.Text = "Clear Error Queue"
        Me._ClearErrorQueueButton.UseVisualStyleBackColor = True
        '
        '_ReadLastErrorButton
        '
        Me._ReadLastErrorButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadLastErrorButton.Location = New System.Drawing.Point(8, 112)
        Me._ReadLastErrorButton.Name = "_ReadLastErrorButton"
        Me._ReadLastErrorButton.Size = New System.Drawing.Size(162, 30)
        Me._ReadLastErrorButton.TabIndex = 17
        Me._ReadLastErrorButton.Text = "Read Last Device Error"
        Me._ReadLastErrorButton.UseVisualStyleBackColor = True
        '
        '_ReadButton
        '
        Me._ReadButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReadButton.Location = New System.Drawing.Point(8, 8)
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(104, 30)
        Me._ReadButton.TabIndex = 14
        Me._ReadButton.Text = "&Read Temp"
        Me._ReadButton.UseVisualStyleBackColor = True
        '
        '_InitializeButton
        '
        Me._InitializeButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InitializeButton.Location = New System.Drawing.Point(8, 42)
        Me._InitializeButton.Name = "_InitializeButton"
        Me._InitializeButton.Size = New System.Drawing.Size(104, 30)
        Me._InitializeButton.TabIndex = 15
        Me._InitializeButton.Text = "&Initialize"
        Me._InitializeButton.UseVisualStyleBackColor = True
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(379, 322)
        Me._Layout.TabIndex = 1
        '
        'ReadingView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "ReadingView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        CType(Me._SrqRefratoryTimeNumeric, System.ComponentModel.ISupportInitialize).EndInit()



        Me._Layout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _ServiceRequestValuesTextBox As Windows.Forms.TextBox
    Private WithEvents _SrqRefratoryTimeNumericLabel As Windows.Forms.Label
    Private WithEvents _SrqRefratoryTimeNumeric As Core.Controls.NumericUpDown
    Private WithEvents _SendComboBox As Windows.Forms.ComboBox
    Private WithEvents _ReceiveTextBox As Windows.Forms.TextBox
    Private WithEvents _ReceiveButton As Windows.Forms.Button
    Private WithEvents _SendButton As Windows.Forms.Button
    Private WithEvents _StatusByteLabel As Windows.Forms.Label
    Private WithEvents _ReadStatusByteButton As Windows.Forms.Button
    Private WithEvents _ClearErrorQueueButton As Windows.Forms.Button
    Private WithEvents _ReadLastErrorButton As Windows.Forms.Button
    Private WithEvents _ReadButton As Windows.Forms.Button
    Private WithEvents _InitializeButton As Windows.Forms.Button
End Class
