'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase.vb
'
' summary:	Session base class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Threading
Imports System.Timers

Imports isr.Core.EscapeSequencesExtensions
Imports isr.VI.Pith.ExceptionExtensions

''' <summary> Base class for SessionBase. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public MustInherit Class SessionBase
    Inherits isr.Core.Models.ViewModelBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    Protected Sub New()
        MyBase.New()
        Me._OperationCompleted = New Boolean?
        ' Me._ResourceNameInfo = New ResourceNameInfo
        Me._Enabled = True
        Me._LastMessageReceived = String.Empty
        Me._LastMessageSent = String.Empty
        Me.UseDefaultTerminationThis()
        Me.InitializeServiceRequestRegisterBitsThis()
        Me._CommunicationTimeouts = New Collections.Generic.Stack(Of TimeSpan)
        AddHandler My.Settings.PropertyChanged, AddressOf Me.HandleSettingsPropertyChanged
        Me._DeviceClearRefractoryPeriod = TimeSpan.FromMilliseconds(1050)
        Me._InterfaceClearRefractoryPeriod = TimeSpan.FromMilliseconds(1050)
        Me._ResetRefractoryPeriod = TimeSpan.FromMilliseconds(200)
        Me._ClearRefractoryPeriod = TimeSpan.FromMilliseconds(100)
        Me._ReadDelay = TimeSpan.Zero
        Me._StatusReadDelay = TimeSpan.Zero
        Me.TimeoutCandidate = TimeSpan.FromMilliseconds(3000)
        Me._ServiceRequestEnableCommand = String.Format(Ieee488.Syntax.ServiceRequestEnableCommandFormat, 16) & isr.Core.EscapeSequencesExtensions.Methods.NewLineEscape
        Me._DefaultOperationCompleteBitmask = ServiceRequests.StandardEvent
        Me._DefaultOperationServiceRequestEnableBitmask = ServiceRequests.All And Not ServiceRequests.RequestingService And Not ServiceRequests.MessageAvailable
        Me._DefaultServiceRequestEnableBitmask = ServiceRequests.All And Not ServiceRequests.RequestingService
        Me._DefaultStandardEventEnableBitmask = StandardEvents.All And Not StandardEvents.RequestControl
        Me._OperationCompleteExpectedBitmask = Me._DefaultOperationCompleteBitmask
        Me._ServiceRequestWaitCompleteEnabledBitmask = Me._DefaultOperationCompleteBitmask
        Me._StandardEventWaitCompleteEnabledBitmask = Me._DefaultStandardEventEnableBitmask
    End Sub

    ''' <summary> Validates the given visa session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session"> The visa session. </param>
    ''' <returns> A SessionBase. </returns>
    Public Shared Function Validated(ByVal session As SessionBase) As SessionBase
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Return session
    End Function

#Region " Disposable Support "


    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the disposed status. </summary>
    ''' <value> The is disposed. </value>
    Protected ReadOnly Property IsDisposed As Boolean


    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.MessageNotificationLevel = NotifySyncLevel.None
                    Me.LastNativeError = Nothing
                    Me.DiscardAllEvents()
                    Me.RemoveServiceRequestedEventHandlers()
                    Me.RemovePropertyChangedEventHandlers()
                    Me.CommunicationTimeouts.Clear()
                    If Me.StatusByteLocker IsNot Nothing Then
                        Me._StatusByteLocker.Dispose() : Me._StatusByteLocker = Nothing
                    End If
                End If
                ' dispose of unmanaged code here; unmanaged code gets disposed if disposing or not.
                RemoveHandler My.Settings.PropertyChanged, AddressOf Me.HandleSettingsPropertyChanged
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' Overrides should Dispose(disposing As Boolean) has code to free unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

#End Region

#End Region

#Region " SESSION "


    ''' <summary>
    ''' Gets or sets the Enabled sentinel. When enabled, the session is allowed to engage actual
    ''' hardware; otherwise, opening the session does not attempt to link to the hardware.
    ''' </summary>
    ''' <value> The enabled sentinel. </value>
    Public Property Enabled As Boolean

    ''' <summary> Gets or sets the default open timeout. </summary>
    ''' <value> The default open timeout. </value>
    Public Property DefaultOpenTimeout As TimeSpan = My.Settings.DefaultOpenSessionTimeout


    ''' <summary>
    ''' Gets or sets the session open sentinel. When open, the session is capable of addressing the
    ''' hardware. See also <see cref="IsDeviceOpen"/>.
    ''' </summary>
    ''' <value> The is session open. </value>
    Public MustOverride ReadOnly Property IsSessionOpen As Boolean

    ''' <summary> True if device is open, false if not. </summary>
    Private _IsDeviceOpen As Boolean


    ''' <summary>
    ''' Gets or sets the Device Open sentinel. When open, the device is capable of addressing real
    ''' hardware if the session is open. See also <see cref="IsSessionOpen"/>.
    ''' </summary>
    ''' <value> The is device open. </value>
    Public Property IsDeviceOpen As Boolean
        Get
            Return Me._IsDeviceOpen
        End Get
        Protected Set(value As Boolean)
            If value <> Me.IsDeviceOpen Then
                Me._IsDeviceOpen = value
                Me.SyncNotifyPropertyChanged()
                Me.UpdateCaptions()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the state of the resource open. </summary>
    ''' <value> The resource open state. </value>
    Public Property ResourceOpenState As ResourceOpenState

    ''' <summary> Gets or sets the sentinel indicating weather this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public MustOverride ReadOnly Property IsDummy As Boolean

    ''' <summary> Executes the session open actions. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The short title of the device. </param>
    Protected Overridable Sub OnSessionOpen(ByVal resourceName As String, ByVal resourceTitle As String)
        Me.OnSessionOpen(resourceName, resourceTitle, ResourceOpenState.Success)
    End Sub

    ''' <summary> Executes the session open actions. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="resourceName">      The name of the resource. </param>
    ''' <param name="resourceTitle">     The short title of the device. </param>
    ''' <param name="resourceOpenState"> The resource open state. </param>
    Protected Overridable Sub OnSessionOpen(ByVal resourceName As String, ByVal resourceTitle As String, ByVal resourceOpenState As ResourceOpenState)
        ' Use SynchronizeCallbacks to specify that the object marshals callbacks across threads appropriately.
        Me.SynchronizeCallbacks = True
        Me.OpenResourceName = resourceName
        Me.OpenResourceTitle = resourceTitle
        Me.CandidateResourceTitle = Me.OpenResourceTitle
        Me.ResourceNameCaption = $"{Me.OpenResourceTitle}.{Me.OpenResourceName}"
        Me.ResourceTitleCaption = Me.OpenResourceTitle
        Me._LastInputOutputStopwatch = New Stopwatch
        Me.ResourceOpenState = resourceOpenState
        Me.IsDeviceOpen = resourceOpenState <> ResourceOpenState.Unknown
        Me.LastAction = $"{resourceName} open"
        Me.StatusPrompt = Me.LastAction
        Me.HandleSessionOpen(resourceName, resourceTitle)
        Me.CommunicationTimeouts.Push(Me.CommunicationTimeout)
    End Sub

    ''' <summary> Creates a session. </summary>
    ''' <remarks> Throws an exception if the resource is not accessible. </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="timeout">      The timeout. </param>
    Protected MustOverride Sub CreateSession(ByVal resourceName As String, ByVal timeout As TimeSpan)

    ''' <summary> Check if a session can be created. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="timeout">      The timeout. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function CanCreateSession(ByVal resourceName As String, ByVal timeout As TimeSpan) As (Success As Boolean, Details As String)
        Try
            Me.CreateSession(resourceName, timeout)
            Return (True, String.Empty)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString)
        Finally
            Try
                Me.DisposeSession()
            Catch
            Finally
            End Try
        End Try
    End Function


    ''' <summary>
    ''' Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
    ''' </summary>
    ''' <remarks>
    ''' Call this first. The synchronization context is captured as part of the property change and
    ''' other event handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The short title of the device. </param>
    ''' <param name="timeout">       The timeout. </param>
    Public Sub OpenSession(ByVal resourceName As String, ByVal resourceTitle As String, ByVal timeout As TimeSpan)
        Try
            Me.ClearLastError()
            Me._ResourceNameInfo = New ResourceNameInfo(resourceName)
            Me.CandidateResourceName = resourceName
            Me.CandidateResourceTitle = resourceTitle
            Me.LastAction = $"Opening {resourceName}"
            Me.StatusPrompt = Me.LastAction
            Me.CommunicationTimeouts.Clear()
            Me.CreateSession(resourceName, timeout)
            Me.OnSessionOpen(resourceName, resourceTitle)
        Catch ex As Exception
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
    ''' </summary>
    ''' <remarks>
    ''' Call this first. The synchronization context is captured as part of the property change and
    ''' other event handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The short title of the device. </param>
    Public Sub OpenSession(ByVal resourceName As String, ByVal resourceTitle As String)
        Me.OpenSession(resourceName, resourceTitle, Me.DefaultOpenTimeout)
    End Sub


    ''' <summary>
    ''' Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
    ''' </summary>
    ''' <remarks>
    ''' Call this first. The synchronization context is captured as part of the property change and
    ''' other event handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    Public Sub OpenSession(ByVal resourceName As String)
        Me.OpenSession(resourceName, resourceName, Me.DefaultOpenTimeout)
    End Sub


    ''' <summary>
    ''' Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
    ''' </summary>
    ''' <remarks>
    ''' Call this first. The synchronization context is captured as part of the property change and
    ''' other event handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="timeout">      The timeout. </param>
    Public Sub OpenSession(ByVal resourceName As String, ByVal timeout As TimeSpan)
        Me.OpenSession(resourceName, resourceName, timeout)
    End Sub

    ''' <summary> Gets or sets the sentinel indication that the VISA session is disposed. </summary>
    ''' <value> The is session disposed. </value>
    Public MustOverride ReadOnly Property IsSessionDisposed As Boolean


    ''' <summary>
    ''' Disposes the VISA <see cref="VI.Pith.SessionBase">Session</see> ending access to the
    ''' instrument.
    ''' </summary>
    Protected MustOverride Sub DisposeSession()

    ''' <summary> Discard all events. </summary>
    Protected MustOverride Sub DiscardAllEvents()


    ''' <summary>
    ''' Closes the VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
    ''' </summary>
    Public Sub CloseSession()
        Me.LastAction = $"{Me.OpenResourceName} clearing last error"
        Me.ClearLastError()
        Me.CommunicationTimeouts.Clear()
        If Me.IsDeviceOpen Then
            Me.LastAction = $"{Me.OpenResourceName} discarding all events"
            Me.DiscardAllEvents()
            Me.LastAction = $"{Me.OpenResourceName} disabling service request"
            Me.DisableServiceRequestEventHandler()
            Me.IsDeviceOpen = False
        End If
        Me.LastAction = $"{Me.OpenResourceName} closed"
        Me.StatusPrompt = Me.LastAction
        ' disposes the session.
        If Me.IsSessionOpen Then Me.DisposeSession()
        ' must use sync notification to prevent race condition if disposing the session.
        Me.SyncNotifyPropertyChanged(NameOf(SessionBase.IsSessionOpen))
    End Sub

    ''' <summary> The last action. </summary>
    Private _LastAction As String

    ''' <summary> Gets or sets the last action. </summary>
    ''' <value> The last action. </value>
    Public Property LastAction As String
        Get
            Return Me._LastAction
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.LastAction, value) Then
                Me._LastAction = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last node number. </summary>
    Private _LastNodeNumber As Integer?

    ''' <summary> Gets or sets the last node. </summary>
    ''' <value> The last node. </value>
    Public Property LastNodeNumber As Integer?
        Get
            Return Me._LastNodeNumber
        End Get
        Set(value As Integer?)
            If Not Nullable.Equals(Me.LastNodeNumber, value) Then
                Me._LastNodeNumber = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property


    ''' <summary>
    ''' Gets the sentinel indicating if call backs are performed in a specific synchronization
    ''' context.
    ''' </summary>
    ''' <value>
    ''' The sentinel indicating if call backs are performed in a specific synchronization context.
    ''' </value>
    Public Overridable Property SynchronizeCallbacks As Boolean

#Region " ERROR / STATUS "

    ''' <summary> Clears the last error. </summary>
    Protected Sub ClearLastError()
        Me.LastNativeError = NativeErrorBase.Success
    End Sub

    ''' <summary> Gets the last native error. </summary>
    ''' <value> The last native error. </value>
    Protected Property LastNativeError As VI.Pith.NativeErrorBase

#End Region

#Region " MESSAGE EVENTS "

    ''' <summary> The notify synchronize levels. </summary>
    Private _NotifySyncLevels As BindingList(Of KeyValuePair(Of NotifySyncLevel, String))

    ''' <summary> Gets a list of the message notification events. </summary>
    ''' <value> A list of the message notification events. </value>
    Public ReadOnly Property NotifySyncLevels As BindingList(Of KeyValuePair(Of NotifySyncLevel, String))
        Get
            If Me._NotifySyncLevels Is Nothing OrElse Not Me._NotifySyncLevels.Any Then
                Me._NotifySyncLevels = New BindingList(Of KeyValuePair(Of NotifySyncLevel, String))
                Dim extender As New isr.Core.EnumExtender(Of NotifySyncLevel)
                extender.Populate(Me._NotifySyncLevels, extender.ValueNamePairs)
            End If
            Return Me._NotifySyncLevels
        End Get
    End Property

    ''' <summary> The message notification event. </summary>
    Private _MessageNotificationEvent As KeyValuePair(Of NotifySyncLevel, String)

    ''' <summary> Gets or sets the message notification event. </summary>
    ''' <value> The message notification event. </value>
    Public Property MessageNotificationEvent As KeyValuePair(Of NotifySyncLevel, String)
        Get
            Return Me._MessageNotificationEvent
        End Get
        Set(value As KeyValuePair(Of NotifySyncLevel, String))
            If value.Key <> Me.MessageNotificationEvent.Key Then
                Me._MessageNotificationEvent = value
                Me.MessageNotificationLevel = value.Key
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The message notification level. </summary>
    Private _MessageNotificationLevel As NotifySyncLevel

    ''' <summary> Gets or sets the message notification level. </summary>
    ''' <value> The message notification level. </value>
    Public Property MessageNotificationLevel As NotifySyncLevel
        Get
            Return Me._MessageNotificationLevel
        End Get
        Set(value As NotifySyncLevel)
            If value <> Me.MessageNotificationLevel Then
                Me._MessageNotificationLevel = value
                Me.NotifyPropertyChanged()
                Me.MessageNotificationEvent = SessionBase.ToNotificationLevel(value)
            End If
        End Set
    End Property

    ''' <summary> Converts a value to a notification level. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a KeyValuePair(Of NotifySyncLevel, String) </returns>
    Public Shared Function ToNotificationLevel(ByVal value As NotifySyncLevel) As KeyValuePair(Of NotifySyncLevel, String)
        Return New KeyValuePair(Of NotifySyncLevel, String)(value, value.ToString)
    End Function

    ''' <summary> The last message received. </summary>
    Private _LastMessageReceived As String

    ''' <summary> Gets or sets the last message Received. </summary>
    ''' <remarks>
    ''' The last message sent is posted asynchronously. This may not be processed fast enough with
    ''' TSP devices to determine the state of the instrument.
    ''' </remarks>
    ''' <value> The last message Received. </value>
    Public Property LastMessageReceived As String
        Get
            Return Me._LastMessageReceived
        End Get
        Protected Set(ByVal value As String)
            Me._LastMessageReceived = value
            If Me.MessageNotificationLevel = NotifySyncLevel.Async Then
                Me.NotifyPropertyChanged()
            ElseIf Me.MessageNotificationLevel = NotifySyncLevel.Sync Then
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last message sent. </summary>
    Private _LastMessageSent As String

    ''' <summary> Gets or sets the last message Sent. </summary>
    ''' <remarks> The last message sent is posted asynchronously. </remarks>
    ''' <value> The last message Sent. </value>
    Public Property LastMessageSent As String
        Get
            Return Me._LastMessageSent
        End Get
        Protected Set(ByVal value As String)
            Me._LastMessageSent = value
            If Me.MessageNotificationLevel = NotifySyncLevel.Async Then
                Me.NotifyPropertyChanged()
            ElseIf Me.MessageNotificationLevel = NotifySyncLevel.Sync Then
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#End Region

#Region " READ / WRITE "

    ''' <summary> Gets the last input output stopwatch. </summary>
    ''' <remarks> Keeps track of the time since the last message based operation. </remarks>
    ''' <value> The last input output stopwatch. </value>
    Protected ReadOnly Property LastInputOutputStopwatch As Stopwatch

    ''' <summary> Gets the ASCII character used to end reading. </summary>
    ''' <value> The termination character. </value>
    Public MustOverride Property ReadTerminationCharacter As Byte


    ''' <summary>
    ''' Gets the termination character enabled specifying whether the read operation ends when a
    ''' termination character is received.
    ''' </summary>
    ''' <value> The termination character enabled. </value>
    Public MustOverride Property ReadTerminationCharacterEnabled As Boolean

    ''' <summary> Gets the timeout for I/O communication on this resource session. </summary>
    ''' <value> The communication timeout. </value>
    Public MustOverride Property CommunicationTimeout As TimeSpan

    ''' <summary> Gets the emulated reply. </summary>
    ''' <value> The emulated reply. </value>
    Public Property EmulatedReply As String


    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="ReadTerminationCharacter">termination character</see>. Limited by the
    ''' <see cref="InputBufferSize"/>. Will time out if end of line is not read before reading a
    ''' buffer.
    ''' </summary>
    ''' <returns> The received message. </returns>
    Public MustOverride Function ReadFiniteLine() As String


    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data until reaching the
    ''' <see cref="ReadTerminationCharacter">termination character</see>. Not limited by the
    ''' <see cref="InputBufferSize"/>.
    ''' </summary>
    ''' <returns> The received message. </returns>
    Public MustOverride Function ReadFreeLine() As String


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
    ''' data with the <see cref="ReadTerminationCharacter">termination character</see>. <para>
    ''' Per IVI documentation: Appends a newline (0xA) to the formatted I/O write buffer, flushes the
    ''' buffer, and sends an end-of-line with the buffer if required.</para>
    ''' </summary>
    ''' <remarks> David, 2020-07-23. </remarks>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function SyncWriteLine(ByVal dataToWrite As String) As String


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface.<para>
    ''' Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
    ''' formatted I/O write buffer</para>
    ''' </summary>
    ''' <remarks> David, 2020-07-23. </remarks>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function SyncWrite(ByVal dataToWrite As String) As String

    ''' <summary> Gets or set the value of the last status byte. </summary>
    ''' <value> The status byte. </value>
    Public Property StatusByte As VI.Pith.ServiceRequests

    ''' <summary> Thread unsafe read status byte. </summary>
    ''' <returns> The VI.Pith.ServiceRequests. </returns>
    Protected MustOverride Function ThreadUnsafeReadStatusByte() As VI.Pith.ServiceRequests

    ''' <summary> Gets the status byte locker. </summary>
    ''' <value> The status byte locker. </value>
    Private ReadOnly Property StatusByteLocker As New ReaderWriterLockSlim()

    ''' <summary> Reads status byte. </summary>
    ''' <returns> The status byte. </returns>
    Public Overridable Function ReadStatusByte() As VI.Pith.ServiceRequests
        Me.StatusByteLocker.EnterReadLock()
        Try
            Return Me.ThreadUnsafeReadStatusByte
        Catch
            Throw
        Finally
            Me.StatusByteLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary> Gets the size of the input buffer. </summary>
    ''' <value> The size of the input buffer. </value>
    Public MustOverride ReadOnly Property InputBufferSize() As Integer

#End Region

#Region " EMULATION "

    ''' <summary> Makes emulated reply. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeTrueFalseReply(ByVal value As Boolean)
        Me.EmulatedReply = SessionBase.ToTrueFalse(value)
    End Sub

    ''' <summary> Makes emulated reply if empty. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeTrueFalseReplyIfEmpty(ByVal value As Boolean)
        If String.IsNullOrWhiteSpace(Me.EmulatedReply) Then
            Me.MakeTrueFalseReply(value)
        End If
    End Sub

    ''' <summary> Makes emulated reply. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReply(ByVal value As Boolean)
        Me.EmulatedReply = SessionBase.ToOneZero(value)
    End Sub

    ''' <summary> Makes emulated reply if empty. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReplyIfEmpty(ByVal value As Boolean)
        If String.IsNullOrWhiteSpace(Me.EmulatedReply) Then
            Me.MakeEmulatedReply(value)
        End If
    End Sub

    ''' <summary> Makes emulated reply. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReply(ByVal value As Double)
        Me.EmulatedReply = CStr(value)
    End Sub

    ''' <summary> Makes emulated reply if empty. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReplyIfEmpty(ByVal value As Double)
        If String.IsNullOrWhiteSpace(Me.EmulatedReply) Then
            Me.MakeEmulatedReply(value)
        End If
    End Sub

    ''' <summary> Makes emulated reply. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReply(ByVal value As Integer)
        Me.EmulatedReply = CStr(value)
    End Sub

    ''' <summary> Makes emulated reply if empty. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReplyIfEmpty(ByVal value As Integer)
        If String.IsNullOrWhiteSpace(Me.EmulatedReply) Then
            Me.MakeEmulatedReply(value)
        End If
    End Sub

    ''' <summary> Makes emulated reply. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReply(ByVal value As TimeSpan)
        Me.EmulatedReply = value.ToString
    End Sub

    ''' <summary> Makes emulated reply if empty. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReplyIfEmpty(ByVal value As TimeSpan)
        If String.IsNullOrWhiteSpace(Me.EmulatedReply) Then
            Me.MakeEmulatedReply(value)
        End If
    End Sub

    ''' <summary> Makes emulated reply. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReply(ByVal value As String)
        Me.EmulatedReply = value
    End Sub

    ''' <summary> Makes emulated reply if empty. </summary>
    ''' <param name="value"> The emulated value. </param>
    Public Sub MakeEmulatedReplyIfEmpty(ByVal value As String)
        If String.IsNullOrWhiteSpace(Me.EmulatedReply) Then
            Me.MakeEmulatedReply(value)
        End If
    End Sub

#End Region

#Region " TRIGGER "


    ''' <summary>
    ''' Asserts a software or hardware trigger depending on the interface; Sends a bus trigger.
    ''' </summary>
    Public MustOverride Sub AssertTrigger()

#End Region

#Region " INTERFACE "

    ''' <summary> Supports clear interface. </summary>
    ''' <value> <c>True</c> if supports clearing the interface. </value>
    Public ReadOnly Property SupportsClearInterface() As Boolean
        Get
            Return (HardwareInterfaceType.Gpib = Me.ResourceNameInfo?.InterfaceType).GetValueOrDefault(False)
        End Get
    End Property

    ''' <summary> The interface clear refractory period. </summary>
    Private _InterfaceClearRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the Interface clear refractory period. </summary>
    ''' <value> The Interface clear refractory period. </value>
    Public Property InterfaceClearRefractoryPeriod As TimeSpan
        Get
            Return Me._InterfaceClearRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.InterfaceClearRefractoryPeriod) Then
                Me._InterfaceClearRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Clears the interface. </summary>
    Public Sub ClearInterface()
        If Me.SupportsClearInterface Then
            Me.ImplementClearHardwareInterface()
            If Me.IsSessionOpen Then isr.Core.ApplianceBase.DoEventsWait(Me.InterfaceClearRefractoryPeriod)
        End If
    End Sub

    ''' <summary> Implement clear hardware interface. </summary>
    Protected MustOverride Sub ImplementClearHardwareInterface()

    ''' <summary> Clears the device (CDC). </summary>
    ''' <remarks>
    ''' When communicating with a message-based device, particularly when you are first developing
    ''' your program, you may need to tell the device to clear its I/O buffers so that you can start
    ''' again. In addition, if a device has more information than you need, you may want to read
    ''' until you have everything you need and then tell the device to throw the rest away. The
    ''' <c>viClear()</c> operation performs these tasks. More specifically, the clear operation lets
    ''' a controller send the device clear command to the device it is associated with, as specified
    ''' by the interface specification and the type of device. The action that the device takes
    ''' depends on the interface to which it is connected. <para>
    ''' For a GPIB device, the controller sends the IEEE 488.1 SDC (04h) command.</para><para>
    ''' For a VXI or MXI device, the controller sends the Word Serial Clear (FFFFh) command.</para>
    ''' <para>
    ''' For the ASRL INSTR or TCPIP SOCKET resource, the controller sends the string "*CLS\n". The
    ''' I/O protocol must be set to VI_PROT_4882_STRS for this service to be available to these
    ''' resources.</para>
    ''' For more details on these clear commands, refer to your device documentation, the IEEE 488.1
    ''' standard, or the VXI bus specification. <para>
    ''' Source: NI-Visa HTML help.</para>
    ''' </remarks>
    Protected MustOverride Sub Clear()

    ''' <summary> Clears the device (SDC). </summary>
    Protected MustOverride Sub ClearDevice()


    ''' <summary>
    ''' Clears the active state. Issues selective device clear. Waits for the
    ''' <see cref="DeviceClearRefractoryPeriod"/> before releasing control.
    ''' </summary>
    Public Overridable Sub ClearActiveState()
        Me.ClearActiveState(Me.DeviceClearRefractoryPeriod)
    End Sub


    ''' <summary>
    ''' Clears the active state. Issues selective device clear. Waits for the
    ''' <paramref name="refractoryPeriod"/> before releasing control.
    ''' </summary>
    ''' <param name="refractoryPeriod"> The refractory period. </param>
    Public Overridable Sub ClearActiveState(ByVal refractoryPeriod As TimeSpan)
        Me.StatusPrompt = $"{Me.OpenResourceName} SDC"
        ' A delay is required before issuing a device clear on some TSP devices, such as the 2600.
        ' First a 1ms delay was added. Without the delay, the instrument had error -286 TSP Runtime Error and User Abort error 
        ' if the clear command was issued after turning off the status _G.status.request_enable=0
        ' Thereafter, the instrument has a resource not found error when trying to connect after failing to connect 
        ' because instruments were off. Stopping here in debug mode seems to have alleviated this issue.  So,
        ' the delay was increased to 10 ms.
        isr.Core.ApplianceBase.Delay(Me.DeviceClearRefractoryPeriod)
        Me.ClearDevice()
        If Me.IsSessionOpen Then isr.Core.ApplianceBase.DoEventsWait(refractoryPeriod)
        Me.StatusPrompt = $"{Me.OpenResourceName} SDC; done"
        Me.QueryOperationCompleted()
    End Sub

    ''' <summary> The device clear delay period. </summary>
    Private _DeviceClearDelayPeriod As TimeSpan

    ''' <summary> Gets or sets the device clear Delay period. </summary>
    ''' <value> The device clear Delay period. </value>
    Public Property DeviceClearDelayPeriod As TimeSpan
        Get
            Return Me._DeviceClearDelayPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.DeviceClearDelayPeriod) Then
                Me._DeviceClearDelayPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The device clear refractory period. </summary>
    Private _DeviceClearRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the device clear refractory period. </summary>
    ''' <value> The device clear refractory period. </value>
    Public Property DeviceClearRefractoryPeriod As TimeSpan
        Get
            Return Me._DeviceClearRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.DeviceClearRefractoryPeriod) Then
                Me._DeviceClearRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " OVERRIDE NOT REQUIRED "

#Region " TERMINATION "

    ''' <summary> The termination sequence. </summary>
    Private _TerminationSequence As String

    ''' <summary> Gets or sets the termination sequence. </summary>
    ''' <value> The termination sequence. </value>
    Public Property TerminationSequence As String
        Get
            Return Me._TerminationSequence
        End Get
        Set(value As String)
            If value <> Me.TerminationSequence Then
                Me._TerminationSequence = value
                Me.NotifyPropertyChanged()
                Me.Terminations = value.ReplaceCommonEscapeSequences.ToCharArray
            End If
        End Set
    End Property

    ''' <summary> Determine if we are equal. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> True if equal, false if not. </returns>
    Private Shared Function AreEqual(ByVal left As IEnumerable(Of Char), ByVal right As IEnumerable(Of Char)) As Boolean
        Dim result As Boolean = True
        If left Is Nothing Then
            result = right Is Nothing
        ElseIf right Is Nothing Then
            result = False
        ElseIf left.Any Then
            If right.Any Then
                If left.Count = right.Count Then
                    For i As Integer = 0 To left.Count - 1
                        If left(i) <> right(i) Then
                            result = False
                            Exit For
                        End If
                    Next
                Else
                    result = False
                End If
            Else
                result = False
            End If
        End If
        Return result
    End Function

    ''' <summary> The termination characters. </summary>
    Private _TerminationCharacters As Char()

    ''' <summary> Gets or sets the termination characters. </summary>
    ''' <value> The termination characters. </value>
    Private Property Terminations As IEnumerable(Of Char)
        Get
            Return Me._TerminationCharacters
        End Get
        Set(value As IEnumerable(Of Char))
            If Not AreEqual(value, Me.Terminations) Then
                Me._TerminationCharacters = New Char(value.Count - 1) {}
                Array.Copy(value.ToArray, Me._TerminationCharacters, value.Count)
                Me.NotifyPropertyChanged(NameOf(SessionBase.Termination))
                Me.TerminationSequence = String.Concat(Me.Terminations).InsertCommonEscapeSequences
            End If
        End Set
    End Property

    ''' <summary> Gets the termination characters. </summary>
    ''' <value> The termination. </value>
    Public ReadOnly Property Termination() As IEnumerable(Of Char)
        Get
            Return Me.Terminations
        End Get
    End Property

    ''' <summary> Use default termination. </summary>
    Private Sub UseDefaultTerminationThis()
        Me.NewTerminationThis(Environment.NewLine.ToCharArray()(1))
    End Sub

    ''' <summary> Use default termination. </summary>
    Public Sub UseDefaultTermination()
        Me.UseDefaultTerminationThis()
    End Sub

    ''' <summary> Creates a new termination. </summary>
    ''' <param name="value"> The emulated value. </param>
    Private Sub NewTerminationThis(ByVal value As Char)
        Me.Terminations = New Char() {value}
    End Sub

    ''' <summary> Creates a new termination. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="values"> The values. </param>
    Private Sub NewTerminationThis(ByVal values() As Char)
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        If values.Length = 0 Then Throw New InvalidOperationException("Failed creating new termination; Source array must have at least one character")
        Dim terms As Char() = New Char(values.Length - 1) {}
        Array.Copy(values, terms, values.Length)
        Me.Terminations = terms
    End Sub

    ''' <summary> Creates a new termination. </summary>
    ''' <param name="values"> The values. </param>
    Public Sub NewTermination(ByVal values() As Char)
        Me.NewTerminationThis(values)
    End Sub

    ''' <summary> Use new line termination. </summary>
    Public Sub UseNewLineTermination()
        Me.NewTerminationThis(Environment.NewLine.ToCharArray)
    End Sub

    ''' <summary> Use line feed termination. </summary>
    Public Sub UseLinefeedTermination()
        Me.NewTerminationThis(Environment.NewLine.ToCharArray()(1))
    End Sub

    ''' <summary> Use Carriage Return termination. </summary>
    Public Sub UseCarriageReturnTermination()
        Me.NewTerminationThis(Environment.NewLine.ToCharArray()(1))
    End Sub

#End Region

#Region " EXECUTE "

    ''' <summary> Executes the command. </summary>
    ''' <param name="command"> The command. </param>
    Public Sub Execute(ByVal command As String)
        Me.WriteLine(command)
    End Sub

    ''' <summary> Executes the command using the specified timeout. </summary>
    ''' <param name="command"> The command. </param>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub Execute(ByVal command As String, ByVal timeout As TimeSpan)
        Try
            Me.StoreCommunicationTimeout(timeout)
            Me.WriteLine(command)
        Catch
            Throw
        Finally
            Me.RestoreCommunicationTimeout()
        End Try
    End Sub


    ''' <summary>
    ''' Executes the <see cref="Action">action</see>/&gt; using the specified timeout.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="action">  The action. </param>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub Execute(ByVal action As Action, ByVal timeout As TimeSpan)
        If action Is Nothing Then Throw New ArgumentNullException(NameOf(action))
        Try
            Me.StoreCommunicationTimeout(timeout)
            action()
        Catch
            Throw
        Finally
            Me.RestoreCommunicationTimeout()
        End Try
    End Sub

    ''' <summary> Executes the command using the specified timeout. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="action">  The action. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A Boolean? </returns>
    Public Function Execute(action As Func(Of Boolean?), ByVal timeout As TimeSpan) As Boolean?
        If action Is Nothing Then Throw New ArgumentNullException(NameOf(action))
        Try
            Me.StoreCommunicationTimeout(timeout)
            Return action()
        Catch
            Throw
        Finally
            Me.RestoreCommunicationTimeout()
        End Try
    End Function

#End Region

#Region " READ / WRITE LINE"


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Replaces Escape
    ''' characters and Terminates the data with the <see cref="ReadTerminationCharacter">termination
    ''' character</see>.
    ''' </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns> A String. </returns>
    Public Function WriteEscapedLine(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.WriteEscapedLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
    End Function


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Replaces Escape
    ''' characters and Terminates the data with the <see cref="ReadTerminationCharacter">termination
    ''' character</see>.
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Public Function WriteEscapedLine(ByVal dataToWrite As String) As String
        Return Me.WriteLine(dataToWrite.ReplaceCommonEscapeSequences)
    End Function


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
    ''' data with the <see cref="ReadTerminationCharacter">termination character</see>. <para>
    ''' Per IVI documentation: Appends a newline (0xA) to the formatted I/O write buffer, flushes the
    ''' buffer, and sends an end-of-line with the buffer if required.</para>
    ''' </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns> A String. </returns>
    Public Function WriteLine(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.WriteLine(String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
    End Function


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
    ''' data with the <see cref="ReadTerminationCharacter">termination character</see>. <para>
    ''' Per IVI documentation: Appends a newline (0xA) to the formatted I/O write buffer, flushes the
    ''' buffer, and sends an end-of-line with the buffer if required.</para>
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Public Function WriteLine(ByVal dataToWrite As String) As String
        Return Me.SyncWriteLine(dataToWrite)
        ' #If LogIO Then
        '        My.Application.Log.WriteEntry($"w: {dataToWrite}")
        ' #End If
#If LogIO Then
        My.Application.Log.WriteEntry($"w: {dataToWrite}")
#End If
    End Function


    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface.<para>
    ''' Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
    ''' formatted I/O write buffer</para>
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
    ''' <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
    ''' </returns>
    Public Function Write(ByVal dataToWrite As String) As String
        Return Me.SyncWrite(dataToWrite)
        ' #If LogIO Then
        '        My.Application.Log.WriteEntry($"w: {dataToWrite}")
        ' #End If
#If LogIO Then
        My.Application.Log.WriteEntry($"w: {dataToWrite}")
#End If
    End Function


    ''' <summary>
    ''' Reads to and including the end <see cref="Termination">termination character</see>.
    ''' </summary>
    ''' <returns> The string. </returns>
    Public Function ReadLine() As String
        ' #If LogIO Then
        '        Dim value As String = Me.ReadString()
        '        My.Application.Log.WriteEntry($"r: {value.TrimEnd}")
        ' #ElseIf False Then
        '        Return Me.ReadFiniteLine()
        ' #Else
#If LogIO Then
        Dim value As String = Me.ReadString()
        My.Application.Log.WriteEntry($"r: {value.TrimEnd}")
#ElseIf False Then
        Return Me.ReadFiniteLine()
#Else
        ' this is an attempt to address seeing that a read is somehow delayed after a few writes. 
        ' not sure how the instrument is not issuing query unterminated errors in this case.
        Return Me.ReadFreeLine()
#End If
    End Function

#End Region

#Region " QUERY "


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal dataToWrite As String) As String
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.WriteLine(dataToWrite)
            Return Me.ReadLine()
        End If
        Return String.Empty
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="readDelay"> The timespan to delay reading after writing. </param>
    ''' <param name="format">    The format of the data to write. </param>
    ''' <param name="args">      The format arguments. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal readDelay As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object) As String
        If Not String.IsNullOrWhiteSpace(format) Then
            Me.WriteLine(format, args)
            isr.Core.ApplianceBase.DoEventsWait(readDelay)
            Return Me.ReadLine()
        End If
        Return String.Empty
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns>
    ''' The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
    ''' </returns>
    Public Function Query(ByVal format As String, ByVal ParamArray args() As Object) As String
        If Not String.IsNullOrWhiteSpace(format) Then
            Me.WriteLine(format, args)
            Return Me.ReadLine()
        End If
        Return String.Empty
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="readDelay">   The timespan to delay reading after writing. </param>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimTermination(ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Query(readDelay, dataToWrite).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimTermination(ByVal dataToWrite As String) As String
        Return Me.Query(dataToWrite).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Queries the device and returns a string save the termination character. Expects terminated
    ''' query command.
    ''' </summary>
    ''' <param name="readDelay">   The timespan to delay reading after writing. </param>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimEnd(ByVal readDelay As TimeSpan, ByVal dataToWrite As String) As String
        Return Me.Query(readDelay, dataToWrite).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimEnd(ByVal dataToWrite As String) As String
        Return Me.Query(dataToWrite).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Queries the device and returns a string save the termination character. Expects terminated
    ''' query command.
    ''' </summary>
    ''' <param name="readDelay"> The timespan to delay reading after writing. </param>
    ''' <param name="format">    The format of the data to write. </param>
    ''' <param name="args">      The format arguments. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimEnd(ByVal readDelay As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Query(readDelay, format, args).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Queries the device and returns a string save the termination character. Expects terminated
    ''' query command.
    ''' </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimEnd(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Query(format, args).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="readDelay"> The timespan to delay reading after writing. </param>
    ''' <param name="format">    The format of the data to write. </param>
    ''' <param name="args">      The format arguments. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimTermination(ByVal readDelay As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Query(readDelay, format, args).TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
    ''' </summary>
    ''' <param name="format"> The format of the data to write. </param>
    ''' <param name="args">   The format arguments. </param>
    ''' <returns>
    ''' The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
    ''' <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Function QueryTrimTermination(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Query(format, args).TrimEnd(Me.Termination.ToArray)
    End Function

#End Region

#Region " READ "


    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="Termination">termination characters</see>.
    ''' </summary>
    ''' <returns>
    ''' The received message without the <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Overloads Function ReadLineTrimTermination() As String
        Return Me.ReadLine().TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="Termination">termination characters</see>.
    ''' </summary>
    ''' <returns>
    ''' The received message without the <see cref="Termination">termination characters</see>.
    ''' </returns>
    Public Overloads Function ReadLineTrimEnd() As String
        Return Me.ReadLine().TrimEnd(Me.Termination.ToArray)
    End Function

    ''' <summary> Reads free line trim end. </summary>
    ''' <returns> The free line trim end. </returns>
    Public Overloads Function ReadFreeLineTrimEnd() As String
        Return Me.ReadFreeLine().TrimEnd(Me.Termination.ToArray)
    End Function


    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="Termination">termination character</see>.
    ''' </summary>
    ''' <returns>
    ''' The received message without the carriage return (13) and line feed (10) characters.
    ''' </returns>
    Public Overloads Function ReadLineTrimNewLine() As String
        Return Me.ReadLine().TrimEnd(New Char() {Convert.ToChar(13), Convert.ToChar(10)})
    End Function


    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="Termination">termination characters</see>.
    ''' </summary>
    ''' <returns> The received message without the line feed (10) characters. </returns>
    Public Overloads Function ReadLineTrimLinefeed() As String
        Return Me.ReadLine().TrimEnd(Convert.ToChar(10))
    End Function

#End Region

#Region " READ LINES "


    ''' <summary>
    ''' Reads multiple lines from the instrument until data is no longer available.
    ''' </summary>
    ''' <param name="pollDelay"> Time to wait between service requests. </param>
    ''' <param name="timeout">   Specifies the time to wait for message available. </param>
    ''' <param name="trimEnd">   Specifies a directive to trim the end character from each line. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan, ByVal trimEnd As Boolean) As String
        Return Me.ReadLines(pollDelay, timeout, False, trimEnd)
    End Function


    ''' <summary>
    ''' Reads multiple lines from the instrument until data is no longer available.
    ''' </summary>
    ''' <param name="pollDelay">  Time to wait between service requests. </param>
    ''' <param name="timeout">    Specifies the time to wait for message available. </param>
    ''' <param name="trimSpaces"> Specifies a directive to trim leading and trailing spaces from each
    '''                           line. This also trims the end character. </param>
    ''' <param name="trimEnd">    Specifies a directive to trim the end character from each line. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan,
                              ByVal trimSpaces As Boolean, ByVal trimEnd As Boolean) As String

        Dim listBuilder As New System.Text.StringBuilder
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim timedOut As Boolean = False
        Do While Not timedOut

            ' allow message available time to materialize
            Me.ReadStatusRegister()

            Do Until Me.HasMeasurementEvent OrElse timedOut
                timedOut = sw.Elapsed > timeout
                isr.Core.ApplianceBase.DoEventsWait(pollDelay)
                Me.ReadStatusRegister()
            Loop

            If Me.HasMeasurementEvent Then
                timedOut = False
                sw.Restart()
                If trimSpaces Then
                    listBuilder.AppendLine(Me.ReadLine().Trim())
                ElseIf trimEnd Then
                    listBuilder.AppendLine(Me.ReadLineTrimEnd())
                Else
                    listBuilder.AppendLine(Me.ReadLine())
                End If
            End If

        Loop
        Return listBuilder.ToString

    End Function

    ''' <summary> Reads multiple lines from the instrument until timeout. </summary>
    ''' <param name="timeout">        Specifies the time in millisecond to wait for message available. </param>
    ''' <param name="trimSpaces">     Specifies a directive to trim leading and trailing spaces from
    '''                               each line. </param>
    ''' <param name="expectedLength"> Specifies the amount of data expected without trimming. </param>
    ''' <returns> Data. </returns>
    Public Function ReadLines(ByVal timeout As TimeSpan, ByVal trimSpaces As Boolean, ByVal expectedLength As Integer) As String

        Try

            Dim listBuilder As New System.Text.StringBuilder
            Me.StoreCommunicationTimeout(timeout)

            Dim currentLength As Integer = 0
            Do While currentLength < expectedLength
                Dim buffer As String = Me.ReadLine()
                expectedLength += buffer.Length
                If trimSpaces Then
                    listBuilder.AppendLine(buffer.Trim)
                Else
                    listBuilder.AppendLine(buffer)
                End If
            Loop

            Return listBuilder.ToString

        Catch
            Throw
        Finally
            Me.RestoreCommunicationTimeout()
        End Try

    End Function

#End Region

#Region " DISCARD UNREAD DATA "


    ''' <summary>
    ''' Reads and discards all data from the VISA session until the END indicator is read.
    ''' </summary>
    ''' <remarks> Uses 10ms poll delay, 100ms timeout. </remarks>
    Public Sub DiscardUnreadData()
        Me.DiscardUnreadData(TimeSpan.FromMilliseconds(10), TimeSpan.FromMilliseconds(100))
    End Sub

    ''' <summary> Information describing the discarded. </summary>
    Private _DiscardedData As System.Text.StringBuilder

    ''' <summary> Gets the information describing the discarded. </summary>
    ''' <value> Information describing the discarded. </value>
    Public ReadOnly Property DiscardedData As String
        Get
            Return If(Me._DiscardedData Is Nothing, String.Empty, Me._DiscardedData.ToString)
        End Get
    End Property


    ''' <summary>
    ''' Reads and discards all data from the VISA session until the END indicator is read and no data
    ''' are added to the output buffer.
    ''' </summary>
    ''' <param name="pollDelay"> Time to wait between service requests. </param>
    ''' <param name="timeout">   Specifies the time to wait for message available. </param>
    Public Sub DiscardUnreadData(ByVal pollDelay As TimeSpan, ByVal timeout As TimeSpan)

        ' to make sure this does not last forever.
        Dim endTime As Date = DateTime.UtcNow.Add(TimeSpan.FromSeconds(2))
        Me._DiscardedData = New System.Text.StringBuilder
        Do
            ' allow message available time to materialize
            isr.Core.ApplianceBase.DoEventsWaitUntil(pollDelay, timeout, Function() Me.QueryMessageAvailableStatus())

            If Me.MessageAvailable Then Me._DiscardedData.AppendLine(Me.ReadLine())

        Loop While Me.MessageAvailable AndAlso DateTime.UtcNow < endTime

    End Sub

#End Region

#Region " TIMEOUT MANAGEMENT "

    ''' <summary> Gets or sets the reference to the stack of communication timeouts. </summary>
    ''' <value> The timeouts. </value>
    Protected ReadOnly Property CommunicationTimeouts As System.Collections.Generic.Stack(Of TimeSpan)

    ''' <summary> The timeout cnandidate. </summary>
    Private _TimeoutCnandidate As TimeSpan

    ''' <summary> Gets or sets the milliseconds timeout candidate. </summary>
    ''' <value> The milliseconds timeout candidate. </value>
    Public Property TimeoutCandidate As TimeSpan
        Get
            Return Me._TimeoutCnandidate
        End Get
        Set(value As TimeSpan)
            If Me.TimeoutCandidate <> value Then
                Me._TimeoutCnandidate = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Restores the last timeout from the stack. </summary>
    Public Sub RestoreCommunicationTimeout()
        If Me.CommunicationTimeouts.Any Then
            Me.CommunicationTimeout = Me.CommunicationTimeouts.Pop
        End If
        Me.TimeoutCandidate = Me.CommunicationTimeout
    End Sub

    ''' <summary> Saves the current timeout and sets a new setting timeout. </summary>
    ''' <param name="timeout"> Specifies the new timeout. </param>
    Public Sub StoreCommunicationTimeout(ByVal timeout As TimeSpan)
        Me.CommunicationTimeouts.Push(Me.CommunicationTimeout)
        Me.CommunicationTimeout = timeout
        Me.TimeoutCandidate = timeout
    End Sub

#End Region

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("Session Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    Public Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DefaultOpenSessionTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResourceTitle))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClosedCaption))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.DefaultOpenSessionTimeout)
                Me.DefaultOpenTimeout = My.Settings.DefaultOpenSessionTimeout
            Case NameOf(My.MySettings.SessionClosureRefractoryPeriod)
            Case NameOf(My.MySettings.ClosedCaption)
                Me.ResourceClosedCaption = My.Settings.ClosedCaption
            Case NameOf(My.MySettings.ResourceTitle)
                Me.CandidateResourceTitle = My.Settings.ResourceTitle
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.DeviceClearRefractoryPeriod = My.Settings.DeviceClearRefractoryPeriod
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.StatusReadTurnaroundTime = My.Settings.StatusReadTurnaroundTime
        End Select
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleSettingsPropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.Settings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            isr.Core.WindowsForms.ShowDialogOkay($"Exception {activity}--ignored;. {ex.ToFullBlownString}",
                                                 "Exception Occurred", isr.Core.MyMessageBoxIcon.Error)
        End Try

    End Sub

#End Region

#Region " STATUS PROMPT "

    ''' <summary> The status prompt. </summary>
    Private _StatusPrompt As String

    ''' <summary> Gets or sets a prompt describing the Status. </summary>
    ''' <value> A prompt describing the Status. </value>
    Public Property StatusPrompt As String
        Get
            Return Me._StatusPrompt
        End Get
        Set(value As String)
            Me._StatusPrompt = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

#End Region

End Class

''' <summary> Values that represent notify Synchronization levels. </summary>
Public Enum NotifySyncLevel

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("No notification")>
    None = 0

    ''' <summary> An enum constant representing the sync] option. </summary>
    <Description("Synchronize")>
    [Sync]

    ''' <summary> An enum constant representing the async] option. </summary>
    <Description("A-Synchronize")>
    [Async]
End Enum
