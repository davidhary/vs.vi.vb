<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DigitalOutputView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DigitalOutputView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._DigitalOutputLine1View = New isr.VI.Facade.DigitalOutputLineView()
        Me._DigitalOutputLine2View = New isr.VI.Facade.DigitalOutputLineView()
        Me._DigitalOutputLine3View = New isr.VI.Facade.DigitalOutputLineView()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StrobeButton = New System.Windows.Forms.ToolStripButton()
        Me._Layout.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 2
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._Layout.Controls.Add(Me._InfoTextBox, 0, 4)
        Me._Layout.Controls.Add(Me._DigitalOutputLine1View, 0, 1)
        Me._Layout.Controls.Add(Me._DigitalOutputLine2View, 0, 2)
        Me._Layout.Controls.Add(Me._DigitalOutputLine3View, 0, 3)
        Me._Layout.Controls.Add(Me._SubsystemToolStrip, 0, 0)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 5
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._Layout.Size = New System.Drawing.Size(423, 352)
        Me._Layout.TabIndex = 0
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InfoTextBox.Location = New System.Drawing.Point(3, 134)
        Me._InfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ReadOnly = True
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InfoTextBox.Size = New System.Drawing.Size(415, 214)
        Me._InfoTextBox.TabIndex = 14
        Me._InfoTextBox.Text = "<info>"
        '
        '_DigitalOutputLine1View
        '
        Me._DigitalOutputLine1View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DigitalOutputLine1View.Dock = System.Windows.Forms.DockStyle.Top
        Me._DigitalOutputLine1View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DigitalOutputLine1View.LineLevel = 0
        Me._DigitalOutputLine1View.LineName = "Line"
        Me._DigitalOutputLine1View.LineNumber = 0
        Me._DigitalOutputLine1View.Location = New System.Drawing.Point(3, 28)
        Me._DigitalOutputLine1View.Name = "_DigitalOutputLine1View"
        Me._DigitalOutputLine1View.Padding = New System.Windows.Forms.Padding(1)
        Me._DigitalOutputLine1View.PulseWidth = System.TimeSpan.Parse("00:00:00")
        Me._DigitalOutputLine1View.Size = New System.Drawing.Size(415, 30)
        Me._DigitalOutputLine1View.TabIndex = 0
        '
        '_DigitalOutputLine2View
        '
        Me._DigitalOutputLine2View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DigitalOutputLine2View.Dock = System.Windows.Forms.DockStyle.Top
        Me._DigitalOutputLine2View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DigitalOutputLine2View.LineLevel = 0
        Me._DigitalOutputLine2View.LineName = "Line"
        Me._DigitalOutputLine2View.LineNumber = 0
        Me._DigitalOutputLine2View.Location = New System.Drawing.Point(3, 64)
        Me._DigitalOutputLine2View.Name = "_DigitalOutputLine2View"
        Me._DigitalOutputLine2View.Padding = New System.Windows.Forms.Padding(1)
        Me._DigitalOutputLine2View.PulseWidth = System.TimeSpan.Parse("00:00:00")
        Me._DigitalOutputLine2View.Size = New System.Drawing.Size(415, 28)
        Me._DigitalOutputLine2View.TabIndex = 1
        '
        '_DigitalOutputLine3View
        '
        Me._DigitalOutputLine3View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DigitalOutputLine3View.Dock = System.Windows.Forms.DockStyle.Top
        Me._DigitalOutputLine3View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DigitalOutputLine3View.LineLevel = 0
        Me._DigitalOutputLine3View.LineName = "Line"
        Me._DigitalOutputLine3View.LineNumber = 0
        Me._DigitalOutputLine3View.Location = New System.Drawing.Point(3, 98)
        Me._DigitalOutputLine3View.Name = "_DigitalOutputLine3View"
        Me._DigitalOutputLine3View.Padding = New System.Windows.Forms.Padding(1)
        Me._DigitalOutputLine3View.PulseWidth = System.TimeSpan.Parse("00:00:00")
        Me._DigitalOutputLine3View.Size = New System.Drawing.Size(415, 29)
        Me._DigitalOutputLine3View.TabIndex = 2
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.AutoSize = False
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._StrobeButton})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(421, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 9
        Me._SubsystemToolStrip.Text = "Digital Out"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(82, 22)
        Me._SubsystemSplitButton.Text = "Digital Out"
        Me._SubsystemSplitButton.ToolTipText = "Subsystem actions split button"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        '
        '_StrobeButton
        '
        Me._StrobeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StrobeButton.Image = CType(resources.GetObject("_StrobeButton.Image"), System.Drawing.Image)
        Me._StrobeButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._StrobeButton.Name = "_StrobeButton"
        Me._StrobeButton.Size = New System.Drawing.Size(45, 22)
        Me._StrobeButton.Text = "Strobe"
        Me._StrobeButton.ToolTipText = "Outputs a binning strop signal"
        '
        'DigitalOutputView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "DigitalOutputView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(425, 354)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _DigitalOutputLine1View As Facade.DigitalOutputLineView
    Private WithEvents _DigitalOutputLine2View As Facade.DigitalOutputLineView
    Private WithEvents _DigitalOutputLine3View As Facade.DigitalOutputLineView
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _StrobeButton As Windows.Forms.ToolStripButton
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InfoTextBox As Windows.Forms.TextBox
End Class
