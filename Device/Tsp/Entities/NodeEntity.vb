''' <summary> Encapsulate the node information. </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 03/02/2009, 3.0.3348.x. </para></remarks>
Public Class NodeEntity
    Inherits NodeEntityBase

    ''' <summary> Constructs the class. </summary>
    ''' <param name="number">               Specifies the node number. </param>
    ''' <param name="controllerNodeNumber"> The controller node number. </param>
    Public Sub New(ByVal number As Integer, ByVal controllerNodeNumber As Integer)
        MyBase.New(number, controllerNodeNumber)
    End Sub

End Class
