''' <summary> Defines a SCPI Sense Voltage Subsystem for a Keithley 2700 instrument. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2014-03-01, 3.0.5173. </para>
''' </remarks>
Public Class SenseVoltageSubsystem
    Inherits VI.SenseFunctionSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:VOLT:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:VOLT:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:VOLT:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:VOLT:NPLC?"

#End Region

#Region " PROTECTION "

    ''' <summary> Gets or sets the Protection enabled command Format. </summary>
    ''' <value> The Protection enabled query command. </value>
    Protected Overrides Property ProtectionEnabledCommandFormat As String = ":SENS:VOLT:PROT:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Protection enabled query command. </summary>
    ''' <value> The Protection enabled query command. </value>
    Protected Overrides Property ProtectionEnabledQueryCommand As String = ":SENS:VOLT:PROT:STAT?"

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overrides Property ProtectionLevelCommandFormat As String = ":SENS:VOLT:PROT {0}"

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overrides Property ProtectionLevelQueryCommand As String = ":SENS:VOLT:PROT?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:VOLT:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SENS:VOLT:RANG?"

#End Region

#End Region

End Class
