''' <summary>
''' Defines the contract that must be implemented by a SCPI Sense Voltage Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific ReSenses, Inc.<para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SenseResistanceSubsystemBase
    Inherits VI.SenseFunctionSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SenseResistanceSubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem, readingAmounts)
        Me.ResistanceRangeCurrents = New ResistanceRangeCurrentCollection
    End Sub

#End Region

#Region " RESISTANCE RANGE "

    ''' <summary> The current. </summary>
    Private _Current As Decimal

    ''' <summary> Gets or sets the current for the specific range. </summary>
    ''' <value> The current. </value>
    Public Property Current As Decimal
        Get
            Return Me._Current
        End Get
        Set(value As Decimal)
            If value <> Me.Current Then
                Me._Current = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the resistance range currents for either two- or four-wire resistance
    ''' measurements.
    ''' </summary>
    ''' <value> The resistance range currents. </value>
    Public ReadOnly Property ResistanceRangeCurrents As ResistanceRangeCurrentCollection

    ''' <summary> The resistance range current. </summary>
    Private _ResistanceRangeCurrent As ResistanceRangeCurrent

    ''' <summary> Gets or sets the resistance range current. </summary>
    ''' <value> The resistance range current. </value>
    Public Property ResistanceRangeCurrent As ResistanceRangeCurrent
        Get
            Return Me._ResistanceRangeCurrent
        End Get
        Set(value As ResistanceRangeCurrent)
            If value IsNot Nothing AndAlso value.ResistanceRange <> Me.ResistanceRangeCurrent.ResistanceRange Then
                Me._ResistanceRangeCurrent = value
                Me.NotifyPropertyChanged()
                Me.Current = value.RangeCurrent
                Me.ApplyRange(value.ResistanceRange)
            End If
        End Set
    End Property

#End Region

End Class

