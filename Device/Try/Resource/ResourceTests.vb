'---------------------------------------------------------------------------------------------------
' file:		.\Resource\ResourceTests.vb
'
' summary:	Resource tests class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

Imports Microsoft.Win32

''' <summary> A resource tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-11 </para>
''' </remarks>
<TestClass()>
Public Class ResourceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

#End Region

#Region " NULLABLE TESTS "

    ''' <summary> A test for Nullable equality. </summary>
    <TestMethod()>
    Public Sub NullableBooleanTest()
        Dim bool As Boolean?
        Dim boolValue As Boolean = True
        Assert.AreEqual(True, bool Is Nothing, "Initialized to nothing")
        Assert.AreEqual(True, bool.Equals(New Boolean?), "Nullable not set equals to a new Boolean")
        Assert.AreEqual(False, bool.Equals(boolValue), "Nullable not set is not equal to {0}", boolValue)
        Assert.AreEqual(Nothing, bool = boolValue, "Nullable '=' operator yields Nothing")
        boolValue = False
        Assert.AreEqual(False, bool.Equals(boolValue), "Nullable not set is not equal to True")
        bool = boolValue
        Assert.AreEqual(True, bool.HasValue, "Has value -- set to {0}", boolValue)

        bool = New Nullable(Of Boolean)()
        Assert.AreEqual(True, bool Is Nothing, "Nullable set to new Boolean is still nothing")
        Assert.AreEqual(True, bool.Equals(New Boolean?), "Nullable set to new Boolean equals to a new Boolean")
        Assert.AreEqual(False, bool.Equals(boolValue), "Nullable set to new Boolean not equal to {0}", boolValue)
        boolValue = False
        Assert.AreEqual(False, bool.Equals(boolValue), "Nullable set to new Boolean not equal to True")
        bool = boolValue
        Assert.AreEqual(True, bool.HasValue, "Has value -- set to {0}", boolValue)
    End Sub

    ''' <summary> A test for Nullable Integer equality. </summary>
    <TestMethod()>
    Public Sub NullableIntegerTest()
        Dim integerValue As Integer = 1
        Dim nullInt As Integer?
        Assert.AreEqual(True, nullInt Is Nothing, "Initialized to nothing")
        nullInt = New Nullable(Of Integer)()
        Assert.AreEqual(True, nullInt Is Nothing, "Nullable set to new Boolean is nothing")
        Assert.AreEqual(True, nullInt.Equals(New Integer?), "Nullable set to new Integer equals to a new Integer")
        Assert.AreEqual(False, nullInt.Equals(integerValue), "Nullable set to new Integer not equal to {0}", integerValue)
        Assert.AreEqual(False, nullInt.Equals(integerValue), "Nullable set to new Integer not equal to {0}", integerValue)
        nullInt = integerValue
        Assert.AreEqual(True, nullInt.HasValue, "Set to {0}", integerValue)
        Assert.AreEqual(integerValue, nullInt.Value, "Set to  {0}", integerValue)
    End Sub

    ''' <summary> (Unit Test Method) tests parse boolean. </summary>
    <TestMethod()>
    Public Sub ParseBooleanTest()
        Dim reading As String = "0"
        Dim expectedResult As Boolean = False
        Dim actualResult As Boolean = True
        Dim successParsing As Boolean = VI.Pith.SessionBase.TryParse(reading, actualResult)
        Assert.AreEqual(expectedResult, actualResult, "Value set to {0}", actualResult)
        Assert.AreEqual(True, successParsing, "Success set to {0}", actualResult)
        reading = "1"
        expectedResult = True
        successParsing = VI.Pith.SessionBase.TryParse(reading, actualResult)
        Assert.AreEqual(expectedResult, actualResult, "Value set to {0}", actualResult)
        Assert.AreEqual(True, successParsing, "Success set to {0}", actualResult)
    End Sub

#End Region

#Region " VERSION TESTS "

    ''' <summary> (Unit Test Method) tests visa assembly version. </summary>
    <TestMethod()>
    Public Sub VisaAssemblyVersionTest()

        Dim result As (Success As Boolean, Details As String) = isr.VI.Foundation.VisaVersionValidator.ValidateFunctionalVisaVersions()
        Assert.IsTrue(result.Success, result.Details)

        result = isr.VI.Foundation.VisaVersionValidator.ValidateVisaAssemblyVersions()
        Assert.IsTrue(result.Success, result.Details)

        result = isr.VI.Foundation.VisaVersionValidator.ValidateVisaVendorVersion()
        Assert.IsTrue(result.Success, result.Details)

    End Sub

    ''' <summary> (Unit Test Method) tests visa functional versions. </summary>
    <TestMethod()>
    Public Sub VisaFunctionalVersionsTest()
        Dim actualVersion As Version = isr.VI.Foundation.VisaVersionValidator.ImplementationVersion
        Dim expectedVersion As New Version(isr.VI.Foundation.MySettings.Default.ImplementationVersion)
        Assert.AreEqual(expectedVersion.ToString, actualVersion.ToString, $"{NameOf(isr.VI.Foundation.VisaVersionValidator.ImplementationVersion).SplitWords} should match")
        actualVersion = isr.VI.Foundation.VisaVersionValidator.SpecificationVersion
        expectedVersion = New Version(isr.VI.Foundation.MySettings.Default.SpecificationVersion)
        Assert.AreEqual(expectedVersion.ToString, actualVersion.ToString, $"{NameOf(isr.VI.Foundation.VisaVersionValidator.SpecificationVersion).SplitWords} should match")

        actualVersion = isr.VI.National.Visa.ResourceManager.ImplementationVersion
        expectedVersion = New Version(isr.VI.National.Visa.MySettings.Default.ImplementationVersion)
        Assert.AreEqual(expectedVersion.ToString, actualVersion.ToString, $"{NameOf(isr.VI.National.Visa.ResourceManager.ImplementationVersion).SplitWords} should match")

        actualVersion = isr.VI.National.Visa.ResourceManager.SpecificationVersion
        expectedVersion = New Version(isr.VI.National.Visa.MySettings.Default.SpecificationVersion)
        Assert.AreEqual(expectedVersion.ToString, actualVersion.ToString, $"{NameOf(isr.VI.National.Visa.ResourceManager.SpecificationVersion).SplitWords} should match")
    End Sub

#End Region

End Class
