'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SlotView.vb
'
' summary:	Slot view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Slot view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SlotView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._SettlingTimeNumeric.NumericUpDownControl.Minimum = 0
        Me._SettlingTimeNumeric.NumericUpDownControl.Maximum = 1000
        Me._SettlingTimeNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SettlingTimeNumeric.NumericUpDownControl.Increment = 10D

        Me._SlotNumberNumeric.NumericUpDownControl.Minimum = 1
        Me._SlotNumberNumeric.NumericUpDownControl.Maximum = 10
        Me._SlotNumberNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SlotNumberNumeric.NumericUpDownControl.Increment = 1D

    End Sub

    ''' <summary> Creates a new <see cref="SlotView"/> </summary>
    ''' <returns> A <see cref="SlotView"/>. </returns>
    Public Shared Function Create() As SlotView
        Dim view As SlotView = Nothing
        Try
            view = New SlotView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Gets or sets the slot card number. </summary>
    ''' <value> The slot card number. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SlotNumber As Integer
        Get
            Return CInt(Me._SlotNumberNumeric.Value)
        End Get
        Set(value As Integer)
            If value <> Me.SlotNumber Then
                Me._SlotNumberNumeric.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the settling time. </summary>
    ''' <value> The settling time. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SettlingTime As TimeSpan
        Get
            Return TimeSpan.FromMilliseconds(Me._SettlingTimeNumeric.Value)
        End Get
        Set(value As TimeSpan)
            If value <> Me.SettlingTime Then
                Me._SettlingTimeNumeric.Value = CDec(value.TotalMilliseconds)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the type of the card. </summary>
    ''' <value> The type of the card. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property CardType As String
        Get
            Return Me._CardTypeTextBox.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.CardType) Then
                Me._CardTypeTextBox.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Reads the settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadSettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading slot configuration"
            SlotView.ReadSettings(Me.RouteSubsystem, Me.SlotNumber)
            Me.ApplyPropertyChanged(Me.RouteSubsystem)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} updating slot configuration"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.SlotNumber > 0 Then
                Me.RouteSubsystem.ApplySlotCardType(Me.SlotNumber, Me._CardTypeTextBox.Text)
                Me.RouteSubsystem.ApplySlotCardSettlingTime(Me.SlotNumber, TimeSpan.FromMilliseconds(CDbl(Me._SettlingTimeNumeric.Value)))
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(SlotView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the Route subsystem. </summary>
    ''' <value> The Route subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property RouteSubsystem As RouteSubsystemBase

    ''' <summary> Bind Route subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As RouteSubsystemBase)
        If Me.RouteSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.RouteSubsystem)
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.RouteSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As RouteSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.RouteSubsystemPropertyChanged
            SlotView.ReadSettings(subsystem, Me.SlotNumber)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.RouteSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As RouteSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(RouteSubsystemBase.SlotCardType))
        Me.HandlePropertyChanged(subsystem, NameOf(RouteSubsystemBase.SlotCardSettlingTime))
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem">  The subsystem. </param>
    ''' <param name="slotNumber"> The slot card number. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As RouteSubsystemBase, ByVal slotNumber As Integer)
        If slotNumber > 0 Then
            subsystem.StartElapsedStopwatch()
            subsystem.QuerySlotCardType(slotNumber)
            subsystem.QuerySlotCardSettlingTime(slotNumber)
            subsystem.StopElapsedStopwatch()
        End If
    End Sub

    ''' <summary> Handle the Route subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As RouteSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(RouteSubsystemBase.SlotCardType)
                Me._CardTypeTextBox.Text = subsystem.SlotCardType(Me.SlotNumber)
            Case NameOf(RouteSubsystemBase.SlotCardSettlingTime)
                Me.SettlingTime = subsystem.SlotCardSettlingTime(Me.SlotNumber)
        End Select
    End Sub

    ''' <summary> Route subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RouteSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(RouteSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.RouteSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.RouteSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, RouteSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Applies the settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
