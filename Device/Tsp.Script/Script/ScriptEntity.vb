''' <summary> Encapsulate the script information. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-07. From TSP Library.  </para><para>
''' David, 2009-03-02, 3.0.3348. </para>
''' </remarks>
Public Class ScriptEntity
    Inherits ScriptEntityBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="name">      Specifies the script name. </param>
    ''' <param name="modelMask"> Specifies the model families for this script. </param>
    Public Sub New(ByVal name As String, ByVal modelMask As String)
        MyBase.New(name, modelMask)
    End Sub

#End Region

End Class

