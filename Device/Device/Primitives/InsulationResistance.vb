'---------------------------------------------------------------------------------------------------
' file:		.\Primitives\InsulationResistance.vb
'
' summary:	Insulation resistance class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

''' <summary> An insulation test configuration. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-03-09 </para>
''' </remarks>
Public Class InsulationResistance
    Inherits isr.Core.Models.ViewModelTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Defines values for the known clear state. </summary>
    Public Overridable Sub DefineClearExecutionState()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overridable Sub InitKnownState()
    End Sub

    ''' <summary> Sets the known preset state. </summary>
    Public Overridable Sub PresetKnownState()
    End Sub

    ''' <summary> Sets the known reset (default) state. </summary>
    Public Overridable Sub ResetKnownState()
        Me.DwellTime = TimeSpan.FromSeconds(2)
        Me.CurrentLimit = 0.00001
        Me.PowerLineCycles = 1
        Me.VoltageLevel = 10
        Me.ResistanceLowLimit = 10000000
        Me.ResistanceRange = 1000000000
        Me.ContactCheckEnabled = True
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> The dwell time. </summary>
    Private _DwellTime As TimeSpan

    ''' <summary> Gets or sets the dwell time. </summary>
    ''' <value> The dwell time. </value>
    Public Property DwellTime As TimeSpan
        Get
            Return Me._DwellTime
        End Get
        Set(value As TimeSpan)
            If Me.DwellTime <> value Then
                Me._DwellTime = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The current limit. </summary>
    Private _CurrentLimit As Double

    ''' <summary> Gets or sets the current limit. </summary>
    ''' <value> The current limit. </value>
    Public Property CurrentLimit As Double
        Get
            Return Me._CurrentLimit
        End Get
        Set(value As Double)
            If value <> Me.CurrentLimit Then
                Me._CurrentLimit = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(InsulationResistance.CurrentRange))
            End If
        End Set
    End Property

    ''' <summary> The power line cycles. </summary>
    Private _PowerLineCycles As Double

    ''' <summary> Gets or sets the power line cycles. </summary>
    ''' <value> The power line cycles. </value>
    Public Property PowerLineCycles As Double
        Get
            Return Me._PowerLineCycles
        End Get
        Set(value As Double)
            If value <> Me.PowerLineCycles Then
                Me._PowerLineCycles = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The voltage level. </summary>
    Private _VoltageLevel As Double

    ''' <summary> Gets or sets the voltage level. </summary>
    ''' <value> The voltage level. </value>
    Public Property VoltageLevel As Double
        Get
            Return Me._VoltageLevel
        End Get
        Set(value As Double)
            If value <> Me.VoltageLevel Then
                Me._VoltageLevel = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(InsulationResistance.CurrentRange))
            End If
        End Set
    End Property

    ''' <summary> The resistance low limit. </summary>
    Private _ResistanceLowLimit As Double

    ''' <summary> Gets or sets the resistance low limit. </summary>
    ''' <value> The resistance low limit. </value>
    Public Property ResistanceLowLimit As Double
        Get
            Return Me._ResistanceLowLimit
        End Get
        Set(value As Double)
            If value <> Me.ResistanceLowLimit Then
                Me._ResistanceLowLimit = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(InsulationResistance.CurrentRange))
            End If
        End Set
    End Property

    ''' <summary> The resistance range. </summary>
    Private _ResistanceRange As Double

    ''' <summary> Gets or sets the resistance range. </summary>
    ''' <value> The resistance range. </value>
    Public Property ResistanceRange As Double
        Get
            Return Me._ResistanceRange

        End Get
        Set(value As Double)
            If value <> Me.ResistanceRange Then
                Me._ResistanceRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True to enable, false to disable the contact check. </summary>
    Private _ContactCheckEnabled As Boolean

    ''' <summary> Gets or sets the contact check enabled. </summary>
    ''' <value> The contact check enabled. </value>
    Public Property ContactCheckEnabled As Boolean
        Get
            Return Me._ContactCheckEnabled
        End Get
        Set(value As Boolean)
            If value <> Me.ContactCheckEnabled Then
                Me._ContactCheckEnabled = value
                Me.NotifyPropertyChanged()
            End If

        End Set
    End Property

    ''' <summary>
    ''' Gets the current range. Based on the ratio of the voltage to the minimum resistance. If the
    ''' resistance is zero, returns the current limit.
    ''' </summary>
    ''' <value> The current range. </value>
    Public ReadOnly Property CurrentRange() As Double
        Get
            Return If(Me.ResistanceLowLimit > 0, Me.VoltageLevel / Me.ResistanceLowLimit, 1.01 * Me.CurrentLimit)
        End Get
    End Property

#End Region

#Region " I TALKER "

    ''' <summary> Identifies talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

#End Region

#Region " TALKER PUBLISH "

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
