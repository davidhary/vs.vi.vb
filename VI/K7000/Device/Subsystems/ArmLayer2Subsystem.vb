''' <summary> Defines a SCPI Arm Layer 2 Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class ArmLayer2Subsystem
    Inherits VI.ArmLayerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ArmLayer2Subsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(2, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.SupportedArmSources = VI.ArmSources.Bus Or VI.ArmSources.External Or VI.ArmSources.Hold Or VI.ArmSources.Immediate Or
                                 VI.ArmSources.Manual Or VI.ArmSources.Timer Or VI.ArmSources.TriggerLink
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " COMMANDS "

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <value> The Immediate command. </value>
    Protected Overrides Property ImmediateCommand As String = ":ARM:LAY2:IMM"

#End Region

#Region " ARM COUNT "

    ''' <summary> Gets or sets Arm count query command. </summary>
    ''' <value> The Arm count query command. </value>
    Protected Overrides Property ArmCountQueryCommand As String = ":ARM:LAY2:COUN?"

    ''' <summary> Gets or sets Arm count command format. </summary>
    ''' <value> The Arm count command format. </value>
    Protected Overrides Property ArmCountCommandFormat As String = ":ARM:LAY2:COUN {0}"

#End Region

#Region " DIRECTION (BYPASS) "

    ''' <summary> Gets or sets the Trigger Direction query command. </summary>
    ''' <value> The Trigger Direction query command. </value>
    Protected Overrides Property ArmLayerBypassModeQueryCommand As String = ":ARM:LAY2:TCON:DIR?"

    ''' <summary> Gets or sets the Trigger Direction command format. </summary>
    ''' <value> The Trigger Direction command format. </value>
    Protected Overrides Property ArmLayerBypassModeCommandFormat As String = ":ARM:LAY2:TCON:DIR {0}"

#End Region

#Region " ARM SOURCE "

    ''' <summary> Gets or sets the Arm source command format. </summary>
    ''' <value> The write Arm source command format. </value>
    Protected Overrides Property ArmSourceCommandFormat As String = ":ARM:LAY2:SOUR {0}"

    ''' <summary> Gets or sets the Arm source query command. </summary>
    ''' <value> The Arm source query command. </value>
    Protected Overrides Property ArmSourceQueryCommand As String = ":ARM:LAY2:SOUR?"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = ":ARM:LAY2:DEL {0:s\.fff}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = ":ARM:LAY2:DEL?"

#End Region

#Region " INPUT LINE NUMBER "

    ''' <summary> Gets or sets the Input Line Number command format. </summary>
    ''' <value> The Input Line Number command format. </value>
    Protected Overrides Property InputLineNumberCommandFormat As String = ":ARM:LAY2:TCON:ASYN:ILIN {0}"

    ''' <summary> Gets or sets the Input Line Number query command. </summary>
    ''' <value> The Input Line Number query command. </value>
    Protected Overrides Property InputLineNumberQueryCommand As String = ":ARM:LAY2:TCON:ASYN:ILIN?"

#End Region

#Region " OUTPUT LINE NUMBER "

    ''' <summary> Gets or sets the Output Line Number command format. </summary>
    ''' <value> The Output Line Number command format. </value>
    Protected Overrides Property OutputLineNumberCommandFormat As String = ":ARM:LAY2:TCON:ASYN:OLIN {0}"

    ''' <summary> Gets or sets the Output Line Number query command. </summary>
    ''' <value> The Output Line Number query command. </value>
    Protected Overrides Property OutputLineNumberQueryCommand As String = ":ARM:LAY2:TCON:ASYN:OLIN?"

#End Region

#Region " TIMER TIME SPAN "

    ''' <summary> Gets or sets the Timer Interval command format. </summary>
    ''' <value> The query command format. </value>
    Protected Overrides Property TimerIntervalCommandFormat As String = ":ARM:LAY2:TIM {0:s\.FFFFFFF}"

    ''' <summary>
    ''' Gets or sets the Timer Interval format for converting the query to time span.
    ''' </summary>
    ''' <value> The Timer Interval query command. </value>
    Protected Overrides Property TimerIntervalFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the Timer Interval query command. </summary>
    ''' <value> The Timer Interval query command. </value>
    Protected Overrides Property TimerIntervalQueryCommand As String = ":ARM:LAY2:TIM?"

#End Region

#End Region

End Class
