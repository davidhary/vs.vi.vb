'---------------------------------------------------------------------------------------------------
' file:		.\Resources\FoundationResourceFinder.vb
'
' summary:	Foundation resource finder class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Foundation.ExceptionExtensions

''' <summary> An IVI Foundation-based Resource Finder. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para>
''' </remarks>
Public Class FoundationResourceFinder
    Inherits VI.Pith.ResourceFinderBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " CONVERSIONS "

    ''' <summary> Converts the given value. </summary>
    ''' <param name="value"> The detailed information derived when VISA.NET parses a resource
    '''                      descriptor. </param>
    ''' <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
    <CLSCompliant(False)>
    Public Shared Function ConvertInterfaceType(ByVal value As Ivi.Visa.HardwareInterfaceType) As VI.Pith.HardwareInterfaceType
        Return If([Enum].IsDefined(GetType(VI.Pith.HardwareInterfaceType), CInt(value)),
                        CType(CInt(value), VI.Pith.HardwareInterfaceType),
                        VI.Pith.HardwareInterfaceType.Gpib)
    End Function

    ''' <summary> Converts the given value. </summary>
    ''' <param name="value"> The detailed information derived when VISA.NET parses a resource
    '''                      descriptor. </param>
    ''' <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
    <CLSCompliant(False)>
    Public Shared Function ConvertInterfaceType(ByVal value As VI.Pith.HardwareInterfaceType) As Ivi.Visa.HardwareInterfaceType
        Return If([Enum].IsDefined(GetType(Ivi.Visa.HardwareInterfaceType), CInt(value)),
                        CType(CInt(value), Ivi.Visa.HardwareInterfaceType),
                        Ivi.Visa.HardwareInterfaceType.Gpib)
    End Function

    ''' <summary> Convert parse result. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="value"> The detailed information derived when VISA.NET parses a resource
    '''                      descriptor. </param>
    ''' <returns> The <see cref="vi.Pith.ResourceNameInfo"/>. </returns>
    <CLSCompliant(False)>
    Public Shared Function ConvertParseResult(ByVal value As Ivi.Visa.ParseResult) As VI.Pith.ResourceNameInfo
        Return New VI.Pith.ResourceNameInfo(value.OriginalResourceName, ConvertInterfaceType(value.InterfaceType), value.InterfaceNumber)
    End Function

    ''' <summary> Converts a value to a resource open state. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="value"> The detailed information derived when VISA.NET parses a resource
    '''                      descriptor. </param>
    ''' <returns> Value as an isr.VI.Pith.ResourceOpenState. </returns>
    <CLSCompliant(False)>
    Public Shared Function ToResourceOpenState(ByVal value As Ivi.Visa.ResourceOpenStatus) As isr.VI.Pith.ResourceOpenState
        Select Case value
            Case Ivi.Visa.ResourceOpenStatus.Unknown
                Return Pith.ResourceOpenState.Unknown
            Case Ivi.Visa.ResourceOpenStatus.Success
                Return Pith.ResourceOpenState.Success
            Case Ivi.Visa.ResourceOpenStatus.DeviceNotResponding
                Return Pith.ResourceOpenState.DeviceNotResponding
            Case Ivi.Visa.ResourceOpenStatus.ConfigurationNotLoaded
                Return Pith.ResourceOpenState.ConfigurationNotLoaded
            Case Else
                Throw New InvalidOperationException($"Unhandled case for {NameOf(Ivi.Visa.ResourceOpenStatus)} {value}({CInt(value)})")
        End Select
    End Function

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Parse resource. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">          Thrown when a Native error condition occurs. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <see cref="vi.Pith.ResourceNameInfo"/>. </returns>
    Public Overrides Function ParseResource(ByVal resourceName As String) As VI.Pith.ResourceNameInfo
        Dim activity As String = $"parsing resource {resourceName}"
        Try
            Return FoundationResourceFinder.ConvertParseResult(Ivi.Visa.GlobalResourceManager.Parse(resourceName))
        Catch ex As System.MissingMethodException
            Throw New isr.Core.OperationFailedException($"Failed {activity}; IVI.Visa assembly {My.Settings.VisaAssemblyVersion} and system file {My.Settings.VisaSystem64FileProductVersion} versions are expected", ex)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, resourceName, "@DM", "Parsing resource")
            Throw New VI.Pith.NativeException(nativeError, ex)
        Catch
            Throw
        End Try
    End Function

    ''' <summary> Gets or sets the using like pattern. </summary>
    ''' <value> The using like pattern. </value>
    Public Overrides ReadOnly Property UsingLikePattern As Boolean = False

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources() As IEnumerable(Of String)
        Dim resources As String = String.Empty
        Try
            Return Ivi.Visa.GlobalResourceManager.Find(VI.Pith.ResourceNamesManager.AllResourcesFilter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, resources, "@DM", "Finding resources")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Lists all resources. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources(ByVal filter As String) As IEnumerable(Of String)
        Try
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, String.Empty, "@DM", "Finding resources")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for all interfaces. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces() As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = VI.Pith.ResourceNamesManager.BuildInterfaceFilter()
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding interfaces")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for the interfaces. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces(ByVal filter As String) As IEnumerable(Of String)
        Try
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding interfaces")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for the interfaces. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim filter As String = String.Empty
        Try
            filter = VI.Pith.ResourceNamesManager.BuildInterfaceFilter(interfaceType)
            Dim resources As IEnumerable(Of String) = Ivi.Visa.GlobalResourceManager.Find(filter)
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding interfaces")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal filter As String) As IEnumerable(Of String)
        Try
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments() As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter()
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType)
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType, boardNumber)
            Return Ivi.Visa.GlobalResourceManager.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

#End Region

End Class
