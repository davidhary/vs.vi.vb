''' <summary> Handles device exception including errors retrieved from the device. </summary>
''' <remarks>
''' David, 2010-12-25, 1.0.4011. Created <para>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<Serializable()>
Public Class DeviceException
    Inherits BaseException

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="isr.Hipot.Driver.DeviceException"/> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New("Device Exception")
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
    ''' data.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

