'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase_StandardRegister.vb
'
' summary:	Session base standard register class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions


Partial Public Class SessionBase

    ''' <summary> The default standard event enable bitmask. </summary>
    Private _DefaultStandardEventEnableBitmask As StandardEvents

    ''' <summary> Gets or sets the default standard event enable bitmask. </summary>
    ''' <value> The default standard event enable bitmask. </value>
    Public Property DefaultStandardEventEnableBitmask As StandardEvents
        Get
            Return Me._DefaultStandardEventEnableBitmask
        End Get
        Set(value As StandardEvents)
            If value <> Me.DefaultStandardEventEnableBitmask Then
                Me._DefaultStandardEventEnableBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Returns a detailed report of the event status register (ESR) byte. </summary>
    ''' <param name="value">     Specifies the value that was read from the status register. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> Returns a detailed report of the event status register (ESR) byte. </returns>
    Public Shared Function BuildReport(ByVal value As VI.Pith.StandardEvents, ByVal delimiter As String) As String

        If String.IsNullOrWhiteSpace(delimiter) Then
            delimiter = "; "
        End If

        Dim builder As New System.Text.StringBuilder

        For Each eventValue As VI.Pith.StandardEvents In [Enum].GetValues(GetType(StandardEvents))
            If eventValue <> VI.Pith.StandardEvents.None AndAlso eventValue <> VI.Pith.StandardEvents.All AndAlso (eventValue And value) <> 0 Then
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(eventValue.Description)
            End If
        Next

        If builder.Length > 0 Then
            builder.Append(".")
            builder.Insert(0, String.Format(Globalization.CultureInfo.CurrentCulture,
                                            "The device standard status register reported: 0x{0:X2}{1}", CInt(value), Environment.NewLine))
        End If
        Return builder.ToString

    End Function

    ''' <summary> The standard event status. </summary>
    Private _StandardEventStatus As VI.Pith.StandardEvents?

    ''' <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
    ''' <value>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
    ''' Events</see>.
    ''' </value>
    Public Property StandardEventStatus() As VI.Pith.StandardEvents?
        Get
            Return Me._StandardEventStatus
        End Get
        Set(ByVal value As VI.Pith.StandardEvents?)
            If Not Nullable.Equals(value, Me.StandardEventStatus) Then
                Me._StandardEventStatus = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(SessionBase.StandardEventStatusHasValue))
            End If
            Me.StandardRegisterCaption = If(value.HasValue, String.Format(SessionBase.RegisterValueFormat, CInt(value.Value)), SessionBase.UnknownRegisterValueCaption)
        End Set
    End Property

    ''' <summary> Gets the standard event status has value. </summary>
    ''' <value> The standard event status has value. </value>
    Public ReadOnly Property StandardEventStatusHasValue As Boolean
        Get
            Return Me.StandardEventStatus.HasValue
        End Get
    End Property

    ''' <summary> Gets or sets the standard event query command. </summary>
    ''' <remarks> <see cref="VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand"></see> </remarks>
    ''' <value> The standard event query command. </value>
    Public Property StandardEventStatusQueryCommand As String = VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand

    ''' <summary> Queries the Standard Event enable bit mask. </summary>
    ''' <returns>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
    ''' Events</see>.
    ''' </returns>
    Public Function QueryStandardEventStatus() As VI.Pith.StandardEvents?
        If Not String.IsNullOrWhiteSpace(Me.StandardEventStatusQueryCommand) Then
            Me.StandardEventStatus = CType(Me.Query(0I, Me.StandardEventStatusQueryCommand), StandardEvents?)
        End If
        Return Me.StandardEventStatus
    End Function

    ''' <summary> The standard register caption. </summary>
    Private _StandardRegisterCaption As String = "0x.."

    ''' <summary> Gets or sets the Standard register caption. </summary>
    ''' <value> The Standard register caption. </value>
    Public Property StandardRegisterCaption As String
        Get
            Return Me._StandardRegisterCaption
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.StandardRegisterCaption) Then
                Me._StandardRegisterCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The standard event enable bitmask. </summary>
    Private _StandardEventEnableBitmask As VI.Pith.StandardEvents?

    ''' <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
    ''' <value>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
    ''' Events</see>.
    ''' </value>
    Public Property StandardEventEnableBitmask() As VI.Pith.StandardEvents?
        Get
            Return Me._StandardEventEnableBitmask
        End Get
        Protected Set(ByVal value As VI.Pith.StandardEvents?)
            If Not Me.StandardEventEnableBitmask.Equals(value) Then
                Me._StandardEventEnableBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property


    ''' <summary>
    ''' Apply (Write and then read) bitmask setting the device to turn on the Event Summary Bit (ESM)
    ''' upon any of the SCPI events unmasked by the bitmask. Uses *ERE to select (unmask) the events
    ''' that will set the ESB.
    ''' </summary>
    ''' <param name="bitmask"> The bitmask; zero to disable all events. </param>
    Public Sub ApplyStandardEventEnableBitmask(ByVal bitmask As VI.Pith.StandardEvents)
        Me.WriteStandardEventEnableBitmask(bitmask)
        Me.QueryStandardEventEnableBitmask()
    End Sub


    ''' <summary>
    ''' Apply (Write and then read) bitmask setting the device to turn on the Event Summary Bit (ESM)
    ''' upon any of the SCPI events unmasked by the bitmask. Uses *ERE to select (unmask) the events
    ''' that will set the ESB.
    ''' </summary>
    ''' <param name="commandFormat"> The standard event enable command format. </param>
    ''' <param name="bitmask">       The bitmask; zero to disable all events. </param>
    Public Sub ApplyStandardEventEnableBitmask(ByVal commandFormat As String, ByVal bitmask As VI.Pith.StandardEvents)
        Me.StandardEventEnableCommandFormat = commandFormat
        Me.ApplyStandardEventEnableBitmask(bitmask)
    End Sub

    ''' <summary> Gets the standard event enable query command. </summary>
    ''' <remarks>
    ''' SCPI: "*ESE?".
    ''' <see cref="VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand"> </see>
    ''' </remarks>
    ''' <value> The standard event enable query command. </value>
    Public Property StandardEventEnableQueryCommand As String = VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand

    ''' <summary> Queries the Standard Event enable bit mask. </summary>
    ''' <returns>
    ''' <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
    ''' Events</see>.
    ''' </returns>
    Public Function QueryStandardEventEnableBitmask() As VI.Pith.StandardEvents?
        If Not String.IsNullOrWhiteSpace(Me.StandardEventEnableQueryCommand) Then
            Me.StandardEventEnableBitmask = CType(Me.Query(0I, Me.StandardEventEnableQueryCommand), VI.Pith.StandardEvents?)
        End If
        Return Me.StandardEventEnableBitmask
    End Function

    ''' <summary> Gets the supports standard event enable query. </summary>
    ''' <value> The supports standard event enable query. </value>
    Public ReadOnly Property SupportsStandardEventEnableQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.StandardEventEnableQueryCommand)
        End Get
    End Property

    ''' <summary> Gets or sets the standard event enable command format. </summary>
    ''' <remarks>
    ''' SCPI: "*ESE {0:D}".
    ''' <see cref="VI.Pith.Ieee488.Syntax.StandardEventEnableCommandFormat"> </see>
    ''' </remarks>
    ''' <value> The standard event enable command format. </value>
    Public Property StandardEventEnableCommandFormat As String = VI.Pith.Ieee488.Syntax.StandardEventEnableCommandFormat

    ''' <summary> Writes the Standard Event enable bitmask. </summary>
    ''' <param name="bitmask"> The bitmask; zero to disable all events. </param>
    Public Sub WriteStandardEventEnableBitmask(ByVal bitmask As VI.Pith.StandardEvents)
        If String.IsNullOrWhiteSpace(Me.StandardEventEnableCommandFormat) Then
            Me.StandardEventEnableBitmask = VI.Pith.StandardEvents.None
        Else
            Me.WriteLine(Me.StandardEventEnableCommandFormat, CInt(bitmask))
            Me.StandardEventEnableBitmask = bitmask
        End If
    End Sub

End Class
