﻿''' <summary> A resistance range current. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-04-09 </para>
''' </remarks>
Public Class ResistanceRangeCurrent

    ''' <summary> Constructor. </summary>
    ''' <param name="range">   The range. </param>
    ''' <param name="current"> The current. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub New(ByVal range As Decimal, ByVal current As Decimal, ByVal caption As String)
        MyBase.New
        Me.ResistanceRange = range
        Me.RangeCurrent = current
        Me.Caption = caption
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="range">   The range. </param>
    ''' <param name="current"> The current. </param>
    Public Sub New(ByVal range As Decimal, ByVal current As Decimal)
        MyBase.New
        Me.ResistanceRange = range
        Me.RangeCurrent = current
        Me.Caption = $"{range:0}{Arebis.StandardUnits.UnitSymbols.Omega} {1000 * current}mA"
    End Sub

    ''' <summary> Gets or sets the resistance range. </summary>
    ''' <value> The resistance range. </value>
    Public Property ResistanceRange As Decimal

    ''' <summary> Gets or sets the range current. </summary>
    ''' <value> The range current. </value>
    Public Property RangeCurrent As Decimal

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    Public Property Caption As String

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Function ToString() As String
        Return Me.Caption
    End Function

End Class

''' <summary> Collection of resistance range currents. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-04-09 </para>
''' </remarks>
Public Class ResistanceRangeCurrentCollection
    Inherits List(Of ResistanceRangeCurrent)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Collections.Generic.List`1" /> class
    ''' that is empty and has the default initial capacity.
    ''' </summary>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Validates the given value. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> A ResistanceRangeCurrentCollection. </returns>
    Public Shared Function Validate(ByVal value As ResistanceRangeCurrentCollection) As ResistanceRangeCurrentCollection
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Return value
    End Function

    ''' <summary> Constructor. </summary>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As ResistanceRangeCurrentCollection)
        MyBase.New
        If values?.Any Then
            For Each v As ResistanceRangeCurrent In values
                Me.Add(v)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Returns the first resistance range greater or equal to the specified range.
    ''' </summary>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A ResistanceRangeCurrent. </returns>
    Public Function MatchResistanceRange(ByVal caption As String) As ResistanceRangeCurrent
        ' select the first range greater than or equal the specified range
        Dim i As Integer = Me.FindIndex(Function(x) x.Caption >= caption)
        Return Me(If(i >= 0, i, 0))
    End Function

    ''' <summary>
    ''' Returns the first resistance range greater or equal to the specified range.
    ''' </summary>
    ''' <param name="range"> The range. </param>
    ''' <returns> A ResistanceRangeCurrent. </returns>
    Public Function MatchResistanceRange(ByVal range As Decimal) As ResistanceRangeCurrent
        ' select the first range greater than or equal the specified range
        Dim i As Integer = Me.FindIndex(Function(x) x.ResistanceRange >= range)
        Return Me(If(i >= 0, i, 0))
    End Function

    ''' <summary>
    ''' Returns the first resistance range greater or equal to the specified range.
    ''' </summary>
    ''' <param name="range">   The range. </param>
    ''' <param name="current"> The current. </param>
    ''' <returns> A ResistanceRangeCurrent. </returns>
    Public Function MatchResistanceRange(ByVal range As Decimal, ByVal current As Decimal) As ResistanceRangeCurrent
        ' select the first range greater than or equal the specified range
        Dim i As Integer = Me.FindIndex(Function(x) (x.ResistanceRange >= range AndAlso x.RangeCurrent >= current))
        Return Me(If(i >= 0, i, 0))
    End Function

End Class