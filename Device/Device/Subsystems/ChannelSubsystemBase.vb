''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-13 </para>
''' </remarks>
Public MustInherit Class ChannelSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        ' clear values to force update.
        Me._ClosedChannels = Nothing
        Me._ClosedChannelsCaption = Nothing
        Me.ClosedChannels = String.Empty
    End Sub

#End Region

#Region " CLOSED CHANNELS "

    ''' <summary> The closed channels. </summary>
    Private _ClosedChannels As String

    ''' <summary> Gets or sets the closed channels. </summary>
    ''' <value> The closed channels. </value>
    Public Overloads Property ClosedChannels As String
        Get
            Return Me._ClosedChannels
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.ClosedChannels) Then
                Me._ClosedChannels = value
                Me.ClosedChannelsCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The closed channels caption. </summary>
    Private _ClosedChannelsCaption As String

    ''' <summary> Gets or sets the closed channels Caption. </summary>
    ''' <value> The closed channels. </value>
    Public Overloads Property ClosedChannelsCaption As String
        Get
            Return Me._ClosedChannelsCaption
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.ClosedChannelsCaption) Then
                If value Is Nothing Then
                    Me._ClosedChannelsCaption = "nil"
                ElseIf String.IsNullOrWhiteSpace(value) Then
                    Me._ClosedChannelsCaption = "all open"
                Else
                    Me._ClosedChannelsCaption = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the closed channels described by value. </summary>
    ''' <param name="value">   The scan list. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyClosedChannels(ByVal value As String, ByVal timeout As TimeSpan) As String
        Me.WriteClosedChannels(value, timeout)
        Return Me.QueryClosedChannels()
    End Function

    ''' <summary> Gets or sets the closed channels query command. </summary>
    ''' <remarks> :ROUT:CLOS. </remarks>
    ''' <value> The closed channels query command. </value>
    Protected Overridable Property ClosedChannelsQueryCommand As String

    ''' <summary> Queries closed channels. </summary>
    ''' <returns> The closed channels. </returns>
    Public Function QueryClosedChannels() As String
        Dim value As String = Me.Query(Me.ClosedChannels, Me.ClosedChannelsQueryCommand)
        Me.ClosedChannels = If(VI.Pith.SessionBase.EqualsNil(value), String.Empty, value)
        Return Me.ClosedChannels
    End Function

    ''' <summary> Gets or sets the closed channels command format. </summary>
    ''' <remarks> :ROUT:CLOS {0} </remarks>
    ''' <value> The closed channels command format. </value>
    Protected Overridable Property ClosedChannelsCommandFormat As String

    ''' <summary> Writes a closed channels. </summary>
    ''' <param name="value">   The scan list. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteClosedChannels(ByVal value As String, ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{String.Format(Me.ClosedChannelsCommandFormat, value)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        Me.ClosedChannels = value
        Return Me.ClosedChannels
    End Function

    ''' <summary> Gets or sets the open channels command format. </summary>
    ''' <remarks> SCPI: :ROUT:OPEN:ALL. </remarks>
    ''' <value> The open channels command format. </value>
    Protected Overridable Property OpenChannelsCommandFormat As String

    ''' <summary> Open the specified channels in the list and read back the closed channels. </summary>
    ''' <param name="channelList"> List of channels. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyOpenChannels(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.WriteOpenChannels(channelList, timeout)
        Return Me.QueryClosedChannels
    End Function

    ''' <summary> Opens the specified channels in the list. </summary>
    ''' <param name="channelList"> List of channels. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteOpenChannels(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{String.Format(Me.OpenChannelsCommandFormat, channelList)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        ' set to nothing to indicate that the value is not known -- requires reading.
        Me.ClosedChannels = Nothing
        Return Me.ClosedChannels
    End Function

    ''' <summary> Gets or sets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    Protected Overridable Property OpenChannelsCommand As String

    ''' <summary> Opens all channels and reads back the closed channels. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyOpenAll(ByVal timeout As TimeSpan) As String
        Me.WriteOpenAll(timeout)
        Return Me.QueryClosedChannels
    End Function

    ''' <summary> Opens all channels. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteOpenAll(ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{Me.OpenChannelsCommand}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        ' set to nothing to indicate that the value is not known -- requires reading.
        Me.ClosedChannels = Nothing
        Return Me.ClosedChannels
    End Function

#End Region

End Class
