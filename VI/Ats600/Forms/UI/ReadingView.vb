'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\ReadingView.vb
'
' summary:	Reading view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A Reading view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ReadingView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ReadingView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As ReadingView
        Dim view As ReadingView = Nothing
        Try
            view = New ReadingView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As Ats600Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As Ats600Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As Ats600Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindThermostreamSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As Ats600Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " THERMO STREAM "

    ''' <summary> Gets or sets the Thermostream subsystem. </summary>
    ''' <value> The Thermostream subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ThermostreamSubsystem As VI.Ats600.ThermostreamSubsystem

    ''' <summary> Bind Thermostream subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindThermostreamSubsystem(ByVal device As Ats600.Ats600Device)
        If Me.ThermostreamSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ThermostreamSubsystem)
            Me._ThermostreamSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._ThermostreamSubsystem = device.ThermostreamSubsystem
            Me.BindSubsystem(True, Me.ThermostreamSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ThermostreamSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ThermostreamSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(Ats600.ThermostreamSubsystem.Temperature))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ThermostreamSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Executes the measurement available action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub OnMeasurementAvailable(ByVal value As Double?)
        If value.HasValue Then
            Me.OnMeasurementAvailable(CStr(value.Value))
        Else
            Me.OnMeasurementAvailable("")
        End If
    End Sub

    ''' <summary> Executes the measurement available action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub OnMeasurementAvailable(ByVal value As String)
        If String.IsNullOrWhiteSpace(value) Then
            'Me._ReadingToolStripStatusLabel.Text = Ats600Control.DegreesCaption("-.-")
            'Me._FailureToolStripStatusLabel.Text = clear
        Else
            'Me._ReadingToolStripStatusLabel.SafeTextSetter(Ats600Control.DegreesCaption(value))
            'Me._FailureToolStripStatusLabel.Text = "  "
            Me.PublishVerbose("Instruments parsed reading elements.")
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ThermostreamSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Ats600.ThermostreamSubsystem.Temperature)
                Me.OnMeasurementAvailable(subsystem.Temperature)
        End Select
    End Sub

    ''' <summary> Sense Current subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ThermostreamSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ThermostreamSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ThermostreamSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ThermostreamSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: MAIN PANEL "

    ''' <summary> Reads last error button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadLastErrorButton_Click(sender As System.Object, e As System.EventArgs) Handles _ReadLastErrorButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading last error"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then

                '  Me.Device.SystemSubsystem.WriteLastSystemErrorQuery()
                '  Dim srq As VI.Pith.ServiceRequests = Me.readServiceRequest
                '  If (srq And VI.Pith.ServiceRequests.MessageAvailable) = 0 Then
                '      Me.InfoProvider.Annunciate(sender,  Core.Forma.InfoProviderLevel.Info, "Nothing to read")
                '  Else
                '      Me.Device.SystemSubsystem.ClearErrorCache()
                '      Me.Device.SystemSubsystem.ReadLastSystemError()
                '  End If

            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the error queue button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearErrorQueueButton_Click(sender As System.Object, e As System.EventArgs) Handles _ClearErrorQueueButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing error queue"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Me.Device.StatusSubsystem.ClearErrorQueue()
                Me.ReadServiceRequest()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by InitButton for click events. Initiates a reading for retrieval by
    ''' way of the service request event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitializeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitializeButton.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} initializing"
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()

            If Me.Device.IsDeviceOpen Then

                ' clear execution state before enabling events
                Me.Device.ClearExecutionState()

                ' set the service request
                Me.Device.ThermostreamSubsystem.WriteTemperatureEventEnableBitmask(255)
                Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)
                Me.Device.ClearExecutionState()
                Me.ReadServiceRequest()
            End If

        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Event handler. Called by _ReadButton for click events. Query the Device for a reading.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading temperature"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                ' update display if changed.
                Me.Device.ThermostreamSubsystem.QueryTemperature()
                Me.ReadServiceRequest()
                Windows.Forms.Application.DoEvents()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Reads service request. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> The service request. </returns>
    Private Function ReadServiceRequest() As VI.Pith.ServiceRequests
        Dim srq As VI.Pith.ServiceRequests = VI.Pith.ServiceRequests.None
        If Me.Device?.IsDeviceOpen Then
            ' it takes a few ms for the event to register. 
            srq = Me.Device.Session.ReadStatusRegister(TimeSpan.FromMilliseconds(Me._SrqRefratoryTimeNumeric.Value))
            Windows.Forms.Application.DoEvents()
        End If
        Return srq
    End Function

    ''' <summary> Reads status byte button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadStatusByteButton_Click(sender As System.Object, e As System.EventArgs) Handles _ReadStatusByteButton.Click

        If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
            Me.ReadServiceRequest()
        End If
    End Sub

    ''' <summary> Sends a button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SendButton_Click(sender As System.Object, e As System.EventArgs) Handles _SendButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} sending"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                If Not String.IsNullOrWhiteSpace(Me._SendComboBox.Text) Then
                    Me.Device.Session.WriteLine(Me._SendComboBox.Text)
                    Me.ReadServiceRequest()
                End If
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Receive button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReceiveButton_Click(sender As Object, e As System.EventArgs) Handles _ReceiveButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} receiving"
            If sender IsNot Nothing AndAlso Me.Device?.IsDeviceOpen Then
                Dim srq As VI.Pith.ServiceRequests = Me.Device.Session.ReadStatusRegister()
                If (srq And VI.Pith.ServiceRequests.MessageAvailable) = 0 Then
                    Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Info, "Nothing to read")
                Else
                    Me._ReceiveTextBox.Text = Me.Device.Session.ReadLineTrimEnd
                    Me.ReadServiceRequest()
                End If
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
