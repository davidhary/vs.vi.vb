'---------------------------------------------------------------------------------------------------
' file:		.\Primitives\MeasurementEventsBitmasks.vb
'
' summary:	Measurement events bitmasks class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

''' <summary>
''' A dictionary of measurement events bitmasks capable of detecting bits status.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-12-19 </para>
''' </remarks>
<Serializable>
Public Class MeasurementEventsBitmaskDictionary
    Inherits BitmasksDictionary

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary> Adds key. </summary>
    ''' <param name="key">     The bitmask key. </param>
    ''' <param name="bitmask"> The bitmask. </param>
    Public Overloads Sub Add(ByVal key As MeasurementEventBitmaskKey, ByVal bitmask As Integer)
        Me.Add(CInt(key), bitmask)
    End Sub

    ''' <summary> Adds key. </summary>
    ''' <param name="key">            The bitmask key. </param>
    ''' <param name="bitmask">        The bitmask. </param>
    ''' <param name="excludeFromAll"> True to exclude, false to include from all. </param>
    Public Overloads Sub Add(ByVal key As MeasurementEventBitmaskKey, ByVal bitmask As Integer, excludeFromAll As Boolean)
        Me.Add(CInt(key), bitmask, excludeFromAll)
    End Sub

    ''' <summary> Query if 'status' bas any bit on. </summary>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> <c>true</c> if any bit on; otherwise <c>false</c> </returns>
    Public Overloads Function IsAnyBitOn(ByVal status As Integer, ByVal key As MeasurementEventBitmaskKey) As Boolean
        Return Me.IsAnyBitOn(status, CInt(key))
    End Function

    ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> A Boolean. </returns>
    Public Overloads Function AreAllBitsOn(ByVal status As Integer, ByVal key As MeasurementEventBitmaskKey) As Boolean
        Return Me.AreAllBitsOn(status, CInt(key))
    End Function

    ''' <summary> Return the masked status value. </summary>
    ''' <param name="status"> The status. </param>
    ''' <param name="key">    The bit mask key. </param>
    ''' <returns> An Integer? </returns>
    Public Overloads Function MaskedValue(ByVal status As Integer, ByVal key As MeasurementEventBitmaskKey) As Integer
        Return Me.MaskedValue(status, CInt(key))
    End Function

    ''' <summary> Query if 'status' bas any bit on. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> <c>true</c> if any bit on; otherwise <c>false</c> </returns>
    Public Overloads Function IsAnyBitOn(ByVal key As MeasurementEventBitmaskKey) As Boolean
        Return Me.IsAnyBitOn(Me.Status, key)
    End Function

    ''' <summary> Query if all bitmask bits in 'status' are on. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> A Boolean. </returns>
    Public Overloads Function AreAllBitsOn(ByVal key As MeasurementEventBitmaskKey) As Boolean
        Return Me.AreAllBitsOn(Me.Status, key)
    End Function

    ''' <summary> Return the masked status value. </summary>
    ''' <param name="key"> The bit mask key. </param>
    ''' <returns> An Integer? </returns>
    Public Overloads Function MaskedValue(ByVal key As MeasurementEventBitmaskKey) As Integer
        Return Me.MaskedValue(Me.Status, key)
    End Function

End Class

''' <summary> Values that represent measurement event bitmask key values. </summary>
Public Enum MeasurementEventBitmaskKey

    ''' <summary> The measurement reading overflow bitmask key value. </summary>
    <Description("Reading Overflow")>
    ReadingOverflow

    ''' <summary> The measurement low limit 1 bitmask key value. </summary>
    <Description("Low Limit 1")>
    LowLimit1

    ''' <summary> The measurement high limit 1 bitmask key value. </summary>
    <Description("High Limit 1")>
    HighLimit1

    ''' <summary> The measurement low limit 2 bitmask key value. </summary>
    <Description("Low Limit 2")>
    LowLimit2

    ''' <summary> The measurement status high limit 2 bitmask key value. </summary>
    <Description("High Limit 2")>
    HighLimit2

    ''' <summary> The measurement status reading available bitmask key value. </summary>
    <Description("Reading Available")>
    ReadingAvailable

    ''' <summary> The measurement status buffer available bitmask key value. </summary>
    <Description("Buffer Available")>
    BufferAvailable

    ''' <summary> The measurement status buffer half full bitmask key value. </summary>
    <Description("Buffer Half Full")>
    BufferHalfFull

    ''' <summary> The measurement status buffer full bitmask key value. </summary>
    <Description("Buffer Full")>
    BufferFull

    ''' <summary> The measurement status buffer pre-triggered bitmask key value. </summary>
    <Description("Buffer Pretriggered")>
    BufferPretriggered

    ''' <summary> The measurement questionable  bitmask key value;
    '''           measurement status is questionable if bit value is on. </summary>
    <Description("Questionable")>
    Questionable

    ''' <summary> The measurement origin bitmask key value specifying 
    '''           A/D converter from which reading originated.
    '''           For the 7510 this will be always 0 (main) or 2 (digitizer). </summary>
    <Description("Origin")>
    Origin

    ''' <summary> The measurement terminal bitmask key value; front is 1 rear is 0. </summary>
    <Description("Measurement Terminal")>
    MeasurementTerminal

    ''' <summary> The first reading in Group bitmask key value. </summary>
    <Description("First Reading In Group")>
    FirstReadingInGroup

    ''' <summary> Indicates a buffer reading with status only value. </summary>
    <Description("Status only measurement")>
    StatusOnly

    ''' <summary> Summary of all limit, overflow and other failure bits. </summary>
    <Description("Failure Summary")>
    FailuresSummary

End Enum
