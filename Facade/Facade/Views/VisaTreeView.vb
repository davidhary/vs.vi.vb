'---------------------------------------------------------------------------------------------------
' file:		.\Views\VisaTreeView.vb
'
' summary:	Visa tree view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> Provides a user interface for a <see cref="VisaSession"/> </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2018-12-24, 6.0.6932.x"> Create based on the read and write control panel </para></remarks>
<System.ComponentModel.DisplayName("VISA Tree View"), System.ComponentModel.Description("Tree View interface for VISA Sessions"),
      System.Drawing.ToolboxBitmap(GetType(VisaView))>
Public Class VisaTreeView
    Inherits ModelViewTalkerBase
    Implements IVisaView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()

        Me._TreePanel.Dock = DockStyle.Fill
        Me._TreePanel.SplitterDistance = 110
        Me._TreePanel.ClearNodes()
        Me.InsertViewControl(0, VisaTreeView._MessagesNodeName, VisaTreeView._MessagesNodeName, Me._TraceMessagesBox)
        Me.InsertViewControl(0, VisaTreeView._SessionNodeName, VisaTreeView._SessionNodeName, Me._SessionPanel)

        ' note that the caption is not set if this is run inside the On Load function.
        ' set defaults for the messages box.
        Me._TraceMessagesBox.ResetCount = 500
        Me._TraceMessagesBox.PresetCount = 250
        Me._TraceMessagesBox.ContainerTreeNode = Me._TreePanel.GetNode(VisaTreeView._MessagesNodeName)
        Me._TraceMessagesBox.TabCaption = VisaTreeView._MessagesNodeName
        Me._TraceMessagesBox.AlertsToggleTreeNode = Me._TreePanel.GetNode(VisaTreeView._MessagesNodeName)
        Me._TraceMessagesBox.CommenceUpdates()
        Me._TreePanel.Enabled = True
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="visaSessionBase"> The visa session. </param>
    Public Sub New(ByVal visaSessionBase As VisaSessionBase)
        Me.New()
        ' assigns the visa session.
        Me.BindVisaSessionBaseThis(visaSessionBase)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.VI.Facade.VisaView and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                Me._TraceMessagesBox.SuspendUpdatesReleaseIndicators()
                ' make sure the session is unbound in case the form is closed without closing the session.
                Me.BindVisaSessionBaseThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me.AddPrivateListener(Me._TraceMessagesBox)
            Me.SelectNavigatorTreeViewFirstNode()
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " VISA SESSION BASE (DEVICE BASE) "

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase Implements IVisaView.VisaSessionBase

    ''' <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
    ''' <param name="visaSessionBase"> The assigned visa session or nothing to release the session. </param>
    Public Overridable Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        Me.BindVisaSessionBaseThis(visaSessionBase)
    End Sub

    ''' <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="visaSessionBase"> The assigned visa session or nothing to release the session. </param>
    Private Sub BindVisaSessionBaseThis(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then

            ' this might remove listeners belonging to the assigned talker. 
            'If Me.VisaSessionBase.Talker IsNot Nothing Then Me.VisaSessionBase.Talker.RemoveListeners()
            Me.RemovePrivateListeners()

            RemoveHandler Me.VisaSessionBase.Opening, AddressOf Me.HandleDeviceOpening
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            RemoveHandler Me.VisaSessionBase.Closing, AddressOf Me.HandleDeviceClosing
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed

            Me._SelectorOpener.AssignSelectorViewModel(Nothing)
            Me._SelectorOpener.AssignOpenerViewModel(Nothing)

            Me.BindVisaSession(False, Me.VisaSessionBase)
            Me.BindSession(False, Me.VisaSessionBase.Session)

            Me._VisaSessionBase = Nothing
        End If
        Me._VisaSessionBase = visaSessionBase
        If visaSessionBase IsNot Nothing Then
            visaSessionBase.AddPrivateListener(Me._TraceMessagesBox)
            AddHandler Me.VisaSessionBase.Opening, AddressOf Me.HandleDeviceOpening
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            AddHandler Me.VisaSessionBase.Closing, AddressOf Me.HandleDeviceClosing
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed

            Me._SelectorOpener.AssignSelectorViewModel(visaSessionBase.SessionFactory)
            Me._SelectorOpener.AssignOpenerViewModel(visaSessionBase)

            Me.BindVisaSession(True, Me.VisaSessionBase)
            Me.BindSession(True, Me.VisaSessionBase.Session)

            Me.VisaSessionBase.Session.ApplySettings()
            If Not String.IsNullOrWhiteSpace(Me.VisaSessionBase.CandidateResourceName) Then Me.VisaSessionBase.Session.CandidateResourceName = Me.VisaSessionBase.CandidateResourceName
            If Not String.IsNullOrWhiteSpace(Me.VisaSessionBase.CandidateResourceTitle) Then Me.VisaSessionBase.Session.CandidateResourceTitle = Me.VisaSessionBase.CandidateResourceTitle
            If Me.VisaSessionBase.IsDeviceOpen Then
                Me.VisaSessionBase.Session.OpenResourceName = Me.VisaSessionBase.OpenResourceName
                Me.VisaSessionBase.Session.OpenResourceTitle = Me.VisaSessionBase.OpenResourceTitle
            End If

            Me.AssignTalker(visaSessionBase.Talker)
            Me.ApplyListenerTraceLevel(ListenerType.Display, visaSessionBase.Talker.TraceShowLevel)

        End If
        Me._SessionView.BindVisaSessionBase(visaSessionBase)
        Me._DisplayView.BindVisaSessionBase(visaSessionBase)
        Me._StatusView.BindVisaSessionBase(visaSessionBase)
    End Sub

    ''' <summary> Bind visa session. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="add">             True to add. </param>
    ''' <param name="visaSessionBase"> The visa session. </param>
    Private Sub BindVisaSession(ByVal add As Boolean, ByVal visaSessionBase As VisaSessionBase)
        For Each t As Control In Me._TreePanel.NodeControls.Values
            If Not (t Is Me._TraceMessagesBox OrElse t Is Me._SessionPanel) Then
                Me.AddRemoveBinding(t, add, NameOf(Control.Enabled), visaSessionBase, NameOf(visaSessionBase.IsSessionOpen))
            End If
        Next
    End Sub

    ''' <summary> Bind session. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindSession(ByVal add As Boolean, ByVal viewModel As VI.Pith.SessionBase)
        Me.AddRemoveBinding(Me._StatusPromptLabel, add, NameOf(Control.Text), viewModel, NameOf(VI.Pith.SessionBase.StatusPrompt))
    End Sub

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Gets the resource name caption. </summary>
    ''' <value> The resource name caption. </value>
    Private Property ResourceNameCaption As String = String.Empty

    ''' <summary> Executes the device Closing action. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Handles the device Closing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceNameCaption} UI handling device closing"
            Me.OnDeviceClosing(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device Closed action. </summary>
    Protected Overridable Sub OnDeviceClosed()
    End Sub

    ''' <summary> Handles the device Close. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceNameCaption} UI handling device closed"
            Me.OnDeviceClosed()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device Opening action. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceOpening(ByVal e As System.ComponentModel.CancelEventArgs)
        Me.ResourceNameCaption = Me.VisaSessionBase.ResourceNameCaption
    End Sub

    ''' <summary> Handles the device Opening. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device Opening"
            Me.OnDeviceOpening(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    Protected Overridable Sub OnDeviceOpened()
        Me.ResourceNameCaption = Me.VisaSessionBase.ResourceNameCaption
    End Sub

    ''' <summary> Handles the device opened. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device opened"
            Me.OnDeviceOpened()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CUSTOMIZATION "

    ''' <summary> Gets the Display view. </summary>
    ''' <value> The Display view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DisplayView As DisplayView Implements IVisaView.DisplayView
        Get
            Return Me._DisplayView
        End Get
    End Property

    ''' <summary> Gets the status view. </summary>
    ''' <value> The status view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property StatusView As StatusView Implements IVisaView.StatusView
        Get
            Return Me._StatusView
        End Get
    End Property

#End Region

#Region " NAVIGATION "

    ''' <summary> Name of the session node. </summary>
    Private Const _SessionNodeName As String = "Session"

    ''' <summary> Name of the messages node. </summary>
    Private Const _MessagesNodeName As String = "Messages"

    ''' <summary> Gets the number of views. </summary>
    ''' <value> The number of tabs. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property ViewsCount As Integer Implements IVisaView.ViewCount
        Get
            Return Me._TreePanel.NodeCount
        End Get
    End Property

    ''' <summary> Removes the node. </summary>
    ''' <param name="nodeName"> Name of the node. </param>
    Public Sub RemoveNode(ByVal nodeName As String)
        Me._TreePanel.RemoveNode(nodeName)
    End Sub

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="view">        The view control. </param>
    ''' <param name="viewIndex">   Zero-based index of the view. </param>
    ''' <param name="viewName">    The name of the view. </param>
    ''' <param name="viewCaption"> The caption of the view. </param>
    Public Sub AddView(ByVal view As ModelViewTalkerBase, ByVal viewIndex As Integer, ByVal viewName As String, ByVal viewCaption As String) Implements IVisaView.AddView
        If view Is Nothing Then Throw New ArgumentNullException(NameOf(view))
        Me.AddView(TryCast(view, Control), viewIndex, viewName, viewCaption)
        view.AddPrivateListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <param name="view"> The view control. </param>
    Public Sub AddView(ByVal view As VisaViewControl) Implements IVisaView.AddView
        Me.AddView(view.Control, view.Index, view.Name, view.Caption)
    End Sub

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="view">        The view control. </param>
    ''' <param name="viewIndex">   Zero-based index of the view. </param>
    ''' <param name="viewName">    The name of the view. </param>
    ''' <param name="viewCaption"> The caption of the view. </param>
    Public Sub AddView(ByVal view As Windows.Forms.Control, ByVal viewIndex As Integer, ByVal viewName As String, ByVal viewCaption As String) Implements IVisaView.AddView
        If view Is Nothing Then Throw New ArgumentNullException(NameOf(view))
        Me.InsertViewControl(viewIndex, viewName, viewCaption, view)
    End Sub

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="index">   Zero-based index of the. </param>
    ''' <param name="name">    Name of the node. </param>
    ''' <param name="caption"> The node caption. </param>
    ''' <param name="view">    The view control. </param>
    Public Sub InsertViewControl(ByVal index As Integer, ByVal name As String, ByVal caption As String, ByVal view As Control)
        If view Is Nothing Then Throw New ArgumentNullException(NameOf(view))
        view.Visible = False
        Me._TreePanel.InsertNode(index, name, caption, view)
    End Sub

    ''' <summary> Gets the last tree view node selected. </summary>
    ''' <value> The last tree view node selected. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property LastTreeViewNodeSelected As String
        Get
            Return If(Me._TreePanel.LastNodeSelected Is Nothing, String.Empty, Me._TreePanel.LastNodeSelected.Name)
        End Get
    End Property

    ''' <summary> Select navigator tree view first node. </summary>
    Public Sub SelectNavigatorTreeViewFirstNode()
        Me._TreePanel.SelectNavigatorTreeViewFirstNode()
    End Sub

    ''' <summary> Selects the navigator tree view node. </summary>
    ''' <param name="nodeName"> The node. </param>
    Public Sub SelectNavigatorTreeViewNode(ByVal nodeName As String)
        Me._TreePanel.SelectNavigatorTreeViewNode(nodeName)
    End Sub

    ''' <summary> After node selected. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="e"> Tree view event information. </param>
    Protected Overridable Sub AfterNodeSelected(e As TreeViewEventArgs)
    End Sub

    ''' <summary> Tree panel after node selected. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tree view event information. </param>
    Private Sub TreePanel_AfterNodeSelected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles _TreePanel.AfterNodeSelected
        Me.AfterNodeSelected(e)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds a private listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Public Overrides Sub AddPrivateListener(ByVal listener As Core.IMessageListener)
        MyBase.AddPrivateListener(listener)
        ' this causes a binding cross thread exception when binding to the selector opener. Weird. 
        Me._SelectorOpener.AddPrivateListener(listener)
        Me._StatusView.AddPrivateListener(listener)
        Me._DisplayView.AddPrivateListener(listener)
        Me._SessionView.AddPrivateListener(listener)
    End Sub

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As ITraceMessageTalker)
        Me._SelectorOpener.AssignTalker(talker)
        Me._StatusView.AssignTalker(talker)
        Me._DisplayView.AssignTalker(talker)
        Me._SessionView.AssignTalker(talker)
        ' assigned last as this identifies all talkers.
        MyBase.AssignTalker(talker)
        ' this adds the private listener to the talker.
        Me.AddPrivateListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Removes the private listeners. </summary>
    Public Overrides Sub RemovePrivateListeners()
        Me._SelectorOpener.RemovePrivateListener(Me._TraceMessagesBox)
        Me._StatusView.RemovePrivateListener(Me._TraceMessagesBox)
        Me._DisplayView.RemovePrivateListener(Me._TraceMessagesBox)
        Me._SessionView.RemovePrivateListener(Me._TraceMessagesBox)
        MyBase.RemovePrivateListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        Me._SelectorOpener.ApplyListenerTraceLevel(listenerType, value)
        Me._SessionView?.ApplyListenerTraceLevel(listenerType, value)
        ' this should apply only to the listeners associated with this form
        ' MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " UNIT TESTS INTERNALS "

    ''' <summary> Gets the number of internal resource names. </summary>
    ''' <value> The number of internal resource names. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalResourceNamesCount As Integer Implements IVisaView.InternalResourceNamesCount
        Get
            Return Me._SelectorOpener.InternalResourceNamesCount
        End Get
    End Property

    ''' <summary> Gets the name of the internal selected resource. </summary>
    ''' <value> The name of the internal selected resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalSelectedResourceName As String Implements IVisaView.InternalSelectedResourceName
        Get
            Return Me._SelectorOpener.InternalSelectedResourceName
        End Get
    End Property

#End Region

End Class

