'---------------------------------------------------------------------------------------------------
' file:		Tests\K2002\Subsystems_Tests.vb
'
' summary:	Subsystems tests class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

''' <summary> K2002 Subsystems unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2002")>
Public Class SubsystemsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(K2002ResourceInfo.Exists, $"{GetType(K2002Tests.ResourceSettings)} settings not found")
        Assert.IsTrue(K2002SubsystemsInfo.Exists, $"{GetType(K2002Tests.SubsystemsSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " STATUS SUSBSYSTEM "

    ''' <summary> Opens session check status. </summary>
    ''' <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
    ''' <param name="resourceInfo">     Information describing the resource. </param>
    ''' <param name="subsystemsInfo">   Information describing the subsystems. </param>
    Private Shared Sub OpenSessionCheckStatus(ByVal readErrorEnabled As Boolean, ByVal resourceInfo As ResourceSettings, ByVal subsystemsInfo As SubsystemsSettings)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValues(device.Session, resourceInfo, subsystemsInfo)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceModel(device.StatusSubsystemBase, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceErrors(device.StatusSubsystemBase, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertTerminationValues(device.Session, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertLineFrequency(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertIntegrationPeriod(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.MeasureSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.SenseSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertClearSessionDeviceErrors(device, subsystemsInfo)
                If readErrorEnabled Then isr.VI.DeviceTests.DeviceManager.AssertReadingDeviceErrors(device, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOrphanMessages(device.StatusSubsystemBase)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> A test for Open Session and status. </summary>
    <TestMethod()>
    Public Sub OpenSessionCheckStatusTest()
        SubsystemsTests.OpenSessionCheckStatus(False, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) tests open session read device errors. </summary>
    <TestMethod()>
    Public Sub OpenSessionReadDeviceErrorsTest()
        SubsystemsTests.OpenSessionCheckStatus(True, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

#End Region

#Region " SENSE SUBSYSTEM TESTS "

    ''' <summary> (Unit Test Method) tests read sense subsystem. </summary>
    <TestMethod()>
    Public Sub ReadSenseSubsystemTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, K2002ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.SenseSubsystem, K2002SubsystemsInfo)
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests toggle sense subsystem function mode. </summary>
    ''' <remarks> David, 2020-07-29. </remarks>
    <TestMethod()>
    Public Sub ToggleSenseSubsystemFunctionModeTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.ToggleFunctionMode(device.SenseSubsystem, SenseFunctionModes.ResistanceFourWire, SenseFunctionModes.Resistance)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MEASURE TESTS "

    ''' <summary> (Unit Test Method) tests measure subsystem fetch. </summary>
    <TestMethod()>
    Public Sub MeasureSubsystemFetchTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Dim propertyName As String
            Dim sw As Stopwatch = Stopwatch.StartNew
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, K2002ResourceInfo)

                propertyName = NameOf(VisaSessionBase.ResetKnownState).SplitWords
                device.Session.ResetKnownState()
                Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed after {propertyName}")

                propertyName = NameOf(StatusSubsystemBase.PresetKnownState).SplitWords
                device.StatusSubsystem.PresetKnownState()
                Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed after {propertyName}")

                propertyName = NameOf(StatusSubsystemBase.MeasurementEventEnableBitmask).SplitWords
                Assert.AreEqual(&H7FFF, device.StatusSubsystem.QueryMeasurementEventEnableBitmask.GetValueOrDefault(0), $"{propertyName} bits should all be set")

                propertyName = NameOf(StatusSubsystemBase.MeasurementEventPositiveTransitionBitmask).SplitWords
                Assert.AreEqual(&H7FFF, device.StatusSubsystem.QueryMeasurementEventPositiveTransitionBitmask.GetValueOrDefault(0), $"{propertyName} bits should all be set")

                propertyName = NameOf(StatusSubsystemBase.MeasurementEventNegativeTransitionBitmask).SplitWords
                Assert.AreEqual(0, device.StatusSubsystem.QueryMeasurementEventNegativeTransitionBitmask.GetValueOrDefault(0), $"{propertyName} bits should all be cleared")

                propertyName = NameOf(VisaSessionBase.ClearExecutionState).SplitWords
                device.Session.ClearExecutionState()
                Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed after {propertyName}")

                propertyName = NameOf(BinningSubsystemBase.Limit1AutoClear).SplitWords
                Assert.IsTrue(device.BinningSubsystem.ApplyLimit1AutoClear(True).GetValueOrDefault(False), $"{propertyName} value should match")

                propertyName = NameOf(TriggerSubsystemBase.Abort).SplitWords
                device.TriggerSubsystem.Abort()
                Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed after {propertyName}")

                propertyName = NameOf(TriggerSubsystemBase.Initiate).SplitWords
                Dim initialTimestamp As DateTimeOffset = DateTimeOffset.Now
                sw.Restart()
                device.TriggerSubsystem.Initiate()

                isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(100))

                propertyName = isr.VI.MeasurementEventBitmaskKey.ReadingAvailable.ToString.SplitWords
                Dim timedOut As Boolean
                Dim actualBitmask As Integer?
                Dim expectedBitmask As Integer = device.StatusSubsystem.MeasurementEventsBitmasks(isr.VI.MeasurementEventBitmaskKey.ReadingAvailable)
                Dim task As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer?)) = Pith.SessionBase.StartAwaitingBitmaskTask(expectedBitmask, TimeSpan.FromMilliseconds(200),
                                                                                                                                            Function() device.StatusSubsystem.QueryMeasurementEventStatus)
                timedOut = task.Result.TimedOut
                actualBitmask = task.Result.Status

                Dim stepNumber As Integer = 1
                Assert.IsFalse(timedOut, $"Step#{stepNumber}: {propertyName} should not timeout")

                propertyName = NameOf(StatusSubsystemBase.MeasurementEventStatus).SplitWords
                Assert.AreEqual(expectedBitmask, actualBitmask, $"Step#{stepNumber}: {propertyName} should match")

                ' the traced (fetched) buffer includes only status values.
                device.TraceSubsystem.OrderedReadingElementTypes = New List(Of ReadingElementTypes) From {VI.ReadingElementTypes.Reading}

                Dim actualBufferReadingsCount As Integer = device.TraceSubsystem.AddBufferReadings(device.MeasureSubsystem.FunctionUnit, initialTimestamp, sw.Elapsed)
                Assert.AreEqual(1, actualBufferReadingsCount, $"Step#{stepNumber}: Buffer reading counts should match")

                Dim readingNumber As Integer = 0
                Dim bufferReadings As IEnumerable(Of BufferReading) = device.TraceSubsystem.DequeueRange(device.TraceSubsystem.NewReadingsCount)

                ' build the buffer readings amounts
                For Each bufferReading As BufferReading In bufferReadings
                    readingNumber += 1
                    ' ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Reading}")
                    bufferReading.BuildAmount(device.SenseSubsystem.FunctionUnit)
                    TestInfo.TraceMessage($"Step#{stepNumber} Reading #{readingNumber} MEAS: Event={device.Session.QueryTrimEnd(":stat:meas:even?")}. Cond={device.Session.QueryTrimEnd(":stat:meas:cond?")}")
                    SubsystemsTests.TestInfo.TraceMessage($"Step#{stepNumber} Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4}")
                Next

                propertyName = NameOf(BufferReading).SplitWords
                Dim epsilon As Double = 0.001
                expectedBitmask = 0
                readingNumber = 0
                For Each bufferReading As BufferReading In bufferReadings
                    readingNumber += 1
                    Assert.AreEqual(0, bufferReading.Amount.Value, epsilon, $"Step#{stepNumber} Reading #{readingNumber} {propertyName} value should match")
                    Assert.AreEqual(expectedBitmask, bufferReading.StatusWord,
                                    $"Step#{stepNumber} Reading #{readingNumber} {NameOf(isr.VI.BufferReading.StatusWord)} should match")
                Next

                propertyName = NameOf(BinningSubsystemBase.Limit1LowerLevel).SplitWords
                Dim expectedLowLevel As Double = 1
                Dim actualLowLevel As Double = device.BinningSubsystem.ApplyLimit1LowerLevel(expectedLowLevel).GetValueOrDefault(0)
                Assert.AreEqual(expectedLowLevel, actualLowLevel, $"{propertyName} value should match")

                propertyName = NameOf(BinningSubsystemBase.Limit1Enabled).SplitWords
                Assert.IsTrue(device.BinningSubsystem.ApplyLimit1Enabled(True).GetValueOrDefault(False), $"{propertyName} value should match")

                propertyName = NameOf(TriggerSubsystemBase.Abort).SplitWords
                device.TriggerSubsystem.Abort()
                Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed after {propertyName}")

                initialTimestamp = DateTimeOffset.Now
                sw.Restart()
                propertyName = NameOf(TriggerSubsystemBase.Initiate).SplitWords
                device.TriggerSubsystem.Initiate()

                expectedBitmask = device.StatusSubsystem.MeasurementEventsBitmasks(isr.VI.MeasurementEventBitmaskKey.ReadingAvailable)
                propertyName = isr.VI.MeasurementEventBitmaskKey.ReadingAvailable.ToString.SplitWords
                task = Pith.SessionBase.StartAwaitingBitmaskTask(expectedBitmask, TimeSpan.FromMilliseconds(200),
                                                                 Function() device.StatusSubsystem.QueryMeasurementEventStatus)
                timedOut = task.Result.TimedOut
                actualBitmask = task.Result.Status

                stepNumber += 1
                Assert.IsFalse(timedOut, $"Step#{stepNumber}: {propertyName} should not timeout")

                actualBufferReadingsCount = device.TraceSubsystem.AddBufferReadings(device.MeasureSubsystem.FunctionUnit, initialTimestamp, sw.Elapsed)
                Assert.AreEqual(1, actualBufferReadingsCount, $"Step#{stepNumber}: Buffer reading counts should match")

                readingNumber = 0
                bufferReadings = device.TraceSubsystem.DequeueRange(device.TraceSubsystem.NewReadingsCount)

                ' build the buffer readings amounts
                For Each bufferReading As BufferReading In bufferReadings
                    readingNumber += 1
                    ' ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Reading}")
                    bufferReading.BuildAmount(device.SenseSubsystem.FunctionUnit)
                    SubsystemsTests.TestInfo.TraceMessage($"Step#{stepNumber} Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4}")
                Next

                propertyName = NameOf(BufferReading).SplitWords
                readingNumber = 0
                expectedBitmask = device.StatusSubsystem.MeasurementEventsBitmasks(isr.VI.MeasurementEventBitmaskKey.LowLimit1)
                For Each bufferReading As BufferReading In bufferReadings
                    readingNumber += 1
                    Assert.AreEqual(0, bufferReading.Amount.Value, epsilon, $"Step#{stepNumber} Reading #{readingNumber} {propertyName} value should match")
                    Assert.AreEqual(expectedBitmask,
                                    bufferReading.StatusWord,
                                    $"Step#{stepNumber} Reading #{readingNumber} {NameOf(isr.VI.BufferReading.StatusWord)} should match")
                Next

            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub


#End Region

#Region " DIGITAL OUTPUT SUBSYSTEM TEST "

    ''' <summary> (Unit Test Method) tests toggle digital output polarity. </summary>
    <TestMethod()>
    Public Sub ToggleDigitalOutputPolarityTest()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(SubsystemsTests.TestInfo, device, K2002ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDigitalOutputSignalPolarity(device.DigitalOutputSubsystem)
            Catch
                Throw
            Finally
                K2002Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class
