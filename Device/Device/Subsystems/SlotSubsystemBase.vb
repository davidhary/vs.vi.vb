''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-13 </para>
''' </remarks>
Public MustInherit Class SlotSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <param name="slotNumber">      The slot number. </param>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal slotNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._SlotNumber = slotNumber
        Me.EnumerateInterlocksThis(2)
    End Sub

#End Region

#Region " SLOT NUMBER "

    ''' <summary> Gets or sets the slot number. </summary>
    ''' <value> The slot number. </value>
    Public ReadOnly Property SlotNumber As Integer

#End Region

#Region " EXISTS "

    ''' <summary> The is slot exists. </summary>
    Private _IsSlotExists As Boolean?

    ''' <summary> Gets or sets (Protected) the Slot existence indicator. </summary>
    ''' <value> The Slot existence indicator. </value>
    Public Property IsSlotExists() As Boolean?
        Get
            Return Me._IsSlotExists
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.IsSlotExists) Then
                Me._IsSlotExists = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ' TspSyntax.Slot.SubsystemNameFormat

    ''' <summary> Gets or sets the slot exists query command format. </summary>
    ''' <value> The slot exists query command format. </value>
    Protected MustOverride ReadOnly Property SlotExistsQueryCommandFormat As String

    ''' <summary> Queries slot exists. </summary>
    ''' <returns> The slot exists. </returns>
    Public Function QuerySlotExists() As Boolean?
        Me.IsSlotExists = Not Me.Session.IsNil(String.Format(Me.SlotExistsQueryCommandFormat, Me.SlotNumber))
        If Not Me.IsSlotExists.GetValueOrDefault(False) Then
            Me.SupportsInterlock = False
            Me.InterlocksState = 0
        End If
        Return Me.IsSlotExists
    End Function

#End Region

#Region " INTERLOCKS "

    ''' <summary> Gets or sets the interlocks. </summary>
    ''' <value> The interlocks. </value>
    Public ReadOnly Property Interlocks As InterlockCollection

    ''' <summary> Enumerate interlocks. </summary>
    ''' <param name="interlockCount"> Number of interlocks. </param>
    Private Sub EnumerateInterlocksThis(ByVal interlockCount As Integer)
        Me._Interlocks = New InterlockCollection
        For i As Integer = 1 To interlockCount
            Me._Interlocks.Add(i)
        Next
    End Sub

#Region " SUPPORTS INTERLOCK "

    ''' <summary> The supports interlock. </summary>
    Private _SupportsInterlock As Boolean?

    ''' <summary> Gets or sets the supports interlock. </summary>
    ''' <value> The supports interlock. </value>
    Public Property SupportsInterlock() As Boolean?
        Get
            Return Me._SupportsInterlock
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.SupportsInterlock) Then
                Me._SupportsInterlock = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ' TspSyntax.Slot.InterlockStateFormat

    ''' <summary> Gets or sets the interlock state name query format. </summary>
    ''' <value> The interlock state name query format. </value>
    Protected MustOverride ReadOnly Property InterlockStateNameQueryFormat As String

    ''' <summary> Queries supports interlock. </summary>
    ''' <returns> The supports interlock. </returns>
    Public Function QuerySupportsInterlock() As Boolean?
        If Not Me.IsSlotExists.HasValue Then
            Me.QuerySlotExists()
        End If
        If Me.IsSlotExists.GetValueOrDefault(False) Then
            Me.SupportsInterlock = Not Me.Session.IsNil(Me.InterlockStateNameQueryFormat, Me.SlotNumber)
        End If
        Return Me.SupportsInterlock
    End Function

#End Region

#Region " INTERLOCAK STATE "

    ''' <summary> State of the interlocks. </summary>
    Private _InterlocksState As Integer?

    ''' <summary> Gets or sets the state of the interlocks. </summary>
    ''' <value> The interlock state. </value>
    Public Property InterlocksState() As Integer?
        Get
            Return Me._InterlocksState
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Integer?.Equals(value, Me.InterlocksState) Then
                Me._InterlocksState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the interlock state query command format. </summary>
    ''' <remarks> see TspSyntax.Slot.InterlockStatePrintCommandFormat. </remarks>
    ''' <value> The interlock state query command format. </value>
    Protected MustOverride ReadOnly Property InterlockStateQueryCommandFormat As String

    ''' <summary> Queries interlocks state. </summary>
    ''' <returns> The interlock state. </returns>
    Public Function QueryInterlocksState() As Integer?
        If Not Me.IsSlotExists.HasValue Then Me.QuerySlotExists()
        If Not Me.SupportsInterlock.HasValue Then Me.QuerySupportsInterlock()
        If Me.IsSlotExists.GetValueOrDefault(False) AndAlso Me.QuerySupportsInterlock.GetValueOrDefault(False) Then
            Me.InterlocksState = Me.Query(Me.InterlocksState, String.Format(Me.InterlockStateQueryCommandFormat, Me.SlotNumber))
            Me.Interlocks.UpdateInterlockState(Me.InterlocksState.GetValueOrDefault(0))
        End If
        Return Me.InterlocksState
    End Function

    ''' <summary> Query if 'interlockNumber' is interlock engaged. </summary>
    ''' <param name="interlockNumber"> The interlock number. </param>
    ''' <returns> <c>true</c> if interlock engaged; otherwise <c>false</c> </returns>
    Public Function IsInterlockEngaged(ByVal interlockNumber As Integer) As Boolean
        If Me.SupportsInterlock.GetValueOrDefault(False) Then
            If Not Me.InterlocksState.HasValue Then
                Me.QueryInterlocksState()
            End If
            Return (Me.InterlocksState.GetValueOrDefault(0) And interlockNumber) = interlockNumber
        Else
            Return True
        End If
    End Function

#End Region
#End Region

End Class

