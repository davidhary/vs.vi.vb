''' <summary> Defines a DigitalInputOutput Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-15 </para>
''' </remarks>
Public MustInherit Class DigitalInputOutputSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " DIGITAL LINES "

    ''' <summary> The digital lines. </summary>
    Private _DigitalLines As DigitalLineCollection

    ''' <summary> Gets or sets the digital lines. </summary>
    ''' <value> The digital lines. </value>
    Public Property DigitalLines As DigitalLineCollection
        Get
            Return Me._DigitalLines
        End Get
        Protected Set(value As DigitalLineCollection)
            Me._DigitalLines = value
        End Set
    End Property

#End Region

#Region " DIGITAL LINE MODE  "

    ''' <summary> Writes and reads back the Digital Input Output Digital Line Mode. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The Digital Input Output Digital Line Mode. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">DigitalInputOutput Digital Line Mode</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalLineMode(ByVal lineNumber As Integer, ByVal value As DigitalLineMode) As DigitalLineMode?
        Return Me.ApplyDigitalLineMode(Me.DigitalLines(lineNumber), value)
    End Function

    ''' <summary> Writes and reads back the Digital Input Output Digital Line Mode. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    ''' <param name="value">       The Digital Input Output Digital Line Mode. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">DigitalInputOutput Digital Line Mode</see> or none if unknown.
    ''' </returns>
    Public Function ApplyDigitalLineMode(ByVal digitalLine As DigitalLine, ByVal value As DigitalLineMode) As DigitalLineMode?
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        Me.WriteDigitalLineMode(digitalLine, value)
        Me.QueryDigitalLineMode(digitalLine)
        Return digitalLine.DigitalLineMode
    End Function

    ''' <summary> Get the Digital Line Mode query command. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function DigitalLineModeQueryCommand(ByVal lineNumber As Integer) As String

    ''' <summary> Queries the Digital Line Mode. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">Digital Line Mode</see> or none if unknown.
    ''' </returns>
    Public Function QueryDigitalLineMode(ByVal lineNumber As Integer) As DigitalLineMode?
        Return Me.QueryDigitalLineMode(Me.DigitalLines(lineNumber))
    End Function

    ''' <summary> Queries the Digital Line Mode. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">Digital Line Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryDigitalLineMode(ByVal digitalLine As DigitalLine) As DigitalLineMode?
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        Return digitalLine.QueryDigitalLineMode(Me.Session, Me.DigitalLineModeQueryCommand(digitalLine.LineNumber))
    End Function

    ''' <summary> Gets Digital Line Mode command format. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function DigitalLineModeCommandFormat(ByVal lineNumber As Integer) As String

    ''' <summary>
    ''' Writes the Digital Line Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The Digital Line Mode. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">Digital Line Mode</see> or none if unknown.
    ''' </returns>
    Public Function WriteDigitalLineMode(ByVal lineNumber As Integer, ByVal value As DigitalLineMode) As DigitalLineMode?
        Return Me.WriteDigitalLineMode(Me.DigitalLines(lineNumber), value)
    End Function

    ''' <summary>
    ''' Writes the Digital Line Mode without reading back the value from the device.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    ''' <param name="value">       The Digital Line Mode. </param>
    ''' <returns>
    ''' The <see cref="DigitalLineMode">Digital Line Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteDigitalLineMode(ByVal digitalLine As DigitalLine, ByVal value As DigitalLineMode) As DigitalLineMode?
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        Return digitalLine.WriteDigitalLineMode(Me.Session, Me.DigitalLineModeCommandFormat(digitalLine.LineNumber), value)
    End Function

#End Region

#Region " DIGITAL LINE RESET "

    ''' <summary> Digital line reset command. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function DigitalLineResetCommand(ByVal lineNumber As Integer) As String

    ''' <summary> Resets the digital line. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    Public Overridable Sub ResetDigitalLine(ByVal lineNumber As Integer)
        Me.ResetDigitalLine(Me.DigitalLines(lineNumber))
    End Sub

    ''' <summary> Resets the digital line. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    Public Overridable Sub ResetDigitalLine(ByVal digitalLine As DigitalLine)
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        digitalLine.ResetDigitalLine(Me.Session, Me.DigitalLineResetCommand(digitalLine.LineNumber))
    End Sub

#End Region

#Region " DIGITAL LINE STATE "

    ''' <summary> True if digital line read write supported. </summary>
    Private _DigitalLineReadWriteSupported As Boolean

    ''' <summary> Gets or sets the digital line read write Supported. </summary>
    ''' <value>
    ''' The digital line read write Supported. <c>True</c> if both read and write are supported and
    ''' Supported; <c>False</c> otherwise.
    ''' </value>
    Public Property DigitalLineReadWriteSupported As Boolean
        Get
            Return Me._DigitalLineReadWriteSupported
        End Get
        Protected Set(ByVal value As Boolean)
            If Not Nullable.Equals(Me.DigitalLineReadWriteSupported, value) Then
                Me._DigitalLineReadWriteSupported = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Digital Input Output Digital Line State. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The Digital Input Output Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Function ApplyDigitalLineState(ByVal lineNumber As Integer, ByVal value As VI.DigitalLineState) As Integer?
        Return Me.ApplyDigitalLineState(Me.DigitalLines(lineNumber), value)
    End Function

    ''' <summary> Writes and reads back the Digital Input Output Digital Line State. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    ''' <param name="value">       The Digital Input Output Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Function ApplyDigitalLineState(ByVal digitalLine As DigitalLine, ByVal value As VI.DigitalLineState) As Integer?
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        If digitalLine.DigitalLineReadWriteEnabled Then
            Me.WriteDigitalLineState(digitalLine, value)
            Me.QueryDigitalLineState(digitalLine)
        Else
            Throw New InvalidOperationException($"Digital line #{digitalLine.LineNumber} read and write not enabled")
        End If
        Return digitalLine.DigitalLineState
    End Function

    ''' <summary> Get the Digital Line State query command. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function DigitalLineStateQueryCommand(ByVal lineNumber As Integer) As String

    ''' <summary> Queries the Digital Line State. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> The digital line state. </returns>
    Public Function QueryDigitalLineState(ByVal lineNumber As Integer) As VI.DigitalLineState?
        Return Me.QueryDigitalLineState(Me.DigitalLines(lineNumber))
    End Function

    ''' <summary> Queries the Digital Line State. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    ''' <returns> The digital line state. </returns>
    Public Overridable Function QueryDigitalLineState(ByVal digitalLine As DigitalLine) As VI.DigitalLineState?
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        Return digitalLine.QueryDigitalLineState(Me.Session, Me.DigitalLineStateQueryCommand(digitalLine.LineNumber))
    End Function

    ''' <summary> Gets Digital Line State command format. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> A String. </returns>
    Protected MustOverride Function DigitalLineStateCommandFormat(ByVal lineNumber As Integer) As String

    ''' <summary>
    ''' Writes the Digital Line State without reading back the value from the device.
    ''' </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Function WriteDigitalLineState(ByVal lineNumber As Integer, ByVal value As VI.DigitalLineState) As VI.DigitalLineState?
        Return Me.WriteDigitalLineState(Me.DigitalLines(lineNumber), value)
    End Function

    ''' <summary>
    ''' Writes the Digital Line State without reading back the value from the device.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalLine"> The digital line. </param>
    ''' <param name="value">       The Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Overridable Function WriteDigitalLineState(ByVal digitalLine As DigitalLine, ByVal value As VI.DigitalLineState) As VI.DigitalLineState?
        If digitalLine Is Nothing Then Throw New ArgumentNullException(NameOf(digitalLine))
        Return digitalLine.WriteDigitalLineState(Me.Session, Me.DigitalLineStateCommandFormat(digitalLine.LineNumber), value)
    End Function

#End Region

#Region " READ PORT (LEVEL) "

    ''' <summary> The level. </summary>
    Private _Level As Integer?

    ''' <summary> Gets or sets the level. </summary>
    ''' <value> The level. </value>
    Public Overloads Property Level As Integer?
        Get
            Return Me._Level
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.Level, value) Then
                Me._Level = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the level described by value. </summary>
    ''' <param name="value"> The Digital Input Output Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Function ApplyLevel(ByVal value As Integer) As Integer?
        Me.WriteLevel(value)
        Return Me.QueryLevel
    End Function

    ''' <summary> Gets or sets the level query command. </summary>
    ''' <value> The level query command. </value>
    Protected Overridable Property LevelQueryCommand As String

    ''' <summary> Queries the level. </summary>
    ''' <returns> The level. </returns>
    Public Function QueryLevel() As Integer?
        Me.Level = Me.Query(Me.Level, Me.LevelQueryCommand)
        Return Me.Level
    End Function

    ''' <summary> Reads the port. </summary>
    ''' <returns> The port. </returns>
    Public Function ReadPort() As Integer?
        Return Me.QueryLevel
    End Function

    ''' <summary> Gets or sets the level command format. </summary>
    ''' <value> The level command format. </value>
    Protected Overridable Property LevelCommandFormat As String

    ''' <summary> Writes the Level without reading back the value from the device. </summary>
    ''' <param name="value"> The Digital Input Output Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Function WriteLevel(ByVal value As Integer) As Integer?
        Me.Level = Me.Write(value, Me.LevelCommandFormat)
        Return Me.Level
    End Function

    ''' <summary> Writes a port. </summary>
    ''' <param name="value"> The Digital Input Output Digital Line State. </param>
    ''' <returns> An Integer? </returns>
    Public Function WritePort(ByVal value As Integer) As Integer?
        Return Me.WriteLevel(value)
    End Function

#End Region

End Class
