﻿'---------------------------------------------------------------------------------------------------
' file:		.\My\MyApplication_Info.vb
'
' summary:	My application information class
'---------------------------------------------------------------------------------------------------
Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.VI.Pith.My.ProjectTraceEventId.DeviceFacade

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Generic Device Facade"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Generic Virtual Instrument Facade"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "VI.Device.Facade"

    End Class

End Namespace

