'---------------------------------------------------------------------------------------------------
' file:		.\Session\Session.vb
'
' summary:	Session class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.InteropServices.ComTypes

Imports isr.VI.Foundation.ExceptionExtensions

Imports Ivi.Visa

''' <summary> A National Instrument message based session. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-20 </para>
''' </remarks>
Public Class Session
    Inherits VI.Pith.SessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the MessageBasedSession object from the specified resource name.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        ' flags service request as not enabled.
        Me._EnabledEventType = Ivi.Visa.EventType.Custom
    End Sub

#Region " Disposable Support"

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Try
                    Me._TcpipSession = Nothing
                    Me.DisposeSession()
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Failed discarding enabled events.",
                                 $"Failed discarding enabled events. {ex.ToFullBlownString}")
                End Try
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    #End Region

    #End Region

    #Region " SESSION "

    ''' <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
    ''' <value> The dummy sentinel. </value>
    Public Overrides ReadOnly Property IsDummy As Boolean = False

    ''' <summary> The visa session. </summary>
    Private _VisaSession As Ivi.Visa.IMessageBasedSession

    ''' <summary> The visa session. </summary>
    ''' <remarks>
    ''' Must be defined without events; Otherwise, setting the timeout causes a memory exception.
    ''' </remarks>
    ''' <value> The visa session. </value>
    <CLSCompliant(False)>
    Public Property VisaSession As Ivi.Visa.IMessageBasedSession
        Get
            Return Me._VisaSession
        End Get
        Set(value As Ivi.Visa.IMessageBasedSession)
            Me._VisaSession = value
            Me._TcpipSession = TryCast(Me.VisaSession, Ivi.Visa.ITcpipSession)
        End Set
    End Property

    ''' <summary> Gets the type of the hardware interface. </summary>
    ''' <value> The type of the hardware interface. </value>
    <CLSCompliant(False)>
    Public ReadOnly Property HardwareInterfaceType As Ivi.Visa.HardwareInterfaceType
        Get
            Return If(Me.VisaSession Is Nothing, Ivi.Visa.HardwareInterfaceType.Custom, Me.VisaSession.HardwareInterfaceType)
        End Get
    End Property

    ''' <summary> Gets the TCP IP session. </summary>
    ''' <value> The TCP IP session. </value>
    <CLSCompliant(False)>
    Public ReadOnly Property TcpipSession As Ivi.Visa.ITcpipSession

    ''' <summary>
    ''' Gets the session open sentinel. When open, the session is capable of addressing the hardware.
    ''' See also <see cref="P:VI.Pith.SessionBase.IsDeviceOpen" />.
    ''' </summary>
    ''' <value> The is session open. </value>
    Public Overrides ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.ResourceOpenState = Pith.ResourceOpenState.Success AndAlso Not Me.VisaSession Is Nothing
        End Get
    End Property

    ''' <summary> Executes the session open action. </summary>
    ''' <param name="resourceName">  Name of the resource. </param>
    ''' <param name="resourceTitle"> The resource title. </param>
    Protected Overrides Sub OnSessionOpen(resourceName As String, resourceTitle As String)
        ' 6824: disable events if enabled. Apparently, the events are enabled, 
        ' which causes issues trying to read STB to detect message available as VISA might be 
        ' reading the STB byte already.
        Me.VisaSession.DisableEvent(Ivi.Visa.EventType.ServiceRequest)
        ' reads session defaults.
        MyBase.OnSessionOpen(resourceName, resourceTitle)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Ivi.Visa.IMessageBasedSession" /> class.
    ''' </summary>
    ''' <remarks>
    ''' This method does not lock the resource. Rev 4.1 and 5.0 of VISA did not support this call and
    ''' could not verify the resource.
    ''' </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <exception cref="NativeVisaException">      Thrown when a Native Visa error condition occurs. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">          Thrown when a Native error condition occurs. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="timeout">      The open timeout. </param>
    Protected Overrides Sub CreateSession(ByVal resourceName As String, ByVal timeout As TimeSpan)
        Try
            Me.ClearLastError()
            Dim result As Ivi.Visa.ResourceOpenStatus = Ivi.Visa.ResourceOpenStatus.Success
            If Me.Enabled Then
                Dim activity As String = "creating visa session"
                result = Ivi.Visa.ResourceOpenStatus.Unknown
                Me.VisaSession = CType(Ivi.Visa.GlobalResourceManager.Open(resourceName, Ivi.Visa.AccessModes.None, CInt(timeout.TotalMilliseconds), result),
                                           Ivi.Visa.IMessageBasedSession)
                If result <> Ivi.Visa.ResourceOpenStatus.Success Then Throw New isr.Core.OperationFailedException($"Failed {activity}; '{result}';. {resourceName}")
                If Me.VisaSession Is Nothing Then Throw New isr.Core.OperationFailedException($"Failed {activity};. {resourceName}")
                Dim currentTimeout As Integer = Me.VisaSession.TimeoutMilliseconds
                ' This was added because Keysight VISA accepts a host device even if the device (e.g., at gpib0,7) is inaccessible.
                Try
                    ' setting short timeout does not seem to return control faster here. Interesting.
                    Me.VisaSession.TimeoutMilliseconds = 50
                    Me.VisaSession.ReadStatusByte()
                Catch
                    Throw New Ivi.Visa.NativeVisaException(Ivi.Visa.NativeErrorCode.ResourceNotFound)
                Finally
                    Me.VisaSession.TimeoutMilliseconds = currentTimeout
                End Try
            End If
        Catch ex As Ivi.Visa.NativeVisaException
            Me.DisposeSession()
            Me.LastNativeError = New NativeError(ex.ErrorCode, resourceName, "@opening", "opening session")
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Catch
            Me.DisposeSession()
            Throw
        End Try
    End Sub

    ''' <summary> Gets the sentinel indication that the VISA session is disposed. </summary>
    ''' <value> The is session disposed. </value>
    Public Overrides ReadOnly Property IsSessionDisposed As Boolean
        Get
            Return Me._VisaSession Is Nothing
        End Get
    End Property

    ''' <summary>
    ''' Disposes the VISA <see cref="T:isr.VI.Pith.SessionBase">Session</see> ending access to the
    ''' instrument.
    ''' </summary>
    Protected Overrides Sub DisposeSession()
        If Me.VisaSession IsNot Nothing Then
            Try
                Me._VisaSession.Dispose()
            Catch
                Throw
            Finally
                Me.ResourceOpenState = Pith.ResourceOpenState.Unknown
                Me._TcpipSession = Nothing
                Me._VisaSession = Nothing
            End Try
        End If
    End Sub

    ''' <summary> Discards the session events. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Protected Overrides Sub DiscardAllEvents()
        If Me.IsSessionOpen Then
            Try
                Me.VisaSession.DiscardEvents(Ivi.Visa.EventType.AllEnabled)
            Catch ex As Ivi.Visa.NativeVisaException
                Me.LastNativeError = New NativeError(ex.ErrorCode, Me.ResourceNameCaption, "@discarding", "discarding  all events")
                Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Checks if the candidate resource name exists. If so, assign to the
    ''' <see cref="ValidatedResourceName">validated resource name</see>
    ''' </summary>
    ''' <returns> <c>true</c> if it the resource exists; otherwise <c>false</c> </returns>
    Public Overrides Function ValidateCandidateResourceName() As Boolean
        Using rm As New ResourcesProvider
            Return MyBase.ValidateCandidateResourceName(rm)
        End Using
    End Function

    ''' <summary>
    ''' Gets or sets the sentinel indicating if call backs are performed in a specific
    ''' synchronization context.
    ''' </summary>
    ''' <remarks>
    ''' For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object marshals
    ''' callbacks across threads appropriately.<para>
    ''' DH: 3339 Setting true prevents display.
    ''' </para><para>
    ''' Note that setting to false also breaks display updates.
    ''' </para>
    ''' </remarks>
    ''' <value>
    ''' The sentinel indicating if call backs are performed in a specific synchronization context.
    ''' </value>
    Public Overrides Property SynchronizeCallBacks As Boolean
        Get
            If Me.IsSessionOpen Then
                MyBase.SynchronizeCallbacks = Me.VisaSession.SynchronizeCallbacks
            End If
            Return MyBase.SynchronizeCallbacks
        End Get
        Set(value As Boolean)
            MyBase.SynchronizeCallbacks = value
            If Me.IsSessionOpen Then
                Me.VisaSession.SynchronizeCallbacks = value
            End If
        End Set
    End Property

#End Region

#Region " READ/WRITE "

    ''' <summary> Gets or sets the ASCII character used to end reading. </summary>
    ''' <value> The termination character. </value>
    Public Overrides Property ReadTerminationCharacter As Byte
        Get
            Return If(Me.IsSessionOpen, Me.VisaSession.TerminationCharacter, isr.Core.EscapeSequencesExtensions.Methods.NewLineValue)
        End Get
        Set(value As Byte)
            If Me.ReadTerminationCharacter <> value Then
                If Me.IsSessionOpen Then
                    Me.VisaSession.TerminationCharacter = value
                    If Not Me.TcpipSession Is Nothing Then
                        Me.TcpipSession.TerminationCharacter = value
                    End If
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the termination character enabled specifying whether the read operation ends
    ''' when a termination character is received.
    ''' </summary>
    ''' <value> The termination character enabled. </value>
    Public Overrides Property ReadTerminationCharacterEnabled As Boolean
        Get
            Return Not Me.IsSessionOpen OrElse Me.VisaSession.TerminationCharacterEnabled
        End Get
        Set(value As Boolean)
            If Me.ReadTerminationCharacterEnabled <> value Then
                If Me.IsSessionOpen Then
                    Me.VisaSession.TerminationCharacterEnabled = value
                    If Not Me.TcpipSession Is Nothing Then
                        Me.TcpipSession.TerminationCharacterEnabled = value
                    End If
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the timeout for I/O communication on this resource session. </summary>
    ''' <value> The communication timeout. </value>
    Public Overrides Property CommunicationTimeout As TimeSpan
        Get
            Return If(Me.IsSessionOpen, TimeSpan.FromMilliseconds(Me.VisaSession.TimeoutMilliseconds), TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            If Me.CommunicationTimeout <> value Then
                If Me.IsSessionOpen Then
                    Me.VisaSession.TimeoutMilliseconds = CInt(value.TotalMilliseconds)
                    If Not Me.TcpipSession Is Nothing Then
                        Me.TcpipSession.TimeoutMilliseconds = CInt(value.TotalMilliseconds)
                    End If
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Query if 'readStatus' is read ended. </summary>
    ''' <param name="readStatus"> The read status. </param>
    ''' <returns> <c>true</c> if read ended; otherwise <c>false</c> </returns>
    Private Shared Function IsReadEnded(ByVal readStatus As Ivi.Visa.ReadStatus) As Boolean
        Return readStatus = Ivi.Visa.ReadStatus.EndReceived OrElse
               readStatus = Ivi.Visa.ReadStatus.TerminationCharacterEncountered
    End Function

    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data irrespective of the buffer size.
    ''' </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <returns> The received message. </returns>
    Public Overrides Function ReadFreeLine() As String
        Dim builder As New System.Text.StringBuilder
        Try
            Me.ClearLastError()
            Dim endReadstatus As Ivi.Visa.ReadStatus = Ivi.Visa.ReadStatus.Unknown
            Dim bufferSize As Integer = Me.ReadBufferSizeAttribute
            If Me.IsSessionOpen Then
                Dim hitEndRead As Boolean = False
                Do
                    Dim msg As String = Me.VisaSession.RawIO.ReadString(bufferSize, endReadstatus)
                    hitEndRead = Session.IsReadEnded(endReadstatus)
                    builder.Append(msg)
                    isr.Core.ApplianceBase.DoEvents()
                Loop Until hitEndRead
                Me.LastMessageReceived = builder.ToString
            Else
                Me.LastMessageReceived = Me.EmulatedReply
            End If
            Return builder.ToString
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, Me.LastMessageSent, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastMessageSent, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Catch ex As Ivi.Visa.IOTimeoutException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(Ivi.Visa.NativeErrorCode.Timeout, Me.OpenResourceName, Me.LastNodeNumber.Value, Me.LastMessageSent, Me.LastAction),
                New NativeError(Ivi.Visa.NativeErrorCode.Timeout, Me.OpenResourceName, Me.LastMessageSent, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Finally
            ' must clear the reply after each reading otherwise could get cross information.
            Me.EmulatedReply = String.Empty
            Me.LastInputOutputStopwatch.Restart()
        End Try
    End Function

    ''' <summary>
    ''' Synchronously reads ASCII-encoded string data. Reads up to the
    ''' <see cref="ReadTerminationCharacter">termination character</see>.
    ''' </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <returns> The received message. </returns>
    Public Overrides Function ReadFiniteLine() As String
        Try
            Me.ClearLastError()
            Me.LastMessageReceived = If(Me.IsSessionOpen, Me.VisaSession.RawIO.ReadString(), Me.EmulatedReply)
            ' must clear the reply after each reading otherwise could get cross information.
            Me.EmulatedReply = String.Empty
            Return Me.LastMessageReceived
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, Me.LastMessageSent, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastMessageSent, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Catch ex As Ivi.Visa.IOTimeoutException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(Ivi.Visa.NativeErrorCode.Timeout, Me.OpenResourceName, Me.LastNodeNumber.Value, Me.LastMessageSent, Me.LastAction),
                New NativeError(Ivi.Visa.NativeErrorCode.Timeout, Me.OpenResourceName, Me.LastMessageSent, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Finally
            Me.LastInputOutputStopwatch.Restart()
        End Try
    End Function

    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
    ''' data with the <see cref="ReadTerminationCharacter">termination character</see>. <para>
    ''' Per IVI documentation: Appends a newline (0xA) to the formatted I/O write buffer, flushes the
    ''' buffer, and sends an end-of-line with the buffer if required.</para>
    ''' </summary>
    ''' <remarks> David, 2020-07-23. </remarks>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function SyncWriteLine(ByVal dataToWrite As String) As String
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Try
                Me.ClearLastError()
                If Me.IsSessionOpen Then
                    Me.VisaSession.FormattedIO.WriteLine(dataToWrite)
                    ' Me.VisaSession.RawIO.Write(dataToWrite)
                End If
                Me.LastMessageSent = dataToWrite
            Catch ex As Ivi.Visa.NativeVisaException
                Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                    New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, dataToWrite, Me.LastAction),
                    New NativeError(ex.ErrorCode, Me.OpenResourceName, dataToWrite, Me.LastAction))
                Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
            Finally
                Me.LastInputOutputStopwatch.Restart()
            End Try
        End If
        Return dataToWrite
    End Function

    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface.<para>
    ''' Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
    ''' formatted I/O write buffer</para>
    ''' </summary>
    ''' <remarks> David, 2020-07-23. </remarks>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function SyncWrite(ByVal dataToWrite As String) As String
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Try
                Me.ClearLastError()
                If Me.IsSessionOpen Then
                    Me.VisaSession.FormattedIO.Write(dataToWrite)
                End If
                Me.LastMessageSent = dataToWrite
            Catch ex As Ivi.Visa.NativeVisaException
                Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                    New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, dataToWrite, Me.LastAction),
                    New NativeError(ex.ErrorCode, Me.OpenResourceName, dataToWrite, Me.LastAction))
                Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
            Finally
                Me.LastInputOutputStopwatch.Restart()
            End Try
        End If
        Return dataToWrite
    End Function

    ''' <summary> Size of the input buffer. </summary>
    Private _InputBufferSize As Integer

    ''' <summary> Gets the size of the input buffer. </summary>
    ''' <value> The size of the input buffer. </value>
    Public Overrides ReadOnly Property InputBufferSize As Integer
        Get
            If Me._InputBufferSize = 0 Then Me._InputBufferSize = Me.ReadBufferSizeAttribute()
            Return Me._InputBufferSize
        End Get
    End Property

#End Region

#Region " REGISTERS "

    ''' <summary> Reads status byte. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    ''' <returns> The status byte. </returns>
    Protected Overrides Function ThreadUnsafeReadStatusByte() As VI.Pith.ServiceRequests
        Try
            Me.ClearLastError()
            Dim value As VI.Pith.ServiceRequests = Me.EmulatedStatusByte
            Me.EmulatedStatusByte = 0
            If Me.IsSessionOpen Then
                value = CType(Me.VisaSession.ReadStatusByte, VI.Pith.ServiceRequests)
            End If
            Me.StatusByte = value
            Return Me.StatusByte
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, "@STB", Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, "@STB", Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Finally
            Me.LastInputOutputStopwatch.Restart()
        End Try
    End Function

    ''' <summary> Clears the device. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Protected Overrides Sub Clear()
        Try
            Me.ClearLastError()
            If Me.IsSessionOpen Then Me.VisaSession.Clear()
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, "@DCL", Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, "@DCL", Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Finally
            Me.LastInputOutputStopwatch.Restart()
        End Try
    End Sub

    ''' <summary> Clears the device (SDC). </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Protected Overrides Sub ClearDevice()
        Me.Clear()
        If Me.SupportsClearInterface AndAlso Me.Enabled Then
            Using gi As GpibInterfaceSession = New GpibInterfaceSession()
                gi.OpenSession(Me.ResourceNameInfo.InterfaceResourceName)
                If gi.IsOpen Then
                    gi.SelectiveDeviceClear(Me.VisaSession.ResourceName)
                Else
                    Throw New isr.Core.OperationFailedException($"Failed opening GPIB Interface Session {Me.ResourceNameInfo.InterfaceResourceName}")
                End If
            End Using
        End If
    End Sub

#End Region

#Region " EVENTS "

#Region " SUSPEND / RESUME SRQ "

    ''' <summary> Resume service request handing. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Public Overrides Sub ResumeServiceRequestHanding()
        Me.LastAction = "Resuming service request"
        Dim lastMessage As String = String.Empty
        Try
            MyBase.ResumeServiceRequestHanding()
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, lastMessage, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, lastMessage, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        End Try
    End Sub

    ''' <summary> Suspends the service request handling. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Public Overrides Sub SuspendServiceRequestHanding()
        Me.LastAction = "Suspending service request"
        Dim lastMessage As String = String.Empty
        Try
            MyBase.SuspendServiceRequestHanding()
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, lastMessage, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, lastMessage, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        End Try
    End Sub

#End Region

    ''' <summary> Visa session service request. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Message based session event information. </param>
    Private Overloads Sub OnServiceRequested(sender As Object, e As Ivi.Visa.VisaEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Ivi.Visa.EventType.ServiceRequest = e.EventType Then
            If Me.ServiceRequestHandlingSuspended Then
                Me.ServiceRequestType = e.EventType.ToString
                Me.SuspendedServiceRequestedCount += 1
            Else
                Me.OnServiceRequested(System.EventArgs.Empty)
            End If
        End If
    End Sub

    ''' <summary> Type of the enabled event. </summary>
    Private _EnabledEventType As Ivi.Visa.EventType

    ''' <summary>
    ''' Gets or sets or set (protected) the sentinel indication if a service request event handler
    ''' was enabled and registered.
    ''' </summary>
    ''' <value>
    ''' <c>True</c> if service request event is enabled and registered; otherwise, <c>False</c>.
    ''' </value>
    Public Overrides Property ServiceRequestEventEnabled As Boolean
        Get
            Return Ivi.Visa.EventType.ServiceRequest = Me._EnabledEventType
        End Get
        Set(value As Boolean)
            If Me.ServiceRequestEventEnabled <> value Then
                Me._EnabledEventType = If(value, Ivi.Visa.EventType.ServiceRequest, Ivi.Visa.EventType.Custom)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Enables and adds the service request event handler. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Public Overrides Sub EnableServiceRequestEventHandler()
        Dim lastMessage As String = String.Empty
        Me.LastAction = "Enabling service request"
        Try
            Me.ClearLastError()
            If Not Me.ServiceRequestEventEnabled Then
                If Me.IsSessionOpen Then
                    ' must define the handler before enabling the events.
                    ' Firewall access must be granted to VXI instrument for service request handling;
                    ' With Windows 1909, both public and private network check box access must be checked.
                    lastMessage = "add SRQ handler"
                    AddHandler Me.VisaSession.ServiceRequest, AddressOf Me.OnServiceRequested
                    ' Enabling the VISA session events causes an exception of unsupported mechanism.
                    ' Apparently, the service request event is enabled when adding the event handler.
                    ' verified using NI Trace. The NI trace shows
                    ' viEnableEvent (TCPIP0::192.168.0.144::inst0::INSTR (0x00000001), 0x3FFF200B (VI_EVENT_SERVICE_REQ), 2 (0x2), 0 (0x0))
                    ' as success following with the same command as failure.
                    ' Note that enabling causes the unsupported mechanism exception.
                    ' Removed per above note: Me.VisaSession.EnableEvent(Ivi.Visa.EventType.ServiceRequest)
                End If
                ' this turns on the enabled sentinel
                lastMessage = "turning on service request enabled"
                Me.ServiceRequestEventEnabled = True
            End If
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, lastMessage, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, lastMessage, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        End Try
    End Sub

    ''' <summary> Disables and removes the service request event handler. </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Public Overrides Sub DisableServiceRequestEventHandler()
        Dim lastMessage As String = String.Empty
        Me.LastAction = "Disabling service request"
        Try
            Me.ClearLastError()
            If Me.ServiceRequestEventEnabled Then
                If Me.IsSessionOpen Then
                    lastMessage = "discard events"
                    Me.DiscardServiceRequests()
                    ' Apparently, the service request event is enabled when removing the event handler.
                    ' Note that disabling twice does not cause an exception.
                    ' Removed per above note: Me.VisaSession.DisableEvent(Ivi.Visa.EventType.ServiceRequest)
                    If Me.HardwareInterfaceType = Ivi.Visa.HardwareInterfaceType.Tcp Then
                        lastMessage = "remove TCP/IP SRQ handler"
                        RemoveHandler Me.TcpipSession.ServiceRequest, AddressOf Me.OnServiceRequested
                    Else
                        lastMessage = "SRQ handler"
                        RemoveHandler Me.VisaSession.ServiceRequest, AddressOf Me.OnServiceRequested
                    End If
                End If
                ' this turns off the enabled sentinel
                lastMessage = "turning off service request enabled"
                Me.ServiceRequestEventEnabled = False
            End If
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, lastMessage, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, lastMessage, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        End Try
    End Sub

    ''' <summary> Discard pending service requests. </summary>
    Public Overrides Sub DiscardServiceRequests()
        If Me.IsSessionOpen Then Me.VisaSession.DiscardEvents(Ivi.Visa.EventType.ServiceRequest)
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets or sets the 'trigger' command. </summary>
    ''' <value> The 'trigger' command. </value>
    Private ReadOnly Property TriggerCommand As String = "*TRG"

    ''' <summary>
    ''' Asserts a software or hardware trigger depending on the interface; Sends a bus trigger.
    ''' </summary>
    ''' <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
    Public Overrides Sub AssertTrigger()
        Try
            Me.ClearLastError()
            If Me.IsSessionOpen Then Me.VisaSession.AssertTrigger()
        Catch ex As Ivi.Visa.NativeVisaException
            Me.LastNativeError = If(Me.LastNodeNumber.HasValue,
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.LastNodeNumber.Value, Me.TriggerCommand, Me.LastAction),
                New NativeError(ex.ErrorCode, Me.OpenResourceName, Me.TriggerCommand, Me.LastAction))
            Throw New VI.Pith.NativeException(Me.LastNativeError, ex)
        Finally
            Me.LastInputOutputStopwatch.Restart()
        End Try
    End Sub


#End Region

#Region " INTERFACE "

    ''' <summary> Keeps the TCP/IP interface alive. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="tcpipSession"> The TCP IP session. </param>
    ''' <returns> A TimeSpan. </returns>
    <CLSCompliant(False)>
    Public Shared Function KeepInterfaceAlive(ByVal tcpipSession As Ivi.Visa.ITcpipSession) As TimeSpan
        If tcpipSession Is Nothing Then Throw New ArgumentNullException(NameOf(tcpipSession))
        Dim sw As System.Diagnostics.Stopwatch = System.Diagnostics.Stopwatch.StartNew
        Dim resourceOpenStatus As Ivi.Visa.ResourceOpenStatus
        Using gi As Ivi.Visa.ITcpipSocketSession = TryCast(Ivi.Visa.GlobalResourceManager.Open($"tcpip0::{tcpipSession.Address}::{tcpipSession.Port}::socket",
                                                                                               Ivi.Visa.AccessModes.None, 3000, resourceOpenStatus),
                                                                Ivi.Visa.ITcpipSocketSession)
            If gi IsNot Nothing AndAlso resourceOpenStatus = Ivi.Visa.ResourceOpenStatus.Success Then
                If gi.ResourceLockState = Ivi.Visa.ResourceLockState.NoLock Then gi.LockResource(1)
                gi.UnlockResource()
            End If
        End Using
        Return sw.Elapsed
    End Function

    ''' <summary> Clears the interface. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Protected Overrides Sub ImplementClearHardwareInterface()
        If Me.SupportsClearInterface AndAlso Me.Enabled Then
            Using gi As GpibInterfaceSession = New GpibInterfaceSession()
                gi.OpenSession(Me.ResourceNameInfo.InterfaceResourceName)
                If gi.IsOpen Then
                    gi.SelectiveDeviceClear(Me.VisaSession.ResourceName)
                Else
                    Throw New isr.Core.OperationFailedException($"Failed opening GPIB Interface Session {Me.ResourceNameInfo.InterfaceResourceName}")
                End If
            End Using
        End If
    End Sub

#End Region

End Class
