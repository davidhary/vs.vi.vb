<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RelayView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RelayView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._AddRelayMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AddMemoryLocationToListMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearChannelLIstMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SlotNumberComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SlotNumberNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._RelayNumberNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._RelayNumberNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._MemoryLocationNumberLabel = New System.Windows.Forms.ToolStripLabel()
        Me._MemoryLocationNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._SlotNumberComboBoxLabel, Me._SlotNumberNumeric, Me._RelayNumberNumericLabel, Me._RelayNumberNumeric, Me._MemoryLocationNumberLabel, Me._MemoryLocationNumeric})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(475, 26)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 2
        Me._SubsystemToolStrip.Text = "ChannelToolStrip"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AddRelayMenuItem, Me._AddMemoryLocationToListMenuItem, Me._ClearChannelLIstMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(52, 23)
        Me._SubsystemSplitButton.Text = "Relay"
        Me._SubsystemSplitButton.ToolTipText = "Select action"
        '
        '_AddRelayMenuItem
        '
        Me._AddRelayMenuItem.Name = "_AddRelayMenuItem"
        Me._AddRelayMenuItem.Size = New System.Drawing.Size(222, 22)
        Me._AddRelayMenuItem.Text = "Add Relay to List"
        Me._AddRelayMenuItem.ToolTipText = "Adds a relay to the channel list"
        '
        '_AddMemoryLocationToListMenuItem
        '
        Me._AddMemoryLocationToListMenuItem.Name = "_AddMemoryLocationToListMenuItem"
        Me._AddMemoryLocationToListMenuItem.Size = New System.Drawing.Size(222, 22)
        Me._AddMemoryLocationToListMenuItem.Text = "Add Memory Location to List"
        Me._AddMemoryLocationToListMenuItem.ToolTipText = "Adds a memory location to the list"
        '
        '_ClearChannelLIstMenuItem
        '
        Me._ClearChannelLIstMenuItem.Name = "_ClearChannelLIstMenuItem"
        Me._ClearChannelLIstMenuItem.Size = New System.Drawing.Size(222, 22)
        Me._ClearChannelLIstMenuItem.Text = "Clear List"
        Me._ClearChannelLIstMenuItem.ToolTipText = "Clears the channel list builder"
        '
        '_SlotNumberComboBoxLabel
        '
        Me._SlotNumberComboBoxLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._SlotNumberComboBoxLabel.Name = "_SlotNumberComboBoxLabel"
        Me._SlotNumberComboBoxLabel.Size = New System.Drawing.Size(27, 23)
        Me._SlotNumberComboBoxLabel.Text = "Slot"
        '
        '_SlotNumberNumeric
        '
        Me._SlotNumberNumeric.Name = "_SlotNumberNumeric"
        Me._SlotNumberNumeric.Size = New System.Drawing.Size(41, 23)
        Me._SlotNumberNumeric.Text = "0"
        Me._SlotNumberNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_RelayNumberNumericLabel
        '
        Me._RelayNumberNumericLabel.Name = "_RelayNumberNumericLabel"
        Me._RelayNumberNumericLabel.Size = New System.Drawing.Size(38, 23)
        Me._RelayNumberNumericLabel.Text = "Relay:"
        '
        '_RelayNumberNumeric
        '
        Me._RelayNumberNumeric.Margin = New System.Windows.Forms.Padding(0, 1, 0, 1)
        Me._RelayNumberNumeric.Name = "_RelayNumberNumeric"
        Me._RelayNumberNumeric.Size = New System.Drawing.Size(41, 24)
        Me._RelayNumberNumeric.Text = "0"
        Me._RelayNumberNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_MemoryLocationNumberLabel
        '
        Me._MemoryLocationNumberLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._MemoryLocationNumberLabel.Name = "_MemoryLocationNumberLabel"
        Me._MemoryLocationNumberLabel.Size = New System.Drawing.Size(104, 23)
        Me._MemoryLocationNumberLabel.Text = "Memory Location:"
        '
        '_MemoryLocationNumeric
        '
        Me._MemoryLocationNumeric.Name = "_MemoryLocationNumeric"
        Me._MemoryLocationNumeric.Size = New System.Drawing.Size(41, 23)
        Me._MemoryLocationNumeric.Text = "0"
        Me._MemoryLocationNumeric.ToolTipText = "Memory location"
        Me._MemoryLocationNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'RelayView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "RelayView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(477, 26)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _AddRelayMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearChannelLIstMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SlotNumberComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SlotNumberNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _RelayNumberNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _RelayNumberNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _MemoryLocationNumberLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _MemoryLocationNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _AddMemoryLocationToListMenuItem As Windows.Forms.ToolStripMenuItem
End Class
