''' <summary> Encapsulates handling a device reported error. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class DeviceError

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DeviceError" /> class specifying no error.
    ''' </summary>
    Public Sub New()
        Me.New(VI.Pith.Scpi.Syntax.NoErrorCompoundMessage)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DeviceError" /> class specifying no error.
    ''' </summary>
    ''' <param name="noErrorCompoundMessage"> Message describing the no error compound. </param>
    Public Sub New(ByVal noErrorCompoundMessage As String)
        MyBase.New()
        Me._NoErrorCompoundMessage = noErrorCompoundMessage
        Me._CompoundErrorMessage = noErrorCompoundMessage
        Me._ErrorNumber = 0
        Me._ErrorLevel = 0
        Me._ErrorMessage = VI.Pith.Scpi.Syntax.NoErrorMessage
        Me._Severity = TraceEventType.Verbose
        Me._Timestamp = DateTimeOffset.Now
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="DeviceError" /> class. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As DeviceError)
        MyBase.New()
        If value Is Nothing Then
            Me._NoErrorCompoundMessage = VI.Pith.Scpi.Syntax.NoErrorCompoundMessage
            Me._CompoundErrorMessage = VI.Pith.Scpi.Syntax.NoErrorCompoundMessage
            Me._ErrorMessage = VI.Pith.Scpi.Syntax.NoErrorMessage
            Me._ErrorNumber = 0
        Else
            Me._NoErrorCompoundMessage = value.NoErrorCompoundMessage
            Me._CompoundErrorMessage = value.CompoundErrorMessage
            Me._ErrorMessage = value.ErrorMessage
            Me._ErrorNumber = value.ErrorNumber
            Me._ErrorLevel = value.ErrorLevel
            Me._Timestamp = value.Timestamp
        End If
    End Sub

    ''' <summary> Gets the no error. </summary>
    ''' <value> The no error. </value>
    Public Shared ReadOnly Property NoError As DeviceError
        Get
            Return New DeviceError(VI.Pith.Scpi.Syntax.NoErrorCompoundMessage)
        End Get
    End Property

#End Region

#Region " PARSE "

    ''' <summary> True if error level parsed. </summary>
    Private _ErrorLevelParsed As Boolean

    ''' <summary> Parses the error message. </summary>
    ''' <remarks>
    ''' TSP2 error: -285,TSP Syntax error at line 1: unexpected symbol near `*',level=1 TSP error: -
    ''' 285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20 SCPI Error: -113,
    ''' "Undefined header;1;2018/05/26 14:00:14.871".
    ''' </remarks>
    ''' <param name="compoundError"> The compound error. </param>
    Public Overridable Sub Parse(ByVal compoundError As String)
        If String.IsNullOrWhiteSpace(compoundError) Then
            Me.CompoundErrorMessage = String.Empty
            Me.ErrorNumber = 0
            Me.ErrorMessage = String.Empty
            Me.ErrorLevel = 0
            Me._Severity = TraceEventType.Verbose
            Me.Timestamp = DateTimeOffset.Now
        Else
            Me._CompoundErrorMessage = compoundError
            Dim parts As New Queue(Of String)(compoundError.Split(","c))
            If parts.Any Then
                If Integer.TryParse(parts.Dequeue, Me.ErrorNumber) Then
                    Me.Severity = If(Me.ErrorNumber < 0, TraceEventType.Error,
                        If(Me.ErrorNumber > 0, TraceEventType.Warning, TraceEventType.Verbose))
                Else
                    Me.ErrorNumber = Integer.MinValue
                    Me.Severity = TraceEventType.Error
                End If
                If parts.Any Then
                    Me.ErrorMessage = parts.Dequeue.Trim.Trim(""""c).Trim
                    If parts.Any Then
                        Me.ParseErrorLevel(parts.Dequeue())
                    End If
                Else
                    Me.ErrorMessage = String.Empty
                End If
            ElseIf Integer.TryParse(compoundError, Me.ErrorNumber) Then
                Me.ErrorMessage = compoundError
            Else
                Me.ErrorNumber = 0
                Me.ErrorMessage = compoundError
            End If
            Me.ParseErrorMessage(Me.ErrorMessage)
        End If
    End Sub

    ''' <summary> Parse error level. </summary>
    ''' <param name="message"> The message. </param>
    Private Sub ParseErrorLevel(ByVal message As String)
        Me._ErrorLevelParsed = False
        Dim levelDelimiter As Char = "="c
        Dim levelPrefix As String = "level"
        Dim parts As New Queue(Of String)(message.Split(levelDelimiter))
        If parts.Any Then
            If String.Equals(levelPrefix, parts.Dequeue) AndAlso parts.Any Then
                If Integer.TryParse(parts.Dequeue, Me.ErrorLevel) Then
                    Me._ErrorLevelParsed = True
                Else
                    Me.ErrorLevel = 0
                End If
            End If
        Else
            Me.ErrorLevel = 0
        End If
    End Sub

    ''' <summary> Parse error message. </summary>
    ''' <param name="message"> The message. </param>
    Private Sub ParseErrorMessage(ByVal message As String)
        Dim errorDelimiter As Char = ";"c
        If Not String.IsNullOrWhiteSpace(message) AndAlso message.Contains(errorDelimiter) Then
            Dim parts As New Queue(Of String)(message.Split(errorDelimiter))
            If parts.Any Then Me.ErrorMessage = parts.Dequeue
            Dim level As Integer = 0
            If parts.Any AndAlso Integer.TryParse(parts.Dequeue, level) AndAlso Not Me._ErrorLevelParsed Then
                Me.ErrorLevel = level
            End If
            If Not (parts.Any AndAlso DateTimeOffset.TryParse(parts.Dequeue, Me.Timestamp)) Then
                Me.Timestamp = DateTimeOffset.Now
            End If
        End If
    End Sub

#End Region

#Region " ERROR INFO "

    ''' <summary> Builds error message. </summary>
    ''' <returns> A String. </returns>
    Public Overridable Function BuildErrorMessage() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0},{1}", Me.ErrorNumber, Me.ErrorMessage)
    End Function

    ''' <summary> Message describing the no error compound. </summary>
    Private _NoErrorCompoundMessage As String

    ''' <summary> Gets or sets a message describing the no error compound message. </summary>
    ''' <value> A message describing the no error compound. </value>
    Public Property NoErrorCompoundMessage As String
        Get
            Return Me._NoErrorCompoundMessage
        End Get
        Protected Set(value As String)
            Me._NoErrorCompoundMessage = value
        End Set
    End Property

    ''' <summary> Gets a value indicating whether the error number represent and error. </summary>
    ''' <value> The is error. </value>
    Public ReadOnly Property IsError As Boolean
        Get
            Return Me.ErrorNumber <> 0
        End Get
    End Property

    ''' <summary> The error Level. </summary>
    Private _ErrorLevel As Integer

    ''' <summary> Gets or sets (protected) the error Level. </summary>
    ''' <value> The error Level. </value>
    Public Overridable Property ErrorLevel As Integer
        Get
            Return Me._ErrorLevel
        End Get
        Protected Set(ByVal value As Integer)
            Me._ErrorLevel = value
        End Set
    End Property

    ''' <summary> The error number. </summary>
    Private _ErrorNumber As Integer

    ''' <summary> Gets or sets (protected) the error number. </summary>
    ''' <value> The error number. </value>
    Public Property ErrorNumber As Integer
        Get
            Return Me._ErrorNumber
        End Get
        Protected Set(ByVal value As Integer)
            Me._ErrorNumber = value
        End Set
    End Property

    ''' <summary> Message describing the error. </summary>
    Private _ErrorMessage As String

    ''' <summary> Gets or sets (protected) the error message. </summary>
    ''' <value> A message describing the error. </value>
    Public Property ErrorMessage As String
        Get
            Return Me._ErrorMessage
        End Get
        Protected Set(ByVal value As String)
            Me._ErrorMessage = value
        End Set
    End Property

    ''' <summary> Message describing the compound error. </summary>
    Private _CompoundErrorMessage As String

    ''' <summary> Gets or sets (protected) the compound error message. </summary>
    ''' <value> A message describing the compound error. </value>
    Public Property CompoundErrorMessage As String
        Get
            Return Me._CompoundErrorMessage
        End Get
        Protected Set(ByVal value As String)
            Me._CompoundErrorMessage = value
        End Set
    End Property

    ''' <summary> The severity. </summary>
    Private _Severity As TraceEventType

    ''' <summary> Gets or sets the severity. </summary>
    ''' <value> The severity. </value>
    Public Property Severity As TraceEventType
        Get
            Return Me._Severity
        End Get
        Protected Set(value As TraceEventType)
            Me._Severity = value
        End Set
    End Property

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return Me.CompoundErrorMessage
    End Function

    ''' <summary> The error Level. </summary>
    Private _Timestamp As DateTimeOffset

    ''' <summary> Gets or sets the timestamp. </summary>
    ''' <value> The timestamp. </value>
    Public Property Timestamp As DateTimeOffset
        Get
            Return Me._Timestamp
        End Get
        Protected Set(ByVal value As DateTimeOffset)
            Me._Timestamp = value
        End Set
    End Property

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Indicates whether the current <see cref="T:DeviceError"></see> value is equal to a specified
    ''' object.
    ''' </summary>
    ''' <param name="obj"> An object. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, DeviceError))
    End Function

    ''' <summary>
    ''' Indicates whether the current <see cref="T:DeviceError"></see> value is equal to a specified
    ''' object.
    ''' </summary>
    ''' <remarks>
    ''' The two Parameters are the same if they have the same actual and cached values.
    ''' </remarks>
    ''' <param name="value"> The value to compare. </param>
    ''' <returns>
    ''' <c>True</c> if the other parameter is equal to the current
    ''' <see cref="T:DeviceError"></see> value;
    ''' otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Function Equals(ByVal value As DeviceError) As Boolean
        Return value IsNot Nothing AndAlso Me.CompoundErrorMessage = value.CompoundErrorMessage
    End Function

    ''' <summary> Returns a hash code for this instance. </summary>
    ''' <returns> A hash code for this object. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return MyBase.GetHashCode
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As DeviceError, ByVal right As DeviceError) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As DeviceError, ByVal right As DeviceError) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

#End Region

End Class

''' <summary> Queue of device errors. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-01-12 </para>
''' </remarks>
Public Class DeviceErrorQueue
    Inherits Queue(Of DeviceError)

    ''' <summary> Constructor. </summary>
    ''' <param name="noErrorCompoundMessage"> A message describing the empty error message. </param>
    Public Sub New(ByVal noErrorCompoundMessage As String)
        MyBase.New
        Me._NoErrorCompoundMessage = noErrorCompoundMessage
    End Sub

    ''' <summary> Gets a message describing the no error compound message. </summary>
    ''' <value> A message describing the no error compound. </value>
    Public ReadOnly Property NoErrorCompoundMessage As String

    ''' <summary> Gets the last error. </summary>
    ''' <value> The last error. </value>
    Public ReadOnly Property LastError As DeviceError
        Get
            Return If(Me.Count = 0, New DeviceError(Me.NoErrorCompoundMessage), Me(Me.Count - 1))
        End Get
    End Property

End Class
