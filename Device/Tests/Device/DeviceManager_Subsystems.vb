'---------------------------------------------------------------------------------------------------
' file:		Tests\Device\DeviceManager_Subsystems.vb
'
' summary:	Device manager subsystems class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

Partial Public NotInheritable Class DeviceManager

#Region " SESSION "

    ''' <summary> Assert session initial values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">        The session. </param>
    ''' <param name="resourceInfo">   Information describing the resource. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertSessionInitialValues(ByVal session As VI.Pith.SessionBase,
                                   ByVal resourceInfo As ResourceSettingsBase, subsystemsInfo As SubsystemsSettingsBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))
        If resourceInfo Is Nothing Then Throw New ArgumentNullException(NameOf(resourceInfo))
        If Not resourceInfo.ResourcePinged Then Assert.Inconclusive($"{resourceInfo.ResourceTitle} not found")
        Dim propertyName As String = NameOf(VI.Pith.ServiceRequests.ErrorAvailable).SplitWords
        Dim expectedErrorAvailableBits As Integer = VI.Pith.ServiceRequests.ErrorAvailable
        Dim actualErrorAvailableBits As Integer = session.ErrorAvailableBit
        Assert.AreEqual(expectedErrorAvailableBits, actualErrorAvailableBits, $"{resourceInfo.ResourceTitle} {propertyName} bits on creating device should match")
    End Sub

    ''' <summary> Assert session open values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">      The session. </param>
    ''' <param name="resourceInfo"> Information describing the resource. </param>
    Public Shared Sub AssertSessionOpenValues(ByVal session As VI.Pith.SessionBase, ByVal resourceInfo As ResourceSettingsBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If resourceInfo Is Nothing Then Throw New ArgumentNullException(NameOf(resourceInfo))
        Dim propertyName As String = NameOf(VI.Pith.SessionBase.ValidatedResourceName).SplitWords
        Dim actualResource As String = session.ValidatedResourceName
        Dim expectedResource As String = resourceInfo.ResourceName
        Assert.AreEqual(expectedResource, actualResource, $"{resourceInfo.ResourceTitle} {propertyName} should match")

        propertyName = NameOf(VI.Pith.SessionBase.CandidateResourceName).SplitWords
        actualResource = session.CandidateResourceName
        expectedResource = resourceInfo.ResourceName
        Assert.AreEqual(expectedResource, actualResource, $"{resourceInfo.ResourceTitle} {propertyName} should match")

        propertyName = NameOf(VI.Pith.SessionBase.OpenResourceName).SplitWords
        actualResource = session.OpenResourceName
        expectedResource = resourceInfo.ResourceName
        Assert.AreEqual(expectedResource, actualResource, $"{resourceInfo.ResourceTitle} {propertyName} should match")

        propertyName = NameOf(VI.Pith.SessionBase.OpenResourceTitle).SplitWords
        Dim actualTitle As String = session.OpenResourceTitle
        Dim expectedTitle As String = resourceInfo.ResourceTitle
        Assert.AreEqual(expectedTitle, actualTitle, $"{resourceInfo.ResourceTitle} {propertyName} should match")

        propertyName = NameOf(VI.Pith.SessionBase.CandidateResourceTitle).SplitWords
        actualTitle = session.CandidateResourceTitle
        expectedTitle = resourceInfo.ResourceTitle
        Assert.AreEqual(expectedTitle, actualTitle, $"{resourceInfo.ResourceTitle} {propertyName} should match")
    End Sub

    ''' <summary> Assert termination values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">        The session. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertTerminationValues(ByVal session As VI.Pith.SessionBase, subsystemsInfo As SubsystemsSettingsBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))

        Dim propertyName As String = NameOf(VI.Pith.ServiceRequests.ErrorAvailable).SplitWords

        ' read termination is enabled from the status subsystem. It is disabled at the IVI Visa level. 
        Dim actualReadTerminationEnabled As Boolean = session.ReadTerminationCharacterEnabled
        Dim expectedReadTerminationEnabled As Boolean = subsystemsInfo.InitialReadTerminationEnabled
        propertyName = NameOf(VI.Pith.SessionBase.ReadTerminationCharacterEnabled).SplitWords
        Assert.AreEqual(expectedReadTerminationEnabled, actualReadTerminationEnabled, $"{session.ResourceNameCaption} initial {propertyName} should match")

        propertyName = NameOf(VI.Pith.SessionBase.ReadTerminationCharacter).SplitWords
        Dim actualTermination As Integer = session.ReadTerminationCharacter
        Dim expectedTermination As Integer = subsystemsInfo.InitialReadTerminationCharacter
        Assert.AreEqual(expectedTermination, actualTermination, $"{session.ResourceNameCaption} initial {propertyName} value should match")

        propertyName = NameOf(VI.Pith.SessionBase.ReadTerminationCharacterEnabled).SplitWords
        expectedReadTerminationEnabled = Not subsystemsInfo.ReadTerminationEnabled
        session.ReadTerminationCharacterEnabled = expectedReadTerminationEnabled
        actualReadTerminationEnabled = session.ReadTerminationCharacterEnabled
        Assert.AreEqual(expectedReadTerminationEnabled, actualReadTerminationEnabled, $"{session.ResourceNameCaption} toggled {propertyName} should match")

        expectedReadTerminationEnabled = subsystemsInfo.ReadTerminationEnabled
        session.ReadTerminationCharacterEnabled = expectedReadTerminationEnabled
        actualReadTerminationEnabled = session.ReadTerminationCharacterEnabled
        Assert.AreEqual(expectedReadTerminationEnabled, actualReadTerminationEnabled, $"{session.ResourceNameCaption} restored {propertyName} should match")

        propertyName = NameOf(VI.Pith.SessionBase.ReadTerminationCharacter).SplitWords
        actualTermination = session.ReadTerminationCharacter
        expectedTermination = subsystemsInfo.ReadTerminationCharacter
        Assert.AreEqual(expectedTermination, actualTermination, $"{session.ResourceNameCaption} {propertyName} value should match")
        Assert.AreEqual(CByte(AscW(session.Termination(0))), session.ReadTerminationCharacter,
                        $"{session.ResourceNameCaption} first termination character value should match")
    End Sub

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Assert device errors. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertDeviceErrors(ByVal subsystem As VI.StatusSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))

        Dim propertyName As String = NameOf(VI.Pith.ServiceRequests.ErrorAvailable).SplitWords
        Assert.IsFalse(subsystem.Session.IsErrorBitSet, $"{subsystem.ResourceNameCaption} {propertyName} bit {subsystem.Session.ServiceRequestStatus:X} should be off")
        Assert.IsFalse(subsystem.ErrorAvailable, $"{subsystem.ResourceNameCaption} error available bit {subsystem.Session.ServiceRequestStatus:X} is on; last device error: {subsystem.CompoundErrorMessage}")

        propertyName = NameOf(VI.Pith.ServiceRequests.ErrorAvailable).SplitWords
        Dim errors As String = subsystem.DeviceErrorReport
        Assert.IsTrue(String.IsNullOrWhiteSpace(errors), $"{subsystem.ResourceNameCaption} device errors: {errors}")
        Assert.IsFalse(subsystem.HasDeviceError, $"{subsystem.ResourceNameCaption} device error report: {subsystem.DeviceErrorReport}")

        Dim deviceError As New VI.DeviceError()
        deviceError.Parse(subsystemsInfo.ParseCompoundErrorMessage)

        propertyName = NameOf(VI.DeviceError.ErrorMessage).SplitWords
        Assert.AreEqual(subsystemsInfo.ParseErrorMessage, deviceError.ErrorMessage, $"{subsystem.ResourceNameCaption} parsed {propertyName} should match")

        propertyName = NameOf(VI.DeviceError.ErrorNumber).SplitWords
        Assert.AreEqual(subsystemsInfo.ParseErrorNumber, deviceError.ErrorNumber, $"{subsystem.ResourceNameCaption} parsed {propertyName} should match")

        propertyName = NameOf(VI.DeviceError.ErrorLevel).SplitWords
        Assert.AreEqual(subsystemsInfo.ParseErrorLevel, deviceError.ErrorLevel, $"{subsystem.ResourceNameCaption} parsed {propertyName} should match")

    End Sub

    ''' <summary> Assert clear session device errors. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device">         The device. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertClearSessionDeviceErrors(ByVal device As VI.VisaSessionBase, subsystemsInfo As SubsystemsSettingsBase)
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        device.Session.ClearActiveState()
        DeviceManager.AssertDeviceErrors(device.StatusSubsystemBase, subsystemsInfo)
    End Sub

    ''' <summary> Assert reading device errors. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device">         The device. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertReadingDeviceErrors(ByVal device As VI.VisaSessionBase, subsystemsInfo As SubsystemsSettingsBase)
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))

        Dim propertyName As String = NameOf(VI.Pith.ServiceRequests.ErrorAvailable).SplitWords

        ' send an erroneous command
        Dim erroneousCommand As String = subsystemsInfo.ErroneousCommand
        device.StatusSubsystemBase.Write(erroneousCommand)

        Dim appliedDelay As TimeSpan = isr.Core.ApplianceBase.DoEventsWaitElapsed(TimeSpan.FromMilliseconds(subsystemsInfo.ErrorAvailableMillisecondsDelay))

        ' read the service request status; this should generate an error available 
        Dim value As Integer = device.Session.ReadStatusRegister()

        ' check the error bits
        Dim actualServiceRequest As VI.Pith.ServiceRequests = device.StatusSubsystemBase.ErrorAvailableBit
        Dim expectedSeriveRequest As VI.Pith.ServiceRequests = VI.Pith.ServiceRequests.ErrorAvailable
        Assert.AreEqual(expectedSeriveRequest, actualServiceRequest,
                        $"{device.ResourceNameCaption} error bits expected {expectedSeriveRequest:X} <> actual {actualServiceRequest:X}")

        ' check the error available status
        Assert.IsTrue((value And expectedSeriveRequest) = expectedSeriveRequest,
                      $"{device.ResourceNameCaption} error bits {expectedSeriveRequest:X} are expected in value: {value:X}; applied {appliedDelay:s\.fff}s delay")

        ' check the error available status
        actualServiceRequest = device.Session.ServiceRequestStatus
        Assert.IsTrue((actualServiceRequest And expectedSeriveRequest) = expectedSeriveRequest,
                      $"{device.ResourceNameCaption} error bits {expectedSeriveRequest:X} are expected in {NameOf(VI.Pith.SessionBase.ServiceRequestStatus)}: {actualServiceRequest:X}")

        Dim actualErrorAvailable As Boolean = device.StatusSubsystemBase.ErrorAvailable
        Assert.IsTrue(actualErrorAvailable, $"{device.ResourceNameCaption} an error is expected")

        propertyName = NameOf(VI.DeviceError.ErrorMessage).SplitWords
        Assert.AreEqual(subsystemsInfo.ExpectedErrorMessage, device.StatusSubsystemBase.DeviceErrorQueue.LastError.ErrorMessage,
                        $"{device.ResourceNameCaption} {propertyName} should match")

        propertyName = NameOf(VI.DeviceError.ErrorNumber).SplitWords
        Assert.AreEqual(subsystemsInfo.ExpectedErrorNumber, device.StatusSubsystemBase.DeviceErrorQueue.LastError.ErrorNumber,
                        $"{device.ResourceNameCaption} {propertyName} should match")

        propertyName = NameOf(VI.DeviceError.ErrorLevel).SplitWords
        Assert.AreEqual(subsystemsInfo.ExpectedErrorLevel, device.StatusSubsystemBase.DeviceErrorQueue.LastError.ErrorLevel,
                        $"{device.ResourceNameCaption} {propertyName} should match")

    End Sub

#End Region

#Region " CHANNEL SUBSYSTEM "

    ''' <summary> Assert initial subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As VI.ChannelSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Assert.IsTrue(String.IsNullOrWhiteSpace(subsystem.ClosedChannels), $"Scan list {subsystem.ClosedChannels}; expected empty")
    End Sub

#End Region

#Region " MEASURE SUBSYSTEM "

    ''' <summary> Assert initial subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As VI.MeasureSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM "

    ''' <summary> Assert initial subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As MultimeterSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))

        Dim propertyName As String = String.Empty
        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}"
        Dim expectedPowerLineCycles As Double = subsystemsInfo.InitialPowerLineCycles
        Dim actualPowerLineCycles As Double? = subsystem.QueryPowerLineCycles.GetValueOrDefault(0)
        Assert.IsTrue(actualPowerLineCycles.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Dim epsilon As Double = subsystemsInfo.LineFrequency / TimeSpan.TicksPerSecond
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles.Value, epsilon, $"{subsystem.ResourceNameCaption} {propertyName} value should match expect value withing {epsilon}")

        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoRangeEnabled)}"
        Dim expectedBoolean As Boolean = subsystemsInfo.InitialAutoRangeEnabled
        Dim actualBoolean As Boolean? = subsystem.QueryAutoRangeEnabled.GetValueOrDefault(False)
        Assert.IsTrue(actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match")

        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}"
        expectedBoolean = subsystemsInfo.InitialAutoZeroEnabled
        actualBoolean = subsystem.QueryAutoZeroEnabled.GetValueOrDefault(False)
        Assert.IsTrue(actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match")

        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FunctionMode)}"
        Dim expectedFunctionMode As VI.MultimeterFunctionModes = subsystemsInfo.InitialMultimeterFunction
        Dim actualFunctionMode? As VI.MultimeterFunctionModes = subsystem.QueryFunctionMode
        Assert.IsTrue(actualFunctionMode.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"{subsystem.ResourceNameCaption} {propertyName} should match")

        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}"
        expectedBoolean = subsystemsInfo.InitialFilterEnabled
        actualBoolean = subsystem.QueryFilterEnabled
        Assert.IsTrue(actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match")

        propertyName = $"initial {propertyName}reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}"
        expectedBoolean = subsystemsInfo.InitialMovingAverageFilterEnabled
        actualBoolean = subsystem.QueryMovingAverageFilterEnabled
        Assert.IsTrue(actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match")

        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}"
        Dim expectedFilterWindow As Double = subsystemsInfo.InitialFilterWindow
        Dim actualFilterWindow As Double? = subsystem.QueryFilterWindow
        Assert.IsTrue(actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedFilterWindow, actualFilterWindow.Value, 0.1 * expectedFilterWindow,
                        $"{subsystem.ResourceNameCaption} {propertyName} should match withing {epsilon}")

        propertyName = $"initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}"
        Dim expectedFilterCount As Integer = subsystemsInfo.InitialFilterCount
        Dim actualFilterCount As Integer? = subsystem.QueryFilterCount
        Assert.IsTrue(actualFilterCount.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(expectedFilterCount, actualFilterCount.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match")

    End Sub

#End Region

#Region " OUTPUT SUBSYSTEM "

    ''' <summary> Assert digital output signal polarity. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="digitalOutputLineNumber"> The digital output line number. </param>
    ''' <param name="subsystem">               The subsystem. </param>
    Public Shared Sub AssertDigitalOutputSignalPolarity(ByVal digitalOutputLineNumber As Integer, ByVal subsystem As VI.DigitalOutputSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))

        Dim propertyName As String = String.Empty
        Dim deviceName As String = subsystem.ResourceNameCaption
        propertyName = $"{GetType(VI.DigitalOutputSubsystemBase)}.{NameOf(VI.DigitalOutputSubsystemBase.CurrentDigitalActiveLevel)}"
        Dim activity As String = $"Reading [{deviceName}].[{propertyName}({digitalOutputLineNumber})] initial value"
        Dim initialPolarity As VI.DigitalActiveLevels? = subsystem.QueryDigitalActiveLevel(digitalOutputLineNumber)
        Assert.IsTrue(initialPolarity.HasValue, $"{activity} should have a value")

        Dim expectedPolarity As VI.DigitalActiveLevels = If(initialPolarity.Value = DigitalActiveLevels.High, DigitalActiveLevels.Low, DigitalActiveLevels.High)
        activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}"
        Dim actualPolarity As VI.DigitalActiveLevels? = subsystem.ApplyDigitalActiveLevel(digitalOutputLineNumber, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

        expectedPolarity = initialPolarity.Value
        activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}"
        actualPolarity = subsystem.ApplyDigitalActiveLevel(digitalOutputLineNumber, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

    End Sub

    ''' <summary> Assert digital output signal polarity. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub AssertDigitalOutputSignalPolarity(ByVal subsystem As VI.DigitalOutputSubsystemBase)

        Dim initialPolarities As New List(Of DigitalActiveLevels)
        Dim inputLineNumbers As New List(Of Integer)(New Integer() {1, 2, 3, 4})
        Dim inputLineNumbersCaption As New System.Text.StringBuilder
        Dim inputLineNumber As Integer
        For Each inputLineNumber In inputLineNumbers
            If inputLineNumbersCaption.Length > 0 Then inputLineNumbersCaption.Append(",")
            inputLineNumbersCaption.Append($"{inputLineNumber}")
            initialPolarities.Add(subsystem.QueryDigitalActiveLevel(inputLineNumber).GetValueOrDefault(DigitalActiveLevels.Low))
        Next

        For Each inputLineNumber In inputLineNumbers
            isr.VI.DeviceTests.DeviceManager.AssertDigitalOutputSignalPolarity(inputLineNumber, subsystem)
        Next

        inputLineNumber = inputLineNumbers(0)
        Dim propertyName As String = $"{GetType(VI.DigitalOutputSubsystemBase)}.{NameOf(VI.DigitalOutputSubsystemBase.CurrentDigitalActiveLevel)}"
        Dim deviceName As String = subsystem.ResourceNameCaption
        Dim activity As String = $"Reading [{deviceName}].[{propertyName}({inputLineNumber})] initial value"
        Dim initialPolarity As VI.DigitalActiveLevels? = subsystem.QueryDigitalActiveLevel(inputLineNumber)
        Assert.IsTrue(initialPolarity.HasValue, $"{activity} should have a value")

        Dim expectedPolarity As VI.DigitalActiveLevels = If(initialPolarity.Value = DigitalActiveLevels.High, DigitalActiveLevels.Low, DigitalActiveLevels.High)
        activity = $"Setting [{deviceName}].[{propertyName}({inputLineNumbersCaption})] to {expectedPolarity}"
        Dim actualPolarity As VI.DigitalActiveLevels? = subsystem.ApplyDigitalActiveLevel(inputLineNumbers, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

        expectedPolarity = initialPolarity.Value
        activity = $"Setting [{deviceName}].[{propertyName}({inputLineNumbersCaption})] to {expectedPolarity}"
        actualPolarity = subsystem.ApplyDigitalActiveLevel(inputLineNumbers, expectedPolarity)
        Assert.IsTrue(actualPolarity.HasValue, $"{activity} should have a value")
        Assert.AreEqual(expectedPolarity, actualPolarity, $"{activity} should equal actual value")

        activity = $"Applying initial [{deviceName}].[{propertyName}s] to lines {inputLineNumbersCaption}"
        Dim actualPolarities As New List(Of DigitalActiveLevels?)(subsystem.ApplyDigitalActiveLevels(inputLineNumbers, initialPolarities))
        For Each inputLineNumber In inputLineNumbers
            Assert.AreEqual(initialPolarities(inputLineNumber - 1), actualPolarities(inputLineNumber - 1).Value, $"[{deviceName}].[{propertyName}({inputLineNumber})] should equal actual value")
        Next

    End Sub

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Assert initial subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As VI.RouteSubsystemBase, ByVal subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))

        Dim propertyName As String = NameOf(VI.RouteSubsystemBase.ClosedChannels).SplitWords
        Dim expectedClosedChannels As String = subsystemsInfo.InitialClosedChannels
        If subsystem.SupportsClosedChannelsQuery Then
            Dim actualClosedChannels As String = subsystem.QueryClosedChannels
            Assert.AreEqual(expectedClosedChannels, actualClosedChannels, $"{subsystem.ResourceNameCaption} initial {propertyName} should be expected")
        End If
        If Not subsystem.ScanListPersists Then
            propertyName = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords
            Dim expectedScanList As String = subsystemsInfo.InitialScanList
            Dim actualScanList As String = subsystem.QueryScanList
            Assert.AreEqual(expectedScanList, actualScanList, $"{subsystem.ResourceNameCaption} initial {propertyName} should be expected")
        End If
    End Sub

    ''' <summary> Assert slot card information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="cardNumber">   The card number. </param>
    ''' <param name="expectedName"> Name of the expected. </param>
    Public Shared Sub AssertSlotCardInfo(ByVal subsystem As VI.RouteSubsystemBase, ByVal cardNumber As Integer, ByVal expectedName As String)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))

        Dim propertyName As String = NameOf(VI.RouteSubsystemBase.SlotCardType).SplitWords
        Dim actualName As String = subsystem.QuerySlotCardType(cardNumber)
        Assert.AreEqual(expectedName, actualName, $"{subsystem.ResourceNameCaption} slot card #{cardNumber} {propertyName} should be expected")

        propertyName = NameOf(VI.RouteSubsystemBase.SlotCardSettlingTime).SplitWords
        Dim existingSettlingTime As TimeSpan = subsystem.QuerySlotCardSettlingTime(cardNumber)
        Dim expectedSettlingTime As TimeSpan = TimeSpan.FromMilliseconds(11)
        Dim actualSettlingTime As TimeSpan = subsystem.ApplySlotCardSettlingTime(cardNumber, expectedSettlingTime)
        Assert.AreEqual(expectedSettlingTime, actualSettlingTime, $"{subsystem.ResourceNameCaption} slot card #{cardNumber} {propertyName} should be expected")

        actualSettlingTime = subsystem.ApplySlotCardSettlingTime(cardNumber, existingSettlingTime)
        Assert.AreEqual(existingSettlingTime, actualSettlingTime, $"{subsystem.ResourceNameCaption} slot card #{cardNumber} restored {propertyName} should be expected")

    End Sub

    ''' <summary> Assert scan card installed. </summary>
    ''' <remarks> David, 2020-04-04. </remarks>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertScanCardInstalled(ByVal subsystem As SystemSubsystemBase, ByVal subsystemsInfo As VI.DeviceTests.SubsystemsSettingsBase)
        Assert.AreEqual(subsystemsInfo.ScanCardInstalled, subsystem.InstalledScanCards.Any,
                        $"Scan card should {If(subsystemsInfo.ScanCardInstalled, String.Empty, "not")} be installed")
    End Sub

    ''' <summary> Assert scan card internal scan. </summary>
    ''' <remarks> David, 2020-04-04. </remarks>
    ''' <param name="routeSubsystem">  The route subsystem. </param>
    ''' <param name="systemSubsystem"> The system subsystem. </param>
    ''' <param name="subsystemsInfo">  Information describing the subsystems. </param>
    Public Shared Sub AssertScanCardInternalScan(ByVal routeSubsystem As VI.RouteSubsystemBase, ByVal systemSubsystem As VI.SystemSubsystemBase,
                                                 ByVal subsystemsInfo As VI.DeviceTests.SubsystemsSettingsBase)
        If Not systemSubsystem.InstalledScanCards.Any Then Return

        Dim expectedScanList As String = subsystemsInfo.InitialScanList
        Dim actualScanList As String = routeSubsystem.QueryScanList
        Assert.AreEqual(expectedScanList, actualScanList, $"Scan list should be '{expectedScanList}'")

        expectedScanList = "(@1,2)"
        actualScanList = routeSubsystem.ApplyScanList(expectedScanList)
        Assert.AreEqual(expectedScanList, actualScanList, $"Scan list should be '{expectedScanList}'")

        Dim expectedScanlistType As ScanListType = ScanListType.None
        Dim expectedSelectedScanlistType As String = routeSubsystem.ScanListTypeReadWrites.SelectItem(expectedScanlistType).ReadValue
        Dim actualSelectedScanlistType As String = routeSubsystem.ApplySelectedScanListType(expectedSelectedScanlistType)
        Dim actualScanListType As ScanListType = routeSubsystem.ScanListType
        Assert.AreEqual(expectedSelectedScanlistType, actualSelectedScanlistType, $"Scan list type should be '{expectedSelectedScanlistType}'")
        Assert.AreEqual(expectedScanlistType, actualScanListType, $"Scan list type should be '{expectedScanlistType}'")

        expectedScanlistType = ScanListType.Internal
        actualScanListType = routeSubsystem.ApplySelectedScanListType(expectedScanlistType)
        Assert.AreEqual(expectedScanlistType, actualScanListType, $"Applied scan list type should be '{expectedScanlistType}'")

        expectedScanlistType = ScanListType.None
        actualScanListType = routeSubsystem.ApplySelectedScanListType(expectedScanlistType)
        Assert.AreEqual(expectedScanlistType, actualScanListType, $"Cleared scan list type should be '{expectedScanlistType}'")

        expectedScanList = "(@1,2)"
        actualScanList = routeSubsystem.QueryScanList
        Assert.AreEqual(expectedScanList, actualScanList, $"Scan list should be '{expectedScanList}'")

        '  Dim caldidateScanListType As String = routeSubsystem.ScanListTypeReadWrites.SelectItem(expectedScanlistType).WriteValue
        '  expectedSelectedScanlistType = "INT"
        '  actualScanList = routeSubsystem.QueryScanList
        '  Assert.AreEqual(expectedSelectedScanlistType, actualSelectedScanlistType, $"Scan list type should be '{expectedSelectedScanlistType}'")
        '  expectedSelectedScanlistType = "NONE"
        '  actualSelectedScanlistType = routeSubsystem.ApplySelectedScanListType(expectedSelectedScanlistType)
        '  Assert.AreEqual(expectedSelectedScanlistType, actualSelectedScanlistType, $"Scan list type should be '{expectedSelectedScanlistType}'")
    End Sub


#End Region

#Region " SENSE SUBSYSTEM "

    ''' <summary> Assert initial sense subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As VI.SenseSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))

        Dim propertyName As String = String.Empty
        Dim deviceName As String = subsystem.ResourceNameCaption

        propertyName = $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.PowerLineCycles)}"
        Dim expectedPowerLineCycles As Double = subsystemsInfo.InitialPowerLineCycles
        Dim actualPowerLineCycles As Double = subsystem.QueryPowerLineCycles.GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, subsystemsInfo.LineFrequency / TimeSpan.TicksPerSecond,
                        $"[{deviceName}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.AutoRangeEnabled)}"
        Dim expectedBoolean As Boolean = subsystemsInfo.InitialAutoRangeEnabled
        Dim actualBoolean As Boolean = subsystem.QueryAutoRangeEnabled.GetValueOrDefault(False)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{deviceName}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.FunctionMode)}"
        Dim actualFunctionMode As VI.SenseFunctionModes = subsystem.QueryFunctionMode.GetValueOrDefault(VI.SenseFunctionModes.Resistance)
        Dim expectedFunctionMode As VI.SenseFunctionModes = subsystemsInfo.InitialSenseFunction
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"[{deviceName}].[{propertyName}] should equal expected value")

    End Sub

    ''' <summary> Toggle sense subsystem function mode. </summary>
    ''' <remarks> David, 2020-07-29. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="toggleFirst">  The toggle first. </param>
    ''' <param name="toggleSecond"> The toggle second. </param>
    Public Shared Sub ToggleFunctionMode(ByVal subsystem As VI.SenseSubsystemBase, ByVal toggleFirst As SenseFunctionModes, ByVal toggleSecond As SenseFunctionModes)

        Dim initialFunctionMode As VI.SenseFunctionModes = subsystem.QueryFunctionMode.GetValueOrDefault(VI.SenseFunctionModes.None)
        Dim expectedFunctionMode = If(initialFunctionMode = toggleFirst, toggleSecond, toggleFirst)
        Dim actualFunctionMode As VI.SenseFunctionModes = subsystem.ApplyFunctionMode(expectedFunctionMode).GetValueOrDefault(VI.SenseFunctionModes.None)
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.FunctionMode)} should be toggled")

        ' restore function mode
        actualFunctionMode = subsystem.ApplyFunctionMode(initialFunctionMode).GetValueOrDefault(VI.SenseFunctionModes.None)
        Assert.AreEqual(initialFunctionMode, actualFunctionMode, $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.FunctionMode)} should be restored")

    End Sub

#End Region

#Region " SOURCE SUBSYSTEM "

    ''' <summary> Assert initial subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As VI.SourceSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))
    End Sub

    ''' <summary> Assert toggle output. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">     The subsystem. </param>
    ''' <param name="outputEnabled"> True to enable, false to disable the output. </param>
    Public Shared Sub AssertToggleOutput(ByVal subsystem As VI.SourceSubsystemBase, ByVal outputEnabled As Boolean)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Dim expectedOutputEnabled As Boolean = outputEnabled
        Dim actualOutputEnabled As Boolean = subsystem.ApplyOutputEnabled(expectedOutputEnabled).GetValueOrDefault(Not expectedOutputEnabled)
        Assert.AreEqual(expectedOutputEnabled, actualOutputEnabled, $"{GetType(VI.SourceSubsystemBase)}.{NameOf(VI.SourceSubsystemBase.OutputEnabled)} is {actualOutputEnabled}; expected {expectedOutputEnabled}")
    End Sub

#End Region

#Region " STATUS SUBSYSTEM "

    ''' <summary> Assert line frequency. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertLineFrequency(ByVal subsystem As VI.StatusSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))
        Dim propertyName As String = NameOf(VI.StatusSubsystemBase.LineFrequency).SplitWords
        Dim actualFrequency As Double = subsystem.LineFrequency.GetValueOrDefault(0)
        Assert.AreEqual(subsystemsInfo.LineFrequency, actualFrequency, $"{subsystem.ResourceNameCaption} {propertyName} should match")
    End Sub

    ''' <summary> Assert integration period. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertIntegrationPeriod(ByVal subsystem As VI.StatusSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))
        Dim propertyName As String = "Integration Period"
        Dim expectedPowerLineCycles As Double = subsystemsInfo.InitialPowerLineCycles
        Dim expectedIntegrationPeriod As TimeSpan = VI.StatusSubsystemBase.FromSecondsPrecise(expectedPowerLineCycles / subsystemsInfo.LineFrequency)
        Dim actualIntegrationPeriod As TimeSpan = VI.StatusSubsystemBase.FromPowerLineCycles(expectedPowerLineCycles)
        Assert.AreEqual(expectedIntegrationPeriod, actualIntegrationPeriod,
                                    $"{propertyName} for {expectedPowerLineCycles} power line cycles is {actualIntegrationPeriod}; expected {expectedIntegrationPeriod}")

        propertyName = "Power line cycles"
        Dim actualPowerLineCycles As Double = VI.StatusSubsystemBase.ToPowerLineCycles(actualIntegrationPeriod)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, subsystemsInfo.LineFrequency / TimeSpan.TicksPerSecond,
                                    $"{propertyName} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}")
    End Sub

    ''' <summary> Assert device model. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="resourceInfo"> Information describing the resource. </param>
    Public Shared Sub AssertDeviceModel(ByVal subsystem As VI.StatusSubsystemBase, ByVal resourceInfo As ResourceSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If resourceInfo Is Nothing Then Throw New ArgumentNullException(NameOf(resourceInfo))
        Dim propertyName As String = NameOf(VersionInfoBase.Model).SplitWords
        Assert.AreEqual(resourceInfo.ResourceModel, subsystem.VersionInfoBase.Model,
                        $"Version Info {propertyName} {subsystem.ResourceNameCaption} Identity: '{subsystem.VersionInfoBase.Identity}'", Globalization.CultureInfo.CurrentCulture)
    End Sub

    ''' <summary> Assert orphan messages. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub AssertOrphanMessages(ByVal subsystem As VI.StatusSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Dim orphanMessages As String = subsystem.OrphanMessages
        Assert.IsTrue(String.IsNullOrWhiteSpace(orphanMessages), $"{subsystem.ResourceNameCaption} orphan messages {orphanMessages} should be empty")
    End Sub

#End Region

#Region " TRIGGER SUBSYSTEM "

    ''' <summary> Assert initial subsystem values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">      The subsystem. </param>
    ''' <param name="subsystemsInfo"> Information describing the subsystems. </param>
    Public Shared Sub AssertInitialSubsystemValues(ByVal subsystem As VI.TriggerSubsystemBase, subsystemsInfo As SubsystemsSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        If subsystemsInfo Is Nothing Then Throw New ArgumentNullException(NameOf(subsystemsInfo))
        Dim propertyName As String = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords
        Dim expectedTriggerSource As VI.TriggerSources = subsystemsInfo.InitialTriggerSource
        Dim actualTriggerSource As VI.TriggerSources? = subsystem.QueryTriggerSource
        Assert.IsTrue(actualTriggerSource.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value")
        Assert.AreEqual(Of VI.TriggerSources)(expectedTriggerSource, actualTriggerSource.Value, $"{subsystem.ResourceNameCaption} initial {propertyName} should be expected")

    End Sub

#End Region

#Region " SERVICE REQUEST POLLING "

    ''' <summary> Assert device polling. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    Public Shared Sub AssertDevicePolling(ByVal device As VI.VisaSessionBase)

        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Assert.IsTrue(device.IsDeviceOpen, "session should be open")

        ' ensure service request handling is disabled.
        device.Session.DisableServiceRequestEventHandler()
        ' must refresh the service request here to update the status register flags.
        device.Session.ReadStatusRegister()
        Assert.IsFalse(device.Session.ServiceRequestEventEnabled, $"{NameOf(Pith.SessionBase.ServiceRequestEventEnabled).SplitWords} should be disabled")

        device.Session.ApplyServiceRequestEnableBitmask(device.Session.DefaultServiceRequestEnableBitmask)
        Assert.IsFalse(device.Session.ServiceRequestEventEnabled, "service request event should not be enabled")

        Dim expectedServiceRequestEnableCommand As String = String.Format($"*SRE {CInt(device.Session.ServiceRequestEnabledBitmask)}")
        Assert.AreEqual(expectedServiceRequestEnableCommand, device.Session.ServiceRequestEnableCommand,
                            $"{NameOf(Pith.SessionBase.ServiceRequestEnableCommand).SplitWords} should be as expected")

        Try

            device.PollTimespan = TimeSpan.FromMilliseconds(100)
            Assert.IsFalse(device.PollAutoRead, $"{NameOf(VI.VisaSessionBase.PollAutoRead).SplitWords} should be disabled")
            device.PollAutoRead = True
            Assert.IsTrue(device.PollAutoRead, $"{NameOf(VI.VisaSessionBase.PollAutoRead).SplitWords} should be enabled")

            Assert.IsFalse(device.PollEnabled, $"{NameOf(VI.VisaSessionBase.PollEnabled).SplitWords} should be disabled")
            device.PollEnabled = True
            Assert.IsTrue(device.PollEnabled, $"{NameOf(VI.VisaSessionBase.PollEnabled).SplitWords} should be enabled")

            Assert.IsTrue(String.IsNullOrWhiteSpace(device.PollReading), $"{NameOf(VI.VisaSessionBase.PollAutoRead).SplitWords} should be empty")

            ' read operation completion
            Dim expectedSPollMessage As String = "1"
            Dim pollMessageName As String = "Operation Completed"
            Dim queryCommand As String = "*OPC?"
            device.Session.WriteLine(queryCommand)

            ' wait for the message
            device.StartAwaitingPollReadingTask(TimeSpan.FromTicks(100 * device.PollTimespan.Ticks)).Wait()

            Assert.IsFalse(String.IsNullOrWhiteSpace(device.PollReading), $"Poll reading '{device.PollReading}' should not be empty")
            Assert.AreEqual(expectedSPollMessage, device.PollReading, $"Expected {pollMessageName} {queryCommand} message")

            device.PollAutoRead = False
            device.PollEnabled = False
            device.Session.ReadStatusRegister()
            ' wait for the poll timer to disabled
            device.StartAwaitingPollTimerDisabledTask(TimeSpan.FromTicks(2 * device.PollTimespan.Ticks)).Wait()

            Assert.IsFalse(device.PollAutoRead, $"{NameOf(VI.VisaSessionBase.PollAutoRead).SplitWords} should be disabled")
            Assert.IsFalse(device.PollEnabled, $"{NameOf(VI.VisaSessionBase.PollEnabled).SplitWords} should be disabled")
            Assert.IsFalse(device.PollTimerEnabled, $"{NameOf(VI.VisaSessionBase.PollTimerEnabled).SplitWords} should be disabled")

        Catch
            Throw
        Finally

            device.PollAutoRead = False
            device.PollEnabled = False
            device.Session.ReadStatusRegister()

        End Try

    End Sub


#End Region

#Region " SERVICE REQUEST HANDLING "

    ''' <summary> Name of the service request message. </summary>
    Private Shared _ServiceRequestMessageName As String = "Operation Completed"

    ''' <summary> Message describing the expected service request. </summary>
    Private Shared _ExpectedServiceRequestMessage As String = String.Empty

    Private Shared ReadOnly ServiceRequestReadingToken As New isr.Core.Constructs.ConcurrentToken(Of String)

    ''' <summary> Handles the service request. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub HandleServiceRequest(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim mbs As VI.Pith.SessionBase = CType(sender, VI.Pith.SessionBase)
            Dim sb As Pith.ServiceRequests = mbs.ReadStatusByte()
            If (sb And Pith.ServiceRequests.MessageAvailable) <> 0 Then
                DeviceManager.ServiceRequestReadingToken.Value = mbs.ReadLineTrimEnd
            Else
                Assert.Fail("MAV in status register is not set, which means that message is not available. Make sure the command to enable SRQ is correct, and the instrument is 488.2 compatible.")
            End If
        Catch ex As Exception
            Assert.Fail(ex.ToString)
        End Try
    End Sub

    ''' <summary> Assert handling service request. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session"> The session. </param>
    Private Shared Sub AssertHandlingServiceRequest(ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsFalse(session.ServiceRequestEventEnabled, "service request event should not be enabled")
        Dim expectedServiceRequestEnableCommand As String = String.Format($"*SRE {CInt(session.ServiceRequestEnabledBitmask)}")
        Assert.AreEqual(expectedServiceRequestEnableCommand, session.ServiceRequestEnableCommand, "should equal the service request enabled command")
        Try
            session.EnableServiceRequestEventHandler()
            Assert.IsTrue(session.ServiceRequestEventEnabled, $"Service request event not enabled")
            AddHandler session.ServiceRequested, AddressOf DeviceManager.HandleServiceRequest
            ' read operation completion
            DeviceManager._ExpectedServiceRequestMessage = "1"
            DeviceManager._ServiceRequestMessageName = "Operation Completed"
            Dim queryCommand As String = "*OPC?"
            session.WriteLine(queryCommand)
            ' wait for the message
            isr.Core.ApplianceBase.DoEventsWaitUntil(TimeSpan.FromMilliseconds(400), Function()
                                                                                         Return Not String.IsNullOrWhiteSpace(DeviceManager.ServiceRequestReadingToken.Value)
                                                                                     End Function)
            Assert.AreEqual(DeviceManager._ExpectedServiceRequestMessage, DeviceManager.ServiceRequestReadingToken.Value, $"Expected {DeviceManager._ServiceRequestMessageName} {queryCommand} message")
            RemoveHandler session.ServiceRequested, AddressOf HandleServiceRequest
            session.DisableServiceRequestEventHandler()
            ' must refresh the service request here to update the status register flags.
            session.ReadStatusRegister()
            Assert.IsFalse(session.ServiceRequestEventEnabled, $"Service request event should be disabled")
        Catch
            Throw
        Finally
            session.DisableServiceRequestEventHandler()
            session.ReadStatusRegister()
        End Try
    End Sub

    ''' <summary> Assert handling service request. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    Public Shared Sub AssertHandlingServiceRequest(ByVal device As VI.VisaSessionBase)
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Assert.IsTrue(device.IsDeviceOpen, "session should be open")
        device.Session.ApplyServiceRequestEnableBitmask(device.Session.DefaultServiceRequestEnableBitmask)
        DeviceManager.AssertHandlingServiceRequest(device.Session)
    End Sub

    ''' <summary> Assert device handling service request. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    Public Shared Sub AssertDeviceHandlingServiceRequest(ByVal device As VI.VisaSessionBase)

        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Assert.IsTrue(device.IsDeviceOpen, "session should be open")

        device.Session.ApplyServiceRequestEnableBitmask(device.Session.DefaultServiceRequestEnableBitmask)
        Assert.IsFalse(device.Session.ServiceRequestEventEnabled, $"{NameOf(Pith.SessionBase.ServiceRequestEventEnabled).SplitWords} should not be enabled")

        Dim expectedServiceRequestEnableCommand As String = String.Format($"*SRE {CInt(device.Session.ServiceRequestEnabledBitmask)}")
        Assert.AreEqual(expectedServiceRequestEnableCommand, device.Session.ServiceRequestEnableCommand,
                            $"{NameOf(Pith.SessionBase.ServiceRequestEnableCommand).SplitWords} should equal the command")

        Dim expectedPollMessageAvailbleBitmask As Integer = Pith.ServiceRequests.MessageAvailable
        Assert.AreEqual(expectedPollMessageAvailbleBitmask, device.PollMessageAvailableBitmask, "Message available ")

        Try
            device.Session.EnableServiceRequestEventHandler()
            Assert.IsTrue(device.Session.ServiceRequestEventEnabled, $"{NameOf(Pith.SessionBase.ServiceRequestEventEnabled).SplitWords} should be enabled")

            Assert.IsFalse(device.ServiceRequestAutoRead, $"{NameOf(VI.VisaSessionBase.ServiceRequestAutoRead).SplitWords} should be off")
            device.ServiceRequestAutoRead = True
            Assert.IsTrue(device.ServiceRequestAutoRead, $"{NameOf(VI.VisaSessionBase.ServiceRequestAutoRead).SplitWords} should be on")

            Assert.IsFalse(device.ServiceRequestHandlerAssigned, $"{NameOf(VI.VisaSessionBase.ServiceRequestHandlerAssigned).SplitWords} should not be assigned")
            device.AddServiceRequestEventHandler()
            Assert.IsTrue(device.ServiceRequestHandlerAssigned, $"{NameOf(VI.VisaSessionBase.ServiceRequestHandlerAssigned).SplitWords} should be assigned")

            ' read operation completion
            DeviceManager._ExpectedServiceRequestMessage = "1"
            DeviceManager._ServiceRequestMessageName = "Operation Completed"
            Dim queryCommand As String = "*OPC?"
            device.Session.WriteLine(queryCommand)

            ' wait for the message
            device.StartAwaitingServiceRequestReadingTask(TimeSpan.FromMilliseconds(10000)).Wait()

            Assert.IsFalse(String.IsNullOrWhiteSpace(device.ServiceRequestReading), $"Service request reading '{device.ServiceRequestReading}' should not be empty")
            Assert.AreEqual(DeviceManager._ExpectedServiceRequestMessage, device.ServiceRequestReading, $"Expected {DeviceManager._ServiceRequestMessageName} {queryCommand} message")

            device.Session.DisableServiceRequestEventHandler()
            ' must refresh the service request here to update the status register flags.
            device.Session.ReadStatusRegister()
            Assert.IsFalse(device.Session.ServiceRequestEventEnabled, $"{NameOf(Pith.SessionBase.ServiceRequestEventEnabled).SplitWords} should be disabled")

        Catch
            Throw
        Finally

            If device.ServiceRequestHandlerAssigned Then device.RemoveServiceRequestEventHandler()
            device.Session.DisableServiceRequestEventHandler()
            device.Session.ReadStatusRegister()

        End Try

    End Sub

#End Region

End Class

