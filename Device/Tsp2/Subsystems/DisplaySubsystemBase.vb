''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public MustInherit Class DisplaySubsystemBase
    Inherits VI.DisplaySubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.RestoreMainScreenWaitCompleteCommand = Tsp.Syntax.Display.RestoreMainWaitCompleteCommand
    End Sub

    #End Region

    #Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Enabled = True
        Me.Exists = True
    End Sub

    #End Region

    #Region " COMMAND SYNTAX "

    #Region " DISPLAY SCREEN  "

    ''' <summary> Gets or sets the display Screen command format. </summary>
    ''' <value> The display Screen command format. </value>
    Protected Overrides Property DisplayScreenCommandFormat As String = Tsp.Syntax.Display.DisplayScreenCommandFormat

    ''' <summary> Gets or sets the display Screen query command. </summary>
    ''' <value> The display Screen query command. </value>
    Protected Overrides Property DisplayScreenQueryCommand As String = String.Empty

    #End Region

    #Region " ENABLED "

    ''' <summary> Gets or sets the display enable command format. </summary>
    ''' <value> The display enable command format. </value>
    Protected Overrides Property DisplayEnableCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the display enabled query command. </summary>
    ''' <value> The display enabled query command. </value>
    Protected Overrides Property DisplayEnabledQueryCommand As String = String.Empty

    #End Region

    #End Region

    #Region " EXISTS "

    ''' <summary>
    ''' Reads the display existence indicator. Some TSP instruments (e.g., 3706) may have no display.
    ''' </summary>
    ''' <returns> <c>True</c> if the display exists; otherwise, <c>False</c>. </returns>
    Public Overrides Function QueryExists() As Boolean?
        ' detect the display
        Me.Session.MakeEmulatedReplyIfEmpty(Me.Exists.GetValueOrDefault(True))
        Me.Exists = Not Me.Session.IsNil(Tsp.Syntax.Display.SubsystemName)
        Return Me.Exists
    End Function

    #End Region

    #Region " CLEAR "

    ''' <summary> Gets or sets the default display screen. </summary>
    ''' <value> The default display. </value>
    Public Property DefaultScreen As VI.DisplayScreens = VI.DisplayScreens.User

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearCommand As String = Tsp.Syntax.Display.ClearCommand

    ''' <summary> Clears the display. </summary>
    ''' <remarks> Sets the display to the user mode. </remarks>
    Public Overrides Sub ClearDisplay()
        Me.DisplayScreen = Me.DefaultScreen
        MyBase.ClearDisplay()
    End Sub

    ''' <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
    ''' <remarks> Sets the display to user. </remarks>
    Public Sub TryClearDisplayMeasurement()
        If Me.Exists AndAlso ((Me.DisplayScreen And DisplayScreens.Measurement) = 0) Then
            Me.TryClearDisplay()
        End If
    End Sub

    ''' <summary> Gets or sets the measurement screen. </summary>
    ''' <value> The measurement screen. </value>
    Public Property MeasurementScreen As VI.DisplayScreens = VI.DisplayScreens.Measurement

    ''' <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
    ''' <remarks> Sets the display to the measurement. </remarks>
    Public Sub ClearDisplayMeasurement()
        If Me.QueryExists.GetValueOrDefault(False) AndAlso ((Me.DisplayScreen And DisplayScreens.Measurement) = 0) Then
            Me.ClearDisplay()
        End If
        Me.DisplayScreen = Me.MeasurementScreen
    End Sub

    #End Region

    #Region " USER SCREEN TEXT "

    ''' <summary> Gets or sets the user screen. </summary>
    ''' <value> The user screen. </value>
    Public Property UserScreen As VI.DisplayScreens = VI.DisplayScreens.UserSwipe

    ''' <summary> Displays a message on the display. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The value. </param>
    Public Overrides Sub DisplayLine(ByVal lineNumber As Integer, ByVal value As String)
        If Me.QueryExists.GetValueOrDefault(False) AndAlso Not String.IsNullOrWhiteSpace(value) Then
            lineNumber = Math.Max(1, Math.Min(2, lineNumber))
            Dim length As Integer = If(lineNumber = 1, Tsp.Syntax.Display.FirstLineLength, Tsp.Syntax.Display.SecondLineLength)
            If value.Length < length Then value = value.PadRight(length)
            Me.Write(Tsp.Syntax.Display.SetTextLineCommandFormat, lineNumber, value)
            Me.DisplayScreen = Me.UserScreen
        End If

    End Sub

    ''' <summary> Displays the program title. </summary>
    ''' <param name="title">    Top row data. </param>
    ''' <param name="subtitle"> Bottom row data. </param>
    Public Sub DisplayTitle(ByVal title As String, ByVal subtitle As String)
        Me.DisplayLine(1, title)
        Me.DisplayLine(2, subtitle)
    End Sub

#End Region

#Region " RESTORE "

    ''' <summary> Gets or sets the restore display command. </summary>
    ''' <value> The restore display command. </value>
    Public Property RestoreMainScreenWaitCompleteCommand As String

    ''' <summary> Restores the instrument display. </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub RestoreDisplay(ByVal timeout As TimeSpan)
        Me.DisplayScreen = VI.DisplayScreens.Default
        If Me.Exists.HasValue AndAlso Me.Exists.Value AndAlso Not String.IsNullOrWhiteSpace(Me.RestoreMainScreenWaitCompleteCommand) Then
            Me.Session.EnableWaitComplete()
            ' Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
            Me.Session.WriteLine(Me.RestoreMainScreenWaitCompleteCommand)
            Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        End If
    End Sub

    ''' <summary> Restores the instrument display. </summary>
    Public Sub RestoreDisplay()
        Me.DisplayScreen = VI.DisplayScreens.Default
        If Me.Exists.HasValue AndAlso Me.Exists.Value AndAlso Not String.IsNullOrWhiteSpace(Me.RestoreMainScreenWaitCompleteCommand) Then
            ' Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
            Me.Session.WriteLine(Me.RestoreMainScreenWaitCompleteCommand)
            Me.Session.DoEventsStatusReadDelay()
            Me.Session.QueryOperationCompleted()
        End If
    End Sub

#End Region

End Class

