''' <summary> Form for viewing the visa tree view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-01-03 </para>
''' </remarks>
Public Class VisaTreeViewForm
    Inherits isr.Core.Forma.ConsoleForm

#Region " CONSTRUCTION and CLEAN UP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.Name = "Visa.View.Form"
    End Sub

    ''' <summary> Creates a new VisaTreeViewForm. </summary>
    ''' <returns> A VisaTreeViewForm. </returns>
    Public Shared Function Create() As VisaTreeViewForm
        Dim VisaTreeView As VisaTreeView = Nothing
        Try
            VisaTreeView = New VisaTreeView()
            Return VisaTreeViewForm.Create(VisaTreeView)
        Catch
            If VisaTreeView IsNot Nothing Then
                VisaTreeView.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary> Creates a new VisaTreeViewForm. </summary>
    ''' <param name="visaTreeView"> The visa view. </param>
    ''' <returns> A VisaTreeViewForm. </returns>
    Public Shared Function Create(ByVal visaTreeView As VisaTreeView) As VisaTreeViewForm
        Dim result As VisaTreeViewForm = Nothing
        Try
            result = New VisaTreeViewForm With {.VisaTreeView = visaTreeView}
        Catch
            If result IsNot Nothing Then
                result.Dispose()
            End If

            Throw
        End Try
        Return result
    End Function

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                If Me.VisaTreeViewDisposeEnabled AndAlso Me.VisaTreeView IsNot Nothing Then
                    Dim session As VisaSessionBase = Me._VisaTreeView.VisaSessionBase
                    If Me.VisaTreeView IsNot Nothing Then Me._VisaTreeView.Dispose()
                    If session IsNot Nothing Then session.Dispose()
                End If
                Me._VisaTreeView = Nothing
                End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding Visa View"
            ' the talker control publishes the device messages which thus get published to the form message box.
            Me.AddTalkerControl(Me.VisaTreeView.VisaSessionBase.CandidateResourceTitle, Me.VisaTreeView, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.MyLibrary.Logger)
            If Not String.IsNullOrWhiteSpace(Me.VisaTreeView.VisaSessionBase.CandidateResourceName) Then
                activity = $"{Me.Name}; starting {Me.VisaTreeView.VisaSessionBase.CandidateResourceName} selection task"
                Me.VisaTreeView.VisaSessionBase.AsyncValidateResourceName(Me.VisaTreeView.VisaSessionBase.CandidateResourceName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets the await the resource name validation task enabled. </summary>
    ''' <value> The await the resource name validation task enabled. </value>
    Protected Property AwaitResourceNameValidationTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.VisaTreeView.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)
            If Me.VisaTreeView.VisaSessionBase.IsValidatingResourceName Then
                If Me.AwaitResourceNameValidationTaskEnabled Then
                    activity = $"{Me.Name}; awaiting {Me.VisaSessionBase.CandidateResourceName} validation"
                    Me.VisaTreeView.VisaSessionBase.AwaitResourceNameValidation(My.Settings.ResourceNameSelectionTimeout)
                Else
                    activity = $"{Me.Name}; validating {Me.VisaSessionBase.CandidateResourceName}"
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.VisaTreeView IsNot Nothing Then Me.VisaTreeView.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " VISA TREE VIEW "

    ''' <summary> Gets the visa view. </summary>
    ''' <value> The visa view. </value>
    Public Property VisaTreeView As isr.VI.Facade.VisaTreeView

    ''' <summary> Gets the visa view dispose enabled. </summary>
    ''' <value> The visa view dispose enabled. </value>
    Public Property VisaTreeViewDisposeEnabled As Boolean

    ''' <summary> Gets the locally assigned visa session if any. </summary>
    ''' <value> The visa session. </value>
    Protected ReadOnly Property VisaSessionBase As VisaSessionBase
        Get
            Return Me.VisaTreeView.VisaSessionBase
        End Get
    End Property

    ''' <summary> Creates view. </summary>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    Private Sub CreateViewView(ByVal candidateResourceName As String, ByVal candidateResourceTitle As String)
        Try
            Me.VisaTreeView = New isr.VI.Facade.VisaTreeView(New VisaSession)
            Me.VisaSessionBase.CandidateResourceTitle = candidateResourceTitle
            Me.VisaSessionBase.CandidateResourceName = candidateResourceName
        Catch
            Me.VisaTreeView?.Dispose()
            Throw
        End Try
    End Sub

#End Region

#Region " SHOW DIALOG "

    ''' <summary> Shows the form dialog. </summary>
    ''' <param name="owner">                  The owner. </param>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal candidateResourceName As String, ByVal candidateResourceTitle As String) As Windows.Forms.DialogResult
        Dim VisaTreeView As isr.VI.Facade.VisaTreeView = Nothing
        Try
            Me.CreateViewView(candidateResourceName, candidateResourceTitle)
            Return Me.ShowDialog(owner, VisaTreeView)
        Catch
            If VisaTreeView IsNot Nothing Then
                VisaTreeView.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary> Shows the form dialog. </summary>
    ''' <param name="owner">        The owner. </param>
    ''' <param name="visaTreeView"> The visa view. </param>
    ''' <returns> A Windows.Forms.DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal visaTreeView As isr.VI.Facade.VisaTreeView) As Windows.Forms.DialogResult
        Me.VisaTreeView = visaTreeView
        Return MyBase.ShowDialog(owner)
    End Function

    ''' <summary> Shows the form. </summary>
    ''' <param name="owner">                  The owner. </param>
    ''' <param name="candidateResourceName">  Name of the candidate resource. </param>
    ''' <param name="candidateResourceTitle"> The candidate resource title. </param>
    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal candidateResourceName As String, ByVal candidateResourceTitle As String)
        Dim VisaTreeView As isr.VI.Facade.VisaTreeView = Nothing
        Try
            Me.CreateViewView(candidateResourceName, candidateResourceTitle)
            Me.Show(owner, VisaTreeView)
        Catch
            If VisaTreeView IsNot Nothing Then
                VisaTreeView.Dispose()
            End If

            Throw
        End Try
    End Sub

    ''' <summary> Shows the form. </summary>
    ''' <param name="owner">        The owner. </param>
    ''' <param name="visaTreeView"> The visa view. </param>
    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal visaTreeView As isr.VI.Facade.VisaTreeView)
        Me.VisaTreeView = visaTreeView
        MyBase.Show(owner)
    End Sub

#End Region

End Class
