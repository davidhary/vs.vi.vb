''' <summary> Defines a System Subsystem for a EG2000 Prober. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-01, 3.0.5022. </para>
''' </remarks>
Public Class SystemSubsystem
    Inherits VI.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <value> The preset command. </value>
    Protected Overrides Property PresetCommand As String = VI.Pith.Scpi.Syntax.SystemPresetCommand

#End Region

End Class
