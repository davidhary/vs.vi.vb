'---------------------------------------------------------------------------------------------------
' file:		National.Visa\Code\ResourceManagerExtensions.vb
'
' summary:	Resource manager extensions class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices

Imports isr.VI.National.Visa.ExceptionExtensions

''' <summary> Resource Manager Extensions. </summary>
''' <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para></remarks>
Friend Module ResourceManagerExtensions

#Region " CONVERSIONS "

    ''' <summary> Converts the given value. </summary>
    ''' <param name="value"> Reference to the
    '''                      <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                      manager</see>. </param>
    ''' <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
    Public Function ConvertInterfaceType(ByVal value As Ivi.Visa.HardwareInterfaceType) As VI.Pith.HardwareInterfaceType
        Return If([Enum].IsDefined(GetType(VI.Pith.HardwareInterfaceType), CInt(value)),
            CType(CInt(value), VI.Pith.HardwareInterfaceType),
            VI.Pith.HardwareInterfaceType.Gpib)
    End Function

    ''' <summary> Converts the given value. </summary>
    ''' <param name="value"> Reference to the
    '''                      <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                      manager</see>. </param>
    ''' <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
    Public Function ConvertInterfaceType(ByVal value As VI.Pith.HardwareInterfaceType) As Ivi.Visa.HardwareInterfaceType
        Return If([Enum].IsDefined(GetType(Ivi.Visa.HardwareInterfaceType), CInt(value)),
            CType(CInt(value), Ivi.Visa.HardwareInterfaceType),
            Ivi.Visa.HardwareInterfaceType.Gpib)
    End Function

    ''' <summary> Build resource message. </summary>
    ''' <param name="ex"> Details of the exception. </param>
    ''' <returns> A an array with a single resource expressing the error. </returns>
    Private Function BuildResourceMessage(ByVal ex As Exception) As IEnumerable(Of String)
        If TypeOf ex.InnerException Is Ivi.Visa.VisaException Then
            Dim nativeEx As Ivi.Visa.NativeVisaException = CType(ex.InnerException, Ivi.Visa.NativeVisaException)
            If nativeEx.ErrorCode = Ivi.Visa.NativeErrorCode.ResourceNotFound Then
                ' Insufficient location information or the device or resource is not present in the system.  
                ' VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
                Return New String() {"Error: Resource Not Found"}
            Else
                Return New String() {$"{nativeEx.Message} code={CInt(nativeEx.ErrorCode)}:{nativeEx.ErrorCode}"}
            End If
        Else
            Return New String() {ex.Message}
        End If
    End Function

    ''' <summary> Convert parse result. </summary>
    ''' <param name="value"> Reference to the
    '''                      <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                      manager</see>. </param>
    ''' <returns> The <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
    Public Function ConvertParseResult(value As Ivi.Visa.ParseResult) As VI.Pith.ResourceNameInfo
        Return New VI.Pith.ResourceNameInfo(value.OriginalResourceName, ConvertInterfaceType(value.InterfaceType), value.InterfaceNumber)
    End Function

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Try parse resource. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value">        Reference to the
    '''                             <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                             manager</see>. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>True</c> if parsed or false if failed parsing. </returns>
    <Extension()>
    Public Function ParseResource(ByVal value As NationalInstruments.Visa.ResourceManager, ByVal resourceName As String) As VI.Pith.ResourceNameInfo

        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            Return ConvertParseResult(value.Parse(resourceName))
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, resourceName, "@DM", "Parsing resource")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try

    End Function

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value"> Reference to the
    '''                      <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                      manager</see>. </param>
    ''' <returns> List of all resources. </returns>
    <Extension()>
    Public Function FindResources(ByVal value As NationalInstruments.Visa.ResourceManager) As IEnumerable(Of String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim resources As String = String.Empty
        Try
            resources = VI.Pith.ResourceNamesManager.AllResourcesFilter
            Return value.Find(VI.Pith.ResourceNamesManager.AllResourcesFilter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, resources, "@DM", "Finding resources")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">     Reference to the
    '''                          <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                          manager</see>. </param>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if resources were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindResources(ByVal value As NationalInstruments.Visa.ResourceManager,
                                         ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            Return value.FindResources().Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As VI.Pith.NativeException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">     Reference to the
    '''                          <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                          manager</see>. </param>
    ''' <param name="filter">    A pattern specifying the search. </param>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if resources were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindResources(ByVal value As NationalInstruments.Visa.ResourceManager,
                                         ByVal filter As String, ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            resources = value.Find(filter)
            Return resources.Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As Ivi.Visa.NativeVisaException
            resources = New String() {ex.ToFullBlownString}
            Return False
        End Try
    End Function

    ''' <summary> Returns true if the specified resource exists. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        Reference to the
    '''                             <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                             manager</see>. </param>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function Exists(ByVal value As NationalInstruments.Visa.ResourceManager, ByVal resourceName As String) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))
        Dim resources As IEnumerable(Of String) = value.FindResources()
        Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
    End Function

    ''' <summary> Searches for the interface. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        Reference to the
    '''                             <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                             manager</see>. </param>
    ''' <param name="resourceName"> The interface resource name. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function InterfaceExists(ByVal value As NationalInstruments.Visa.ResourceManager, ByVal resourceName As String) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim resourceNameInfo As VI.Pith.ResourceNameInfo = value.ParseResource(resourceName)
        If resourceNameInfo.IsParsed Then
#Disable Warning CA1825 ' Avoid zero-length array allocations.
            Dim resources As IEnumerable(Of String) = New String() {}
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            If value.TryFindInterfaces(resourceNameInfo.InterfaceType, resources) Then
                Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
            End If
        End If
        Return False
    End Function

    ''' <summary> Searches for all interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value"> Reference to the
    '''                      <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                      manager</see>. </param>
    ''' <returns> The found interface resource names. </returns>
    <Extension()>
    Public Function FindInterfaces(ByVal value As NationalInstruments.Visa.ResourceManager) As IEnumerable(Of String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim filter As String = String.Empty
        Try
            filter = VI.Pith.ResourceNamesManager.BuildInterfaceFilter()
            Return value.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding interfaces")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Tries to find interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">     Reference to the
    '''                          <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                          manager</see>. </param>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindInterfaces(ByVal value As NationalInstruments.Visa.ResourceManager,
                                          ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            resources = value.FindInterfaces()
            Return resources.Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As VI.Pith.NativeException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

    ''' <summary> Searches for the interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value">         Reference to the
    '''                              <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                              manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    <Extension()>
    Public Function FindInterfaces(ByVal value As NationalInstruments.Visa.ResourceManager,
                                       ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim filter As String = String.Empty
        Try
            filter = VI.Pith.ResourceNamesManager.BuildInterfaceFilter(interfaceType)
            Return value.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding interfaces")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Tries to find interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the
    '''                              <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                              manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindInterfaces(ByVal value As NationalInstruments.Visa.ResourceManager,
                                          ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                          ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            resources = value.FindInterfaces(interfaceType)
            Return resources.Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As VI.Pith.NativeException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

    ''' <summary> Searches for the instrument. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        Reference to the
    '''                             <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                             manager</see>. </param>
    ''' <param name="resourceName"> The instrument resource name. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function InstrumentExists(ByVal value As NationalInstruments.Visa.ResourceManager, ByVal resourceName As String) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim result As VI.Pith.ResourceNameInfo = value.ParseResource(resourceName)
        If result.IsParsed Then
#Disable Warning CA1825 ' Avoid zero-length array allocations.
            Dim resources As IEnumerable(Of String) = New String() {}
#Enable Warning CA1825 ' Avoid zero-length array allocations.
            If value.TryFindInstruments(result.InterfaceType, result.InterfaceNumber, resources) Then
                Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
            End If
        End If
        Return False
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value"> Reference to the
    '''                      <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                      manager</see>. </param>
    ''' <returns> The found instrument resource names. </returns>
    <Extension()>
    Public Function FindInstruments(ByVal value As NationalInstruments.Visa.ResourceManager) As IEnumerable(Of String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter()
            Return value.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">     Reference to the
    '''                          <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                          manager</see>. </param>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindInstruments(ByVal value As NationalInstruments.Visa.ResourceManager,
                                           ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            resources = value.FindInstruments()
            Return resources.Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As VI.Pith.NativeException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value">         Reference to the
    '''                              <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                              manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    <Extension()>
    Public Function FindInstruments(ByVal value As NationalInstruments.Visa.ResourceManager,
                                        ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType)
            Return value.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the
    '''                              <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                              manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindInstruments(ByVal value As NationalInstruments.Visa.ResourceManager,
                                           ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                           ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            resources = value.FindInstruments(interfaceType)
            Return resources.Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As VI.Pith.NativeException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
    ''' <param name="value">         Reference to the
    '''                              <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                              manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    <Extension()>
    Public Function FindInstruments(ByVal value As NationalInstruments.Visa.ResourceManager,
                                        ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType, boardNumber)
            Return value.Find(filter)
        Catch ex As Ivi.Visa.NativeVisaException
            Dim nativeError As New NativeError(ex.ErrorCode, filter, "@DM", "Finding instruments")
            Throw New VI.Pith.NativeException(nativeError, ex)
        End Try
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">           Reference to the
    '''                                <see cref="NationalInstruments.Visa.ResourceManager">resource
    '''                                manager</see>. </param>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
    ''' <param name="resources">       [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    <Extension()>
    Public Function TryFindInstruments(ByVal value As NationalInstruments.Visa.ResourceManager,
                                           ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                           ByVal interfaceNumber As Integer, ByRef resources As IEnumerable(Of String)) As Boolean
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            resources = value.FindInstruments(interfaceType, interfaceNumber)
            Return resources.Any
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.BuildResourceMessage(ex)
            Return False
        Catch ex As VI.Pith.NativeException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

#End Region

End Module

