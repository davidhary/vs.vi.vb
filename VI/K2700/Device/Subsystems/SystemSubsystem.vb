''' <summary> Defines a System Subsystem for a Keithley 2700 instrument. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class SystemSubsystem
    Inherits VI.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.AutoZeroEnabled = True
        Me.BeeperEnabled = True
        Me.FourWireSenseEnabled = False
        Me.QueryFrontTerminalsSelected()
    End Sub

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Gets or sets the automatic zero enabled query command. </summary>
    ''' <value> The automatic zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = ":SYST:AZER:STAT?"

    ''' <summary> Gets or sets the automatic zero enabled command format. </summary>
    ''' <value> The automatic zero enabled command format. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = ":SYST:AZER:STAT {0:'ON';'ON';'OFF'}"

#End Region

#Region " BEEPER ENABLED + IMMEDIATE "

    ''' <summary> Gets or sets the Beeper enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overrides Property BeeperEnabledQueryCommand As String = ":SYST:BEEP:STAT?"

    ''' <summary> Gets or sets the Beeper enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overrides Property BeeperEnabledCommandFormat As String = ":SYST:BEEP:STAT {0:'1';'1';'0'}"

    ''' <summary> Gets or sets the beeper immediate command format. </summary>
    ''' <value> The beeper immediate command format. </value>
    Protected Overrides Property BeeperImmediateCommandFormat As String = ":SYST:BEEP:IMM {0}, {1}"

#End Region

#Region " FOUR WIRE SENSE ENABLED "

    ''' <summary> Gets or sets the Four Wire Sense enabled query command. </summary>
    ''' <value> The Four Wire Sense enabled query command. </value>
    Protected Overrides Property FourWireSenseEnabledQueryCommand As String = ":SYST:RSEN?"

    ''' <summary> Gets or sets the Four Wire Sense enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:RSEN {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Four Wire Sense enabled query command. </value>
    Protected Overrides Property FourWireSenseEnabledCommandFormat As String = ":SYST:RSEN {0:'ON';'ON';'OFF'}"

#End Region

#Region " FRONT SWITCHED "

    ''' <summary> Gets or sets the Front Terminals Selected query command. </summary>
    ''' <value> The Front Terminals Selected query command. </value>
    Protected Overrides Property FrontTerminalsSelectedQueryCommand As String = ":SYST:FRSW?"

    ''' <summary> Gets or sets the Front Terminals Selected command Format. </summary>
    ''' <remarks> SCPI: ":SYST:FRSW {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Front Terminals Selected query command. </value>
    Protected Overrides Property FrontTerminalsSelectedCommandFormat As String = String.Empty

#End Region

End Class
