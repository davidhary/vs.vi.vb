'---------------------------------------------------------------------------------------------------
' file:		.\Device\DeviceManager_SelectorOpener.vb
'
' summary:	Device manager selector opener class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

Partial Public NotInheritable Class DeviceManager

    ''' <summary> An opener. </summary>
    Private Class Opener
        Inherits isr.Core.Models.OpenerViewModel

        ''' <summary> True if is open, false if not. </summary>
        Private _IsOpen As Boolean

        ''' <summary> Gets the Open status. </summary>
        ''' <value> The Open status. </value>
        Public Overrides ReadOnly Property IsOpen As Boolean
            Get
                Return Me._IsOpen
            End Get
        End Property

        ''' <summary> Opens a resource. </summary>
        ''' <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
        '''                                               occurs. </exception>
        ''' <param name="resourceName">  The name of the resource. </param>
        ''' <param name="resourceTitle"> The resource title. </param>
        Public Overrides Sub OpenResource(resourceName As String, resourceTitle As String)
            Me._IsOpen = True
            MyBase.OpenResource(resourceName, resourceTitle)
            Dim e As New ComponentModel.CancelEventArgs
            Me.OnOpening(e)
            If e.Cancel Then
                Throw New OperationCanceledException($"Opening {resourceTitle}:{resourceName} status canceled;. ")
            End If
            Me.OnOpened(EventArgs.Empty)
        End Sub

        ''' <summary> Closes the resource. </summary>
        Public Overrides Sub CloseResource()
            MyBase.CloseResource()
            Me._IsOpen = False
            Me.OnClosing(New ComponentModel.CancelEventArgs)
            Me.OnClosed(EventArgs.Empty)
        End Sub

        ''' <summary> Gets or sets the active state cleared. </summary>
        ''' <value> The active state cleared. </value>
        Public Property ActiveStateCleared As Boolean

        ''' <summary> Applies default settings and clears the resource active state. </summary>
        Public Overrides Sub ClearActiveState()
            Me.ActiveStateCleared = True
        End Sub

        ''' <summary> Identifies talkers. </summary>
        Public Overrides Sub IdentifyTalkers()
            MyBase.IdentifyTalkers()
            VI.My.MyLibrary.Appliance.Identify(Me.Talker)
        End Sub

        ''' <summary>
        ''' Uses the <see cref="Core.ITalker"/> to publish the message or log if
        ''' <see cref="Core.ITalker"/> is nothing.
        ''' </summary>
        ''' <param name="eventType"> Type of the event. </param>
        ''' <param name="activity">  The activity. </param>
        ''' <returns> A String. </returns>
        Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
            Return Me.Publish(New Core.TraceMessage(eventType, VI.My.MyLibrary.TraceEventId, activity))
        End Function

        ''' <summary> Publish exception. </summary>
        ''' <param name="activity"> The activity. </param>
        ''' <param name="ex">       The ex. </param>
        ''' <returns> A String. </returns>
        Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
            Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
        End Function

    End Class

    ''' <summary> Assert selector enumerate resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="selectorOpener"> The selector opener. </param>
    ''' <param name="selector">       The selector. </param>
    Public Shared Sub AssertSelectorEnumerateResources(selectorOpener As isr.Core.Controls.SelectorOpener, selector As isr.Core.Models.SelectorViewModel)
        If selectorOpener Is Nothing Then Throw New ArgumentNullException(NameOf(selectorOpener))
        If selector Is Nothing Then Throw New ArgumentNullException(NameOf(selector))
        selectorOpener.AssignSelectorViewModel(selector)
        selector.EnumerateResources(False)
        Assert.AreEqual(selector.ResourceNames.Count, selectorOpener.InternalResourceNamesCount, $"Resource names count should match internal resource names count")
    End Sub

    ''' <summary> Assert resource open close. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="selectorOpener"> The control. </param>
    ''' <param name="selector">       The selector. </param>
    ''' <param name="opener">         The opener. </param>
    Public Shared Sub AssertResourceOpenClose(selectorOpener As isr.Core.Controls.SelectorOpener, selector As isr.Core.Models.SelectorViewModel,
                                              opener As isr.Core.Models.OpenerViewModel)
        If selectorOpener Is Nothing Then Throw New ArgumentNullException(NameOf(selectorOpener))
        If selector Is Nothing Then Throw New ArgumentNullException(NameOf(selector))
        If opener Is Nothing Then Throw New ArgumentNullException(NameOf(opener))
        Using form As New Windows.Forms.Form
            form.Controls.Add(selectorOpener)
            form.Show()
            selectorOpener.Visible = True
            selectorOpener.AssignSelectorViewModel(selector)
            selectorOpener.AssignOpenerViewModel(opener)
            selector.EnumerateResources(True)
            Assert.AreEqual(selector.ResourceNames.Count, selectorOpener.InternalResourceNamesCount, $"Resource names count should match internal resource names count")
            Assert.IsTrue(selectorOpener.SelectedValueChangeCount > 0, $"Selected value count {selectorOpener.SelectedValueChangeCount} should exceed zero")
            Assert.IsFalse(String.IsNullOrWhiteSpace(selector.ValidatedResourceName), "Validated resource name is not empty")
            Assert.IsTrue(opener.OpenEnabled, $"Opener open enabled after validating the resource")
            Dim e As New isr.Core.ActionEventArgs
            opener.TryOpen(e)
            Assert.IsFalse(e.Failed, $"open failed {e.Details}")
            Assert.AreEqual(selector.ValidatedResourceName, opener.OpenResourceName, "validated resource name assigned to resource name")
            Assert.AreEqual(opener.CandidateResourceTitle, opener.OpenResourceTitle, "title assigned to resource title")
            opener.TryClose(e)
            Assert.IsFalse(e.Failed, $"close failed {e.Details}")
        End Using
    End Sub

    ''' <summary> Assert resource open close. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub AssertResourceOpenClose(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal resourceSettings As VI.DeviceTests.ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        Using control As New isr.Core.Controls.SelectorOpener
            control.AddListener(testInfo.TraceMessagesQueueListener)
            Dim selector As isr.Core.Models.SelectorViewModel = New isr.VI.SessionFactory() With {.Searchable = True,
                                                                .ResourcesFilter = VI.SessionFactory.Get.Factory.ResourcesProvider.ResourceFinder.BuildMinimalResourcesFilter}
            Dim opener As New Opener With {.CandidateResourceTitle = resourceSettings.ResourceTitle,
                                           .CandidateResourceName = resourceSettings.ResourceName}
            DeviceManager.AssertResourceOpenClose(control, selector, opener)
        End Using
    End Sub

End Class
