'---------------------------------------------------------------------------------------------------
' file:		.\Device\VisaSessionBase_SRQ.vb
'
' summary:	Visa session base srq class
'---------------------------------------------------------------------------------------------------
Imports isr.Core

Partial Public Class VisaSessionBase

    ''' <summary> Message describing the service request failure. </summary>
    Private _ServiceRequestFailureMessage As String

    ''' <summary> Gets or sets a message describing the service request failure. </summary>
    ''' <value> A message describing the service request failure. </value>
    Public Property ServiceRequestFailureMessage As String
        Get
            Return Me._ServiceRequestFailureMessage
        End Get
        Set(value As String)
            If String.IsNullOrEmpty(value) Then value = String.Empty
            If Not String.Equals(value, Me.ServiceRequestFailureMessage) Then
                Me._ServiceRequestFailureMessage = value
                If Not String.IsNullOrWhiteSpace(Me.ServiceRequestFailureMessage) Then
                    Me.PublishWarning(Me.ServiceRequestFailureMessage)
                End If
            End If
            ' send every time
            Me.SyncNotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Processes the service request. </summary>
    Protected MustOverride Sub ProcessServiceRequest()

    ''' <summary> Reads the event registers after receiving a service request. </summary>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Function TryProcessServiceRequest() As Boolean
        Dim result As Boolean = True
        Try
            Me.ProcessServiceRequest()
        Catch ex As Exception
            Me.ServiceRequestFailureMessage = Me.PublishException("processing service request", ex)
            result = False
        End Try
        Return result
    End Function

    ''' <summary> The service request reading concurrent token. </summary>
    Private ReadOnly _ServiceRequestReadingToken As New isr.Core.Constructs.ConcurrentToken(Of String)

    ''' <summary> Gets or sets the Service Request reading. </summary>
    ''' <value> The Service Request reading. </value>
    Public Property ServiceRequestReading As String
        Get
            Return Me._ServiceRequestReadingToken.Value
        End Get
        Set(value As String)
            If Not String.Equals(Me.ServiceRequestReading, value) Then
                Me._ServiceRequestReadingToken.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True to service request automatic read. </summary>
    Private _ServiceRequestAutoRead As Boolean

    ''' <summary> Gets or sets the automatic read Service Requesting option . </summary>
    ''' <value> The automatic read service request option. </value>
    Public Property ServiceRequestAutoRead As Boolean
        Get
            Return Me._ServiceRequestAutoRead
        End Get
        Set(value As Boolean)
            If Me.ServiceRequestAutoRead <> value Then
                Me._ServiceRequestAutoRead = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Removes the ServiceRequested event handlers. </summary>
    Protected Sub RemoveServiceRequestedEventHandlers()
        Me._ServiceRequestedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The ServiceRequested event handlers. </summary>
    Private ReadOnly _ServiceRequestedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in ServiceRequested events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ServiceRequested As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._ServiceRequestedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._ServiceRequestedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._ServiceRequestedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="ServiceRequested">ServiceRequested Event</see>.
    ''' </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyServiceRequested(ByVal e As System.EventArgs)
        Me._ServiceRequestedEventHandlers.Send(Me, e)
    End Sub

    ''' <summary> Session base service requested. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionBaseServiceRequested(ByVal sender As Object, ByVal e As EventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.Pith.SessionBase)} service request"
        Try
            Me.TryProcessServiceRequest()
            If Me.ServiceNotificationLevel = isr.VI.Pith.NotifySyncLevel.Sync Then
                Me._ServiceRequestedEventHandlers.Send(Me, e)
            ElseIf Me.ServiceNotificationLevel = isr.VI.Pith.NotifySyncLevel.Async Then
                Me._ServiceRequestedEventHandlers.Post(Me, e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Starts awaiting service request reading task. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A Threading.Tasks.Task. </returns>
    Public Function StartAwaitingServiceRequestReadingTask(ByVal timeout As TimeSpan) As Threading.Tasks.Task
        Return Threading.Tasks.Task.Factory.StartNew(Sub()
                                                         Dim ticks As Long = timeout.Ticks
                                                         Dim sw As Stopwatch = Stopwatch.StartNew()
                                                         Do While Me.ServiceRequestEventEnabled AndAlso
                                                                  String.IsNullOrWhiteSpace(Me.ServiceRequestReading) AndAlso sw.ElapsedTicks < ticks
                                                             Threading.Thread.SpinWait(1)
                                                             isr.Core.ApplianceBase.DoEvents()
                                                         Loop
                                                     End Sub)
    End Function


End Class
