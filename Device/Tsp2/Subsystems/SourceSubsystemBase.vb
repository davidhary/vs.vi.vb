'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Subsystems\SourceSubsystemBase.vb
'
' summary:	Source subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary>
''' Defines the contract that must be implemented by a Source Current Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SourceSubsystemBase
    Inherits VI.SourceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        MyBase.FunctionModeReadWrites.AddReplace(VI.SourceFunctionModes.CurrentDC, "smu.FUNC_DC_CURRENT", VI.SourceFunctionModes.CurrentDC.DescriptionUntil)
        MyBase.FunctionModeReadWrites.AddReplace(VI.SourceFunctionModes.VoltageDC, "smu.FUNC_DC_VOLTAGE", VI.SourceFunctionModes.VoltageDC.DescriptionUntil)
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.CurrentDC).SetRange(-1.05, 1.05)
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.VoltageDC).SetRange(-210, 210)
    End Sub

#End Region

#Region " AUTO RANGE STATE "

    ''' <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Overrides Property AutoRangeEnabled As Boolean?
        Get
            Return MyBase.AutoRangeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoRangeEnabled, value) Then
                MyBase.AutoRangeEnabled = value
                Me.AutoRangeState = If(value.HasValue, If(value.Value, OnOffState.On, OnOffState.Off), New OnOffState?)
            End If
        End Set
    End Property

    ''' <summary> State of the automatic range. </summary>
    Private _AutoRangeState As OnOffState?

    ''' <summary> Gets or sets the Auto Range. </summary>
    ''' <value> The automatic range state. </value>
    Public Property AutoRangeState() As OnOffState?
        Get
            Return Me._AutoRangeState
        End Get
        Protected Set(ByVal value As OnOffState?)
            If Not Nullable.Equals(value, Me.AutoRangeState) Then
                Me._AutoRangeState = value
                Me.AutoRangeEnabled = If(value.HasValue, (value.Value = OnOffState.On), New Boolean?)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the AutoRange state. </summary>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> An OnOffState? </returns>
    Public Function ApplyAutoRangeState(ByVal value As OnOffState) As OnOffState?
        Me.WriteAutoRangeState(value)
        Return Me.QueryAutoRangeState()
    End Function

    ''' <summary> Gets the Auto Range state query command. </summary>
    ''' <value> The Auto Range state query command. </value>
    Protected Overridable Property AutoRangeStateQueryCommand As String = isr.VI.Tsp.Syntax.Lua.PrintCommand("_G.smu.measure.autorange")

    ''' <summary> Queries automatic range state. </summary>
    ''' <returns> The automatic range state. </returns>
    Public Function QueryAutoRangeState() As OnOffState?
        Me.Session.LastAction = Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"Reading {NameOf(AutoRangeState)};. ")
        Me.Session.LastNodeNumber = New Integer?
        Dim mode As String = Me.AutoRangeState.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd(Me.AutoRangeStateQueryCommand)
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = $"Failed fetching {NameOf(AutoRangeState)}"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.AutoRangeState = New OnOffState?
        Else
            Dim se As New StringEnumerator(Of OnOffState)
            Me.AutoRangeState = se.ParseContained(mode.BuildDelimitedValue)
        End If
        Return Me.AutoRangeState
    End Function

    ''' <summary> The Auto Range state command format. </summary>
    ''' <value> The automatic range state command format. </value>
    Protected Overridable Property AutoRangeStateCommandFormat As String = "_G.smu.measure.autorange={0}"

    ''' <summary> Writes an automatic range state. </summary>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> An OnOffState. </returns>
    Public Function WriteAutoRangeState(ByVal value As OnOffState) As OnOffState
        Me.Session.LastAction = Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, $"Writing {NameOf(AutoRangeState)}={value};. ")
        Me.Session.LastNodeNumber = New Integer?
        Me.Session.WriteLine(Me.AutoRangeStateCommandFormat, value.ExtractBetween)
        Me.AutoRangeState = value
        Return value
    End Function

    #End Region

    #Region " FUNCTION MODE "

    ''' <summary> Gets the function mode query command. </summary>
    ''' <value> The function mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = isr.VI.Tsp.Syntax.Lua.PrintCommand("_G.smu.Source.func")

    ''' <summary> Gets the function mode command format. </summary>
    ''' <value> The function mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = "_G.smu.Source.func={0}"

#End Region

#Region " SYNTAX "

#Region " LIMIT "

    ''' <summary> The current limit function. </summary>
    Private Const _CurrentLimitFunction As String = "i"

    ''' <summary> The voltage limit function. </summary>
    Private Const _VoltageLimitFunction As String = "v"

    ''' <summary> Limit function mode. </summary>
    ''' <returns> A String. </returns>
    Private Function LimitFunctionMode() As String
        Return If(Me.FunctionMode.Value = VI.SourceFunctionModes.CurrentDC, SourceSubsystemBase._VoltageLimitFunction, SourceSubsystemBase._CurrentLimitFunction)
    End Function

    ''' <summary> Gets the limit query command format. </summary>
    ''' <value> The limit query command format. </value>
    Protected Overridable Property LimitQueryCommandFormat As String

    ''' <summary> Gets the limit query command. </summary>
    ''' <value> The limit query command. </value>
    Protected Overrides Property ModalityLimitQueryCommandFormat As String
        Get
            Dim tspCommand As String = String.Format(Me.LimitQueryCommandFormat, Me.LimitFunctionMode)
            Return String.Format(Tsp.Syntax.Lua.PrintCommandStringNumberFormat, "9.6", tspCommand)
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets the limit command format. </summary>
    ''' <value> The limit command format. </value>
    Protected Overridable Property LimitCommandFormat As String

    ''' <summary> Gets the modality limit command format. </summary>
    ''' <value> The modality limit command format. </value>
    Protected Overrides Property ModalityLimitCommandFormat As String
        Get
            Return String.Format(Me.LimitCommandFormat, Me.LimitFunctionMode, "{0}")
        End Get
        Set(value As String)
        End Set
    End Property

    #End Region

    #Region " LIMIT TRIPPED "

    ''' <summary> Gets the limit tripped query command format. </summary>
    ''' <value> The limit tripped query command format. </value>
    Protected Overridable Property LimitTrippedQueryCommandFormat As String

    ''' <summary> Gets the limit tripped print command. </summary>
    ''' <value> The limit tripped print command. </value>
    Protected Overrides Property LimitTrippedQueryCommand As String
        Get
            Return String.Format(Me.LimitTrippedQueryCommandFormat, Me.LimitFunctionMode)
        End Get
        Set(value As String)
        End Set
    End Property

    #End Region

    #End Region

End Class

