'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\ChannelScanView.vb
'
' summary:	Channel scan view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.SplitExtensions

''' <summary>
''' A Keithley 2002 edition of the basic <see cref="Facade.ChannelScanView"/> user interface.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ChannelScanView
    Inherits isr.VI.Facade.ChannelScanView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
        If Not Me.DesignMode Then
            Me._FourWireResistanceScanMenu = Me.FourWireResistanceScanMakeMenu()
            Me._FourWireResistanceTimerScanMenu = Me.FourWireResistanceTimerScanMakeMenu(False)
        End If
    End Sub

    ''' <summary> Creates a new <see cref="ChannelScanView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ChannelScanView"/>. </returns>
    Public Overloads Shared Function Create() As ChannelScanView
        Dim view As ChannelScanView = Nothing
        Try
            view = New ChannelScanView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property K2002Device As K2002Device

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Shadows Sub AssignDevice(ByVal value As K2002Device)
        MyBase.AssignDevice(value)
        If Me.K2002Device IsNot Nothing Then
            RemoveHandler Me.K2002Device.SystemSubsystem.PropertyChanged, AddressOf Me.HandleSystemSubsystemPropertyChange
            Me.BindSubsystem(CType(Nothing, SystemSubsystemBase))
            Me.BindSubsystem(CType(Nothing, RouteSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SenseSubsystemBase))
        End If
        Me._K2002Device = value
        If value IsNot Nothing Then
            Me.BindSubsystem(Me.K2002Device.SystemSubsystem)
            Me.BindSubsystem(Me.K2002Device.RouteSubsystem)
            Me.BindSubsystem(Me.K2002Device.SenseSubsystem)
            AddHandler Me.K2002Device.SystemSubsystem.PropertyChanged, AddressOf Me.HandleSystemSubsystemPropertyChange
            Me.HandlePropertyChanged(Me.K2002Device.SystemSubsystem, NameOf(VI.K2002.SystemSubsystem.SupportsScanCardOption))
        End If
    End Sub

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As VI.K2002.SystemSubsystem, ByVal propertyName As String)
        If subsystem IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.SystemSubsystemBase.SupportsScanCardOption)
                    Me._FourWireResistanceScanMenu.Enabled = subsystem.SupportsScanCardOption
                    Me._FourWireResistanceTimerScanMenu.Enabled = subsystem.SupportsScanCardOption
            End Select
        End If
    End Sub

    ''' <summary> Handles the system subsystem property change. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub HandleSystemSubsystemPropertyChange(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.K2002.SystemSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.HandleSystemSubsystemPropertyChange), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.K2002.SystemSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub


#End Region

#Region " FOUR WIRE RESISTANCE SCAN "

    ''' <summary> The four wire resistance scan menu. </summary>
    Private ReadOnly _FourWireResistanceScanMenu As Windows.Forms.ToolStripMenuItem

    ''' <summary> Four wire resistance scan make menu. </summary>
    ''' <remarks> David, 2020-04-07. </remarks>
    ''' <returns> A Windows.Forms.ToolStripMenuItem. </returns>
    Private Function FourWireResistanceScanMakeMenu() As Windows.Forms.ToolStripMenuItem
        Dim infoMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceScanInfoMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Info",
            .ToolTipText = "Displays information about the four wire resistor scan"
        }
        AddHandler infoMenuItem.Click, AddressOf Me.FourWireResistanceScanInfoMenuItem_Click

        Dim configureMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceScanConfigureMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Configure",
            .ToolTipText = "Configures four wire resistance scan"
        }
        AddHandler configureMenuItem.Click, AddressOf Me.FourWireResistanceScanConfigureMenuItem_Click

        Dim usingLibraryFunctionsMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceLibraryFunctionsMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Using VI Library Function",
            .ToolTipText = "Check for using library functions; otherwise, messages are used",
            .CheckOnClick = True,
            .Checked = False
        }
        AddHandler usingLibraryFunctionsMenuItem.CheckedChanged, AddressOf Me.UsingLibraryFunctionsMenuItem_CheckedChanged

        Dim executeMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceScanExecuteMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Execute",
            .ToolTipText = "Executes the four wire resistance scan"
        }
        AddHandler executeMenuItem.Click, AddressOf Me.FourWireResistanceScanExecuteMenuItem_Click

        Dim mainMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceScanMenuItem",
            .Size = New System.Drawing.Size(192, 22),
            .Text = "Four wire resistance scan...",
            .ToolTipText = "Selects four wire resistance scan options",
            .Enabled = False
        }
        mainMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {infoMenuItem, usingLibraryFunctionsMenuItem, configureMenuItem, executeMenuItem})
        Me.ScanView.AddMenuItem(mainMenuItem)
        Return mainMenuItem
    End Function

    ''' <summary> Gets or sets the using library functions. </summary>
    ''' <value> The using library functions. </value>
    Private Property UsingLibraryFunctions As Boolean

    ''' <summary>
    ''' Event handler. Called by UsingLibraryFunctionsMenuItem for checked changed events.
    ''' </summary>
    ''' <remarks> David, 2020-04-07. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UsingLibraryFunctionsMenuItem_CheckedChanged(sender As Object, e As EventArgs)
        Dim menuItem As Windows.Forms.ToolStripMenuItem = TryCast(sender, Windows.Forms.ToolStripMenuItem)
        Me.UsingLibraryFunctions = Nullable.Equals(True, menuItem?.Checked)
    End Sub

    ''' <summary>
    ''' Event handler. Called by FourWireResistanceScanInfoMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-04-07. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FourWireResistanceScanInfoMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} displaying {NameOf(isr.VI.K2002.RouteSubsystem.BuildFourWireResistanceScanHelpMessage).SplitWords}"
            Core.Controls.PopupContainer.PopupInfo(Me, isr.VI.K2002.RouteSubsystem.BuildFourWireResistanceScanHelpMessage(), Me.Location, Me.Size)
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by FourWireResistanceScanConfigureMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-04-07. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FourWireResistanceScanConfigureMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan).SplitWords}"
            If Me.UsingLibraryFunctions Then
                isr.VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(Me.K2002Device, ArmSources.Immediate, 1, TimeSpan.FromMilliseconds(600))
            Else
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(Me.K2002Device.Session, "IMM", 1, TimeSpan.FromMilliseconds(600))
            End If
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by FourWireResistanceScanExecuteMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-04-07. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FourWireResistanceScanExecuteMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Dim result As (Status As String, Reading As String, Elapsed As TimeSpan)
            Dim bitmask As Integer = Me.K2002Device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull)
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan).SplitWords}"
            If Me.UsingLibraryFunctions Then
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(Me.K2002Device, bitmask)
                activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.FetchFourWireResistanceScan).SplitWords}"
                result = VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(Me.K2002Device)
            Else
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(Me.K2002Device.Session, bitmask)
                activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.FetchFourWireResistanceScan).SplitWords}"
                result = VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(Me.K2002Device.Session, bitmask)
            End If
            Core.Controls.PopupContainer.PopupInfo(Me, $"Status: {result.Status}
Values: {result.Reading}
Elapsed: {result.Elapsed.TotalMilliseconds:0}ms", Me.Location, Me.Size)
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " FOUR WIRE RESISTANCE TIMER SCAN "

    ''' <summary> The four wire resistance timer scan menu. </summary>
    Private ReadOnly _FourWireResistanceTimerScanMenu As Windows.Forms.ToolStripMenuItem

    ''' <summary> Four wire resistance timer scan make menu. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="visible"> True to show, false to hide. </param>
    ''' <returns> A Windows.Forms.ToolStripMenuItem. </returns>
    Private Function FourWireResistanceTimerScanMakeMenu(ByVal visible As Boolean) As Windows.Forms.ToolStripMenuItem
        Dim infoMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceTimerScanInfoMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Info",
            .ToolTipText = "Displays information about the four wire resistor timer scan"
        }
        AddHandler infoMenuItem.Click, AddressOf Me.FourWireResistanceTimerScanInfoMenuItem_Click

        Dim usingLibraryFunctionsMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceLibraryFunctionsMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Using VI Library Function",
            .ToolTipText = "Check for using library functions; otherwise, messages are used",
            .CheckOnClick = True,
            .Checked = False
        }
        AddHandler usingLibraryFunctionsMenuItem.CheckedChanged, AddressOf Me.UsingLibraryFunctionsMenuItem_CheckedChanged

        Dim configureMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceTimerScanConfigureMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Configure",
            .ToolTipText = "Configures four wire resistance timer scan"
        }
        AddHandler configureMenuItem.Click, AddressOf Me.FourWireResistanceTimerScanConfigureMenuItem_Click

        Dim executeMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceScanExecuteMenuItem",
            .Size = New System.Drawing.Size(126, 22),
            .Text = "Execute",
            .ToolTipText = "Executes the four wire resistance timer scan"
        }
        AddHandler executeMenuItem.Click, AddressOf Me.FourWireResistanceTimerScanExecuteMenuItem_Click

        Dim mainMenuItem As New Windows.Forms.ToolStripMenuItem With {
            .Name = "FourWireResistanceTimerScanMenuItem",
            .Size = New System.Drawing.Size(192, 22),
            .Text = "Four wire resistance timer scan...",
            .ToolTipText = "Selects four wire resistance time scan options",
            .Enabled = False,
            .Visible = visible
        }
        mainMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {infoMenuItem, usingLibraryFunctionsMenuItem, configureMenuItem, executeMenuItem})
        Me.ScanView.AddMenuItem(mainMenuItem)
        Return mainMenuItem
    End Function

    ''' <summary>
    ''' Event handler. Called by FourWireResistanceTimerScanInfoMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FourWireResistanceTimerScanInfoMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} displaying {NameOf(isr.VI.K2002.RouteSubsystem.BuildFourWireResistanceTimerScanHelpMessage).SplitWords}"
            Core.Controls.PopupContainer.PopupInfo(Me, isr.VI.K2002.RouteSubsystem.BuildFourWireResistanceTimerScanHelpMessage(), Me.Location, Me.Size)
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by FourWireResistanceTimerScanConfigureMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FourWireResistanceTimerScanConfigureMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan)}"
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan).SplitWords}"
            If Me.UsingLibraryFunctions Then
                isr.VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(Me.K2002Device, ArmSources.Timer, 10, TimeSpan.FromMilliseconds(600))
            Else
                VI.K2002.RouteSubsystem.ConfigureFourWireResistanceScan(Me.K2002Device.Session, "TIM", 10, TimeSpan.FromMilliseconds(600))
            End If
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by FourWireResistanceTimerScanExecuteMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FourWireResistanceTimerScanExecuteMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Dim triggerCount As Integer = 10
            Dim result As (Status As String, Reading As String, Elapsed As TimeSpan)
            Dim bitmask As Integer = Me.K2002Device.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.BufferFull)
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan).SplitWords}"
            If Me.UsingLibraryFunctions Then
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(Me.K2002Device, bitmask)
            Else
                VI.K2002.RouteSubsystem.InitiateFourWireResistanceScan(Me.K2002Device.Session, bitmask)
            End If
            activity = $"{Me.K2002Device.ResourceNameCaption} {NameOf(isr.VI.K2002.RouteSubsystem.FetchFourWireResistanceScan).SplitWords}"
            Dim builder As New System.Text.StringBuilder
            Do While triggerCount > 0
                result = If(Me.UsingLibraryFunctions,
                    VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(Me.K2002Device),
                    VI.K2002.RouteSubsystem.FetchFourWireResistanceScan(Me.K2002Device.Session, bitmask))
                builder.AppendLine($"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}")
                Me.K2002Device.Session.ReadStatusRegister()
                triggerCount -= 1
                VI.K2002.RouteSubsystem.ClearBuffer(Me.K2002Device.Session)
            Loop
            Core.Controls.PopupContainer.PopupInfo(Me, builder.ToString, Me.Location, Me.Size)
        Catch ex As Exception
            Me.K2002Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

End Class
