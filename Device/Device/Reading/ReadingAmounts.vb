'---------------------------------------------------------------------------------------------------
' file:		.\Reading\ReadingAmounts.vb
'
' summary:	Reading amounts class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices

''' <summary>
''' Holds and processes an <see cref="ReadingAmount">base class</see> to a single set of
''' instrument readings.
''' </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-02 </para>
''' </remarks>
Public MustInherit Class ReadingAmounts

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> Use this constructor to instantiate this class and set its properties. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.Readings = New ReadingEntityCollection
        Me.RawReading = ReadingAmounts.Empty
        Me.BaseReadings = New ReadingEntityCollection
    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <param name="model"> The value. </param>
    Protected Sub New(ByVal model As ReadingAmounts)
        Me.New()
        If model IsNot Nothing Then
            Me.Readings = New ReadingEntityCollection(model.Readings)
            Me.BaseReadings = New ReadingEntityCollection(model.BaseReadings)
            Me.RawReading = model.RawReading
            Me.ActiveReadingType = model._ActiveReadingType
            Me.Elements = model.Elements
        End If
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <remarks> David, 2020-07-27. </remarks>
    ''' <param name="model"> The value. </param>
    ''' <returns> A copy of this object. </returns>
    Public MustOverride Function Clone(ByVal model As ReadingAmounts) As ReadingAmounts

#End Region

#Region " PARSE "

    ''' <summary> Gets the default delimiter. </summary>
    Public Const DefaultDelimiter As String = ","

    ''' <summary> Applies the measured data. </summary>
    ''' <param name="values">    A record of one or more reading values or an empty string to clear
    '''                            the current readings. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> <c>True</c> if parsed; <c>False</c> otherwise. </returns>
    Public Overridable Function TryApplyReadings(ByVal values As String, ByVal delimiter As String) As Boolean

        If values Is Nothing Then
            Me._RawReading = ReadingAmounts.Empty
            Return False
        ElseIf values.Length = 0 Then
            ' indicate that we do not have a valid value
            Me.Reset()
            Me._RawReading = ReadingAmounts.Empty
            Return True
        Else
            Me._RawReading = values
            Return Me.TryApplyReadings(New Queue(Of String)(values.Split(CChar(delimiter))))
        End If

    End Function

    ''' <summary> Applies the measured data. </summary>
    ''' <param name="values"> A record of one or more readings. </param>
    ''' <returns> <c>True</c> if parsed; <c>False</c> otherwise. </returns>
    Public Overridable Function TryApplyReadings(ByVal values As String) As Boolean
        Return Me.TryApplyReadings(values, ReadingAmounts.DefaultDelimiter)
    End Function

    ''' <summary> Applies the measured data. </summary>
    ''' <param name="values"> The reading values. </param>
    ''' <returns> <c>True</c> if parsed; <c>False</c> otherwise. </returns>
    Public Overridable Function TryApplyReadings(ByVal values As String()) As Boolean
        Return Me.TryApplyReadings(New Queue(Of String)(values))
    End Function

    ''' <summary> Applies  the measured data. </summary>
    ''' <param name="values"> Specifies the values. </param>
    ''' <returns> <c>True</c> if applies; <c>False</c> otherwise. </returns>
    Public Overridable Function TryApplyReadings(ByVal values As Queue(Of String)) As Boolean

        Dim affirmative As Boolean = False
        If values Is Nothing Then
            Me._RawReading = ReadingAmounts.Empty
        ElseIf values.Count < Me._Readings.Count Then
            ' if the queue has fewer values than expected, reset to 
            ' indicate that the value is invalid
            Me.Reset()
            Me._RawReading = ReadingAmounts.Empty
        Else
            Dim builder As New System.Text.StringBuilder
            affirmative = True
            For Each readingItem As ReadingEntity In Me._Readings
                Dim valueReading As String = values.Dequeue
                If builder.Length > 0 Then builder.Append(ReadingAmounts.DefaultDelimiter)
                builder.Append(valueReading)
                If readingItem.IncludesUnitsSuffix Then
                    Dim unitsSuffix As String = ReadingAmounts.ParseUnitSuffix(valueReading)
                    valueReading = ReadingAmounts.TrimUnits(valueReading, unitsSuffix)
                    affirmative = affirmative And readingItem.TryApplyReading(valueReading, unitsSuffix)
                Else
                    affirmative = affirmative And readingItem.TryApplyReading(valueReading)
                End If
            Next
            Me._RawReading = builder.ToString
        End If
        Return affirmative

    End Function

    ''' <summary>
    ''' Parses all <see cref="ReadingAmount">reading elements</see> in a
    ''' <see cref="ReadingElementTypes">set of reading amounts</see>.
    ''' Use for parsing reading elements that were set before limits were set.
    ''' </summary>
    ''' <param name="values"> Specifies a reading element. </param>
    ''' <returns> <c>True</c> if parsed; <c>False</c> otherwise. </returns>
    Public Overridable Function TryApplyReadings(ByVal values As ReadingAmounts) As Boolean

        If values Is Nothing Then
            Me._RawReading = ReadingAmounts.Empty
            Return False
        End If

        ' clear all as we start from a fresh slate.
        Me.Reset()

        Return Me.TryApplyReadings(values.Readings.ToRawReadings)

    End Function

    ''' <summary> Builds meta status. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns> The <see cref="MetaStatus"/> <see cref="MetaStatus.StatusValue"/> . </returns>
    Protected MustOverride Function BuildMetaStatus(ByVal status As Long) As Long

    ''' <summary> Attempts to evaluate using the applied reading and given status. </summary>
    ''' <param name="status"> The status. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryEvaluate(ByVal status As Long) As Boolean
        status = Me.BuildMetaStatus(status)
        Dim affirmative As Boolean = True
        For Each readingItem As ReadingEntity In Me._Readings
            affirmative = affirmative And readingItem.TryEvaluate(status)
        Next
        Return affirmative
    End Function

    ''' <summary> Resets the measured outcomes. </summary>
    Public Overridable Sub Reset()
        Me.Readings.Reset()
    End Sub

    ''' <summary> Attempts to parse from the given data. </summary>
    ''' <remarks>
    ''' Parsing takes two steps. First all values are assigned. Then the status is used to evaluate
    ''' the measured amounts.
    ''' </remarks>
    ''' <param name="readings"> The readings. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryParse(ByVal readings As String) As Boolean
        Dim affirmative As Boolean

        If Me.TryApplyReadings(readings) Then
            Dim statusValue As Long = 0
            affirmative = Me.TryEvaluate(statusValue)
        Else
            affirmative = False
        End If
        Return affirmative
    End Function

    ''' <summary> Attempts to parse from the given data. </summary>
    ''' <param name="values"> A queue of reading values. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryParse(ByVal values As Queue(Of String)) As Boolean
        Dim affirmative As Boolean
        If Me.TryApplyReadings(values) Then
            Dim statusValue As Long = 0
            affirmative = Me.TryEvaluate(statusValue)
        Else
            affirmative = False
        End If
        Return affirmative
    End Function

    ''' <summary> Enumerates parse many in this collection. </summary>
    ''' <remarks> David, 2020-07-27. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="baseReading">    Specifies the base reading which includes the limits for all
    '''                               reading elements. </param>
    ''' <param name="readingRecords"> The reading records. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process parse many in this collection.
    ''' </returns>
    Protected Overridable Function ParseMany(ByVal baseReading As ReadingAmounts, ByVal readingRecords As String) As IList(Of ReadingAmounts)
        Dim readingsArray As New List(Of ReadingAmounts)
        If readingRecords Is Nothing Then
            Throw New ArgumentNullException(NameOf(readingRecords))
        ElseIf baseReading Is Nothing Then
            Throw New ArgumentNullException(NameOf(baseReading))
        ElseIf baseReading.Readings Is Nothing Then
            Throw New InvalidOperationException("Base reading readings not defined")
        ElseIf baseReading.Readings.Count = 0 Then
            Throw New InvalidOperationException("Base reading has not readings")
        ElseIf readingRecords.Length > 0 Then
            Dim values As New Queue(Of String)(readingRecords.Split(ReadingAmounts.DefaultDelimiter.ToCharArray))
            If values.Count >= baseReading.Readings.Count Then
                For j As Integer = 0 To values.Count \ baseReading.Readings.Count - 1
                    Dim reading As ReadingAmounts = Me.Clone(baseReading)
                    reading.TryParse(values)
                    readingsArray.Add(reading)
                Next
            End If
        End If
        Return readingsArray.ToArray
    End Function

    ''' <summary> Parses reading data into a readings array. </summary>
    ''' <remarks> David, 2020-09-04. </remarks>
    ''' <param name="baseReading">    Specifies the base reading which includes the limits for all
    '''                               reading elements. </param>
    ''' <param name="readingRecords"> The reading records. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process parse multi in this collection.
    ''' </returns>
    Public Overridable Function Parse(ByVal baseReading As ReadingAmounts, ByVal readingRecords As String) As IList(Of ReadingAmounts)
        Return Me.ParseMany(baseReading, readingRecords)
    End Function


#End Region

#Region " UNITS "

    ''' <summary> Adds a unit to the units dictionary. </summary>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="suffix"> The suffix. </param>
    ''' <param name="unit">   The unit. </param>
    Public Shared Sub AddUnit(ByVal suffix As String, ByVal unit As Arebis.TypedUnits.Unit)
        If String.IsNullOrWhiteSpace(suffix) Then Throw New ArgumentNullException(NameOf(suffix))
        If unit Is Nothing Then Throw New ArgumentNullException(NameOf(unit))
        If ReadingAmounts.UnitsDictionary.Keys.Contains(suffix) Then
            If Not unit.Equals(ReadingAmounts.UnitsDictionary(suffix)) Then
                Throw New InvalidOperationException($"Mismatch detected: Attempting to add {unit.Symbol} unit where existing {suffix} is {ReadingAmounts.UnitsDictionary(suffix).Symbol}")
            End If
        Else
            ReadingAmounts._UnitsDictionary.Add(suffix, unit)
        End If
    End Sub

    ''' <summary> Dictionary of units. </summary>
    Private Shared _UnitsDictionary As Dictionary(Of String, Arebis.TypedUnits.Unit)

    ''' <summary> Returns the Unit Parser hash. </summary>
    ''' <returns>
    ''' A Dictionary for translating SCPI unit names to <see cref="Arebis.StandardUnits">standard
    ''' units</see>.
    ''' </returns>
    Public Shared Function UnitsDictionary() As IDictionary(Of String, Arebis.TypedUnits.Unit)
        If ReadingAmounts._UnitsDictionary Is Nothing Then
            ReadingAmounts._UnitsDictionary = New Dictionary(Of String, Arebis.TypedUnits.Unit)
            Dim dix3 As Dictionary(Of String, Arebis.TypedUnits.Unit) = ReadingAmounts._UnitsDictionary
            dix3.Add("ADC", Arebis.StandardUnits.ElectricUnits.Ampere)
            dix3.Add("OHM", Arebis.StandardUnits.ElectricUnits.Ohm)
            dix3.Add("OHM4W", Arebis.StandardUnits.ElectricUnits.Ohm)
            dix3.Add("VDC", Arebis.StandardUnits.ElectricUnits.Volt)
            dix3.Add("SECS", Arebis.StandardUnits.TimeUnits.Second)
            dix3.Add("RDNG#", Arebis.StandardUnits.UnitlessUnits.Count)
        End If
        Return ReadingAmounts._UnitsDictionary
    End Function

    ''' <summary> Tries to parse a unit from the unit suffix of the reading. </summary>
    ''' <param name="reading"> Specifies the reading text. </param>
    ''' <param name="unit">    [in,out] The unit. </param>
    ''' <returns> <c>True</c> if parsed. </returns>
    Public Shared Function TryParseUnit(ByVal reading As String, ByRef unit As Arebis.TypedUnits.Unit) As Boolean
        Dim suffix As String = ReadingAmounts.ParseUnitSuffix(reading)
        unit = If(String.IsNullOrEmpty(suffix) OrElse Not ReadingAmounts.UnitsDictionary.Keys.Contains(suffix),
            Nothing,
            ReadingAmounts.UnitsDictionary(suffix))
        Return unit IsNot Nothing
    End Function

    ''' <summary> Extracts the unit suffix from the reading. </summary>
    ''' <param name="reading"> The reading value that includes units as a suffix. </param>
    ''' <returns> The unit suffix. </returns>
    Public Shared Function ParseUnitSuffix(ByVal reading As String) As String
        Dim suffix As String = String.Empty
        If Not String.IsNullOrWhiteSpace(reading) AndAlso ReadingAmounts.UnitsDictionary.Keys.Any Then
            For Each suffix In ReadingAmounts.UnitsDictionary.Keys
                If reading.EndsWith(suffix, StringComparison.OrdinalIgnoreCase) Then
                    Exit For
                End If
            Next
        End If
        Return suffix
    End Function

    ''' <summary> Trims unit suffixes. </summary>
    ''' <param name="reading">     The raw reading. </param>
    ''' <param name="unitsSuffix"> The units suffix. </param>
    ''' <returns> A string with the units suffix removed. </returns>
    Public Shared Function TrimUnits(ByVal reading As String, ByVal unitsSuffix As String) As String
        If Not (String.IsNullOrWhiteSpace(reading) OrElse String.IsNullOrWhiteSpace(unitsSuffix)) Then
            reading = reading.Substring(0, reading.Length - unitsSuffix.Length)
        End If
        Return reading
    End Function

    ''' <summary> Trims unit suffixes. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A string with the units suffix removed. </returns>
    Public Shared Function TrimUnits(ByVal value As String) As String
        Return ReadingAmounts.TrimUnits(value, ReadingAmounts.ParseUnitSuffix(value))
    End Function

#End Region

#Region " READINGS "

    ''' <summary> The empty reading string. </summary>
    Public Const Empty As String = "nil"

    ''' <summary> Gets the raw reading. </summary>
    ''' <value> The raw reading. </value>
    Public ReadOnly Property RawReading As String

    ''' <summary> Gets the is empty. </summary>
    ''' <value> The is empty. </value>
    Public ReadOnly Property IsEmpty As Boolean
        Get
            Return String.IsNullOrWhiteSpace(Me.RawReading) OrElse String.Equals(Me.RawReading, ReadingAmounts.Empty)
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets a collection of readings that implements the <see cref="ReadingAmount">reading
    ''' base</see>.
    ''' </summary>
    ''' <value> The readings. </value>
    Public ReadOnly Property Readings() As ReadingEntityCollection

    ''' <summary> Gets or sets the base readings. </summary>
    ''' <value> The base readings. </value>
    Public ReadOnly Property BaseReadings() As ReadingEntityCollection

#End Region

#Region " ACTIVE READING "

    ''' <summary> Query if this object has reading elements. </summary>
    ''' <returns> <c>true</c> if reading elements; otherwise <c>false</c> </returns>
    Public Function HasReadingElements() As Boolean
        Return Me.Readings.Any
    End Function

    ''' <summary> Gets or sets the reading elements. </summary>
    ''' <value> The elements. </value>
    Public ReadOnly Property Elements() As VI.ReadingElementTypes

    ''' <summary> Gets or sets the reading type of the active reading entity. </summary>
    ''' <value> The active element. </value>
    Public Property ActiveReadingType As VI.ReadingElementTypes

    ''' <summary> Returns the meta status of the active reading. </summary>
    ''' <returns> The MetaStatus. </returns>
    Public Function ActiveMetaStatus() As MetaStatus
        Return Me.Readings.MetaStatus(Me.ActiveReadingType)
    End Function

    ''' <summary> Active reading amount. </summary>
    ''' <returns> A ReadingAmount. </returns>
    Public Function ActiveReadingAmount() As ReadingAmount
        Return Me.Readings.ReadingAmount(Me.ActiveReadingType)
    End Function

    ''' <summary> Active reading unit symbol. </summary>
    ''' <returns> A String. </returns>
    Public Function ActiveReadingUnitSymbol() As String
        Return If(Me.ActiveReadingAmount Is Nothing, String.Empty, Me.ActiveReadingAmount.Symbol)
    End Function

    ''' <summary> Returns the caption value of the active reading. </summary>
    ''' <returns> A String. </returns>
    Public Function ActiveAmountCaption() As String
        Dim amount As ReadingAmount = Me.ActiveReadingAmount
        Dim result As String
        If amount Is Nothing Then
            If Me.IsEmpty Then
                result = "0x------- "
            Else
                Dim value As ReadingStatus = Me.Readings.ReadingStatus(Me.ActiveReadingType)
                result = If(value Is Nothing, "-.------- :(", value.ToString())
            End If
        ElseIf Me.IsEmpty Then
            result = $"-.------- {amount.Symbol}"
        Else
            result = amount.ToString
        End If
        Return result
    End Function

#End Region

#Region " CUSTOM READING ELEMENTS "

    ''' <summary> Adds a reading entity. </summary>
    ''' <remarks> David, 2020-07-27. </remarks>
    ''' <param name="elementType"> Type of the element. </param>
    Private Sub AddReadingEntity(ByVal elementType As ReadingElementTypes)
        Me.Readings.AddIf(elementType, Me.ChannelNumber)
        Me.Readings.AddIf(elementType, Me.CurrentReading)
        Me.Readings.AddIf(elementType, Me.Limits)
        Me.Readings.AddIf(elementType, Me.LimitStatus)
        Me.Readings.AddIf(elementType, Me.PrimaryReading)
        Me.Readings.AddIf(elementType, Me.ReadingNumber)
        Me.Readings.AddIf(elementType, Me.ResistanceReading)
        Me.Readings.AddIf(elementType, Me.Seconds)
        Me.Readings.AddIf(elementType, Me.SecondaryReading)
        Me.Readings.AddIf(elementType, Me.StatusReading)
        Me.Readings.AddIf(elementType, Me.Timestamp)
        Me.Readings.AddIf(elementType, Me.VoltageReading)
    End Sub

    ''' <summary> Adds a reading entities. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="elementTypes"> List of types of the elements. </param>
    Private Sub AddReadingEntities(ByVal elementTypes As ReadingElementTypes)
        For Each elementType As ReadingElementTypes In Me.BaseReadings.ElementTypes
            Me.AddReadingEntity(elementType And elementTypes)
        Next
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks>
    ''' Adds reading elements in the order they are returned by the instrument so as to automate
    ''' parsing of these data.
    ''' </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub Initialize(ByVal value As VI.ReadingElementTypes)
        Me._Elements = value
        Me._Readings.Clear()
        Me.AddReadingEntities(value)
        Me.Readings.IncludeUnitSuffixIf(value)
    End Sub

    ''' <summary> Gets or sets the <see cref="isr.VI.ReadingAmount">channel number</see>. </summary>
    ''' <value> The channel number. </value>
    Public Property ChannelNumber() As VI.ReadingValue

    ''' <summary>
    ''' Gets or sets the source meter <see cref="isr.VI.MeasuredAmount">current reading</see>.
    ''' </summary>
    ''' <value> The current reading. </value>
    Public Property CurrentReading() As VI.MeasuredAmount

    ''' <summary> Gets or sets the <see cref="isr.VI.ReadingAmount">limits</see>. </summary>
    ''' <value> The limits. </value>
    Public Property Limits() As VI.ReadingValue

    ''' <summary>
    ''' Gets or sets the <see cref="isr.VI.ReadingValue">alarm limits threshold state</see>.
    ''' </summary>
    ''' <value> The limit status. </value>
    Public Property LimitStatus() As VI.ReadingValue

    ''' <summary> Gets or sets the <see cref="isr.VI.ReadingAmount">reading number</see>. </summary>
    ''' <value> The reading number. </value>
    Public Property ReadingNumber() As VI.ReadingValue

    ''' <summary> Gets or sets the <see cref="isr.VI.MeasuredAmount">primary reading</see>. </summary>
    ''' <value> The primary reading. </value>
    Public Property PrimaryReading() As VI.MeasuredAmount

    ''' <summary>
    ''' Gets or sets the <see cref="isr.VI.MeasuredAmount">Secondary reading</see>.
    ''' </summary>
    ''' <value> The secondary reading. </value>
    Public Property SecondaryReading() As VI.MeasuredAmount

    ''' <summary>
    ''' Gets or sets the source meter <see cref="isr.VI.MeasuredAmount">resistance reading</see>.
    ''' </summary>
    ''' <value> The resistance reading. </value>
    Public Property ResistanceReading() As VI.MeasuredAmount

    ''' <summary>
    ''' Gets or sets the <see cref="isr.VI.ReadingValue">time span in seconds</see>.
    ''' </summary>
    ''' <value> The seconds. </value>
    Public Property Seconds() As VI.ReadingValue

    ''' <summary>
    ''' Gets or sets the source meter <see cref="isr.VI.MeasuredAmount">status reading</see>.
    ''' </summary>
    ''' <value> The status reading. </value>
    Public Property StatusReading() As VI.ReadingStatus

    ''' <summary> Gets or sets the timestamp <see cref="isr.VI.ReadingAmount">reading</see>. </summary>
    ''' <value> The timestamp. </value>
    Public Property Timestamp() As VI.ReadingAmount

    ''' <summary>
    ''' Gets or sets the source meter <see cref="isr.VI.MeasuredAmount">voltage reading</see>.
    ''' </summary>
    ''' <value> The voltage reading. </value>
    Public Property VoltageReading() As VI.MeasuredAmount

#End Region

End Class

