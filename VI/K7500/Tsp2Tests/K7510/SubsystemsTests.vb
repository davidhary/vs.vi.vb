''' <summary> K7510 Subsystems unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k7510tsp")>
Public Class SubsystemsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K7500Tests.ResourceSettings.Get.Exists, $"{GetType(K7500Tests.ResourceSettings)} settings not found")
        Assert.IsTrue(K7500Tests.SubsystemsSettings.Get.Exists, $"{GetType(K7500Tests.SubsystemsSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " STATUS SUSBSYSTEM "

    ''' <summary> Opens session check status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
    ''' <param name="resourceInfo">     Information describing the resource. </param>
    ''' <param name="subsystemsInfo">   Information describing the subsystems. </param>
    Private Shared Sub OpenSessionCheckStatus(ByVal readErrorEnabled As Boolean, ByVal resourceInfo As ResourceSettings, ByVal subsystemsInfo As SubsystemsSettings)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp2.K7500.K7500Device = VI.Tsp2.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValues(device.Session, resourceInfo, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertSessionOpenValues(device.Session, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceModel(device.StatusSubsystemBase, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceErrors(device.StatusSubsystemBase, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertTerminationValues(device.Session, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertLineFrequency(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertIntegrationPeriod(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertClearSessionDeviceErrors(device, subsystemsInfo)
                If readErrorEnabled Then isr.VI.DeviceTests.DeviceManager.AssertReadingDeviceErrors(device, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOrphanMessages(device.StatusSubsystemBase)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> A test for Open Session and status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionCheckStatusTest()
        SubsystemsTests.OpenSessionCheckStatus(False, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) tests open session read device errors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionReadDeviceErrorsTest()
        SubsystemsTests.OpenSessionCheckStatus(True, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

#End Region

#Region " BUFFER SUBSYSTEM TEST "

    ''' <summary> Reads buffer subsystem information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub ReadBufferSubsystemInfo(ByVal subsystem As VI.BufferSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Dim actualCapacilty As Integer = subsystem.QueryCapacity.GetValueOrDefault(-1)
        Dim expectedcapacilty As Integer = SubsystemsSettings.Get.BufferCapacity
        Assert.AreEqual(expectedcapacilty, actualCapacilty, $"Buffer capacity")
        Dim actualFirstPointNumber As Integer = subsystem.QueryFirstPointNumber.GetValueOrDefault(-1)
        Dim expectedFirstPointNumber As Integer = SubsystemsSettings.Get.BufferFirstPointNumber
        Assert.AreEqual(expectedFirstPointNumber, actualFirstPointNumber, $"Buffer First Point Number")
        Dim actualLastPointNumber As Integer = subsystem.QueryLastPointNumber.GetValueOrDefault(-1)
        Dim expectedLastPointNumber As Integer = SubsystemsSettings.Get.BufferLastPointNumber
        Assert.AreEqual(expectedLastPointNumber, actualLastPointNumber, $"Buffer Last Point Number")
        Dim expectedFillOnceEnabled As Boolean = SubsystemsSettings.Get.BufferFillOnceEnabled
        Dim actualFillOnceEnabled As Boolean = subsystem.QueryFillOnceEnabled.GetValueOrDefault(Not expectedFillOnceEnabled)
        Assert.AreEqual(expectedFillOnceEnabled, actualFillOnceEnabled, $"Initial fill once enabled")
    End Sub

    ''' <summary> (Unit Test Method) tests buffer subsystem initial values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub BufferSubsystemInitialValuesTest()
        Using device As VI.Tsp2.K7500.K7500Device = VI.Tsp2.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.ReadBufferSubsystemInfo(device.Buffer1Subsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM INITIAL VALUES TEST "

    ''' <summary> Tests initial values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub TestInitialValues(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase)

        Dim expectedPowerLineCycles As Double = SubsystemsSettings.Get.InitialPowerLineCycles
        Dim actualPowerLineCycles As Double? = subsystem.QueryPowerLineCycles
        Assert.IsTrue(actualPowerLineCycles.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles.Value, SubsystemsSettings.Get.LineFrequency / TimeSpan.TicksPerSecond,
                        $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")

        Dim expectedBoolean As Boolean = SubsystemsSettings.Get.InitialAutoRangeEnabled
        Dim actualBoolean As Boolean? = subsystem.QueryAutoRangeEnabled
        Assert.IsTrue(actualBoolean.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoRangeEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoRangeEnabled)}")

        Dim expectedFunctionMode As VI.MultimeterFunctionModes = SubsystemsSettings.Get.InitialMultimeterFunction
        Dim actualFunctionMode As VI.MultimeterFunctionModes? = subsystem.QueryFunctionMode
        Assert.IsTrue(actualBoolean.HasValue, $"Failed reading {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.FunctionMode)}")
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode.Value,
                        $"Failed initial {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.FunctionMode)}")

        Dim expectedInputImpedanceMode As VI.InputImpedanceModes = SubsystemsSettings.Get.InitialInputImpedanceMode
        Dim actualInputImpedanceMode As VI.InputImpedanceModes? = subsystem.QueryInputImpedanceMode
        Assert.IsTrue(actualInputImpedanceMode.HasValue, $"Failed reading {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.InputImpedanceMode)}")
        Assert.AreEqual(expectedInputImpedanceMode, actualInputImpedanceMode.Value,
                        $"Failed initial {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.InputImpedanceMode)}")

        expectedBoolean = SubsystemsSettings.Get.InitialFilterEnabled
        actualBoolean = subsystem.QueryFilterEnabled
        Assert.IsTrue(actualBoolean.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")

        expectedBoolean = SubsystemsSettings.Get.InitialMovingAverageFilterEnabled
        actualBoolean = subsystem.QueryMovingAverageFilterEnabled
        Assert.IsTrue(actualBoolean.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")

        Dim expectedFilterWindow As Double = SubsystemsSettings.Get.InitialFilterWindow
        Dim actualFilterWindow As Double? = subsystem.QueryFilterWindow
        Assert.IsTrue(actualBoolean.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
        Assert.AreEqual(expectedFilterWindow, actualFilterWindow.Value, 0.1 * expectedFilterWindow,
                        $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")

        Dim expectedFilterCount As Integer = SubsystemsSettings.Get.InitialFilterCount
        Dim actualFilterCount As Integer? = subsystem.QueryFilterCount
        Assert.IsTrue(actualFilterCount.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
        Assert.AreEqual(expectedFilterCount, actualFilterCount.Value, $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")

    End Sub

    ''' <summary> (Unit Test Method) tests multimeter subsystem initial values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemInitialValuesTest()
        Using device As VI.Tsp2.K7500.K7500Device = VI.Tsp2.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.TestInitialValues(device.MultimeterSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM: TEST VOLTAGE MEASUREMENT "

    ''' <summary> Applies the input impedance mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyInputImpedanceMode(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As VI.InputImpedanceModes)
        Dim expectedInputImpedanceMode As VI.InputImpedanceModes = value
        Dim actualInputImpedanceMode As VI.InputImpedanceModes? = subsystem.ApplyInputImpedanceMode(expectedInputImpedanceMode)
        Assert.IsTrue(actualInputImpedanceMode.HasValue, $"Failed applying {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.InputImpedanceMode)}")
        Assert.AreEqual(expectedInputImpedanceMode, actualInputImpedanceMode.Value,
                        $"Failed applying {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.InputImpedanceMode)}")
    End Sub

    ''' <summary> Applies the automatic zero once described by subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ApplyAutoZeroOnce(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase)
        subsystem.AutoZeroOnce()
        Dim expectedBoolean As Boolean = False
        Dim actualBoolean As Boolean? = subsystem.QueryAutoZeroEnabled()
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroOnce)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroOnce)} {NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)} still enabled")
    End Sub

    ''' <summary> Applies the automatic zero enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyAutoZeroEnabled(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyAutoZeroEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}")
    End Sub

    ''' <summary> Applies the filter enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyFilterEnabled(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyFilterEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
    End Sub

    ''' <summary> Applies the moving average filter enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyMovingAverageFilterEnabled(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyMovingAverageFilterEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
    End Sub

    ''' <summary> Applies the filter count. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyFilterCount(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Integer)
        Dim expectedInteger As Integer = value
        Dim actualInteger As Integer? = subsystem.ApplyFilterCount(value)
        Assert.IsTrue(actualInteger.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
        Assert.AreEqual(expectedInteger, actualInteger.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
    End Sub

    ''' <summary> Applies the filter window. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyFilterWindow(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyFilterWindow(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
    End Sub

    ''' <summary> Applies the power line cycles. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyPowerLineCycles(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyPowerLineCycles(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
    End Sub

    ''' <summary> Applies the range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub ApplyRange(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyRange(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
    End Sub

    ''' <summary> Measures the given subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub Measure(ByVal subsystem As VI.Tsp2.MultimeterSubsystemBase)
        Dim expectedTimeSpan As TimeSpan = subsystem.EstimateMeasurementTime
        Dim timeout As Integer = Math.Max(10000, 4 * CInt(expectedTimeSpan.TotalMilliseconds))
        subsystem.Session.StoreCommunicationTimeout(TimeSpan.FromMilliseconds(timeout))
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim value As Double? = subsystem.MeasurePrimaryReading()
        Dim actualTimespan As TimeSpan = sw.Elapsed
        Assert.IsTrue(value.HasValue, $"Failed reading {GetType(VI.Tsp2.MultimeterSubsystemBase)}.{NameOf(VI.Tsp2.MultimeterSubsystemBase.PrimaryReadingValue)}")
        Assert.IsTrue(value.Value > -0.001, $"Failed reading positive {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PrimaryReadingValue)}")
        Assert.IsTrue(actualTimespan >= expectedTimeSpan, $"Reading too short; expected {expectedTimeSpan} actual {actualTimespan}")
        Dim twiceInterval As TimeSpan = expectedTimeSpan.Add(expectedTimeSpan)
        Assert.IsTrue(actualTimespan < twiceInterval, $"Reading too long; expected {twiceInterval} actual {actualTimespan}")
        subsystem.Session.RestoreCommunicationTimeout()
    End Sub

    ''' <summary> (Unit Test Method) tests multimeter subsystem voltage measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemVoltageMeasurementTest()
        Using device As VI.Tsp2.K7500.K7500Device = VI.Tsp2.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                Dim powerLineCycles As Double = 5
                Dim FilterCount As Integer = 20
                Dim window As Double = 0.095
                Dim range As Double = 0.1
                SubsystemsTests.ApplyPowerLineCycles(device.MultimeterSubsystem, powerLineCycles)
                SubsystemsTests.ApplyRange(device.MultimeterSubsystem, range)
                SubsystemsTests.ApplyAutoZeroEnabled(device.MultimeterSubsystem, False)
                SubsystemsTests.ApplyAutoZeroOnce(device.MultimeterSubsystem)
                SubsystemsTests.ApplyInputImpedanceMode(device.MultimeterSubsystem, SubsystemsSettings.Get.TestInputImpedanceMode)
                SubsystemsTests.ApplyMovingAverageFilterEnabled(device.MultimeterSubsystem, False) ' use repeat filter.
                SubsystemsTests.ApplyFilterCount(device.MultimeterSubsystem, FilterCount)
                SubsystemsTests.ApplyFilterWindow(device.MultimeterSubsystem, window)
                SubsystemsTests.ApplyFilterEnabled(device.MultimeterSubsystem, True)
                SubsystemsTests.Measure(device.MultimeterSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " DIGITAL INPUT OUTPUT SUBSYSTEM TEST "

    ''' <summary> Reads DIgitalInputOutput subsystem information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub ReadDigitalInputOutputSubsystemInfo(ByVal subsystem As VI.Tsp2.DigitalInputOutputSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Dim actualLineCount As Integer = subsystem.DigitalLines.Count
        Dim expectedLineCount As Integer = SubsystemsSettings.Get.DigitalLineCount
        Assert.AreEqual(expectedLineCount, actualLineCount, $"Digital Input Output line count")
        Dim expectedMode As isr.VI.DigitalLineMode = isr.VI.DigitalLineMode.DigitalInput
        Dim actualMode As isr.VI.DigitalLineMode? = subsystem.QueryDigitalLineMode(SubsystemsSettings.Get.DigitalInputLineNumber)
        Assert.IsTrue(actualMode.HasValue, $"Digital line mode {SubsystemsSettings.Get.DigitalInputLineNumber} has mode value")
        Assert.AreEqual(expectedMode, actualMode.Value, $"Digital {SubsystemsSettings.Get.DigitalInputLineNumber} line mode")
    End Sub

    ''' <summary> (Unit Test Method) tests Digital Input Output subsystem initial values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DigitalInputOutputSubsystemInitialValuesTest()
        Using device As VI.Tsp2.K7500.K7500Device = VI.Tsp2.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.ReadDigitalInputOutputSubsystemInfo(device.DigitalInputOutputSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> Writes a read digital input output. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub WriteReadDigitalInputOutput(ByVal subsystem As VI.Tsp2.DigitalInputOutputSubsystemBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Dim expectedMode As isr.VI.DigitalLineMode = isr.VI.DigitalLineMode.DigitalOutput
        Dim actualMode As isr.VI.DigitalLineMode? = subsystem.ApplyDigitalLineMode(SubsystemsSettings.Get.DigitalOutputLineNumber, expectedMode)
        Assert.IsTrue(actualMode.HasValue, $"Digital line mode {SubsystemsSettings.Get.DigitalOutputLineNumber} has mode value")
        Assert.AreEqual(expectedMode, actualMode.Value, $"Digital {SubsystemsSettings.Get.DigitalOutputLineNumber} Line mode")
        Dim expectedOutputValue As isr.VI.DigitalLineState = isr.VI.DigitalLineState.High
        subsystem.WriteDigitalLineState(SubsystemsSettings.Get.DigitalOutputLineNumber, expectedOutputValue)
        Dim actualOutputValue As isr.VI.DigitalLineState? = subsystem.QueryDigitalLineState(SubsystemsSettings.Get.DigitalInputLineNumber)
        Assert.IsTrue(actualOutputValue.HasValue, $"Digital input {SubsystemsSettings.Get.DigitalInputLineNumber} state has value")
        Assert.AreEqual(expectedOutputValue, actualOutputValue.Value,
                        $"Digital input #{SubsystemsSettings.Get.DigitalInputLineNumber} equals digital output #{SubsystemsSettings.Get.DigitalOutputLineNumber} ")
        expectedOutputValue = isr.VI.DigitalLineState.Low
        subsystem.WriteDigitalLineState(SubsystemsSettings.Get.DigitalOutputLineNumber, expectedOutputValue)
        actualOutputValue = subsystem.QueryDigitalLineState(SubsystemsSettings.Get.DigitalInputLineNumber)
        Assert.IsTrue(actualOutputValue.HasValue, $"Digital output {SubsystemsSettings.Get.DigitalInputLineNumber} state has value")
        Assert.AreEqual(expectedOutputValue, actualOutputValue.Value,
                        $"Digital input #{SubsystemsSettings.Get.DigitalInputLineNumber} equals digital output #{SubsystemsSettings.Get.DigitalOutputLineNumber} ")
    End Sub

    ''' <summary> (Unit Test Method) tests digital input output state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DigitalInputOutputStateTest()
        Using device As VI.Tsp2.K7500.K7500Device = VI.Tsp2.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.WriteReadDigitalInputOutput(device.DigitalInputOutputSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class

