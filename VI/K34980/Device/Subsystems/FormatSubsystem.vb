''' <summary> Defines a Format Subsystem for a Keysight 34980 Meter/Scanner. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class FormatSubsystem
    Inherits VI.FormatSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="FormatSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Elements = ReadingElementTypes.Reading
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " ELEMENTS "

    ''' <summary> Gets the elements query command. </summary>
    ''' <value> The elements query command. </value>
    <Obsolete("Not supported with the 34980A scanner")>
    Protected Overrides Property ElementsQueryCommand As String = String.Empty

    ''' <summary> Gets the elements command format. </summary>
    ''' <value> The elements command format. </value>
    <Obsolete("Not supported with the 34980A scanner")>
    Protected Overrides Property ElementsCommandFormat As String = String.Empty

#End Region

#End Region

End Class
