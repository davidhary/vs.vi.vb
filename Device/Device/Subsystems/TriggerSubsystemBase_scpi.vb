'---------------------------------------------------------------------------------------------------
' file:		.VI\Subsystems\TriggerSubsystemBase_scpi.vb
'
' summary:	Trigger subsystem base scpi class
'           Defines the contract that must be implemented by a SCPI Trigger Subsystem. 
'---------------------------------------------------------------------------------------------------

Partial Public Class TriggerSubsystemBase

#Region " TRIGGER LAYER BYPASS MODE "

    ''' <summary> Defines trigger layer bypass read writes. </summary>
    Private Sub DefineTriggerLayerBypassReadWrites()
        Me._TriggerLayerBypassModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As TriggerLayerBypassModes In [Enum].GetValues(GetType(TriggerLayerBypassModes))
            Me._TriggerLayerBypassModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of trigger layer bypass mode parses. </summary>
    ''' <value> A Dictionary of trigger layer bypass mode parses. </value>
    Public ReadOnly Property TriggerLayerBypassModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported trigger layer bypass modes. </summary>
    Private _SupportedTriggerLayerBypassModes As TriggerLayerBypassModes

    ''' <summary>
    ''' Gets or sets the supported Function Mode. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported trigger layer bypass modes. </value>
    Public Property SupportedTriggerLayerBypassModes() As TriggerLayerBypassModes
        Get
            Return Me._SupportedTriggerLayerBypassModes
        End Get
        Set(ByVal value As TriggerLayerBypassModes)
            If Not Me.SupportedTriggerLayerBypassModes.Equals(value) Then
                Me._SupportedTriggerLayerBypassModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if bypassing the source trigger using
    ''' <see cref="TriggerLayerBypassModes.Acceptor"/> .
    ''' </summary>
    ''' <value> True if trigger layer is bypassed. </value>
    Public ReadOnly Property IsTriggerLayerBypass As Boolean?
        Get
            Return If(Me.TriggerLayerBypassMode.HasValue, Me.TriggerLayerBypassMode.Value = TriggerLayerBypassModes.Acceptor, New Boolean?)
        End Get
    End Property

    ''' <summary> The trigger layer bypass mode. </summary>
    Private _TriggerLayerBypassMode As TriggerLayerBypassModes?

    ''' <summary> Gets or sets the cached trigger layer bypass mode. </summary>
    ''' <value>
    ''' The <see cref="TriggerLayerBypassMode">trigger layer bypass mode</see> or none if not set or
    ''' unknown.
    ''' </value>
    Public Overloads Property TriggerLayerBypassMode As TriggerLayerBypassModes?
        Get
            Return Me._TriggerLayerBypassMode
        End Get
        Protected Set(ByVal value As TriggerLayerBypassModes?)
            If Not Me.TriggerLayerBypassMode.Equals(value) Then
                Me._TriggerLayerBypassMode = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(TriggerSubsystemBase.IsTriggerLayerBypass))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the trigger layer bypass mode. </summary>
    ''' <param name="value"> The  trigger layer bypass mode. </param>
    ''' <returns>
    ''' The <see cref="TriggerLayerBypassMode">source trigger layer bypass mode</see> or none if
    ''' unknown.
    ''' </returns>
    Public Function ApplyTriggerLayerBypassMode(ByVal value As TriggerLayerBypassModes) As TriggerLayerBypassModes?
        Me.WriteTriggerLayerBypassMode(value)
        Return Me.QueryTriggerLayerBypassMode()
    End Function

    ''' <summary> Gets or sets the trigger layer bypass mode query command. </summary>
    ''' <value> The trigger layer bypass mode query command. </value>
    Protected Overridable Property TriggerLayerBypassModeQueryCommand As String

    ''' <summary> Queries the trigger layer bypass mode. </summary>
    ''' <returns>
    ''' The <see cref="TriggerLayerBypassMode">trigger layer bypass mode</see> or none if unknown.
    ''' </returns>
    Public Function QueryTriggerLayerBypassMode() As TriggerLayerBypassModes?
        Me.TriggerLayerBypassMode = Me.Query(Of TriggerLayerBypassModes)(Me.TriggerLayerBypassModeQueryCommand,
                                                                         Me.TriggerLayerBypassMode.GetValueOrDefault(TriggerLayerBypassModes.None),
                                                                         Me.TriggerLayerBypassModeReadWrites)
        Return Me.TriggerLayerBypassMode
    End Function

    ''' <summary> Gets or sets the trigger layer bypass mode command format. </summary>
    ''' <value> The trigger layer bypass mode command format. </value>
    Protected Overridable Property TriggerLayerBypassModeCommandFormat As String

    ''' <summary>
    ''' Writes the trigger layer bypass mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The trigger layer bypass mode. </param>
    ''' <returns>
    ''' The <see cref="TriggerLayerBypassMode">trigger layer bypass mode</see> or none if unknown.
    ''' </returns>
    Public Function WriteTriggerLayerBypassMode(ByVal value As TriggerLayerBypassModes) As TriggerLayerBypassModes?
        Me.TriggerLayerBypassMode = Me.Write(Of TriggerLayerBypassModes)(Me.TriggerLayerBypassModeCommandFormat, value, Me.TriggerLayerBypassModeReadWrites)
        Return Me.TriggerLayerBypassMode
    End Function

#End Region

#Region " TRIGGER SOURCE "

    ''' <summary> Define trigger source read writes. </summary>
    Private Sub DefineTriggerSourceReadWrites()
        Me._TriggerSourceReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As TriggerSources In [Enum].GetValues(GetType(TriggerSources))
            Me._TriggerSourceReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of trigger source parses. </summary>
    ''' <value> A Dictionary of trigger source parses. </value>
    Public ReadOnly Property TriggerSourceReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported trigger sources. </summary>
    Private _SupportedTriggerSources As TriggerSources

    ''' <summary> Gets or sets the supported trigger sources. </summary>
    ''' <value> The supported trigger sources. </value>
    Public Property SupportedTriggerSources() As TriggerSources
        Get
            Return Me._SupportedTriggerSources
        End Get
        Set(ByVal value As TriggerSources)
            If Not Me.SupportedTriggerSources.Equals(value) Then
                Me._SupportedTriggerSources = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The trigger source. </summary>
    Private _TriggerSource As TriggerSources?

    ''' <summary> Gets or sets the cached source TriggerSource. </summary>
    ''' <value>
    ''' The <see cref="TriggerSource">source Trigger Source</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property TriggerSource As TriggerSources?
        Get
            Return Me._TriggerSource
        End Get
        Protected Set(ByVal value As TriggerSources?)
            If Not Me.TriggerSource.Equals(value) Then
                Me._TriggerSource = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Trigger Source. </summary>
    ''' <param name="value"> The  Source Trigger Source. </param>
    ''' <returns>
    ''' The <see cref="TriggerSource">source Trigger Source</see> or none if unknown.
    ''' </returns>
    Public Function ApplyTriggerSource(ByVal value As TriggerSources) As TriggerSources?
        Me.WriteTriggerSource(value)
        Return Me.QueryTriggerSource()
    End Function

    ''' <summary> Gets or sets the Trigger source query command. </summary>
    ''' <remarks> SCPI: ":TRIG:SOUR?". </remarks>
    ''' <value> The Trigger source query command. </value>
    Protected Overridable Property TriggerSourceQueryCommand As String

    ''' <summary> Queries the trigger source. </summary>
    ''' <returns> The <see cref="TriggerSource">trigger source</see> or none if unknown. </returns>
    Public Function QueryTriggerSource() As TriggerSources?
        Me.TriggerSource = Me.Query(Of TriggerSources)(Me.TriggerSourceQueryCommand,
                                                       Me.TriggerSource.GetValueOrDefault(TriggerSources.None),
                                                       Me.TriggerSourceReadWrites)
        Return Me.TriggerSource
    End Function

    ''' <summary> Gets or sets the Trigger source command format. </summary>
    ''' <remarks> SCPI: "{0}". </remarks>
    ''' <value> The write Trigger source command format. </value>
    Protected Overridable Property TriggerSourceCommandFormat As String

    ''' <summary> Writes the Trigger Source without reading back the value from the device. </summary>
    ''' <param name="value"> The Trigger Source. </param>
    ''' <returns> The <see cref="TriggerSource">Trigger Source</see> or none if unknown. </returns>
    Public Function WriteTriggerSource(ByVal value As TriggerSources) As TriggerSources?
        Me.TriggerSource = Me.Write(Of TriggerSources)(Me.TriggerSourceCommandFormat, value, Me.TriggerSourceReadWrites)
        Return Me.TriggerSource
    End Function

#End Region

#Region " TRIGGER EVENT "

    ''' <summary> Define trigger event read writes. </summary>
    Private Sub DefineTriggerEventReadWrites()
        Me._TriggerEventReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As TriggerEvents In [Enum].GetValues(GetType(TriggerEvents))
            Me._TriggerEventReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of trigger Event parses. </summary>
    ''' <value> A Dictionary of trigger Event parses. </value>
    Public ReadOnly Property TriggerEventReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported trigger events. </summary>
    Private _SupportedTriggerEvents As TriggerEvents

    ''' <summary> Gets or sets the supported trigger Events. </summary>
    ''' <value> The supported trigger Events. </value>
    Public Property SupportedTriggerEvents() As TriggerEvents
        Get
            Return Me._SupportedTriggerEvents
        End Get
        Set(ByVal value As TriggerEvents)
            If Not Me.SupportedTriggerEvents.Equals(value) Then
                Me._SupportedTriggerEvents = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The trigger event. </summary>
    Private _TriggerEvent As TriggerEvents?

    ''' <summary> Gets or sets the cached Event TriggerEvent. </summary>
    ''' <value>
    ''' The <see cref="TriggerEvent">Event Trigger Event</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property TriggerEvent As TriggerEvents?
        Get
            Return Me._TriggerEvent
        End Get
        Protected Set(ByVal value As TriggerEvents?)
            If Not Me.TriggerEvent.Equals(value) Then
                Me._TriggerEvent = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Event Trigger Event. </summary>
    ''' <param name="value"> The  Event Trigger Event. </param>
    ''' <returns> The <see cref="TriggerEvent">Event Trigger Event</see> or none if unknown. </returns>
    Public Function ApplyTriggerEvent(ByVal value As TriggerEvents) As TriggerEvents?
        Me.WriteTriggerEvent(value)
        Return Me.QueryTriggerEvent()
    End Function

    ''' <summary> Gets or sets the Trigger Event query command. </summary>
    ''' <remarks> SCPI: ":TRIG:SOUR?". </remarks>
    ''' <value> The Trigger Event query command. </value>
    Protected Overridable Property TriggerEventQueryCommand As String

    ''' <summary> Queries the trigger Event. </summary>
    ''' <returns> The <see cref="TriggerEvent">trigger Event</see> or none if unknown. </returns>
    Public Function QueryTriggerEvent() As TriggerEvents?
        Me.TriggerEvent = Me.Query(Of TriggerEvents)(Me.TriggerEventQueryCommand,
                                                       Me.TriggerEvent.GetValueOrDefault(TriggerEvents.None),
                                                       Me.TriggerEventReadWrites)
        Return Me.TriggerEvent
    End Function

    ''' <summary> Gets or sets the Trigger Event command format. </summary>
    ''' <remarks> SCPI: "{0}". </remarks>
    ''' <value> The write Trigger Event command format. </value>
    Protected Overridable Property TriggerEventCommandFormat As String

    ''' <summary> Writes the Trigger Event without reading back the value from the device. </summary>
    ''' <param name="value"> The Trigger Event. </param>
    ''' <returns> The <see cref="TriggerEvent">Trigger Event</see> or none if unknown. </returns>
    Public Function WriteTriggerEvent(ByVal value As TriggerEvents) As TriggerEvents?
        Me.TriggerEvent = Me.Write(Of TriggerEvents)(Me.TriggerEventCommandFormat, value, Me.TriggerEventReadWrites)
        Return Me.TriggerEvent
    End Function

#End Region

End Class

''' <summary> Enumerates the trigger or arm layer bypass mode. </summary>
<Flags>
Public Enum TriggerLayerBypassModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 1 << 0

    ''' <summary> An enum constant representing the acceptor option. </summary>
    <ComponentModel.Description("Acceptor (ACC)")>
    Acceptor = 1 << 1

    ''' <summary> An enum constant representing the source option. </summary>
    <ComponentModel.Description("Source (SOUR)")>
    Source = 1 << 2
End Enum

''' <summary> Enumerates the arm to trigger events. </summary>
<Flags>
<CodeAnalysis.SuppressMessage("Usage", "CA2217:Do not mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum TriggerEvents As Long

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None (NONE)")>
    None = 0

    ''' <summary> An enum constant representing the source option. </summary>
    <ComponentModel.Description("Source (SOUR)")>
    Source = 1 << 1

    ''' <summary> An enum constant representing the delay option. </summary>
    <ComponentModel.Description("Delay (DEL)")>
    Delay = 1 << 2

    ''' <summary> An enum constant representing the sense option. </summary>
    <ComponentModel.Description("Sense (SENS)")>
    Sense = 1 << 3

    ' TSP2 EVENTS
    <ComponentModel.Description("None (trigger.EVENT_NONE)")>
    TspNone = 1 << 4

    ''' <summary> An enum constant representing the display option. </summary>
    <ComponentModel.Description("Display (trigger.EVENT_DISPLAY)")>
    Display = 1 << 5

    ''' <summary> An enum constant representing the notify 1 option. generates a trigger event when
    '''    the trigger model executes it </summary>
    <ComponentModel.Description("Notify trigger block 1 (trigger.EVENT_NOTIFY1)")>
    Notify1 = 1 << 6

    ''' <summary> An enum constant representing the notify 2 option. </summary>
    <ComponentModel.Description("Notify trigger block 2 (trigger.EVENT_NOTIFY2)")>
    Notify2 = 1 << 7

    ''' <summary> An enum constant representing the notify 3 option. </summary>
    <ComponentModel.Description("Notify trigger block 3 (trigger.EVENT_NOTIFY3)")>
    Notify3 = 1 << 8

    ''' <summary> An enum constant representing the notify 4 option. </summary>
    <ComponentModel.Description("Notify trigger block 4 (trigger.EVENT_NOTIFY4)")>
    Notify4 = 1 << 9

    ''' <summary> An enum constant representing the notify 5 option. </summary>
    <ComponentModel.Description("Notify trigger block 5 (trigger.EVENT_NOTIFY5)")>
    Notify5 = 1 << 10

    ''' <summary> An enum constant representing the notify 6 option. </summary>
    <ComponentModel.Description("Notify trigger block 6 (trigger.EVENT_NOTIFY6)")>
    Notify6 = 1 << 11

    ''' <summary> An enum constant representing the notify 7 option. </summary>
    <ComponentModel.Description("Notify trigger block 7 (trigger.EVENT_NOTIFY7)")>
    Notify7 = 1 << 12

    ''' <summary> An enum constant representing the notify 8 option. </summary>
    <ComponentModel.Description("Notify trigger block 8 (trigger.EVENT_NOTIFY8)")>
    Notify8 = 1 << 13

    ''' <summary> An enum constant representing the bus option. A command interface trigger (bus trigger): 
    '''           Any remote interface: *TRG or GPIB GET bus command  or VXI-11 command device_trigger  </summary>
    <ComponentModel.Description("Bus command (trigger.EVENT_COMMAND)")>
    Bus = 1 << 14

    ''' <summary> An enum constant representing the line edge option. 
    '''           Line edge (either rising, falling, or either based on the 
    '''           configuration of the line) detected on digital input line N (1 to 6)  </summary>
    <ComponentModel.Description("Line Edge 1 (trigger.EVENT_DIGIO1)")>
    LineEdge1 = 1 << 15

    ''' <summary> An enum constant representing the line edge 2 option. </summary>
    <ComponentModel.Description("Line Edge 2 (trigger.EVENT_DIGIO2)")>
    LineEdge2 = 1 << 16

    ''' <summary> An enum constant representing the line edge 3 option. </summary>
    <ComponentModel.Description("Line Edge 3 (trigger.EVENT_DIGIO3)")>
    LineEdge3 = 1 << 17

    ''' <summary> An enum constant representing the line edge 4 option. </summary>
    <ComponentModel.Description("Line Edge 4 (trigger.EVENT_DIGIO4)")>
    LineEdge4 = 1 << 18

    ''' <summary> An enum constant representing the line edge 5 option. </summary>
    <ComponentModel.Description("Line Edge 5 (trigger.EVENT_DIGIO5)")>
    LineEdge5 = 1 << 19

    ''' <summary> An enum constant representing the line edge 6 option. </summary>
    <ComponentModel.Description("Line Edge 6 (trigger.EVENT_DIGIO6)")>
    LineEdge6 = 1 << 20

    ''' <summary> An enum constant representing the tsp link 1 option. 
    '''           Line edge detected on TSP-Link synchronization line N (1 to 3) </summary>
    <ComponentModel.Description("Tsp-Link Line Edge 1 (trigger.EVENT_TSPLINK1)")>
    TspLink1 = 1 << 21

    ''' <summary> An enum constant representing the tsp link 2 option. </summary>
    <ComponentModel.Description("Tsp-Link Line Edge 2 (trigger.EVENT_TSPLINK2)")>
    TspLink2 = 1 << 22

    ''' <summary> An enum constant representing the tsp link 3 option. </summary>
    <ComponentModel.Description("Tsp-Link Line Edge 3 (trigger.EVENT_TSPLINK3)")>
    TspLink3 = 1 << 23

    ''' <summary> An Enum constant representing the Tangent 1 option. 
    '''           Appropriate LXI trigger packet is received on LAN trigger object N (1 to 8) </summary>
    <ComponentModel.Description("Lan Trigger 1 (trigger.EVENT_LAN1)")>
    Lan1 = 1 << 24

    ''' <summary> An enum constant representing the LAN 2 option. </summary>
    <ComponentModel.Description("Lan Trigger 2 (trigger.EVENT_LAN2)")>
    Lan2 = 1 << 25

    ''' <summary> An enum constant representing the LAN 3 option. </summary>
    <ComponentModel.Description("Lan Trigger 3 (trigger.EVENT_LAN3)")>
    Lan3 = 1 << 26

    ''' <summary> An enum constant representing the LAN 4 option. </summary>
    <ComponentModel.Description("Lan Trigger 4 (trigger.EVENT_LAN4)")>
    Lan4 = 1 << 27

    ''' <summary> An enum constant representing the LAN 5 option. </summary>
    <ComponentModel.Description("Lan Trigger 5 (trigger.EVENT_LAN5)")>
    Lan5 = 1 << 28

    ''' <summary> An enum constant representing the LAN 6 option. </summary>
    <ComponentModel.Description("Lan Trigger 6 (trigger.EVENT_LAN6)")>
    Lan6 = 1 << 29

    ''' <summary> An enum constant representing the LAN 7 option. </summary>
    <ComponentModel.Description("Lan Trigger 7 (trigger.EVENT_LAN7)")>
    Lan7 = 1 << 30

    ''' <summary> An enum constant representing the LAN 8 option. </summary>
    <ComponentModel.Description("Lan Trigger 8 (trigger.EVENT_LAN8)")>
    Lan8 = 1 << 31

    ''' <summary> An enum constant representing the blender 1 option. </summary>
    <ComponentModel.Description("Blender 1 (trigger.EVENT_BLENDER1)")>
    Blender1 = 1 << 32

    ''' <summary> An enum constant representing the blender 2 option. </summary>
    <ComponentModel.Description("Blender 2 (trigger.EVENT_BLENDER2)")>
    Blender2 = 1 << 33

    ''' <summary> An enum constant representing the timer 1 option. </summary>
    <ComponentModel.Description("Timer Expired 2 (trigger.EVENT_TIMER1)")>
    Timer1 = 1 << 34

    ''' <summary> An enum constant representing the timer 2 option. </summary>
    <ComponentModel.Description("Timer Expired 2 (trigger.EVENT_TIMER2)")>
    Timer2 = 35

    ''' <summary> An enum constant representing the timer 3 option. </summary>
    <ComponentModel.Description("Timer Expired 3 (trigger.EVENT_TIMER3)")>
    Timer3 = 1 << 36

    ''' <summary> An enum constant representing the timer 4 option. </summary>
    <ComponentModel.Description("Timer Expired 4 (trigger.EVENT_TIMER4)")>
    Timer4 = 1 << 37

    ''' <summary> An enum constant representing the analog option. </summary>
    <ComponentModel.Description("Analog Trigger (trigger.EVENT_ANALOGTRIGGER)")>
    Analog = 1 << 38

    ''' <summary> An enum constant representing the external option. </summary>
    <ComponentModel.Description("External in trigger (trigger.EVENT_EXTERNAL)")>
    External = 1 << 39

End Enum

''' <summary> Enumerates the trigger layer control sources. </summary>
<Flags>
Public Enum TriggerSources

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 1 << 0

    ''' <summary> An enum constant representing the bus option. </summary>
    <ComponentModel.Description("Bus (BUS)")>
    Bus = 1 << 1

    ''' <summary> An enum constant representing the external option. </summary>
    <ComponentModel.Description("External (EXT)")>
    External = 1 << 2

    ''' <summary> An enum constant representing the immediate option. </summary>
    <ComponentModel.Description("Immediate (IMM)")>
    Immediate = 1 << 3

    ''' <summary> An enum constant representing the trigger link option. </summary>
    <ComponentModel.Description("Trigger Link (TLIN)")>
    TriggerLink = 1 << 4

    ''' <summary> . </summary>
    <ComponentModel.Description("Internal (INT)")>
    Internal = 1 << 5

    ''' <summary> An enum constant representing the manual option. </summary>
    <ComponentModel.Description("Manual (MAN)")>
    Manual = 1 << 6

    ''' <summary> An enum constant representing the hold option. </summary>
    <ComponentModel.Description("Hold (HOLD)")>
    Hold = 1 << 7

    ''' <summary> An enum constant representing the timer option. </summary>
    <ComponentModel.Description("Timer (TIM)")>
    Timer = 1 << 8

    ''' <summary> An enum constant representing the LAN option. </summary>
    <ComponentModel.Description("LXI (LAN)")>
    Lan = 1 << 9

    ''' <summary> An enum constant representing the analog option. </summary>
    <ComponentModel.Description("Analog (ATR)")>
    Analog = 1 << 10

    ''' <summary> An enum constant representing the blender option. </summary>
    <ComponentModel.Description("Blender (BLEND)")>
    Blender = 1 << 11

    ''' <summary> An enum constant representing the digital option. </summary>
    <ComponentModel.Description("Digital I/O (DIG)")>
    Digital = 1 << 12

End Enum

