'---------------------------------------------------------------------------------------------------
' file:		Device\My\MyLibrary_Internals.vb
'
' summary:	My library internals class
'---------------------------------------------------------------------------------------------------
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.Tsp2.K7500Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.K3700Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.Cinco.K3700Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.Morphe.K3700Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Verrzzano.K3700Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.StrainPublicKey)>
