﻿Imports System.Reflection
Imports System.Runtime.InteropServices

<Assembly: AssemblyTitle("Facade VI Tests")>
<Assembly: AssemblyDescription("Virtual Instrument Facade Unit Tests Library")>
<Assembly: AssemblyProduct("isr.VI.Facade.Tests")>
<Assembly: CLSCompliant(True)>
<Assembly: ComVisible(False)>

