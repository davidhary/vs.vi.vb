'---------------------------------------------------------------------------------------------------
' file:		.VI\K2002\Resource_IEEE488p2.vb
'
' summary:	Resource IEEE 488p 2 class
'---------------------------------------------------------------------------------------------------

Partial Public Class ResourceTests

    ''' <summary> (Unit Test Method) tests open status session. </summary>
    <TestMethod()>
    Public Sub OpenStatusSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests wait for message avaiable. </summary>
    <TestMethod()>
    Public Sub WaitForMessageAvaiableTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertWaitForMessageAvailable(TestInfo, device.Session)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests wait for operation completion. </summary>
    <TestMethod()>
    Public Sub WaitForOperationCompletionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertWaitForOperationCompletion(TestInfo, device.Session, ":SYST:CLE; *OPC")
                isr.VI.DeviceTests.DeviceManager.AssertWaitForServiceRequestOperationCompletion(TestInfo, device.Session, ":SYST:CLE; *OPC")
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests toggling service request handling. </summary>
    ''' <remarks>
    ''' This test will fail the first time it is run if Windows requests access through the Firewall.
    ''' </remarks>
    <TestMethod()>
    Public Sub ToggleServiceRequestHandlingTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertToggleServiceRequestHandling(TestInfo, device.Session)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

End Class
