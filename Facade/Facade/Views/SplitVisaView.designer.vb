'---------------------------------------------------------------------------------------------------
' file:		.\Views\SplitVisaView.designer.vb
'
' summary:	Split visa view.designer class
'---------------------------------------------------------------------------------------------------
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SplitVisaView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._ProgressBar = New isr.Core.Controls.StatusStripCustomProgressBar()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._TraceMessagesBox = New isr.Core.Forma.TraceMessagesBox()
        Me._TreePanel = New isr.Core.Controls.TreePanel()
        Me._StatusStrip.SuspendLayout()
        Me._Layout.SuspendLayout()
        CType(Me._TreePanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._TreePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_StatusLabel
        '
        Me._StatusLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._StatusLabel.Size = New System.Drawing.Size(663, 20)
        Me._StatusLabel.Spring = True
        Me._StatusLabel.Text = "Loading..."
        Me._StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel, Me._ProgressBar})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 615)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me._StatusStrip.Size = New System.Drawing.Size(780, 25)
        Me._StatusStrip.TabIndex = 7
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_ProgressBar
        '
        Me._ProgressBar.Maximum = 100
        Me._ProgressBar.Name = "_ProgressBar"
        Me._ProgressBar.Size = New System.Drawing.Size(100, 23)
        Me._ProgressBar.Text = "0 %"
        Me._ProgressBar.Value = 0
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.Controls.Add(Me._TreePanel, 1, 1)
        Me._Layout.Location = New System.Drawing.Point(27, 167)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.Size = New System.Drawing.Size(341, 217)
        Me._Layout.TabIndex = 13
        '
        '_TraceMessagesBox
        '
        Me._TraceMessagesBox.AlertLevel = System.Diagnostics.TraceEventType.Warning
        Me._TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info
        Me._TraceMessagesBox.CaptionFormat = "{0} ≡"
        Me._TraceMessagesBox.CausesValidation = False
        Me._TraceMessagesBox.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TraceMessagesBox.Location = New System.Drawing.Point(113, 52)
        Me._TraceMessagesBox.Multiline = True
        Me._TraceMessagesBox.Name = "_TraceMessagesBox"
        Me._TraceMessagesBox.PresetCount = 100
        Me._TraceMessagesBox.ReadOnly = True
        Me._TraceMessagesBox.ResetCount = 200
        Me._TraceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._TraceMessagesBox.Size = New System.Drawing.Size(601, 471)
        Me._TraceMessagesBox.StatusPrompt = Nothing
        Me._TraceMessagesBox.TabIndex = 15
        Me._TraceMessagesBox.TraceLevel = System.Diagnostics.TraceEventType.Verbose
        '
        '_TreePanel
        '
        Me._TreePanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TreePanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TreePanel.Location = New System.Drawing.Point(3, 6)
        Me._TreePanel.Name = "_TreePanel"
        Me._TreePanel.Size = New System.Drawing.Size(335, 205)
        Me._TreePanel.SplitterDistance = 111
        Me._TreePanel.TabIndex = 0
        '
        'SplitVisaView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._TraceMessagesBox)
        Me.Controls.Add(Me._Layout)
        Me.Controls.Add(Me._StatusStrip)
        Me.Name = "SplitVisaView"
        Me.Size = New System.Drawing.Size(780, 640)
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me._Layout.ResumeLayout(False)
        CType(Me._TreePanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me._TreePanel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

        Private WithEvents _StatusStrip As Windows.Forms.StatusStrip

        Private WithEvents _StatusLabel As Windows.Forms.ToolStripStatusLabel

        Private WithEvents _Layout As Windows.Forms.TableLayoutPanel

        Private WithEvents _ProgressBar As Core.Controls.StatusStripCustomProgressBar

        Private WithEvents _TreePanel As Core.Controls.TreePanel

        Private WithEvents _TraceMessagesBox As Core.Forma.TraceMessagesBox
End Class
