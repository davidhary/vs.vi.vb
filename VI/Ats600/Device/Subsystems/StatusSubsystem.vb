'---------------------------------------------------------------------------------------------------
' file:		Device\Subsystems\StatusSubsystem.vb
'
' summary:	Status subsystem class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EscapeSequencesExtensions

''' <summary> Defines a Status Subsystem for a InTest Thermo Stream instrument. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits VI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(session)
        Me._VersionInfo = New VersionInfo
        StatusSubsystem.InitializeSession(session)
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region


#Region " SESSION "

    ''' <summary> Initializes the session. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Private Shared Sub InitializeSession(ByVal session As VI.Pith.SessionBase)
        session.ClearExecutionStateCommand = VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand
        session.ResetKnownStateCommand = String.Empty
        session.ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        session.OperationCompletedQueryCommand = String.Empty
        session.StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
        session.StandardEventStatusQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand
        session.StandardEventEnableQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand
        session.StandardServiceEnableCommandFormat = VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat
        ' complete not supported (?)
        session.StandardServiceEnableCompleteCommandFormat = VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat
        session.ServiceRequestEnableCommandFormat = VI.Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat
        session.ServiceRequestEnableQueryCommand = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand
        session.WaitCommand = VI.Pith.Ieee488.Syntax.WaitCommand
        session.OperationCompleted = True
    End Sub

#End Region

#Region " MEASUREMENT REGISTER EVENTS "

    ''' <summary> Gets the measurement status query command. </summary>
    ''' <value> The measurement status query command. </value>
    Protected Overrides Property MeasurementStatusQueryCommand As String = String.Empty

    ''' <summary> Gets the measurement event condition query command. </summary>
    ''' <value> The measurement event condition query command. </value>
    Protected Overrides Property MeasurementEventConditionQueryCommand As String = String.Empty

#End Region

#Region " OPERATION REGISTER EVENTS "

    ''' <summary> Gets the operation event enable Query command. </summary>
    ''' <value> The operation event enable Query command. </value>
    Protected Overrides Property OperationEventEnableQueryCommand As String = String.Empty

    ''' <summary> Gets the operation event enable command format. </summary>
    ''' <value> The operation event enable command format. </value>
    Protected Overrides Property OperationEventEnableCommandFormat As String = String.Empty

    ''' <summary> Gets the operation event status query command. </summary>
    ''' <value> The operation event status query command. </value>
    Protected Overrides Property OperationEventStatusQueryCommand As String = String.Empty

#End Region

#Region " QUESTIONABLE REGISTER "

    ''' <summary> Gets the questionable status query command. </summary>
    ''' <value> The questionable status query command. </value>
    Protected Overrides Property QuestionableStatusQueryCommand As String = String.Empty

#End Region

#Region " IDENTITY "

    ''' <summary> Gets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = VI.Pith.Ieee488.Syntax.IdentityQueryCommand

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
    ''' <returns> System.String. </returns>
    Public Overrides Function QueryIdentity() As String
        If Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.PublishVerbose("Requesting identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Me.WriteIdentityQueryCommand()
            Me.PublishVerbose("Trying to read identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Dim value As String = Me.Session.ReadLineTrimEnd
            value = value.ReplaceCommonEscapeSequences.Trim
            Me.PublishVerbose($"Setting identity to {value};. ")
            Me.VersionInfo.Parse(value)
            MyBase.VersionInfoBase = Me.VersionInfo
            Me.Identity = Me.VersionInfo.Identity
        End If
        Return Me.Identity
    End Function

    ''' <summary> Gets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public ReadOnly Property VersionInfo As VersionInfo

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets the clear error queue command. </summary>
    ''' <remarks>
    ''' This supposed to use 'CLER' and requires to wait 4 seconds after the error is cleared!
    ''' </remarks>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = String.Empty

    ''' <summary> Gets the error queue query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = String.Empty

    ''' <summary> Gets the 'Next Error' query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property DequeueErrorQueryCommand As String = String.Empty

    ''' <summary> Clears the error queue and waits. </summary>
    ''' <remarks> Uses 'CLER' and wait 4 seconds after the error is cleared. </remarks>
    Public Overrides Sub ClearErrorQueue()
        MyBase.ClearErrorQueue()
        Me.Session.Execute("CLER")
        If Me.Session.IsSessionOpen Then
            isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(4000))
        End If
    End Sub

    ''' <summary> Gets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String
        Get
            Return If(Debugger.IsAttached, VI.Pith.Scpi.Syntax.LastSystemErrorQueryCommand, String.Empty)
        End Get
    End Property

#End Region

#Region " LINE FREQUENCY "

    ''' <summary> Gets or sets line frequency query command. </summary>
    ''' <value> The line frequency query command. </value>
    Protected Overrides Property LineFrequencyQueryCommand As String = String.Empty

#End Region

End Class
