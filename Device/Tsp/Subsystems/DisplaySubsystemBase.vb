''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public MustInherit Class DisplaySubsystemBase
    Inherits VI.DisplaySubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.RestoreMainScreenWaitCompleteCommand = Tsp.Syntax.Display.RestoreMainWaitCompleteCommand
    End Sub

#End Region

#Region " EXISTS "

    ''' <summary>
    ''' Reads the display existence indicator. Some TSP instruments (e.g., 3706) may have no display.
    ''' </summary>
    ''' <returns> <c>True</c> if the display exists; otherwise, <c>False</c>. </returns>
    Public Overrides Function QueryExists() As Boolean?
        ' detect the display
        Me.Session.MakeEmulatedReplyIfEmpty(Me.Exists.GetValueOrDefault(True))
        Me.Exists = Not Me.Session.IsNil(Tsp.Syntax.Display.SubsystemName)
        Return Me.Exists
    End Function

    #End Region

    #Region " CLEAR "

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearCommand As String = Tsp.Syntax.Display.ClearCommand

    ''' <summary> Clears the display. </summary>
    ''' <remarks> Sets the display to the user mode. </remarks>
    Public Overrides Sub ClearDisplay()
        Me.DisplayScreen = VI.DisplayScreens.User
        If Me.QueryExists.GetValueOrDefault(False) Then
            Me.Session.WriteLine(Tsp.Syntax.Display.ClearCommand)
        End If
    End Sub

    ''' <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
    ''' <remarks> Sets the display to user. </remarks>
    Public Sub TryClearDisplayMeasurement()
        If Me.Exists AndAlso ((Me.DisplayScreen And DisplayScreens.Measurement) = 0) Then
            Me.TryClearDisplay()
        End If
    End Sub

    ''' <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
    ''' <remarks> Sets the display to the measurement. </remarks>
    Public Sub ClearDisplayMeasurement()
        If Me.QueryExists.GetValueOrDefault(False) AndAlso ((Me.DisplayScreen And DisplayScreens.Measurement) = 0) Then
            Me.ClearDisplay()
        End If
        Me.DisplayScreen = DisplayScreens.Measurement
    End Sub

#End Region

#Region " DISPLAY CHARACTER "

    ''' <summary> Displays the character. </summary>
    ''' <param name="lineNumber">      The line number. </param>
    ''' <param name="position">        The position. </param>
    ''' <param name="characterNumber"> The character number. </param>
    Public Sub DisplayCharacter(ByVal lineNumber As Integer, ByVal position As Integer,
                                ByVal characterNumber As Integer)
        Me.DisplayScreen = DisplayScreens.User Or DisplayScreens.Custom
        If Not Me.QueryExists.GetValueOrDefault(False) Then
            Return
        End If
        ' ignore empty character.
        If characterNumber <= 0 OrElse characterNumber > Tsp.Syntax.Display.MaximumCharacterNumber Then
            Return
        End If
        Me.Session.WriteLine(Tsp.Syntax.Display.SetCursorCommandFormat, lineNumber, position)
        Me.Session.WriteLine(Tsp.Syntax.Display.SetCharacterCommandFormat, characterNumber)
    End Sub

#End Region

#Region " DISPLAY LINE "

    ''' <summary> Displays a message on the display. </summary>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <param name="value">      The value. </param>
    Public Overrides Sub DisplayLine(ByVal lineNumber As Integer, ByVal value As String)

        Me.DisplayScreen = DisplayScreens.User Or DisplayScreens.Custom
        If Not Me.QueryExists.GetValueOrDefault(False) Then
            Return
        End If

        ' ignore empty strings.
        If String.IsNullOrWhiteSpace(value) Then
            Return
        End If

        Dim length As Integer = Tsp.Syntax.Display.FirstLineLength

        If lineNumber < 1 Then
            lineNumber = 1
        ElseIf lineNumber > 2 Then
            lineNumber = 2
        End If
        If lineNumber = 2 Then
            length = Tsp.Syntax.Display.SecondLineLength
        End If

        Me.Session.WriteLine(Tsp.Syntax.Display.SetCursorLineCommandFormat, lineNumber)
        If value.Length < length Then value = value.PadRight(length)
        Me.Session.WriteLine(Tsp.Syntax.Display.SetTextCommandFormat, value)

    End Sub

    ''' <summary> Displays the program title. </summary>
    ''' <param name="title">    Top row data. </param>
    ''' <param name="subtitle"> Bottom row data. </param>
    Public Sub DisplayTitle(ByVal title As String, ByVal subtitle As String)
        Me.DisplayLine(0, title)
        Me.DisplayLine(2, subtitle)
    End Sub

#End Region

#Region " RESTORE "

    ''' <summary> Gets or sets the restore display command. </summary>
    ''' <value> The restore display command. </value>
    Public Property RestoreMainScreenWaitCompleteCommand As String

    ''' <summary> Restores the instrument display. </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub RestoreDisplay(ByVal timeout As TimeSpan)
        Me.DisplayScreen = DisplayScreens.Default
        If Me.Exists.HasValue AndAlso Me.Exists.Value AndAlso Not String.IsNullOrWhiteSpace(Me.RestoreMainScreenWaitCompleteCommand) Then
            ' Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
            Me.Session.Execute($"{Me.RestoreMainScreenWaitCompleteCommand}; {Me.Session.OperationCompleteCommand}")
            Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        End If
    End Sub

    ''' <summary> Restores the instrument display. </summary>
    Public Sub RestoreDisplay()
        Me.DisplayScreen = DisplayScreens.Default
        If Me.Exists.HasValue AndAlso Me.Exists.Value AndAlso Not String.IsNullOrWhiteSpace(Me.RestoreMainScreenWaitCompleteCommand) Then
            ' Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
            Me.Session.WriteLine(Me.RestoreMainScreenWaitCompleteCommand)
            Me.Session.DoEventsReadDelay()
            Me.Session.QueryOperationCompleted()
        End If
    End Sub

#End Region

End Class

