'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SenseSubsystemBase.vb
'
' summary:	Sense subsystem base class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core
Imports isr.Core.EnumExtensions

''' <summary> Defines the contract that must be implemented by a Sense Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SenseSubsystemBase
    Inherits SourceMeasureUnitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.SenseMode = SenseActionMode.Local
    End Sub

#End Region

#Region " SENSE MODE "

    ''' <summary> The Sense Action. </summary>
    Private _SenseMode As SenseActionMode?

    ''' <summary> Gets or sets the cached Sense Action. </summary>
    ''' <value>
    ''' The <see cref="SenseActionMode">Sense Action</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property SenseMode As SenseActionMode?
        Get
            Return Me._SenseMode
        End Get
        Protected Set(ByVal value As SenseActionMode?)
            If Not Nullable.Equals(Me.SenseMode, value) Then
                Me._SenseMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sense Action. </summary>
    ''' <param name="value"> The  Sense Action. </param>
    ''' <returns> The <see cref="SenseActionMode">Sense Action</see> or none if unknown. </returns>
    Public Function ApplySenseMode(ByVal value As SenseActionMode) As SenseActionMode?
        Me.WriteSenseMode(value)
        Return Me.QuerySenseMode()
    End Function

    ''' <summary> Queries the Sense Action. </summary>
    ''' <returns> The <see cref="SenseActionMode">Sense Action</see> or none if unknown. </returns>
    Public Function QuerySenseMode() As SenseActionMode?
        Dim currentValue As String = Me.SenseMode.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(currentValue)
        currentValue = Me.Session.QueryTrimEnd($"_G.print({Me.SourceMeasureUnitReference}.sense)")
        If String.IsNullOrWhiteSpace(currentValue) Then
            Dim message As String = "Failed fetching Sense Action"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.SenseMode = New SenseActionMode?
        Else
            Dim se As New StringEnumerator(Of SenseActionMode)
            Me.SenseMode = se.ParseContained(currentValue.BuildDelimitedValue)
        End If
        Return Me.SenseMode
    End Function

    ''' <summary> Writes the Sense Action without reading back the value from the device. </summary>
    ''' <param name="value"> The Sense Action. </param>
    ''' <returns> The <see cref="SenseActionMode">Sense Action</see> or none if unknown. </returns>
    Public Function WriteSenseMode(ByVal value As SenseActionMode) As SenseActionMode?
        Me.Session.WriteLine("{0}.sense = {0}.{1}", Me.SourceMeasureUnitReference, value.ExtractBetween())
        Me.SenseMode = value
        Return Me.SenseMode
    End Function

#End Region

End Class

''' <summary> Specifies the sense modes. </summary>
Public Enum SenseActionMode

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("None")>
    None

    ''' <summary> An enum constant representing the remote option. </summary>
    <Description("Remote (SENSE_REMOTE)")>
    Remote

    ''' <summary> An enum constant representing the local option. </summary>
    <Description("Local (SENSE_LOCAL)")>
    Local
End Enum

