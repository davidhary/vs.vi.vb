''' <summary> Defines the contract that must be implemented by a Route Subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-15, 1.0.1841.x. </para>
''' </remarks>
Public MustInherit Class RouteSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="RouteSubsystemBase" /> class. </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._ScanList = String.Empty
        Me.DefineTerminalsModeReadWrites()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ScanList = String.Empty
        Me.TerminalsMode = RouteTerminalsModes.Front
    End Sub

#End Region

#Region " CLOSED CHANNEL "

    ''' <summary> The closed channel. </summary>
    Private _ClosedChannel As String

    ''' <summary> Gets or sets the closed Channel. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The closed Channel. </value>
    Public Overloads Property ClosedChannel As String
        Get
            Return Me._ClosedChannel
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.ClosedChannel) Then
                Me._ClosedChannel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the closed Channel described by value. </summary>
    ''' <param name="value">   The scan list. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyClosedChannel(ByVal value As String, ByVal timeout As TimeSpan) As String
        Me.WriteClosedChannel(value, timeout)
        Return Me.QueryClosedChannel()
    End Function

    ''' <summary> Gets or sets the closed Channel query command. </summary>
    ''' <remarks> :ROUT:CLOS. </remarks>
    ''' <value> The closed Channel query command. </value>
    Protected Overridable Property ClosedChannelQueryCommand As String

    ''' <summary> Queries closed Channel. </summary>
    ''' <returns> The closed Channel. </returns>
    Public Function QueryClosedChannel() As String
        Me.ClosedChannel = Me.Query(Me.ClosedChannel, Me.ClosedChannelQueryCommand)
        Return Me.ClosedChannel
    End Function

    ''' <summary> Gets or sets the closed Channel command format. </summary>
    ''' <remarks> :ROUT:CLOS {0} </remarks>
    ''' <value> The closed Channel command format. </value>
    Protected Overridable Property ClosedChannelCommandFormat As String

    ''' <summary> Writes a closed Channel. </summary>
    ''' <param name="value">   The scan list. </param>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteClosedChannel(ByVal value As String, ByVal timeout As TimeSpan) As String
        Me.Write(Me.ClosedChannelCommandFormat, value)
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        Me.ClosedChannel = value
        Me.ClosedChannels = Nothing
        Me.ClosedChannelsState = Nothing
        Me.OpenChannels = Nothing
        Me.OpenChannelsState = Nothing
        Return Me.ClosedChannel
    End Function

    ''' <summary> Gets or sets the open Channel command format. </summary>
    ''' <remarks> :ROUT:OPEN {0} </remarks>
    ''' <value> The open Channel command format. </value>
    Protected Overridable Property OpenChannelCommandFormat As String

    ''' <summary> Applies the open channel list and reads back the list. </summary>
    ''' <param name="channelList"> List of Channel. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyOpenChannel(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.WriteOpenChannel(channelList, timeout)
        Return Me.QueryClosedChannel()
        Return Me.ClosedChannel
    End Function

    ''' <summary> Opens the specified Channel in the list. </summary>
    ''' <param name="channelList"> List of Channel. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteOpenChannel(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{String.Format(Me.OpenChannelCommandFormat, channelList)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        ' set to nothing to indicate that the value is not known -- requires reading.
        Me.ClosedChannel = Nothing
        Me.ClosedChannels = Nothing
        Me.ClosedChannelsState = Nothing
        Me.OpenChannels = Nothing
        Me.OpenChannelsState = Nothing
        Return Me.ClosedChannel
    End Function

#End Region

#Region " CLOSED CHANNELS "

    ''' <summary> State of the closed channels. </summary>
    Private _ClosedChannelsState As String

    ''' <summary> Gets or sets the closed channels state. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The closed channels. </value>
    Public Overloads Property ClosedChannelsState As String
        Get
            Return Me._ClosedChannelsState
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.ClosedChannelsState) Then
                Me._ClosedChannelsState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The closed channels checked. </summary>
    Private _ClosedChannelsChecked As String

    ''' <summary> Gets or sets the closed channels checked for state. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The closed channels. </value>
    Public Overloads Property ClosedChannelsChecked As String
        Get
            Return Me._ClosedChannelsChecked
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.ClosedChannelsChecked) Then
                Me._ClosedChannelsChecked = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the closed channels query command. </summary>
    ''' <remarks> :ROUT:CLOS? {0} </remarks>
    ''' <value> The closed channels query command. </value>
    Protected Overridable Property ClosedChannelsStateQueryCommand As String

    ''' <summary> Gets the supports closed channels state query. </summary>
    ''' <value> The supports closed channels state query. </value>
    Public ReadOnly Property SupportsClosedChannelsStateQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ClosedChannelsStateQueryCommand)
        End Get
    End Property

    ''' <summary> Queries closed channels state. </summary>
    ''' <param name="channelList"> List of Channel. </param>
    ''' <returns> The closed channels state. </returns>
    Public Function QueryClosedChannelsState(ByVal channelList As String) As String
        Me.ClosedChannelsState = Me.Query(Me.ClosedChannelsState, String.Format(Me.ClosedChannelsStateQueryCommand, channelList))
        Me.ClosedChannelsChecked = channelList
        Return Me.ClosedChannelsState
    End Function

    ''' <summary> The closed channels. </summary>
    Private _ClosedChannels As String

    ''' <summary> Gets or sets the closed channels. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The closed channels. </value>
    Public Overloads Property ClosedChannels As String
        Get
            Return Me._ClosedChannels
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.ClosedChannels) Then
                Me._ClosedChannels = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the closed channels described by value. </summary>
    ''' <param name="channelList"> The scan list. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyClosedChannels(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.WriteClosedChannels(channelList, timeout)
        If Me.SupportsClosedChannelsQuery Then
            Me.QueryClosedChannels()
        End If
        If Me.SupportsClosedChannelsStateQuery Then
            Me.QueryClosedChannelsState(channelList)
        End If
        Return Me.ClosedChannels
    End Function

    ''' <summary> Gets the closed channels query command. </summary>
    ''' <remarks> :ROUT:CLOS? </remarks>
    ''' <value> The closed channels query command. </value>
    Protected Overridable Property ClosedChannelsQueryCommand As String

    ''' <summary> Gets the supports closed channels query. </summary>
    ''' <value> The supports closed channels query. </value>
    Public ReadOnly Property SupportsClosedChannelsQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ClosedChannelsQueryCommand)
        End Get
    End Property

    ''' <summary> Queries closed channels. </summary>
    ''' <returns> The closed channels. </returns>
    Public Function QueryClosedChannels() As String
        Me.ClosedChannels = Me.Query(Me.ClosedChannels, Me.ClosedChannelsQueryCommand)
        Return Me.ClosedChannels
    End Function

    ''' <summary> Gets or sets the closed channels command format. </summary>
    ''' <remarks> :ROUT:CLOS {0} </remarks>
    ''' <value> The closed channels command format. </value>
    Protected Overridable Property ClosedChannelsCommandFormat As String

    ''' <summary> Writes a closed channels. </summary>
    ''' <param name="channelList"> The scan list. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteClosedChannels(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{String.Format(Me.ClosedChannelsCommandFormat, channelList)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        ' set to nothing to indicate that the value is not known -- requires reading.
        Me.ClosedChannel = Nothing
        Me.ClosedChannelsState = Nothing
        Me.OpenChannels = Nothing
        Me.OpenChannelsState = Nothing
        Me.ClosedChannels = channelList
        Return Me.ClosedChannels
    End Function

#End Region

#Region " OPEN CHANNELS "

    ''' <summary> Builds channel list. </summary>
    ''' <param name="slotNumber">    The slot number. </param>
    ''' <param name="channelsState"> State of the channels. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildChannelList(ByVal slotNumber As Integer, ByVal channelsState As String) As String
        Dim builder As New ChannelListBuilder
        If Not String.IsNullOrEmpty(channelsState) Then
            Dim q As New Queue(Of String)(channelsState.Split(","c))
            Dim relayNumer As Integer = 0
            Do While q.Any
                relayNumer += 1
                If q.Dequeue = "1" Then
                    builder.AddChannel(slotNumber, relayNumer)
                End If
            Loop
        End If
        Return builder.ToString
    End Function

    ''' <summary> Builds channel list. </summary>
    ''' <param name="channelList">   List of channels. </param>
    ''' <param name="channelsState"> State of the channels. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildChannelList(ByVal channelList As String, ByVal channelsState As String) As String
        Dim builder As New ChannelListBuilder
        If Not String.IsNullOrEmpty(channelsState) Then
            Dim channels As New Queue(Of String)(channelList.TrimStart("(@".ToCharArray).TrimEnd(")"c).Split(","c))
            Dim states As New Queue(Of String)(channelsState.Split(","c))
            Do While states.Any AndAlso channels.Any
                If states.Dequeue = "1" Then
                    builder.AddChannel(channels.Dequeue)
                Else
                    channels.Dequeue()
                End If
            Loop
        End If
        Return builder.ToString
    End Function

    ''' <summary> State of the open channels. </summary>
    Private _OpenChannelsState As String

    ''' <summary> Gets or sets the Open channels state. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The Open channels state. </value>
    Public Overloads Property OpenChannelsState As String
        Get
            Return Me._OpenChannelsState
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.OpenChannelsState) Then
                Me._OpenChannelsState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The open channels checked. </summary>
    Private _OpenChannelsChecked As String

    ''' <summary> Gets or sets the Open channels Checked. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The Open channels Checked. </value>
    Public Overloads Property OpenChannelsChecked As String
        Get
            Return Me._OpenChannelsChecked
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.OpenChannelsChecked) Then
                Me._OpenChannelsChecked = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the Open channels query command. </summary>
    ''' <remarks> :ROUT:CLOS? {0} </remarks>
    ''' <value> The Open channels query command. </value>
    Protected Overridable Property OpenChannelsStateQueryCommand As String

    ''' <summary> Gets the supports open channels state query. </summary>
    ''' <value> The supports open channels sate query. </value>
    Public ReadOnly Property SupportsOpenChannelsStateQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.OpenChannelsStateQueryCommand)
        End Get
    End Property

    ''' <summary> Queries Open channels state. </summary>
    ''' <param name="channelList"> List of Channel. </param>
    ''' <returns> The Open channels state. </returns>
    Public Function QueryOpenChannelsState(ByVal channelList As String) As String
        Me.OpenChannelsState = Me.Query(Me.OpenChannelsState, String.Format(Me.OpenChannelsStateQueryCommand, channelList))
        Me.OpenChannelsChecked = channelList
        Return Me.OpenChannelsState
    End Function

    ''' <summary> The open channels. </summary>
    Private _OpenChannels As String

    ''' <summary> Gets or sets the Open channels. </summary>
    ''' <remarks> Nothing is not set. </remarks>
    ''' <value> The Open channels. </value>
    Public Overloads Property OpenChannels As String
        Get
            Return Me._OpenChannels
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.OpenChannels) Then
                Me._OpenChannels = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the Open channels described by value. </summary>
    ''' <param name="channelList"> The scan list. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyOpenChannels(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.WriteOpenChannels(channelList, timeout)
        If Me.SupportsOpenChannelsQuery Then
            Me.QueryOpenChannels()
        End If
        If Me.SupportsOpenChannelsStateQuery Then
            Me.QueryOpenChannelsState(channelList)
        End If
        Return Me.OpenChannels
    End Function

    ''' <summary> Gets the Open channels query command. </summary>
    ''' <remarks> :ROUT:OPEN? </remarks>
    ''' <value> The Open channels query command. </value>
    Protected Overridable Property OpenChannelsQueryCommand As String

    ''' <summary> Gets the supports open channels query. </summary>
    ''' <value> The supports open channels query. </value>
    Public ReadOnly Property SupportsOpenChannelsQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.OpenChannelsQueryCommand)
        End Get
    End Property

    ''' <summary> Queries Open channels. </summary>
    ''' <returns> The Open channels. </returns>
    Public Function QueryOpenChannels() As String
        Me.OpenChannels = Me.Query(Me.OpenChannels, Me.OpenChannelsQueryCommand)
        Return Me.OpenChannels
    End Function

    ''' <summary> Gets or sets the open channels command format. </summary>
    ''' <remarks> :ROUT:OPEN {0} </remarks>
    ''' <value> The open channels command format. </value>
    Protected Overridable Property OpenChannelsCommandFormat As String

    ''' <summary> Opens the specified channels in the list. </summary>
    ''' <param name="channelList"> List of channels. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteOpenChannels(ByVal channelList As String, ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{String.Format(Me.OpenChannelsCommandFormat, channelList)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        ' set to nothing to indicate that the value is not known -- requires reading.
        Me.ClosedChannel = Nothing
        Me.ClosedChannels = Nothing
        Me.ClosedChannelsState = Nothing
        Me.OpenChannelsState = Nothing
        Me.OpenChannels = channelList
        Return Me.OpenChannels
    End Function

#End Region

#Region " OPEN ALL CHANNELS "

    ''' <summary> Gets or sets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    Protected Overridable Property OpenChannelsCommand As String

    ''' <summary>
    ''' Applies the open all command, wait for timeout and read back the closed channels.
    ''' </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyOpenAll(ByVal timeout As TimeSpan) As String
        Me.WriteOpenAll(timeout)
        Return Me.QueryOpenChannels()
    End Function

    ''' <summary> Opens all channels. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> A String. </returns>
    Public Function WriteOpenAll(ByVal timeout As TimeSpan) As String
        Me.Session.Execute($"{Me.OpenChannelsCommand}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        ' set to nothing to indicate that the value is not known -- requires reading.
        Me.ClosedChannel = Nothing
        Me.ClosedChannels = Nothing
        Me.ClosedChannelsState = Nothing
        Me.OpenChannels = Nothing
        Me.OpenChannelsState = Nothing
        Return Me.OpenChannels
    End Function

#End Region

#Region " CHANNEL PATERN = MEMORY SCANS "

    ''' <summary> Gets or sets the recall channel pattern command format. </summary>
    ''' <value> The recall channel pattern command format. </value>
    Protected Overridable Property RecallChannelPatternCommandFormat As String

    ''' <summary> Recalls channel pattern from a memory location. </summary>
    ''' <param name="memoryLocation"> Specifies a memory location between 1 and 100. </param>
    ''' <param name="timeout">        The timeout. </param>
    Public Sub RecallChannelPattern(ByVal memoryLocation As Integer, ByVal timeout As TimeSpan)
        Me.Session.Execute($"{String.Format(Me.RecallChannelPatternCommandFormat, memoryLocation)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
    End Sub

    ''' <summary> Gets or sets the save channel pattern command format. </summary>
    ''' <value> The save channel pattern command format. </value>
    Protected Overridable Property SaveChannelPatternCommandFormat As String

    ''' <summary> Saves existing channel pattern into a memory location. </summary>
    ''' <param name="memoryLocation"> Specifies a memory location between 1 and 100. </param>
    ''' <param name="timeout">        The timeout. </param>
    Public Sub SaveChannelPattern(ByVal memoryLocation As Integer, ByVal timeout As TimeSpan)
        Me.Session.Execute($"{String.Format(Me.SaveChannelPatternCommandFormat, memoryLocation)}; {Me.Session.OperationCompleteCommand}")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
    End Sub

    ''' <summary> Saves a channel list to a memory item. </summary>
    ''' <param name="channelList">    List of channels. </param>
    ''' <param name="memoryLocation"> Specifies a memory location between 1 and 100. </param>
    ''' <param name="timeout">        The timeout. </param>
    ''' <returns> The memory location. </returns>
    Public Function SaveChannelPattern(ByVal channelList As String, ByVal memoryLocation As Integer, ByVal timeout As TimeSpan) As Integer
        If Not String.IsNullOrWhiteSpace(channelList) Then
            Me.WriteClosedChannels(channelList, timeout)
            Me.SaveChannelPattern(memoryLocation, timeout)
            Me.WriteOpenAll(timeout)
        End If
        Return memoryLocation
    End Function

    ''' <summary>
    ''' Gets or sets the one-based location of the first memory location of the default channel
    ''' pattern set.
    ''' </summary>
    ''' <value> The first memory location of the default channel pattern set. </value>
    Public ReadOnly Property FirstMemoryLocation As Integer

    ''' <summary>
    ''' Gets or sets the one-based location of the memory location of the default channel pattern set.
    ''' </summary>
    ''' <value> The last automatic scan index. </value>
    Public ReadOnly Property LastMemoryLocation As Integer

    ''' <summary> Initializes the memory locations. </summary>
    Public Sub InitializeMemoryLocation()
        Me._FirstMemoryLocation = 0
        Me._LastMemoryLocation = 0
    End Sub

    ''' <summary>
    ''' Adds a channel list to the <see cref="LastMemoryLocation">+1: first available memory
    ''' location</see>.
    ''' </summary>
    ''' <param name="channelList"> List of channels. </param>
    ''' <param name="timeout">     The timeout. </param>
    ''' <returns> The new memory location. </returns>
    Public Function MemorizeChannelPattern(ByVal channelList As String, ByVal timeout As TimeSpan) As Integer
        If Not String.IsNullOrWhiteSpace(channelList) Then
            Me._LastMemoryLocation += 1
            If Me.LastMemoryLocation = 1 Then
                Me._FirstMemoryLocation = Me.LastMemoryLocation
            End If
            Return Me.SaveChannelPattern(channelList, Me.LastMemoryLocation, timeout)
        End If
        Return Me.LastMemoryLocation
    End Function

#End Region

#Region " SCAN LIST "

    ''' <summary> Gets or sets the scan list persists after toggling power. </summary>
    ''' <remarks>
    ''' If the scan lists persists after toggling power, the scan list initial values cannot be
    ''' checked.
    ''' </remarks>
    ''' <value> The scan list persists. </value>
    Public Overridable ReadOnly Property ScanListPersists As Boolean

    ''' <summary> List of scans. </summary>
    Private _ScanList As String

    ''' <summary> Gets or sets the cached Scan List. </summary>
    ''' <value> A List of scans. </value>
    Public Overloads Property ScanList As String
        Get
            Return Me._ScanList
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ScanList, StringComparison.OrdinalIgnoreCase) Then
                Me._ScanList = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Scan List. </summary>
    ''' <param name="value"> The scan list. </param>
    ''' <returns> A List of scans. </returns>
    Public Function ApplyScanList(ByVal value As String) As String
        Me.WriteScanList(value)
        Return Me.QueryScanList()
    End Function

    ''' <summary> Gets or sets the scan list command query. </summary>
    ''' <value> The scan list query command. </value>
    Protected Overridable Property ScanListQueryCommand As String

    ''' <summary>
    ''' Queries the Scan List. Also sets the <see cref="ScanList">Route on</see> sentinel.
    ''' </summary>
    ''' <returns> A List of scans. </returns>
    Public Function QueryScanList() As String
        Me.ScanList = Me.Query(Me.ScanList, Me.ScanListQueryCommand)
        Return Me.ScanList
    End Function

    ''' <summary> Gets or sets the scan list command format. </summary>
    ''' <value> The scan list command format. </value>
    Protected Overridable Property ScanListCommandFormat As String

    ''' <summary> Writes the Scan List. Does not read back from the instrument. </summary>
    ''' <param name="value"> The scan list. </param>
    ''' <returns> A List of scans. </returns>
    Public Function WriteScanList(ByVal value As String) As String
        Me.Write(Me.ScanListCommandFormat, value)
        Me.ScanList = value
        Return Me.ScanList
    End Function

#End Region

#Region " SCAN LIST FUNCTION "

    ''' <summary> List of scans. </summary>
    Private _ScanListFunction As String

    ''' <summary> Gets or sets the cached Scan List. </summary>
    ''' <value> A List of scans. </value>
    Public Overloads Property ScanListFunction As String
        Get
            Return Me._ScanListFunction
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ScanListFunction, StringComparison.OrdinalIgnoreCase) Then
                Me._ScanListFunction = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the scan list function described by value. </summary>
    ''' <param name="value">         The scan list. </param>
    ''' <param name="functionMode">  The function mode. </param>
    ''' <param name="functionModes"> The function modes. </param>
    ''' <returns> A String. </returns>
    Public Function ApplyScanListFunction(ByVal value As String, ByVal functionMode As SenseFunctionModes, ByVal functionModes As Pith.EnumReadWriteCollection) As String
        Me.WriteScanListFunction(value, functionMode, functionModes)
        Return Me.QueryScanListFunction()
    End Function

    ''' <summary> Gets the scan list function command query. </summary>
    ''' <value> The scan list query command. </value>
    Protected Overridable Property ScanListFunctionQueryCommand As String

    ''' <summary>
    ''' Queries the Scan List function. Also sets the <see cref="ScanList">Route on</see> sentinel.
    ''' </summary>
    ''' <returns> A List of scans. </returns>
    Public Function QueryScanListFunction() As String
        Me.ScanList = Me.Query(Me.ScanList, Me.ScanListQueryCommand)
        Return $"{Me.ScanList},{Me.ScanListFunction}"
    End Function

    ''' <summary> Gets the scan list function command format. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The scan list command format. </value>
    Protected Overridable Property ScanListFunctionCommandFormat As String

    ''' <summary> Writes the Scan List. Does not read back from the instrument. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         The scan list. </param>
    ''' <param name="functionMode">  The function mode. </param>
    ''' <param name="functionModes"> The function modes. </param>
    ''' <returns> A List of scans. </returns>
    Public Function WriteScanListFunction(ByVal value As String, ByVal functionMode As SenseFunctionModes, ByVal functionModes As Pith.EnumReadWriteCollection) As String
        If functionModes Is Nothing Then Throw New ArgumentNullException(NameOf(functionModes))
        Me.Write(Me.ScanListFunctionCommandFormat, value, functionModes.SelectItem(CLng(functionMode)).WriteValue)
        Me.ScanListFunction = functionModes.SelectItem(CLng(functionMode)).WriteValue
        Me.ScanList = value
        Return $"{ Me.ScanList},{ Me.ScanListFunction}"
    End Function

#End Region

#Region " SELECTED SCAN LIST TYPE "

    ''' <summary> Define function mode read writes. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="scanListTypeReadWrites"> The scan list type read writes. </param>
    Public Shared Sub DefineScanListTypeReadWrites(ByVal scanListTypeReadWrites As Pith.EnumReadWriteCollection)
        If scanListTypeReadWrites Is Nothing Then Throw New ArgumentNullException(NameOf(scanListTypeReadWrites))
        scanListTypeReadWrites.Clear()
        For Each ScanListType As VI.ScanListType In [Enum].GetValues(GetType(VI.ScanListType))
            scanListTypeReadWrites.Add(ScanListType)
        Next
    End Sub

    ''' <summary> Define function mode read writes. </summary>
    Protected Overridable Sub DefineScanListTypeReadWrites()
        Me._ScanListTypeReadWrites = New Pith.EnumReadWriteCollection
        RouteSubsystemBase.DefineScanListTypeReadWrites(Me.ScanListTypeReadWrites)
    End Sub

    ''' <summary> The scan list type read writes. </summary>
    Private _ScanListTypeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> Gets a dictionary of Sense function mode parses. </summary>
    ''' <value> A Dictionary of Sense function mode parses. </value>
    Public ReadOnly Property ScanListTypeReadWrites As Pith.EnumReadWriteCollection
        Get
            If Me._ScanListTypeReadWrites Is Nothing Then
                Me.DefineScanListTypeReadWrites()
            End If
            Return Me._ScanListTypeReadWrites
        End Get
    End Property

    ''' <summary> Type of the scan list. </summary>
    Private _ScanListType As ScanListType

    ''' <summary>
    ''' Gets or sets the supported Function Modes. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported Sense function modes. </value>
    Public Property ScanListType() As ScanListType
        Get
            Return Me._ScanListType
        End Get
        Set(ByVal value As ScanListType)
            If Not Me.ScanListType.Equals(value) Then
                Me._ScanListType = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> List of scans. </summary>
    Private _SelectedScanListType As String

    ''' <summary> Gets or sets the cached Selected Scan List Type. </summary>
    ''' <value> A List of scans. </value>
    Public Overloads Property SelectedScanListType As String
        Get
            Return Me._SelectedScanListType
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.SelectedScanListType, StringComparison.OrdinalIgnoreCase) Then
                Me._SelectedScanListType = value
                Me.ScanListType = CType(Me.ScanListTypeReadWrites.SelectItem(value).EnumValue, ScanListType)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Selected Scan List Type. </summary>
    ''' <param name="value"> The Selected Scan List Type. </param>
    ''' <returns> A List of scans. </returns>
    Public Function ApplySelectedScanListType(ByVal value As ScanListType) As ScanListType
        Me.WriteSelectedScanListType(value)
        Me.QuerySelectedScanListType()
        Return Me.ScanListType
    End Function

    ''' <summary> Writes and reads back the Selected Scan List Type. </summary>
    ''' <param name="value"> The Selected Scan List Type. </param>
    ''' <returns> A List of scans. </returns>
    Public Function ApplySelectedScanListType(ByVal value As String) As String
        Me.WriteSelectedScanListType(value)
        Return Me.QuerySelectedScanListType()
    End Function

    ''' <summary> Gets or sets the Selected Scan List Type command query. </summary>
    ''' <value> The Selected Scan List Type query command. </value>
    Protected Overridable Property SelectedScanListTypeQueryCommand As String

    ''' <summary>
    ''' Queries the Selected Scan List Type. Also sets the <see cref="SelectedScanListType">Route
    ''' on</see> sentinel.
    ''' </summary>
    ''' <returns> A List of scans. </returns>
    Public Function QuerySelectedScanListType() As String
        Me.SelectedScanListType = Me.Query(Me.SelectedScanListType, Me.SelectedScanListTypeQueryCommand)
        Return Me.SelectedScanListType
    End Function

    ''' <summary> Gets or sets the Selected Scan List Type command format. </summary>
    ''' <value> The Selected Scan List Type command format. </value>
    Protected Overridable Property SelectedScanListTypeCommandFormat As String

    ''' <summary>
    ''' Writes the Selected Scan List Type. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> The Selected Scan List Type. </param>
    ''' <returns> A List of scans. </returns>
    Public Function WriteSelectedScanListType(ByVal value As String) As String
        Me.Write(Me.SelectedScanListTypeCommandFormat, value)
        Me.SelectedScanListType = value
        Return Me.SelectedScanListType
    End Function

    ''' <summary>
    ''' Writes the Selected Scan List Type. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> The Selected Scan List Type. </param>
    ''' <returns> A List of scans. </returns>
    Public Overridable Function WriteSelectedScanListType(ByVal value As ScanListType) As ScanListType
        Me.SelectedScanListType = Me.WriteSelectedScanListType(Me.ScanListTypeReadWrites.SelectItem(value).WriteValue)
        Return Me.ScanListType
    End Function

#End Region

#Region " SLOT CARD TYPE "

    ''' <summary> Gets or sets the slot card type query command format. </summary>
    ''' <value> The slot card type query command format. </value>
    Protected Overridable Property SlotCardTypeQueryCommandFormat As String

    ''' <summary> Gets or sets a list of types of the slot cards. </summary>
    ''' <value> A list of types of the slot cards. </value>
    Private Property SlotCardTypes As IDictionary(Of Integer, String)

    ''' <summary> Slot card type. </summary>
    ''' <param name="slotNumber"> The slot number. </param>
    ''' <returns> A String. </returns>
    Public Function SlotCardType(ByVal slotNumber As Integer) As String
        Return If(Me._SlotCardTypes?.ContainsKey(slotNumber), Me._SlotCardTypes(slotNumber), String.Empty)
    End Function

    ''' <summary> Applies the card type. </summary>
    ''' <param name="cardNumber"> The card number. </param>
    ''' <param name="cardType">   Type of the card. </param>
    ''' <returns> A String. </returns>
    Public Function ApplySlotCardType(ByVal cardNumber As Integer, ByVal cardType As String) As String
        Me.WriteSlotCardType(cardNumber, cardType)
        Return Me.QuerySlotCardType(cardNumber)
    End Function

    ''' <summary> Queries the Slot Card Type. </summary>
    ''' <param name="slotNumber"> The slot number. </param>
    ''' <returns> A Slot Card Type. </returns>
    Public Function QuerySlotCardType(ByVal slotNumber As Integer) As String
        Dim value As String = If(String.IsNullOrWhiteSpace(Me.SlotCardTypeQueryCommandFormat),
            String.Empty,
            Me.Query("", String.Format(Me.SlotCardTypeQueryCommandFormat, slotNumber)))
        If Me._SlotCardTypes Is Nothing Then Me._SlotCardTypes = New Dictionary(Of Integer, String)
        If Me._SlotCardTypes.ContainsKey(slotNumber) Then
            Me._SlotCardTypes.Remove(slotNumber)
        End If
        If Not String.IsNullOrWhiteSpace(value) Then
            Me._SlotCardTypes.Add(slotNumber, value)
        End If
        Me.NotifyPropertyChanged(NameOf(RouteSubsystemBase.SlotCardType))
        Return value
    End Function

    ''' <summary> Gets or sets the slot card type command format. </summary>
    ''' <value> The slot card type command format. </value>
    Protected Overridable Property SlotCardTypeCommandFormat As String

    ''' <summary> Writes a slot card type. </summary>
    ''' <param name="cardNumber"> The card number. </param>
    ''' <param name="cardType">   Type of the card. </param>
    ''' <returns> A String. </returns>
    Public Function WriteSlotCardType(ByVal cardNumber As Integer, ByVal cardType As String) As String
        If Not String.IsNullOrWhiteSpace(Me.SlotCardTypeCommandFormat) Then
            Me.Write("", String.Format(Me.SlotCardTypeCommandFormat, cardNumber, cardType))
        End If
        Return cardType
    End Function

#End Region

#Region " SLOT CARD SETTLING TIME "

    ''' <summary> Gets or sets the slot card settling time query command format. </summary>
    ''' <value> The slot card settling time query command format. </value>
    Protected Overridable Property SlotCardSettlingTimeQueryCommandFormat As String

    ''' <summary> Gets or sets a list of times of the slot card settlings. </summary>
    ''' <value> A list of times of the slot card settlings. </value>
    Private Property SlotCardSettlingTimes As IDictionary(Of Integer, TimeSpan)

    ''' <summary> Slot card settling time. </summary>
    ''' <param name="slotNumber"> The slot number. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function SlotCardSettlingTime(ByVal slotNumber As Integer) As TimeSpan
        Dim ts As TimeSpan = TimeSpan.Zero
        If Me._SlotCardSettlingTimes?.ContainsKey(slotNumber) Then
            ts = Me._SlotCardSettlingTimes(slotNumber)
        End If
        Return ts
    End Function

    ''' <summary> Applies the slot card settling time. </summary>
    ''' <param name="cardNumber">   The card number. </param>
    ''' <param name="settlingTime"> The settling time. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function ApplySlotCardSettlingTime(ByVal cardNumber As Integer, ByVal settlingTime As TimeSpan) As TimeSpan
        Me.WriteSlotCardSettlingTime(cardNumber, settlingTime)
        Return Me.QuerySlotCardSettlingTime(cardNumber)
    End Function

    ''' <summary> Queries the Slot Card settling time. </summary>
    ''' <param name="slotNumber"> The slot number. </param>
    ''' <returns> A Slot Card settling time. </returns>
    Public Function QuerySlotCardSettlingTime(ByVal slotNumber As Integer) As TimeSpan
        Dim ts As TimeSpan = TimeSpan.Zero
        Dim value As Double?
        If Not String.IsNullOrWhiteSpace(Me.SlotCardSettlingTimeQueryCommandFormat) Then
            value = Me.Query(New Double?, String.Format(Me.SlotCardSettlingTimeQueryCommandFormat, slotNumber))
            ts = If(value.HasValue, TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * value.Value)), TimeSpan.Zero)
        End If
        If Me._SlotCardSettlingTimes Is Nothing Then Me._SlotCardSettlingTimes = New Dictionary(Of Integer, TimeSpan)
        If Me._SlotCardSettlingTimes.ContainsKey(slotNumber) Then
            Me._SlotCardSettlingTimes.Remove(slotNumber)
        End If
        If value.HasValue Then
            Me._SlotCardSettlingTimes.Add(slotNumber, ts)
        End If
        Me.NotifyPropertyChanged(NameOf(RouteSubsystemBase.SlotCardSettlingTime))
        Return ts
    End Function

    ''' <summary> Gets or sets the slot card settling time command format. </summary>
    ''' <value> The slot card settling time command format. </value>
    Protected Overridable Property SlotCardSettlingTimeCommandFormat As String

    ''' <summary> Writes a slot card settling time. </summary>
    ''' <param name="cardNumber">   The card number. </param>
    ''' <param name="settlingTime"> The settling time. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function WriteSlotCardSettlingTime(ByVal cardNumber As Integer, ByVal settlingTime As TimeSpan) As TimeSpan
        Return Me.WriteSlotCardSettlingTime(cardNumber, settlingTime.TotalSeconds)
    End Function

    ''' <summary> Writes a slot card settling time. </summary>
    ''' <param name="cardNumber">   The card number. </param>
    ''' <param name="settlingTime"> The settling time. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function WriteSlotCardSettlingTime(ByVal cardNumber As Integer, ByVal settlingTime As Double) As TimeSpan
        If Not String.IsNullOrWhiteSpace(Me.SlotCardSettlingTimeCommandFormat) Then
            Me.Write(settlingTime, String.Format(Me.SlotCardSettlingTimeCommandFormat, cardNumber, settlingTime))
        End If
        Return TimeSpan.FromSeconds(settlingTime)
    End Function

#End Region

End Class

''' <summary> Specifies the Scan List Types. </summary>
Public Enum ScanListType

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None (NONE)")>
    None

    <ComponentModel.Description("Internal (INT)")>
    Internal

    ''' <summary> An enum constant representing the external option. </summary>
    <ComponentModel.Description("External (EXT)")>
    External

    ''' <summary> An enum constant representing the ratio option. </summary>
    <ComponentModel.Description("Ratio (RAT)")>
    Ratio

    ''' <summary> An enum constant representing the delta option. </summary>
    <ComponentModel.Description("Delta (DELT)")>
    Delta
End Enum

