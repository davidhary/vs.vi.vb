'---------------------------------------------------------------------------------------------------
' file:		Device\Subsystems\K2002Device.vb
'
' summary:	2002 device class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements a Keithley 2002 instrument device. </summary>
''' <remarks>
''' An instrument is defined, for the purpose of this library, as a device with a front panel.
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class K2002Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="K2002Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        If statusSubsystem IsNot Nothing Then
            statusSubsystem.ExpectedLanguage = VI.Pith.Ieee488.Syntax.LanguageTsp
            AddHandler K2002.My.MySettings.Default.PropertyChanged, AddressOf Me.HandleSettingsPropertyChanged
        End If
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As K2002Device
        Dim device As K2002Device = Nothing
        Try
            device = New K2002Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As K2002Device) As K2002Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
            ' release unmanaged-only resources.
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(K2002Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindMeasureSubsystem(Nothing)
            Me.BindFormatSubsystem(Nothing)
            Me.BindRouteSubsystem(Nothing)
            Me.BindSenseSubsystem(Nothing)
            Me.BindSenseVoltageSubsystem(Nothing)
            Me.BindSenseCurrentSubsystem(Nothing)
            Me.BindSenseResistanceSubsystem(Nothing)
            Me.BindSenseResistanceFourWireSubsystem(Nothing)
            Me.BindTraceSubsystem(Nothing)
            Me.BindTriggerSubsystem(Nothing)
            Me.BindBinningSubsystem(Nothing)
            Me.BindArmLayer1Subsystem(Nothing)
            Me.BindArmLayer2Subsystem(Nothing)
            Me.BindDigitalOutputSubsystem(Nothing)
            Me.BindSystemSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            ' better add before the format subsystem, which reset initializes the readings.
            Me.BindMeasureSubsystem(New MeasureSubsystem(Me.StatusSubsystem))
            ' the measure subsystem readings are set when the format system is reset.
            Me.BindFormatSubsystem(New FormatSubsystem(Me.StatusSubsystem))
            Me.BindRouteSubsystem(New RouteSubsystem(Me.StatusSubsystem))
            Me.BindSenseVoltageSubsystem(New SenseVoltageSubsystem(Me.StatusSubsystem))
            Me.BindSenseCurrentSubsystem(New SenseCurrentSubsystem(Me.StatusSubsystem))
            Me.BindSenseResistanceSubsystem(New SenseResistanceSubsystem(Me.StatusSubsystem))
            Me.BindSenseResistanceFourWireSubsystem(New SenseResistanceFourWireSubsystem(Me.StatusSubsystem))
            Me.BindSenseSubsystem(New SenseSubsystem(Me.StatusSubsystem))
            Me.BindTraceSubsystem(New TraceSubsystem(Me.StatusSubsystem))
            Me.BindTriggerSubsystem(New TriggerSubsystem(Me.StatusSubsystem))
            Me.BindBinningSubsystem(New BinningSubsystem(Me.StatusSubsystem))
            Me.BindArmLayer1Subsystem(New ArmLayer1Subsystem(Me.StatusSubsystem))
            Me.BindArmLayer2Subsystem(New ArmLayer2Subsystem(Me.StatusSubsystem))
            Me.BindDigitalOutputSubsystem(New DigitalOutputSubsystem(Me.StatusSubsystem))
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Gets or sets the arm layer 1 subsystem. </summary>
    ''' <value> The arm layer 1 subsystem. </value>
    Public ReadOnly Property ArmLayer1Subsystem As ArmLayer1Subsystem

    ''' <summary> Binds the Arm Layer 1 subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindArmLayer1Subsystem(ByVal subsystem As ArmLayer1Subsystem)
        If Me.ArmLayer1Subsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ArmLayer1Subsystem)
            Me._ArmLayer1Subsystem = Nothing
        End If
        Me._ArmLayer1Subsystem = subsystem
        If Me.ArmLayer1Subsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ArmLayer1Subsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the arm layer 2 subsystem. </summary>
    ''' <value> The arm layer 2 subsystem. </value>
    Public ReadOnly Property ArmLayer2Subsystem As ArmLayer2Subsystem

    ''' <summary> Binds the ArmLayer2 subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindArmLayer2Subsystem(ByVal subsystem As ArmLayer2Subsystem)
        If Me.ArmLayer2Subsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ArmLayer2Subsystem)
            Me._ArmLayer2Subsystem = Nothing
        End If
        Me._ArmLayer2Subsystem = subsystem
        If Me.ArmLayer2Subsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ArmLayer2Subsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Binning subsystem. </summary>
    ''' <value> The Binning subsystem. </value>
    Public ReadOnly Property BinningSubsystem As BinningSubsystem

    ''' <summary> Binds the Binning subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindBinningSubsystem(ByVal subsystem As BinningSubsystem)
        If Me.BinningSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.BinningSubsystem)
            Me._BinningSubsystem = Nothing
        End If
        Me._BinningSubsystem = subsystem
        If Me.BinningSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.BinningSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Digital output subsystem. </summary>
    ''' <value> The Digital Output subsystem. </value>
    Public ReadOnly Property DigitalOutputSubsystem As DigitalOutputSubsystem

    ''' <summary> Binds the Digital Output subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindDigitalOutputSubsystem(ByVal subsystem As DigitalOutputSubsystem)
        If Me.DigitalOutputSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.DigitalOutputSubsystem)
            Me._DigitalOutputSubsystem.Dispose()
            Me._DigitalOutputSubsystem = Nothing
        End If
        Me._DigitalOutputSubsystem = subsystem
        If Me.DigitalOutputSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.DigitalOutputSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Measure Subsystem. </summary>
    ''' <value> The Measure Subsystem. </value>
    Public ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Binds the Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureSubsystem(ByVal subsystem As MeasureSubsystem)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        Me._MeasureSubsystem = subsystem
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Route Subsystem. </summary>
    ''' <value> The Route Subsystem. </value>
    Public ReadOnly Property RouteSubsystem As RouteSubsystem

    ''' <summary> Binds the Route subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindRouteSubsystem(ByVal subsystem As RouteSubsystem)
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.RouteSubsystem)
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.RouteSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Current Subsystem. </summary>
    ''' <value> The Sense Current Subsystem. </value>
    Public ReadOnly Property SenseCurrentSubsystem As SenseCurrentSubsystem

    ''' <summary> Binds the Sense Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseCurrentSubsystem(ByVal subsystem As SenseCurrentSubsystem)
        If Me.SenseCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseCurrentSubsystem)
            Me._SenseCurrentSubsystem = Nothing
        End If
        Me._SenseCurrentSubsystem = subsystem
        If Me.SenseCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Voltage Subsystem. </summary>
    ''' <value> The Sense Voltage Subsystem. </value>
    Public ReadOnly Property SenseVoltageSubsystem As SenseVoltageSubsystem

    ''' <summary> Binds the Sense Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseVoltageSubsystem(ByVal subsystem As SenseVoltageSubsystem)
        If Me.SenseVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseVoltageSubsystem)
            Me._SenseVoltageSubsystem = Nothing
        End If
        Me._SenseVoltageSubsystem = subsystem
        If Me.SenseVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseVoltageSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Four Wire Resistance Subsystem. </summary>
    ''' <value> The Sense Four Wire Resistance Subsystem. </value>
    Public ReadOnly Property SenseResistanceFourWireSubsystem As SenseResistanceFourWireSubsystem

    ''' <summary> Binds the Sense Four Wire Resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseResistanceFourWireSubsystem(ByVal subsystem As SenseResistanceFourWireSubsystem)
        If Me.SenseResistanceFourWireSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseResistanceFourWireSubsystem)
            Me._SenseResistanceFourWireSubsystem = Nothing
        End If
        Me._SenseResistanceFourWireSubsystem = subsystem
        If Me.SenseResistanceFourWireSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseResistanceFourWireSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Resistance Subsystem. </summary>
    ''' <value> The Sense Resistance Subsystem. </value>
    Public ReadOnly Property SenseResistanceSubsystem As SenseResistanceSubsystem

    ''' <summary> Binds the Sense Resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseResistanceSubsystem(ByVal subsystem As SenseResistanceSubsystem)
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseResistanceSubsystem)
            Me._SenseResistanceSubsystem = Nothing
        End If
        Me._SenseResistanceSubsystem = subsystem
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseResistanceSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Trigger Subsystem. </summary>
    ''' <value> The Trigger Subsystem. </value>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Binds the Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystem)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.TriggerSubsystem)
            If Me.TriggerSubsystem.Session.IsSessionOpen Then Me.TriggerSubsystem.Abort()
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TriggerSubsystem)
        End If
    End Sub

#End Region

#Region " FORMAT SUBSYSTEM "

    ''' <summary> Gets or sets the Format Subsystem. </summary>
    ''' <value> The Format Subsystem. </value>
    Public ReadOnly Property FormatSubsystem As FormatSubsystem

    ''' <summary> Binds the Format subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindFormatSubsystem(ByVal subsystem As FormatSubsystem)
        If Me.FormatSubsystem IsNot Nothing Then
            RemoveHandler Me.FormatSubsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.FormatSubsystem)
            Me._FormatSubsystem = Nothing
        End If
        Me._FormatSubsystem = subsystem
        If Me.FormatSubsystem IsNot Nothing Then
            AddHandler Me.FormatSubsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            Me.Subsystems.Add(Me.FormatSubsystem)
            Me.HandlePropertyChanged(Me.FormatSubsystem, NameOf(VI.FormatSubsystemBase.Elements))
        End If
    End Sub

    ''' <summary> Handle the Format subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As FormatSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.FormatSubsystemBase.Elements)
                If subsystem.SupportsElements Then
                    ' supported elements for the 2002 are: Reading channel, reading number, units, timestamp and status.
                    ' must define reading at a minimum
                    Me.MeasureSubsystem.ReadingAmounts.Initialize(subsystem.Elements Or ReadingElementTypes.Reading)
                End If
        End Select
    End Sub

    ''' <summary> Format subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FormatSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(FormatSubsystemBase)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, FormatSubsystemBase), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE SUBSYSTEM "

    ''' <summary> Gets or sets the Sense Subsystem. </summary>
    ''' <value> The Sense Subsystem. </value>
    Public ReadOnly Property SenseSubsystem As SenseSubsystem

    ''' <summary> Binds the Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseSubsystem(ByVal subsystem As SenseSubsystem)
        If Me.SenseSubsystem IsNot Nothing Then
            RemoveHandler Me.SenseSubsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        Me._SenseSubsystem = subsystem
        If Me.SenseSubsystem IsNot Nothing Then
            AddHandler Me.SenseSubsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.Subsystems.Add(Me.SenseSubsystem)
            Me.HandlePropertyChanged(Me.SenseSubsystem, NameOf(K2002.SenseSubsystem.FunctionMode))
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SenseSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K2002.SenseSubsystem.FunctionMode)
                Me.MeasureSubsystem.FunctionMode = subsystem.FunctionMode
                Me.SelectSenseFunctionSubsystem(subsystem.FunctionMode.GetValueOrDefault(SenseFunctionModes.Voltage))
        End Select
    End Sub

    ''' <summary> Sense subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inSenseion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(K2002.SenseSubsystem)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, K2002.SenseSubsystem), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Gets or sets the sense function subsystem. </summary>
    ''' <value> The sense function subsystem. </value>
    Public ReadOnly Property SenseFunctionSubsystem As SenseFunctionSubsystemBase

    ''' <summary> Select sense function subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="functionMode"> The function mode. </param>
    Private Sub SelectSenseFunctionSubsystem(ByVal functionMode As SenseFunctionModes)
        Select Case functionMode
            Case SenseFunctionModes.Current
                Me._SenseFunctionSubsystem = Me.SenseCurrentSubsystem
            Case SenseFunctionModes.Resistance
                Me._SenseFunctionSubsystem = Me.SenseResistanceSubsystem
            Case SenseFunctionModes.ResistanceFourWire
                Me._SenseFunctionSubsystem = Me.SenseResistanceFourWireSubsystem
            Case SenseFunctionModes.Voltage
                Me._SenseFunctionSubsystem = Me.SenseVoltageSubsystem
        End Select
    End Sub

#End Region

#Region " STATUS SUBSYSTEM "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM SUBSYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            Me.Subsystems.Add(Me.SystemSubsystem)
            If Me.TraceSubsystem IsNot Nothing Then
                Me.TraceSubsystem.MemoryOption = Me.SystemSubsystem.MemoryOption
            End If
        End If
    End Sub

    ''' <summary> Handle the System subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K2002.SystemSubsystem.MemoryOption)
                Me.TraceSubsystem.MemoryOption = subsystem.MemoryOption
        End Select
    End Sub

    ''' <summary> System subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inSystemion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(K2002.SystemSubsystem)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, K2002.SystemSubsystem), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TRACE SUBSYSTEM "

    ''' <summary> Gets or sets the Trace Subsystem. </summary>
    ''' <value> The Trace Subsystem. </value>
    Public ReadOnly Property TraceSubsystem As TraceSubsystem

    ''' <summary> Binds the Trace subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTraceSubsystem(ByVal subsystem As TraceSubsystem)
        If Me.TraceSubsystem IsNot Nothing Then
            RemoveHandler Me.TraceSubsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.TraceSubsystem)
            Me._TraceSubsystem = Nothing
        End If
        Me._TraceSubsystem = subsystem
        If Me.TraceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TraceSubsystem)
            AddHandler Me.TraceSubsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Trace subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TraceSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.TraceSubsystemBase.LastBufferReading)
                Me.MeasureSubsystem.ParsePrimaryReading(subsystem.LastBufferReading?.Reading)
        End Select
    End Sub

    ''' <summary> Trace subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inTraceion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TraceSubsystemBase)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, TraceSubsystemBase), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("K2002 Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.Settings.ReadDelay)
                Me.Session.ReadDelay = My.Settings.ReadDelay
            Case NameOf(My.Settings.StatusReadDelay)
                Me.Session.StatusReadDelay = My.Settings.StatusReadDelay
            Case NameOf(My.Settings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = My.Settings.StatusReadTurnaroundTime
        End Select
    End Sub

    ''' <summary> Handles settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleSettingsPropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
