<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ScannerView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ScannerView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._RelayView = New isr.VI.Facade.RelayView()
        Me._ScanListView = New isr.VI.Facade.ScanListView()
        Me._SlotView = New isr.VI.Facade.SlotView()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ExternalTriggerPlanToolMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ExplainExternalTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConfigureExternalTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._BusTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ExplainBusTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConfigureBusTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InitiateButton = New System.Windows.Forms.ToolStripButton()
        Me._SendBusTriggerButton = New System.Windows.Forms.ToolStripButton()
        Me._AbortButton = New System.Windows.Forms.ToolStripButton()
        Me._ServiceRequestView = New isr.VI.Facade.ServiceRequestView()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._Layout.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 2
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2.0!))
        Me._Layout.Controls.Add(Me._RelayView, 0, 4)
        Me._Layout.Controls.Add(Me._ScanListView, 0, 3)
        Me._Layout.Controls.Add(Me._SlotView, 0, 1)
        Me._Layout.Controls.Add(Me._SubsystemToolStrip, 0, 0)
        Me._Layout.Controls.Add(Me._ServiceRequestView, 0, 2)
        Me._Layout.Controls.Add(Me._InfoTextBox, 0, 5)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 6
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Size = New System.Drawing.Size(421, 324)
        Me._Layout.TabIndex = 0
        '
        '_RelayView
        '
        Me._RelayView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._RelayView.Dock = System.Windows.Forms.DockStyle.Top
        Me._RelayView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RelayView.Location = New System.Drawing.Point(3, 132)
        Me._RelayView.Name = "_RelayView"
        Me._RelayView.Padding = New System.Windows.Forms.Padding(1)
        Me._RelayView.Size = New System.Drawing.Size(413, 28)
        Me._RelayView.TabIndex = 17
        '
        '_ScanListView
        '
        Me._ScanListView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ScanListView.Dock = System.Windows.Forms.DockStyle.Top
        Me._ScanListView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ScanListView.Location = New System.Drawing.Point(3, 100)
        Me._ScanListView.Name = "_ScanListView"
        Me._ScanListView.Padding = New System.Windows.Forms.Padding(1)
        Me._ScanListView.Size = New System.Drawing.Size(413, 26)
        Me._ScanListView.TabIndex = 16
        '
        '_SlotView
        '
        Me._SlotView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SlotView.Dock = System.Windows.Forms.DockStyle.Top
        Me._SlotView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SlotView.Location = New System.Drawing.Point(3, 28)
        Me._SlotView.Name = "_SlotView"
        Me._SlotView.Padding = New System.Windows.Forms.Padding(1)
        Me._SlotView.Size = New System.Drawing.Size(413, 31)
        Me._SlotView.TabIndex = 1
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._InitiateButton, Me._SendBusTriggerButton, Me._AbortButton})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(419, 25)
        Me._SubsystemToolStrip.TabIndex = 2
        Me._SubsystemToolStrip.Text = "Subsystem Tool strip"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ExternalTriggerPlanToolMenuItem, Me._BusTriggerPlanMenuItem})
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(65, 22)
        Me._SubsystemSplitButton.Text = "Scanner"
        Me._SubsystemSplitButton.ToolTipText = "Device options and actions"
        '
        '_ExternalTriggerPlanToolMenuItem
        '
        Me._ExternalTriggerPlanToolMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ExplainExternalTriggerPlanMenuItem, Me._ConfigureExternalTriggerPlanMenuItem})
        Me._ExternalTriggerPlanToolMenuItem.Name = "_ExternalTriggerPlanToolMenuItem"
        Me._ExternalTriggerPlanToolMenuItem.Size = New System.Drawing.Size(190, 22)
        Me._ExternalTriggerPlanToolMenuItem.Text = "External Trigger Plan..."
        Me._ExternalTriggerPlanToolMenuItem.ToolTipText = "Configures an external trigger plan"
        '
        '_ExplainExternalTriggerPlanMenuItem
        '
        Me._ExplainExternalTriggerPlanMenuItem.Name = "_ExplainExternalTriggerPlanMenuItem"
        Me._ExplainExternalTriggerPlanMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ExplainExternalTriggerPlanMenuItem.Text = "Info"
        Me._ExplainExternalTriggerPlanMenuItem.ToolTipText = "Displays information about the external trigger plan"
        '
        '_ConfigureExternalTriggerPlanMenuItem
        '
        Me._ConfigureExternalTriggerPlanMenuItem.Name = "_ConfigureExternalTriggerPlanMenuItem"
        Me._ConfigureExternalTriggerPlanMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ConfigureExternalTriggerPlanMenuItem.Text = "Configure"
        Me._ConfigureExternalTriggerPlanMenuItem.ToolTipText = "Configures an external trigger plan"
        '
        '_BusTriggerPlanMenuItem
        '
        Me._BusTriggerPlanMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ExplainBusTriggerPlanMenuItem, Me._ConfigureBusTriggerPlanMenuItem})
        Me._BusTriggerPlanMenuItem.Name = "_BusTriggerPlanMenuItem"
        Me._BusTriggerPlanMenuItem.Size = New System.Drawing.Size(190, 22)
        Me._BusTriggerPlanMenuItem.Text = "Bus Trigger Plan..."
        Me._BusTriggerPlanMenuItem.ToolTipText = "Configures a bus trigger plan"
        '
        '_ExplainBusTriggerPlanMenuItem
        '
        Me._ExplainBusTriggerPlanMenuItem.Name = "_ExplainBusTriggerPlanMenuItem"
        Me._ExplainBusTriggerPlanMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ExplainBusTriggerPlanMenuItem.Text = "Info"
        Me._ExplainBusTriggerPlanMenuItem.ToolTipText = "Displays information about the simple trigger plan"
        '
        '_ConfigureBusTriggerPlanMenuItem
        '
        Me._ConfigureBusTriggerPlanMenuItem.Name = "_ConfigureBusTriggerPlanMenuItem"
        Me._ConfigureBusTriggerPlanMenuItem.Size = New System.Drawing.Size(127, 22)
        Me._ConfigureBusTriggerPlanMenuItem.Text = "Configure"
        Me._ConfigureBusTriggerPlanMenuItem.ToolTipText = "Configures a bus trigger plan"
        '
        '_InitiateButton
        '
        Me._InitiateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._InitiateButton.Image = CType(resources.GetObject("_InitiateButton.Image"), System.Drawing.Image)
        Me._InitiateButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._InitiateButton.Name = "_InitiateButton"
        Me._InitiateButton.Size = New System.Drawing.Size(47, 22)
        Me._InitiateButton.Text = "Initiate"
        Me._InitiateButton.ToolTipText = "Starts the trigger plan"
        '
        '_SendBusTriggerButton
        '
        Me._SendBusTriggerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SendBusTriggerButton.Image = CType(resources.GetObject("_SendBusTriggerButton.Image"), System.Drawing.Image)
        Me._SendBusTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SendBusTriggerButton.Name = "_SendBusTriggerButton"
        Me._SendBusTriggerButton.Size = New System.Drawing.Size(43, 22)
        Me._SendBusTriggerButton.Text = "Assert"
        Me._SendBusTriggerButton.ToolTipText = "Sends a bus trigger to the instrument"
        '
        '_AbortButton
        '
        Me._AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AbortButton.Image = CType(resources.GetObject("_AbortButton.Image"), System.Drawing.Image)
        Me._AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AbortButton.Margin = New System.Windows.Forms.Padding(10, 1, 0, 2)
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(41, 22)
        Me._AbortButton.Text = "Abort"
        Me._AbortButton.ToolTipText = "Aborts a trigger plan"
        '
        '_ServiceRequestView
        '
        Me._ServiceRequestView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ServiceRequestView.Dock = System.Windows.Forms.DockStyle.Top
        Me._ServiceRequestView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ServiceRequestView.Location = New System.Drawing.Point(3, 65)
        Me._ServiceRequestView.Name = "_ServiceRequestView"
        Me._ServiceRequestView.Padding = New System.Windows.Forms.Padding(1)
        Me._ServiceRequestView.Size = New System.Drawing.Size(413, 29)
        Me._ServiceRequestView.TabIndex = 3
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InfoTextBox.Location = New System.Drawing.Point(3, 166)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ReadOnly = True
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InfoTextBox.Size = New System.Drawing.Size(413, 155)
        Me._InfoTextBox.TabIndex = 4
        Me._InfoTextBox.Text = "<info>"
        '
        'ScannerView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "ScannerView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(423, 326)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _SlotView As Facade.SlotView
    Private WithEvents _InfoTextBox As Windows.Forms.TextBox
    Private WithEvents _ExternalTriggerPlanToolMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ExplainExternalTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ConfigureExternalTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _BusTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ExplainBusTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ConfigureBusTriggerPlanMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InitiateButton As Windows.Forms.ToolStripButton
    Private WithEvents _SendBusTriggerButton As Windows.Forms.ToolStripButton
    Private WithEvents _AbortButton As Windows.Forms.ToolStripButton
    Private WithEvents _ServiceRequestView As Facade.ServiceRequestView
    Private WithEvents _ScanListView As Facade.ScanListView
    Private WithEvents _RelayView As Facade.RelayView
End Class
