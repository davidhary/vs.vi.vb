'---------------------------------------------------------------------------------------------------
' file:		.VI\K7000\Subsystems_Initial.vb
'
' summary:	initial values tests
'---------------------------------------------------------------------------------------------------

Partial Public Class SubsystemsTests

    ''' <summary> Opens session check status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
    ''' <param name="resourceInfo">     Information describing the resource. </param>
    ''' <param name="subsystemsInfo">   Information describing the subsystems. </param>
    Private Shared Sub OpenSessionCheckStatus(ByVal readErrorEnabled As Boolean, ByVal resourceInfo As ResourceSettings, ByVal subsystemsInfo As SubsystemsSettings)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValues(device.Session, resourceInfo, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceModel(device.StatusSubsystemBase, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceErrors(device.StatusSubsystemBase, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertTerminationValues(device.Session, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.RouteSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.TriggerSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertClearSessionDeviceErrors(device, subsystemsInfo)
                If readErrorEnabled Then isr.VI.DeviceTests.DeviceManager.AssertReadingDeviceErrors(device, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOrphanMessages(device.StatusSubsystemBase)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> A test for Open Session and status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionCheckStatusTest()
        SubsystemsTests.OpenSessionCheckStatus(False, isr.VI.K7000Tests.K7000ResourceInfo, isr.VI.K7000Tests.SubsystemsInfo)
    End Sub

    ''' <summary> (Unit Test Method) tests open session read device errors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionReadDeviceErrorsTest()
        SubsystemsTests.OpenSessionCheckStatus(True, isr.VI.K7000Tests.K7000ResourceInfo, isr.VI.K7000Tests.SubsystemsInfo)
    End Sub

End Class
