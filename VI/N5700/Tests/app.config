﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <sectionGroup name="userSettings" type="System.Configuration.UserSettingsGroup">
      <section name="isr.VI.N5700Tests.MySettings" type="System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" allowExeDefinition="MachineToLocalUser" requirePermission="false" />
      <section name="isr.VI.N5700Tests.N5700ResourceInfo" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.N5700Tests.N5700SubsystemsInfo" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.N5700Tests.N5700ResistanceTestInfo" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.N5700Tests.TestSite" type="System.Configuration.ClientSettingsSection" />
    </sectionGroup>
  </configSections>

  <userSettings>

    <!-- TEST SITE SETTINGS -->
    <isr.VI.N5700Tests.TestSite>
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="All" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="IPv4Prefixes" serializeAs="String">
        <value>192.168|10.1</value>
      </setting>
      <setting name="TimeZones" serializeAs="String">
        <value>Pacific Standard Time|Central Standard Time</value>
      </setting>
      <setting name="TimeZoneOffsets" serializeAs="String">
        <value>-8|-6</value>
      </setting>
    </isr.VI.N5700Tests.TestSite>

    <!-- 5700 RESOURCE INFO -->
    <isr.VI.N5700Tests.MySettings>
      <setting name="Setting" serializeAs="String">
        <value>5</value>
      </setting>
    </isr.VI.N5700Tests.MySettings>
    <isr.VI.N5700Tests.N5700ResourceInfo>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>

      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- RESOURCE -->
      <setting name="ResourceModel" serializeAs="String">
        <value>2002</value>
      </setting>
      <setting name="ResourceNamesDelimiter" serializeAs="String">
        <value>|</value>
      </setting>
      <setting name="ResourceNames" serializeAs="String">
        <value>
          TCPIP0::192.168.0.254::gpib0,22::INSTR|

          TCPIP0::192.168.0.254::gpib0,22::INSTR|
          TCPIP0::10.1.1.25::gpib0,22::INSTR|
          TCPIP0::10.1.1.24::gpib0,22::INSTR
		  </value>
      </setting>
      <setting name="ResourceTitle" serializeAs="String">
        <value>N5700</value>
      </setting>
      <setting name="Language" serializeAs="String">
        <value>SCPI</value>
      </setting>
      <setting name="FirmwareRevision" serializeAs="String">
        <value>1.6.4c</value>
      </setting>
    </isr.VI.N5700Tests.N5700ResourceInfo>
    <!-- 5700 SUBSYSTEMS INFO -->
    <isr.VI.N5700Tests.N5700SubsystemsInfo>

      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>

      <!-- VISA SESSION -->
      <setting name="KeepAliveQueryCommand" serializeAs="String">
        <value>*OPC?</value>
      </setting>
      <setting name="KeepAliveCommand" serializeAs="String">
        <value>*OPC</value>
      </setting>
      <setting name="ReadTerminationEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="TerminationCharacter" serializeAs="String">
        <value>10</value>
      </setting>
      <!-- INITIAL MEASURE VALUES -->
      <setting name="InitialPowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
      <setting name="InitialAutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="InitialAutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="InitialSenseFunction" serializeAs="String">
        <value>VoltageDC</value>
      </setting>
      <setting name="InitialRemoteSenseSelected" serializeAs="String">
        <value>False</value>
      </setting>
      <!-- STATUS SETTINGS -->
      <setting name="LineFrequency" serializeAs="String">
        <value>60</value>
      </setting>
      <!-- INITIAL SENSE VALUES -->
      <setting name="InitialFrontTerminalsSelected" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- DEVICE ERRORS -->
      <setting name="ParseCompoundErrorMessage" serializeAs="String">
        <value>-113,"Undefined header;1;2018/05/26 14:00:14.871"</value>
      </setting>
      <setting name="ParseErrorNumber" serializeAs="String">
        <value>-113</value>
      </setting>
      <setting name="ParseErrorMessage" serializeAs="String">
        <value>Undefined header</value>
      </setting>
      <setting name="ParseErrorLevel" serializeAs="String">
        <value>1</value>
      </setting>
      <setting name="ErroneousCommand" serializeAs="String">
        <value>*CLL</value>
      </setting>
      <!-- 140 ms does not work -->
      <setting name="ErrorAvailableMillisecondsDelay" serializeAs="String">
        <value>0</value>
      </setting>
      <setting name="ExpectedCompoundErrorMessage" serializeAs="String">
        <value>-113,"Undefined header"</value>
      </setting>
      <setting name="ExpectedErrorNumber" serializeAs="String">
        <value>-113</value>
      </setting>
      <setting name="ExpectedErrorMessage" serializeAs="String">
        <value>Undefined header</value>
      </setting>
      <setting name="ExpectedErrorLevel" serializeAs="String">
        <value>0</value>
      </setting>
    </isr.VI.N5700Tests.N5700SubsystemsInfo>
    <!-- 5700 RESISTANCE -->
    <isr.VI.N5700Tests.N5700ResistanceTestInfo>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>

      <!-- MEASUREMENT SETTINGS -->
      <setting name="AutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="AutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="SenseFunction" serializeAs="String">
        <value>3</value>
      </setting>
      <setting name="PowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
      <!-- RESISTANCE MEASUREMENT: SETTINGS -->
      <setting name="FrontTerminalsSelected" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="RemoteSenseSelected" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="SourceFunction" serializeAs="String">
        <value>Current</value>
      </setting>
      <setting name="SourceLevel" serializeAs="String">
        <value>0.02</value>
      </setting>
      <!-- RESISTANCE MEASUREMENT: VALUE -->
      <setting name="ExpectedResistance" serializeAs="String">
        <value>100</value>
      </setting>
      <setting name="ResistanceTolerance" serializeAs="String">
        <value>0.01</value>
      </setting>
    </isr.VI.N5700Tests.N5700ResistanceTestInfo>
  </userSettings>
  <system.web>
    <membership defaultProvider="ClientAuthenticationMembershipProvider">
      <providers>
        <add name="ClientAuthenticationMembershipProvider" type="System.Web.ClientServices.Providers.ClientFormsAuthenticationMembershipProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" />
      </providers>
    </membership>
    <roleManager defaultProvider="ClientRoleProvider" enabled="true">
      <providers>
        <add name="ClientRoleProvider" type="System.Web.ClientServices.Providers.ClientRoleProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" cacheTimeout="86400" />
      </providers>
    </roleManager>
  </system.web>
</configuration>