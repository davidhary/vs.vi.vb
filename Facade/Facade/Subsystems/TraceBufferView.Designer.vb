<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TraceBufferView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TraceBufferView))
        Me._LimitToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FeedSourceComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._FeedSourceComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ElementGroupToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._BufferControlComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._FeedControlComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._BufferConfigurationToolStrip = New System.Windows.Forms.ToolStrip()
        Me._BufferCondigureLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BufferNameLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SizeNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SizeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._PreTriggerCountNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PreTriggerCountNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._ClearBufferButton = New System.Windows.Forms.ToolStripButton()
        Me._FreeCountLabelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._FreeCountLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BufferFormatToolStrip = New System.Windows.Forms.ToolStrip()
        Me._FormatLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TimestampFormatToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._LimitToolStripPanel.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me._BufferConfigurationToolStrip.SuspendLayout()
        Me._BufferFormatToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_LimitToolStripPanel
        '
        Me._LimitToolStripPanel.BackColor = System.Drawing.Color.Transparent
        Me._LimitToolStripPanel.Controls.Add(Me._BufferConfigurationToolStrip)
        Me._LimitToolStripPanel.Controls.Add(Me._SubsystemToolStrip)
        Me._LimitToolStripPanel.Controls.Add(Me._BufferFormatToolStrip)
        Me._LimitToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._LimitToolStripPanel.Location = New System.Drawing.Point(1, 1)
        Me._LimitToolStripPanel.Name = "_LimitToolStripPanel"
        Me._LimitToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._LimitToolStripPanel.RowMargin = New System.Windows.Forms.Padding(0)
        Me._LimitToolStripPanel.Size = New System.Drawing.Size(440, 78)
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._FeedSourceComboLabel, Me._FeedSourceComboBox, Me._ElementGroupToggleButton, Me._BufferControlComboBoxLabel, Me._FeedControlComboBox})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(440, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 0
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DoubleClickEnabled = True
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(58, 22)
        Me._SubsystemSplitButton.Text = "Trace"
        Me._SubsystemSplitButton.ToolTipText = "Trace or buffer label"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(150, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings fromt he instrument"
        '
        '_FeedSourceComboLabel
        '
        Me._FeedSourceComboLabel.Name = "_FeedSourceComboLabel"
        Me._FeedSourceComboLabel.Size = New System.Drawing.Size(34, 22)
        Me._FeedSourceComboLabel.Text = "Data:"
        '
        '_FeedSourceComboBox
        '
        Me._FeedSourceComboBox.Name = "_FeedSourceComboBox"
        Me._FeedSourceComboBox.Size = New System.Drawing.Size(80, 25)
        Me._FeedSourceComboBox.Text = "Sense"
        Me._FeedSourceComboBox.ToolTipText = "Selects the source of buffer data"
        '
        '_ElementGroupToggleButton
        '
        Me._ElementGroupToggleButton.Checked = True
        Me._ElementGroupToggleButton.CheckOnClick = True
        Me._ElementGroupToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._ElementGroupToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ElementGroupToggleButton.Image = CType(resources.GetObject("_ElementGroupToggleButton.Image"), System.Drawing.Image)
        Me._ElementGroupToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ElementGroupToggleButton.Name = "_ElementGroupToggleButton"
        Me._ElementGroupToggleButton.Size = New System.Drawing.Size(67, 22)
        Me._ElementGroupToggleButton.Text = "Elements ?"
        Me._ElementGroupToggleButton.ToolTipText = "Toggle element group between Full and Compact"
        '
        '_BufferControlComboBoxLabel
        '
        Me._BufferControlComboBoxLabel.Name = "_BufferControlComboBoxLabel"
        Me._BufferControlComboBoxLabel.Size = New System.Drawing.Size(50, 22)
        Me._BufferControlComboBoxLabel.Text = "Control:"
        '
        '_FeedControlComboBox
        '
        Me._FeedControlComboBox.Name = "_FeedControlComboBox"
        Me._FeedControlComboBox.Size = New System.Drawing.Size(121, 25)
        Me._FeedControlComboBox.Text = "Never"
        Me._FeedControlComboBox.ToolTipText = "Selects the feed control"
        '
        '_BufferConfigurationToolStrip
        '
        Me._BufferConfigurationToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._BufferConfigurationToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._BufferConfigurationToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._BufferCondigureLabel, Me._BufferNameLabel, Me._SizeNumericLabel, Me._SizeNumeric, Me._PreTriggerCountNumericLabel, Me._PreTriggerCountNumeric, Me._ClearBufferButton, Me._FreeCountLabelLabel, Me._FreeCountLabel})
        Me._BufferConfigurationToolStrip.Location = New System.Drawing.Point(0, 25)
        Me._BufferConfigurationToolStrip.Name = "_BufferConfigurationToolStrip"
        Me._BufferConfigurationToolStrip.Size = New System.Drawing.Size(440, 28)
        Me._BufferConfigurationToolStrip.Stretch = True
        Me._BufferConfigurationToolStrip.TabIndex = 1
        '
        '_BufferCondigureLabel
        '
        Me._BufferCondigureLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BufferCondigureLabel.Margin = New System.Windows.Forms.Padding(10, 1, 0, 2)
        Me._BufferCondigureLabel.Name = "_BufferCondigureLabel"
        Me._BufferCondigureLabel.Size = New System.Drawing.Size(39, 25)
        Me._BufferCondigureLabel.Text = "Buffer"
        '
        '_BufferNameLabel
        '
        Me._BufferNameLabel.Name = "_BufferNameLabel"
        Me._BufferNameLabel.Size = New System.Drawing.Size(34, 25)
        Me._BufferNameLabel.Text = "Trace"
        Me._BufferNameLabel.ToolTipText = "Buffer name"
        '
        '_SizeNumericLabel
        '
        Me._SizeNumericLabel.Name = "_SizeNumericLabel"
        Me._SizeNumericLabel.Size = New System.Drawing.Size(30, 25)
        Me._SizeNumericLabel.Text = "Size:"
        '
        '_SizeNumeric
        '
        Me._SizeNumeric.Name = "_SizeNumeric"
        Me._SizeNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SizeNumeric.Text = "3"
        Me._SizeNumeric.ToolTipText = "Buffer size"
        Me._SizeNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_PreTriggerCountNumericLabel
        '
        Me._PreTriggerCountNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._PreTriggerCountNumericLabel.Name = "_PreTriggerCountNumericLabel"
        Me._PreTriggerCountNumericLabel.Size = New System.Drawing.Size(68, 25)
        Me._PreTriggerCountNumericLabel.Text = "Pre-Trigger:"
        '
        '_PreTriggerCountNumeric
        '
        Me._PreTriggerCountNumeric.Name = "_PreTriggerCountNumeric"
        Me._PreTriggerCountNumeric.Size = New System.Drawing.Size(41, 25)
        Me._PreTriggerCountNumeric.Text = "0"
        Me._PreTriggerCountNumeric.ToolTipText = "Number of Pre-Trigger  readings"
        Me._PreTriggerCountNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_ClearBufferButton
        '
        Me._ClearBufferButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ClearBufferButton.Image = CType(resources.GetObject("_ClearBufferButton.Image"), System.Drawing.Image)
        Me._ClearBufferButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearBufferButton.Name = "_ClearBufferButton"
        Me._ClearBufferButton.Size = New System.Drawing.Size(38, 25)
        Me._ClearBufferButton.Text = "Clear"
        Me._ClearBufferButton.ToolTipText = "Clears the buffer"
        '
        '_FreeCountLabelLabel
        '
        Me._FreeCountLabelLabel.Name = "_FreeCountLabelLabel"
        Me._FreeCountLabelLabel.Size = New System.Drawing.Size(32, 25)
        Me._FreeCountLabelLabel.Text = "Free:"
        '
        '_FreeCountLabel
        '
        Me._FreeCountLabel.Name = "_FreeCountLabel"
        Me._FreeCountLabel.Size = New System.Drawing.Size(13, 25)
        Me._FreeCountLabel.Text = "0"
        '
        '_BufferFormatToolStrip
        '
        Me._BufferFormatToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._BufferFormatToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._BufferFormatToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FormatLabel, Me._TimestampFormatToggleButton})
        Me._BufferFormatToolStrip.Location = New System.Drawing.Point(0, 53)
        Me._BufferFormatToolStrip.Name = "_BufferFormatToolStrip"
        Me._BufferFormatToolStrip.Size = New System.Drawing.Size(440, 25)
        Me._BufferFormatToolStrip.Stretch = True
        Me._BufferFormatToolStrip.TabIndex = 2
        '
        '_FormatLabel
        '
        Me._FormatLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FormatLabel.Margin = New System.Windows.Forms.Padding(6, 1, 2, 2)
        Me._FormatLabel.Name = "_FormatLabel"
        Me._FormatLabel.Size = New System.Drawing.Size(44, 22)
        Me._FormatLabel.Text = "Format"
        '
        '_TimestampFormatToggleButton
        '
        Me._TimestampFormatToggleButton.Checked = True
        Me._TimestampFormatToggleButton.CheckOnClick = True
        Me._TimestampFormatToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._TimestampFormatToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TimestampFormatToggleButton.Image = CType(resources.GetObject("_TimestampFormatToggleButton.Image"), System.Drawing.Image)
        Me._TimestampFormatToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TimestampFormatToggleButton.Name = "_TimestampFormatToggleButton"
        Me._TimestampFormatToggleButton.Size = New System.Drawing.Size(131, 22)
        Me._TimestampFormatToggleButton.Text = "Timestamp: Absolute ?"
        Me._TimestampFormatToggleButton.ToolTipText = "Toggles timestamp format style"
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InfoTextBox.Location = New System.Drawing.Point(1, 79)
        Me._InfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ReadOnly = True
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InfoTextBox.Size = New System.Drawing.Size(440, 167)
        Me._InfoTextBox.TabIndex = 0
        Me._InfoTextBox.Text = "<info>"
        '
        'TraceBufferView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._InfoTextBox)
        Me.Controls.Add(Me._LimitToolStripPanel)
        Me.Name = "TraceBufferView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(442, 247)
        Me._LimitToolStripPanel.ResumeLayout(False)
        Me._LimitToolStripPanel.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me._BufferConfigurationToolStrip.ResumeLayout(False)
        Me._BufferConfigurationToolStrip.PerformLayout()
        Me._BufferFormatToolStrip.ResumeLayout(False)
        Me._BufferFormatToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private _LimitToolStripPanel As Windows.Forms.ToolStripPanel
    Private _BufferFormatToolStrip As Windows.Forms.ToolStrip
    Private _FormatLabel As Windows.Forms.ToolStripLabel
    Private _BufferConfigurationToolStrip As Windows.Forms.ToolStrip
    Private _BufferCondigureLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SizeNumeric As Core.Controls.ToolStripNumericUpDown
    Private _PreTriggerCountNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _PreTriggerCountNumeric As Core.Controls.ToolStripNumericUpDown
    Private _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private _SizeNumericLabel As Windows.Forms.ToolStripLabel
    Private _FeedSourceComboLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _FeedSourceComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _BufferNameLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ClearBufferButton As Windows.Forms.ToolStripButton
    Private WithEvents _FreeCountLabelLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _FreeCountLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ElementGroupToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _BufferControlComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _FeedControlComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _TimestampFormatToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InfoTextBox As TextBox
End Class
