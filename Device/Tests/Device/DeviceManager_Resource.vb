'---------------------------------------------------------------------------------------------------
' file:		Tests\Device\DeviceManager_Resource.vb
'
' summary:	Device manager resource class
'---------------------------------------------------------------------------------------------------

Partial Public NotInheritable Class DeviceManager

#Region " DEVICE TALKER TEST "

    ''' <summary> Assert device talks. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="device">   The device. </param>
    Public Shared Sub AssertDeviceTalks(ByVal testInfo As TestSite, ByVal device As VisaSessionBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        device.AddListener(testInfo.TraceMessagesQueueListener)
        Dim payload As String = "Device message"
        Dim traceEventId As Integer = 1
        device.Talker.Publish(TraceEventType.Warning, traceEventId, payload)

        ' with the new talker, the device identifies the following libraries: 
        ' 0x0100 core agnostic; 0x01006 vi device and 0x01026 Keithley Meter
        ' so these test looks for the first warning
        Dim fetchNumber As Integer = 0
        Dim traceMessage As Core.TraceMessage = Nothing
        Do While testInfo.TraceMessagesQueueListener.Any
            traceMessage = testInfo.TraceMessagesQueueListener.TryDequeue()
            fetchNumber += 1
            If traceMessage.EventType <= TraceEventType.Warning Then
                ' we expect a single such message
                Exit Do
            End If
        Loop
        If traceMessage Is Nothing Then Assert.Fail($"{payload} failed to trace fetch number {fetchNumber}")
        Assert.AreEqual(traceEventId, traceMessage.Id, $"{payload} trace event id mismatch fetch #{fetchNumber} message {traceMessage.Details}")
        Assert.AreEqual(0, testInfo.TraceMessagesQueueListener.Count, $"{payload} expected no more messages after fetch #{fetchNumber} message {traceMessage.Details}")

        traceEventId = 1
        payload = "Status subsystem message"
        device.Talker.Publish(TraceEventType.Warning, traceEventId, payload)
        traceMessage = testInfo.TraceMessagesQueueListener.TryDequeue()
        If traceMessage Is Nothing Then Assert.Fail($"{payload} failed to trace")
        Assert.AreEqual(traceEventId, traceMessage.Id, $"{payload} trace event id mismatch")
    End Sub

#End Region

#Region " VISA SESSION BASE OPEN, CLOSE "

    ''' <summary> Asserts opening a session . </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="device">           The device. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub AssertOpenSession(ByVal testInfo As TestSite, ByVal device As VI.VisaSessionBase, ByVal resourceSettings As ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        Dim r As (Success As Boolean, Details As String) = device.TryOpenSession(resourceSettings.ResourceName, resourceSettings.ResourceTitle)
        Assert.IsTrue(r.Success, $"Failed to open session: { r.Details}")
        testInfo.AssertMessageQueue()
        If device.IsSessionOpen Then device.StatusSubsystemBase.TryQueryExistingDeviceErrors()
        testInfo.AssertMessageQueue()
    End Sub

    ''' <summary> Asserts closing a session. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="device">   The device. </param>
    Public Shared Sub AssertCloseSession(ByVal testInfo As TestSite, ByVal device As VI.VisaSessionBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Try
            testInfo.AssertMessageQueue()
            If device.IsSessionOpen Then device.StatusSubsystemBase.TryQueryExistingDeviceErrors()
            testInfo.AssertMessageQueue()
        Catch
            Throw
        Finally
            device.CloseSession()
        End Try
        Assert.IsFalse(device.IsDeviceOpen, $"{device.ResourceNameCaption} failed closing session")
        testInfo.AssertMessageQueue()
    End Sub

#End Region

End Class

