''' <summary>
''' Defines the contract that must be implemented by Subsystems that report status.
''' </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-15, 1.0.1841.x. </para>
''' </remarks>
Public MustInherit Class SubsystemPlusStatusBase
    Inherits SubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SubsystemPlusStatusBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(VI.StatusSubsystemBase.Validated(statusSubsystem).Session)
        Me._StatusSubsystem = statusSubsystem
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the status subsystem. </summary>
    ''' <value> The status subsystem. </value>
    Protected ReadOnly Property StatusSubsystem As StatusSubsystemBase

#End Region

#Region " CHECK AND THROW "

    ''' <summary>
    ''' Checks and throws an exception if device errors occurred. Can only be used after receiving a
    ''' full reply from the device.
    ''' </summary>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowDeviceException(ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.CheckThrowDeviceException(flushReadFirst, format, args)
    End Sub

#End Region

#Region " CHECK AND REPORT "

    ''' <summary>
    ''' Check and reports visa or device error occurred. Can only be used after receiving a full
    ''' reply from the device.
    ''' </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Specifies the report format. </param>
    ''' <param name="args">       Specifies the report arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overridable Function TraceVisaDeviceOperationOkay(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.TraceVisaDeviceOperationOkay(nodeNumber, format, args)
    End Function

    ''' <summary>
    ''' Checks and reports if a visa or device error occurred. Can only be used after receiving a
    ''' full reply from the device.
    ''' </summary>
    ''' <param name="nodeNumber">     Specifies the remote node number to validate. </param>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function TraceVisaDeviceOperationOkay(ByVal nodeNumber As Integer,
                                                  ByVal flushReadFirst As Boolean, ByVal format As String,
                                                  ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.TraceVisaDeviceOperationOkay(nodeNumber, flushReadFirst, format, args)
    End Function

    ''' <summary>
    ''' Checks and reports if a visa or device error occurred. Can only be used after receiving a
    ''' full reply from the device.
    ''' </summary>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function TraceVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal format As String,
                                                 ByVal ParamArray args() As Object) As Boolean
        Return Me.StatusSubsystem.TraceVisaDeviceOperationOkay(flushReadFirst, format, args)
    End Function

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.TraceVisaOperation(format, args)
    End Sub

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <param name="ex">     The exception. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal ex As VI.Pith.NativeException, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.TraceVisaOperation(ex, format, args)
    End Sub

    ''' <summary> Reports if a operation error occurred. Can be used with queries. </summary>
    ''' <param name="ex">     The exception. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub TraceOperation(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.TraceOperation(ex, format, args)
    End Sub

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.TraceVisaOperation(nodeNumber, format, args)
    End Sub

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <param name="ex">         The exception. </param>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal ex As VI.Pith.NativeException, ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.TraceVisaOperation(ex, nodeNumber, format, args)
    End Sub

    ''' <summary> Reports if an operation error occurred. Can be used with queries. </summary>
    ''' <param name="ex">         The exception. </param>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub TraceOperation(ByVal ex As Exception, ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.StatusSubsystem.TraceOperation(ex, nodeNumber, format, args)
    End Sub

#End Region

End Class

