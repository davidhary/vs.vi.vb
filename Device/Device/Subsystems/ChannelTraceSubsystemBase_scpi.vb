Partial Public Class ChannelTraceSubsystemBase

#Region " TRACE PARAMETER "

    ''' <summary> Define trace parameter read writes. </summary>
    Private Sub DefineTraceParameterReadWrites()
        Me._TraceParameterReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As TraceParameters In [Enum].GetValues(GetType(TraceParameters))
            Me._TraceParameterReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Trace Parameter parses. </summary>
    ''' <value> A Dictionary of Trace Parameter parses. </value>
    Public ReadOnly Property TraceParameterReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> Options for controlling the supported. </summary>
    Private _SupportedParameters As TraceParameters

    ''' <summary>
    ''' Gets or sets the supported Trace Parameter. This is a subset of the functions supported by
    ''' the instrument.
    ''' </summary>
    ''' <value> Options that control the supported. </value>
    Public Property SupportedParameters() As TraceParameters
        Get
            Return Me._SupportedParameters
        End Get
        Set(ByVal value As TraceParameters)
            If Not Me.SupportedParameters.Equals(value) Then
                Me._SupportedParameters = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Trace Parameter. </summary>
    Private _Parameter As TraceParameters?

    ''' <summary> Gets or sets the cached source Parameter. </summary>
    ''' <value>
    ''' The <see cref="TraceParameters">Trace Parameter</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property Parameter As TraceParameters?
        Get
            Return Me._Parameter
        End Get
        Protected Set(ByVal value As TraceParameters?)
            If Not Nullable.Equals(Me.Parameter, value) Then
                Me._Parameter = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trace Parameter. </summary>
    ''' <param name="value"> The  Trace Parameter. </param>
    ''' <returns> The <see cref="TraceParameters">Trace Parameter</see> or none if unknown. </returns>
    Public Function ApplyParameter(ByVal value As TraceParameters) As TraceParameters?
        Me.WriteParameter(value)
        Return Me.QueryParameter()
    End Function

    ''' <summary> Gets or sets the Trace Parameter query command. </summary>
    ''' <value> The Trace Parameter query command, e.g., :CALC&lt;c#&gt;:PAR&lt;t#&gt;:DEF? </value>
    Protected Overridable Property ParameterQueryCommand As String

    ''' <summary> Queries the Trace Parameter. </summary>
    ''' <returns> The parameter. </returns>
    Public Function QueryParameter() As TraceParameters?
        Me.Parameter = Me.Query(Of TraceParameters)(Me.ParameterQueryCommand,
                                                       Me.Parameter.GetValueOrDefault(TraceParameters.None),
                                                       Me.TraceParameterReadWrites)
        Return Me.Parameter
    End Function

    ''' <summary> Gets or sets the Trace Parameter command. </summary>
    ''' <value> The Trace Parameter command, e.g., :CALC&lt;c#&gt;:PAR&lt;t#&gt;:DEF {0}. </value>
    Protected Overridable Property ParameterCommandFormat As String

    ''' <summary> Writes the Trace Parameter without reading back the value from the device. </summary>
    ''' <param name="value"> The Trace Parameter. </param>
    ''' <returns> The <see cref="TraceParameters">Trace Parameter</see> or none if unknown. </returns>
    Public Function WriteParameter(ByVal value As TraceParameters) As TraceParameters?
        Me.Parameter = Me.Write(Of TraceParameters)(Me.ParameterCommandFormat, value, Me.TraceParameterReadWrites)
        Return Me.Parameter
    End Function

#End Region

End Class

''' <summary> A bit-field of flags for specifying trace parameters. </summary>
<Flags>
Public Enum TraceParameters

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the absolute impedance option. </summary>
    <ComponentModel.Description("Z: Absolute impedance value (Z)")>
    AbsoluteImpedance = 1

    ''' <summary> An enum constant representing the absolute admittance option. </summary>
    <ComponentModel.Description("Y: Absolute admittance (Y)")>
    AbsoluteAdmittance = 2

    ''' <summary> An enum constant representing the series resistance option. </summary>
    <ComponentModel.Description("R: Equivalent series resistance (R)")>
    SeriesResistance = 4

    ''' <summary> An enum constant representing the series reactance option. </summary>
    <ComponentModel.Description("X: Equivalent series reactance (X)")>
    SeriesReactance = 8

    ''' <summary> An enum constant representing the parallel conductance option. </summary>
    <ComponentModel.Description("G: Equivalent parallel conductance (G)")>
    ParallelConductance = &H10

    ''' <summary> An enum constant representing the parallel susceptance option. </summary>
    <ComponentModel.Description("B: Equivalent parallel susceptance (B)")>
    ParallelSusceptance = &H20

    ''' <summary> An enum constant representing the series inductance option. </summary>
    <ComponentModel.Description("LS: Equivalent series inductance (LS)")>
    SeriesInductance = &H40

    ''' <summary> An enum constant representing the parallel inductance option. </summary>
    <ComponentModel.Description("LP: Equivalent parallel inductance (LP)")>
    ParallelInductance = &H80

    ''' <summary> An enum constant representing the series capacitance option. </summary>
    <ComponentModel.Description("CS: Equivalent series capacitance (CS)")>
    SeriesCapacitance = &H100

    ''' <summary> An enum constant representing the parallel capacitance option. </summary>
    <ComponentModel.Description("CP: Equivalent parallel capacitance (CP)")>
    ParallelCapacitance = &H200

    ''' <summary> An enum constant representing the series resistance 1 option. </summary>
    <ComponentModel.Description("RS: Equivalent series resistance (RS)")>
    SeriesResistance1 = &H400

    ''' <summary> An enum constant representing the parallel resistance option. </summary>
    <ComponentModel.Description("RP: Equivalent parallel resistance (RP)")>
    ParallelResistance = &H800

    ''' <summary> An enum constant representing the quality factor option. </summary>
    <ComponentModel.Description("Q: Q value (Q)")>
    QualityFactor = &H1000

    ''' <summary> An enum constant representing the dissipation factor option. </summary>
    <ComponentModel.Description("D: Dissipation factor (D)")>
    DissipationFactor = &H2000

    ''' <summary> An enum constant representing the impedance phase option. </summary>
    <ComponentModel.Description("TZ: Impedance phase (TZ)")>
    ImpedancePhase = &H4000

    ''' <summary> An enum constant representing the absolute phase option. </summary>
    <ComponentModel.Description("TY: Absolute phase (TY)")>
    AbsolutePhase = &H8000

    ''' <summary> An enum constant representing the oscillator voltage option. </summary>
    <ComponentModel.Description("VAC: Oscillator Voltage (VAC)")>
    OscillatorVoltage = &H10000

    ''' <summary> An enum constant representing the oscillator current option. </summary>
    <ComponentModel.Description("IAC: Oscillator Current (IAC)")>
    OscillatorCurrent = &H20000

    ''' <summary> An enum constant representing the bias voltage option. </summary>
    <ComponentModel.Description("VDC: DC Bias Voltage (VDC)")>
    BiasVoltage = &H40000

    ''' <summary> An enum constant representing the bias current option. </summary>
    <ComponentModel.Description("IDC: DC Bias Current (IDC)")>
    BiasCurrent = &H80000

    ''' <summary> An enum constant representing the complex impedance option. </summary>
    <ComponentModel.Description("IMP: Complex Impedance (IMP)")>
    ComplexImpedance = &H100000

    ''' <summary> An enum constant representing the complex admittance option. </summary>
    <ComponentModel.Description("ADM: Complex Admittance (ADM)")>
    ComplexAdmittance = &H200000
End Enum
