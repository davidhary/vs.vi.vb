'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Subsystems\LinkSubsystemBase.vb
'
' summary:	Link subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.StackTraceExtensions

''' <summary> Defines a subsystem for handing TSP Link and multiple nodes. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-11-01. Based on legacy status subsystem. </para>
''' </remarks>
Public MustInherit Class LinkSubsystemBase
    Inherits VI.SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SystemSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.TspLinkResetTimeout = TimeSpan.FromMilliseconds(5000)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        ' 2016-01-07: Moved from the Master Device.
        Me.UsingTspLink = False
        If Me.IsControllerNode Then
            ' establish the current node as the controller node. 
            Me.InitiateControllerNode()
        End If
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me._NodeEntities = New NodeEntityCollection()
        Me.NotifyPropertyChanged(NameOf(LinkSubsystemBase.NodeEntities))
        Me.ControllerNodeNumber = New Integer?
        Me.ControllerNode = Nothing

        ' these values are set upon loading or initializing the framework.
        Me.TspLinkOnlineStateQueryCommand = String.Empty
        Me.TspLinkOfflineStateQueryCommand = String.Empty
        Me.TspLinkResetCommand = String.Empty
        Me.ResetNodesCommand = String.Empty

        Me.IsTspLinkOnline = New Boolean?
        Me.IsTspLinkOffline = New Boolean?

    End Sub

#End Region

#Region " ERROR QUEUE: NODE "

    ''' <summary> Queries error count on a remote node. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> The error count. </returns>
    Public Function QueryErrorQueueCount(ByVal node As NodeEntityBase) As Integer
        Dim count As Integer?
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        count = If(node.IsController,
            Me.Session.QueryPrint(0I, 1, Tsp.Syntax.EventLog.ErrorCount),
            Me.Session.QueryPrint(0I, 1, Tsp.Syntax.Node.NodeErrorCountBuilder, node.Number))
        Return count.GetValueOrDefault(0)
    End Function

    ''' <summary> Clears the error cache. </summary>
    Public Sub ClearErrorCache()
        Me.StatusSubsystem.ClearErrorCache()
        Me._DeviceErrorQueue = New Queue(Of TspDeviceError)
    End Sub

    ''' <summary> Clears the error queue for the specified node. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Public Sub ClearErrorQueue(ByVal nodeNumber As Integer)
        If Not Me.NodeExists(nodeNumber) Then
            Me.Session.WriteLine(Tsp.Syntax.EventLog.NodeClearEventLogCommand, nodeNumber)
        End If
    End Sub

    ''' <summary> Clears the error queue. </summary>
    Public Sub ClearErrorQueue()
        If Me.NodeEntities Is Nothing Then
            Me.StatusSubsystem.ClearErrorQueue()
        Else
            Me.ClearErrorCache()
            For Each node As NodeEntityBase In Me.NodeEntities
                Me.ClearErrorQueue(node.Number)
            Next
        End If
    End Sub

    ''' <summary> Queue of device errors. </summary>
    Private _DeviceErrorQueue As Queue(Of TspDeviceError)

    ''' <summary> Gets the error queue. </summary>
    ''' <value> A Queue of device errors. </value>
    Protected Shadows ReadOnly Property DeviceErrorQueue As Queue(Of TspDeviceError)
        Get
            Return Me._DeviceErrorQueue
        End Get
    End Property

    ''' <summary> Returns the queued error. </summary>
    ''' <remarks> Sends the error print format query and reads back and parses the error. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> The queued error. </returns>
    Public Shadows Function QueryQueuedError(ByVal node As NodeEntityBase) As TspDeviceError
        Dim err As New TspDeviceError()
        If Me.QueryErrorQueueCount(node) > 0 Then
            If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
            Me.Session.LastAction = Me.PublishInfo("Querying queued device errors;. ")
            Dim message As String
            If node.IsController Then
                Me.Session.LastNodeNumber = node.ControllerNodeNumber
                message = Me.Session.QueryPrintStringFormatTrimEnd($"%d,%s,%d,node{node.Number}", "_G.eventlog.next(eventlog.SEV_ERROR)")
            Else
                Me.Session.LastNodeNumber = node.Number
                message = Me.Session.QueryPrintStringFormatTrimEnd($"%d,%s,%d,node{node.Number}", $"node[{node.Number}].eventlog.next(eventlog.SEV_ERROR)")
            End If
            Me.CheckThrowDeviceException(False, "getting queued error;. using {0}.", Me.Session.LastMessageSent)
            err = New TspDeviceError()
            err.Parse(message)
        End If
        Return err
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
    Public Shadows Function QueryDeviceErrors(ByVal node As NodeEntityBase) As String
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        Dim deviceError As TspDeviceError
        Do
            deviceError = Me.QueryQueuedError(node)
            If deviceError.IsError Then
                Me.DeviceErrorQueue.Enqueue(deviceError)
            End If
        Loop While Me.StatusSubsystem.ErrorAvailable()
        Dim message As New System.Text.StringBuilder
        If Me.DeviceErrorQueue IsNot Nothing AndAlso Me.DeviceErrorQueue.Count > 0 Then
            message.AppendFormat("Instrument {0} Node {1} Errors:", Me.ResourceNameCaption, node.Number)
            message.AppendLine()
            For Each e As TspDeviceError In Me.DeviceErrorQueue
                message.AppendLine(e.ErrorMessage)
            Next
        End If
        Me.StatusSubsystem.AppendDeviceErrorMessage(message.ToString)
        Return Me.StatusSubsystem.DeviceErrorReport
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
    Public Shadows Function QueryDeviceErrors() As String
        Me.ClearErrorCache()
        For Each node As NodeEntityBase In Me.NodeEntities
            Me.QueryDeviceErrors(node)
        Next
        Return Me.StatusSubsystem.DeviceErrorReport
    End Function

#End Region

#Region " OPC "

    ''' <summary> Enables group wait complete. </summary>
    ''' <param name="groupNumber"> Specifies the group number. That would be the same as the TSP
    '''                            Link group number for the node. </param>
    Public Overloads Sub EnableWaitComplete(ByVal groupNumber As Integer)
        Me.Session.EnableServiceRequestWaitComplete()
        Me.Session.WriteLine(Tsp.Syntax.Lua.WaitGroupCommandFormat, groupNumber)
    End Sub

    ''' <summary> Waits completion after command. </summary>
    ''' <param name="nodeNumber"> Specifies the node number. </param>
    ''' <param name="timeout">    The timeout. </param>
    ''' <param name="isQuery">    Specifies the condition indicating if the command that preceded the
    '''                           wait is a query, which determines how errors are fetched. </param>
    Public Sub WaitComplete(ByVal nodeNumber As Integer, ByVal timeout As TimeSpan, ByVal isQuery As Boolean)

        Me.Session.LastAction = Me.PublishInfo("Enabling wait complete;. ")
        Me.Session.LastNodeNumber = nodeNumber
        Me.EnableWaitComplete(0)
        Me.CheckThrowDeviceException(Not isQuery, "enabled wait complete group '{0}';. ", 0)
        Me.Session.LastAction = Me.PublishInfo("waiting completion;. ")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        Me.CheckThrowDeviceException(Not isQuery, "waiting completion;. ")
        Me.Session.LastNodeNumber = New Integer?

    End Sub

#End Region

#Region " COLLECT GARBAGE "

    ''' <summary> Collect garbage wait complete. </summary>
    ''' <param name="node">    Specifies the remote node number to validate. </param>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    ''' <param name="format">  Describes the format to use. </param>
    ''' <param name="args">    A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function CollectGarbageWaitComplete(ByVal node As NodeEntityBase, ByVal timeout As TimeSpan,
                                               ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.CollectGarbageWaitCompleteThis(node, timeout, format, args)
    End Function

    ''' <summary> Does garbage collection. Reports operations synopsis. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node">    Specifies the remote node number to validate. </param>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    ''' <param name="format">  Describes the format to use. </param>
    ''' <param name="args">    A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function CollectGarbageWaitCompleteThis(ByVal node As NodeEntityBase, ByVal timeout As TimeSpan,
                                                    ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        If node.IsController Then
            Return Me.StatusSubsystem.CollectGarbageWaitComplete(timeout, format, args)
        End If

        Dim affirmative As Boolean
        ' do a garbage collection
        Try
            Me.Session.WriteLine(Tsp.Syntax.Node.CollectNodeGarbageFormat, node.Number)
            affirmative = Me.TraceVisaDeviceOperationOkay(node.Number, True, "collecting garbage after {0};. ",
                                                          String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        Catch ex As VI.Pith.NativeException
            Me.TraceVisaOperation(ex, node.Number, "collecting garbage after {0};. ",
                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            affirmative = False
        Catch ex As Exception
            Me.TraceOperation(ex, node.Number, "collecting garbage after {0};. ",
                              String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            affirmative = False
        End Try


        Try
            If affirmative Then
                Me.EnableWaitComplete(0)
                Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
                affirmative = Me.TraceVisaDeviceOperationOkay(True, "awaiting completion after collecting garbage after {0};. ",
                                                              String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        Catch ex As VI.Pith.NativeException
            Me.TraceVisaOperation(ex, "awaiting completion after collecting garbage after {0};. ",
                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            affirmative = False
        Catch ex As Exception
            Me.TraceOperation(ex, "awaiting completion after collecting garbage after {0};. ",
                              String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            affirmative = False
        End Try
        Return affirmative

    End Function

    #End Region

    #Region " DATA QUEUE "

    ''' <summary> clears the data queue for the specified node. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node">                Specifies the node. </param>
    ''' <param name="reportQueueNotEmpty"> true to report queue not empty. </param>
    Public Sub ClearDataQueue(ByVal node As NodeEntityBase, ByVal reportQueueNotEmpty As Boolean)
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        If Me.NodeExists(node.Number) Then
            If reportQueueNotEmpty Then
                If Me.QueryDataQueueCount(node) > 0 Then
                    Me.PublishInfo("Data queue not empty on node {0};. ", node.Number)
                End If
            End If
            Me.Session.WriteLine("node[{0}].dataqueue.clear() waitcomplete({0})", node.Number)
        End If
    End Sub

    ''' <summary> clears the data queue for the specified node. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Public Sub ClearDataQueue(ByVal nodeNumber As Integer)
        If Me.NodeExists(nodeNumber) Then
            Me.Session.WriteLine("node[{0}].dataqueue.clear() waitcomplete({0})", nodeNumber)
        End If
    End Sub

    ''' <summary> Clears data queue on all nodes. </summary>
    ''' <param name="nodeEntities">        The node entities. </param>
    ''' <param name="reportQueueNotEmpty"> true to report queue not empty. </param>
    Public Sub ClearDataQueue(ByVal nodeEntities As NodeEntityCollection, ByVal reportQueueNotEmpty As Boolean)
        If nodeEntities IsNot Nothing Then
            For Each node As NodeEntityBase In nodeEntities
                Me.ClearDataQueue(node, reportQueueNotEmpty)
            Next
        End If
    End Sub

    ''' <summary> Clears data queue on the nodes. </summary>
    ''' <param name="nodes"> The nodes. </param>
    Public Sub ClearDataQueue(ByVal nodes As NodeEntityCollection)
        If nodes IsNot Nothing Then
            For Each node As NodeEntityBase In nodes
                If node IsNot Nothing Then
                    Me.ClearDataQueue(node.Number)
                End If
            Next
        End If
    End Sub

    ''' <summary> Clears data queue on all nodes. </summary>
    Public Sub ClearDataQueue()
        Me.ClearDataQueue(Me.NodeEntities)
    End Sub

    #Region " CAPACITY "

    ''' <summary> Queries the capacity of the data queue. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> Capacity. </returns>
    Public Function QueryDataQueueCapacity(ByVal node As NodeEntityBase) As Integer
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        Return Me.QueryDataQueueCapacity(node.Number)
    End Function

    ''' <summary> Queries the capacity of the data queue. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    ''' <returns> Capacity. </returns>
    Public Function QueryDataQueueCapacity(ByVal nodeNumber As Integer) As Integer
        Return If(Me.NodeExists(nodeNumber), Me.Session.QueryPrint(0I, 1, $"node[{nodeNumber}].dataqueue.CAPACITY"), 0)
    End Function

    #End Region

    #Region " COUNT "

    ''' <summary> Queries the data queue count. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> Count. </returns>
    Public Function QueryDataQueueCount(ByVal node As NodeEntityBase) As Integer
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        Return Me.QueryDataQueueCount(node.Number)
    End Function

    ''' <summary> Queries the data queue count. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    ''' <returns> Count. </returns>
    Public Function QueryDataQueueCount(ByVal nodeNumber As Integer) As Integer
        If Me.NodeExists(nodeNumber) Then
            Return Me.Session.QueryPrint(0I, 1, $"node[{nodeNumber}].dataqueue.count")
        End If
    End Function

    #End Region

    #End Region

    #Region " NODE "

    ''' <summary> Resets the local TSP node. </summary>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False&gt;</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ResetNode() As Boolean
        Dim affirmative As Boolean

        Try
            Me.Session.WriteLine("localnode.reset()")
            affirmative = Me.TraceVisaDeviceOperationOkay(False, "resetting local TSP node;. ")
        Catch ex As VI.Pith.NativeException
            Me.TraceVisaOperation(ex, "resetting local TSP node;. ")
            affirmative = False
        Catch ex As Exception
            Me.TraceOperation(ex, "resetting local TSP node;. ")
            affirmative = False
        End Try
        Return affirmative
    End Function

    ''' <summary> Resets a node. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> The node. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False&gt;</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ResetNode(ByVal node As NodeEntityBase) As Boolean

        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        Dim affirmative As Boolean

        If node.IsController Then
            affirmative = Me.ResetNode()
        Else
            Try
                Me.Session.WriteLine("node[{0}].reset()", node.Number)
                affirmative = Me.TraceVisaDeviceOperationOkay(node.Number, "resetting TSP node {0};. ", node.Number)
            Catch ex As VI.Pith.NativeException
                Me.TraceVisaOperation(ex, node.Number, "resetting TSP node {0};. ", node.Number)
                affirmative = False
            Catch ex As Exception
                Me.TraceOperation(ex, node.Number, "resetting TSP node {0};. ", node.Number)
                affirmative = False
            End Try
        End If
        Return affirmative

    End Function

    ''' <summary> Sets the connect rule on the specified node. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number. </param>
    ''' <param name="value">      true to value. </param>
    Public Sub ConnectRuleSetter(ByVal nodeNumber As Integer, ByVal value As Integer)
        Me.Session.WriteLine(Tsp.Syntax.Node.ConnectRuleSetterCommandFormat, nodeNumber, value)
    End Sub

#End Region

#Region " RESET NODES "

    ''' <summary> Gets the reset nodes command. </summary>
    ''' <value> The reset nodes command. </value>
    Public Property ResetNodesCommand As String

    ''' <summary> Resets the TSP nodes. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    Public Sub ResetNodes(ByVal timeout As TimeSpan)
        If Not String.IsNullOrWhiteSpace(Me.ResetNodesCommand) Then
            Me.Session.EnableServiceRequestWaitComplete()
            Me.Session.WriteLine("isr.node.reset() waitcomplete(0)")
            Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
        End If
    End Sub

    ''' <summary> Try reset nodes. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False&gt;</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryResetNodes(ByVal timeout As TimeSpan) As Boolean
        Try
            Me.ResetNodes(timeout)
        Catch ex As Exception
            Me.PublishWarning($"{Me.Session.ResourceNameCaption} timed out resetting nodes; Ignored;. ")
            Return False
        End Try
    End Function

#End Region

#Region " CONTROLLER NODE "

    ''' <summary> Gets the controller node model. </summary>
    ''' <value> The controller node model. </value>
    Public ReadOnly Property ControllerNodeModel As String
        Get
            Return If(Me.ControllerNode Is Nothing, String.Empty, Me.ControllerNode.ModelNumber)
        End Get
    End Property

    ''' <summary> The controller node. </summary>
    Private _ControllerNode As NodeEntityBase

    ''' <summary> Gets or sets reference to the controller node. </summary>
    ''' <value> The controller node. </value>
    Public Property ControllerNode() As NodeEntityBase
        Get
            Return Me._ControllerNode
        End Get
        Set(ByVal value As NodeEntityBase)
            If value Is Nothing Then
                If Me.ControllerNode IsNot Nothing Then
                    Me._ControllerNode = value
                    Me.NotifyPropertyChanged()
                End If
            ElseIf Me.ControllerNode Is Nothing OrElse Not value.Equals(Me.ControllerNode) Then
                Me._ControllerNode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the is controller node. </summary>
    ''' <remarks> Required to allow this subsystem to initialize properly. </remarks>
    ''' <value> The is controller node. </value>
    Public Property IsControllerNode As Boolean

    ''' <summary> Initiates the controller node. </summary>
    ''' <remarks> This also clears the <see cref="NodeEntities">collection of nodes</see>. </remarks>
    Public Sub InitiateControllerNode()
        If Me.ControllerNode Is Nothing Then
            Me.QueryControllerNodeNumber()
            Me.ControllerNode = New NodeEntity(Me.ControllerNodeNumber.Value, Me.ControllerNodeNumber.Value)
            Me.ControllerNode.InitializeKnownState(Me.Session)
            Me.NotifyPropertyChanged(NameOf(Tsp2.LinkSubsystemBase.ControllerNodeModel))
            Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                               "Initiated controller node #{3};. Instrument model {0} S/N={1} Firmware={2} enumerated on node.",
                               Me.ControllerNode.ModelNumber, Me.ControllerNode.SerialNumber,
                               Me.ControllerNode.FirmwareVersion, Me.ControllerNode.Number)
        End If
        Me._NodeEntities = New NodeEntityCollection()
        Me.NodeEntities.Add(Me.ControllerNode)
    End Sub

#Region " CONTROLLER NODE NUMBER "

    ''' <summary> The controller node number. </summary>
    Private _ControllerNodeNumber As Integer?

    ''' <summary> Gets or sets the Controller (local) node number. </summary>
    ''' <value> The Controller (local) node number. </value>
    Public Property ControllerNodeNumber As Integer?
        Get
            Return Me._ControllerNodeNumber
        End Get
        Set(ByVal value As Integer?)
            If Not Nullable.Equals(value, Me.ControllerNodeNumber) Then
                Me._ControllerNodeNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Reads the Controller node number. </summary>
    ''' <returns> An Integer or Null of failed. </returns>
    Public Function QueryControllerNodeNumber() As Integer?
        Me.ControllerNodeNumber = Me.Session.QueryPrint(0I, 1, "_G.tsplink.node")
        Return Me.ControllerNodeNumber
    End Function

    ''' <summary> Reads the Controller node number. </summary>
    ''' <returns> An Integer or Null of failed. </returns>
    Public Function TryQueryControllerNodeNumber() As Integer?
        Try
            Me.QueryControllerNodeNumber()
        Catch ex As isr.Core.OperationFailedException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, ex.ToString)
        Catch ex As InvalidCastException
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId, ex.ToString)
        End Try
        Return Me.ControllerNodeNumber
    End Function

#End Region

#End Region

#Region " TSP: NODE ENTITIES "

    ''' <summary> Gets or sets the node entities. </summary>
    ''' <remarks> Required for reading the system errors. </remarks>
    Private _NodeEntities As NodeEntityCollection

    ''' <summary> Returns the enumerated list of node entities. </summary>
    ''' <returns> A list of. </returns>
    Public Function NodeEntities() As NodeEntityCollection
        Return Me._NodeEntities
    End Function

    ''' <summary> Gets the number of nodes detected in the system. </summary>
    ''' <value> The number of nodes. </value>
    Public ReadOnly Property NodeCount() As Integer
        Get
            Return If(Me._NodeEntities Is Nothing, 0, Me._NodeEntities.Count)
        End Get
    End Property

    ''' <summary> Adds a node entity. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Private Sub AddNodeEntity(ByVal nodeNumber As Integer)
        If nodeNumber = Me.ControllerNodeNumber Then
            Me._NodeEntities.Add(Me.ControllerNode)
        Else
            Dim node As NodeEntity = New NodeEntity(nodeNumber, Me.ControllerNodeNumber.Value)
            node.InitializeKnownState(Me.Session)
            Me._NodeEntities.Add(node)
            Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                               "Added node #{3};. Instrument model {0} S/N={1} Firmware={2} enumerated on node.",
                               node.ModelNumber, node.SerialNumber, node.FirmwareVersion, node.Number)
        End If
    End Sub

    ''' <summary> Queries if a given node exists. </summary>
    ''' <remarks>
    ''' Uses the <see cref="NodeEntities">node entities</see> as a cache and add to the cache is not
    ''' is not cached.
    ''' </remarks>
    ''' <param name="nodeNumber"> Specifies the node number. </param>
    ''' <returns> <c>True</c> if node exists; otherwise, false. </returns>
    Public Function NodeExists(ByVal nodeNumber As Integer) As Boolean
        Dim affirmative As Boolean = True
        If Me._NodeEntities.Count > 0 AndAlso Me._NodeEntities.Contains(NodeEntityBase.BuildKey(nodeNumber)) Then
            Return affirmative
        Else
            If NodeEntity.NodeExists(Me.Session, nodeNumber) Then
                Me.AddNodeEntity(nodeNumber)
                affirmative = Me._NodeEntities.Count > 0 AndAlso Me._NodeEntities.Contains(NodeEntityBase.BuildKey(nodeNumber))
            Else
                affirmative = False
            End If
        End If
        Return affirmative
    End Function

    ''' <summary> Enumerates the collection of nodes on the TSP Link net. </summary>
    ''' <param name="maximumCount"> Specifies the maximum expected node number. There could be up to
    '''                             64 nodes on the TSP link. Specify 0 to use the maximum node
    '''                             count. </param>
    Public Sub EnumerateNodes(ByVal maximumCount As Integer)

        Me.InitiateControllerNode()
        If maximumCount > 1 Then
            For i As Integer = 1 To maximumCount
                If Not NodeEntity.NodeExists(Me.Session, i) Then
                    Me.AddNodeEntity(i)
                End If
            Next
        End If
        Me.NotifyPropertyChanged(NameOf(Tsp2.LinkSubsystemBase.NodeCount))
    End Sub

#End Region

#Region " TSP LINK "

    ''' <summary> True to using tsp link. </summary>
    Private _UsingTspLink As Boolean

    ''' <summary>
    ''' Gets or sets the condition for using TSP Link. Must be affirmative otherwise TSP link reset
    ''' commands are ignored.
    ''' </summary>
    ''' <value> The using tsp link. </value>
    Public Property UsingTspLink() As Boolean
        Get
            Return Me._UsingTspLink
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.UsingTspLink Then
                Me._UsingTspLink = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#Region " TSP LINK ONLINE STATE "

    ''' <summary> The is tsp link online. </summary>
    Private _IsTspLinkOnline As Boolean?

    ''' <summary> gets or sets the sentinel indicating if the TSP Link System is ready. </summary>
    ''' <value>
    ''' <c>null</c> if Not known; <c>True</c> if the tsp link is on line; otherwise <c>False</c>.
    ''' </value>
    Public Property IsTspLinkOnline() As Boolean?
        Get
            Return Me._IsTspLinkOnline
        End Get
        Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.IsTspLinkOnline) Then
                Me._IsTspLinkOnline = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tsp link on-line state query command. </summary>
    ''' <value> The tsp link on line state query command. </value>
    Public Property TspLinkOnlineStateQueryCommand As String

    ''' <summary> Reads tsp link on line state. </summary>
    ''' <returns>
    ''' <c>null</c> if Not known; <c>True</c> if the tsp link is on line; otherwise
    ''' <c>False</c>.
    ''' </returns>
    Public Function ReadTspLinkOnlineState() As Boolean?
        If Not String.IsNullOrWhiteSpace(Me.TspLinkOnlineStateQueryCommand) Then
            Me.IsTspLinkOnline = Me.Session.IsStatementTrue(Me.TspLinkOnlineStateQueryCommand)
        End If
        Return Me.IsTspLinkOnline
    End Function

#End Region

#Region " TSP LINK OFFLINE STATE "

    ''' <summary> The is tsp link offline. </summary>
    Private _IsTspLinkOffline As Boolean?

    ''' <summary> gets or sets the sentinel indicating if the TSP Link System is ready. </summary>
    ''' <value>
    ''' <c>null</c> if Not known; <c>True</c> if the tsp link is Off line; otherwise <c>False</c>.
    ''' </value>
    Public Property IsTspLinkOffline() As Boolean?
        Get
            Return Me._IsTspLinkOffline
        End Get
        Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.IsTspLinkOffline) Then
                Me._IsTspLinkOffline = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tsp link Off line state query command. </summary>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <value> The tsp link Off line state query command. </value>
    Public Property TspLinkOfflineStateQueryCommand As String

    ''' <summary> Reads tsp link Off line state. </summary>
    ''' <returns>
    ''' <c>null</c> if Not known; <c>True</c> if the tsp link is Off line; otherwise
    ''' <c>False</c>.
    ''' </returns>
    Public Function ReadTspLinkOfflineState() As Boolean?
        If Not String.IsNullOrWhiteSpace(Me.TspLinkOfflineStateQueryCommand) Then
            Me.IsTspLinkOnline = Me.Session.IsStatementTrue(Me.TspLinkOfflineStateQueryCommand)
        End If
        Return Me.IsTspLinkOffline
    End Function

#End Region

#Region " RESET "

    ''' <summary> Gets or sets the tsp link reset command. </summary>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <value> The tsp link reset command. </value>
    Public Property TspLinkResetCommand As String

    ''' <summary> Resets the TSP link and the ISR support framework. </summary>
    ''' <remarks> Requires loading the 'isr.tsplink' scripts. </remarks>
    ''' <param name="timeout">          The timeout. </param>
    ''' <param name="maximumNodeCount"> Number of maximum nodes. </param>
    Public Sub ResetTspLinkWaitComplete(ByVal timeout As TimeSpan, ByVal maximumNodeCount As Integer)

        Try

            Me.Session.StoreCommunicationTimeout(timeout)

            If Not String.IsNullOrWhiteSpace(Me.TspLinkResetCommand) Then
                Me.Session.LastAction = Me.PublishInfo("resetting TSP Link;. ")
                Me.Session.LastNodeNumber = New Integer?
                ' do not condition the reset upon a previous reset.
                Me.Session.EnableServiceRequestWaitComplete()
                Me.Session.WriteLine(Me.TspLinkResetCommand)
                Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
                Me.CheckThrowDeviceException(False, "resetting TSP Link")
            End If

            ' clear the reset status
            Me.IsTspLinkOffline = New Boolean?
            Me.IsTspLinkOnline = New Boolean?
            Me.ReadTspLinkOnlineState()
            Me.EnumerateNodes(maximumNodeCount)

        Catch

            Throw

        Finally

            Me.Session.RestoreCommunicationTimeout()

        End Try

    End Sub

    ''' <summary> Reset the TSP Link or just the first node if TSP link not defined. </summary>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="timeout">          The timeout. </param>
    ''' <param name="maximumNodeCount"> Number of maximum nodes. </param>
    ''' <param name="displaySubsystem"> The display subsystem. </param>
    ''' <param name="frameworkName">    Name of the framework. </param>
    Public Sub ResetTspLink(ByVal timeout As TimeSpan, ByVal maximumNodeCount As Integer,
                            ByVal displaySubsystem As DisplaySubsystemBase, ByVal frameworkName As String)
        If displaySubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(displaySubsystem))
        displaySubsystem.DisplayLine(1, "Resetting  {0}", frameworkName)
        displaySubsystem.DisplayLine(2, "Resetting TSP Link")
        Me.ResetTspLinkWaitComplete(timeout, maximumNodeCount)
        If Me.NodeCount <= 0 Then
            If Me.UsingTspLink Then
                Throw New isr.Core.OperationFailedException($"{Me.Session.ResourceNameCaption} failed resetting TSP Link--no nodes;. ")
            Else
                Throw New isr.Core.OperationFailedException($"{Me.Session.ResourceNameCaption} failed setting master node;. ")
            End If
        ElseIf Me.UsingTspLink AndAlso Not Me.IsTspLinkOnline Then
            Throw New isr.Core.OperationFailedException($"{Me.Session.ResourceNameCaption} failed resetting TSP Link;. TSP Link is not on line.")
        End If
    End Sub

#End Region

#Region " TSP LINK STATE "

    ''' <summary> State of the online. </summary>
    Private Const _OnlineState As String = "online"

    ''' <summary> State of the tsp link. </summary>
    Private _TspLinkState As String

    ''' <summary> Gets or sets the state of the tsp link. </summary>
    ''' <value> The tsp state. </value>
    Public Property TspLinkState As String
        Get
            Return Me._TspLinkState
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.TspLinkState) Then
                Me._TspLinkState = value
                Me.NotifyPropertyChanged()
                Me.IsTspLinkOnline = Me.TspLinkState.Equals(LinkSubsystemBase._OnlineState, StringComparison.OrdinalIgnoreCase)
                Me.IsTspLinkOffline = Not Me.IsTspLinkOnline.Value
            End If
        End Set
    End Property

    ''' <summary> Reads tsp link state. </summary>
    ''' <returns> The tsp link state. </returns>
    Public Function QueryTspLinkState() As String
        Me.Session.LastAction = Me.PublishInfo("Reading TSP Link state;. ")
        Me.Session.LastNodeNumber = New Integer?
        Me.TspLinkState = Me.Session.QueryPrintStringFormatTrimEnd("tsplink.state")
        Me.CheckThrowDeviceException(False, "getting tsp link state;. using {0}.", Me.Session.LastMessageSent)
        Return Me.TspLinkState
    End Function

#End Region

#Region " TSP LINK GROUP NUMBERS "

    ''' <summary>
    ''' Assigns group numbers to the nodes. A unique group number is required for executing
    ''' concurrent code on all nodes.
    ''' </summary>
    ''' <remarks>
    ''' David, 2009-09-08, 3.0.3538. Allows setting groups even if TSP Link is not on line.
    ''' </remarks>
    ''' <returns> <c>True</c> of okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function AssignNodeGroupNumbers() As Boolean
        Dim affirmative As Boolean = True
        If Me.NodeEntities IsNot Nothing Then
            For Each node As NodeEntityBase In Me._NodeEntities
                Try
                    If Me.IsTspLinkOnline Then
                        Me.Session.WriteLine("node[{0}].tsplink.group = {0}", node.Number)
                    Else
                        Me.Session.WriteLine("localnode.tsplink.group = {0}", node.Number)
                    End If
                    affirmative = Me.TraceVisaDeviceOperationOkay(False, "assigning group to node number {0};. ", node.Number)
                Catch ex As VI.Pith.NativeException
                    Me.TraceVisaOperation(ex, "assigning group to node number {0};. ", node.Number)
                    affirmative = False
                Catch ex As Exception
                    Me.TraceOperation(ex, "assigning group to node number {0};. ", node.Number)
                    affirmative = False
                End Try
                If Not affirmative Then Exit For
            Next
            'If Me.IsTspLinkOnline Then
            'End If
        End If
        Return affirmative

    End Function

#End Region

#Region " TSP LINK RESET "

    ''' <summary> Gets or sets the tsp link reset timeout. </summary>
    ''' <value> The tsp link reset timeout. </value>
    Public Property TspLinkResetTimeout As TimeSpan

    ''' <summary> Reset TSP Link with error reporting. </summary>
    ''' <remarks>
    ''' David, 2009-09-22, 3.0.3552.x"> The procedure caused error 1220 - TSP link failure on the
    ''' remote instrument. The error occurred only if the program was stopped and restarted without
    ''' toggling power on the instruments. Waiting completion of the previous task helped even though
    ''' that task did not access the remote node!
    ''' </remarks>
    Private Sub ResetTspLinkIgnoreError()
        Me.IsTspLinkOnline = New Boolean?
        Me.EnableWaitComplete(0)
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(Me.TspLinkResetTimeout).Status)
        Me.Session.EnableWaitComplete()
        Me.Session.WriteLine("tsplink.reset() waitcomplete(0) errorqueue.clear() waitcomplete()")
        Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(Me.TspLinkResetTimeout).Status)
    End Sub

    ''' <summary> Reset TSP Link with error reporting. </summary>
    ''' <returns> <c>True</c> of okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function TryResetTspLinkReportError() As Boolean
        Me.IsTspLinkOnline = New Boolean?
        Dim affirmative As Boolean
        Try
            Me.Session.EnableServiceRequestWaitComplete()
            Me.Session.WriteLine("tsplink.reset() waitcomplete(0)")
            affirmative = Me.TraceVisaDeviceOperationOkay(False, "resetting TSP Link;. ")
        Catch ex As VI.Pith.NativeException
            Me.TraceVisaOperation(ex, "resetting TSP Link;. ")
            affirmative = False
        Catch ex As Exception
            Me.TraceOperation(ex, "resetting TSP Link;. ")
            affirmative = False
        End Try
        If affirmative Then
            Try
                Dim r As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) = Me.Session.AwaitStatusBitmask(VI.Pith.ServiceRequests.RequestingService,
                                                                                                                  TimeSpan.FromMilliseconds(1000), TimeSpan.Zero,
                                                                                                                  TimeSpan.FromMilliseconds(10))
                Me.Session.ApplyServiceRequest(r.Status)
                affirmative = Me.TraceVisaDeviceOperationOkay(False, "resetting TSP Link;. ")
            Catch ex As VI.Pith.NativeException
                Me.TraceVisaOperation(ex, "awaiting completion after resetting TSP Link;. ")
                affirmative = False
            Catch ex As Exception
                Me.TraceOperation(ex, "awaiting completion after resetting TSP Link;. ")
                affirmative = False
            End Try
        Else
            Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                              "Instrument '{0}' failed resetting TSP Link;. {1}{2}",
                              Me.ResourceNameCaption, Environment.NewLine, New StackFrame(True).UserCallStack())
        End If
        Return affirmative

    End Function

    ''' <summary> Resets the TSP link if not on line. </summary>
    ''' <remarks>
    ''' David, 2009-09-08, 3.0.3538.x"> Allows to complete TSP Link reset even on failure in case we
    ''' have a single node.
    ''' </remarks>
    ''' <param name="maximumNodeCount"> Number of maximum nodes. </param>
    Public Sub ResetTspLink(ByVal maximumNodeCount As Integer)

        If Me.UsingTspLink Then
            Me.ResetTspLinkIgnoreError()
        End If

        If Me.IsTspLinkOnline Then
            ' enumerate all nodes.
            Me.EnumerateNodes(maximumNodeCount)
        Else
            ' enumerate the controller node.
            Me.EnumerateNodes(1)
        End If

        ' assign node group numbers.
        Me.AssignNodeGroupNumbers()

        ' clear the error queue on all nodes.
        Me.ClearErrorQueue()

        ' clear data queues.
        Me.ClearDataQueue(Me.NodeEntities)

    End Sub

#End Region

#End Region

#Region " CHECK AND REPORT "

    ''' <summary>
    ''' Check and reports visa or device error occurred. Can only be used after receiving a full
    ''' reply from the instrument.
    ''' </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Specifies the report format. </param>
    ''' <param name="args">       Specifies the report arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overrides Function TraceVisaDeviceOperationOkay(ByVal nodeNumber As Integer, ByVal format As String,
                                                           ByVal ParamArray args() As Object) As Boolean
        Dim success As Boolean = Me.TraceVisaDeviceOperationOkay(nodeNumber, False, format, args)
        If success AndAlso (nodeNumber <> Me.ControllerNode.ControllerNodeNumber) Then
            success = Me.QueryErrorQueueCount(Me.ControllerNode) = 0
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            If success Then
                Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                   "Instrument {0} node {1} done {2}", Me.ResourceNameCaption, nodeNumber, details)
            Else
                Me.Talker.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                   "Instrument {0} node {1} encountered errors {2}Details: {3}{4}{5}",
                                   Me.ResourceNameCaption, nodeNumber, Me.StatusSubsystem.DeviceErrorReport, details,
                                   Environment.NewLine, New StackFrame(True).UserCallStack())
            End If
        End If
        Return success
    End Function

#End Region

End Class

