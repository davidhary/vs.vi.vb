'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase_OPC.vb
'
' summary:	Session base OPC class
'---------------------------------------------------------------------------------------------------
Partial Public Class SessionBase

    ''' <summary> Gets or sets the wait command. </summary>
    ''' <value> The wait command. </value>
    Public Property WaitCommand As String = VI.Pith.Ieee488.Syntax.WaitCommand

    ''' <summary> Issues the wait command. </summary>
    Public Sub Wait()
        Me.SyncWriteLine(Me.WaitCommand)
    End Sub

    ''' <summary> The operation completed. </summary>
    Private _OperationCompleted As Boolean?


    ''' <summary>
    ''' Gets or sets the cached value indicating whether the last operation completed.
    ''' </summary>
    ''' <value>
    ''' <c>null</c> if operation completed contains no value, <c>True</c> if operation completed;
    ''' otherwise, <c>False</c>.
    ''' </value>
    Public Property OperationCompleted As Boolean?
        Get
            Return Me._OperationCompleted
        End Get
        Set(ByVal value As Boolean?)
            If Not Nullable.Equals(value, Me.OperationCompleted) Then
                Me._OperationCompleted = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the operation completed query command. </summary>
    ''' <remarks>
    ''' SCPI: "*OPC?".
    ''' <see cref="VI.Pith.Ieee488.Syntax.OperationCompletedQueryCommand"> </see>
    ''' </remarks>
    ''' <value> The operation completed query command. </value>
    Public Property OperationCompletedQueryCommand As String = VI.Pith.Ieee488.Syntax.OperationCompletedQueryCommand

    ''' <summary> Queries operation completed. </summary>
    ''' <param name="timeout"> Specifies how long to wait for the service request before throwing
    '''                        the timeout exception. Set to zero for an infinite (120 seconds)
    '''                        timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function QueryOperationCompleted(ByVal timeout As TimeSpan) As Boolean?
        Me.OperationCompleted = Me.Execute(AddressOf Me.QueryOperationCompleted, timeout)
    End Function

    ''' <summary> Issues the operation completion query, waits and returns a reply. </summary>
    ''' <remarks> Sends the '*OPC?' query. </remarks>
    ''' <returns> <c>True</c> if operation is complete; <c>False</c> otherwise. </returns>
    Public Function QueryOperationCompleted() As Boolean?
        Me.MakeEmulatedReplyIfEmpty(True)
        Me.OperationCompleted = If(Not String.IsNullOrWhiteSpace(Me.OperationCompletedQueryCommand), Me.Query(True, Me.OperationCompletedQueryCommand), True)
        Return Me.OperationCompleted
    End Function

    ''' <summary> The standard event wait complete enabled bitmask. </summary>
    Private _StandardEventWaitCompleteEnabledBitmask As VI.Pith.StandardEvents

    ''' <summary> Gets or sets the standard event wait complete bitmask. </summary>
    ''' <remarks> 3475. Add Or VI.Pith.Ieee4882.ServiceRequests.OperationEvent. </remarks>
    ''' <value> The standard event wait complete bitmask. </value>
    Public Property StandardEventWaitCompleteEnabledBitmask As VI.Pith.StandardEvents
        Get
            Return Me._StandardEventWaitCompleteEnabledBitmask
        End Get
        Set(value As VI.Pith.StandardEvents)
            If value <> Me.StandardEventWaitCompleteEnabledBitmask Then
                Me._StandardEventWaitCompleteEnabledBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The service request wait complete enabled bitmask. </summary>
    Private _ServiceRequestWaitCompleteEnabledBitmask As VI.Pith.ServiceRequests

    ''' <summary> Gets or sets the service request wait complete bitmask. </summary>
    ''' <value> The service request wait complete bitmask. </value>
    Public Property ServiceRequestWaitCompleteEnabledBitmask As VI.Pith.ServiceRequests
        Get
            Return Me._ServiceRequestWaitCompleteEnabledBitmask
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.ServiceRequestWaitCompleteEnabledBitmask Then
                Me._ServiceRequestWaitCompleteEnabledBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The operation complete expected bitmask. </summary>
    Private _OperationCompleteExpectedBitmask As VI.Pith.ServiceRequests

    ''' <summary> Gets or sets the expected service request operation complete bitmask. </summary>
    ''' <value> The expected service request operation complete bitmask. </value>
    Public Property OperationCompleteExpectedBitmask As VI.Pith.ServiceRequests
        Get
            Return Me._OperationCompleteExpectedBitmask
        End Get
        Set(value As VI.Pith.ServiceRequests)
            If value <> Me.OperationCompleteExpectedBitmask Then
                Me._OperationCompleteExpectedBitmask = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the operation completed command. </summary>
    ''' <remarks>
    ''' SCPI: "*OPC".
    ''' <see cref="VI.Pith.Ieee488.Syntax.OperationCompleteCommand"> </see>
    ''' </remarks>
    ''' <value> The operation complete query command. </value>
    Public Property OperationCompleteCommand As String = VI.Pith.Ieee488.Syntax.OperationCompleteCommand

    ''' <summary> Gets the supports operation complete. </summary>
    ''' <value> The supports operation complete. </value>
    Public ReadOnly Property SupportsOperationComplete As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.OperationCompleteCommand)
        End Get
    End Property

    ''' <summary> Issue operation complete. </summary>
    ''' <remarks>
    ''' When the *OPC command is sent, the instrument exits from the Operation Complete Command Idle
    ''' State (OCIS) And enters the Operation Complete Command Active State (OCAS). In OCAS, the
    ''' instrument continuously monitors the No-Operation-Pending flag. After the last pending
    ''' overlapped command Is complete (NoOperation-Pending flag set to true), the Operation Complete
    ''' (OPC) bit in the Standard Event Status Register sets, And the instrument goes back into OCIS.
    ''' </remarks>
    Public Sub IssueOperationComplete()
        If Me.SupportsOperationComplete Then Me.Execute(Me.OperationCompleteCommand)
    End Sub


    ''' <summary>
    ''' Enables service requests upon detection of completion. <para>
    ''' Writes the standard and service bitmasks to unmask events to let the device set the Event
    ''' Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
    ''' (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
    ''' the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
    ''' bit for requesting service. Also issues *OPC.</para><para>
    ''' This sets the default bitmasks.</para>
    ''' </summary>
    Public Sub EnableServiceRequestWaitComplete()
        Me.EnableServiceRequestWaitComplete(Me.StandardEventWaitCompleteEnabledBitmask, Me.ServiceRequestWaitCompleteEnabledBitmask)
    End Sub


    ''' <summary>
    ''' Enables service requests upon detection of completion. <para>
    ''' Writes the standard and service bitmasks to unmask events to let the device set the Event
    ''' Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
    ''' (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
    ''' the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
    ''' bit for requesting service. Also issues *OPC.</para><para>
    ''' This sets the default bitmasks.</para>
    ''' </summary>
    ''' <param name="standardEventEnableBitmask">  The standard event enable bitmask. </param>
    ''' <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
    Public Sub EnableServiceRequestWaitComplete(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.WriteStandardServiceCompleteEnableBitmasks(standardEventEnableBitmask, serviceRequestEnableBitmask)
    End Sub


    ''' <summary>
    ''' Enables service requests upon detection of completion. <para>
    ''' Writes the standard bitmask to unmask events to let the device set the Event Summary bit
    ''' (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/>. Uses *ESE to select (unmask) the
    ''' events that set the ESB. Also issues *OPC.</para><para>
    ''' This sets the default bitmask.</para>
    ''' </summary>
    Public Sub EnableWaitComplete()
        Me.EnabletWaitComplete(Me.StandardEventWaitCompleteEnabledBitmask)
    End Sub


    ''' <summary>
    ''' Enables service requests upon detection of completion. <para>
    ''' Writes the standard bitmask to unmask events to let the device set the Event Summary bit
    ''' (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/>. Uses *ESE to select (unmask) the
    ''' events that set the ESB. Also issues *OPC.</para><para>
    ''' This sets the default bitmask.</para>
    ''' </summary>
    ''' <param name="standardEventEnableBitmask"> The standard event enable bitmask. </param>
    Public Sub EnabletWaitComplete(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents)
        Me.WriteOperationCompleteEnableBitmask(standardEventEnableBitmask)
    End Sub

    ''' <summary> Waits for completion of last operation. </summary>
    ''' <remarks> Assumes requesting service event is registered with the instrument. </remarks>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    ''' <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
    Public Function AwaitOperationCompleted(ByVal timeout As TimeSpan) As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests)
        Return Me.AwaitOperationCompleted(Me.OperationCompleteExpectedBitmask, timeout)
    End Function

    ''' <summary> Waits for completion of last operation. </summary>
    ''' <param name="value">   The value. </param>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    ''' <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
    Public Function AwaitOperationCompleted(ByVal value As VI.Pith.ServiceRequests, ByVal timeout As TimeSpan) As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests)
        Return Me.AwaitStatusBitmask(value, timeout, Me.StatusReadDelay, Me.EffectiveStatusReadTurnaroundTime)
    End Function

    ''' <summary> Define service request wait complete bitmasks. </summary>
    Public Sub DefineServiceRequestWaitCompleteBitmasks()
        Me.DefineServiceRequestWaitCompleteBitmasks(Me.DefaultStandardEventEnableBitmask, Me.DefaultOperationCompleteBitmask)
    End Sub

    ''' <summary> Define service request wait complete bitmasks. </summary>
    ''' <param name="standardEventEnableBitmask">  The standard event enable bitmask. </param>
    ''' <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
    Public Sub DefineServiceRequestWaitCompleteBitmasks(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.ServiceRequestWaitCompleteEnabledBitmask = serviceRequestEnableBitmask
        Me.StandardEventWaitCompleteEnabledBitmask = standardEventEnableBitmask
    End Sub

End Class
