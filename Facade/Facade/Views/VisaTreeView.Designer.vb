<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class VisaTreeView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._SessionView = New isr.VI.Facade.SessionView()
        Me._StatusView = New isr.VI.Facade.StatusView()
        Me._SelectorOpener = New isr.Core.Controls.SelectorOpener()
        Me._TraceMessagesBox = New isr.Core.Forma.TraceMessagesBox()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusPromptLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._TreePanel = New isr.Core.Controls.TreePanel()
        Me._SessionPanel = New System.Windows.Forms.Panel()
        Me._DisplayView = New isr.VI.Facade.DisplayView()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._StatusStrip.SuspendLayout()
        Me._Panel.SuspendLayout()
        CType(Me._TreePanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._TreePanel.SuspendLayout()
        Me._SessionPanel.SuspendLayout()
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SessionView
        '
        Me._SessionView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SessionView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SessionView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SessionView.Location = New System.Drawing.Point(0, 3)
        Me._SessionView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SessionView.Name = "_SessionView"
        Me._SessionView.Padding = New System.Windows.Forms.Padding(1)
        Me._SessionView.Size = New System.Drawing.Size(217, 117)
        Me._SessionView.TabIndex = 25
        '
        '_StatusView
        '
        Me._StatusView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._StatusView.BackColor = System.Drawing.Color.Transparent
        Me._StatusView.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._StatusView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatusView.Location = New System.Drawing.Point(0, 149)
        Me._StatusView.Margin = New System.Windows.Forms.Padding(0)
        Me._StatusView.Name = "_StatusView"
        Me._StatusView.Size = New System.Drawing.Size(217, 31)
        Me._StatusView.TabIndex = 27
        Me._StatusView.UsingStatusSubsystemOnly = False
        '
        '_SelectorOpener
        '
        Me._SelectorOpener.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SelectorOpener.BackColor = System.Drawing.Color.Transparent
        Me._SelectorOpener.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._SelectorOpener.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SelectorOpener.Location = New System.Drawing.Point(0, 120)
        Me._SelectorOpener.Margin = New System.Windows.Forms.Padding(0)
        Me._SelectorOpener.Name = "_SelectorOpener"
        Me._SelectorOpener.Size = New System.Drawing.Size(217, 29)
        Me._SelectorOpener.TabIndex = 26
        '
        '_TraceMessagesBox
        '
        Me._TraceMessagesBox.AlertLevel = System.Diagnostics.TraceEventType.Warning
        Me._TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info
        Me._TraceMessagesBox.CaptionFormat = "{0} ≡"
        Me._TraceMessagesBox.CausesValidation = False
        Me._TraceMessagesBox.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TraceMessagesBox.Location = New System.Drawing.Point(315, 365)
        Me._TraceMessagesBox.Multiline = True
        Me._TraceMessagesBox.Name = "_TraceMessagesBox"
        Me._TraceMessagesBox.PresetCount = 500
        Me._TraceMessagesBox.ReadOnly = True
        Me._TraceMessagesBox.ResetCount = 1000
        Me._TraceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._TraceMessagesBox.Size = New System.Drawing.Size(134, 49)
        Me._TraceMessagesBox.StatusPrompt = Nothing
        Me._TraceMessagesBox.TabIndex = 1
        Me._TraceMessagesBox.TraceLevel = System.Diagnostics.TraceEventType.Verbose
        '
        '_StatusStrip
        '
        Me._StatusStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._StatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusPromptLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 426)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me._StatusStrip.ShowItemToolTips = True
        Me._StatusStrip.Size = New System.Drawing.Size(495, 22)
        Me._StatusStrip.SizingGrip = False
        Me._StatusStrip.TabIndex = 15
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_StatusPromptLabel
        '
        Me._StatusPromptLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StatusPromptLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._StatusPromptLabel.Name = "_StatusPromptLabel"
        Me._StatusPromptLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._StatusPromptLabel.Size = New System.Drawing.Size(478, 17)
        Me._StatusPromptLabel.Spring = True
        Me._StatusPromptLabel.Text = "<Status>"
        Me._StatusPromptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._StatusPromptLabel.ToolTipText = "Status prompt"
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._TreePanel)
        Me._Panel.Controls.Add(Me._TraceMessagesBox)
        Me._Panel.Controls.Add(Me._SessionPanel)
        Me._Panel.Controls.Add(Me._DisplayView)
        Me._Panel.Controls.Add(Me._StatusStrip)
        Me._Panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Panel.Location = New System.Drawing.Point(0, 0)
        Me._Panel.Margin = New System.Windows.Forms.Padding(0)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(495, 448)
        Me._Panel.TabIndex = 16
        '
        '_TreePanel
        '
        Me._TreePanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TreePanel.Location = New System.Drawing.Point(27, 174)
        Me._TreePanel.Name = "_TreePanel"
        Me._TreePanel.Size = New System.Drawing.Size(78, 84)
        Me._TreePanel.SplitterDistance = 25
        Me._TreePanel.TabIndex = 18
        '
        '_SessionPanel
        '
        Me._SessionPanel.Controls.Add(Me._SessionView)
        Me._SessionPanel.Controls.Add(Me._SelectorOpener)
        Me._SessionPanel.Controls.Add(Me._StatusView)
        Me._SessionPanel.Location = New System.Drawing.Point(123, 168)
        Me._SessionPanel.Name = "_SessionPanel"
        Me._SessionPanel.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me._SessionPanel.Size = New System.Drawing.Size(217, 180)
        Me._SessionPanel.TabIndex = 17
        '
        '_DisplayView
        '
        Me._DisplayView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DisplayView.Dock = System.Windows.Forms.DockStyle.Top
        Me._DisplayView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DisplayView.Location = New System.Drawing.Point(0, 0)
        Me._DisplayView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DisplayView.Name = "_DisplayView"
        Me._DisplayView.Padding = New System.Windows.Forms.Padding(1)
        Me._DisplayView.Size = New System.Drawing.Size(495, 152)
        Me._DisplayView.TabIndex = 16
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 1
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.Controls.Add(Me._Panel, 0, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Margin = New System.Windows.Forms.Padding(0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 2
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Size = New System.Drawing.Size(495, 448)
        Me._Layout.TabIndex = 17
        '
        'VisaTreeView
        '
        Me.Controls.Add(Me._Layout)
        Me.Name = "VisaTreeView"
        Me.Size = New System.Drawing.Size(495, 448)
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        CType(Me._TreePanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me._TreePanel.ResumeLayout(False)
        Me._SessionPanel.ResumeLayout(False)
        Me._Layout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Panel As System.Windows.Forms.Panel
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _TraceMessagesBox As isr.Core.Forma.TraceMessagesBox
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _StatusPromptLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _SessionView As Facade.SessionView
    Private WithEvents _StatusView As StatusView
    Private WithEvents _SelectorOpener As Core.Controls.SelectorOpener
    Private WithEvents _DisplayView As DisplayView
    Private WithEvents _SessionPanel As Windows.Forms.Panel
    Private WithEvents _TreePanel As Core.Controls.TreePanel
End Class
