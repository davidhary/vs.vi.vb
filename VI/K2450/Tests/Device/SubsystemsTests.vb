﻿''' <summary> K2450 Subsystems unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2450")>
Public Class SubsystemsTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K2450Tests.ResourceSettings.Get.Exists, $"{GetType(K2450Tests.ResourceSettings)} settings not found")
        Assert.IsTrue(K2450Tests.SubsystemsSettings.Get.Exists, $"{GetType(K2450Tests.SubsystemsSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " STATUS SUSBSYSTEM "

    ''' <summary> Opens session check status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
    ''' <param name="resourceInfo">     Information describing the resource. </param>
    ''' <param name="subsystemsInfo">   Information describing the subsystems. </param>
    Private Shared Sub OpenSessionCheckStatus(ByVal readErrorEnabled As Boolean, ByVal resourceInfo As ResourceSettings, ByVal subsystemsInfo As SubsystemsSettings)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp2.K2450.K2450Device = VI.Tsp2.K2450.K2450Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValues(device.Session, resourceInfo, subsystemsInfo)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceModel(device.StatusSubsystemBase, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceErrors(device.StatusSubsystemBase, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertTerminationValues(device.Session, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertLineFrequency(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertIntegrationPeriod(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.MeasureSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertInitialSubsystemValues(device.SourceSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertClearSessionDeviceErrors(device, subsystemsInfo)
                If readErrorEnabled Then isr.VI.DeviceTests.DeviceManager.AssertReadingDeviceErrors(device, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOrphanMessages(device.StatusSubsystemBase)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> A test for Open Session and status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionCheckStatusTest()
        SubsystemsTests.OpenSessionCheckStatus(False, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) tests open session read device errors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionReadDeviceErrorsTest()
        SubsystemsTests.OpenSessionCheckStatus(True, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

#End Region

#Region " MEASURE SUBSYSTEM TEST "

    ''' <summary> Check measure subsystem information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub ReadMeasureSubsystemInfo(ByVal device As VI.Tsp2.K2450.K2450Device)

        Dim expectedPowerLineCycles As Double = SubsystemsSettings.Get.InitialPowerLineCycles
        Dim actualPowerLineCycles As Double = device.MeasureSubsystem.QueryPowerLineCycles.GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, SubsystemsSettings.Get.LineFrequency / TimeSpan.TicksPerSecond,
                        $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.PowerLineCycles)} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}")

        Dim expectedBoolean As Boolean = SubsystemsSettings.Get.InitialAutoRangeEnabled
        Dim actualBoolean As Boolean = device.MeasureSubsystem.QueryAutoRangeEnabled.GetValueOrDefault(False)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.AutoRangeEnabled)} is {actualBoolean }; expected {True}")

        expectedBoolean = SubsystemsSettings.Get.InitialAutoZeroEnabled
        actualBoolean = device.MeasureSubsystem.QueryAutoZeroEnabled.GetValueOrDefault(False)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.AutoZeroEnabled)} is {actualBoolean }; expected {True}")

        expectedBoolean = SubsystemsSettings.Get.InitialFrontTerminalsSelected
        actualBoolean = device.MeasureSubsystem.QueryFrontTerminalsSelected.GetValueOrDefault(False)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.FrontTerminalsSelected)} is {actualBoolean }; expected {expectedBoolean }")

        expectedBoolean = SubsystemsSettings.Get.InitialRemoteSenseSelected
        actualBoolean = device.MeasureSubsystem.QueryRemoteSenseSelected.GetValueOrDefault(False)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.RemoteSenseSelected)} is {actualBoolean }; expected {expectedBoolean }")

        Dim senseFn As VI.SenseFunctionModes = device.MeasureSubsystem.QueryFunctionMode.GetValueOrDefault(VI.SenseFunctionModes.Resistance)
        Dim expectedFunctionMode As VI.SenseFunctionModes = SubsystemsSettings.Get.InitialSenseFunction
        Assert.AreEqual(expectedFunctionMode, senseFn, $"{GetType(VI.Tsp2.MeasureSubsystemBase)}.{NameOf(VI.Tsp2.MeasureSubsystemBase.FunctionMode)} is {senseFn} ; expected {expectedFunctionMode}")

    End Sub

    ''' <summary> (Unit Test Method) tests read measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ReadMeasureSubsystemTest()
        Using device As VI.Tsp2.K2450.K2450Device = VI.Tsp2.K2450.K2450Device.Create
            Try
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.ReadMeasureSubsystemInfo(device)
            Catch
                Throw
            Finally
                K2450Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " SOURCE SUBSYSTEM TEST "

    ''' <summary> Check Source subsystem information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub ReadSourceSubsystemInfo(ByVal device As VI.Tsp2.K2450.K2450Device)

        Dim expectedBoolean As Boolean = SubsystemsSettings.Get.InitialAutoRangeEnabled
        Dim actualBoolean As Boolean = device.SourceSubsystem.QueryAutoRangeEnabled.GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.AutoRangeEnabled)} is {actualBoolean }; expected {True}")

        expectedBoolean = SubsystemsSettings.Get.InitialAutoDelayEnabled
        actualBoolean = device.SourceSubsystem.QueryAutoDelayEnabled.GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.AutoDelayEnabled)} is {actualBoolean }; expected {True}")

        expectedBoolean = False
        actualBoolean = device.SourceSubsystem.QueryOutputEnabled.GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.OutputEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        Dim functionMode As VI.SourceFunctionModes = device.SourceSubsystem.QueryFunctionMode.GetValueOrDefault(VI.SourceFunctionModes.None)
        Dim expectedFunctionMode As VI.SourceFunctionModes = SubsystemsSettings.Get.InitialSourceFunction
        Assert.AreEqual(expectedFunctionMode, functionMode, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.FunctionMode)} is {functionMode} ; expected {expectedFunctionMode}")

        expectedBoolean = False
        actualBoolean = device.SourceSubsystem.QueryLimitTripped.GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.OutputEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        Dim expectedDouble As Double = SubsystemsSettings.Get.InitialSourceLevel
        Dim actualDouble As Double = device.SourceSubsystem.QueryLevel.GetValueOrDefault(-1)
        Assert.AreEqual(expectedDouble, actualDouble, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.Level)} is {actualDouble}; expected {expectedDouble}")

        expectedDouble = SubsystemsSettings.Get.InitialSourceLimit
        actualDouble = device.SourceSubsystem.QueryLimit.GetValueOrDefault(-1)
        Assert.AreEqual(expectedDouble, actualDouble, $"{GetType(VI.Tsp2.SourceSubsystemBase)}.{NameOf(VI.Tsp2.SourceSubsystemBase.Limit)} is {actualDouble}; expected {expectedDouble}")

    End Sub

    ''' <summary> (Unit Test Method) tests read source subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ReadSourceSubsystemTest()
        Using device As VI.Tsp2.K2450.K2450Device = VI.Tsp2.K2450.K2450Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.ReadSourceSubsystemInfo(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class
