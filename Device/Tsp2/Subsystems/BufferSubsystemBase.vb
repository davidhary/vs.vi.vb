''' <summary> Defines the contract that must be implemented by a SCPI Trace Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class BufferSubsystemBase
    Inherits VI.BufferSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BufferSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        Me.New(BufferSubsystemBase.DefaultBuffer1Name, statusSubsystem)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="bufferName">      The name of the buffer. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal bufferName As String, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.BufferName = bufferName
    End Sub

#End Region

#Region " BUFFER NAME "

    ''' <summary> The default buffer 1 name. </summary>
    Public Const DefaultBuffer1Name As String = "defbuffer1"

    ''' <summary> The default buffer 2 name. </summary>
    Public Const DefaultBuffer2Name As String = "defbuffer2"

    ''' <summary> Name of the buffer. </summary>
    Private _BufferName As String

    ''' <summary> Builds the commands. </summary>
    ''' <remarks> David, 2020-11-30. </remarks>
    ''' <param name="bufferName"> The name of the buffer. </param>
    Private Sub BuildCommands(ByVal bufferName As String)
        Me.ClearBufferCommand = $"{bufferName}.clear"
        Me.CapacityQueryCommand = $"_G.print(string.format('%d',{Me.BufferName}.capacity))"
        Me.CapacityCommandFormat = $"{Me.BufferName}.capacity={{0}}"
        Me.FillOnceEnabledQueryCommand = $"_G.print({Me.BufferName}.fillmode==buffer.FILL_ONCE)"
        Me.FillOnceEnabledCommandFormat = $"{Me.BufferName}.fillmode={0:'buffer.FILL_ONCE';'buffer.FILL_ONCE';'buffer.FILL_CONTINUOUS'}"
        Me.ActualPointCountQueryCommand = $"_G.print(string.format('%d',{Me.BufferName}.n))"
        Me.FirstPointNumberQueryCommand = $"_G.print(string.format('%d',{Me.BufferName}.startindex))"
        Me.LastPointNumberQueryCommand = $"_G.print(string.format('%d',{Me.BufferName}.endindex))"
        Me.BufferReadCommandFormat = $"_G.printbuffer({{0}},{{1}},{Me.BufferName}.readings,{Me.BufferName}.relativetimestampts,{Me.BufferName}.statuses,{Me.BufferName}.units"
    End Sub

    ''' <summary> Gets or sets the name of the buffer. </summary>
    ''' <value> The name of the buffer. </value>
    Public Property BufferName As String
        Get
            Return Me._BufferName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.BufferName) Then
                Me._BufferName = If(String.IsNullOrWhiteSpace(value), BufferSubsystemBase.DefaultBuffer1Name, value)
                If Not String.IsNullOrWhiteSpace(value) Then Me.BuildCommands(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " COMMAND SYNTAX "

#Region " CLEAR "

    ''' <summary> Gets the Clear Buffer command. </summary>
    ''' <value> The ClearBuffer command. </value>
    Protected Overrides Property ClearBufferCommand As String

#End Region

#Region " CAPACITY "

    ''' <summary> Gets the points count query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property CapacityQueryCommand As String

    ''' <summary> Gets the points count command format. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:COUN {0}". </remarks>
    ''' <value> The points count query command format. </value>
    Protected Overrides Property CapacityCommandFormat As String

#End Region

#Region " FILL ONCE ENABLED  "

    ''' <summary> Gets the automatic Delay enabled query command. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property FillOnceEnabledQueryCommand As String

    ''' <summary> Gets the automatic Delay enabled command Format. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property FillOnceEnabledCommandFormat As String

#End Region

#Region " ACTUAL POINTS "

    ''' <summary> Gets the ActualPoint count query command. </summary>
    ''' <value> The ActualPoint count query command. </value>
    Protected Overrides Property ActualPointCountQueryCommand As String

    ''' <summary> Gets The First Point Number (1-based) query command. </summary>
    ''' <value> The First Point Number query command. </value>
    Protected Overrides Property FirstPointNumberQueryCommand As String

    ''' <summary> Gets The Last Point Number (1-based) query command. </summary>
    ''' <value> The Last Point Number query command. </value>
    Protected Overrides Property LastPointNumberQueryCommand As String

#End Region

#Region " BUFFER STREAMING "

    ''' <summary> Gets the buffer read command format. </summary>
    ''' <value> The buffer read command format. </value>
    Public Overrides  Property BufferReadCommandFormat As String

#End Region

#End Region

#Region " DATA "

    ''' <summary> Gets or sets the data query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property DataQueryCommand As String = String.Empty

#End Region

End Class

