''' <summary> Defines the SCPI numeric limit subsystem base. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para>
''' David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
''' </remarks>
Public MustInherit Class NumericLimitBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="NumericLimitBase" /> class. </summary>
    ''' <param name="limitNumber">     The limit number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal limitNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._LimitNumber = limitNumber
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Enabled = False
    End Sub

#End Region

#Region " NUMERIC COMMAND BUILDER "

    ''' <summary> Gets or sets the limit number. </summary>
    ''' <value> The limit number. </value>
    Protected ReadOnly Property LimitNumber As Integer

    ''' <summary> Builds a command. </summary>
    ''' <param name="baseCommand"> The base command. </param>
    ''' <returns> A String. </returns>
    Protected Function BuildCommand(ByVal baseCommand As String) As String
        Return String.Format(Globalization.CultureInfo.InvariantCulture, baseCommand, Me.LimitNumber)
    End Function

#End Region

#Region " LIMIT FAILED "

    ''' <summary> The failed. </summary>
    Private _Failed As Boolean?

    ''' <summary> Gets or sets the cached In Limit Failed Condition sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if In Limit Failed Condition is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Failed As Boolean?
        Get
            Return Me._Failed
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Failed, value) Then
                Me._Failed = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Limit Failed query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM[#]:FAIL?". </remarks>
    ''' <value> The Limit Failed query command. </value>
    Protected Overridable Property FailedQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Failed sentinel. Also sets the
    ''' <see cref="Failed">Failed</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
    Public Function QueryFailed() As Boolean?
        Me.Failed = Me.Query(Me.Failed, Me.BuildCommand(Me.FailedQueryCommand))
        Return Me.Failed
    End Function

#End Region

#Region " LIMIT ENABLED "

    ''' <summary> The enabled. </summary>
    Private _Enabled As Boolean?

    ''' <summary> Gets or sets the cached Limit Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Limit Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Enabled As Boolean?
        Get
            Return Me._Enabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Enabled, value) Then
                Me._Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteEnabled(value)
        Return Me.QueryEnabled()
    End Function

    ''' <summary> Gets or sets the Limit enabled query command. </summary>
    ''' <remarks> SCPI: "CALC2:LIM[#]:STAT?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property EnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Enabled sentinel. Also sets the
    ''' <see cref="Enabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryEnabled() As Boolean?
        Me.Enabled = Me.Query(Me.Enabled, Me.BuildCommand(Me.EnabledQueryCommand))
        Return Me.Enabled
    End Function

    ''' <summary> Gets or sets the Limit enabled command Format. </summary>
    ''' <remarks> SCPI: "CALC2:LIM[#]:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overridable Property EnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteEnabled(ByVal value As Boolean) As Boolean?
        Me.Enabled = Me.Write(value, Me.BuildCommand(Me.EnabledCommandFormat))
        Return Me.Enabled
    End Function

#End Region

End Class

