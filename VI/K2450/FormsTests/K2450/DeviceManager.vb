'---------------------------------------------------------------------------------------------------
' file:		.VI\K2450\VisaView_Device.vb
'
' summary:	Visa view device class
'---------------------------------------------------------------------------------------------------

''' <summary>
''' Static class for managing the common functions.
''' </summary>
Partial Friend NotInheritable Class DeviceManager

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

End Class



