## ISR HIPOT Tester<sub>&trade;</sub>: High Potential Insulation Meter Tester. Revision
History

*7.1.7220 2019-10-08*  
Visa 2019. Updates all views to using base-class info
provider and tool tip. Updates all views to notify of form closing and
use for updating resource names and titles.

*1.2.6319 2017-04-20*  
Adds exception extensions to display full blown
exception information.

*1.2.6276 2017-03-08*  
Uses .NET 4.

*1.2.5362 2014-09-06*  
Adds contact check. Supports VS2013. Removes resources
and settings from driver tester.

*1.1.4819 2013-03-11*  
Converted to VS2010.

*1.0.4232 2011-08-03*  
Standardize code elements and documentation.

*1.0.4078 2011-03-02*  
Sets resistance ranges default to 500M. Set default
Aperture to 10.

*1.0.4070 2011-02-27*  
Adds End-Of-Test strobe duration.

*1.0.4063 2011-02-15*  
Adds Driver exception. Raises the driver exception if
exception message is not handled by the calling application.

*1.0.4062 2011-02-14*  
Disposes the meter upon closing.

*1.0.4059 2011-02-11*  
No longer requires dependent libraries.

*1.0.4031 2010-01-14*  
Adds binaries to the developer installer.

*1.0.4015 2010-12-29*  
Updates installer upgrade codes.

*1.0.4014 2010-12-28*  
Beta release.

\(C\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed using the Microsoft [This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:

[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[
### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
