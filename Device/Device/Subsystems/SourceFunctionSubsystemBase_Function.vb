'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SourceFunctionSubsystemBase_Function.vb
'
' summary:	Source function subsystem base function class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

Partial Public Class SourceFunctionSubsystemBase

#Region " RANGE "

    ''' <summary> Define function mode ranges. </summary>
    Private Sub DefineFunctionModeRanges()
        Me._FunctionModeRanges = New RangeDictionary
        SourceSubsystemBase.DefineFunctionModeRanges(Me.FunctionModeRanges, Me.DefaultFunctionRange)
    End Sub

    ''' <summary> Gets or sets the function mode ranges. </summary>
    ''' <value> The function mode ranges. </value>
    Public ReadOnly Property FunctionModeRanges As RangeDictionary

    ''' <summary> Gets or sets the default function range. </summary>
    ''' <value> The default function range. </value>
    Public Property DefaultFunctionRange As isr.Core.Constructs.RangeR

    ''' <summary> Converts a functionMode to a range. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> FunctionMode as an isr.Core.Constructs.RangeR. </returns>
    Public Overridable Function ToRange(ByVal functionMode As Integer) As isr.Core.Constructs.RangeR
        Return Me.FunctionModeRanges(functionMode)
    End Function

    ''' <summary> The function range. </summary>
    Private _FunctionRange As Core.Constructs.RangeR

    ''' <summary> The Range of the range. </summary>
    ''' <value> The function range. </value>
    Public Property FunctionRange As Core.Constructs.RangeR
        Get
            Return Me._FunctionRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.FunctionRange <> value Then
                Me._FunctionRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " DECIMAL PLACES "

    ''' <summary> Gets or sets the default decimal places. </summary>
    ''' <value> The default decimal places. </value>
    Public Property DefaultFunctionModeDecimalPlaces As Integer

    ''' <summary> Define function mode decimal places. </summary>
    Private Sub DefineFunctionModeDecimalPlaces()
        Me._FunctionModeDecimalPlaces = New IntegerDictionary
        MultimeterSubsystemBase.DefineFunctionModeDecimalPlaces(Me.FunctionModeDecimalPlaces, Me.DefaultFunctionModeDecimalPlaces)
    End Sub

    ''' <summary> Gets or sets the function mode decimal places. </summary>
    ''' <value> The function mode decimal places. </value>
    Public ReadOnly Property FunctionModeDecimalPlaces As IntegerDictionary

    ''' <summary> Converts a function Mode to a decimal places. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> FunctionMode as an Integer. </returns>
    Public Overridable Function ToDecimalPlaces(ByVal functionMode As Integer) As Integer
        Return Me.FunctionModeDecimalPlaces(functionMode)
    End Function

    ''' <summary> The function range decimal places. </summary>
    Private _FunctionRangeDecimalPlaces As Integer

    ''' <summary> Gets or sets the function range decimal places. </summary>
    ''' <value> The function range decimal places. </value>
    Public Property FunctionRangeDecimalPlaces As Integer
        Get
            Return Me._FunctionRangeDecimalPlaces
        End Get
        Set(value As Integer)
            If Me.FunctionRangeDecimalPlaces <> value Then
                Me._FunctionRangeDecimalPlaces = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " UNIT "

    ''' <summary> Gets or sets the default unit. </summary>
    ''' <value> The default unit. </value>
    Public Property DefaultFunctionUnit As Arebis.TypedUnits.Unit

    ''' <summary> Define function mode units. </summary>
    Private Sub DefineFunctionModeUnits()
        Me._FunctionModeUnits = New UnitDictionary
        SourceSubsystemBase.DefineFunctionModeUnits(Me._FunctionModeUnits)
    End Sub

    ''' <summary> Gets or sets the function mode decimal places. </summary>
    ''' <value> The function mode decimal places. </value>
    Public ReadOnly Property FunctionModeUnits As UnitDictionary

    ''' <summary> Parse units. </summary>
    ''' <param name="functionMode"> The  Multimeter Function Mode. </param>
    ''' <returns> An Arebis.TypedUnits.Unit. </returns>
    Public Overridable Function ToUnit(ByVal functionMode As Integer) As Arebis.TypedUnits.Unit
        Return Me.FunctionModeUnits(functionMode)
    End Function

    ''' <summary> Gets or sets the function unit. </summary>
    ''' <value> The function unit. </value>
    Public Property FunctionUnit As Arebis.TypedUnits.Unit
        Get
            Return Me.Amount.Unit
        End Get
        Set(value As Arebis.TypedUnits.Unit)
            If Me.FunctionUnit <> value Then
                Me.NewAmount(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Define function mode read writes. </summary>
    Private Sub DefineFunctionModeReadWrites()
        Me._FunctionModeReadWrites = New Pith.EnumReadWriteCollection
        SourceSubsystemBase.DefineFunctionModeReadWrites(Me.FunctionModeReadWrites)
    End Sub

    ''' <summary> Gets or sets a dictionary of Source function mode parses. </summary>
    ''' <value> A Dictionary of Source function mode parses. </value>
    Public ReadOnly Property FunctionModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported function modes. </summary>
    Private _SupportedFunctionModes As SourceFunctionModes

    ''' <summary>
    ''' Gets or sets the supported Function Modes. This is a subset of the functions supported by the
    ''' instrument.
    ''' </summary>
    ''' <value> The supported Source function modes. </value>
    Public Property SupportedFunctionModes() As SourceFunctionModes
        Get
            Return Me._SupportedFunctionModes
        End Get
        Set(ByVal value As SourceFunctionModes)
            If Not Me.SupportedFunctionModes.Equals(value) Then
                Me._SupportedFunctionModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the function code. </summary>
    ''' <value> The function code. </value>
    Protected ReadOnly Property FunctionCode As String

    ''' <summary> Define function clear known state. </summary>
    Protected Overridable Sub DefineFunctionClearKnownState()
        Me.NewAmount(Me.FunctionUnit)
        Me._FunctionCode = If(Me.FunctionMode.HasValue, Me.FunctionMode.Value.ExtractBetween, String.Empty)
    End Sub

    ''' <summary> The function mode. </summary>
    Private _FunctionMode As SourceFunctionModes?

    ''' <summary> Gets or sets the cached Source function mode. </summary>
    ''' <value>
    ''' The <see cref="FunctionMode">Source function mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property FunctionMode As SourceFunctionModes?
        Get
            Return Me._FunctionMode
        End Get
        Protected Set(ByVal value As SourceFunctionModes?)
            If Not Me.FunctionMode.Equals(value) Then
                Me._FunctionMode = value
                If value.HasValue Then
                    Me.FunctionRange = Me.ToRange(value.Value)
                    Me.FunctionUnit = Me.ToUnit(value.Value)
                    Me.FunctionRangeDecimalPlaces = Me.ToDecimalPlaces(value.Value)
                Else
                    Me.FunctionRange = Me.DefaultFunctionRange
                    Me.FunctionUnit = Me.DefaultFunctionUnit
                    Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
                End If
                Me.DefineFunctionClearKnownState()
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Source function mode. </summary>
    ''' <param name="value"> The  Source function mode. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">source Source function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function ApplyFunctionMode(ByVal value As SourceFunctionModes) As SourceFunctionModes?
        Me.WriteFunctionMode(value)
        Return Me.QueryFunctionMode()
    End Function

    ''' <summary> Gets or sets the Source function mode query command. </summary>
    ''' <value> The Source function mode query command. </value>
    Protected MustOverride ReadOnly Property FunctionModeQueryCommand As String

    ''' <summary> Queries the Source function mode. </summary>
    ''' <returns>
    ''' The <see cref="FunctionMode">Source function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryFunctionMode() As SourceFunctionModes?
        Return Me.QueryFunctionMode(Me.FunctionModeQueryCommand)
    End Function

    ''' <summary> Queries print function mode. </summary>
    ''' <returns> The print function mode. </returns>
    Public Overridable Function QueryPrintFunctionMode() As SourceFunctionModes?
        Return Me.QueryFunctionMode(Me.Session.BuildQueryPrintCommand(Me.FunctionModeQueryCommand))
    End Function

    ''' <summary> Queries the Source function mode. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">Source function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryFunctionMode(ByVal queryCommand As String) As SourceFunctionModes?
        Me.FunctionMode = Me.Query(Of SourceFunctionModes)(queryCommand,
                                                          Me.FunctionMode.GetValueOrDefault(SourceFunctionModes.None),
                                                          Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

    ''' <summary> Gets or sets the Source function mode command format. </summary>
    ''' <value> The Source function mode command format. </value>
    Protected MustOverride ReadOnly Property FunctionModeCommandFormat As String

    ''' <summary>
    ''' Writes the Source function mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Source function mode. </param>
    ''' <returns>
    ''' The <see cref="FunctionMode">Source function mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteFunctionMode(ByVal value As SourceFunctionModes) As SourceFunctionModes?
        Me.FunctionMode = Me.Write(Of SourceFunctionModes)(Me.FunctionModeCommandFormat, value, Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

#End Region

End Class

