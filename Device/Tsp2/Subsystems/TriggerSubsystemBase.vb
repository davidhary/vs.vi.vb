'---------------------------------------------------------------------------------------------------
' file:		Tsp2\Subsystems\TriggerSubsystemBase.vb
'
' summary:	Trigger subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

''' <summary> Defines the contract that must be implemented by a SCPI Trigger Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class TriggerSubsystemBase
    Inherits VI.TriggerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TriggerSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        MyBase.TriggerEventReadWrites.AddReplace(TriggerEvents.None, "trigger.EVENT_NONE", TriggerEvents.None.DescriptionUntil)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.AutoDelayEnabled = False
        Me.TriggerCount = 1
        Me.Delay = TimeSpan.Zero
        Me.TriggerLayerBypassMode = VI.TriggerLayerBypassModes.Acceptor
        Me.InputLineNumber = 1
        Me.OutputLineNumber = 2
        Me.TriggerSource = TriggerSources.Immediate
        Me.TimerInterval = TimeSpan.FromSeconds(0.1)
        Me.SupportedTriggerSources = TriggerSources.Bus Or TriggerSources.External Or TriggerSources.Immediate
        Me.ContinuousEnabled = False
        Me.TriggerState = VI.TriggerState.None
    End Sub

#End Region

#Region " BLENDER CLEAR "

    ''' <summary> Gets or sets the trigger blender clear command format. </summary>
    ''' <value> The trigger blender clear command format. </value>
    Protected Overridable Property TriggerBlenderClearCommandFormat As String = "trigger.blender[{0}].clear()"

    ''' <summary> Trigger blender clear. </summary>
    ''' <param name="blenderNumber"> The blender number. </param>
    Public Sub TriggerBlenderClear(ByVal blenderNumber As Integer)
        Me.Execute(Me.TriggerBlenderClearCommandFormat, blenderNumber)
    End Sub

#End Region

#Region " BLENDER OR ENABLED  "

    ''' <summary> Applies the trigger blender or enabled described by value. </summary>
    ''' <param name="value"> True to value. </param>
    ''' <returns> A Boolean? </returns>
    Public Function ApplyTriggerBlenderOrEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoDelayEnabled(value)
        Return Me.QueryAutoDelayEnabled()
    End Function

    ''' <summary> Gets or sets the trigger blender or enable query command format. </summary>
    ''' <value> The trigger blender or enable query command format. </value>
    Protected Overridable Property TriggerBlenderOrEnableQueryCommandFormat As String = "_G.print(trigger.blender[{0}].orenable=true)"

    ''' <summary> Queries trigger blender or enabled. </summary>
    ''' <param name="blenderNumber"> The blender number. </param>
    ''' <returns> The trigger blender or enabled. </returns>
    Public Function QueryTriggerBlenderOrEnabled(ByVal blenderNumber As Integer) As Boolean?
        Me.AutoDelayEnabled = Me.Query(False, String.Format(Me.TriggerBlenderOrEnableQueryCommandFormat, blenderNumber))
        Return Me.AutoDelayEnabled
    End Function

    ''' <summary> Gets or sets the trigger blender or enable command format. </summary>
    ''' <value> The trigger blender or enable command format. </value>
    Protected Overridable Property TriggerBlenderOrEnableCommandFormat As String = "trigger.blender[{0}].orenable={1}"

    ''' <summary> Writes a trigger blender or enables. </summary>
    ''' <param name="blenderNumber"> The blender number. </param>
    ''' <param name="value">         True to value. </param>
    Public Sub WriteTriggerBlenderOrEnables(ByVal blenderNumber As Integer, ByVal value As Boolean)
        Me.Execute(Me.TriggerBlenderOrEnableCommandFormat, blenderNumber, value)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " ABORT / INIT COMMANDS "

    ''' <summary> Gets or sets the Abort command. </summary>
    ''' <value> The Abort command. </value>
    Protected Overrides Property AbortCommand As String = "trigger.model.abort()"

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <value> The initiate command. </value>
    Protected Overrides Property InitiateCommand As String = "trigger.model.initiate()"

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearCommand As String = "trigger.extin.clear()"

    ''' <summary> Gets or sets the clear  trigger model command. </summary>
    ''' <remarks> SCPI: ":TRIG:LOAD 'EMPTY'". </remarks>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearTriggerModelCommand As String = "trigger.model.clear()"


#End Region

#Region " TRIGGER STATE "

    ''' <summary> Gets or sets the Trigger State query command. </summary>
    ''' <remarks>
    '''    <c>SCPI: :TRIG:STAT? <para>
    ''' TSP2: trigger.model.state()
    ''' </para></c>
    ''' </remarks>
    ''' <value> The Trigger State query command. </value>
    Protected Overrides Property TriggerStateQueryCommand As String = "_G.print(trigger.model.state())"

#End Region

#Region " TRIGGER COUNT "

    ''' <summary> Gets or sets trigger count query command. </summary>
    ''' <value> The trigger count query command. </value>
    Protected Overrides Property TriggerCountQueryCommand As String = String.Empty '  ":TRIG:COUN?"

    ''' <summary> Gets or sets trigger count command format. </summary>
    ''' <value> The trigger count command format. </value>
    Protected Overrides Property TriggerCountCommandFormat As String = String.Empty '  ":TRIG:COUN {0}"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = String.Empty '  ":TRIG:DEL {0:s\.FFFFFFF}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = String.Empty ' "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = String.Empty ' ":TRIG:DEL?"

#End Region

#End Region

End Class

