Imports System.Reflection

Imports isr.Core.EnumerableExtensions
Imports isr.Core.EnumExtensions
''' <summary> A session tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-11 </para>
''' </remarks>
<TestClass()>
Public Class ParseTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite()
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()

        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Overridable Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " REGISTER VALUE FORMAT PROVIDER "

    ''' <summary> (Unit Test Method) tests hexadecimal value format provider. </summary>
    <TestMethod()>
    Public Sub HexValueFormatProviderTest()
        Dim provider As Pith.HexFormatProvider = Pith.HexFormatProvider.FormatProvider(2)
        Dim expectedValue As Pith.ServiceRequests = Pith.ServiceRequests.All And Not Pith.ServiceRequests.RequestingService
        Dim expectedCaption As String = $"0x{CInt(expectedValue):X2}"
        Dim actualCaption As String = provider.Format(expectedValue)
        Assert.AreEqual(expectedCaption, actualCaption, $"Should be the hexadecimal caption of {expectedValue}")
        Dim actualValue As Pith.ServiceRequests = provider.ParseEnumHexValue(Of Pith.ServiceRequests)(actualCaption).GetValueOrDefault(Pith.ServiceRequests.None)
        Assert.AreEqual(expectedValue, actualValue, $"Should be the hexadecimal value parsed value {expectedValue}")

        expectedCaption = "0x.."
        actualCaption = provider.NullValueCaption
        Assert.AreEqual(expectedCaption, actualCaption, $"Should be the null caption")

        provider = Pith.HexFormatProvider.FormatProvider(4)
        expectedValue = Pith.ServiceRequests.All And Not Pith.ServiceRequests.RequestingService
        expectedCaption = $"0x{CInt(expectedValue):X4}"
        actualCaption = provider.Format(expectedValue)
        Assert.AreEqual(expectedCaption, actualCaption, $"Should be the hexadecimal caption of {expectedValue}")
        actualValue = provider.ParseEnumHexValue(Of Pith.ServiceRequests)(actualCaption).GetValueOrDefault(Pith.ServiceRequests.None)
        Assert.AreEqual(expectedValue, actualValue, $"Should be the hexadecimal value parsed value {expectedValue}")

        expectedCaption = "0x...."
        actualCaption = provider.NullValueCaption
        Assert.AreEqual(expectedCaption, actualCaption, $"Should be the null caption")

    End Sub


#End Region

#Region " PARSE: ENUM; BOOLEAN "

    ''' <summary> (Unit Test Method) nullable tests. </summary>
    ''' <remarks> David, 2020-10-28. </remarks>
    <TestMethod()>
    Public Sub NullableTests()
        Assert.AreEqual(New Boolean?, New Boolean?, "? New Boolean? = New Boolean?")
        Assert.AreNotEqual(New Boolean?(True), New Boolean?, "? New Boolean?(True) = New Boolean?")
        Assert.IsNull(New Boolean? = New Boolean?, "? Null = New Boolean? = New Boolean?")
    End Sub


    ''' <summary> A test for ParseEnumValue. </summary>
    ''' <param name="value">    The value. </param>
    ''' <param name="expected"> The expected. </param>
    Public Shared Sub ParseEnumValueTestHelper(Of T As Structure)(ByVal value As String, ByVal expected As Nullable(Of T))
        Dim actual As Nullable(Of T)
        actual = VI.Pith.SessionBase.ParseEnumValue(Of T)(value)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests parse enum value. </summary>
    <TestMethod()>
    Public Sub ParseEnumValueTest()
        ParseTests.ParseEnumValueTestHelper(Of Diagnostics.TraceEventType)("2", TraceEventType.Error)
    End Sub

    ''' <summary> (Unit Test Method) tests parse boolean. </summary>
    <TestMethod()>
    Public Sub ParseBooleanTest()
        Dim reading As String = "0"
        Dim expectedResult As Boolean = False
        Dim actualResult As Boolean = True
        Dim successParsing As Boolean = VI.Pith.SessionBase.TryParse(reading, actualResult)
        Assert.AreEqual(expectedResult, actualResult, "Value set to {0}", actualResult)
        Assert.AreEqual(True, successParsing, "Success set to {0}", actualResult)
        reading = "1"
        expectedResult = True
        successParsing = VI.Pith.SessionBase.TryParse(reading, actualResult)
        Assert.AreEqual(expectedResult, actualResult, "Value set to {0}", actualResult)
        Assert.AreEqual(True, successParsing, "Success set to {0}", actualResult)
    End Sub

    ''' <summary> (Unit Test Method) tests enum names. </summary>
    ''' <remarks> David, 2020-10-28. </remarks>
    <TestMethod()>
    Public Sub EnumNamesTest()
        Dim traceEvent As TraceEventType = TraceEventType.Verbose
        Dim expectedValue As String = "Verbose"
        Dim actualValue As String = traceEvent.Names()
        Assert.AreEqual(expectedValue, actualValue,
                        $"{NameOf(isr.Core.EnumExtensions.Methods.Names)} of {NameOf(System.Diagnostics.TraceEventType)}.{NameOf(System.Diagnostics.TraceEventType.Verbose)} should match")

        Dim armSource As ArmSources = ArmSources.Bus
        expectedValue = "Bus"
        actualValue = armSource.Names()
        Assert.AreEqual(expectedValue, actualValue,
                        $"{NameOf(isr.Core.EnumExtensions.Methods.Names)} of {NameOf(ArmSources)}.{NameOf(ArmSources.Bus)} should match")

        expectedValue = "Bus (BUS)"
        actualValue = armSource.Description()
        Assert.AreEqual(expectedValue, actualValue,
                        $"{NameOf(isr.Core.EnumExtensions.Methods.Description)} of {NameOf(ArmSources)}.{NameOf(ArmSources.Bus)} should match")

        expectedValue = "Bus (BUS)"
        actualValue = armSource.Descriptions()
        Assert.AreEqual(expectedValue, actualValue,
                        $"{NameOf(isr.Core.EnumExtensions.Methods.Descriptions)} of {NameOf(ArmSources)}.{NameOf(ArmSources.Bus)} should match")

        armSource = ArmSources.Bus Or ArmSources.External
        expectedValue = "Bus, External"
        actualValue = armSource.Names()
        Assert.AreEqual(expectedValue, actualValue,
                        $"{NameOf(isr.Core.EnumExtensions.Methods.Names)} of {NameOf(ArmSources)}.({NameOf(ArmSources.Bus)} or {NameOf(ArmSources.External)}) should match")

        expectedValue = "Bus (BUS), External (EXT)"
        actualValue = armSource.Descriptions()
        Assert.AreEqual(expectedValue, actualValue,
                        $"{NameOf(isr.Core.EnumExtensions.Methods.Descriptions)} of {NameOf(ArmSources)}.({NameOf(ArmSources.Bus)} or {NameOf(ArmSources.External)}) should match")

    End Sub

#End Region

#Region " ENUM READ WRITE "

    ''' <summary> Tests unknown enum read write value. </summary>
    ''' <param name="enumReadWrites">    The enum read writes. </param>
    ''' <param name="expectedEnumValue"> The expected enum value. </param>
    Private Shared Sub TestUnknownEnumReadWriteValue(enumReadWrites As VI.Pith.EnumReadWriteCollection, expectedEnumValue As Long)
        Assert.IsFalse(enumReadWrites.Exists(expectedEnumValue), $"{expectedEnumValue} should Not exist")
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of KeyNotFoundException)(Function() enumReadWrites.SelectItem(expectedEnumValue), $"{expectedEnumValue} enum value Is out of range")
    End Sub

    ''' <summary> Tests unknown enum read write value. </summary>
    ''' <param name="enumReadWrites">    The enum read writes. </param>
    ''' <param name="expectedReadValue"> The expected read value. </param>
    Private Shared Sub TestUnknownEnumReadWriteValue(enumReadWrites As VI.Pith.EnumReadWriteCollection, expectedReadValue As String)
        Assert.IsFalse(enumReadWrites.Exists(expectedReadValue), $"{expectedReadValue} should Not exist")
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of KeyNotFoundException)(Function() enumReadWrites.SelectItem(expectedReadValue), $"{expectedReadValue} read value should Not exist")
    End Sub

    ''' <summary> Tests enum read write value. </summary>
    ''' <param name="enumReadWrites">    The enum read writes. </param>
    ''' <param name="expectedEnumValue"> The expected enum value. </param>
    ''' <param name="expectedReadValue"> The expected read value. </param>
    Private Shared Sub TestEnumReadWriteValue(enumReadWrites As VI.Pith.EnumReadWriteCollection, expectedEnumValue As Long, expectedReadValue As String)
        Assert.IsTrue(enumReadWrites.Exists(expectedEnumValue), $"{expectedEnumValue} exists")
        Assert.IsTrue(enumReadWrites.Exists(expectedReadValue), $"{expectedReadValue} exists")
        Dim enumReadWrite As VI.Pith.EnumReadWrite = enumReadWrites.SelectItem(expectedEnumValue)
        Assert.AreEqual(expectedEnumValue, enumReadWrite.EnumValue, "expected item equals item selected from the collection")
        Assert.AreEqual(expectedReadValue, enumReadWrite.ReadValue, "expected read value equals read value selected from the collection")
    End Sub

    ''' <summary> (Unit Test Method) tests enum read write. </summary>
    <TestMethod()>
    Public Sub EnumReadWriteTest()
        Dim enumReadWrites As New VI.Pith.EnumReadWriteCollection
        VI.MultimeterSubsystemBase.DefineFunctionModeReadWrites(enumReadWrites)

        Dim expectedEnumValue As Long = 1000
        Dim expectedReadValue As String = "xx"
        ParseTests.TestUnknownEnumReadWriteValue(enumReadWrites, expectedEnumValue)
        ParseTests.TestUnknownEnumReadWriteValue(enumReadWrites, expectedReadValue)

        expectedEnumValue = CLng(VI.MultimeterFunctionModes.VoltageDC)
        expectedReadValue = "dmm.FUNC_DC_VOLTAGE"
        ParseTests.TestEnumReadWriteValue(enumReadWrites, expectedEnumValue, expectedReadValue)

        Dim removedEnumValue As Long = VI.MultimeterFunctionModes.ResistanceCommonSide
        enumReadWrites.RemoveAt(removedEnumValue)
        ParseTests.TestUnknownEnumReadWriteValue(enumReadWrites, removedEnumValue)

        ' check the diminished collection
        ParseTests.TestEnumReadWriteValue(enumReadWrites, expectedEnumValue, expectedReadValue)
    End Sub

#End Region

#Region " BUFFER READINGS TESTS "

    ''' <summary> (Unit Test Method) tests parse reading unit. </summary>
    <TestMethod()>
    Public Sub ParseReadingUnitTest()
        Dim expectedValue As Double = 1.234
        Dim expectedUnit As Arebis.TypedUnits.Unit = Arebis.StandardUnits.ElectricUnits.Volt
        Dim readingValue As String = $"{expectedValue}VDC"
        Dim expectedAmount As Arebis.TypedUnits.Amount = New Arebis.TypedUnits.Amount(expectedValue, expectedUnit)
        Dim actualAmount As Arebis.TypedUnits.Amount = isr.VI.BufferReading.ToAmount(readingValue)
        Assert.AreEqual(expectedAmount, actualAmount)
    End Sub


#End Region

#Region " BITMAPS TESTS "

    ''' <summary> Define measurement events bitmasks. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.MeasurementEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.ReadingOverflow, 1 << 0)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit1, 1 << 1)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit1, 1 << 2)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit2, 1 << 3)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit2, 1 << 4)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.ReadingAvailable, 1 << 5)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.BufferAvailable, 1 << 7)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.BufferHalfFull, 1 << 8)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.BufferFull, 1 << 9)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.BufferPretriggered, 1 << 11)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.Questionable, 1 << 31, True)
    End Sub

    ''' <summary> Define operation event bitmasks. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.OperationEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        bitmaskDictionary.Add(OperationEventBitmaskKey.Arming, 1 << 6)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Calibrating, 1 << 0)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Idle, 1 << 10)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Measuring, 1 << 4)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Triggering, 1 << 5)
    End Sub

    ''' <summary> Define questionable event bitmasks. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.QuestionableEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        bitmaskDictionary.Add(QuestionableEventBitmaskKey.CalibrationSummary, 1 << 4)
        bitmaskDictionary.Add(QuestionableEventBitmaskKey.CommandWarning, 1 << 8)
        ' bitmaskDictionary.Add(QuestionableEventBitmask.TemperatureSummary, 1 << 14)
    End Sub

    ''' <summary> (Unit Test Method) tests build bitmap dictionary. </summary>
    <TestMethod()>
    Public Sub BuildBitmapDictionaryTest()
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        ParseTests.DefineBitmasks(measurementBitmasks)

        ' test existing key
        Dim queryKey As Integer = MeasurementEventBitmaskKey.HighLimit2
        Dim expectedBitmask As Integer = 1 << 4
        Dim actualBitmask As Integer = measurementBitmasks.Item(queryKey)
        Assert.AreEqual(expectedBitmask, actualBitmask, $"Bitmask of key 0x{queryKey:X} should match expected value")

        ' test non existing key
        queryKey = MeasurementEventBitmaskKey.FirstReadingInGroup
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of ArgumentException)(Sub() measurementBitmasks.IsAnyBitOn(queryKey), $"Bitmask of key 0x{queryKey:X} should throw an exception")

        ' test invalid bitmask
        queryKey = MeasurementEventBitmaskKey.FirstReadingInGroup
        Dim existingBitmask As Integer = 1 << 4
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of ArgumentException)(Sub() measurementBitmasks.Add(queryKey, existingBitmask), $"Bitmask of key 0x{queryKey:X} should throw an exception because {existingBitmask} already exists")

        Dim operationBitmasks As New OperationEventsBitmaskDictionary
        ParseTests.DefineBitmasks(operationBitmasks)

        ' test existing key
        queryKey = OperationEventBitmaskKey.Arming
        expectedBitmask = 1 << 6
        actualBitmask = operationBitmasks.Item(queryKey)
        Assert.AreEqual(expectedBitmask, actualBitmask, $"Bitmask of key 0x{queryKey:X} should match expected value")

        ' test non existing key
        queryKey = OperationEventBitmaskKey.Setting
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of ArgumentException)(Sub() operationBitmasks.IsAnyBitOn(queryKey), $"Bitmask of key 0x{queryKey:X} should throw an exception")

        ' test invalid bitmask
        queryKey = OperationEventBitmaskKey.Setting
        existingBitmask = 1 << 6
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of ArgumentException)(Sub() operationBitmasks.Add(queryKey, existingBitmask), $"Bitmask of key 0x{queryKey:X} should throw an exception because {existingBitmask} already exists")

        Dim questionableBitmasks As New QuestionableEventsBitmaskDictionary
        ParseTests.DefineBitmasks(questionableBitmasks)

        ' test existing key
        queryKey = QuestionableEventBitmaskKey.CalibrationSummary
        expectedBitmask = 1 << 4
        actualBitmask = questionableBitmasks.Item(queryKey)
        Assert.AreEqual(expectedBitmask, actualBitmask, $"Bitmask of key 0x{queryKey:X} should match expected value")

        ' test non existing key
        queryKey = QuestionableEventBitmaskKey.TemperatureSummary
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of ArgumentException)(Sub() questionableBitmasks.IsAnyBitOn(queryKey), $"Bitmask of key 0x{queryKey:X} should throw an exception")

        ' test invalid bitmask
        queryKey = QuestionableEventBitmaskKey.TemperatureSummary
        existingBitmask = 1 << 4
        isr.Core.FrameworkTests.Asserts.Get().Throws(Of ArgumentException)(Sub() questionableBitmasks.Add(queryKey, existingBitmask), $"Bitmask of key 0x{queryKey:X} should throw an exception because {existingBitmask} already exists")

    End Sub

#End Region

End Class


