'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\ArmLayerView.vb
'
' summary:	Arm layer view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.Core.EnumExtensions
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.Core.SplitExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> An Arm Layer view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ArmLayerView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._CountNumeric.NumericUpDownControl.Minimum = 1
        Me._CountNumeric.NumericUpDownControl.Maximum = 99999
        Me._CountNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._CountNumeric.NumericUpDownControl.Value = 1

        Me._InputLineNumeric.NumericUpDownControl.Minimum = 1
        Me._InputLineNumeric.NumericUpDownControl.Maximum = 6
        Me._InputLineNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._InputLineNumeric.NumericUpDownControl.Value = 2

        Me._OutputLineNumeric.NumericUpDownControl.Minimum = 1
        Me._OutputLineNumeric.NumericUpDownControl.Maximum = 6
        Me._OutputLineNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._OutputLineNumeric.NumericUpDownControl.Value = 1

        Me._TimerIntervalNumeric.NumericUpDownControl.Minimum = 0
        Me._TimerIntervalNumeric.NumericUpDownControl.Maximum = 99999D
        Me._TimerIntervalNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._TimerIntervalNumeric.NumericUpDownControl.Value = 1

        Me._DelayNumeric.NumericUpDownControl.Minimum = 0
        Me._DelayNumeric.NumericUpDownControl.Maximum = 99999D
        Me._DelayNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._DelayNumeric.NumericUpDownControl.Value = 0

    End Sub

    ''' <summary> Creates a new ArmLayerView. </summary>
    ''' <returns> An ArmLayerView. </returns>
    Public Shared Function Create() As ArmLayerView
        Dim view As ArmLayerView = Nothing
        Try
            view = New ArmLayerView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> The layer number. </summary>
    Private _LayerNumber As Integer

    ''' <summary> Gets or sets the layer number. </summary>
    ''' <value> The layer number. </value>
    Public Property LayerNumber As Integer
        Get
            Return Me._LayerNumber
        End Get
        Set(value As Integer)
            If value <> Me.LayerNumber Then
                Me._LayerNumber = value
                Me._SubsystemSplitButton.Text = $"Arm{value}"
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the number of triggers. </summary>
    ''' <value> The number of triggers. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Count As Integer
        Get
            Return If(Me._InfiniteCountButton.CheckState = CheckState.Checked, Integer.MaxValue, CInt(Me._CountNumeric.Value))
        End Get
        Set(value As Integer)
            If value <> Me.Count Then
                If value = Integer.MaxValue Then
                    Me._InfiniteCountButton.CheckState = CheckState.Checked
                Else
                    Me._InfiniteCountButton.CheckState = CheckState.Unchecked
                    Me._CountNumeric.Value = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the arm source. </summary>
    ''' <value> The arm source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Source As ArmSources
        Get
            Return CType(Me._SourceComboBox.ComboBox.SelectedValue, ArmSources)
        End Get
        Set(value As ArmSources)
            If value <> Me.Source Then
                Me._SourceComboBox.ComboBox.SelectedItem = value.ValueNamePair
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the settings onto the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying {Me._SubsystemSplitButton.Text} settings" : Me.PublishInfo($"{activity};. ")
            Me.ArmLayerSubsystem.StartElapsedStopwatch()

            If Me._InfiniteCountButton.CheckState = CheckState.Checked Then
                Me.ArmLayerSubsystem.ApplyArmCount(Integer.MaxValue)
            Else
                Me.ArmLayerSubsystem.ApplyArmCount(CInt(Me._CountNumeric.Value))
            End If
            Me.ArmLayerSubsystem.ApplyInputLineNumber(CInt(Me._InputLineNumeric.Value))
            Me.ArmLayerSubsystem.ApplyOutputLineNumber(CInt(Me._OutputLineNumeric.Value))
            Me.ArmLayerSubsystem.ApplyArmSource(CType(Me._SourceComboBox.ComboBox.SelectedValue, ArmSources))
            Me.ArmLayerSubsystem.ApplyArmLayerBypassMode(If(Me._BypassToggleButton.CheckState = CheckState.Checked, TriggerLayerBypassModes.Source, TriggerLayerBypassModes.Acceptor))

            If Me.LayerNumber = 2 Then
                Me.ArmLayerSubsystem.ApplyDelay(TimeSpan.FromMilliseconds(1000 * Me._DelayNumeric.Value))
                Me.ArmLayerSubsystem.ApplyTimerTimeSpan(TimeSpan.FromMilliseconds(1000 * Me._TimerIntervalNumeric.Value))
            End If
            Me.ArmLayerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading  {Me._SubsystemSplitButton.Text} settings" : Me.PublishInfo($"{activity};. ")
            ArmLayerView.ReadSettings(Me.ArmLayerSubsystem)
            Me.ApplyPropertyChanged(Me.ArmLayerSubsystem)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(ArmLayerView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SUBSYSTEM "

    ''' <summary> Gets or sets the arm layer subsystem. </summary>
    ''' <value> The arm layer subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayerSubsystem As ArmLayerSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="layerNumber"> The layer number. </param>
    Public Sub BindSubsystem(ByVal subsystem As ArmLayerSubsystemBase, ByVal layerNumber As Integer)
        If Me.ArmLayerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ArmLayerSubsystem)
            Me._ArmLayerSubsystem = Nothing
        End If
        Me.LayerNumber = layerNumber
        Me._ArmLayerSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.ArmLayerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ArmLayerSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ArmLayerSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.SupportedArmSources))
            ArmLayerView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ArmLayerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As ArmLayerSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.ArmCount))
        Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.ArmSource))
        Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.IsArmCountInfinite))
        Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.IsArmLayerBypass))
        Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.InputLineNumber))
        Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.OutputLineNumber))
        If subsystem.LayerNumber = 2 Then
            Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.Delay))
            Me.HandlePropertyChanged(subsystem, NameOf(ArmLayerSubsystemBase.TimerInterval))
        End If
    End Sub

    ''' <summary> Handle the Calculate subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ArmLayerSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(ArmLayerSubsystemBase.ArmCount)
                If subsystem.ArmCount.HasValue Then Me._CountNumeric.Value = CDec(subsystem.ArmCount.Value)

            Case NameOf(ArmLayerSubsystemBase.ArmSource)
                If subsystem.ArmSource.HasValue AndAlso Me._SourceComboBox.ComboBox.Items.Count > 0 Then
                    Me._SourceComboBox.ComboBox.SelectedItem = subsystem.ArmSource.Value.ValueNamePair
                End If

            Case NameOf(ArmLayerSubsystemBase.SupportedArmSources)
                Me.InitializingComponents = True
                Me._SourceComboBox.ComboBox.ListSupportedArmSources(subsystem.SupportedArmSources)
                Me.InitializingComponents = False
                If subsystem.ArmSource.HasValue AndAlso Me._SourceComboBox.ComboBox.Items.Count > 0 Then
                    Me._SourceComboBox.ComboBox.SelectedItem = subsystem.ArmSource.Value.ValueNamePair
                End If

            Case NameOf(ArmLayerSubsystemBase.InputLineNumber)
                If subsystem.InputLineNumber.HasValue Then Me._InputLineNumeric.Value = CDec(subsystem.InputLineNumber.Value)

            Case NameOf(ArmLayerSubsystemBase.IsArmCountInfinite)
                Me._InfiniteCountButton.CheckState = subsystem.IsArmCountInfinite.ToCheckState

            Case NameOf(ArmLayerSubsystemBase.IsArmLayerBypass)
                Me._BypassToggleButton.CheckState = subsystem.IsArmLayerBypass.ToCheckState

            Case NameOf(ArmLayerSubsystemBase.OutputLineNumber)
                If subsystem.OutputLineNumber.HasValue Then Me._OutputLineNumeric.Value = CDec(subsystem.OutputLineNumber.Value)

            Case NameOf(ArmLayerSubsystemBase.Delay)
                If subsystem.Delay.HasValue Then Me._DelayNumeric.Value = CDec(0.001 * subsystem.Delay.Value.TotalMilliseconds)

            Case NameOf(ArmLayerSubsystemBase.TimerInterval)
                If subsystem.TimerInterval.HasValue Then Me._TimerIntervalNumeric.Value = CDec(0.001 * subsystem.TimerInterval.Value.TotalMilliseconds)

        End Select
    End Sub

    ''' <summary> Arm layer subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ArmLayerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.ArmLayerSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ArmLayerSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ArmLayerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.ArmLayerSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As ArmLayerSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryArmCount()
        subsystem.QueryInputLineNumber()
        subsystem.QueryOutputLineNumber()
        subsystem.QueryArmSource()
        subsystem.QueryArmLayerBypassMode()
        If subsystem.LayerNumber = 2 Then
            subsystem.QueryDelay()
            subsystem.QueryTimerTimeSpan()
        End If
        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Applies the menu ite click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplyMenuIte_Click(sender As Object, e As EventArgs) Handles _ApplyMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads menu ite click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadMenuIte_Click(sender As Object, e As EventArgs) Handles _ReadMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Bypass toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BypassToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _BypassToggleButton.CheckStateChanged
        Me._BypassToggleButton.Text = Me._BypassToggleButton.CheckState.ToCheckStateCaption("Bypass", "~Bypass", "Bypass?")
    End Sub

    ''' <summary> Infinite count button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InfiniteCountButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _InfiniteCountButton.CheckStateChanged
        Me._InfiniteCountButton.Text = Me._InfiniteCountButton.CheckState.ToCheckStateCaption("Infinite", "Finite", "Infinite?")
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
