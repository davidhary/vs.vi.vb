﻿''' <summary> Information about the version. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-09-21 </para>
''' </remarks>
Public Class VersionInfo
    Inherits isr.VI.VersionInfoBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me.ParseFirmwareRevision("")
    End Sub

    ''' <summary> Parses the instrument identity string. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> Specifies the instrument identity string, which includes at a minimum the
    '''                      following information: e.g., <para> <c>???</c>.</para><para>
    '''                      </para>
    '''                      <see cref="ManufacturerName">manufacturer</see>,
    '''                      <see cref="Model">model</see>,
    '''                      <see cref="SerialNumber">serial number</see>,. </param>
    Public Overrides Sub Parse(value As String)

        ' clear
        MyBase.Parse("")

        ' save the identity.
        Me.Identity = value

        If Not String.IsNullOrWhiteSpace(value) Then

            MyBase.Parse(value)

            ' parse thee firmware revision
            Me.ParseFirmwareRevision("")

        End If

    End Sub

End Class
