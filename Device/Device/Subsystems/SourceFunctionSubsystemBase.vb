''' <summary>
''' Defines the contract that must be implemented by a Source Function Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SourceFunctionSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceFunctionSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefaultFunctionRange = VI.Pith.Ranges.NonnegativeFullRange
        Me.DefaultFunctionModeDecimalPlaces = 3
        Me._Amount = New Arebis.TypedUnits.Amount(0, Me.DefaultFunctionUnit)
        Me.FunctionUnit = Me.DefaultFunctionUnit
        Me.FunctionRange = Me.DefaultFunctionRange
        Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
        Me.DefineFunctionModeDecimalPlaces()
        Me.DefineFunctionModeReadWrites()
        Me.DefineFunctionModeRanges()
        Me.DefineFunctionModeUnits()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the clear execution state (CLS) by setting system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.Level = New Double?
        Me.DefineFunctionClearKnownState()
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Amount = New Arebis.TypedUnits.Amount(0, Me.DefaultFunctionUnit)
        Me.FunctionUnit = Me.DefaultFunctionUnit
        Me._FunctionRange = Me.DefaultFunctionRange
        Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
        Me.AutoClearEnabled = False
        Me.AutoDelayEnabled = True
        Me.Delay = TimeSpan.Zero
        Me.SweepPoints = 2500
    End Sub

#End Region

#Region " AUTO CLEAR ENABLED "

    ''' <summary> The automatic clear enabled. </summary>
    Private _AutoClearEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Clear Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Clear Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoClearEnabled As Boolean?
        Get
            Return Me._AutoClearEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoClearEnabled, value) Then
                Me._AutoClearEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Clear Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoClearEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoClearEnabled(value)
        Return Me.QueryAutoClearEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Clear enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:CLE:AUTO?". </remarks>
    ''' <value> The automatic Clear enabled query command. </value>
    Protected Overridable Property AutoClearEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Clear Enabled sentinel. Also sets the
    ''' <see cref="AutoClearEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoClearEnabled() As Boolean?
        Me.AutoClearEnabled = Me.Query(Me.AutoClearEnabled, Me.AutoClearEnabledQueryCommand)
        Return Me.AutoClearEnabled
    End Function

    ''' <summary> Gets or sets the automatic Clear enabled command Format. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Clear enabled query command. </value>
    Protected Overridable Property AutoClearEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Clear Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoClearEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoClearEnabled = Me.Write(value, Me.AutoClearEnabledCommandFormat)
        Return Me.AutoClearEnabled
    End Function

#End Region

#Region " AUTO DELAY ENABLED "

    ''' <summary> The automatic delay enabled. </summary>
    Private _AutoDelayEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Delay Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Delay Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoDelayEnabled As Boolean?
        Get
            Return Me._AutoDelayEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoDelayEnabled, value) Then
                Me._AutoDelayEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Delay Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoDelayEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoDelayEnabled(value)
        Return Me.QueryAutoDelayEnabled()
    End Function

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:DEL:AUTO?". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property AutoDelayEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Enabled sentinel. Also sets the
    ''' <see cref="AutoDelayEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoDelayEnabled() As Boolean?
        Me.AutoDelayEnabled = Me.Query(Me.AutoDelayEnabled, Me.AutoDelayEnabledQueryCommand)
        Return Me.AutoDelayEnabled
    End Function

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:DEL:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overridable Property AutoDelayEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoDelayEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoDelayEnabled = Me.Write(value, Me.AutoDelayEnabledCommandFormat)
        Return Me.AutoDelayEnabled
    End Function

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Auto Range enabled. </summary>
    Private _AutoRangeEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoRangeEnabled As Boolean?
        Get
            Return Me._AutoRangeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoRangeEnabled, value) Then
                Me._AutoRangeEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoRangeEnabled(value)
        Return Me.QueryAutoRangeEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <remarks> SCPI: "SOUR:RANG:AUTO?". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overridable Property AutoRangeEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Range Enabled sentinel. Also sets the
    ''' <see cref="AutoRangeEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoRangeEnabled() As Boolean?
        Me.AutoRangeEnabled = Me.Query(Me.AutoRangeEnabled, Me.AutoRangeEnabledQueryCommand)
        Return Me.AutoRangeEnabled
    End Function

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <remarks> SCPI: "SOUR:RANGE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overridable Property AutoRangeEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoRangeEnabled = Me.Write(value, Me.AutoRangeEnabledCommandFormat)
        Return Me.AutoRangeEnabled
    End Function

#End Region

#Region " DELAY "

    ''' <summary> The delay. </summary>
    Private _Delay As TimeSpan?

    ''' <summary> Gets or sets the cached Source Delay. </summary>
    ''' <remarks>
    ''' The delay is used to delay operation in the Source layer. After the programmed Source event
    ''' occurs, the instrument waits until the delay period expires before performing the Device
    ''' Action.
    ''' </remarks>
    ''' <value> The Source Delay or none if not set or unknown. </value>
    Public Overloads Property Delay As TimeSpan?
        Get
            Return Me._Delay
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Nullable.Equals(Me.Delay, value) Then
                Me._Delay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Source Delay. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Source Delay or none if unknown. </returns>
    Public Function ApplyDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteDelay(value)
        Me.QueryDelay()
    End Function

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:DEL?". </remarks>
    ''' <value> The delay query command. </value>
    Protected Overridable Property DelayQueryCommand As String

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Delay query command. </value>
    Protected Overridable Property DelayFormat As String

    ''' <summary> Queries the Delay. </summary>
    ''' <returns> The Delay or none if unknown. </returns>
    Public Function QueryDelay() As TimeSpan?
        Me.Delay = Me.Query(Me.Delay, Me.DelayFormat, Me.DelayQueryCommand)
        Return Me.Delay
    End Function

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:DEL {0:s\.FFFFFFF}". </remarks>
    ''' <value> The delay command format. </value>
    Protected Overridable Property DelayCommandFormat As String

    ''' <summary> Writes the Source Delay without reading back the value from the device. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Source Delay or none if unknown. </returns>
    Public Function WriteDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.Delay = Me.Write(value, Me.DelayCommandFormat)
        Return Me.Delay
    End Function

#End Region

#Region " LEVEL "

    ''' <summary> The amount. </summary>
    Private _Amount As Arebis.TypedUnits.Amount

    ''' <summary> Gets or sets the amount. </summary>
    ''' <value> The amount. </value>
    Public Property Amount As Arebis.TypedUnits.Amount
        Get
            Return Me._Amount
        End Get
        Set(value As Arebis.TypedUnits.Amount)
            Me._Amount = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Creates a new amount. </summary>
    ''' <param name="unit"> The unit. </param>
    Private Sub NewAmount(ByVal unit As Arebis.TypedUnits.Unit)
        If Me.Level.HasValue Then
            Me._Amount = New Arebis.TypedUnits.Amount(Me.Level.Value, unit)
            Me.LevelCaption = $"{Me.Amount} {Me.Amount.Unit}"
        Else
            Me._Amount = New Arebis.TypedUnits.Amount(0, unit)
            Me.LevelCaption = ($"-.---- {Me.Amount.Unit}")
        End If
        Me.NotifyPropertyChanged(NameOf(SourceSubsystemBase.Amount))
        Me.NotifyPropertyChanged(NameOf(SourceSubsystemBase.FunctionUnit))
    End Sub

    ''' <summary> The level caption. </summary>
    Private _LevelCaption As String

    ''' <summary> Gets or sets the Level caption. </summary>
    ''' <value> The Level caption. </value>
    Public Property LevelCaption As String
        Get
            Return Me._LevelCaption
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LevelCaption, StringComparison.OrdinalIgnoreCase) Then
                Me._LevelCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The level. </summary>
    Private _Level As Double?

    ''' <summary> Gets or sets the cached Source Current Level. </summary>
    ''' <value> The Source Current Level. Actual current depends on the power supply mode. </value>
    Public Overloads Property Level As Double?
        Get
            Return Me._Level
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Level, value) Then
                Me._Level = value
                Me.NewAmount(Me.FunctionUnit)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source current level. </summary>
    ''' <remarks>
    ''' This command set the immediate output current level. The value is in Amperes. The immediate
    ''' level is the output current setting. At *RST, the current values = 0.
    ''' </remarks>
    ''' <param name="value"> The current level. </param>
    ''' <returns> The Source Current Level. </returns>
    Public Function ApplyLevel(ByVal value As Double) As Double?
        Me.WriteLevel(value)
        Return Me.QueryLevel
    End Function

    ''' <summary> Gets or sets The Level query command. </summary>
    ''' <value> The Level query command. </value>
    Protected Overridable Property LevelQueryCommand As String

    ''' <summary> Queries the current level. </summary>
    ''' <returns> The current level or none if unknown. </returns>
    Public Function QueryLevel() As Double?
        Me.Level = Me.Query(Me.Level.GetValueOrDefault(0), Me.LevelQueryCommand)
        Return Me.Level
    End Function

    ''' <summary> Gets or sets The Level command format. </summary>
    ''' <value> The Level command format. </value>
    Protected Overridable Property LevelCommandFormat As String

    ''' <summary>
    ''' Writes the source current level without reading back the value from the device.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the immediate output current level. The value is in Amperes. The immediate
    ''' level is the output current setting. At *RST, the current values = 0.
    ''' </remarks>
    ''' <param name="value"> The current level. </param>
    ''' <returns> The Source Current Level. </returns>
    Public Function WriteLevel(ByVal value As Double) As Double?
        Me.Session.WriteLine(Me.LevelCommandFormat, value)
        Me.Level = value
        Return Me.Level
    End Function

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> The protection level. </summary>
    Private _ProtectionLevel As Double?

    ''' <summary> Gets or sets the cached Over Voltage Protection Level. </summary>
    ''' <remarks>
    ''' This command sets the over-voltage protection (OVP) level of the output. The values are
    ''' programmed in volts. If the output voltage exceeds the OVP level, the output is disabled and
    ''' OVP is set in the Questionable Condition status register. The*RST value = Max.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property ProtectionLevel As Double?
        Get
            Return Me._ProtectionLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.ProtectionLevel, value) Then
                Me._ProtectionLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the protection level. </summary>
    ''' <param name="value"> the protection level. </param>
    ''' <returns> the protection level. </returns>
    Public Function ApplyProtectionLevel(ByVal value As Double) As Double?
        Me.WriteProtectionLevel(value)
        Return Me.QueryProtectionLevel
    End Function

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overridable Property ProtectionLevelQueryCommand As String

    ''' <summary> Queries the protection level. </summary>
    ''' <returns> the protection level or none if unknown. </returns>
    Public Function QueryProtectionLevel() As Double?
        Me.ProtectionLevel = Me.Query(Me.ProtectionLevel, Me.ProtectionLevelQueryCommand)
        Return Me.ProtectionLevel
    End Function

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overridable Property ProtectionLevelCommandFormat As String

    ''' <summary>
    ''' Writes the protection level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets the protection level. </remarks>
    ''' <param name="value"> the protection level. </param>
    ''' <returns> the protection level. </returns>
    Public Function WriteProtectionLevel(ByVal value As Double) As Double?
        Me.ProtectionLevel = Me.Write(value, Me.ProtectionLevelCommandFormat)
        Return Me.ProtectionLevel
    End Function

#End Region

#Region " RANGE "

    ''' <summary> The range. </summary>
    Private _Range As Double?

    ''' <summary>
    ''' Gets or sets the cached range. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Range As Double?
        Get
            Return Me._Range
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Range, value) Then
                Me._Range = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the range. </summary>
    ''' <param name="value"> The range. </param>
    ''' <returns> The range. </returns>
    Public Function ApplyRange(ByVal value As Double) As Double?
        Me.WriteRange(value)
        Return Me.QueryRange
    End Function

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overridable Property RangeQueryCommand As String

    ''' <summary> Queries the range. </summary>
    ''' <returns> The range or none if unknown. </returns>
    Public Function QueryRange() As Double?
        Me.Range = Me.Query(Me.Range, Me.RangeQueryCommand)
        Return Me.Range
    End Function

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overridable Property RangeCommandFormat As String

    ''' <summary> Writes the range without reading back the value from the device. </summary>
    ''' <remarks> This command sets the range. </remarks>
    ''' <param name="value"> The range. </param>
    ''' <returns> The range. </returns>
    Public Function WriteRange(ByVal value As Double) As Double?
        Me.Range = Me.Write(value, Me.RangeCommandFormat)
        Return Me.Range
    End Function

#End Region

#Region " SWEEP START LEVEL "

    ''' <summary> The Sweep Start Level. </summary>
    Private _SweepStartLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Sweep Start Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property SweepStartLevel As Double?
        Get
            Return Me._SweepStartLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.SweepStartLevel, value) Then
                Me._SweepStartLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Start Level. </summary>
    ''' <param name="value"> The Sweep Start Level. </param>
    ''' <returns> The Sweep Start Level. </returns>
    Public Function ApplySweepStartLevel(ByVal value As Double) As Double?
        Me.WriteSweepStartLevel(value)
        Return Me.QuerySweepStartLevel
    End Function

    ''' <summary> Gets or sets the Sweep Start Level query command. </summary>
    ''' <value> The Sweep Start Level query command. </value>
    Protected Overridable Property SweepStartLevelQueryCommand As String

    ''' <summary> Queries the Sweep Start Level. </summary>
    ''' <returns> The Sweep Start Level or none if unknown. </returns>
    Public Function QuerySweepStartLevel() As Double?
        Me.SweepStartLevel = Me.Query(Me.SweepStartLevel, Me.SweepStartLevelQueryCommand)
        Return Me.SweepStartLevel
    End Function

    ''' <summary> Gets or sets the Sweep Start Level command format. </summary>
    ''' <value> The Sweep Start Level command format. </value>
    Protected Overridable Property SweepStartLevelCommandFormat As String

    ''' <summary>
    ''' Writes the Sweep Start Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets the Sweep Start Level. </remarks>
    ''' <param name="value"> The Sweep Start Level. </param>
    ''' <returns> The Sweep Start Level. </returns>
    Public Function WriteSweepStartLevel(ByVal value As Double) As Double?
        Me.SweepStartLevel = Me.Write(value, Me.SweepStartLevelCommandFormat)
        Return Me.SweepStartLevel
    End Function

#End Region

#Region " SWEEP STOP LEVEL "

    ''' <summary> The Sweep Stop Level. </summary>
    Private _SweepStopLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Sweep Stop Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property SweepStopLevel As Double?
        Get
            Return Me._SweepStopLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.SweepStopLevel, value) Then
                Me._SweepStopLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Stop Level. </summary>
    ''' <param name="value"> The Sweep Stop Level. </param>
    ''' <returns> The Sweep Stop Level. </returns>
    Public Function ApplySweepStopLevel(ByVal value As Double) As Double?
        Me.WriteSweepStopLevel(value)
        Return Me.QuerySweepStopLevel
    End Function

    ''' <summary> Gets or sets the Sweep Stop Level query command. </summary>
    ''' <value> The Sweep Stop Level query command. </value>
    Protected Overridable Property SweepStopLevelQueryCommand As String

    ''' <summary> Queries the Sweep Stop Level. </summary>
    ''' <returns> The Sweep Stop Level or none if unknown. </returns>
    Public Function QuerySweepStopLevel() As Double?
        Me.SweepStopLevel = Me.Query(Me.SweepStopLevel, Me.SweepStopLevelQueryCommand)
        Return Me.SweepStopLevel
    End Function

    ''' <summary> Gets or sets the Sweep Stop Level command format. </summary>
    ''' <value> The Sweep Stop Level command format. </value>
    Protected Overridable Property SweepStopLevelCommandFormat As String

    ''' <summary>
    ''' Writes the Sweep Stop Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets the Sweep Stop Level. </remarks>
    ''' <param name="value"> The Sweep Stop Level. </param>
    ''' <returns> The Sweep Stop Level. </returns>
    Public Function WriteSweepStopLevel(ByVal value As Double) As Double?
        Me.SweepStopLevel = Me.Write(value, Me.SweepStopLevelCommandFormat)
        Return Me.SweepStopLevel
    End Function

#End Region

#Region " SWEEP MODE  "

    ''' <summary> The Sweep Mode. </summary>
    Private _SweepMode As SweepMode?

    ''' <summary> Gets or sets the cached Sweep Mode. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property SweepMode As SweepMode?
        Get
            Return Me._SweepMode
        End Get
        Protected Set(ByVal value As SweepMode?)
            If Not Nullable.Equals(Me.SweepMode, value) Then
                Me._SweepMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Mode. </summary>
    ''' <param name="value"> The Sweep Mode. </param>
    ''' <returns> The Sweep Mode. </returns>
    Public Function ApplySweepMode(ByVal value As SweepMode) As SweepMode?
        Me.WriteSweepMode(value)
        Return Me.QuerySweepMode
    End Function

    ''' <summary> Gets or sets the Sweep Mode  query command. </summary>
    ''' <value> The Sweep Mode  query command. </value>
    Protected Overridable Property SweepModeQueryCommand As String

    ''' <summary> Queries the Sweep Mode. </summary>
    ''' <returns> The Sweep Mode  or none if unknown. </returns>
    Public Function QuerySweepMode() As SweepMode?
        Me.SweepMode = Me.Query(Me.SweepModeQueryCommand, Me.SweepMode)
        Return Me.SweepMode
    End Function

    ''' <summary> Gets or sets the Sweep Mode  command format. </summary>
    ''' <value> The Sweep Mode  command format. </value>
    Protected Overridable Property SweepModeCommandFormat As String

    ''' <summary> Writes the Sweep Mode  without reading back the value from the device. </summary>
    ''' <remarks> This command sets the Sweep Mode. </remarks>
    ''' <param name="value"> The Sweep Mode. </param>
    ''' <returns> The Sweep Mode. </returns>
    Public Function WriteSweepMode(ByVal value As SweepMode) As SweepMode?
        Me.SweepMode = Me.Write(Me.SweepModeCommandFormat, value)
        Return Me.SweepMode
    End Function

#End Region

#Region " SWEEP POINTS "

    ''' <summary> The sweep point. </summary>
    Private _SweepPoint As Integer?

    ''' <summary> Gets or sets the cached Sweep Points. </summary>
    ''' <value> The Sweep Points or none if not set or unknown. </value>
    Public Overloads Property SweepPoints As Integer?
        Get
            Return Me._SweepPoint
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.SweepPoints, value) Then
                Me._SweepPoint = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sweep Points. </summary>
    ''' <param name="value"> The current SweepPoints. </param>
    ''' <returns> The SweepPoints or none if unknown. </returns>
    Public Function ApplySweepPoints(ByVal value As Integer) As Integer?
        Me.WriteSweepPoints(value)
        Return Me.QuerySweepPoints()
    End Function

    ''' <summary> Gets or sets Sweep Points query command. </summary>
    ''' <remarks> SCPI: ":SOUR&lt;ch&gt;:SWE:POIN?". </remarks>
    ''' <value> The Sweep Points query command. </value>
    Protected Overridable Property SweepPointsQueryCommand As String

    ''' <summary> Queries the current Sweep Points. </summary>
    ''' <returns> The Sweep Points or none if unknown. </returns>
    Public Function QuerySweepPoints() As Integer?
        If Not String.IsNullOrWhiteSpace(Me.SweepPointsQueryCommand) Then
            Me.SweepPoints = Me.Session.Query(0I, Me.SweepPointsQueryCommand)
        End If
        Return Me.SweepPoints
    End Function

    ''' <summary> Gets or sets Sweep Points command format. </summary>
    ''' <remarks> SCPI: "SOUR&lt;ch&gt;:SWE:POIN {0}". </remarks>
    ''' <value> The Sweep Points command format. </value>
    Protected Overridable Property SweepPointsCommandFormat As String

    ''' <summary> Write the Sweep Points without reading back the value from the device. </summary>
    ''' <param name="value"> The current Sweep Points. </param>
    ''' <returns> The Sweep Points or none if unknown. </returns>
    Public Function WriteSweepPoints(ByVal value As Integer) As Integer?
        If Not String.IsNullOrWhiteSpace(Me.SweepPointsCommandFormat) Then
            Me.Session.WriteLine(Me.SweepPointsCommandFormat, value)
        End If
        Me.SweepPoints = value
        Return Me.SweepPoints
    End Function

#End Region

End Class

''' <summary> Specifies the source sweep modes. </summary>
Public Enum SweepMode

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None

    ''' <summary> An enum constant representing the fixed option. </summary>
    <ComponentModel.Description("Fixed (FIX)")>
    Fixed

    ''' <summary> An enum constant representing the sweep option. </summary>
    <ComponentModel.Description("Sweep (SWE)")>
    Sweep

    ''' <summary> An enum constant representing the list option. </summary>
    <ComponentModel.Description("List (LIST)")>
    List
End Enum

