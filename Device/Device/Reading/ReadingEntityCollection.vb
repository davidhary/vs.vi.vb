''' <summary>
''' Defines the collection of <see cref="ReadingEntity">reading elements</see>.
''' </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-11-02 </para>
''' </remarks>
Public Class ReadingEntityCollection
    Inherits System.Collections.ObjectModel.KeyedCollection(Of ReadingElementTypes, ReadingEntity)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ReadingEntity) As ReadingElementTypes
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.ReadingType
    End Function

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.Elements = VI.ReadingElementTypes.None
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As ReadingEntityCollection)
        Me.New
        If model IsNot Nothing Then
            For Each entity As ReadingEntity In model
                Me.Add(entity)
            Next
        End If
    End Sub

    ''' <summary> Gets a list of types of the elements. </summary>
    ''' <value> A list of types of the elements. </value>
    Public ReadOnly Property ElementTypes As IEnumerable(Of ReadingElementTypes)
        Get
            Return If(Me.Dictionary Is Nothing, New ReadingElementTypes() {ReadingElementTypes.None}, Me.Dictionary.Keys)
        End Get
    End Property

    ''' <summary> Gets the reading entities. </summary>
    ''' <value> The reading entities. </value>
    Public ReadOnly Property ReadingEntities As IEnumerable(Of ReadingEntity)
        Get
            Return If(Me.Dictionary Is Nothing, Array.Empty(Of ReadingEntity), Me.Dictionary.Values)
        End Get
    End Property

    ''' <summary> Gets or sets the reading elements. </summary>
    ''' <value> The elements. </value>
    Public ReadOnly Property Elements() As VI.ReadingElementTypes

    ''' <summary> Adds an item. </summary>
    ''' <param name="item"> The item. </param>
    Public Shadows Sub Add(ByVal item As ReadingEntity)
        If item Is Nothing Then Return
        MyBase.Add(item)
        ' add the length of the delimiter.
        If Me.ReadingsLength > 0 Then Me._ReadingsLength += 1
        Me._ReadingsLength += item.ReadingLength
        Me._Elements = Me.Elements Or item.ReadingType
    End Sub

    ''' <summary> Adds if reading entity type is included in the mask. </summary>
    ''' <param name="mask"> The mask. </param>
    ''' <param name="item"> The item. </param>
    Public Sub AddIf(ByVal mask As ReadingElementTypes, ByVal item As ReadingEntity)
        If item IsNot Nothing AndAlso (mask And item.ReadingType) <> 0 Then Me.Add(item)
    End Sub

    ''' <summary> Include unit suffix if. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub IncludeUnitSuffixIf(ByVal value As ReadingElementTypes)
        ' Units is a property of each element. If units are turned on, each element units is enabled.
        If (value And isr.VI.ReadingElementTypes.Units) <> 0 Then
            For Each e As ReadingEntity In Me
                e.IncludesUnitsSuffix = True
            Next
        End If
    End Sub

    ''' <summary> Returns the total length of the reading elements including delimiters. </summary>
    ''' <value> The length of the elements. </value>
    Public ReadOnly Property ReadingsLength() As Integer

    ''' <summary> Resets all values to null. </summary>
    Public Sub Reset()
        For Each r As ReadingEntity In Me
            r.Reset()
        Next
    End Sub

    ''' <summary> Gets the list of raw readings. </summary>
    ''' <returns> A list of raw readings. </returns>
    Public Function ToRawReadings() As String()
        Dim values As New List(Of String)
        For Each readingItem As ReadingAmount In Me
            values.Add(readingItem.RawValueReading)
        Next
        Return values.ToArray
    End Function

    ''' <summary> Returns the meta status or new if does not exist. </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    ''' <returns> The MetaStatus. </returns>
    Public Function MetaStatus(ByVal readingType As ReadingElementTypes) As MetaStatus
        Dim result As MetaStatus = New MetaStatus
        If Me.Contains(readingType) Then
            Dim amount As MeasuredAmount = TryCast(Me(readingType), MeasuredAmount)
            If amount IsNot Nothing Then result = amount.MetaStatus
        End If
        Return result
    End Function

    ''' <summary> Reading amount. </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    ''' <returns> A ReadingAmount. </returns>
    Public Function ReadingAmount(ByVal readingType As ReadingElementTypes) As ReadingAmount
        Dim result As ReadingAmount = Nothing
        If Me.Contains(readingType) Then
            result = TryCast(Me(readingType), ReadingAmount)
        End If
        If result Is Nothing Then result = New ReadingAmount(readingType, Arebis.StandardUnits.ElectricUnits.Volt)
        Return result
    End Function

    ''' <summary> Reading value. </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    ''' <returns> A ReadingValue. </returns>
    Public Function ReadingValue(ByVal readingType As ReadingElementTypes) As ReadingValue
        Dim result As ReadingValue = Nothing
        If Me.Contains(readingType) Then
            result = TryCast(Me(readingType), ReadingValue)
        End If
        If result Is Nothing Then result = New ReadingValue(readingType)
        Return result
    End Function

    ''' <summary> Reading status. </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    ''' <returns> The ReadingStatus. </returns>
    Public Function ReadingStatus(ByVal readingType As ReadingElementTypes) As ReadingStatus
        Dim result As ReadingStatus = Nothing
        If Me.Contains(readingType) Then
            result = TryCast(Me(readingType), ReadingStatus)
        End If
        If result Is Nothing Then result = New ReadingStatus(readingType)
        Return result
    End Function

End Class
