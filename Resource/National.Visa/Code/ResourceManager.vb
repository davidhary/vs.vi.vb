'---------------------------------------------------------------------------------------------------
' file:		National.Visa\Code\ResourceManager.vb
'
' summary:	Resource manager class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Foundation
Imports isr.VI.National.Visa.ResourceManagerExtensions

''' <summary> A VISA Resource Manager. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para>
''' </remarks>
Public Class ResourceManager
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Public Sub New()
        MyBase.New
        Me._VisaResourceManager = New NationalInstruments.Visa.ResourceManager
    End Sub

#Region " IDisposable Support "

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean ' To detect redundant calls


    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me._Disposed AndAlso disposing Then
                If Me._VisaResourceManager IsNot Nothing Then Me._VisaResourceManager.Dispose() : Me._VisaResourceManager = Nothing
            End If
        Finally
            Me._Disposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' David, 2015-11-23. Override Finalize() only if Dispose(disposing As Boolean) above has code
    ''' to free unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncommented the following line because Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#End Region

#Region " RESOURCE MANAGER "

    ''' <summary> Gets the manager for resource. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <value> The resource manager. </value>
    Private ReadOnly Property VisaResourceManager As NationalInstruments.Visa.ResourceManager

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Parse resource. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A VI.ResourceNameInfo. </returns>
    Public Function ParseResource(ByVal resourceName As String) As VI.Pith.ResourceNameInfo
        Dim activity As String = $"parsing resource {resourceName}"
        Try
            Return Me.VisaResourceManager.ParseResource(resourceName)
        Catch ex As System.MissingMethodException
            Throw New isr.Core.OperationFailedException($"Failed {activity}; Please verify using IVI.Visa version {My.Settings.FoundationVisaAssemblyVersion} with NI VISA {My.Settings.FoundationSystemFileVersion64}", ex)
        Catch
            Throw
        End Try
    End Function

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources. </summary>
    ''' <returns> List of all resources. </returns>
    Public Function FindResources() As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindResources()
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if resources were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindResources(ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindResources(resources)
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <param name="filter">    A pattern specifying the search. </param>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if resources were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindResources(ByVal filter As String, ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindResources(filter, resources)
    End Function

    ''' <summary> Returns true if the specified resource exists. </summary>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    Public Function Exists(ByVal resourceName As String) As Boolean
        Return Me.VisaResourceManager.Exists(resourceName)
    End Function

    ''' <summary> Searches for the interface. </summary>
    ''' <param name="resourceName"> The interface resource name. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    Public Function InterfaceExists(ByVal resourceName As String) As Boolean
        Return Me.VisaResourceManager.InterfaceExists(resourceName)
    End Function

    ''' <summary> Searches for all interfaces. </summary>
    ''' <returns> The found interface resource names. </returns>
    Public Function FindInterfaces() As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindInterfaces()
    End Function

    ''' <summary> Tries to find interfaces. </summary>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindInterfaces(ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindInterfaces(resources)
    End Function

    ''' <summary> Searches for the interfaces. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindInterfaces(interfaceType)
    End Function

    ''' <summary> Tries to find interfaces. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindInterfaces(interfaceType, resources)
    End Function

    ''' <summary> Searches for the instrument. </summary>
    ''' <param name="resourceName"> The instrument resource name. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    Public Function FindInstrument(ByVal resourceName As String) As Boolean
        Return Me.VisaResourceManager.InstrumentExists(resourceName)
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <returns> The found instrument resource names. </returns>
    Public Function FindInstruments() As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindInstruments()
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindInstruments(ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindInstruments(resources)
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindInstruments(interfaceType)
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                                  ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindInstruments(interfaceType, resources)
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)
        Return Me.VisaResourceManager.FindInstruments(interfaceType, boardNumber)
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
    ''' <param name="resources">       [in,out] The resources. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>.
    ''' </returns>
    Public Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                                  ByVal interfaceNumber As Integer, ByRef resources As IEnumerable(Of String)) As Boolean
        Return Me.VisaResourceManager.TryFindInstruments(interfaceType, interfaceNumber, resources)
    End Function

#End Region

#Region " REGISTERY VISA VERSION "

    ''' <summary> Gets the full pathname of the vendor version file. </summary>
    ''' <value> The full pathname of the vendor version file. </value>
    Public Shared Property VendorVersionPath As String = "SOFTWARE\National Instruments\NI-VISA\"

    ''' <summary> Gets the name of the vendor version key. </summary>
    ''' <value> The name of the vendor version key. </value>
    Public Shared Property VendorVersionKeyName As String = "CurrentVersion"

    ''' <summary> Reads vendor version. </summary>
    ''' <returns> The vendor version. </returns>
    Public Shared Function ReadVendorVersion() As Version
        Dim version As String = isr.Core.MachineInfo.ReadRegistry(Microsoft.Win32.RegistryHive.LocalMachine, VendorVersionPath, VendorVersionKeyName, "0.0.0.0")
        Return New Version(version)
    End Function

    ''' <summary> Validates the vendor version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVendorVersion(ByVal expectedVersion As String) As (Success As Boolean, Details As String)
        Dim actualVersion As String = ResourceManager.ReadVendorVersion.ToString
        Return If(isr.VI.Pith.ResourcesProviderBase.AreEqual(expectedVersion, actualVersion),
            (True, String.Empty),
            (False, $"Vendor {ResourceManager.VendorVersionPath} version {actualVersion} different from expected {expectedVersion}"))
    End Function

    ''' <summary> Validates the National Instruments vendor version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVendorVersion() As (Success As Boolean, Details As String)
        Return ResourceManager.ValidateVendorVersion(ResourceManager.ExpectedVendorVersion)
    End Function

    ''' <summary> Gets the expected vendor version. </summary>
    ''' <value> The expected vendor version. </value>
    Public Shared ReadOnly Property ExpectedVendorVersion As String
        Get
            Return My.Settings.NationalRegisteredVisaVersion
        End Get
    End Property

    ''' <summary>
    ''' Attempts to validate visa vendor and assembly versions using settings versions.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaAssemblyVersions() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = ResourceManager.ValidateVendorVersion()
        If result.Success Then result = VisaVersionValidator.ValidateVisaAssemblyVersions()
        Return result
    End Function

    ''' <summary>
    ''' Validates the implementation specification visa versions against settings values.
    ''' </summary>
    ''' <remarks> David, 2020-04-12. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateFunctionalVisaVersions() As (Success As Boolean, Details As String)
        Return If(String.Equals(ResourceManager.ImplementationVersion.ToString, My.Settings.ImplementationVersion),
            If(String.Equals(ResourceManager.ImplementationVersion.ToString, My.Settings.ImplementationVersion),
                (True, String.Empty),
                (False, $"Expected specification version {My.Settings.SpecificationVersion} different from actual {ResourceManager.SpecificationVersion}")),
            (False, $"Expected implementation version {My.Settings.ImplementationVersion} different from actual {ResourceManager.ImplementationVersion}"))
    End Function

    ''' <summary> Gets the implementation version. </summary>
    ''' <value> The implementation version. </value>
    Public Shared ReadOnly Property ImplementationVersion As Version
        Get
            Using rm As New NationalInstruments.Visa.ResourceManager
                Return rm.ImplementationVersion
            End Using
        End Get
    End Property

    ''' <summary> Gets the specification version. </summary>
    ''' <value> The specification version. </value>
    Public Shared ReadOnly Property SpecificationVersion As Version
        Get
            Using rm As New NationalInstruments.Visa.ResourceManager
                Return rm.SpecificationVersion
            End Using
        End Get
    End Property

#End Region

End Class
