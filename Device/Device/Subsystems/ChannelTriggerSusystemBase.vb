''' <summary> Defines the Calculate Channel SCPI subsystem. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public MustInherit Class ChannelTriggerSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ChannelTriggerSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="channelNumber">   The channel number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Protected Sub New(ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.ChannelNumber = channelNumber
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Gets or sets the channel number. </summary>
    ''' <value> The channel number. </value>
    Public ReadOnly Property ChannelNumber As Integer

#End Region

#Region " IMMEDIATE "

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <remarks> SCPI: ":INIT&lt;c#&gt;:IMM". </remarks>
    ''' <value> The initiate command. </value>
    Protected Overridable Property InitiateCommand As String

    ''' <summary>
    ''' Changes the state of the channel to the initiation state of the trigger system.
    ''' </summary>
    Public Sub Initiate()
        Me.Write(Me.InitiateCommand)
    End Sub

#End Region

#Region " Continuous ENABLED "

    ''' <summary> The continuous enabled. </summary>
    Private _ContinuousEnabled As Boolean?

    ''' <summary> Gets or sets the cached Continuous Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Continuous Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property ContinuousEnabled As Boolean?
        Get
            Return Me._ContinuousEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ContinuousEnabled, value) Then
                Me._ContinuousEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Continuous Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyContinuousEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteContinuousEnabled(value)
        Return Me.QueryContinuousEnabled()
    End Function

    ''' <summary> Gets or sets the continuous trigger enabled query command. </summary>
    ''' <remarks> SCPI: ":INIT&lt;c#&gt;:CONT?". </remarks>
    ''' <value> The continuous trigger enabled query command. </value>
    Protected Overridable Property ContinuousEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Continuous Enabled sentinel. Also sets the
    ''' <see cref="ContinuousEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryContinuousEnabled() As Boolean?
        Me.ContinuousEnabled = Me.Query(Me.ContinuousEnabled, Me.ContinuousEnabledQueryCommand)
        Return Me.ContinuousEnabled
    End Function

    ''' <summary> Gets or sets the continuous trigger enabled command Format. </summary>
    ''' <remarks> SCPI: ":INIT&lt;c#&gt;:CONT {0:1;1;0}". </remarks>
    ''' <value> The continuous trigger enabled query command. </value>
    Protected Overridable Property ContinuousEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Continuous Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteContinuousEnabled(ByVal value As Boolean) As Boolean?
        Me.ContinuousEnabled = Me.Write(value, Me.ContinuousEnabledCommandFormat)
        Return Me.ContinuousEnabled
    End Function

#End Region

End Class

