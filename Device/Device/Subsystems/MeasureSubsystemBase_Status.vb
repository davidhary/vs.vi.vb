'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\MeasureSubsystemBase_Status.vb
'
' summary:	Measure subsystem base status class
'---------------------------------------------------------------------------------------------------

Partial Public Class MeasureSubsystemBase

    ''' <summary> Information describing the failure short. </summary>
    Private _FailureShortDescription As String

    ''' <summary> Gets or sets the failure Short Description. </summary>
    ''' <value> The failure Short Description. </value>
    Public Property FailureShortDescription As String
        Get
            Return Me._FailureShortDescription
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FailureShortDescription, StringComparison.OrdinalIgnoreCase) Then
                Me._FailureShortDescription = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The failure color. </summary>
    Private _FailureColor As Drawing.Color

    ''' <summary> Gets or sets the color of the failure. </summary>
    ''' <value> The color of the failure. </value>
    Public Property FailureColor As Drawing.Color
        Get
            Return Me._FailureColor
        End Get
        Set(value As Drawing.Color)
            If Not Drawing.Color.Equals(value, Me.FailureCode) Then
                Me._FailureColor = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The failure code. </summary>
    Private _FailureCode As String

    ''' <summary> Gets or sets the failure Code. </summary>
    ''' <value> The failure Code. </value>
    Public Property FailureCode As String
        Get
            Return Me._FailureCode
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FailureCode, StringComparison.OrdinalIgnoreCase) Then
                Me._FailureCode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Information describing the failure long. </summary>
    Private _FailureLongDescription As String

    ''' <summary> Gets or sets the failure long description. </summary>
    ''' <value> The failure long description. </value>
    Public Property FailureLongDescription As String
        Get
            Return Me._FailureLongDescription
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FailureLongDescription, StringComparison.OrdinalIgnoreCase) Then
                Me._FailureLongDescription = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Notifies of failure information. </summary>
    Public Sub NotifyFailureInfo()
        Me.NotifyPropertyChanged(NameOf(MultimeterSubsystemBase.FailureLongDescription))
        Me.NotifyPropertyChanged(NameOf(MultimeterSubsystemBase.FailureShortDescription))
        Me.NotifyPropertyChanged(NameOf(MultimeterSubsystemBase.FailureCode))
    End Sub

    ''' <summary> Updates the meta status described by metaStatus. </summary>
    ''' <remarks> David, 2020-08-12. </remarks>
    ''' <param name="metaStatus"> The meta status. </param>
    Private Sub UpdateMetaStatus(ByVal metaStatus As MetaStatus)
        Dim failureColor As Drawing.Color = Drawing.Color.Black
        Dim failureCode As String = String.Empty
        Dim failureShortDescription As String = String.Empty
        Dim failureLongDescription As String = String.Empty
        If metaStatus.HasValue Then
            failureColor = metaStatus.ToColor
            failureCode = $"{metaStatus.TwoCharDescription(""),2}"
            failureShortDescription = $"{metaStatus.ToShortDescription(""),4}"
            failureLongDescription = metaStatus.ToLongDescription("")
        End If
        Me.FailureLongDescription = failureLongDescription
        Me.FailureShortDescription = failureShortDescription
        Me.FailureCode = failureCode
        Me.FailureColor = failureColor
    End Sub

    ''' <summary> Parse measured value. </summary>
    Private Sub ClearMetaStatus()
        Me.FailureCode = String.Empty
        Me.FailureShortDescription = String.Empty
        Me.FailureLongDescription = String.Empty
    End Sub

End Class

