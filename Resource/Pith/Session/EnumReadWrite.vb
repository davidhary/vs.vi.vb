'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\EnumReadWrite.vb
'
' summary:	Enum read write class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

''' <summary> Defines an enumerated value for reading and writing device messages. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-04-09 </para>
''' </remarks>
Public Class EnumReadWrite

    ''' <summary> Constructor. </summary>
    ''' <param name="enumValue">   The enum value. </param>
    ''' <param name="readValue">   The read value. </param>
    ''' <param name="writeValue">  The write value. </param>
    ''' <param name="description"> The description. </param>
    Public Sub New(ByVal enumValue As Long, ByVal readValue As String, ByVal writeValue As String, ByVal description As String)
        MyBase.New()
        Me.ReadValue = readValue
        Me.WriteValue = writeValue
        Me.EnumValue = enumValue
        Me.Description = description
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="enumValue">   The enum value. </param>
    ''' <param name="readValue">   The read value. </param>
    ''' <param name="description"> The description. </param>
    Public Sub New(enumValue As Integer, ByVal readValue As String, ByVal description As String)
        Me.New(enumValue, readValue, readValue, description)
    End Sub

    ''' <summary> Gets or sets the read value. </summary>
    ''' <value> The read value. </value>
    Public Property ReadValue As String

    ''' <summary> Gets or sets the enum value. </summary>
    ''' <value> The enum value. </value>
    Public Property EnumValue As Long

    ''' <summary> Gets or sets the description. </summary>
    ''' <value> The description. </value>
    Public Property Description As String

    ''' <summary> Gets or sets the write value. </summary>
    ''' <value> The write value. </value>
    Public Property WriteValue As String

End Class

''' <summary> Collection of enum reading and writing values. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-04-09 </para>
''' </remarks>
Public Class EnumReadWriteCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of Long, EnumReadWrite)


    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    Public Sub New()
        MyBase.New
        Me.ReadValueLookup = New ReadValueCollection
    End Sub


    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As EnumReadWrite) As Long
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.EnumValue
    End Function

    ''' <summary> Collection of read values. </summary>
    Private Class ReadValueCollection
        Inherits Collections.ObjectModel.KeyedCollection(Of String, EnumReadWrite)

        ''' <summary>
        ''' When implemented in a derived class, extracts the key from the specified element.
        ''' </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="item"> The element from which to extract the key. </param>
        ''' <returns> The key for the specified element. </returns>
        Protected Overrides Function GetKeyForItem(item As EnumReadWrite) As String
            If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
            Return item.ReadValue
        End Function
    End Class

    ''' <summary> Gets or sets the read value lookup. </summary>
    ''' <value> The read value lookup. </value>
    Private ReadOnly Property ReadValueLookup As ReadValueCollection


    ''' <summary>
    ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    Public Overloads Sub Clear()
        MyBase.Clear()
        Me.ReadValueLookup.Clear()
    End Sub

    ''' <summary> Select item by the <see cref="EnumReadWrite.ReadValue"/>. </summary>
    ''' <param name="readValue"> The code. </param>
    ''' <returns> A CodeValueDescription. </returns>
    Public Function SelectItem(ByVal readValue As String) As EnumReadWrite
        Return Me.ReadValueLookup(readValue)
    End Function


    ''' <summary>
    ''' Select item or default. This is required if Subsystem enum value is set at None whereas None
    ''' is not an instrument option.
    ''' </summary>
    ''' <param name="enumValue"> The value. </param>
    ''' <returns> An EnumReadWrite. </returns>
    Public Function SelectItemOrDefault(ByVal enumValue As Long) As EnumReadWrite
        Return If(Me.Contains(enumValue), Me.Item(enumValue), Me.Items.FirstOrDefault)
        Return Me.Item(enumValue)
    End Function

    ''' <summary> Select item by the <see cref="EnumReadWrite.EnumValue"/>. </summary>
    ''' <param name="enumValue"> The value. </param>
    ''' <returns> A CodeValueDescription. </returns>
    Public Function SelectItem(ByVal enumValue As Long) As EnumReadWrite
        Return Me.Item(enumValue)
    End Function

    ''' <summary> Determine if 'readValue' exists. </summary>
    ''' <param name="readValue"> The code. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function Exists(ByVal readValue As String) As Boolean
        Return Me.ReadValueLookup.Contains(readValue)
    End Function

    ''' <summary> Determine if 'enumValue' exists. </summary>
    ''' <param name="enumValue"> The value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function Exists(ByVal enumValue As Long) As Boolean
        Return Me.Contains(enumValue)
    End Function

    ''' <summary> Adds or replaces an item. </summary>
    ''' <param name="enumValue">   The value. </param>
    ''' <param name="readValue">   The code. </param>
    ''' <param name="description"> The description. </param>
    Public Overloads Sub Add(enumValue As Long, ByVal readValue As String, ByVal description As String)
        Me.AddReplace(enumValue, readValue, readValue, description)
    End Sub

    ''' <summary> Gets or sets the read value decorator. </summary>
    ''' <value> The read value decorator. </value>
    Public Property ReadValueDecorator As String = "{0}"

    ''' <summary> Gets or sets the write value decorator. </summary>
    ''' <value> The write value decorator. </value>
    Public Property WriteValueDecorator As String = "{0}"

    ''' <summary> Adds or replaces an item. </summary>
    ''' <param name="enumItem"> The enum item to add. </param>
    Public Overloads Sub Add(enumItem As System.Enum)
        Dim readValue As String = enumItem.ExtractBetween
        Dim writeValue As String = String.Format(Me.WriteValueDecorator, readValue)
        readValue = String.Format(Me.ReadValueDecorator, readValue)
        Dim description As String = enumItem.DescriptionUntil
        Dim enumValue As Long = Convert.ToInt64(enumItem, Globalization.CultureInfo.CurrentCulture)
        Me.AddReplace(enumValue, readValue, writeValue, description)
    End Sub

    ''' <summary> Adds or replaces an item. </summary>
    ''' <param name="enumValue">   The value. </param>
    ''' <param name="readValue">   The code. </param>
    ''' <param name="writeValue">  The write value. </param>
    ''' <param name="description"> The description. </param>
    Public Overloads Sub Add(enumValue As Long, ByVal readValue As String, ByVal writeValue As String, ByVal description As String)
        Me.AddReplace(enumValue, readValue, writeValue, description)
    End Sub

    ''' <summary> Adds or replaces an item. </summary>
    ''' <param name="enumValue">   The value. </param>
    ''' <param name="readValue">   The code. </param>
    ''' <param name="description"> The description. </param>
    Public Overloads Sub AddReplace(enumValue As Long, ByVal readValue As String, ByVal description As String)
        Me.AddReplace(enumValue, readValue, readValue, description)
    End Sub

    ''' <summary> Adds or replaces an item. </summary>
    ''' <param name="enumValue">   The value. </param>
    ''' <param name="readValue">   The code. </param>
    ''' <param name="writeValue">  The write value. </param>
    ''' <param name="description"> The description. </param>
    Public Overloads Sub AddReplace(enumValue As Long, ByVal readValue As String, ByVal writeValue As String, ByVal description As String)
        Me.AddReplace(New EnumReadWrite(enumValue, readValue, writeValue, description))
    End Sub

    ''' <summary> Adds or replaces an item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="enumReadWrite"> The enum read write. </param>
    Public Overloads Sub AddReplace(enumReadWrite As EnumReadWrite)
        If enumReadWrite Is Nothing Then Throw New ArgumentNullException(NameOf(enumReadWrite))
        Me.RemoveAt(enumReadWrite)
        MyBase.Add(enumReadWrite)
        Me.ReadValueLookup.Add(enumReadWrite)
    End Sub


    ''' <summary>
    ''' Attempts to remove an item if its <see cref="EnumReadWrite.ReadValue"/> or
    ''' <see cref="EnumReadWrite.EnumValue"/> exist.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="enumCodeValue"> The <see cref="EnumReadWrite"/> . </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function RemoveAt(ByVal enumCodeValue As EnumReadWrite) As Boolean
        If enumCodeValue Is Nothing Then Throw New ArgumentNullException(NameOf(enumCodeValue))
        Return Me.RemoveAt(enumCodeValue.ReadValue) OrElse Me.RemoveAt(enumCodeValue.EnumValue)
    End Function

    ''' <summary> Removes an item if its <see cref="EnumReadWrite.ReadValue"/> exists. </summary>
    ''' <param name="readValue"> The code. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function RemoveAt(ByVal readValue As String) As Boolean
        If Me.Exists(readValue) Then
            Dim value As EnumReadWrite = Me.SelectItem(readValue)
            Me.ReadValueLookup.Remove(readValue)
            If Me.Exists(value.EnumValue) Then Me.Remove(value.EnumValue)
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary> Remove an item if its <see cref="EnumReadWrite.EnumValue"/> exists. </summary>
    ''' <param name="enumValue"> The enum value to remove. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function RemoveAt(ByVal enumValue As Byte) As Boolean
        Return Me.RemoveAt(CLng(enumValue))
    End Function

    ''' <summary> Remove an item if its <see cref="EnumReadWrite.EnumValue"/> exists. </summary>
    ''' <param name="enumValue"> The enum value to remove. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function RemoveAt(ByVal enumValue As Short) As Boolean
        Return Me.RemoveAt(CLng(enumValue))
    End Function

    ''' <summary> Remove an item if its <see cref="EnumReadWrite.EnumValue"/> exists. </summary>
    ''' <param name="enumValue"> The value. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function RemoveAt(ByVal enumValue As Integer) As Boolean
        Return Me.RemoveAt(CLng(enumValue))
    End Function

    ''' <summary> Remove an item if its <see cref="EnumReadWrite.EnumValue"/> exists. </summary>
    ''' <param name="enumValue"> The enum value to remove. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overloads Function RemoveAt(ByVal enumValue As Long) As Boolean
        If Me.Exists(enumValue) Then
            Dim value As EnumReadWrite = Me.SelectItem(enumValue)
            Me.Remove(enumValue)
            If Me.Exists(value.ReadValue) Then Me.ReadValueLookup.Remove(value.ReadValue)
            Return True
        Else
            Return False
        End If
    End Function

End Class

