'---------------------------------------------------------------------------------------------------
' file:		Device\Subsystems\TraceSubsystem.vb
'
' summary:	Trace subsystem class
'---------------------------------------------------------------------------------------------------
Imports Arebis.StandardUnits

Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Defines a Trace Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TraceSubsystem
    Inherits VI.TraceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PointsCount = 10
        Me.FeedSource = VI.FeedSources.None
        Me.FeedControl = VI.FeedControls.Never
    End Sub

#End Region

#Region " MEMORY OPTION "

    ''' <summary> The buffer size standard full. </summary>
    Protected Const BufferSizeStandardFull As Integer = 404

    ''' <summary> The buffer size standard compact. </summary>
    Protected Const BufferSizeStandardCompact As Integer = 2027

    ''' <summary> The buffer size memory 1 full. </summary>
    Protected Const BufferSizeMemory1Full As Integer = 1381

    ''' <summary> The buffer size memory 1 compact. </summary>
    Protected Const BufferSizeMemory1Compact As Integer = 6909

    ''' <summary> The buffer size memory 2 full. </summary>
    Protected Const BufferSizeMemory2Full As Integer = 5780 ' Manual page 3-148 is incorrect at 5980

    ''' <summary> The buffer size memory 2 compact. </summary>
    Protected Const BufferSizeMemory2Compact As Integer = 29908

    ''' <summary> The memory option. </summary>
    Private _MemoryOption As Integer?

    ''' <summary> Gets or sets the memory Option. </summary>
    ''' <value> The memory Option. </value>
    Public Property MemoryOption() As Integer?
        Get
            Return Me._MemoryOption
        End Get
        Set(ByVal value As Integer?)
            If Not Integer?.Equals(Me.MemoryOption, value) Then
                Me._MemoryOption = value
                Select Case value
                    Case 0
                        Me.MaximumCapacity = BufferSizeStandardFull
                    Case 1
                        Me.MaximumCapacity = BufferSizeMemory1Full
                    Case 2
                        Me.MaximumCapacity = BufferSizeMemory2Full
                    Case Else
                        Me.MaximumCapacity = BufferSizeStandardFull
                End Select
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the Actual Points count query command. </summary>
    ''' <value> The Actual Points count query command. </value>
    Protected Overrides Property ActualPointCountQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the automatic Points enabled command Format. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Points enabled query command. </value>
    Protected Overrides Property AutoPointsEnabledCommandFormat As String = ":TRAC:POIN:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Points enabled query command. </summary>
    ''' <remarks> SCPI: ":TRAC:POIN:AUTO?". </remarks>
    ''' <value> The automatic Points enabled query command. </value>
    Protected Overrides Property AutoPointsEnabledQueryCommand As String = ":TRAC:POIN:AUTO?"

    ''' <summary> Gets or sets the Buffer Free query command. </summary>
    ''' <value> The buffer free query command. </value>
    Protected Overrides Property BufferFreePointCountQueryCommand As String = ":TRAC:FREE?"

    ''' <summary> Gets or sets the Clear Buffer command. </summary>
    ''' <value> The ClearBuffer command. </value>
    Protected Overrides Property ClearBufferCommand As String = ":TRAC:CLE"

    ''' <summary> Gets or sets the data query command. </summary>
    ''' <value> The data query command. </value>
    Protected Overrides Property DataQueryCommand As String = ":TRAC:DATA?"

    ''' <summary> Gets or sets the feed Control query command. </summary>
    ''' <value> The write feed Control query command. </value>
    Protected Overrides Property FeedControlQueryCommand As String = ":TRAC:FEED:CONTROL?"

    ''' <summary> Gets or sets the feed Control command format. </summary>
    ''' <value> The write feed Control command format. </value>
    Protected Overrides Property FeedControlCommandFormat As String = ":TRAC:FEED:CONTROL {0}"

    ''' <summary> Gets or sets the feed source query command. </summary>
    ''' <value> The write feed source query command. </value>
    Protected Overrides Property FeedSourceQueryCommand As String = ":TRAC:FEED?"

    ''' <summary> Gets or sets the feed source command format. </summary>
    ''' <value> The write feed source command format. </value>
    Protected Overrides Property FeedSourceCommandFormat As String = ":TRAC:FEED {0}"

    ''' <summary> Gets or sets the fetch command. </summary>
    ''' <value> The fetch command. </value>
    Protected Overrides Property FetchCommand As String = ":FETCH?"

    ''' <summary> Gets or sets the points count query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property PointsCountQueryCommand As String = ":TRAC:POIN?"

    ''' <summary> Gets or sets the points count command format. </summary>
    ''' <value> The points count command format. </value>
    Protected Overrides Property PointsCountCommandFormat As String = ":TRAC:POIN {0}"

#End Region

#Region " CLEAR OVERRIDES "

    ''' <summary> Clears the data buffer and restores the feed Control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="feedControl"> The feed control. </param>
    ''' <returns> The FeedControls. </returns>
    Public Overloads Function ClearBuffer(ByVal feedControl As FeedControls) As FeedControls?
        Me.ClearBuffer()
        Return Me.ApplyFeedControl(feedControl)
    End Function

#End Region

#Region " BUFFER STREAMING "

    ''' <summary> Queries the current Data. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Data or empty if none. </returns>
    Public Overrides Function QueryBufferReadings() As IList(Of BufferReading)
        Dim measurementBitmasks As New MeasurementEventsBitmaskDictionary
        isr.VI.K2002.StatusSubsystem.DefineBitmasks(measurementBitmasks)
        measurementBitmasks.Status = Me.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
        Return If(measurementBitmasks.IsAnyBitOn(MeasurementEventBitmaskKey.BufferAvailable),
                     Me.EnumerateBufferReadings(Me.QueryData),
                     New List(Of BufferReading))
    End Function

    ''' <summary> Adds all buffer readings. </summary>
    ''' <remarks>
    ''' This assumes fetching a trace of multiple readings and, therefore, does not parse timestamps
    ''' or status.
    ''' </remarks>
    ''' <returns> An Integer. </returns>
    Public Overrides Function AddAllBufferReadings() As Integer
        Dim newReadings As IList(Of BufferReading) = Me.QueryBufferReadings()
        If newReadings.Any Then
            Me.BufferReadingsBindingList.Add(newReadings)
            Me.EnqueueRange(newReadings)
            Me.LastBufferReading = Me.BufferReadingsBindingList.LastReading
            Me.NotifyPropertyChanged(NameOf(VI.TraceSubsystemBase.BufferReadingsCount))
        End If
        Return newReadings.Count
    End Function

    ''' <summary> Adds buffer readings to the reading queue and binding list. </summary>
    ''' <remarks>
    ''' This assumes the measurement event status indicates that a reading is ready to be fetched and
    ''' the status reflect reading exceptions such as out of range or overflow.
    ''' </remarks>
    ''' <param name="readingUnit">      The reading unit. </param>
    ''' <param name="initialTimestamp"> The initial timestamp. </param>
    ''' <param name="elapsedTimespan">  The elapsed timespan. </param>
    ''' <returns> An Integer. </returns>
    Public Overloads Function AddBufferReadings(ByVal readingUnit As Arebis.TypedUnits.Unit, ByVal initialTimestamp As DateTimeOffset, ByVal elapsedTimespan As TimeSpan) As Integer
        Return Me.AddBufferReadings(Me.FetchReading, readingUnit, initialTimestamp, elapsedTimespan)
    End Function

    ''' <summary> Adds buffer readings to the reading queue and binding list. </summary>
    ''' <remarks>
    ''' This assumes the reading were already fetched and the status reflect reading exceptions such
    ''' as out of range or overflow.
    ''' </remarks>
    ''' <param name="readingValues">    The reading values. </param>
    ''' <param name="readingUnit">      The reading unit. </param>
    ''' <param name="initialTimestamp"> The initial timestamp. </param>
    ''' <param name="elapsedTimespan">  The elapsed timespan. </param>
    ''' <returns> An Integer. </returns>
    Public Overloads Function AddBufferReadings(ByVal readingValues As String, ByVal readingUnit As Arebis.TypedUnits.Unit,
                                                ByVal initialTimestamp As DateTimeOffset, ByVal elapsedTimespan As TimeSpan) As Integer
        Dim newReadings As IList(Of BufferReading) = New List(Of BufferReading) From {New BufferReading}
        If Not String.IsNullOrWhiteSpace(readingValues) Then
            newReadings = Me.EnumerateBufferReadings(readingValues)
        End If
        If newReadings.Any Then
            Dim status As Integer = Me.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
            For Each reading As BufferReading In newReadings
                ' if no reading, the amount will be zero and the buffer has reading will be use to indicate that.
                reading.BuildAmount(readingUnit)
                reading.ApplyStatus(status Or If(reading.HasReading, 0, Me.StatusSubsystem.MeasurementEventsBitmasks(MeasurementEventBitmaskKey.StatusOnly)))
                reading.ParseTimestamp(initialTimestamp, elapsedTimespan)
            Next
            Me.BufferReadingsBindingList.Add(newReadings)
            Me.EnqueueRange(newReadings)
            Me.LastBufferReading = Me.BufferReadingsBindingList.LastReading
            ' this notifies the automation that buffer readings are 
            Me.SyncNotifyPropertyChanged(NameOf(VI.TraceSubsystemBase.BufferReadingsCount))
            isr.Core.ApplianceBase.DoEvents()
        End If
        Return newReadings.Count
    End Function

    ''' <summary> Stream buffer; use with buffer counts exceeding 1 point. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="pollPeriod"> The poll period. </param>
    Private Sub StreamBufferThis(ByVal pollPeriod As TimeSpan)

        ' save the currently set feed control
        Dim feed As FeedControls = Me.FeedControl.Value

        ' clear the buffer
        Me.ClearBuffer(feed)

        ' clear trace buffer
        Me.BufferStreamingEnabled = True
        Me.BufferReadingsBindingList.Clear()
        Me.ClearBufferReadingsQueue()
        Me.BufferStreamingActive = True
        Me.StatusSubsystem.ApplyMeasurementEventEnableBitmask(Me.StatusSubsystem.MeasurementEventsBitmasks.All)
        Me.StatusSubsystem.ApplyOperationEventEnableBitmask(Me.StatusSubsystem.OperationEventsBitmasks.All)
        Dim initialTimestamp As DateTimeOffset = DateTimeOffset.Now
        Dim sampleTimeStopwatch As Stopwatch = Stopwatch.StartNew
        Dim bufferCycleDurationStopwatch As Stopwatch = Stopwatch.StartNew
        Dim measurementEvent As MeasurementEventsBitmaskDictionary = Me.StatusSubsystem.MeasurementEventsBitmasks
        Dim pollStopwatch As Stopwatch = Stopwatch.StartNew
        Do While Me.BufferStreamingEnabled
            isr.Core.ApplianceBase.DoEvents()
            measurementEvent.Status = Me.StatusSubsystem.QueryMeasurementEventStatus.Value
            If measurementEvent.IsAnyBitOn(MeasurementEventBitmaskKey.BufferFull) Then
                Me.AddBufferReadings(Me.QueryData, Me.BufferReadingUnit, initialTimestamp, sampleTimeStopwatch.Elapsed)
                ' allow enough time for binning to take place
                Me.BinningDuration.SpinWait()
                ' clear the buffer for the next trigger
                Me.ClearBuffer(feed)
                Me.StreamCycleDuration = bufferCycleDurationStopwatch.Elapsed
                bufferCycleDurationStopwatch.Restart()
            Else
                If Me.BusTriggerRequested Then Me.BusTriggerRequested = False : Me.Session.AssertTrigger()
                If Me.BinningStrobeRequested Then Me.BinningStrobeRequested = False : Me.BinningStrobeAction.Invoke()
                pollPeriod.Subtract(pollStopwatch.Elapsed).SpinWait()
                pollStopwatch.Restart()
                isr.Core.ApplianceBase.DoEvents()
            End If
        Loop
        Me.BufferStreamingActive = False
    End Sub

    ''' <summary> Stream reading this. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="pollPeriod"> The poll period. </param>
    Private Sub StreamReadingThis(ByVal pollPeriod As TimeSpan)
        Dim measurementEvent As MeasurementEventsBitmaskDictionary = Me.StatusSubsystem.MeasurementEventsBitmasks
        Me.BufferStreamingAlert = False
        Me.BufferStreamingEnabled = True
        Me.BufferReadingsBindingList.Clear()
        Me.ClearBufferReadingsQueue()
        Me.BufferStreamingActive = True
        Me.StatusSubsystem.ApplyMeasurementEventEnableBitmask(Me.StatusSubsystem.MeasurementEventsBitmasks.All)
        Me.StatusSubsystem.ApplyOperationEventEnableBitmask(Me.StatusSubsystem.OperationEventsBitmasks.All)
        Dim initialTimestamp As DateTimeOffset = DateTimeOffset.Now
        Dim bufferCycleDurationStopwatch As Stopwatch = Stopwatch.StartNew
        Dim sampleTimeStopwatch As Stopwatch = Stopwatch.StartNew
        Dim pollStopwatch As Stopwatch = Stopwatch.StartNew
        Do While Me.BufferStreamingEnabled
            isr.Core.ApplianceBase.DoEvents()
            measurementEvent.Status = Me.StatusSubsystem.QueryMeasurementEventStatus.Value
            If measurementEvent.IsAnyBitOn(MeasurementEventBitmaskKey.ReadingAvailable) Then
                Me.AddBufferReadings(Me.FetchReading, Me.BufferReadingUnit, initialTimestamp, sampleTimeStopwatch.Elapsed)
                ' allow enough time for binning to take place
                Me.BinningDuration.SpinWait()
                Me.StreamCycleDuration = bufferCycleDurationStopwatch.Elapsed
                bufferCycleDurationStopwatch.Restart()
            Else
                If Me.BusTriggerRequested Then Me.BusTriggerRequested = False : Me.Session.AssertTrigger()
                If Me.BinningStrobeRequested Then Me.BinningStrobeRequested = False : Me.BinningStrobeAction.Invoke()
                pollPeriod.Subtract(pollStopwatch.Elapsed).SpinWait()
                pollStopwatch.Restart()
                isr.Core.ApplianceBase.DoEvents()
            End If
        Loop
        Me.BufferStreamingActive = False
    End Sub

    ''' <summary> Stream buffer. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="triggerSubsystem"> The trigger subsystem. </param>
    ''' <param name="pollPeriod">       The poll period. </param>
    ''' <param name="readingUnit">      The reading unit. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Overrides Sub StreamBuffer(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal pollPeriod As TimeSpan, ByVal readingUnit As Arebis.TypedUnits.Unit)
        ' the 2002 instrument does not monitor the trigger plan.
        Try
            Me.BufferReadingUnit = readingUnit
            Dim feedControls As VI.FeedControls = Me.FeedControl.GetValueOrDefault(VI.FeedControls.None)
            If (VI.FeedControls.Never = feedControls) OrElse (VI.FeedControls.None = feedControls) Then
                Me.StreamReadingThis(pollPeriod)
            Else
                Me.StreamBufferThis(pollPeriod)
            End If
        Catch
            ' stop buffer streaming; the exception is handled in the Async Completed event handler
            Me.BufferStreamingAlert = True
            Me.BufferStreamingEnabled = False
            Me.BufferStreamingActive = False
        End Try
    End Sub

#End Region

End Class

