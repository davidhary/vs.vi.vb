'---------------------------------------------------------------------------------------------------
' file:		.\Device\SessionFactory.vb
'
' summary:	Session factory class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A session factory. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-29 </para>
''' </remarks>
Public Class SessionFactory
    Inherits isr.Core.Models.SelectorViewModel

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.NewThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Sub New(ByVal talker As Core.ITraceMessageTalker)
        MyBase.New(talker)
        Me.NewThis()
    End Sub

    ''' <summary> Common constructors. </summary>
    Private Sub NewThis()
        Me.Searchable = True
        Me.PingFilterEnabled = False
        Me.ApplySessionFactory()
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As SessionFactory

    ''' <summary> Returns a new or existing instance of this class. </summary>
    ''' <remarks>
    ''' Use this property to get an instance of this class. Returns the default instance of this form
    ''' allowing to use this form as a singleton for.
    ''' </remarks>
    ''' <value> <c>A</c> new or existing instance of the class. </value>
    Public Shared ReadOnly Property [Get]() As SessionFactory
        Get
            If SessionFactory._Instance Is Nothing Then
                SyncLock SessionFactory.SyncLocker
                    SessionFactory._Instance = New SessionFactory
                    SessionFactory._Instance.ApplySessionFactory()
                End SyncLock
            End If
            Return SessionFactory._Instance
        End Get
    End Property

    ''' <summary> Gets or sets the factory. </summary>
    ''' <value> The factory. </value>
    Public Property Factory As VI.Pith.SessionFactoryBase

#End Region

#Region " RESOURE MANAGER "

    ''' <summary> A filter specifying the resources. </summary>
    Private _ResourcesFilter As String

    ''' <summary> Gets or sets the resources search pattern. </summary>
    ''' <value> The resources search pattern. </value>
    Public Property ResourcesFilter As String
        Get
            If String.IsNullOrEmpty(Me._ResourcesFilter) Then
                Me._ResourcesFilter = VI.SessionFactory.Get.Factory.ResourcesProvider.ResourceFinder.BuildMinimalResourcesFilter
            End If
            Return Me._ResourcesFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourcesFilter, StringComparison.OrdinalIgnoreCase) Then
                Me._ResourcesFilter = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> True to enable, false to disable the ping filter. </summary>
    Private _PingFilterEnabled As Boolean

    ''' <summary>
    ''' Gets or sets the Ping Filter enabled sentinel. When enabled, Tcp/IP resources are added only
    ''' if they can be pinged.
    ''' </summary>
    ''' <value> The ping filter enabled. </value>
    Public Property PingFilterEnabled As Boolean
        Get
            Return Me._PingFilterEnabled
        End Get
        Set(value As Boolean)
            If value <> Me.PingFilterEnabled Then
                Me._PingFilterEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Enumerate resource names. </summary>
    ''' <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    ''' <returns> A list of <see cref="T:System.String" />. </returns>
    Public Overrides Function EnumerateResources(ByVal applyEnabledFilters As Boolean) As BindingList(Of String)
        Dim resources As IEnumerable(Of String) = Array.Empty(Of String)
        Using rm As VI.Pith.ResourcesProviderBase = Me.Factory.ResourcesProvider()
            resources = If(String.IsNullOrWhiteSpace(Me.ResourcesFilter), rm.FindResources(), rm.FindResources(Me.ResourcesFilter))
        End Using
        ' TO_DO: ignore ping option at this time since this is the only filter we use.
        ' If applyEnabledFilters AndAlso Me.PingFilterEnabled Then resources = VI.Pith.ResourceNamesManager.PingFilter(resources)
        If applyEnabledFilters Then resources = VI.Pith.ResourceNamesManager.PingFilter(resources)
        Return Me.EnumerateResources(resources)
    End Function

    ''' <summary> Enumerates the default resource name patterns in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the default resource name patters in
    ''' this collection.
    ''' </returns>
    Public Shared Function EnumerateDefaultResourceNamePatterns() As BindingList(Of String)
        Return New BindingList(Of String) From {
            "GPIB[board]::number[::INSTR]",
            "GPIB[board]::INTFC",
            "TCPIP[board]::host address[::LAN device name][::INSTR]",
            "TCPIP[board]::host address::port::SOCKET"
        }
    End Function

    ''' <summary> Validates the functional visa versions. </summary>
    ''' <remarks> David, 2020-04-16. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateFunctionalVisaVersions() As (Success As Boolean, Details As String)
        Using rm As VI.Pith.ResourcesProviderBase = VI.SessionFactory.Get.Factory.ResourcesProvider()
            Return rm.ValidateFunctionalVisaVersions
        End Using
    End Function

#End Region

#Region " RESOURCE SELECTION "

    ''' <summary> Queries if a given check resource exists. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overrides Function QueryResourceExists(ByVal resourceName As String) As Boolean
        If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))
        Using rm As VI.Pith.ResourcesProviderBase = Me.Factory.ResourcesProvider()
            Return rm.Exists(resourceName)
        End Using
    End Function

    ''' <summary> Attempts to select resource from the given data. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Function TryValidateResource(ByVal resourceName As String) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(resourceName) Then resourceName = String.Empty
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = String.Empty
        Try
            Me.CandidateResourceName = resourceName
            activity = "checking if need to select the resource"
            If String.IsNullOrWhiteSpace(resourceName) Then
                activity = "attempted to select an empty resource name"
                result = (True, activity)
            ElseIf String.Equals(resourceName, Me.ValidatedResourceName, StringComparison.OrdinalIgnoreCase) Then
                activity = $"resource {resourceName} already validated"
                result = (True, activity)
            ElseIf Me.ValidationEnabled Then
                activity = $"finding resource {resourceName}"
                If Me.QueryResourceExists(resourceName) Then
                    activity = $"setting validated resource name to {resourceName}"
                    Me.ValidatedResourceName = resourceName
                Else
                    activity = $"resource {resourceName} not found; clearing validated resource name"
                    Me.ValidatedResourceName = String.Empty
                End If
            Else
                activity = $"validation disabled--setting validated resource name to {resourceName}"
                Me.ValidatedResourceName = resourceName
            End If
        Catch ex As Exception
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
        End Try
        Return result
    End Function

#End Region

#Region " I TALKER "

    ''' <summary> Identifies talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

#End Region

#Region " TALKER PUBLISH "

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
