''' <summary>
''' Test manager for <see cref="isr.VI.Facade.VisaView"/>, <see cref="VisaSessionBase"/>.
''' </summary>
''' <remarks>
''' David, 2019-12-12 <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Partial Public NotInheritable Class DeviceManager

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

End Class
