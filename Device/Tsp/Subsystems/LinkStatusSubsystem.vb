'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\LinkStatusSubsystem.vb
'
' summary:	Link status subsystem class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EscapeSequencesExtensions

''' <summary> A link status subsystem. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-03-28 </para>
''' </remarks>
Public Class LinkStatusSubsystem
    Inherits VI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="LinkStatusSubsystem" /> class.
    ''' </summary>
    ''' <param name="session"> The session. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(VI.Pith.SessionBase.Validated(session))
        Me._VersionInfo = New VersionInfo
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session"> The session. </param>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create(ByVal session As VI.Pith.SessionBase) As LinkStatusSubsystem
        Dim subsystem As LinkStatusSubsystem = Nothing
        Try
            subsystem = New LinkStatusSubsystem(session)
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region

#Region " ERROR QUEUE: NODE "

    ''' <summary> Gets or sets the node entities. </summary>
    ''' <remarks> Required for reading the system errors. </remarks>
    ''' <value> The node entities. </value>
    Public ReadOnly Property NodeEntities As NodeEntityCollection

    ''' <summary> The error queue count query command. </summary>
    Private _ErrorQueueCountQueryCommand As String = "_G.print(_G.string.format('%d',_G.errorqueue.count))"

    ''' <summary> Gets or sets The ErrorQueueCount query command. </summary>
    ''' <value> The ErrorQueueCount query command. </value>
    Protected Overrides Property ErrorQueueCountQueryCommand As String
        Get
            Return Me._ErrorQueueCountQueryCommand
        End Get
        Set(value As String)
            Me._ErrorQueueCountQueryCommand = value
            MyBase.ErrorQueueCountQueryCommand = value
        End Set
    End Property

    ''' <summary> Queries error count on a remote node. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> The error count. </returns>
    Public Overloads Function QueryErrorQueueCount(ByVal node As NodeEntityBase) As Integer
        Dim count As Integer?
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        Me.ErrorQueueCountQueryCommand = If(node.IsController,
            "_G.print(_G.string.format('%d',_G.errorqueue.count))",
            $"_G.print(_G.string.format('%d',node[{node.Number}].errorqueue.count))")
        count = Me.QueryErrorQueueCount()
        Return count.GetValueOrDefault(0)
    End Function

    ''' <summary> Clears the error cache. </summary>
    Public Overrides Sub ClearErrorCache()
        MyBase.ClearErrorCache()
        Me._DeviceErrorQueue = New Queue(Of TspDeviceError)
    End Sub

    ''' <summary> Queries if a given node exists. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function NodeExists(ByVal nodeNumber As Integer) As Boolean
        Dim affirmative As Boolean = True
        Return If(Me.NodeEntities.Count > 0 AndAlso Me.NodeEntities.Contains(NodeEntityBase.BuildKey(nodeNumber)), affirmative, affirmative)
    End Function

    ''' <summary> Clears the error queue for the specified node. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Private Overloads Sub ClearErrorQueue(ByVal nodeNumber As Integer)
        If Not Me.NodeExists(nodeNumber) Then
            Me.Session.WriteLine("node[{0}].errorqueue.clear() waitcomplete({0})", nodeNumber)
        End If
    End Sub

    ''' <summary> Clears the error queue. </summary>
    Public Overrides Sub ClearErrorQueue()
        If Me.NodeEntities Is Nothing Then
            MyBase.ClearErrorQueue()
        Else
            Me.ClearErrorCache()
            For Each node As NodeEntityBase In Me.NodeEntities
                Me.ClearErrorQueue(node.Number)
            Next
        End If
    End Sub

    ''' <summary> Queue of device errors. </summary>
    Private _DeviceErrorQueue As Queue(Of TspDeviceError)

    ''' <summary> Gets the error queue. </summary>
    ''' <value> A Queue of device errors. </value>
    Protected Overloads ReadOnly Property DeviceErrorQueue As Queue(Of TspDeviceError)
        Get
            Return Me._DeviceErrorQueue
        End Get
    End Property

    ''' <summary> Returns the queued error. </summary>
    ''' <remarks> Sends the error print format query and reads back and parses the error. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> The queued error. </returns>
    Private Function QueryQueuedError(ByVal node As NodeEntityBase) As TspDeviceError
        Dim err As New TspDeviceError()
        If Me.QueryErrorQueueCount(node) > 0 Then
            If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
            Me.Session.LastAction = Me.Talker.Publish(TraceEventType.Information, My.MyLibrary.TraceEventId, "Querying queued device errors;. ")
            Dim message As String
            If node.IsController Then
                Me.Session.LastNodeNumber = node.ControllerNodeNumber
                message = Me.Session.QueryPrintStringFormatTrimEnd($"%d,%s,%d,node{node.Number}", "_G.errorqueue.next()")
            Else
                Me.Session.LastNodeNumber = node.Number
                message = Me.Session.QueryPrintStringFormatTrimEnd($"%d,%s,%d,node{node.Number}", $"node[{node.Number}].errorqueue.next()")
            End If
            Me.CheckThrowDeviceException(False, "getting queued error;. using {0}.", Me.Session.LastMessageSent)
            err = New TspDeviceError()
            err.Parse(message)
        End If
        Return err
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
    Private Overloads Function QueryDeviceErrors(ByVal node As NodeEntityBase) As String
        If node Is Nothing Then Throw New ArgumentNullException(NameOf(node))
        Dim deviceError As TspDeviceError
        Do
            deviceError = Me.QueryQueuedError(node)
            If deviceError.IsError Then
                Me.DeviceErrorQueue.Enqueue(deviceError)
            End If
        Loop While Me.ErrorAvailable()
        Dim builder As New System.Text.StringBuilder
        If Me.DeviceErrorQueue IsNot Nothing AndAlso Me.DeviceErrorQueue.Count > 0 Then
            builder.AppendLine($"Instrument {Me.ResourceNameCaption} Node {node.Number} Errors:")
            builder.AppendLine()
            For Each e As TspDeviceError In Me.DeviceErrorQueue
                builder.AppendLine(e.ErrorMessage)
            Next
        End If
        Me.AppendDeviceErrorMessage(builder.ToString)
        Return Me.DeviceErrorReport
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
    Protected Overrides Function QueryDeviceErrors() As String
        Me.ClearErrorCache()
        For Each node As NodeEntityBase In Me.NodeEntities
            Me.QueryDeviceErrors(node)
        Next
        Me.NotifyPropertyChanged(NameOf(VI.StatusSubsystemBase.DeviceErrorReport))
        Return Me.DeviceErrorReport
    End Function

    #End Region

    #Region " IDENTITY "

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = Tsp.Syntax.LocalNode.IdentityQueryCommand

    ''' <summary> Gets or sets the serial number query command. </summary>
    ''' <value> The serial number query command. </value>
    Protected Overrides Property SerialNumberQueryCommand As String = "_G.print(string.format('%d',_G.localnode.serialno))"

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
    ''' <returns> System.String. </returns>
    Public Overrides Function QueryIdentity() As String
        If Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.PublishVerbose("Requesting identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Me.WriteIdentityQueryCommand()
            Me.PublishVerbose("Trying to read identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Dim value As String = Me.Session.ReadLineTrimEnd
            value = value.ReplaceCommonEscapeSequences.Trim
            Me.PublishVerbose($"Setting identity to {value};. ")
            Me.VersionInfo.Parse(value)
            MyBase.VersionInfoBase = Me.VersionInfo
            Me.Identity = Me.VersionInfo.Identity
        End If
        Return Me.Identity
    End Function

    ''' <summary> Gets or sets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public ReadOnly Property VersionInfo As VersionInfo

#End Region

End Class
