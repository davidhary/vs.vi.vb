'---------------------------------------------------------------------------------------------------
' file:		Pith\Syntax\Ieee488Syntax.vb
'
' summary:	Ieee 488 syntax class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Namespace Ieee488

    Namespace Syntax

        ''' <summary> Defines the standard IEEE488 command set. </summary>
        ''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
        ''' Licensed under The MIT License.</para><para>
        ''' David, 2005-01-15, 1.0.1841.x. </para></remarks>
        Public Module Ieee488Syntax

#Region " IEEE 488.2 STANDARD COMMANDS "

            ''' <summary> Gets the Clear Status (CLS) command. </summary>
            Public Const ClearExecutionStateCommand As String = "*CLS"

            ''' <summary> Gets the Identity query (*IDN?) command. </summary>
            Public Const IdentityQueryCommand As String = "*IDN?"

            ''' <summary> Gets the operation complete (*OPC) command. </summary>
            Public Const OperationCompleteCommand As String = "*OPC"

            ''' <summary> Gets the operation complete query (*OPC?) command. </summary>
            Public Const OperationCompletedQueryCommand As String = "*OPC?"

            ''' <summary> Gets the options query (*OPT?) command. </summary>
            Public Const OptionsQueryCommand As String = "*OPT?"

            ''' <summary> Gets the Wait (*WAI) command. </summary>
            Public Const WaitCommand As String = "*WAI"

            ''' <summary> Gets the Standard Event Enable (*ESE) command. </summary>
            Public Const StandardEventEnableCommandFormat As String = "*ESE {0:D}"

            ''' <summary> Gets the Standard Event Enable query (*ESE?) command. </summary>
            Public Const StandardEventEnableQueryCommand As String = "*ESE?"

            ''' <summary> Gets the Standard Event Enable (*ESR?) command. </summary>
            Public Const StandardEventStatusQueryCommand As String = "*ESR?"

            ''' <summary> Gets the Service Request Enable (*SRE) command. </summary>
            Public Const ServiceRequestEnableCommandFormat As String = "*SRE {0:D}"

            ''' <summary> Gets the Standard Event and Service Request Enable '*CLS; *ESE {0:D}; *SRE {1:D}' command format. </summary>
            Public Const StandardServiceEnableCommandFormat As String = "*CLS; *ESE {0:D}; *SRE {1:D}"

            ''' <summary> Gets the Standard Event and Service Request Enable '*CLS; *ESE {0:D}; *SRE {1:D}; *OPC' command format. </summary>
            Public Const StandardServiceEnableCompleteCommandFormat As String = "*CLS; *ESE {0:D}; *SRE {1:D}; *OPC"

            ''' <summary> Gets the Operation Complete Enable '*CLS; *ESE {0:D}; *OPC' command format. </summary>
            Public Const OperationCompleteEnableCommandFormat As String = "*CLS; *ESE {0:D}; *OPC"

            ''' <summary> Gets the Service Request Enable query (*SRE?) command. </summary>
            Public Const ServiceRequestEnableQueryCommand As String = "*SRE?"

            ''' <summary> Gets the Service Request Status query (*STB?) command. </summary>
            Public Const ServiceRequestQueryCommand As String = "*STB?"

            ''' <summary> Gets the reset to know state (*RST) command. </summary>
            Public Const ResetKnownStateCommand As String = "*RST"

#End Region

#Region " KEITHLEY IEEE488 COMMANDS "

            ''' <summary> Gets the Language query (*LANG?) command. </summary>
            Public Const LanguageQueryCommand As String = "*LANG?"

            ''' <summary> Gets the Language command format (*LANG). </summary>
            Public Const LanguageCommandFormat As String = "*LANG {0}"

            ''' <summary> The language scpi. </summary>
            Public Const LanguageScpi As String = "SCPI"

            ''' <summary> The language TSP. </summary>
            Public Const LanguageTsp As String = "TSP"

#End Region

#Region " BUILDERS "

            ''' <summary> Builds the device clear (DCL) command. </summary>
            ''' <returns>
            ''' An enumerator that allows for-each to be used to process build device clear command in this
            ''' collection.
            ''' </returns>
            Public Function BuildDeviceClear() As IEnumerable(Of Byte)
                ' Thee DCL command to the interface.
                Dim commands As Byte() = New Byte() {Convert.ToByte(Ieee488.CommandCode.Untalk),
                                             Convert.ToByte(Ieee488.CommandCode.Unlisten),
                                             Convert.ToByte(Ieee488.CommandCode.DeviceClear),
                                             Convert.ToByte(Ieee488.CommandCode.Untalk),
                                             Convert.ToByte(Ieee488.CommandCode.Unlisten)}
                Return commands
            End Function

            ''' <summary> Builds selective device clear (SDC) in this collection. </summary>
            ''' <param name="gpibAddress"> The gpib address. </param>
            ''' <returns>
            ''' An enumerator that allows for-each to be used to process build selective device clear in this
            ''' collection.
            ''' </returns>
            Public Function BuildSelectiveDeviceClear(ByVal gpibAddress As Byte) As IEnumerable(Of Byte)
                Dim commands As Byte() = New Byte() {Convert.ToByte(Ieee488.CommandCode.Untalk),
                                     Convert.ToByte(Ieee488.CommandCode.Unlisten),
                                     Convert.ToByte(Ieee488.CommandCode.ListenAddressGroup) Or Convert.ToByte(gpibAddress),
                                     Convert.ToByte(Ieee488.CommandCode.SelectiveDeviceClear),
                                     Convert.ToByte(Ieee488.CommandCode.Untalk),
                                     Convert.ToByte(Ieee488.CommandCode.Unlisten)}
                Return commands
            End Function

#End Region

        End Module

    End Namespace

    ''' <summary> Values that represent IEEE 488.2 Command Code. </summary>
    Public Enum CommandCode

        ''' <summary> An enum constant representing the none option. </summary>
        None = 0

        ''' <summary> An enum constant representing the go to local option. </summary>
        <Description("GTL")>
        GoToLocal = &H1

        ''' <summary> An enum constant representing the selective device clear option. </summary>
        <Description("SDC")>
        SelectiveDeviceClear = &H4

        ''' <summary> An enum constant representing the group execute trigger option. </summary>
        <Description("GET")>
        GroupExecuteTrigger = &H8

        ''' <summary> An enum constant representing the local lockout option. </summary>
        <Description("LLO")>
        LocalLockout = &H11

        ''' <summary> An enum constant representing the device clear option. </summary>
        <Description("DCL")>
        DeviceClear = &H14

        ''' <summary> An enum constant representing the serial poll enable option. </summary>
        <Description("SPE")>
        SerialPollEnable = &H18

        ''' <summary> An enum constant representing the serial poll disable option. </summary>
        <Description("SPD")>
        SerialPollDisable = &H19

        ''' <summary> An enum constant representing the listen address group option. </summary>
        <Description("LAG")>
        ListenAddressGroup = &H20

        ''' <summary> An enum constant representing the talk address group option. </summary>
        <Description("TAG")>
        TalkAddressGroup = &H40

        ''' <summary> An enum constant representing the secondary command group option. </summary>
        <Description("SCG")>
        SecondaryCommandGroup = &H60

        ''' <summary> An enum constant representing the unlisten option. </summary>
        <Description("UNL")>
        Unlisten = &H3F

        ''' <summary> An enum constant representing the untalk option. </summary>
        <Description("UNT")>
        Untalk = &H5F
    End Enum

End Namespace
