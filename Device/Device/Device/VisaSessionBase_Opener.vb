'---------------------------------------------------------------------------------------------------
' file:		.\Device\VisaSessionBase_Opener.vb
'
' summary:	Visa session base opener class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Threading.Tasks
Imports isr.Core.Models
Imports isr.Core
Imports isr.Core.TaskExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Pith

Partial Public Class VisaSessionBase
    Inherits OpenerViewModel

#Region " SELECTOR "

    ''' <summary> Gets or sets the resources search pattern. </summary>
    ''' <value> The resources search pattern. </value>
    Public Property ResourcesFilter As String
        Get
            Return Me._SessionFactory.ResourcesFilter
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ResourcesFilter) Then
                Me._SessionFactory.ResourcesFilter = value
                Me.NotifyPropertyChanged()
                Me.Session.ResourcesFilter = value
            End If
        End Set
    End Property

    ''' <summary> The session factory. </summary>
    Private _SessionFactory As SessionFactory

    ''' <summary> Gets the session factory. </summary>
    ''' <value> The session factory. </value>
    Public ReadOnly Property SessionFactory As SessionFactory
        Get
            ' enable validation; Validation is disabled by default to facilitate
            ' resource selection in case of resource manager mismatch between 
            ' VISA implementations.
            Me._SessionFactory.ValidationEnabled = Me.ValidationEnabled
            Return Me._SessionFactory
        End Get
    End Property

    ''' <summary> Attempts to validate resource from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryValidateResource(ByVal resourceName As String) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(resourceName) Then resourceName = String.Empty
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)

        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceTitleCaption} validating the resource" : Me.PublishInfo($"{activity};. ")
            Me.Session.StatusPrompt = activity
            result = Me.SessionFactory.TryValidateResource(resourceName)
            If Not String.IsNullOrEmpty(result.Details) Then Me.PublishInfo($"{activity};. reported { result.Details}")
            Me.Session.StatusPrompt = If(result.Success, $"done {activity}", $"failed {activity}")
        Catch ex As Exception
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            ' this enables opening if the validated name has value
            Me.ValidatedResourceName = Me.SessionFactory.ValidatedResourceName
        End Try
        Return result
    End Function

    ''' <summary> Validates the resource name described by resourceName. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    Public Sub ValidateResourceName(ByVal resourceName As String)
        Me.TryValidateResource(resourceName)
    End Sub

#End Region

#Region " RESOURCE NAME VALIDATION TASK "

    ''' <summary> Gets or sets the resource name validation asynchronous task. </summary>
    ''' <value> The resource name validation asynchronous task. </value>
    Private ReadOnly Property ResourceNameValidationAsyncTask As Task

    ''' <summary> Gets or sets the resource name validation task. </summary>
    ''' <value> The resource name validation task. </value>
    Private ReadOnly Property ResourceNameValidationTask As Task

    ''' <summary> Query if the task validating the resource name is active. </summary>
    ''' <returns>
    ''' <c>true</c> if the task validating the resource name is active; otherwise <c>false</c>
    ''' </returns>
    Public Function IsValidatingResourceName() As Boolean
        Return Me.ResourceNameValidationTask.IsTaskActive
    End Function

    ''' <summary> Await resource name validation. </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub AwaitResourceNameValidation(ByVal timeout As TimeSpan)
        Me.ResourceNameValidationTask?.Wait(timeout)
    End Sub

    ''' <summary> Asynchronously validate the resource name. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A Threading.Tasks.Task. </returns>
    Public Function AsyncValidateResourceName(ByVal resourceName As String) As Threading.Tasks.Task
        Me._ResourceNameValidationAsyncTask = Me.AsyncAwaitValidateResourceName(resourceName)
        Return Me.ResourceNameValidationTask
    End Function

    ''' <summary> Asynchronous await validate resource name. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A Task. </returns>
    Private Async Function AsyncAwaitValidateResourceName(ByVal resourceName As String) As Task
        Me._ResourceNameValidationTask = Threading.Tasks.Task.Run(Sub()
                                                                      Me.ValidateResourceName(resourceName)
                                                                  End Sub)
        Await Me.ResourceNameValidationTask
    End Function

#End Region

#Region " RESOURCE NAME "

    ''' <summary> Gets or sets the name of the candidate resource. </summary>
    ''' <value> The name of the candidate resource. </value>
    Public Overrides Property CandidateResourceName As String
        Get
            Return MyBase.CandidateResourceName
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.Session.CandidateResourceName) OrElse
               Not String.Equals(value, MyBase.CandidateResourceName) Then
                Me.Session.CandidateResourceName = value
                Me.SessionFactory.CandidateResourceName = value
                MyBase.CandidateResourceName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the candidate resource name validated. </summary>
    ''' <value> The candidate resource name validated. </value>
    Public Overrides Property CandidateResourceNameValidated As Boolean
        Get
            Return MyBase.CandidateResourceNameValidated
        End Get
        Set(value As Boolean)
            Me.Session.CandidateResourceNameConnected = value
            MyBase.CandidateResourceNameValidated = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Returns true if the validated candidate resource name was also connected. </summary>
    ''' <value> The resource name connected. </value>
    Public ReadOnly Property ResourceNameConnected As Boolean
        Get
            Return Me.CandidateResourceNameValidated AndAlso String.Equals(Me.CandidateResourceName, Me.OpenResourceName)
        End Get
    End Property

    ''' <summary> Gets or sets the name of the open resource. </summary>
    ''' <value> The name of the open resource. </value>
    Public Overrides Property OpenResourceName As String
        Get
            Return MyBase.OpenResourceName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Session.OpenResourceName) OrElse
               Not String.Equals(value, MyBase.OpenResourceName) Then
                Me.Session.OpenResourceName = value
                MyBase.OpenResourceName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the candidate resource title. </summary>
    ''' <value> The candidate resource title. </value>
    Public Overrides Property CandidateResourceTitle As String
        Get
            Return MyBase.CandidateResourceTitle
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Session.CandidateResourceTitle) OrElse
               Not String.Equals(value, MyBase.CandidateResourceTitle) Then
                Me.Session.CandidateResourceTitle = value
                MyBase.CandidateResourceTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the resource title. </summary>
    ''' <value> The resource title. </value>
    Public Overrides Property OpenResourceTitle As String
        Get
            Return MyBase.OpenResourceTitle
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Session.OpenResourceTitle) OrElse
               Not String.Equals(value, MyBase.OpenResourceTitle) Then
                Me.Session.OpenResourceTitle = value
                MyBase.OpenResourceTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the resource name caption. </summary>
    ''' <value>
    ''' The caption for the <see cref="VI.Pith.SessionBase.OpenResourceName"/> open or
    ''' <see cref="VI.Pith.SessionBase.candidateResourceName"/>resource names.
    ''' </value>
    Public Overrides Property ResourceNameCaption As String
        Get
            Return MyBase.ResourceNameCaption
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Session.ResourceNameCaption) OrElse
               Not String.Equals(value, MyBase.ResourceNameCaption) Then
                Me.Session.ResourceNameCaption = value
                MyBase.ResourceNameCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the resource Title caption. </summary>
    ''' <value>
    ''' The caption for the <see cref="VI.Pith.SessionBase.OpenResourceTitle"/> open,
    ''' <see cref="VI.Pith.SessionBase.candidateResourceTitle"/> or identity.
    ''' </value>
    Public Overrides Property ResourceTitleCaption As String
        Get
            Return MyBase.ResourceTitleCaption
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Session.ResourceTitleCaption) OrElse
               Not String.Equals(value, MyBase.ResourceTitleCaption) Then
                Me.Session.ResourceTitleCaption = value
                MyBase.ResourceTitleCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " OPENER "

    ''' <summary> Gets the Open status. </summary>
    ''' <value> The Open status. </value>
    Public Overrides ReadOnly Property IsOpen As Boolean
        Get
            Return Me.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Gets the is session open. </summary>
    ''' <value> The is session open. </value>
    Public ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me.Session IsNot Nothing AndAlso Me.Session.IsSessionOpen
        End Get
    End Property

    ''' <summary> Gets the is device open. </summary>
    ''' <remarks>
    ''' The session device open property can be toggled without having the actual session open.  This
    ''' is useful when emulating session functionality.
    ''' </remarks>
    ''' <value> The is device open. </value>
    Public ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me.Session IsNot Nothing AndAlso Me.Session.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Initializes the session prior to opening access to the instrument. </summary>
    Protected Overridable Sub BeforeOpening()
        Me.ApplySettings()
    End Sub

    ''' <summary> Notifies an open changed. </summary>
    Public Overrides Sub NotifyOpenChanged()
        Me.NotifyPropertyChanged(NameOf(VisaSessionBase.IsDeviceOpen))
        Me.NotifyPropertyChanged(NameOf(VisaSessionBase.IsSessionOpen))
        MyBase.NotifyOpenChanged()
    End Sub

    ''' <summary> Opens the session. </summary>
    ''' <remarks> Register the device trace notifier before opening the session. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException">   Thrown when operation failed to execute. </exception>
    ''' <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
    '''                                               occurs. </exception>
    ''' <param name="resourceName">  Name of the resource. </param>
    ''' <param name="resourceTitle"> The resource title. </param>
    Public Overridable Sub OpenSession(ByVal resourceName As String, ByVal resourceTitle As String)
        Dim success As Boolean = False

        Try
            If Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
                Me.CandidateResourceName = resourceName
                Me.CandidateResourceTitle = resourceTitle
            End If
            Dim activity As String = $"Opening session to {resourceName}"

            If Me.Enabled Then Me.PublishVerbose($"{activity};. ")
            ' initializes the session keep-alive commands.
            Me.BeforeOpening()
            Me.Session.OpenSession(resourceName, resourceTitle)
            If Me.Session.IsSessionOpen Then
                Me.PublishVerbose($"Session open to {resourceName};. ")
            ElseIf Me.Session.Enabled Then
                Throw New isr.Core.OperationFailedException($"Unable to open session to {resourceName};. ")
            ElseIf Not Me.IsDeviceOpen Then
                Throw New isr.Core.OperationFailedException($"Unable to emulate {resourceName};. ")
            End If
            If Me.Session.IsSessionOpen OrElse (Me.IsDeviceOpen AndAlso Not Me.Session.Enabled) Then

                activity = $"{resourceTitle}:{resourceName} handling opening actions"

                Dim e As New CancelEventArgs
                Me.OnOpening(e)

                If e.Cancel Then Throw New OperationCanceledException($"{activity} canceled;. ")

                activity = $"{resourceTitle}:{resourceName} handling opened actions"

                ' this clears any existing errors using SDC, RST, and CLS.  
                Me.OnOpened(EventArgs.Empty)

                If Me.IsDeviceOpen Then
                    activity = $"tagging {resourceTitle}:{resourceName} as open"
                    MyBase.OpenResource(resourceName, resourceTitle)
                    ' this causes a second device reset in order to initialize all the subsystems.
                    ' which is a duplication.  Possibly at this point there is no need to do a full reset etc.
                    activity = $"{resourceTitle}:{resourceName} handling initialization"
                    Me.OnInitializing(e)
                    If e.Cancel Then Throw New OperationCanceledException($"{activity} canceled;. ")
                    Me.OnInitialized(e)
                Else
                    Throw New isr.Core.OperationFailedException($"{activity} failed;. ")
                End If

            Else
                Throw New isr.Core.OperationFailedException($"{activity} failed;. ")
            End If
            success = True
        Catch
            Throw
        Finally
            If Not success Then
                Me.PublishWarning($"{resourceName} failed connecting. Disconnecting.")
                Me.TryCloseSession()
            End If
        End Try
    End Sub

    ''' <summary> Allows the derived device to take actions after closing. </summary>
    ''' <remarks> This override should occur as the last call of the overriding method. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosed(ByVal e As EventArgs)
        MyBase.OnClosed(e)
    End Sub

    ''' <summary> Closes the session. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Public Overridable Sub CloseSession()
        Dim activity As String = String.Empty
        Try
            ' check if already closed.
            If Me.IsDisposed OrElse Not Me.IsDeviceOpen Then Return
            Dim e As New CancelEventArgs
            activity = $"{Me.OpenResourceTitle}:{Me.OpenResourceName} handling closing actions"
            Me.OnClosing(e)
            If e.Cancel Then
                Me.PublishWarning($"{activity} canceled;. ")
            Else
                activity = $"{Me.CandidateResourceTitle} removing service request handler"
                Me.RemoveServiceRequestEventHandler()
                activity = $"{Me.CandidateResourceTitle} disabling service request handler"
                Me.Session.DisableServiceRequestEventHandler()
                activity = $"{Me.CandidateResourceTitle} closing session"
                Me.Session.CloseSession()
                activity = $"{Me.CandidateResourceTitle} handling closed actions"
                Me.OnClosed(EventArgs.Empty)
            End If
        Catch ex As VI.Pith.NativeException
            Throw New isr.Core.OperationFailedException($"Failed {activity} while closing the VISA session.", ex)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Exception occurred {activity} while closing the session.", ex)
        End Try
    End Sub

#End Region

#Region " OPENER DEVICE or STATUS ONLY "

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks>
    ''' This override should occur as the first call of the overriding method. After this call, the
    ''' parent class adds the subsystems.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Me.SubsystemSupportMode = SubsystemSupportMode.StatusOnly Then
            If Me.Subsystems.Count > 1 Then
                Me.PublishWarning($"{Me.ResourceNameCaption} subsystem count {Me.Subsystems.Count} on opening;. ")
                Me.Subsystems.Clear()
                Me.Subsystems.Add(Me.StatusSubsystemBase)
            Else
                Me.IsInitialized = False
            End If
        End If
        MyBase.OnOpening(e)
    End Sub

    ''' <summary> Allows the derived device to take actions after opening. </summary>
    ''' <remarks>
    ''' This override should occur as the last call of the overriding method. The subsystems are
    ''' added as part of the <see cref="OnOpening(CancelEventArgs)"/> method. The synchronization
    ''' context is captured as part of the property change and other event handlers and is no longer
    ''' needed here.
    ''' </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpened(ByVal e As EventArgs)
        If e Is Nothing Then e = EventArgs.Empty
        If Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            ' saving the resource names in full mode only
            Me.OpenResourceTitle = Me.Session.OpenResourceTitle
            Me.OpenResourceName = Me.Session.OpenResourceName
            Dim outcome As TraceEventType = TraceEventType.Information
            If Me.Session.Enabled And Not Me.Session.IsSessionOpen Then outcome = TraceEventType.Warning
            Me.Publish(outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ",
                       Me.ResourceNameCaption, Me.Session.Enabled.GetHashCode, Me.Session.IsDeviceOpen.GetHashCode, Me.Session.IsSessionOpen.GetHashCode)
            ResourceNameInfoCollection.AddNewResource(Me.OpenResourceName)
        End If

        ' A talker is assigned to the subsystem when the device is constructed. 
        ' This talker is assigned to each subsystem when it is added.
        ' Me.Subsystems.AssignTalker(Me.Talker)

        ' The synchronization context is captured as part of the property change and other event
        ' handlers and is no longer needed here.

        ' reset and clear the device to remove any existing errors.
        If Me.SubsystemSupportMode <> SubsystemSupportMode.Native Then Me.StatusSubsystemBase.OnDeviceOpen()

        ' 20181219: this is included in the base method: Me.NotifyPropertyChanged(NameOf(VisaSessionBase.IsDeviceOpen))
        ' 2016/01/18: this was done before adding listeners, which was useful when using the device
        ' as a class in a 'meter'. As a result, the actions taken when handling the Opened event, 
        ' such as Reset and Initialize do not get reported. 
        ' The solution was to add the device Initialize event to process publishing and initialization of device
        ' and subsystems.
        MyBase.OnOpened(e)
    End Sub

    ''' <summary> Opens a resource. </summary>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The resource title. </param>
    Public Overrides Sub OpenResource(ByVal resourceName As String, ByVal resourceTitle As String)
        Me.OpenSession(resourceName, resourceTitle)
        If Me.IsOpen Then
            ' at this point, the resource might have been updated using the instrument identity information.
            MyBase.OpenResource(Me.OpenResourceName, Me.OpenResourceTitle)
        Else
            MyBase.OpenResource(resourceName, resourceTitle)
        End If
    End Sub

    ''' <summary> Try open session. </summary>
    ''' <param name="resourceName">  Name of the resource. </param>
    ''' <param name="resourceTitle"> The resource title. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String); Success: <c>True</c> if session opened;
    ''' otherwise <c>False</c>
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Function TryOpenSession(ByVal resourceName As String, ByVal resourceTitle As String) As (Success As Boolean, Details As String)
        Dim activity As String = $"opening VISA session to {resourceTitle}:{resourceName}"
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            Me.OpenSession(resourceName, resourceTitle)
        Catch ex As Exception
            success = False
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            Me.PublishException(activity, ex)
        End Try
        Return (success AndAlso Me.IsDeviceOpen, details)
    End Function

    ''' <summary> Closes the resource. </summary>
    Public Overrides Sub CloseResource()
        Me.CloseSession()
        MyBase.CloseResource()
    End Sub

    ''' <summary> Try close session. </summary>
    ''' <returns>
    ''' The (Success As Boolean, Details As String); Success: <c>True</c> if session closed;
    ''' otherwise <c>False</c>
    ''' </returns>
    Public Overridable Function TryCloseSession() As (Success As Boolean, Details As String)
        Dim activity As String = $"closing VISA session to {Me.OpenResourceTitle}:{Me.OpenResourceName}"
        Dim success As Boolean = True
        Dim details As String = String.Empty
        Try
            Me.CloseResource()
            If Not Me.IsDeviceOpen Then details = $"failed {activity}"
        Catch ex As isr.Core.OperationFailedException
            success = False
            details = $"Exception {activity};. {ex.ToFullBlownString}"
            Me.PublishException(activity, ex)
        End Try
        Return (success AndAlso Not Me.IsDeviceOpen, details)
    End Function

    ''' <summary> The subsystem support mode. </summary>
    Private _SubsystemSupportMode As SubsystemSupportMode

    ''' <summary> Gets or sets the subsystem support mode. </summary>
    ''' <value> The subsystem support mode. </value>
    Public Property SubsystemSupportMode As SubsystemSupportMode
        Get
            Return Me._SubsystemSupportMode
        End Get
        Set(value As SubsystemSupportMode)
            If value <> Me.SubsystemSupportMode Then
                Me._SubsystemSupportMode = value
                Me.NotifyOpenChanged()
            End If
        End Set
    End Property

#End Region

#Region " INITIALIZE "

    ''' <summary> Executes the initializing actions. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnInitializing(ByVal e As CancelEventArgs)
        If e IsNot Nothing Then
            MyBase.OnInitializing(e)
            ' if starting a Visa Session (w/o a status subsystem, skip initializing.
            If e IsNot Nothing AndAlso Not e.Cancel Then
                ' 16-2-18: replaces calls to reset and then init known states
                ' Me.ResetClearInit()
                ' 12/2019: Reset clear init is already part of the On Opened function.
                If Me.SubsystemSupportMode = SubsystemSupportMode.StatusOnly Then
                    Me.StatusSubsystemBase.DefineKnownResetState()
                    Me.StatusSubsystemBase.DefineClearExecutionState()
                    Me.StatusSubsystemBase.InitKnownState()
                ElseIf Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
                    Me.DefineResetKnownState()
                    Me.DefineClearExecutionState()
                    Me.InitKnownState()
                End If
            End If
        End If
    End Sub

    ''' <summary> Executes the initialized action. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnInitialized(e As EventArgs)
        Dim hasIdentity As Boolean = Me.StatusSubsystemBase?.Identity IsNot Nothing
        Me.Identity = If(hasIdentity, Me.StatusSubsystemBase.Identity, String.Empty)
        If Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            ' resource name is saved only with full support
            ' update the session open resource title, which updates the visa session base title as well.
            If Not String.IsNullOrEmpty(Me.StatusSubsystemBase.VersionInfoBase.Model) Then
                Me.Session.OpenResourceTitle = Me.StatusSubsystemBase.VersionInfoBase.Model
            End If
        End If
        MyBase.OnInitialized(e)
    End Sub

#End Region

End Class

''' <summary> Values that represent session subsystem support mode. </summary>
Public Enum SubsystemSupportMode

    ''' <summary> An enum constant representing the full option. </summary>
    <Description("Full subsystems support")>
    Full

    ''' <summary> An enum constant representing the status only option. </summary>
    <Description("Status subsystem support")>
    StatusOnly

    ''' <summary> An enum constant representing the native option. </summary>
    <Description("No subsystem support")>
    Native
End Enum

