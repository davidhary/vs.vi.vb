'---------------------------------------------------------------------------------------------------
' file:		Pith\Code\ResourceNameInfo.vb
'
' summary:	Resource name information class
'---------------------------------------------------------------------------------------------------
Imports System.IO
Imports System.Reflection
Imports System.Security

Imports isr.Core
Imports System.Runtime.CompilerServices

''' <summary> Information about the resource name. </summary>
''' <remarks>
''' David, 2020-06-06. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class ResourceNameInfo

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    Public Sub New(ByVal resourceName As String)
        MyBase.New
        Me.ResourceName = resourceName
        Me.ParseThis(resourceName)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="fields"> The fields. </param>
    Public Sub New(ByVal fields As IEnumerable(Of String))
        MyBase.New()
        If fields?.Any Then
            Dim queue As New Queue(Of String)(fields)
            If queue.Any Then Me.ResourceName = queue.Dequeue
            Me.ParseThis(Me.ResourceName)
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="resourceName">    The name of the resource. </param>
    ''' <param name="interfaceType">   The type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number. </param>
    Public Sub New(ByVal resourceName As String, ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal interfaceNumber As Integer)
        MyBase.New()
        Me._ResourceName = resourceName
        Me._InterfaceType = interfaceType
        Me._InterfaceNumber = interfaceNumber
        Me._InterfaceBaseName = ResourceNamesManager.InterfaceResourceBaseName(Me._InterfaceType)
        Me._InterfaceResourceName = ResourceNamesManager.BuildInterfaceResourceName(Me._InterfaceBaseName, Me._InterfaceNumber)
        Me._ResourceType = ResourceNamesManager.ParseResourceType(resourceName)
        Me._ResourceAddress = String.Empty
        If Me.ResourceType = ResourceType.Instrument Then
            Me._ResourceAddress = ResourceNamesManager.ParseAddress(resourceName)
        End If
        If Me._InterfaceType = HardwareInterfaceType.Gpib Then
            If Not Integer.TryParse(Me._ResourceAddress, Me._GpibAddress) Then
                Me._GpibAddress = 0
            End If
        End If
    End Sub

    ''' <summary> Parses. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    Private Sub ParseThis(ByVal resourceName As String)
        Me._ResourceName = resourceName
        Me._InterfaceType = ResourceNamesManager.ParseHardwareInterfaceType(resourceName)
        Me._InterfaceNumber = ResourceNamesManager.ParseInterfaceNumber(resourceName)
        Me._InterfaceBaseName = ResourceNamesManager.InterfaceResourceBaseName(Me._InterfaceType)
        Me._InterfaceResourceName = ResourceNamesManager.BuildInterfaceResourceName(Me._InterfaceBaseName, Me._InterfaceNumber)
        Me._ResourceType = ResourceNamesManager.ParseResourceType(resourceName)
        Me._ResourceAddress = String.Empty
        If Me.ResourceType = ResourceType.Instrument Then
            Me._ResourceAddress = ResourceNamesManager.ParseAddress(resourceName)
        End If
        If Me._InterfaceType = HardwareInterfaceType.Gpib Then
            If Not Integer.TryParse(Me._ResourceAddress, Me._GpibAddress) Then
                Me._GpibAddress = 0
            End If
        End If
        Me.UsingLanController = Globalization.CultureInfo.InvariantCulture.CompareInfo.IndexOf(resourceName, "gpib0,", Globalization.CompareOptions.OrdinalIgnoreCase) >= 0
    End Sub

    ''' <summary> Parses resource name. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    Public Sub Parse(ByVal resourceName As String)
        Me.ParseThis(resourceName)
    End Sub

#End Region

#Region " FIELDS "

    ''' <summary> Gets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName As String

    ''' <summary> Gets the type of the resource. </summary>
    ''' <value> The type of the resource. </value>
    Public Property ResourceType As ResourceType

    ''' <summary> Gets the type of the interface. </summary>
    ''' <value> The type of the interface. </value>
    Public Property InterfaceType As VI.Pith.HardwareInterfaceType

    ''' <summary> Gets the interface number. </summary>
    ''' <value> The interface number. </value>
    Public Property InterfaceNumber As Integer

    ''' <summary> Gets the resource address. </summary>
    ''' <value> The resource address. </value>
    Public Property ResourceAddress As String

    ''' <summary> Gets the gpib address. </summary>
    ''' <value> The gpib address. </value>
    Public Property GpibAddress As Integer

    ''' <summary> Gets the sentinel indicating if the resource uses a LAN controller. </summary>
    ''' <value> The sentinel indicating if the resource uses a LAN controller. </value>
    Public Property UsingLanController As Boolean

    ''' <summary> Gets the is parsed. </summary>
    ''' <value> The is parsed. </value>
    Public ReadOnly Property IsParsed As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ResourceName)
        End Get
    End Property

    ''' <summary> Gets the name of the interface base. </summary>
    ''' <value> The name of the interface base. </value>
    Public ReadOnly Property InterfaceBaseName As String

    ''' <summary> Gets the name of the interface resource. </summary>
    ''' <value> The name of the interface resource. </value>
    Public ReadOnly Property InterfaceResourceName() As String

#End Region

#Region " STORAGE "

    ''' <summary> Builds header record. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildHeaderRecord(ByVal delimiter As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.Append(NameOf(ResourceNameInfo.ResourceName))
        builder.Append(delimiter)
        builder.Append(NameOf(ResourceNameInfo.InterfaceBaseName))
        Return builder.ToString
    End Function

    ''' <summary> Builds data record. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> A String. </returns>
    Public Function BuildDataRecord(ByVal delimiter As String) As String
        Dim builder As New System.Text.StringBuilder
        builder.Append(Me.ResourceName)
        builder.Append(delimiter)
        builder.Append(Me.InterfaceBaseName)
        Return builder.ToString
    End Function

#End Region

End Class

''' <summary> Collection of resource name informations. </summary>
''' <remarks>
''' David, 2020-06-06. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class ResourceNameInfoCollection
    Inherits ObjectModel.KeyedCollection(Of String, ResourceNameInfo)

#Region " CONSTRUCTION "


    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
    ''' equality comparer.
    ''' </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Public Sub New()
        MyBase.New
        Me._Keys = New List(Of String)
        Me.DefaultFileName = "VisaResources.txt"
        Me.BackupFileName = "VisaResourcesBackup.txt"
        Me.DefaultFolderName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), "isr.visa")
    End Sub


    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ResourceNameInfo) As String
        Return item.ResourceName
    End Function

#End Region

#Region " ITEM MANAGEMENT "


    ''' <summary>
    ''' Adds or replaces an object to the end of the
    ''' <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="item"> The object to be added to the end of the
    '''                     <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
    '''                     can be <see langword="null" /> for reference types. </param>
    Public Overloads Sub Add(ByVal item As ResourceNameInfo)
        If Me.Contains(item.ResourceName) Then Me.Remove(item.ResourceName)
        MyBase.Add(item)
        Me.Keys.Add(item.ResourceName)
    End Sub


    ''' <summary>
    ''' Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="resourceName"> The resource name to remove. </param>
    Public Overloads Sub Add(ByVal resourceName As String)
        Me.Add(New ResourceNameInfo(resourceName))
    End Sub

    ''' <summary> Removes the given resourceName. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="resourceName"> The resource name to remove. </param>
    Public Overloads Sub Remove(ByVal resourceName As String)
        MyBase.Remove(resourceName)
        Me.Keys.Remove(resourceName)
    End Sub


    ''' <summary>
    ''' Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Public Overloads Sub Clear()
        MyBase.Clear()
        Me.Keys.Clear()
    End Sub

    ''' <summary> Gets the keys. </summary>
    ''' <value> The keys. </value>
    Public ReadOnly Property Keys As IList(Of String)

    ''' <summary> Gets a list of names of the resources. </summary>
    ''' <value> A list of names of the resources. </value>
    Public ReadOnly Property ResourceNames As IList(Of String)
        Get
            Return Me.Keys
        End Get
    End Property

    ''' <summary> Populates the given items. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="items"> The items. </param>
    ''' <returns> An Integer. </returns>
    Public Function Populate(ByVal items As IList(Of ResourceNameInfo)) As Integer
        For Each item As ResourceNameInfo In items
            Me.Add(item)
        Next
        Return Me.Count
    End Function

    ''' <summary> Adds a new resource. </summary>
    ''' <remarks> David, 2020-06-08. </remarks>
    ''' <param name="resourceName"> The resource name to remove. </param>
    Public Shared Sub AddNewResource(ByVal resourceName As String)
        Dim rm As New ResourceNameInfoCollection
        rm.ReadResources()
        rm.Add(resourceName)
        rm.WriteResources()
        rm.BackupResources()
    End Sub
#End Region

#Region " FIND "

    ''' <summary> Enumerates the items in this collection that meet given criteria. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="pattern"> Specifies the pattern. </param>
    ''' <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
    Public Function Find(ByVal pattern As String) As IList(Of String)
        Dim coll As New ResourceNameInfoCollection()
        coll.Populate(Me.FindResourceNamesInfo(pattern))
        Return coll.ResourceNames
    End Function

    ''' <summary> Query if <see cref="ResourceNameInfo.ResourceName"/> matches the pattern. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <param name="resourceNameInfo"> Information describing the resource name. </param>
    ''' <param name="pattern">          Specifies the pattern. </param>
    ''' <returns> True if match, false if not. </returns>
    Public Shared Function IsMatch(ByVal resourceNameInfo As ResourceNameInfo, ByVal pattern As String) As Boolean
        Return resourceNameInfo.ResourceName Like pattern
    End Function

    ''' <summary> Finds the resource names informations in this collection. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="pattern"> Specifies the pattern. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the resource names informations in
    ''' this collection.
    ''' </returns>
    Public Function FindResourceNamesInfo(ByVal pattern As String) As IList(Of ResourceNameInfo)
        Return (From v As ResourceNameInfo In Me Where v.ResourceName Like pattern).ToList
    End Function

#End Region

#Region " STORAGE "

    ''' <summary> Opens a file. </summary>
    ''' <param name="fileName">   Filename of the file. </param>
    ''' <param name="openMode">   The open mode. </param>
    ''' <param name="accessMode"> The access mode. </param>
    ''' <param name="shareMode">  The share mode. </param>
    ''' <returns> An Integer. </returns>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Private Shared Function OpenFile(ByVal fileName As String,
                                     ByVal openMode As Microsoft.VisualBasic.OpenMode, ByVal accessMode As OpenAccess, ByVal shareMode As OpenShare) As Integer
        Dim fileNumber As Integer = 0
        Try
            fileNumber = Microsoft.VisualBasic.FreeFile()
            ' TO_DO: Use task; See VI Session Base Await Service request
            Dim trialCount As Integer = 10
            Do While trialCount > 0
                trialCount -= 1
                Try
                    Microsoft.VisualBasic.FileOpen(fileNumber, fileName, openMode, accessMode, shareMode)
                    trialCount = 0
                    Exit Do
                Catch
                    If trialCount = 1 Then Throw
                End Try
                If trialCount > 0 Then
                    isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(100))
                End If
            Loop
        Catch
            ' close to meet strong guarantees
            Try : CloseFile(fileNumber) : Finally : End Try
            Throw
        Finally
            isr.Core.ApplianceBase.DoEvents()
        End Try
        Return fileNumber
    End Function

    ''' <summary> Closes a file. </summary>
    ''' <param name="fileNumber"> The file number. </param>
    <System.Security.Permissions.FileIOPermission(Security.Permissions.SecurityAction.Demand, Unrestricted:=True)>
    Private Shared Sub CloseFile(ByVal fileNumber As Integer)
        ' close the file if not closed
        If fileNumber <> 0 Then Microsoft.VisualBasic.FileClose(fileNumber)
    End Sub

    ''' <summary> Query if this  is file exists. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns> True if file exists, false if not. </returns>
    Public Function IsFileExists() As Boolean
        Return New FileInfo(Me.DefaultFullFileName).Exists
    End Function

    ''' <summary> The delimiter. </summary>
    Private Const _Delimiter As String = "|"

    ''' <summary> Reads resources from file. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="filename"> The filename to read. </param>
    Public Sub ReadResources(ByVal filename As String)
        Me.Clear()
        Dim fields As String()
        Dim fileNumber As Integer = 0
        Dim fi As New FileInfo(filename)
        ' file has to be written before it is read.
        If Not fi.Exists Then Return
        Try
            fileNumber = ResourceNameInfoCollection.OpenFile(filename, OpenMode.Input, OpenAccess.Read, OpenShare.LockWrite)
            Using reader As New Microsoft.VisualBasic.FileIO.TextFieldParser(filename)
                reader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
                reader.Delimiters = New String() {ResourceNameInfoCollection._Delimiter}
                Dim isHeaderRow As Boolean = True
                While Not reader.EndOfData
                    fields = reader.ReadFields()
                    If Not isHeaderRow AndAlso (fields IsNot Nothing) AndAlso fields.Length > 0 Then
                        Me.Add(New ResourceNameInfo(fields))
                    End If
                    isHeaderRow = False
                End While
            End Using

        Catch
            Throw
        Finally
            ' close to meet strong guarantees
            Try : CloseFile(fileNumber) : Finally : End Try
        End Try
    End Sub

    ''' <summary> Reads resources from default file. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Public Sub ReadResources()
        Me.ReadResources(Me.DefaultFullFileName)
        Dim fi As New FileInfo(Me.BackupFullFileName)
        If Not fi.Exists Then Me.WriteResources(Me.BackupFullFileName)
    End Sub

    ''' <summary> Restore resources. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    Public Sub RestoreResources()
        Dim fi As New FileInfo(Me.BackupFullFileName)
        If Not fi.Exists Then Me.WriteResources(Me.BackupFullFileName)
        Me.ReadResources(Me.BackupFullFileName)
    End Sub

    ''' <summary> Writes resources to file. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="filename"> The filename to read. </param>
    Public Sub WriteResources(ByVal filename As String)
        Dim fileNumber As Integer = 0
        Try
            Dim fi As New FileInfo(filename)
            If Not fi.Directory.Exists Then
                fi.Directory.Create()
            End If
            fileNumber = ResourceNameInfoCollection.OpenFile(filename, OpenMode.Output, OpenAccess.Write, OpenShare.LockReadWrite)
            Microsoft.VisualBasic.PrintLine(fileNumber, ResourceNameInfo.BuildHeaderRecord(ResourceNameInfoCollection._Delimiter))
            For Each resourceNameInfo As ResourceNameInfo In Me
                Microsoft.VisualBasic.PrintLine(fileNumber, resourceNameInfo.BuildDataRecord(ResourceNameInfoCollection._Delimiter))
            Next
        Catch
            Throw
        Finally
            ' close to meet strong guarantees
            Try : CloseFile(fileNumber) : Finally : End Try
        End Try
    End Sub

    ''' <summary> Writes resources to default file. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Public Sub WriteResources()
        Me.WriteResources(Me.DefaultFullFileName)
        Dim fi As New FileInfo(Me.BackupFullFileName)
        If Not fi.Exists Then Me.WriteResources(Me.BackupFullFileName)
    End Sub

    ''' <summary> Backup resources. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    Public Sub BackupResources()
        Me.WriteResources(Me.BackupFullFileName)
    End Sub

    ''' <summary> Gets the default folder name. </summary>
    ''' <value> The default folder name. </value>
    Public Property DefaultFolderName As String

    ''' <summary> Gets the default file name. </summary>
    ''' <value> The default file name. </value>
    Public Property DefaultFileName As String

    ''' <summary> Gets the default full file name. </summary>
    ''' <value> The default full file name. </value>
    Public ReadOnly Property DefaultFullFileName As String
        Get
            Return Path.Combine(Me.DefaultFolderName, Me.DefaultFileName)
        End Get
    End Property

    ''' <summary> Gets the filename of the backup file. </summary>
    ''' <value> The filename of the backup file. </value>
    Public Property BackupFileName As String

    ''' <summary> Gets the full filename of the backup file. </summary>
    ''' <value> The full filename of the backup file. </value>
    Public ReadOnly Property BackupFullFileName As String
        Get
            Return Path.Combine(Me.DefaultFolderName, Me.BackupFileName)
        End Get
    End Property

#End Region

End Class
