'---------------------------------------------------------------------------------------------------
' file:		.\Views\DisplayView.vb
'
' summary:	Display view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.Core.Forma
Imports isr.Core.WinForms.BindingExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A control for handling status display and control. </summary>
''' <remarks>
''' David, 2018-12-20, 6.0.6928. <para>
'''   <div>Icons made by
'''   <a href="https://www.flaticon.com/authors/darius-dan" title="Darius Dan">Darius Dan</a>
'''   from <a href="https://www.flaticon.com/" title="Flat Icon">www.flaticon.com</a></div>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
<Description("Displays instrument measurement, status and error")>
<System.Drawing.ToolboxBitmap(GetType(DisplayView), "DisplayView"), ToolboxItem(True)>
Public Class DisplayView
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.BindSettings()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="DisplayView"/> </summary>
    ''' <returns> A <see cref="DisplayView"/>. </returns>
    Public Shared Function Create() As DisplayView
        Dim view As DisplayView = Nothing
        Try
            view = New DisplayView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM METHODS "

    ''' <summary> Layout resize. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Layout_Resize(sender As Object, e As EventArgs) Handles _Layout.Resize
        If Me.InitializingComponents Then Return
        Me.Height = Me._Layout.Height
    End Sub

#End Region

#Region " VISA SESSION BASE: DEVICE "

    ''' <summary> The visa session base. </summary>
    Private _VisaSessionBase As VisaSessionBase

    ''' <summary> Gets the visa session base. </summary>
    ''' <value> The visa session base. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase
        Get
            Return Me._VisaSessionBase
        End Get
    End Property

    ''' <summary> Binds the visa session base (device base) to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session base (device base) view model. </param>
    Public Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            Me.BindViewModel(False, Me.VisaSessionBase)
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed
            Me._VisaSessionBase = Nothing
        End If
        If visaSessionBase IsNot Nothing Then
            Me._VisaSessionBase = visaSessionBase
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed
            Me.BindViewModel(True, Me.VisaSessionBase)
        End If
        Me.BindSessionBase(visaSessionBase)
        Me.BindStatusSubsystemBase(visaSessionBase)
    End Sub

    ''' <summary> Bind view model. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The status subsystem view model. </param>
    Private Sub BindViewModel(ByVal add As Boolean, ByVal viewModel As VisaSessionBase)
        Me.AddRemoveBinding(Me._ServiceRequestHandledLabel, add, NameOf(Control.Visible), viewModel, NameOf(VisaSessionBase.ServiceRequestHandlerAssigned), DataSourceUpdateMode.Never)
    End Sub

    ''' <summary> Handles the device Close. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device closed"
            ' TO_DO: Doe we need this? Me.BindStatusSubsystemBase(Me.VisaSessionBase)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the device opened. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device opened"
            ' TO_DO: Doe we need this? Me.BindStatusSubsystemBase(me.VisaSessionBase)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SESSION BASE "

    ''' <summary> Gets or sets the session. </summary>
    ''' <value> The session. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SessionBase As isr.VI.Pith.SessionBase

    ''' <summary> Binds the Session base to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session base. </param>
    Private Sub BindSessionBase(ByVal visaSessionBase As isr.VI.VisaSessionBase)
        If visaSessionBase Is Nothing Then
            Me.BindSessionBaseThis(Nothing)
        Else
            Me.BindSessionBaseThis(visaSessionBase.Session)
        End If
    End Sub

    ''' <summary> Bind session base. </summary>
    ''' <param name="sessionBase"> The session. </param>
    Private Sub BindSessionBaseThis(ByVal sessionBase As isr.VI.Pith.SessionBase)
        If Me.SessionBase IsNot Nothing Then
            Me.BindSessionViewModel(False, Me.SessionBase)
            Me._SessionBase = Nothing
        End If
        Me._TitleStatusStrip.Visible = sessionBase IsNot Nothing
        Me._SessionReadingStatusStrip.Visible = sessionBase IsNot Nothing
        Me._StatusStrip.Visible = sessionBase IsNot Nothing OrElse Me.StatusSubsystemBase IsNot Nothing
        ' hide the sense and source status strip if nothing was assigned
        Me._MeasureStatusStrip.Visible = Me._MeasureToolstripSubsystemBase IsNot Nothing
        Me._SubsystemStatusStrip.Visible = Me._SubsystemToolStripSentinel IsNot Nothing
        If sessionBase IsNot Nothing Then
            Me._SessionBase = sessionBase
            Me.BindSessionViewModel(True, Me.SessionBase)
        End If
    End Sub

    ''' <summary> Bind visibility. </summary>
    ''' <param name="label">     The label. </param>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The status subsystem view model. </param>
    ''' <param name="bitValue">  The bit value (must include only a single bit). </param>
    Private Sub BindVisibility(ByVal label As isr.Core.Controls.ToolStripStatusLabel, ByVal add As Boolean, ByVal viewModel As isr.VI.Pith.SessionBase, ByVal bitValue As Pith.ServiceRequests)
        Dim binding As Binding = Me.AddRemoveBinding(label, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.ServiceRequestStatus), DataSourceUpdateMode.Never)
        Dim eventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                      If e IsNot Nothing AndAlso e.DesiredType Is GetType(Boolean) Then
                                                          e.Value = 0 <> (bitValue And CType(e.Value, Pith.ServiceRequests))
                                                      End If
                                                  End Sub
        If add Then
            AddHandler binding.Format, eventHandler
        Else
            RemoveHandler binding.Format, eventHandler
        End If
    End Sub

    ''' <summary> Bind visibility. </summary>
    ''' <param name="label">     The label. </param>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The status subsystem view model. </param>
    ''' <param name="bitValue">  The bit value (must include only a single bit). </param>
    Private Sub BindVisibility(ByVal label As isr.Core.Controls.ToolStripStatusLabel, ByVal add As Boolean, ByVal viewModel As isr.VI.Pith.SessionBase, ByVal bitValue As Pith.StandardEvents)
        Dim binding As Binding = Me.AddRemoveBinding(label, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.StandardEventStatus), DataSourceUpdateMode.Never)
        Dim eventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                      If e IsNot Nothing AndAlso e.DesiredType Is GetType(Boolean) Then
                                                          e.Value = 0 <> (bitValue And CType(e.Value, Pith.StandardEvents))
                                                      End If
                                                  End Sub
        If add Then
            AddHandler binding.Format, eventHandler
        Else
            RemoveHandler binding.Format, eventHandler
        End If
    End Sub

    ''' <summary> Binds the Session view model to its controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The status subsystem view model. </param>
    Private Sub BindSessionViewModel(ByVal add As Boolean, ByVal viewModel As isr.VI.Pith.SessionBase)
        Me.AddRemoveBinding(Me._TitleLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.ResourceTitleCaption), DataSourceUpdateMode.Never)
        Me.AddRemoveBinding(Me._StatusRegisterLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.StatusRegisterCaption), DataSourceUpdateMode.Never)
        If My.Settings.DisplayStandardServiceRequests Then
            Me.BindVisibility(Me._MeasurementEventBitLabel, add, viewModel, Pith.ServiceRequests.MeasurementEvent)
            Me.BindVisibility(Me._SystemEventBitLabel, add, viewModel, Pith.ServiceRequests.SystemEvent)
            Me.BindVisibility(Me._ErrorAvailableBitLabel, add, viewModel, Pith.ServiceRequests.ErrorAvailable)
            Me.BindVisibility(Me._MessageAvailableBitLabel, add, viewModel, Pith.ServiceRequests.MessageAvailable)
            Me.BindVisibility(Me._QuestionableSummaryBitLabel, add, viewModel, Pith.ServiceRequests.QuestionableEvent)
            Me.BindVisibility(Me._EventSummaryBitLabel, add, viewModel, Pith.ServiceRequests.StandardEvent)
            Me.BindVisibility(Me._ServiceRequestBitLabel, add, viewModel, Pith.ServiceRequests.RequestingService)
        Else
            Me.AddRemoveBinding(Me._MeasurementEventBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.HasMeasurementEvent), DataSourceUpdateMode.Never)
            Me.AddRemoveBinding(Me._SystemEventBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.HasSystemEvent), DataSourceUpdateMode.Never)
            Me.AddRemoveBinding(Me._ErrorAvailableBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.ErrorAvailable), DataSourceUpdateMode.Never)
            Me.AddRemoveBinding(Me._MessageAvailableBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.MessageAvailable), DataSourceUpdateMode.Never)
            Me.AddRemoveBinding(Me._QuestionableSummaryBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.HasQuestionableEvent), DataSourceUpdateMode.Never)
            Me.AddRemoveBinding(Me._EventSummaryBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.HasStandardEvent), DataSourceUpdateMode.Never)
            Me.AddRemoveBinding(Me._ServiceRequestBitLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.RequestingService), DataSourceUpdateMode.Never)
        End If
        Me.AddRemoveBinding(Me._ServiceRequestEnabledLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.ServiceRequestEventEnabled), DataSourceUpdateMode.Never)
        Dim binding As Binding = Me.AddRemoveBinding(Me._ServiceRequestEnableBitmaskLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.ServiceRequestEnabledBitmask), DataSourceUpdateMode.Never)
        If add Then
            AddHandler binding.Format, BindingEventHandlers.DisplayX2EventHandler
        Else
            AddHandler binding.Format, BindingEventHandlers.DisplayX2EventHandler
        End If
        binding = Me.AddRemoveBinding(Me._StandardEventEnableBitmaskLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.StandardEventEnableBitmask), DataSourceUpdateMode.Never)
        If add Then
            AddHandler binding.Format, BindingEventHandlers.DisplayX2EventHandler
        Else
            AddHandler binding.Format, BindingEventHandlers.DisplayX2EventHandler
        End If
        Me.AddRemoveBinding(Me._SessionElapsedTimeLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.ElapsedTimeCaption), DataSourceUpdateMode.Never)
        Me.AddRemoveBinding(Me._StandardRegisterLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.Pith.SessionBase.StandardRegisterCaption), DataSourceUpdateMode.Never)
        Me.AddRemoveBinding(Me._StandardRegisterLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.Pith.SessionBase.StandardEventStatusHasValue), DataSourceUpdateMode.Never)

        Me.BindVisibility(Me._PowerOnEventLabel, add, viewModel, Pith.StandardEvents.PowerToggled)

        binding = Me.AddRemoveBinding(Me._SessionOpenCloseStatusLabel, add, NameOf(ToolStripLabel.Image), viewModel, NameOf(isr.VI.Pith.SessionBase.IsDeviceOpen), DataSourceUpdateMode.Never)
        Dim eventHandler As ConvertEventHandler = Sub(sender As Object, e As ConvertEventArgs)
                                                      e.ToggleImage(My.Resources.user_online_2, My.Resources.user_offline_2)
                                                  End Sub
        If add Then
            AddHandler binding.Format, eventHandler
        Else
            RemoveHandler binding.Format, eventHandler
        End If
        Me._SessionOpenCloseStatusLabel.Image = My.Resources.user_offline_2
    End Sub

#End Region

#Region " STATUS SUBSYSTEM BASE "

    ''' <summary> Gets or sets the status subsystem. </summary>
    ''' <value> The status subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property StatusSubsystemBase As StatusSubsystemBase

    ''' <summary> Bind status subsystem view model. </summary>
    ''' <param name="visaSessionBase"> The visa session view model. </param>
    Private Sub BindStatusSubsystemBase(ByVal visaSessionBase As VI.VisaSessionBase)
        If visaSessionBase Is Nothing Then
            Me.BindStatusSubsystemBaseThis(Nothing)
        Else
            Me.BindStatusSubsystemBaseThis(visaSessionBase.StatusSubsystemBase)
        End If
    End Sub

    ''' <summary> Bind status subsystem base. </summary>
    ''' <param name="statusSubsystemBase"> The status subsystem. </param>
    Private Sub BindStatusSubsystemBaseThis(ByVal statusSubsystemBase As StatusSubsystemBase)
        If Me.StatusSubsystemBase IsNot Nothing Then
            Me.BindStatusSubsystemViewModel(False, Me.StatusSubsystemBase)
            Me._StatusSubsystemBase = Nothing
        End If
        Me._ErrorStatusStrip.Visible = statusSubsystemBase IsNot Nothing
        Me._StatusStrip.Visible = statusSubsystemBase IsNot Nothing OrElse Me.SessionBase IsNot Nothing
        ' hide the sense and source status strip if nothing was assigned
        Me._MeasureStatusStrip.Visible = Me._MeasureToolstripSubsystemBase IsNot Nothing
        Me._SubsystemStatusStrip.Visible = Me._SubsystemToolStripSentinel IsNot Nothing
        If statusSubsystemBase IsNot Nothing Then
            Me._StatusSubsystemBase = statusSubsystemBase
            Me.BindStatusSubsystemViewModel(True, Me.StatusSubsystemBase)
        End If
    End Sub

    ''' <summary> Binds the StatusSubsystem view model to its controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="viewModel"> The status subsystem view model. </param>
    Private Sub BindStatusSubsystemViewModel(ByVal add As Boolean, ByVal viewModel As StatusSubsystemBase)
        Me.AddRemoveBinding(Me._ErrorLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.StatusSubsystemBase.CompoundErrorMessage))
        Me.AddRemoveBinding(Me._ErrorLabel, add, NameOf(Control.ForeColor), viewModel, NameOf(isr.VI.StatusSubsystemBase.ErrorForeColor))
        Me.AddRemoveBinding(Me._MeasurementEventStatusLabel, add, NameOf(Control.Visible), viewModel, NameOf(isr.VI.StatusSubsystemBase.MeasurementEventStatus), DataSourceUpdateMode.Never)
        Dim binding As Binding = Me.AddRemoveBinding(Me._MeasurementEventStatusLabel, add, NameOf(Control.Text), viewModel, NameOf(isr.VI.StatusSubsystemBase.MeasurementEventStatus), DataSourceUpdateMode.Never)
        If add Then
            AddHandler binding.Format, BindingEventHandlers.DisplayX2EventHandler
        Else
            AddHandler binding.Format, BindingEventHandlers.DisplayX2EventHandler
        End If
    End Sub

    ''' <summary> Event handler. Called by _ErrorLabel for text changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ErrorLabel_TextChanges(ByVal sender As Object, ByVal e As EventArgs) Handles _ErrorLabel.TextChanged
        Dim activity As String = String.Empty
        Try
            If Me.StatusSubsystemBase?.HasDeviceError OrElse Me.StatusSubsystemBase?.HasErrorReport Then
                Dim value As String = String.Empty
                value = If(Me.StatusSubsystemBase.HasErrorReport, Me.StatusSubsystemBase.DeviceErrorReport, Me.StatusSubsystemBase.CompoundErrorMessage)
                If Not String.IsNullOrWhiteSpace(value) Then
                    activity = $"logging error label text: {value}"
                    Me.PublishWarning($"error reported: {value}")
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MEASURE TOOL STRIP SUBSYSTEMS "

    ''' <summary>
    ''' Gets or sets the measure tool strip subsystem sentinel, which indicates if a measure
    ''' subsystem was assigned.
    ''' </summary>
    ''' <value> The measure subsystem sentinel. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property MeasureToolstripSubsystemBase As SubsystemBase

    ''' <summary> Releases the subsystem from its measure tool strip controls. </summary>
    ''' <param name="subsystem"> The System subsystem. </param>
    Public Sub ReleaseMeasureToolStrip(ByVal subsystem As SubsystemBase)
        If subsystem IsNot Nothing Then
            Dim bufferSubsystem As BufferSubsystemBase = TryCast(subsystem, BufferSubsystemBase)
            If bufferSubsystem IsNot Nothing Then
                Me.BindMeasureToolStrip(False, bufferSubsystem)
            Else
                Dim meterSubsystem As MultimeterSubsystemBase = TryCast(subsystem, MultimeterSubsystemBase)
                If meterSubsystem IsNot Nothing Then
                    Me.BindMeasureToolStrip(False, meterSubsystem)
                Else
                    Dim measureSubsystem As MeasureSubsystemBase = TryCast(subsystem, MeasureSubsystemBase)
                    If measureSubsystem IsNot Nothing Then
                        Me.BindMeasureToolStrip(False, measureSubsystem)
                    Else
                        Dim measureCurrentSubsystem As MeasureCurrentSubsystemBase = TryCast(subsystem, MeasureCurrentSubsystemBase)
                        If measureCurrentSubsystem IsNot Nothing Then
                            Me.BindMeasureToolStrip(False, measureCurrentSubsystem)
                        Else
                            Dim measureVoltageSubsystem As MeasureVoltageSubsystemBase = TryCast(subsystem, MeasureVoltageSubsystemBase)
                            If measureVoltageSubsystem IsNot Nothing Then
                                Me.BindMeasureToolStrip(False, measureVoltageSubsystem)
                            Else
                                Dim SenseFunctionSubsystem As SenseFunctionSubsystemBase = TryCast(subsystem, SenseFunctionSubsystemBase)
                                If SenseFunctionSubsystem IsNot Nothing Then
                                    Me.BindMeasureToolStrip(False, SenseFunctionSubsystem)
                                Else
                                    Dim SenseSubsystem As SenseSubsystemBase = TryCast(subsystem, SenseSubsystemBase)
                                    If SenseSubsystem IsNot Nothing Then
                                        Me.BindMeasureToolStrip(False, SenseSubsystem)
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

#Region " BUFFER SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the buffer  subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The buffer subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As BufferSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the buffer subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The buffer subsystem. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As BufferSubsystemBase)
        ' Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), viewModel, NameOf(isr.VI.BufferSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.BufferSubsystemBase.LastReadingStatus))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.BufferSubsystemBase.LastReadingStatus))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.BufferSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.BufferSubsystemBase.LastReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.BufferSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.BufferSubsystemBase.LastRawReading))
    End Sub

#End Region

#Region " MEASURE SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Measure subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As MeasureSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
        Me.BindTerminalsDisplay(subsystem)
    End Sub

    ''' <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Measure subsystem. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As MeasureSubsystemBase)
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), subsystem, NameOf(isr.VI.MeasureSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureSubsystemBase.FailureCode))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MeasureSubsystemBase.FailureLongDescription))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MeasureSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.MeasureSubsystemBase.ReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureSubsystemBase.LastReading))
    End Sub

#End Region

#Region " MEASURE CURRENT SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Measure Current subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As MeasureCurrentSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Measure Current subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Measure Current. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As MeasureCurrentSubsystemBase)
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.ReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.FailureCode))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.FailureLongDescription))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureCurrentSubsystemBase.LastReading))
    End Sub

#End Region

#Region " MEASURE VOLTAGE SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Measure Voltage subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Measure Voltage subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As MeasureVoltageSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Measure Voltage subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Measure Voltage subsystem. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As MeasureVoltageSubsystemBase)
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.ReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.FailureCode))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.FailureLongDescription))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureVoltageSubsystemBase.LastReading))
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Multimeter subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Multimeter subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As MultimeterSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Multimeter subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Multimeter subsystem. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As MultimeterSubsystemBase)
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.ReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.FailureCode))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.FailureLongDescription))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.LastReading))
    End Sub

#End Region

#Region " SENSE FUNCTION SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Sense Function subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Sense Function subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As SenseFunctionSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Sense Function subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Sense Function subsystem. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As SenseFunctionSubsystemBase)
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.ReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.FailureCode))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.FailureLongDescription))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SenseFunctionSubsystemBase.LastReading))
    End Sub

#End Region

#Region " SENSE SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Sense subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Sense subsystem. </param>
    Public Sub BindMeasureToolStrip(ByVal subsystem As SenseSubsystemBase)
        If Me.MeasureToolstripSubsystemBase IsNot Nothing Then
            Me.ReleaseMeasureToolStrip(Me.MeasureToolstripSubsystemBase)
        End If
        Me._MeasureToolstripSubsystemBase = subsystem
        Me._MeasureStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindMeasureToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Sense subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Sense Function subsystem. </param>
    Private Sub BindMeasureToolStrip(ByVal add As Boolean, ByVal subsystem As SenseSubsystemBase)
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.SenseSubsystemBase.LastReading))
        Me.AddRemoveBinding(Me._ReadingAmountLabel, add, NameOf(ToolStripItem.Text), subsystem, NameOf(isr.VI.SenseSubsystemBase.ReadingCaption))
        Me.AddRemoveBinding(Me._MeasureElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SenseSubsystemBase.ElapsedTimeCaption))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), subsystem, NameOf(isr.VI.SenseSubsystemBase.FailureColor))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SenseSubsystemBase.FailureCode))
        Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(ToolStripItem.ToolTipText), subsystem, NameOf(isr.VI.SenseSubsystemBase.FailureLongDescription))
        Me.AddRemoveBinding(Me._LastReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SenseSubsystemBase.LastReading))
    End Sub

#End Region

#End Region

#Region " TERMINALS LABEL BINDING "

    ''' <summary>
    ''' Gets or sets the terminals subsystem sentinel, which indicates if a terminals subsystem was
    ''' assigned.
    ''' </summary>
    ''' <value> The measure subsystem sentinel. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property TerminalsSubsystemBase As SubsystemBase

    ''' <summary> Releases the subsystem from its terminals controls. </summary>
    ''' <param name="subsystem"> The System subsystem. </param>
    Public Sub ReleaseTerminalsDisplay(ByVal subsystem As SubsystemBase)
        If subsystem IsNot Nothing Then
            Dim sysSubsystem As SystemSubsystemBase = TryCast(subsystem, SystemSubsystemBase)
            If sysSubsystem IsNot Nothing Then
                Me.BindTerminalsDisplay(False, sysSubsystem)
            Else
                Dim meterSubsystem As MultimeterSubsystemBase = TryCast(subsystem, MultimeterSubsystemBase)
                If meterSubsystem IsNot Nothing Then
                    Me.BindTerminalsDisplay(False, meterSubsystem)
                Else
                    Dim measureSubsystem As MeasureSubsystemBase = TryCast(subsystem, MeasureSubsystemBase)
                    If measureSubsystem IsNot Nothing Then
                        Me.BindTerminalsDisplay(False, measureSubsystem)
                    End If
                End If
            End If
        End If
    End Sub

#Region " MEASURE SUBSYSTEM "

    ''' <summary> Binds the Measure subsystem to the its terminals controls. </summary>
    ''' <param name="subsystem"> The System subsystem. </param>
    Public Sub BindTerminalsDisplay(ByVal subsystem As MeasureSubsystemBase)
        If subsystem?.SupportsFrontTerminalsSelectionQuery Then
            If Me.TerminalsSubsystemBase IsNot Nothing Then
                Me.ReleaseTerminalsDisplay(Me.TerminalsSubsystemBase)
            End If
            Me._TerminalsSubsystemBase = subsystem
            If subsystem IsNot Nothing Then
                Me.BindTerminalsDisplay(True, subsystem)
            End If
        End If
    End Sub

    ''' <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The System subsystem. </param>
    Private Sub BindTerminalsDisplay(ByVal add As Boolean, ByVal subsystem As MeasureSubsystemBase)
        If subsystem.SupportsFrontTerminalsSelectionQuery Then
            Me.AddRemoveBinding(Me._TerminalsStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MeasureSubsystemBase.TerminalsCaption))
            Me._TerminalsStatusLabel.Visible = add
            If add Then
                Me._TerminalsStatusLabel.Text = subsystem.TerminalsCaption
            End If
        End If
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM "

    ''' <summary> Binds the multimeter subsystem to the its terminals controls. </summary>
    ''' <param name="subsystem"> The System subsystem. </param>
    Public Sub BindTerminalsDisplay(ByVal subsystem As MultimeterSubsystemBase)
        If subsystem?.SupportsFrontTerminalsSelectionQuery Then
            If Me.TerminalsSubsystemBase IsNot Nothing Then
                Me.ReleaseTerminalsDisplay(Me.TerminalsSubsystemBase)
            End If
            Me._TerminalsSubsystemBase = subsystem
            If subsystem IsNot Nothing Then
                Me.BindTerminalsDisplay(True, subsystem)
            End If
        End If
    End Sub

    ''' <summary> Binds the multimeter subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The System subsystem. </param>
    Private Sub BindTerminalsDisplay(ByVal add As Boolean, ByVal subsystem As MultimeterSubsystemBase)
        If subsystem.SupportsFrontTerminalsSelectionQuery Then
            Me.AddRemoveBinding(Me._TerminalsStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.MultimeterSubsystemBase.TerminalsCaption))
            Me._TerminalsStatusLabel.Visible = add
            If add Then
                Me._TerminalsStatusLabel.Text = subsystem.TerminalsCaption
            End If
        End If
    End Sub

#End Region

#Region " SYSTEM SUBSYSTEM "

    ''' <summary> Binds the System subsystem to the its terminals controls. </summary>
    ''' <param name="subsystem"> The System subsystem. </param>
    Public Sub BindTerminalsDisplay(ByVal subsystem As SystemSubsystemBase)
        If subsystem?.SupportsFrontTerminalsSelectionQuery Then
            If Me.TerminalsSubsystemBase IsNot Nothing Then
                Me.ReleaseTerminalsDisplay(Me.TerminalsSubsystemBase)
            End If
            Me._TerminalsSubsystemBase = subsystem
            If subsystem IsNot Nothing Then
                Me.BindTerminalsDisplay(True, subsystem)
            End If
        End If
    End Sub

    ''' <summary> Binds the System subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The System subsystem. </param>
    Private Sub BindTerminalsDisplay(ByVal add As Boolean, ByVal subsystem As SystemSubsystemBase)
        If subsystem.SupportsFrontTerminalsSelectionQuery Then
            Me.AddRemoveBinding(Me._TerminalsStatusLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SystemSubsystemBase.TerminalsCaption))
            Me._TerminalsStatusLabel.Visible = add
            If add Then
                Me._TerminalsStatusLabel.Text = subsystem.TerminalsCaption
            End If
        End If
    End Sub

#End Region

#End Region

#Region " SUBSYSTEM TOOL STRIP SENTINEL "

    ''' <summary> Gets or sets sentinel for the subsystem tool strip. </summary>
    ''' <value> The sentinel for the subsystem tool strip. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SubsystemToolStripSentinel As SubsystemBase

    ''' <summary> Releases the subsystem from its SUBSYSTEM controls. </summary>
    ''' <param name="subsystem"> The System subsystem. </param>
    Public Sub ReleaseSubsystemToolStrip(ByVal subsystem As SubsystemBase)
        If subsystem IsNot Nothing Then
            Dim sourceSubsystem As SourceSubsystemBase = TryCast(subsystem, SourceSubsystemBase)
            If sourceSubsystem IsNot Nothing Then
                Me.BindSubsystemToolStrip(False, sourceSubsystem)
            Else
                Dim channelSubsystem As ChannelSubsystemBase = TryCast(subsystem, ChannelSubsystemBase)
                If channelSubsystem IsNot Nothing Then
                    Me.BindSubsystemToolStrip(False, channelSubsystem)
                Else
                    Dim triggerSubsystem As TriggerSubsystemBase = TryCast(subsystem, TriggerSubsystemBase)
                    If triggerSubsystem IsNot Nothing Then
                        Me.BindSubsystemToolStrip(False, triggerSubsystem)
                    End If
                End If
            End If
        End If
    End Sub


#Region " SOURCE SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Source subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Source subsystem. </param>
    Public Sub BindSubsystemToolStrip(ByVal subsystem As SourceSubsystemBase)
        If Me.SubsystemToolStripSentinel IsNot Nothing Then
            Me.ReleaseSubsystemToolStrip(Me.SubsystemToolStripSentinel)
        End If
        Me._SubsystemToolStripSentinel = subsystem
        Me._SubsystemStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindSubsystemToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Source subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Source subsystem. </param>
    Private Sub BindSubsystemToolStrip(ByVal add As Boolean, ByVal subsystem As SourceSubsystemBase)
        Me.AddRemoveBinding(Me._SubsystemReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SourceSubsystemBase.LevelCaption))
        Me.AddRemoveBinding(Me._SubsystemElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SubsystemBase.ElapsedTimeCaption))
    End Sub

#End Region

#Region " CHANNEL SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Channel subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Channel subsystem. </param>
    Public Sub BindSubsystemToolStrip(ByVal subsystem As ChannelSubsystemBase)
        If Me.SubsystemToolStripSentinel IsNot Nothing Then
            Me.ReleaseSubsystemToolStrip(Me.SubsystemToolStripSentinel)
        End If
        Me._SubsystemToolStripSentinel = subsystem
        Me._SubsystemStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindSubsystemToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Channel subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Channel subsystem. </param>
    Private Sub BindSubsystemToolStrip(ByVal add As Boolean, ByVal subsystem As ChannelSubsystemBase)
        Me.AddRemoveBinding(Me._SubsystemReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.ChannelSubsystemBase.ClosedChannelsCaption))
        Me.AddRemoveBinding(Me._SubsystemElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SubsystemBase.ElapsedTimeCaption))
    End Sub

#End Region

#Region " TRIGGER SUBSYSTEM MODEL VIEW "

    ''' <summary> Binds the Trigger subsystem to the its tool strip controls. </summary>
    ''' <param name="subsystem"> The Trigger subsystem. </param>
    Public Sub BindSubsystemToolStrip(ByVal subsystem As TriggerSubsystemBase)
        If Me.SubsystemToolStripSentinel IsNot Nothing Then
            Me.ReleaseSubsystemToolStrip(Me.SubsystemToolStripSentinel)
        End If
        Me._SubsystemToolStripSentinel = subsystem
        Me._SubsystemStatusStrip.Visible = subsystem IsNot Nothing
        If subsystem IsNot Nothing Then
            Me.BindSubsystemToolStrip(True, subsystem)
        End If
    End Sub

    ''' <summary> Binds the Trigger subsystem to the its tool strip controls. </summary>
    ''' <param name="add">       True to add; otherwise, remove. </param>
    ''' <param name="subsystem"> The Trigger subsystem. </param>
    Private Sub BindSubsystemToolStrip(ByVal add As Boolean, ByVal subsystem As TriggerSubsystemBase)
        Me.AddRemoveBinding(Me._SubsystemReadingLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.TriggerSubsystemBase.TriggerStateCaption))
        Me.AddRemoveBinding(Me._SubsystemElapsedTimeLabel, add, NameOf(Control.Text), subsystem, NameOf(isr.VI.SubsystemBase.ElapsedTimeCaption))
    End Sub

#End Region

#End Region

#Region " BIND SETTINGS "

    ''' <summary> Bind settings. </summary>
    Private Sub BindSettings()
        Me._Layout.BackColor = isr.VI.Facade.My.Settings.CharcoalColor
        Me._Layout.DataBindings.Add(New System.Windows.Forms.Binding(NameOf(Control.BackColor), isr.VI.Facade.My.Settings,
                                                                     NameOf(isr.VI.Facade.My.Settings.CharcoalColor), True,
                                                                     System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._SessionReadingStatusStrip.BackColor = isr.VI.Facade.My.Settings.CharcoalColor

        Me._SessionReadingStatusStrip.DataBindings.Add(New System.Windows.Forms.Binding(NameOf(Control.BackColor), isr.VI.Facade.My.Settings,
                                                                         NameOf(isr.VI.Facade.My.Settings.CharcoalColor), True,
                                                                         System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))

        Me._SubsystemStatusStrip.BackColor = isr.VI.Facade.My.Settings.CharcoalColor
        Me._SubsystemStatusStrip.DataBindings.Add(New System.Windows.Forms.Binding(NameOf(Control.BackColor), isr.VI.Facade.My.Settings,
                                                                     NameOf(isr.VI.Facade.My.Settings.CharcoalColor), True,
                                                                     System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))

        Me._TitleStatusStrip.BackColor = isr.VI.Facade.My.Settings.CharcoalColor
        Me._TitleStatusStrip.DataBindings.Add(New System.Windows.Forms.Binding(NameOf(Control.BackColor), isr.VI.Facade.My.Settings,
                                                                     NameOf(isr.VI.Facade.My.Settings.CharcoalColor), True,
                                                                     System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))

        Me._ErrorStatusStrip.BackColor = isr.VI.Facade.My.Settings.CharcoalColor
        Me._ErrorStatusStrip.DataBindings.Add(New System.Windows.Forms.Binding(NameOf(Control.BackColor), isr.VI.Facade.My.Settings,
                                                                     NameOf(isr.VI.Facade.My.Settings.CharcoalColor), True,
                                                                     System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))

        Me._StatusStrip.BackColor = isr.VI.Facade.My.Settings.CharcoalColor
        Me._StatusStrip.DataBindings.Add(New System.Windows.Forms.Binding(NameOf(Control.BackColor), isr.VI.Facade.My.Settings,
                                                                     NameOf(isr.VI.Facade.My.Settings.CharcoalColor), True,
                                                                     System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
