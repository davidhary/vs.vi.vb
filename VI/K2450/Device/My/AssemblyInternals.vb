'---------------------------------------------------------------------------------------------------
' file:		Device\My\MyLibrary_Internals.vb
'
' summary:	My library internals class
'---------------------------------------------------------------------------------------------------
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.VI.K2500.Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.Cinco.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Ohmni.Morphe.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.SharedPublicKey)>
<Assembly: Runtime.CompilerServices.InternalsVisibleTo("isr.Verrzzano.K2450Tests,PublicKey=" & Global.isr.VI.My.SolutionInfo.StrainPublicKey)>
