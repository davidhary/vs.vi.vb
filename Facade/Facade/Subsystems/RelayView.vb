'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\RelayView.vb
'
' summary:	Relay view class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

''' <summary> A relay view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class RelayView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._ChannelListBuilder = New VI.ChannelListBuilder

        Me._MemoryLocationNumeric.NumericUpDownControl.Minimum = 1
        Me._MemoryLocationNumeric.NumericUpDownControl.Maximum = 100
        Me._MemoryLocationNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._MemoryLocationNumeric.NumericUpDownControl.Value = 1

        Me._SlotNumberNumeric.NumericUpDownControl.Minimum = 1
        Me._SlotNumberNumeric.NumericUpDownControl.Maximum = 10
        Me._SlotNumberNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SlotNumberNumeric.NumericUpDownControl.Value = 1

        Me._RelayNumberNumeric.NumericUpDownControl.Minimum = 1
        Me._RelayNumberNumeric.NumericUpDownControl.Maximum = 100
        Me._RelayNumberNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._RelayNumberNumeric.NumericUpDownControl.Value = 1
    End Sub

    ''' <summary> Creates a new <see cref="RelayView"/> </summary>
    ''' <returns> A <see cref="RelayView"/>. </returns>
    Public Shared Function Create() As RelayView
        Dim view As RelayView = Nothing
        Try
            view = New RelayView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary>Gets or sets the channel list.</summary>
    Private _ChannelListBuilder As VI.ChannelListBuilder

    ''' <summary> Gets a list of channels. </summary>
    ''' <value> A List of channels. </value>
    Public ReadOnly Property ChannelList As String
        Get
            Return Me._ChannelListBuilder.ToString
        End Get
    End Property

    ''' <summary> Adds channel to list. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub AddChannelToList()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"adding channel [{CInt(Me._SlotNumberNumeric.Value)},{CInt(Me._RelayNumberNumeric.Value)}] to the list"
            If Me._ChannelListBuilder Is Nothing Then
                Me._ChannelListBuilder = New isr.VI.ChannelListBuilder
            End If
            Me._ChannelListBuilder.AddChannel(CInt(Me._SlotNumberNumeric.Value), CInt(Me._RelayNumberNumeric.Value))
            Me.NotifyPropertyChanged(NameOf(ChannelList))
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the channel list. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearChannelList()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"clearing channel list"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me._ChannelListBuilder = New isr.VI.ChannelListBuilder
            Me.NotifyPropertyChanged(NameOf(RelayView.ChannelList))
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Adds memory location to list. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub AddMemoryLocationToList()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"adding memory location {CInt(Me._MemoryLocationNumeric.Value)}"
            If Me._ChannelListBuilder Is Nothing Then
                Me._ChannelListBuilder = New isr.VI.ChannelListBuilder
            End If
            Me._ChannelListBuilder.AddChannel(CInt(Me._MemoryLocationNumeric.Value))
            Me.NotifyPropertyChanged(NameOf(RelayView.ChannelList))
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Adds a channel to list click to 'e'. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddChannelToList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AddRelayMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.AddChannelToList()
    End Sub

    ''' <summary> Adds a memory location button click to 'e'. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddMemoryLocationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AddMemoryLocationToListMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.AddMemoryLocationToList()
    End Sub

    ''' <summary> Clears the channel ist menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearChannelLIstMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearChannelLIstMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ClearChannelList()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
