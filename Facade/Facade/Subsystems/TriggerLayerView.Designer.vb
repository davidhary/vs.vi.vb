<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TriggerLayerView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TriggerLayerView))
        Me._ToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SendBusTriggerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CountNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._CountNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._InfiniteCountButton = New System.Windows.Forms.ToolStripButton()
        Me._SourceComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SourceComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._TriggerConfigurationToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TriggerCondigureLabel = New System.Windows.Forms.ToolStripLabel()
        Me._InputLineNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._InputLineNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._OutputLineNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._OutputLineNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._BypassToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._TimingToolStrip = New System.Windows.Forms.ToolStrip()
        Me._LayerTimingLabel = New System.Windows.Forms.ToolStripLabel()
        Me._DelayNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._DelayNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._TimerIntervalNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TimerIntervalNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._ToolStripPanel.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me._TriggerConfigurationToolStrip.SuspendLayout()
        Me._TimingToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolStripPanel
        '
        Me._ToolStripPanel.BackColor = System.Drawing.Color.Transparent
        Me._ToolStripPanel.Controls.Add(Me._SubsystemToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._TriggerConfigurationToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._TimingToolStrip)
        Me._ToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ToolStripPanel.Location = New System.Drawing.Point(1, 1)
        Me._ToolStripPanel.Name = "_ToolStripPanel"
        Me._ToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._ToolStripPanel.RowMargin = New System.Windows.Forms.Padding(0)
        Me._ToolStripPanel.Size = New System.Drawing.Size(395, 84)
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._CountNumericLabel, Me._CountNumeric, Me._InfiniteCountButton, Me._SourceComboLabel, Me._SourceComboBox})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(395, 28)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 3
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DoubleClickEnabled = True
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem, Me._SendBusTriggerMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(53, 25)
        Me._SubsystemSplitButton.Text = "Trig1"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(162, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(162, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument"
        '
        '_SendBusTriggerMenuItem
        '
        Me._SendBusTriggerMenuItem.Name = "_SendBusTriggerMenuItem"
        Me._SendBusTriggerMenuItem.Size = New System.Drawing.Size(162, 22)
        Me._SendBusTriggerMenuItem.Text = "Send Bus Trigger"
        Me._SendBusTriggerMenuItem.ToolTipText = "Sends a buis trigger to the instrument"
        '
        '_CountNumericLabel
        '
        Me._CountNumericLabel.Name = "_CountNumericLabel"
        Me._CountNumericLabel.Size = New System.Drawing.Size(43, 25)
        Me._CountNumericLabel.Text = "Count:"
        '
        '_CountNumeric
        '
        Me._CountNumeric.AutoSize = False
        Me._CountNumeric.Name = "_CountNumeric"
        Me._CountNumeric.Size = New System.Drawing.Size(60, 25)
        Me._CountNumeric.Text = "0"
        Me._CountNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_InfiniteCountButton
        '
        Me._InfiniteCountButton.Checked = True
        Me._InfiniteCountButton.CheckOnClick = True
        Me._InfiniteCountButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._InfiniteCountButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._InfiniteCountButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._InfiniteCountButton.Name = "_InfiniteCountButton"
        Me._InfiniteCountButton.Size = New System.Drawing.Size(37, 25)
        Me._InfiniteCountButton.Text = "INF ?"
        Me._InfiniteCountButton.ToolTipText = "Toggle to enable infinite count"
        '
        '_SourceComboLabel
        '
        Me._SourceComboLabel.Name = "_SourceComboLabel"
        Me._SourceComboLabel.Size = New System.Drawing.Size(46, 25)
        Me._SourceComboLabel.Text = "Source:"
        '
        '_SourceComboBox
        '
        Me._SourceComboBox.Name = "_SourceComboBox"
        Me._SourceComboBox.Size = New System.Drawing.Size(80, 28)
        Me._SourceComboBox.Text = "Immediate"
        Me._SourceComboBox.ToolTipText = "Selects the source"
        '
        '_TriggerConfigurationToolStrip
        '
        Me._TriggerConfigurationToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._TriggerConfigurationToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TriggerConfigurationToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TriggerConfigurationToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TriggerCondigureLabel, Me._InputLineNumericLabel, Me._InputLineNumeric, Me._OutputLineNumericLabel, Me._OutputLineNumeric, Me._BypassToggleButton})
        Me._TriggerConfigurationToolStrip.Location = New System.Drawing.Point(0, 28)
        Me._TriggerConfigurationToolStrip.Name = "_TriggerConfigurationToolStrip"
        Me._TriggerConfigurationToolStrip.Size = New System.Drawing.Size(395, 28)
        Me._TriggerConfigurationToolStrip.Stretch = True
        Me._TriggerConfigurationToolStrip.TabIndex = 2
        '
        '_TriggerCondigureLabel
        '
        Me._TriggerCondigureLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerCondigureLabel.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._TriggerCondigureLabel.Name = "_TriggerCondigureLabel"
        Me._TriggerCondigureLabel.Size = New System.Drawing.Size(44, 25)
        Me._TriggerCondigureLabel.Text = "Trigger"
        '
        '_InputLineNumericLabel
        '
        Me._InputLineNumericLabel.Name = "_InputLineNumericLabel"
        Me._InputLineNumericLabel.Size = New System.Drawing.Size(63, 25)
        Me._InputLineNumericLabel.Text = "Input Line:"
        '
        '_InputLineNumeric
        '
        Me._InputLineNumeric.Name = "_InputLineNumeric"
        Me._InputLineNumeric.Size = New System.Drawing.Size(41, 25)
        Me._InputLineNumeric.Text = "3"
        Me._InputLineNumeric.ToolTipText = "Asynchronous trigger link input line"
        Me._InputLineNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_OutputLineNumericLabel
        '
        Me._OutputLineNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._OutputLineNumericLabel.Name = "_OutputLineNumericLabel"
        Me._OutputLineNumericLabel.Size = New System.Drawing.Size(73, 25)
        Me._OutputLineNumericLabel.Text = "Output Line:"
        '
        '_OutputLineNumeric
        '
        Me._OutputLineNumeric.Name = "_OutputLineNumeric"
        Me._OutputLineNumeric.Size = New System.Drawing.Size(41, 25)
        Me._OutputLineNumeric.Text = "0"
        Me._OutputLineNumeric.ToolTipText = "Trigger Link output line"
        Me._OutputLineNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_BypassToggleButton
        '
        Me._BypassToggleButton.Checked = True
        Me._BypassToggleButton.CheckOnClick = True
        Me._BypassToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._BypassToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._BypassToggleButton.Image = CType(resources.GetObject("_BypassToggleButton.Image"), System.Drawing.Image)
        Me._BypassToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._BypassToggleButton.Name = "_BypassToggleButton"
        Me._BypassToggleButton.Size = New System.Drawing.Size(55, 25)
        Me._BypassToggleButton.Text = "Bypass ?"
        Me._BypassToggleButton.ToolTipText = "Toggle to bypass or enable triggers"
        '
        '_TimingToolStrip
        '
        Me._TimingToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._TimingToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TimingToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TimingToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LayerTimingLabel, Me._DelayNumericLabel, Me._DelayNumeric, Me._TimerIntervalNumericLabel, Me._TimerIntervalNumeric})
        Me._TimingToolStrip.Location = New System.Drawing.Point(0, 56)
        Me._TimingToolStrip.Name = "_TimingToolStrip"
        Me._TimingToolStrip.Size = New System.Drawing.Size(395, 28)
        Me._TimingToolStrip.Stretch = True
        Me._TimingToolStrip.TabIndex = 1
        '
        '_LayerTimingLabel
        '
        Me._LayerTimingLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LayerTimingLabel.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._LayerTimingLabel.Name = "_LayerTimingLabel"
        Me._LayerTimingLabel.Size = New System.Drawing.Size(43, 25)
        Me._LayerTimingLabel.Text = "Timing"
        '
        '_DelayNumericLabel
        '
        Me._DelayNumericLabel.Name = "_DelayNumericLabel"
        Me._DelayNumericLabel.Size = New System.Drawing.Size(50, 25)
        Me._DelayNumericLabel.Text = "Delay, s:"
        '
        '_DelayNumeric
        '
        Me._DelayNumeric.AutoSize = False
        Me._DelayNumeric.Name = "_DelayNumeric"
        Me._DelayNumeric.Size = New System.Drawing.Size(61, 25)
        Me._DelayNumeric.Text = "3"
        Me._DelayNumeric.ToolTipText = "Delay in seconds"
        Me._DelayNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_TimerIntervalNumericLabel
        '
        Me._TimerIntervalNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._TimerIntervalNumericLabel.Name = "_TimerIntervalNumericLabel"
        Me._TimerIntervalNumericLabel.Size = New System.Drawing.Size(51, 25)
        Me._TimerIntervalNumericLabel.Text = "Timer, s:"
        '
        '_TimerIntervalNumeric
        '
        Me._TimerIntervalNumeric.AutoSize = False
        Me._TimerIntervalNumeric.Name = "_TimerIntervalNumeric"
        Me._TimerIntervalNumeric.Size = New System.Drawing.Size(61, 25)
        Me._TimerIntervalNumeric.Text = "10"
        Me._TimerIntervalNumeric.ToolTipText = "Timer interval in seconds"
        Me._TimerIntervalNumeric.Value = New Decimal(New Integer() {9999, 0, 0, 196608})
        '
        'TriggerLayerView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ToolStripPanel)
        Me.Name = "TriggerLayerView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(397, 85)
        Me._ToolStripPanel.ResumeLayout(False)
        Me._ToolStripPanel.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me._TriggerConfigurationToolStrip.ResumeLayout(False)
        Me._TriggerConfigurationToolStrip.PerformLayout()
        Me._TimingToolStrip.ResumeLayout(False)
        Me._TimingToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private _ToolStripPanel As Windows.Forms.ToolStripPanel
    Private _TimingToolStrip As Windows.Forms.ToolStrip
    Private _LayerTimingLabel As Windows.Forms.ToolStripLabel
    Private _TimerIntervalNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _TimerIntervalNumeric As Core.Controls.ToolStripNumericUpDown
    Private _DelayNumericLabel As Windows.Forms.ToolStripLabel
    Private _TriggerConfigurationToolStrip As Windows.Forms.ToolStrip
    Private _TriggerCondigureLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _InputLineNumeric As Core.Controls.ToolStripNumericUpDown
    Private _OutputLineNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _OutputLineNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _BypassToggleButton As Windows.Forms.ToolStripButton
    Private _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private _InputLineNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _DelayNumeric As Core.Controls.ToolStripNumericUpDown
    Private _CountNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _InfiniteCountButton As Windows.Forms.ToolStripButton
    Private WithEvents _CountNumeric As Core.Controls.ToolStripNumericUpDown
    Private _SourceComboLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SourceComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SendBusTriggerMenuItem As Windows.Forms.ToolStripMenuItem
End Class
