''' <summary> Defines a SCPI Sense Current Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2014-03-01, 3.0.5173. </para>
''' </remarks>
Public Class SenseCurrentSubsystem
    Inherits VI.SenseFunctionSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ampere)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere
        Me.DefineFunctionModeReadWrites("""{0}""", "'{0}'")
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere
        MyBase.DefineKnownResetState()
        Me.FunctionUnit = Me.DefaultFunctionUnit
    End Sub

#End Region

#Region " APERTURE "

    ''' <summary> Gets or sets The Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property ApertureQueryCommand As String = ":SENS:CURR:DC:APER {0}"

    ''' <summary> Gets or sets The Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overrides Property ApertureCommandFormat As String = ":SENS:CURR:DC:APER?"

#End Region

#Region " AVERAGE COUNT "

    ''' <summary> Gets or sets The average count command format. </summary>
    ''' <value> The average count command format. </value>
    Protected Overrides Property AverageCountCommandFormat As String = ":SENS:CURR:DC:AVER:COUN {0}"

    ''' <summary> Gets or sets The average count query command. </summary>
    ''' <value> The average count query command. </value>
    Protected Overrides Property AverageCountQueryCommand As String = ":SENS:CURR:DC:AVER:COUN?"

#End Region

#Region " AVERAGE ENABLED "

    ''' <summary> Gets or sets the Average enabled command Format. </summary>
    ''' <value> The Average enabled query command. </value>
    Protected Overrides Property AverageEnabledCommandFormat As String = ":SENS:CURR:DC:AVER:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Average enabled query command. </summary>
    ''' <value> The Average enabled query command. </value>
    Protected Overrides Property AverageEnabledQueryCommand As String = ":SENS:CURR:DC:AVER:STAT?"

#End Region

#Region " AVERAGE FILTER TYPE "

    ''' <summary> Gets or sets the Average Filter Type command format. </summary>
    ''' <value> The write Average Filter Type command format. </value>
    Protected Overrides Property AverageFilterTypeCommandFormat As String = ":SENS:CURR:DC:AVER:TCON {0}"

    ''' <summary> Gets or sets the Average Filter Type query command. </summary>
    ''' <value> The Average Filter Type query command. </value>
    Protected Overrides Property AverageFilterTypeQueryCommand As String = ":SENS:CURR:DC:AVER:TCON?"

#End Region

#Region " AVERAGE PERCENT WINDOW "

    ''' <summary> Gets or sets The Average Percent Window command format. </summary>
    ''' <value> The Average Percent Window command format. </value>
    Protected Overrides Property AveragePercentWindowCommandFormat As String = ":SENS:CURR:DC:AVER:ADV:NTOL {0}"

    ''' <summary> Gets or sets The Average Percent Window query command. </summary>
    ''' <value> The Average Percent Window query command. </value>
    Protected Overrides Property AveragePercentWindowQueryCommand As String = ":SENS:CURR:DC:AVER:ADV:NTOL?"

#End Region

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:CURR:DC:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:CURR:DC:RANG:AUTO?"

#End Region

#Region " AUTO ZERO "

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = String.Empty

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:CURR:DC:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:CURR:DC:NPLC?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:CURR:DC:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SENS:CURR:DC:RANG?"

#End Region

#Region " RESOLUTION DIGITS "

    ''' <summary> Gets or sets the Resolution Digits command format. </summary>
    ''' <value> The Resolution Digits command format. </value>
    Protected Overrides Property ResolutionDigitsCommandFormat As String = ":SENS:CURR:DC:DIG {0:0}"

    ''' <summary> Gets or sets the Resolution Digits query command. </summary>
    ''' <value> The Resolution Digits query command. </value>
    Protected Overrides Property ResolutionDigitsQueryCommand As String = ":SENS:CURR:DC:DIG?"

#End Region

End Class

