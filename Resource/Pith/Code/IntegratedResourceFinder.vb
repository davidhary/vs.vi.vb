''' <summary> An Integrated Scientific Resources-based Resource finder. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para>
''' </remarks>
Public Class IntegratedResourceFinder
    Inherits ResourceFinderBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " PARSE RESOURCES "

    ''' <summary> Parse resource. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <see cref="vi.Pith.ResourceNameInfo"/>. </returns>
    Public Overrides Function ParseResource(ByVal resourceName As String) As VI.Pith.ResourceNameInfo
        Return New VI.Pith.ResourceNameInfo(resourceName)
    End Function

    ''' <summary> Gets or sets the using like pattern. </summary>
    ''' <value> The using like pattern. </value>
    Public Overrides ReadOnly Property UsingLikePattern As Boolean = True

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Fetches resource cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns> The resource cache. </returns>
    Private Shared Function FetchResourceCache() As ResourceNameInfoCollection
        Dim resourceNameInfoCollection As New ResourceNameInfoCollection
        resourceNameInfoCollection.ReadResources()
        Return resourceNameInfoCollection
    End Function

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources() As IEnumerable(Of String)
        Return IntegratedResourceFinder.FetchResourceCache.Find(VI.Pith.ResourceNamesManager.AllResourcesFilter)
    End Function

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> List of all resources. </returns>
    Public Overrides Function FindResources(ByVal filter As String) As IEnumerable(Of String)
        Try
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for all interfaces in the resource names cache. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces() As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = VI.Pith.ResourceNamesManager.BuildInterfaceFilter()
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for the interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces(ByVal filter As String) As IEnumerable(Of String)
        Try
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for the interfaces in the resource names cache. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public Overrides Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Dim filter As String = String.Empty
        Try
            filter = VI.Pith.ResourceNamesManager.BuildInterfaceFilter(interfaceType)
            Dim resources As IEnumerable(Of String) = IntegratedResourceFinder.FetchResourceCache.Find(filter)
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal filter As String) As IEnumerable(Of String)
        Try
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments() As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter()
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType)
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public Overrides Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)
        Dim filter As String = String.Empty
        Try
            filter = isr.VI.Pith.ResourceNamesManager.BuildInstrumentFilter(interfaceType, boardNumber)
            Return IntegratedResourceFinder.FetchResourceCache.Find(filter)
        Catch ex As Exception
            Throw New isr.Core.OperationFailedException($"Failed finding resources using filter='{filter}'", ex)
        End Try
    End Function

#End Region

End Class
