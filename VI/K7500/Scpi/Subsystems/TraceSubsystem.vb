''' <summary> Defines a Trace Subsystem for a Keithley 7500 Meter. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TraceSubsystem
    Inherits VI.TraceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PointsCount = 10
        Me.FeedSource = VI.FeedSources.None
        Me.FeedControl = VI.FeedControls.Never
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the ActualPoint count query command. </summary>
    ''' <value> The ActualPoint count query command. </value>
    Protected Overrides Property ActualPointCountQueryCommand As String = ":TRAC:ACT?"

    ''' <summary> Gets or sets the Clear Buffer command. </summary>
    ''' <value> The ClearBuffer command. </value>
    Protected Overrides Property ClearBufferCommand As String = ":TRAC:CLE"

    ''' <summary> Gets or sets the data query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property DataQueryCommand As String = ":TRAC:DATA?"

    ''' <summary> Gets or sets The First Point Number query command. </summary>
    ''' <value> The First Point Number query command. </value>
    Protected Overrides Property FirstPointNumberQueryCommand As String = ":TRAC:ACT:STAR?"

    ''' <summary> Gets or sets The Last Point Number query command. </summary>
    ''' <value> The Last Point Number query command. </value>
    Protected Overrides Property LastPointNumberQueryCommand As String = ":TRAC:ACT:END?"

    ''' <summary> Gets or sets the points count query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property PointsCountQueryCommand As String = ":TRAC:POIN?"

    ''' <summary> Gets or sets the points count command format. </summary>
    ''' <value> The points count command format. </value>
    Protected Overrides Property PointsCountCommandFormat As String = ":TRAC:POIN 10; *WAI; :TRAC:POIN {0}"


#End Region

End Class

