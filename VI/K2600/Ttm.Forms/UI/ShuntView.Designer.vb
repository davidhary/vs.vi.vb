﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ShuntView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ShuntLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ShuntDisplayLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ShuntResistanceTextBox = New System.Windows.Forms.TextBox()
        Me._ShuntResistanceTextBoxLabel = New System.Windows.Forms.Label()
        Me._ShuntGroupBoxLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._MeasureShuntResistanceButton = New System.Windows.Forms.Button()
        Me._ShuntConfigureGroupBoxLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ShuntResistanceConfigurationGroupBox = New System.Windows.Forms.GroupBox()
        Me._RestoreShuntResistanceDefaultsButton = New System.Windows.Forms.Button()
        Me._ApplyNewShuntResistanceConfigurationButton = New System.Windows.Forms.Button()
        Me._ApplyShuntResistanceConfigurationButton = New System.Windows.Forms.Button()
        Me._ShuntResistanceCurrentRangeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ShuntResistanceCurrentRangeNumericLabel = New System.Windows.Forms.Label()
        Me._ShuntResistanceLowLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ShuntResistanceLowLimitNumericLabel = New System.Windows.Forms.Label()
        Me._ShuntResistanceHighLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ShuntResistanceHighLimitNumericLabel = New System.Windows.Forms.Label()
        Me._ShuntResistanceVoltageLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ShuntResistanceVoltageLimitNumericLabel = New System.Windows.Forms.Label()
        Me._ShuntResistanceCurrentLevelNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ShuntResistanceCurrentLevelNumericLabel = New System.Windows.Forms.Label()
        Me._ShuntLayout.SuspendLayout()
        Me._ShuntDisplayLayout.SuspendLayout()
        Me._ShuntGroupBoxLayout.SuspendLayout()
        Me._ShuntConfigureGroupBoxLayout.SuspendLayout()
        Me._ShuntResistanceConfigurationGroupBox.SuspendLayout()
        CType(Me._ShuntResistanceCurrentRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ShuntResistanceLowLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ShuntResistanceHighLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ShuntResistanceVoltageLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ShuntResistanceCurrentLevelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ShuntLayout
        '
        Me._ShuntLayout.ColumnCount = 3
        Me._ShuntLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))

        Me._ShuntLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ShuntLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntLayout.Controls.Add(Me._ShuntDisplayLayout, 1, 1)
        Me._ShuntLayout.Controls.Add(Me._ShuntGroupBoxLayout, 1, 3)
        Me._ShuntLayout.Controls.Add(Me._ShuntConfigureGroupBoxLayout, 1, 2)
        Me._ShuntLayout.Location = New System.Drawing.Point(3, 9)
        Me._ShuntLayout.Name = "_ShuntLayout"
        Me._ShuntLayout.RowCount = 5
        Me._ShuntLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntLayout.Size = New System.Drawing.Size(466, 492)
        Me._ShuntLayout.TabIndex = 1
        '
        '_ShuntDisplayLayout
        '
        Me._ShuntDisplayLayout.BackColor = System.Drawing.Color.Black
        Me._ShuntDisplayLayout.ColumnCount = 3
        Me._ShuntDisplayLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me._ShuntDisplayLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ShuntDisplayLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me._ShuntDisplayLayout.Controls.Add(Me._ShuntResistanceTextBox, 1, 2)
        Me._ShuntDisplayLayout.Controls.Add(Me._ShuntResistanceTextBoxLabel, 1, 1)
        Me._ShuntDisplayLayout.Dock = System.Windows.Forms.DockStyle.Left
        Me._ShuntDisplayLayout.Location = New System.Drawing.Point(64, 11)
        Me._ShuntDisplayLayout.Name = "_ShuntDisplayLayout"
        Me._ShuntDisplayLayout.RowCount = 4
        Me._ShuntDisplayLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me._ShuntDisplayLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntDisplayLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntDisplayLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me._ShuntDisplayLayout.Size = New System.Drawing.Size(337, 75)
        Me._ShuntDisplayLayout.TabIndex = 0
        '
        '_ShuntResistanceTextBox
        '
        Me._ShuntResistanceTextBox.BackColor = System.Drawing.Color.Black
        Me._ShuntResistanceTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ShuntResistanceTextBox.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ShuntResistanceTextBox.ForeColor = System.Drawing.Color.Aquamarine
        Me._ShuntResistanceTextBox.Location = New System.Drawing.Point(68, 25)
        Me._ShuntResistanceTextBox.Name = "_ShuntResistanceTextBox"
        Me._ShuntResistanceTextBox.ReadOnly = True
        Me._ShuntResistanceTextBox.Size = New System.Drawing.Size(201, 39)
        Me._ShuntResistanceTextBox.TabIndex = 7
        Me._ShuntResistanceTextBox.Text = "0.000"
        Me._ShuntResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_ShuntResistanceTextBoxLabel
        '
        Me._ShuntResistanceTextBoxLabel.AutoSize = True
        Me._ShuntResistanceTextBoxLabel.BackColor = System.Drawing.Color.Black
        Me._ShuntResistanceTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ShuntResistanceTextBoxLabel.ForeColor = System.Drawing.Color.Yellow
        Me._ShuntResistanceTextBoxLabel.Location = New System.Drawing.Point(68, 5)
        Me._ShuntResistanceTextBoxLabel.Name = "_ShuntResistanceTextBoxLabel"
        Me._ShuntResistanceTextBoxLabel.Size = New System.Drawing.Size(201, 17)
        Me._ShuntResistanceTextBoxLabel.TabIndex = 6
        Me._ShuntResistanceTextBoxLabel.Text = "SHUNT RESISTANCE [Ω]"
        Me._ShuntResistanceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_ShuntGroupBoxLayout
        '
        Me._ShuntGroupBoxLayout.ColumnCount = 3
        Me._ShuntGroupBoxLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntGroupBoxLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ShuntGroupBoxLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntGroupBoxLayout.Controls.Add(Me._MeasureShuntResistanceButton, 1, 1)
        Me._ShuntGroupBoxLayout.Location = New System.Drawing.Point(64, 417)
        Me._ShuntGroupBoxLayout.Name = "_ShuntGroupBoxLayout"
        Me._ShuntGroupBoxLayout.RowCount = 3
        Me._ShuntGroupBoxLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._ShuntGroupBoxLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntGroupBoxLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._ShuntGroupBoxLayout.Size = New System.Drawing.Size(337, 63)
        Me._ShuntGroupBoxLayout.TabIndex = 2
        '
        '_MeasureShuntResistanceButton
        '
        Me._MeasureShuntResistanceButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._MeasureShuntResistanceButton.Location = New System.Drawing.Point(53, 13)
        Me._MeasureShuntResistanceButton.Name = "_MeasureShuntResistanceButton"
        Me._MeasureShuntResistanceButton.Size = New System.Drawing.Size(230, 36)
        Me._MeasureShuntResistanceButton.TabIndex = 3
        Me._MeasureShuntResistanceButton.Text = "READ SHUNT RESISTANCE"
        Me._MeasureShuntResistanceButton.UseVisualStyleBackColor = True
        '
        '_ShuntConfigureGroupBoxLayout
        '
        Me._ShuntConfigureGroupBoxLayout.ColumnCount = 3
        Me._ShuntConfigureGroupBoxLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntConfigureGroupBoxLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ShuntConfigureGroupBoxLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntConfigureGroupBoxLayout.Controls.Add(Me._ShuntResistanceConfigurationGroupBox, 1, 1)
        Me._ShuntConfigureGroupBoxLayout.Location = New System.Drawing.Point(64, 92)
        Me._ShuntConfigureGroupBoxLayout.Name = "_ShuntConfigureGroupBoxLayout"
        Me._ShuntConfigureGroupBoxLayout.RowCount = 3
        Me._ShuntConfigureGroupBoxLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntConfigureGroupBoxLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ShuntConfigureGroupBoxLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ShuntConfigureGroupBoxLayout.Size = New System.Drawing.Size(337, 319)
        Me._ShuntConfigureGroupBoxLayout.TabIndex = 3
        '
        '_ShuntResistanceConfigurationGroupBox
        '
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._RestoreShuntResistanceDefaultsButton)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ApplyNewShuntResistanceConfigurationButton)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ApplyShuntResistanceConfigurationButton)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceCurrentRangeNumeric)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceCurrentRangeNumericLabel)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceLowLimitNumeric)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceLowLimitNumericLabel)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceHighLimitNumeric)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceHighLimitNumericLabel)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceVoltageLimitNumeric)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceVoltageLimitNumericLabel)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceCurrentLevelNumeric)
        Me._ShuntResistanceConfigurationGroupBox.Controls.Add(Me._ShuntResistanceCurrentLevelNumericLabel)
        Me._ShuntResistanceConfigurationGroupBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ShuntResistanceConfigurationGroupBox.Location = New System.Drawing.Point(39, 12)
        Me._ShuntResistanceConfigurationGroupBox.Name = "_ShuntResistanceConfigurationGroupBox"
        Me._ShuntResistanceConfigurationGroupBox.Size = New System.Drawing.Size(258, 294)
        Me._ShuntResistanceConfigurationGroupBox.TabIndex = 2
        Me._ShuntResistanceConfigurationGroupBox.TabStop = False
        Me._ShuntResistanceConfigurationGroupBox.Text = "SHUNT RESISTANCE CONFIG."
        '
        '_RestoreShuntResistanceDefaultsButton
        '
        Me._RestoreShuntResistanceDefaultsButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RestoreShuntResistanceDefaultsButton.Location = New System.Drawing.Point(14, 252)
        Me._RestoreShuntResistanceDefaultsButton.Name = "_RestoreShuntResistanceDefaultsButton"
        Me._RestoreShuntResistanceDefaultsButton.Size = New System.Drawing.Size(234, 30)
        Me._RestoreShuntResistanceDefaultsButton.TabIndex = 12
        Me._RestoreShuntResistanceDefaultsButton.Text = "RESTORE DEFAULTS"
        Me._RestoreShuntResistanceDefaultsButton.UseVisualStyleBackColor = True
        '
        '_ApplyNewShuntResistanceConfigurationButton
        '
        Me._ApplyNewShuntResistanceConfigurationButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ApplyNewShuntResistanceConfigurationButton.Location = New System.Drawing.Point(133, 210)
        Me._ApplyNewShuntResistanceConfigurationButton.Name = "_ApplyNewShuntResistanceConfigurationButton"
        Me._ApplyNewShuntResistanceConfigurationButton.Size = New System.Drawing.Size(115, 30)
        Me._ApplyNewShuntResistanceConfigurationButton.TabIndex = 11
        Me._ApplyNewShuntResistanceConfigurationButton.Text = "APPLY CHANGES"
        Me._ApplyNewShuntResistanceConfigurationButton.UseVisualStyleBackColor = True
        '
        '_ApplyShuntResistanceConfigurationButton
        '
        Me._ApplyShuntResistanceConfigurationButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ApplyShuntResistanceConfigurationButton.Location = New System.Drawing.Point(11, 209)
        Me._ApplyShuntResistanceConfigurationButton.Name = "_ApplyShuntResistanceConfigurationButton"
        Me._ApplyShuntResistanceConfigurationButton.Size = New System.Drawing.Size(115, 30)
        Me._ApplyShuntResistanceConfigurationButton.TabIndex = 10
        Me._ApplyShuntResistanceConfigurationButton.Text = "APPLY ALL"
        Me._ApplyShuntResistanceConfigurationButton.UseVisualStyleBackColor = True
        '
        '_ShuntResistanceCurrentRangeNumeric
        '
        Me._ShuntResistanceCurrentRangeNumeric.DecimalPlaces = 3
        Me._ShuntResistanceCurrentRangeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ShuntResistanceCurrentRangeNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
        Me._ShuntResistanceCurrentRangeNumeric.Location = New System.Drawing.Point(161, 62)
        Me._ShuntResistanceCurrentRangeNumeric.Maximum = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._ShuntResistanceCurrentRangeNumeric.Name = "_ShuntResistanceCurrentRangeNumeric"
        Me._ShuntResistanceCurrentRangeNumeric.Size = New System.Drawing.Size(75, 25)
        Me._ShuntResistanceCurrentRangeNumeric.TabIndex = 3
        Me._ShuntResistanceCurrentRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ShuntResistanceCurrentRangeNumeric.Value = New Decimal(New Integer() {1, 0, 0, 131072})
        '
        '_ShuntResistanceCurrentRangeNumericLabel
        '
        Me._ShuntResistanceCurrentRangeNumericLabel.AutoSize = True
        Me._ShuntResistanceCurrentRangeNumericLabel.Location = New System.Drawing.Point(25, 66)
        Me._ShuntResistanceCurrentRangeNumericLabel.Name = "_ShuntResistanceCurrentRangeNumericLabel"
        Me._ShuntResistanceCurrentRangeNumericLabel.Size = New System.Drawing.Size(134, 17)
        Me._ShuntResistanceCurrentRangeNumericLabel.TabIndex = 2
        Me._ShuntResistanceCurrentRangeNumericLabel.Text = "CURRENT RANGE [A]:"
        Me._ShuntResistanceCurrentRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ShuntResistanceLowLimitNumeric
        '
        Me._ShuntResistanceLowLimitNumeric.DecimalPlaces = 1
        Me._ShuntResistanceLowLimitNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ShuntResistanceLowLimitNumeric.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ShuntResistanceLowLimitNumeric.Location = New System.Drawing.Point(161, 172)
        Me._ShuntResistanceLowLimitNumeric.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me._ShuntResistanceLowLimitNumeric.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ShuntResistanceLowLimitNumeric.Name = "_ShuntResistanceLowLimitNumeric"
        Me._ShuntResistanceLowLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ShuntResistanceLowLimitNumeric.Size = New System.Drawing.Size(75, 25)
        Me._ShuntResistanceLowLimitNumeric.TabIndex = 9
        Me._ShuntResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ShuntResistanceLowLimitNumeric.Value = New Decimal(New Integer() {1150, 0, 0, 0})
        '
        '_ShuntResistanceLowLimitNumericLabel
        '
        Me._ShuntResistanceLowLimitNumericLabel.AutoSize = True
        Me._ShuntResistanceLowLimitNumericLabel.Location = New System.Drawing.Point(63, 176)
        Me._ShuntResistanceLowLimitNumericLabel.Name = "_ShuntResistanceLowLimitNumericLabel"
        Me._ShuntResistanceLowLimitNumericLabel.Size = New System.Drawing.Size(96, 17)
        Me._ShuntResistanceLowLimitNumericLabel.TabIndex = 8
        Me._ShuntResistanceLowLimitNumericLabel.Text = "LO&W LIMIT [Ω]:"
        Me._ShuntResistanceLowLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ShuntResistanceHighLimitNumeric
        '
        Me._ShuntResistanceHighLimitNumeric.DecimalPlaces = 1
        Me._ShuntResistanceHighLimitNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ShuntResistanceHighLimitNumeric.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ShuntResistanceHighLimitNumeric.Location = New System.Drawing.Point(161, 135)
        Me._ShuntResistanceHighLimitNumeric.Maximum = New Decimal(New Integer() {3000, 0, 0, 0})
        Me._ShuntResistanceHighLimitNumeric.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ShuntResistanceHighLimitNumeric.Name = "_ShuntResistanceHighLimitNumeric"
        Me._ShuntResistanceHighLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ShuntResistanceHighLimitNumeric.Size = New System.Drawing.Size(75, 25)
        Me._ShuntResistanceHighLimitNumeric.TabIndex = 7
        Me._ShuntResistanceHighLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ShuntResistanceHighLimitNumeric.Value = New Decimal(New Integer() {1250, 0, 0, 0})
        '
        '_ShuntResistanceHighLimitNumericLabel
        '
        Me._ShuntResistanceHighLimitNumericLabel.AutoSize = True
        Me._ShuntResistanceHighLimitNumericLabel.Location = New System.Drawing.Point(61, 139)
        Me._ShuntResistanceHighLimitNumericLabel.Name = "_ShuntResistanceHighLimitNumericLabel"
        Me._ShuntResistanceHighLimitNumericLabel.Size = New System.Drawing.Size(98, 17)
        Me._ShuntResistanceHighLimitNumericLabel.TabIndex = 6
        Me._ShuntResistanceHighLimitNumericLabel.Text = "&HIGH LIMIT [Ω]:"
        Me._ShuntResistanceHighLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ShuntResistanceVoltageLimitNumeric
        '
        Me._ShuntResistanceVoltageLimitNumeric.DecimalPlaces = 2
        Me._ShuntResistanceVoltageLimitNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ShuntResistanceVoltageLimitNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._ShuntResistanceVoltageLimitNumeric.Location = New System.Drawing.Point(161, 98)
        Me._ShuntResistanceVoltageLimitNumeric.Maximum = New Decimal(New Integer() {40, 0, 0, 0})
        Me._ShuntResistanceVoltageLimitNumeric.Name = "_ShuntResistanceVoltageLimitNumeric"
        Me._ShuntResistanceVoltageLimitNumeric.Size = New System.Drawing.Size(75, 25)
        Me._ShuntResistanceVoltageLimitNumeric.TabIndex = 5
        Me._ShuntResistanceVoltageLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ShuntResistanceVoltageLimitNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_ShuntResistanceVoltageLimitNumericLabel
        '
        Me._ShuntResistanceVoltageLimitNumericLabel.AutoSize = True
        Me._ShuntResistanceVoltageLimitNumericLabel.Location = New System.Drawing.Point(38, 102)
        Me._ShuntResistanceVoltageLimitNumericLabel.Name = "_ShuntResistanceVoltageLimitNumericLabel"
        Me._ShuntResistanceVoltageLimitNumericLabel.Size = New System.Drawing.Size(119, 17)
        Me._ShuntResistanceVoltageLimitNumericLabel.TabIndex = 4
        Me._ShuntResistanceVoltageLimitNumericLabel.Text = "&VOLTAGE LIMIT [V]:"
        Me._ShuntResistanceVoltageLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ShuntResistanceCurrentLevelNumeric
        '
        Me._ShuntResistanceCurrentLevelNumeric.DecimalPlaces = 4
        Me._ShuntResistanceCurrentLevelNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ShuntResistanceCurrentLevelNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
        Me._ShuntResistanceCurrentLevelNumeric.Location = New System.Drawing.Point(161, 27)
        Me._ShuntResistanceCurrentLevelNumeric.Maximum = New Decimal(New Integer() {100, 0, 0, 262144})
        Me._ShuntResistanceCurrentLevelNumeric.Name = "_ShuntResistanceCurrentLevelNumeric"
        Me._ShuntResistanceCurrentLevelNumeric.Size = New System.Drawing.Size(75, 25)
        Me._ShuntResistanceCurrentLevelNumeric.TabIndex = 1
        Me._ShuntResistanceCurrentLevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ShuntResistanceCurrentLevelNumeric.Value = New Decimal(New Integer() {1, 0, 0, 196608})
        '
        '_ShuntResistanceCurrentLevelNumericLabel
        '
        Me._ShuntResistanceCurrentLevelNumericLabel.AutoSize = True
        Me._ShuntResistanceCurrentLevelNumericLabel.Location = New System.Drawing.Point(33, 31)
        Me._ShuntResistanceCurrentLevelNumericLabel.Name = "_ShuntResistanceCurrentLevelNumericLabel"
        Me._ShuntResistanceCurrentLevelNumericLabel.Size = New System.Drawing.Size(126, 17)
        Me._ShuntResistanceCurrentLevelNumericLabel.TabIndex = 0
        Me._ShuntResistanceCurrentLevelNumericLabel.Text = "C&URRENT LEVEL [A]:"
        Me._ShuntResistanceCurrentLevelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ShuntView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ShuntLayout)
        Me.Name = "ShuntView"
        Me.Size = New System.Drawing.Size(472, 510)
        Me._ShuntLayout.ResumeLayout(False)
        Me._ShuntDisplayLayout.ResumeLayout(False)
        Me._ShuntDisplayLayout.PerformLayout()
        Me._ShuntGroupBoxLayout.ResumeLayout(False)
        Me._ShuntConfigureGroupBoxLayout.ResumeLayout(False)
        Me._ShuntResistanceConfigurationGroupBox.ResumeLayout(False)
        Me._ShuntResistanceConfigurationGroupBox.PerformLayout()
        CType(Me._ShuntResistanceCurrentRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ShuntResistanceLowLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ShuntResistanceHighLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ShuntResistanceVoltageLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ShuntResistanceCurrentLevelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ShuntLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _ShuntDisplayLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _ShuntResistanceTextBox As Windows.Forms.TextBox
    Private WithEvents _ShuntResistanceTextBoxLabel As Windows.Forms.Label
    Private WithEvents _ShuntGroupBoxLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _MeasureShuntResistanceButton As Windows.Forms.Button
    Private WithEvents _ShuntConfigureGroupBoxLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _ShuntResistanceConfigurationGroupBox As Windows.Forms.GroupBox
    Private WithEvents _RestoreShuntResistanceDefaultsButton As Windows.Forms.Button
    Private WithEvents _ApplyNewShuntResistanceConfigurationButton As Windows.Forms.Button
    Private WithEvents _ApplyShuntResistanceConfigurationButton As Windows.Forms.Button
    Private WithEvents _ShuntResistanceCurrentRangeNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ShuntResistanceCurrentRangeNumericLabel As Windows.Forms.Label
    Private WithEvents _ShuntResistanceLowLimitNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ShuntResistanceLowLimitNumericLabel As Windows.Forms.Label
    Private WithEvents _ShuntResistanceHighLimitNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ShuntResistanceHighLimitNumericLabel As Windows.Forms.Label
    Private WithEvents _ShuntResistanceVoltageLimitNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ShuntResistanceVoltageLimitNumericLabel As Windows.Forms.Label
    Private WithEvents _ShuntResistanceCurrentLevelNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ShuntResistanceCurrentLevelNumericLabel As Windows.Forms.Label
End Class
