'---------------------------------------------------------------------------------------------------
' file:		.\Resources\VisaVersionValidator.vb
'
' summary:	Visa version validator class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Foundation.ExceptionExtensions

''' <summary> A visa version validator. </summary>
Public NotInheritable Class VisaVersionValidator

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    Private Sub New()
        MyBase.New
    End Sub
#End Region

#Region " VALIDATE VISA VERSIONS "

    ''' <summary>
    ''' Validates the visa assembly and system file versions using settings versions.
    ''' </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaAssemblyVersions() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = isr.VI.Foundation.VisaVersionValidator.ValidateVisaAssemblyVersion()
        If result.Success Then result = isr.VI.Foundation.VisaVersionValidator.ValidateVisaSystemFileVersion()
        Return result
    End Function

    ''' <summary>
    ''' Validates the implementation and specification visa versions against settings values.
    ''' </summary>
    ''' <remarks> David, 2020-04-12. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateFunctionalVisaVersions() As (Success As Boolean, Details As String)
        Return If(String.Equals(VisaVersionValidator.ImplementationVersion.ToString, My.Settings.ImplementationVersion),
                    If(String.Equals(VisaVersionValidator.ImplementationVersion.ToString, My.Settings.ImplementationVersion),
                      (True, String.Empty),
                      (False, $"Expected specification version {My.Settings.SpecificationVersion} different from actual {VisaVersionValidator.SpecificationVersion}")),
                   (False, $"Expected implementation version {My.Settings.ImplementationVersion} different from actual {VisaVersionValidator.ImplementationVersion}"))
    End Function

    ''' <summary> Gets the version of this VISA.NET implementation. </summary>
    ''' <value> The implementation version. </value>
    Public Shared ReadOnly Property ImplementationVersion As Version
        Get
            Return Ivi.Visa.GlobalResourceManager.ImplementationVersion
        End Get
    End Property

    ''' <summary> Gets the specification version. </summary>
    ''' <value> The specification version. </value>
    Public Shared ReadOnly Property SpecificationVersion As Version
        Get
            Return Ivi.Visa.GlobalResourceManager.SpecificationVersion
        End Get
    End Property

#End Region

#Region " VISA SYSTEM DLL (visa32.dll) FILE VERSION "

    ''' <summary> Gets the filename of the VISA system DLL file (x86 is used). </summary>
    ''' <value> The the filename of the VISA system DLL file (x86 is used). </value>
    Public Shared ReadOnly Property VisaSystemFileName As String = "visa32.dll"

    ''' <summary> Gets the full name of the VISA system DLL file (x86 is used). </summary>
    ''' <value> The full name of the VISA system DLL file (x86 is used). </value>
    Public Shared ReadOnly Property VisaSystemFileFullName As String
        Get
            Return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), VisaVersionValidator.VisaSystemFileName)
        End Get
    End Property

    ''' <summary> Gets the visa system file product version. </summary>
    ''' <value> The visa system file product version. </value>
    Public Shared ReadOnly Property VisaSystemFileProductVersion() As String

    ''' <summary> Reads the file version info of the VISA system DLL file (x86 is used). </summary>
    ''' <returns> The file version info of the VISA system DLL file (x86 is used). </returns>
    Public Shared Function ReadVisaSystemFileVersionInfo() As FileVersionInfo
        Return FileVersionInfo.GetVersionInfo(VisaVersionValidator.VisaSystemFileFullName)
    End Function

    ''' <summary> Validates the VISA system DLL file (x86 is used). </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaSystemFileVersion(ByVal expectedVersion As String) As (Success As Boolean, Details As String)
        VisaVersionValidator._VisaSystemFileProductVersion = VisaVersionValidator.ReadVisaSystemFileVersionInfo.ProductVersion
        Return If(isr.VI.Pith.ResourcesProviderBase.AreEqual(expectedVersion, VisaVersionValidator._VisaSystemFileProductVersion),
            DirectCast((True, String.Empty), (Success As Boolean, Details As String)),
            (False, $"{My.Settings.VisaImplementationVendor} VISA {My.Settings.VisaProductVersion} {NameOf(VisaVersionValidator.VisaSystemFileProductVersion)} {VisaVersionValidator.VisaSystemFileFullName} version {VisaVersionValidator.VisaSystemFileProductVersion } different from expected {expectedVersion}"))
    End Function

    ''' <summary> Gets the expected VISA system DLL file (x86 is used). </summary>
    ''' <value> The expected foundation system file version. </value>
    Public Shared ReadOnly Property ExpectedVisaSystemFileVersion As String
        Get
            If Environment.Is64BitProcess Then
                Return My.Settings.VisaSystem64FileProductVersion
            Else
                ' IDE is running in 32 bit mode.
                Return My.Settings.VisaSystem32FileProductVersion
            End If
        End Get
    End Property

    ''' <summary> Validates the VISA system DLL file (x86 is used) version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaSystemFileVersion() As (Success As Boolean, Details As String)
        Return VisaVersionValidator.ValidateVisaSystemFileVersion(VisaVersionValidator.ExpectedVisaSystemFileVersion)
    End Function

#End Region

#Region " VISA ASSEMBLY (ivi.visa.dll) FILE VERSION "

    ''' <summary> Gets the filename of the .NET visa assembly file name. </summary>
    ''' <value> The filename of the .NET visa assembly file name. </value>
    Public Shared ReadOnly Property VisaAssemblyFileName As String = "ivi.visa.dll"

    ''' <summary> Gets the full name of the .NET visa assembly file name. </summary>
    ''' <value> The full name of the .NET visa assembly file name. </value>
    Public Shared ReadOnly Property VisaAssemblyFileFullName As String
        Get
            Return System.IO.Path.Combine(My.Application.Info.DirectoryPath, VisaVersionValidator.VisaAssemblyFileName)
        End Get
    End Property

    ''' <summary> Gets the visa assembly file version. </summary>
    ''' <value> The visa assembly file version. </value>
    Public Shared ReadOnly Property VisaAssemblyFileVersion() As String

    ''' <summary> Reads the .NET visa assembly file version. </summary>
    ''' <returns> The .NET visa assembly file version. </returns>
    Public Shared Function ReadVisaAssemblyFileVersionInfo() As FileVersionInfo
        Return FileVersionInfo.GetVersionInfo(VisaVersionValidator.VisaAssemblyFileFullName)
    End Function

    ''' <summary> Validates the .NET visa assembly file version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaAssemblyVersion(ByVal expectedVersion As String) As (Success As Boolean, Details As String)
        VisaVersionValidator._VisaAssemblyFileVersion = VisaVersionValidator.ReadVisaAssemblyFileVersionInfo.FileVersion
        Return If(isr.VI.Pith.ResourcesProviderBase.AreEqual(expectedVersion, VisaVersionValidator.VisaAssemblyFileVersion),
            (True, String.Empty),
            (False, $"{My.Settings.VisaImplementationVendor} VISA {My.Settings.VisaProductVersion} {NameOf(VisaVersionValidator.VisaAssemblyFileVersion)} {VisaVersionValidator.VisaAssemblyFileFullName} version {VisaVersionValidator.VisaAssemblyFileVersion } different from expected {expectedVersion}"))
    End Function

    ''' <summary> Validates the .NET visa assembly file version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaAssemblyVersion() As (Success As Boolean, Details As String)
        Return VisaVersionValidator.ValidateVisaAssemblyVersion(VisaVersionValidator.ExpectedVisaAssemblyVersion)
    End Function

    ''' <summary> Gets the expected .NET visa assembly version. </summary>
    ''' <value> The expected .NET visa assembly version. </value>
    Public Shared ReadOnly Property ExpectedVisaAssemblyVersion As String
        Get
            Return My.Settings.VisaAssemblyVersion
        End Get
    End Property

#End Region

#Region " VISA PRODUCT VERSION (registry)"

    ''' <summary> Gets the registry pathname of the vendor version. </summary>
    ''' <value> The the registry pathname of the vendor version. </value>
    Public Shared Property VendorVersionPath As String = My.Settings.VendorVersionPath

    ''' <summary> Gets the name of the vendor version registry key. </summary>
    ''' <value> The name of the vendor version registry key. </value>
    Public Shared Property VendorVersionKeyName As String = "CurrentVersion"

    ''' <summary> Gets the visa vendor version. </summary>
    ''' <value> The visa vendor version. </value>
    Public Shared ReadOnly Property VisaVendorVersion As String

    ''' <summary> Reads the Visa vendor version. </summary>
    ''' <returns> The Visa vendor version. </returns>
    Public Shared Function ReadVisaVendorVersion() As Version
        Dim version As String = isr.Core.MachineInfo.ReadRegistry(Microsoft.Win32.RegistryHive.LocalMachine,
                                                                     VisaVersionValidator.VendorVersionPath, VisaVersionValidator.VendorVersionKeyName, "0.0.0.0")
        Return New Version(version)
    End Function

    ''' <summary> Validates the Visa vendor version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="expectedVersion"> The expected version. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Shared Function ValidateVisaVendorVersion(ByVal expectedVersion As String) As (Success As Boolean, Details As String)
        Try
            VisaVersionValidator._VisaVendorVersion = VisaVersionValidator.ReadVisaVendorVersion.ToString
            Return If(isr.VI.Pith.ResourcesProviderBase.AreEqual(expectedVersion, VisaVersionValidator._VisaVendorVersion),
            (True, String.Empty),
            (False, $"{My.Settings.VisaImplementationVendor} VISA {My.Settings.VisaProductVersion} {NameOf(VisaVersionValidator.VisaVendorVersion)} version {VisaVersionValidator.VisaVendorVersion} different from expected {expectedVersion}"))
        Catch ex As Exception
            Return (False, ex.ToFullBlownString)
        End Try
    End Function

    ''' <summary> Validates the Visa vendor version. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Shared Function ValidateVisaVendorVersion() As (Success As Boolean, Details As String)
        Return VisaVersionValidator.ValidateVisaVendorVersion(VisaVersionValidator.ExpectedVendorVersion)
    End Function

    ''' <summary> Gets the expected vendor version. </summary>
    ''' <value> The expected vendor version. </value>
    Public Shared ReadOnly Property ExpectedVendorVersion As String
        Get
            Return My.Settings.VisaProductVersion
        End Get
    End Property

#End Region


End Class
