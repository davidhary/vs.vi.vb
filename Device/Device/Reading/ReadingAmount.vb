''' <summary> Implements a reading <see cref="Arebis.TypedUnits.Amount">amount</see>. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-11-01 </para>
''' </remarks>
Public Class ReadingAmount
    Inherits ReadingValue

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a measured value without specifying the value or its validity, which must be
    ''' specified for the value to be made valid.
    ''' </summary>
    ''' <param name="readingType"> Type of the reading. </param>
    ''' <param name="unit">        The unit. </param>
    Public Sub New(ByVal readingType As ReadingElementTypes, ByVal unit As Arebis.TypedUnits.Unit)
        MyBase.New(readingType)
        Me._Amount = New Arebis.TypedUnits.Amount(0, unit)
    End Sub

    ''' <summary> Constructs a copy of an existing value. </summary>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As ReadingAmount)
        MyBase.New(model)
        If model IsNot Nothing Then
            Me._Amount = New Arebis.TypedUnits.Amount(model.Amount)
        End If
    End Sub

    ''' <summary>
    ''' Constructs a measured value specifying the value or its validity, which must be specified for
    ''' the value to be made valid.
    ''' </summary>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As BufferReading)
        MyBase.New(BufferReading.Validated(model).MeasuredElementType)
        Me.Value = If(model.HasReading, model.Value, New Double?)
        Me._Amount = model.Amount
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator =(ByVal left As ReadingAmount, ByVal right As ReadingAmount) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return ReadingAmount.Equals(left, right)
        End If
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Overloads Shared Operator <>(ByVal left As ReadingAmount, ByVal right As ReadingAmount) As Boolean
        Return Not ReadingAmount.Equals(left, right)
    End Operator

    ''' <summary> Returns True if equal. </summary>
    ''' <remarks>
    ''' Ranges are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values.
    ''' </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals. </returns>
    Public Overloads Shared Function Equals(ByVal left As ReadingAmount, ByVal right As ReadingAmount) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return Arebis.TypedUnits.Amount.Equals(left.Amount, right.Amount)
        End If
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, ReadingAmount))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Ranges are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values.
    ''' </remarks>
    ''' <param name="other"> The other <see cref="ReadingAmount">Range</see> to compare for equality
    '''                      with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As ReadingAmount) As Boolean
        Return other IsNot Nothing AndAlso Equals(Me, other)
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Amount.GetHashCode
    End Function

#End Region

#Region " AMOUNT "

    ''' <summary> Gets the has value. </summary>
    ''' <value> The has value. </value>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.Value.HasValue
        End Get
    End Property

    ''' <summary> Gets the symbol. </summary>
    ''' <value> The symbol. </value>
    Public ReadOnly Property Symbol As String
        Get
            Return Me.Amount.Unit.Symbol
        End Get
    End Property

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return If(Me.HasValue, $"{Me.Amount}", $"-.---- {Me.Amount.Unit}")
    End Function

    ''' <summary> The amount. </summary>
    Private _Amount As Arebis.TypedUnits.Amount

    ''' <summary> The amount. </summary>
    ''' <value> The amount. </value>
    Public Property Amount As Arebis.TypedUnits.Amount
        Get
            Return Me._Amount
        End Get
        Set(value As Arebis.TypedUnits.Amount)
            If value Is Nothing Then
                value = New Arebis.TypedUnits.Amount(VI.Pith.Scpi.Syntax.NotANumber, Me.Amount.Unit)
            End If
            Me._Amount = value
        End Set
    End Property

    ''' <summary> Applies the unit described by unit. </summary>
    ''' <param name="unit"> The unit. </param>
    Public Overridable Sub ApplyUnit(ByVal unit As Arebis.TypedUnits.Unit)
        If Me.Amount.Unit <> unit Then
            Me.Amount = New Arebis.TypedUnits.Amount(VI.Pith.Scpi.Syntax.NotANumber, unit)
            Me.Value = New Double?
        End If
    End Sub

    ''' <summary>
    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <param name="rawValueReading"> The raw value reading. </param>
    ''' <param name="rawUnitsReading"> The raw units reading. </param>
    ''' <returns> <c>True</c> if parsed. </returns>
    Public Overrides Function TryApplyReading(ByVal rawValueReading As String, ByVal rawUnitsReading As String) As Boolean
        If MyBase.TryApplyReading(rawValueReading, rawUnitsReading) Then
            Return Me.TryApplyReading(rawValueReading)
        Else
            Me.Amount = New Arebis.TypedUnits.Amount(VI.Pith.Scpi.Syntax.NotANumber, Me.Amount.Unit)
            Return False
        End If
    End Function

    ''' <summary>
    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <param name="valueReading"> The value reading. </param>
    ''' <returns> <c>True</c> if parsed. </returns>
    Public Overrides Function TryApplyReading(ByVal valueReading As String) As Boolean
        If MyBase.TryApplyReading(valueReading) Then
            If Me.Value.HasValue Then
                Me.Amount = New Arebis.TypedUnits.Amount(Me.Value.Value, Me.Amount.Unit)
            End If
            Return True
        Else
            Me.Amount = New Arebis.TypedUnits.Amount(VI.Pith.Scpi.Syntax.NotANumber, Me.Amount.Unit)
            Return False
        End If
    End Function

#End Region

End Class

