<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ScanListView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsytemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CloseMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OpenMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._OpenAllMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SaveToMemoryLocationMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MemoryLocationTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._SaveToMemotyLocationMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._CandidateScanListComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ScanListLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ClosedChannelsLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsytemSplitButton, Me._CandidateScanListComboBox, Me._ScanListLabel, Me._ClosedChannelsLabel})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(352, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 15
        Me._SubsystemToolStrip.Text = "Subsystem Tool Strip"
        '
        '_SubsytemSplitButton
        '
        Me._SubsytemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem, Me._CloseMenuItem, Me._OpenMenuItem, Me._OpenAllMenuItem, Me._SaveToMemoryLocationMenuItem})
        Me._SubsytemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsytemSplitButton.Name = "_SubsytemSplitButton"
        Me._SubsytemSplitButton.Size = New System.Drawing.Size(69, 22)
        Me._SubsytemSplitButton.Text = "Scan List"
        Me._SubsytemSplitButton.ToolTipText = "Scan List actions"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(163, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(163, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        '
        '_CloseMenuItem
        '
        Me._CloseMenuItem.Name = "_CloseMenuItem"
        Me._CloseMenuItem.Size = New System.Drawing.Size(163, 22)
        Me._CloseMenuItem.Text = "Close"
        '
        '_OpenMenuItem
        '
        Me._OpenMenuItem.Name = "_OpenMenuItem"
        Me._OpenMenuItem.Size = New System.Drawing.Size(163, 22)
        Me._OpenMenuItem.Text = "Open"
        '
        '_OpenAllMenuItem
        '
        Me._OpenAllMenuItem.Name = "_OpenAllMenuItem"
        Me._OpenAllMenuItem.Size = New System.Drawing.Size(163, 22)
        Me._OpenAllMenuItem.Text = "Open All"
        '
        '_SaveToMemoryLocationMenuItem
        '
        Me._SaveToMemoryLocationMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._MemoryLocationTextBox, Me._SaveToMemotyLocationMenuItem})
        Me._SaveToMemoryLocationMenuItem.Name = "_SaveToMemoryLocationMenuItem"
        Me._SaveToMemoryLocationMenuItem.Size = New System.Drawing.Size(163, 22)
        Me._SaveToMemoryLocationMenuItem.Text = "Memory Location"
        '
        '_MemoryLocationTextBox
        '
        Me._MemoryLocationTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me._MemoryLocationTextBox.Name = "_MemoryLocationTextBox"
        Me._MemoryLocationTextBox.Size = New System.Drawing.Size(100, 23)
        Me._MemoryLocationTextBox.Text = "1"
        '
        '_SaveToMemotyLocationMenuItem
        '
        Me._SaveToMemotyLocationMenuItem.Name = "_SaveToMemotyLocationMenuItem"
        Me._SaveToMemotyLocationMenuItem.Size = New System.Drawing.Size(160, 22)
        Me._SaveToMemotyLocationMenuItem.Text = "Save"
        '
        '_CandidateScanListComboBox
        '
        Me._CandidateScanListComboBox.Items.AddRange(New Object() {"(@1!1:1!10)", "(@M1,M2)", "(@ 4!1,5!1)", "(@1!1)", "(@1!2:1!10, 2!3!6)", "(@1!30:1!40, 2!1:2!10)", "(@1!4!1:1!4!10, 2!1!1:2!4!10)"})
        Me._CandidateScanListComboBox.Name = "_CandidateScanListComboBox"
        Me._CandidateScanListComboBox.Size = New System.Drawing.Size(141, 25)
        Me._CandidateScanListComboBox.Text = "(@1!1:1!10)"
        Me._CandidateScanListComboBox.ToolTipText = "Edits the scan list"
        '
        '_ScanListLabel
        '
        Me._ScanListLabel.Name = "_ScanListLabel"
        Me._ScanListLabel.Size = New System.Drawing.Size(12, 22)
        Me._ScanListLabel.Text = "?"
        '
        '_ClosedChannelsLabel
        '
        Me._ClosedChannelsLabel.Name = "_ClosedChannelsLabel"
        Me._ClosedChannelsLabel.Size = New System.Drawing.Size(57, 22)
        Me._ClosedChannelsLabel.Text = "<closed>"
        Me._ClosedChannelsLabel.ToolTipText = "Scan list of closed channels"
        '
        'ScanListView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "ScanListView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(354, 28)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _CandidateScanListComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsytemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ScanListLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _MemoryLocationTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _SaveToMemotyLocationMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SaveToMemoryLocationMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _OpenAllMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _CloseMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _OpenMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClosedChannelsLabel As Windows.Forms.ToolStripLabel
End Class
