## ISR TTM TM Loader Instructions: How to Load the TTM Firmware

**Turning on the instrument and verifying its connection to the Virtual Machine:**
-   Turn on the instrument;
-   Connect the instrument to the computer from the USB socket at the
    back of the instrument;
-   Click *Player* from the Virtual Machine top window (move the mouse
    to the top of the screen and the top window with drop down). Then
    click *Removable Devices*. The instrument will be listed as:\
    Keithley Instruments SYSTEM Source Meter

**Compiling a new release**

This step is needed only after adding the registration code for the
instrument to the TTM library:
-   Turn on the instrument as described above;
-   Click *VS 2010* from the *Apps* Toolbar folder (bottom right corner
    of the Desktop Window) to open Visual Studio;
-   Click *Start Page* from the *View* menu;
-   Click *TTM.4.x* to open the TTM solution. The solution project will
    display in the *Solution Explorer* at the right side of the Visual
    Studio development environment screen;
-   Right-Click on the *isr TTM Loader* project from the *APPS* folder
    of the *Solution Explorer* and click *Set as StartUp Project*;
-   Right-Click on the *isr TTM Loader* project from the *APPS* folder
    of the *Solution Explorer* and click *Rebuild*. This compiles the
    project and the library;

**Loading the TTM Firmware**
-   Turn on the instrument as described above;
-   Click *TTM Loader* from the TTM Toolbar folder at the bottom left
    corner of the screen;
-   If running from the development environment, click the Run button
    (green right arrow) or press *F5* to run the program;
-   From the *Connect* panel, click the *Search* icon to display the
    instrument resource name in the instrument drop down list;
-   Select the instrument from the list if it is not selected;
-   Click the *Link* icon to connect the instrument. This displays the
    instrument identity in the instrument information panel as follows
    (instrument serial number and revision will change):\
    Keithley Instruments Inc., Model 2601B, 4089222, 3.2.1
-   Select the *Firmware* panel. With a new instrument, the *Load
    Firmware* button will be enabled;
-   Click *Load Firmware*. The firmware will be loaded and the *Save
    Firmware* button will be displayed.
-   Click *Save Firmware*. The firmware will be saved and the *Unload
    Firmware* button will be enabled.
-   The TTM title will display on the instrument panel:\
    TTM 2.3.4009\
    Integrated Scientific Resources
-   Select the *Connect* panel;
-   Click the *Link* icon. This disconnects the instrument. The default
    mV display shows on the instrument panel;
-   Turn off the instrument and proceed to *Testing the TTM Firmware*.
-   Close the program from the program window close button (top right X
    button).

**Testing the TTM Firmware \-- TTM 4.x**
-   Turn on the instrument as described above;
-   Connect a device to test to the instrument *Channel A* connector a
    the back panel;
-   Click *TTM Driver Tester* from the TTM Toolbar folder at the bottom
    left corner of the screen;
-   Select the *Connect* panel;
-   Click *List Resources*;
-   Select the instrument resource name and click *Connect*. The
    instrument will connect and display the TTM title on the instrument
    panel:\
    TTM 2.3.4009\
    Integrated Scientific Resources
-   Select the *Configure* panel. Change any settings, such as *High
    Limit*, and click *Apply Changes* or Apply All;
-   Select the *Measure* panel;
-   From the *Measure* drop down at the bottom click *All*. The
    instrument run the test and measure the initial bridge wire
    resistance, thermal transient, and final bridge wire resistance.
-   Select the *Connect* panel and click *Disconnect*.

**Unloading the TTM Firmware**
-   Turn on the instrument as described above;
-   Click *TTM Loader* from the TTM Toolbar folder at the bottom left
    corner of the screen;
-   If running from the development environment, click the Run button
    (green right arrow) or press *F5* to run the program;
-   From the *Connect* panel, click the *Search* icon to display the
    instrument resource name in the instrument drop down list;
-   Select the instrument from the list if it is not selected;
-   Click the *Link* icon to connect the instrument. This displays the
    instrument identity in the instrument information panel as follows
    (instrument serial number and revision will change):\
    Keithley Instruments Inc., Model 2601B, 4089222, 3.2.1
-   Select the *Firmware* panel. With a new instrument, the *Load
    Firmware* button will be enabled;
-   Click Unload Firmware. The firmware will be removed.

**Updating the TTM Firmware**
-   Follow the procedures for *Unloading the TTM Firmware* and then the
    steps for *Loading the TTM Firmware*.

**TTM Driver 4.x Tester Known Bugs**

Cross Thread Exception

Observation:
-   At time, measuring causes a cross thread exception. This exception
    results from attempting to display content on the screen from a
    program thread other than the thread of the screen. This is a know
    Windows issue and will be fixed on future releases. Click ignore as
    this does not affect actual measurements.

Workaround:
-   At time, measuring causes a cross thread exception. This exception
    results from attempting to display content on the screen from a
    program thread other than the thread of the screen. This is a know
    Windows issue and will be fixed on future releases. Click ignore as
    this does not affect actual measurements.

\(c\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

Licensed under the [ISR Fair End User Use License Version
*1.0](http://www.isr.cc/licenses/FairEndUserUseLicense.pdf).
Unless required by applicable law or agreed to in writing, this software
is provided \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.
