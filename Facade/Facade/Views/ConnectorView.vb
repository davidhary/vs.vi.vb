'---------------------------------------------------------------------------------------------------
' file:		.\Views\ConnectorView.vb
'
' summary:	Connector view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A connector view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ConnectorView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ConnectorView"/> </summary>
    ''' <returns> A <see cref="ConnectorView"/>. </returns>
    Public Shared Function Create() As ConnectorView
        Dim view As ConnectorView = Nothing
        Try
            view = New ConnectorView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the session is unbound in case the form is closed without closing the device.
                Me.BindVisaSessionBaseThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " VISA SESSION BASE (DEVICE BASE) "

    ''' <summary> Gets or sets the visa session base. </summary>
    ''' <value> The visa session base. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VI.VisaSessionBase

    ''' <summary> Bind visa session base. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub BindVisaSessionBase(ByVal value As VI.VisaSessionBase)
        Me.BindVisaSessionBaseThis(value)
    End Sub

    ''' <summary> Bind visa session base. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub BindVisaSessionBaseThis(ByVal value As VI.VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            Me.AssignTalker(Nothing)
            RemoveHandler Me.VisaSessionBase.Opening, AddressOf Me.DeviceOpening
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.DeviceOpened
            RemoveHandler Me.VisaSessionBase.Closing, AddressOf Me.DeviceClosing
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.DeviceClosed
            RemoveHandler Me.VisaSessionBase.Initialized, AddressOf Me.DeviceInitialized
            RemoveHandler Me.VisaSessionBase.Initializing, AddressOf Me.DeviceInitializing
            Me._ResourceSelectorConnector.Enabled = False
            Me._ResourceSelectorConnector.AssignSelectorViewModel(Nothing)
            Me._ResourceSelectorConnector.AssignOpenerViewModel(Nothing)
            Me._ResourceSelectorConnector.AssignTalker(Nothing)
        End If
        Me._VisaSessionBase = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.VisaSessionBase.Talker)
            AddHandler Me.VisaSessionBase.Opening, AddressOf Me.DeviceOpening
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.DeviceOpened
            AddHandler Me.VisaSessionBase.Closing, AddressOf Me.DeviceClosing
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.DeviceClosed
            AddHandler Me.VisaSessionBase.Initialized, AddressOf Me.DeviceInitialized
            AddHandler Me.VisaSessionBase.Initializing, AddressOf Me.DeviceInitializing
            Me._ResourceSelectorConnector.Enabled = True
            Me._ResourceSelectorConnector.Openable = True : Me._ResourceSelectorConnector.Clearable = True : Me._ResourceSelectorConnector.Searchable = True
            Me._ResourceSelectorConnector.AssignSelectorViewModel(value.SessionFactory)
            Me._ResourceSelectorConnector.AssignOpenerViewModel(value)
            Me._ResourceSelectorConnector.AssignTalker(value.Talker)
        End If
        Me.BindSessionFactory(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.VisaSessionBase.ResourceNameCaption} reading service request"
        Try
            Me.VisaSessionBase.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE EVENTS "

    ''' <summary>
    ''' Event handler. Called upon device opening so as to instantiated all subsystems.
    ''' </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Sub DeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Me.PublishVerbose($"Opening access to {Me.ResourceName};. ")
        Me._IdentityTextBox.Text = "connecting..."
    End Sub

    ''' <summary>
    ''' Event handler. Called after the device opened and all subsystems were defined.
    ''' </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim outcome As TraceEventType = TraceEventType.Information
        If Me.VisaSessionBase.Session.Enabled And Not Me.VisaSessionBase.Session.IsSessionOpen Then outcome = TraceEventType.Warning
        Me.Publish(outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ",
                   Me.VisaSessionBase.ResourceTitleCaption, Me.VisaSessionBase.Session.Enabled.GetHashCode,
                   Me.VisaSessionBase.Session.IsDeviceOpen.GetHashCode, Me.VisaSessionBase.Session.IsSessionOpen.GetHashCode)
        Dim activity As String = String.Empty
        Try
            Me._IdentityTextBox.Text = "identifying..."
            Application.DoEvents()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Attempts to open a session to the device using the specified resource name.
    ''' </summary>
    ''' <remarks> David, 2020-06-08. </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The title. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryOpenDeviceSession(ByVal resourceName As String, ByVal resourceTitle As String) As (Success As Boolean, Details As String)
        Dim activity As String = $"opening {resourceName} VISA session"
        Try
            Dim r As (Success As Boolean, Details As String) = Me.VisaSessionBase.TryOpenSession(resourceName, resourceTitle)
            Return If(r.Success, (r.Success, r.Details), (r.Success, $"failed {activity};. { r.Details}"))
        Catch ex As Exception
            Return (False, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Function

    ''' <summary> Device initializing. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Overridable Sub DeviceInitializing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
        Me._IdentityTextBox.Text = Me.VisaSessionBase.StatusSubsystemBase.Identity
    End Sub

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                activity = $"Disconnecting from {Me.ResourceName}" : Me.PublishInfo($"{activity};. ")
                If Me.VisaSessionBase.Session IsNot Nothing Then Me.VisaSessionBase.Session.DisableServiceRequestEventHandler()
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                activity = $"Disconnected from {Me.ResourceName}" : Me.PublishVerbose($"{activity};. ")
                If Me.VisaSessionBase.Session.IsSessionOpen Then
                    Me.PublishWarning($"{Me.VisaSessionBase.Session.ResourceNameCaption} closed but session still open;. ")
                    Me._IdentityTextBox.Text = "<failed disconnecting>"
                ElseIf Me.VisaSessionBase.Session.IsDeviceOpen Then
                    Me.PublishWarning($"{Me.VisaSessionBase.Session.ResourceNameCaption} closed but emulated session still open;. ")
                    Me._IdentityTextBox.Text = "<failed disconnecting>"
                Else
                    Me.PublishVerbose("Disconnected; Device access closed.")
                    Me._IdentityTextBox.Text = "<disconnected>"
                End If
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        Else
            Me.PublishInfo("Disconnected; Device disposed.")
        End If
    End Sub

#End Region

#Region " SESSION FACTORY "

    ''' <summary> Gets or sets the session factory. </summary>
    ''' <value> The session factory. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SessionFactory As VI.SessionFactory

    ''' <summary> Bind session factory. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub BindSessionFactory(ByVal value As VI.VisaSessionBase)
        If value Is Nothing Then
            Me.BindSessionFactoryThis(Nothing)
        Else
            Me.BindSessionFactoryThis(value.SessionFactory)
        End If
    End Sub

    ''' <summary> Bind session factory. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub BindSessionFactoryThis(ByVal value As VI.SessionFactory)
        If Me.SessionFactory IsNot Nothing Then
            Me.AssignTalker(Nothing)
            RemoveHandler Me.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
        End If
        Me._SessionFactory = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.SessionFactory.Talker)
            AddHandler Me.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            Me.HandlePropertyChanged(value, NameOf(VI.SessionFactory.IsOpen))
        End If
    End Sub

    ''' <summary> Name of the resource. </summary>
    Private ReadOnly _ResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourceName As String
        Get
            Return Me._ResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceName) Then
                Me.VisaSessionBase.SessionFactory.CandidateResourceName = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Search Pattern of the resource. </summary>
    ''' <value> The Search Pattern of the resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ResourceFilter As String
        Get
            Return Me.VisaSessionBase.SessionFactory.ResourcesFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceFilter) Then
                Me.VisaSessionBase.SessionFactory.ResourcesFilter = value
            End If
        End Set
    End Property

    ''' <summary> Executes the session factory property changed action. </summary>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal sender As SessionFactory, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(SessionFactory.ValidatedResourceName)
                Me.ResourceName = sender.ValidatedResourceName
            Case NameOf(SessionFactory.CandidateResourceName)
                Me.ResourceName = sender.CandidateResourceName
            Case NameOf(SessionFactory.IsOpen)
                If sender.IsOpen Then
                    Me.ResourceName = sender.ValidatedResourceName
                    Me._IdentityTextBox.Text = $"Resource {Me.ResourceName} connected"
                    Me.PublishInfo($"Resource connected;. {Me.ResourceName}")
                Else
                    Me.ResourceName = sender.CandidateResourceName
                    Me._IdentityTextBox.Text = $"Resource {Me.ResourceName} not open"
                    Me.PublishInfo($"Resource not open;. {Me.ResourceName}")
                End If

        End Select
    End Sub

    ''' <summary> Session factory property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionFactoryPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling session factory {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SessionFactoryPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SessionFactory), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
