'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\SystemSubsystemBase.vb
'
' summary:	System subsystem base class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

''' <summary> Defines the contract that must be implemented by System Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SystemSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SystemSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me._PresetRefractoryPeriod = TimeSpan.FromMilliseconds(10)
    End Sub

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> The automatic zero enabled. </summary>
    Private _AutoZeroEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Zero enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Zero enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoZeroEnabled As Boolean?
        Get
            Return Me._AutoZeroEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoZeroEnabled, value) Then
                Me._AutoZeroEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Zero enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoZeroEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoZeroEnabled(value)
        Return Me.QueryAutoZeroEnabled()
    End Function

    ''' <summary> Gets or sets the Auto Zero enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:AZERO?". </remarks>
    ''' <value> The Auto Zero enabled query command. </value>
    Protected Overridable Property AutoZeroEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Zero Enabled sentinel. Also sets the
    ''' <see cref="AutoZeroEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoZeroEnabled() As Boolean?
        Me.AutoZeroEnabled = Me.Query(Me.AutoZeroEnabled, Me.AutoZeroEnabledQueryCommand)
        Return Me.AutoZeroEnabled
    End Function

    ''' <summary> Gets or sets the Auto Zero enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:AZERO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Auto Zero enabled query command. </value>
    Protected Overridable Property AutoZeroEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Zero Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoZeroEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoZeroEnabled = Me.Write(value, Me.AutoZeroEnabledCommandFormat)
        Return Me.AutoZeroEnabled
    End Function

#End Region

#Region " BEEPER ENABLED + IMMEDIATE "

    ''' <summary> The beeper enabled. </summary>
    Private _BeeperEnabled As Boolean?

    ''' <summary> Gets or sets the cached Beeper enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Beeper enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property BeeperEnabled As Boolean?
        Get
            Return Me._BeeperEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.BeeperEnabled, value) Then
                Me._BeeperEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Beeper enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyBeeperEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteBeeperEnabled(value)
        Return Me.QueryBeeperEnabled()
    End Function

    ''' <summary> Gets or sets the Beeper enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overridable Property BeeperEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Beeper Enabled sentinel. Also sets the
    ''' <see cref="BeeperEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryBeeperEnabled() As Boolean?
        Me.BeeperEnabled = Me.Query(Me.BeeperEnabled, Me.BeeperEnabledQueryCommand)
        Return Me.BeeperEnabled
    End Function

    ''' <summary> Gets or sets the Beeper enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overridable Property BeeperEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Beeper Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteBeeperEnabled(ByVal value As Boolean) As Boolean?
        Me.BeeperEnabled = Me.Write(value, Me.BeeperEnabledCommandFormat)
        Return Me.BeeperEnabled
    End Function

    ''' <summary> Gets or sets the beeper immediate command format. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:IMM {0}, {1}". </remarks>
    ''' <value> The beeper immediate command format. </value>
    Protected Overridable Property BeeperImmediateCommandFormat As String

    ''' <summary> Commands the instrument to issue a Beep on the instrument. </summary>
    ''' <param name="frequency"> Specifies the frequency of the beep. </param>
    ''' <param name="duration">  Specifies the duration of the beep. </param>
    Public Sub BeepImmediately(ByVal frequency As Integer, ByVal duration As Single)
        If Not String.IsNullOrEmpty(Me.BeeperImmediateCommandFormat) Then
            Me.Session.WriteLine(Me.BeeperEnabledCommandFormat, frequency, duration)

        End If
    End Sub

#End Region

#Region " CONTACT CHECK ENABLED "

    ''' <summary> The contact check enabled. </summary>
    Private _ContactCheckEnabled As Boolean?

    ''' <summary> Gets or sets the cached Contact Check enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Contact Check enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property ContactCheckEnabled As Boolean?
        Get
            Return Me._ContactCheckEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ContactCheckEnabled, value) Then
                Me._ContactCheckEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Contact Check enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyContactCheckEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteContactCheckEnabled(value)
        Return Me.QueryContactCheckEnabled()
    End Function

    ''' <summary> Gets or sets the Contact Check enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:CCH?". </remarks>
    ''' <value> The Contact Check enabled query command. </value>
    Protected Overridable Property ContactCheckEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the contact check Enabled sentinel. Also sets the
    ''' <see cref="ContactCheckEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryContactCheckEnabled() As Boolean?
        Me.ContactCheckEnabled = Me.Query(Me.ContactCheckEnabled, Me.ContactCheckEnabledQueryCommand)
        Return Me.ContactCheckEnabled
    End Function

    ''' <summary> Gets or sets the Contact Check enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:CCH {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Contact Check enabled query command. </value>
    Protected Overridable Property ContactCheckEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the contact check Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteContactCheckEnabled(ByVal value As Boolean) As Boolean?
        Me.ContactCheckEnabled = Me.Write(value, Me.ContactCheckEnabledCommandFormat)
        Return Me.ContactCheckEnabled
    End Function

#End Region

#Region " CONTACT CHECK RESISTANCE "

    ''' <summary> The Contact Check Resistance. </summary>
    Private _ContactCheckResistance As Double?

    ''' <summary> Gets or sets the cached source Contact Check Resistance. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property ContactCheckResistance As Double?
        Get
            Return Me._ContactCheckResistance
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.ContactCheckResistance, value) Then
                Me._ContactCheckResistance = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the ContactCheck Resistance. </summary>
    ''' <param name="value"> the ContactCheck Resistance. </param>
    ''' <returns> the ContactCheck Resistance. </returns>
    Public Function ApplyContactCheckResistance(ByVal value As Double) As Double?
        Me.WriteContactCheckResistance(value)
        Return Me.QueryContactCheckResistance
    End Function

    ''' <summary> Gets or sets the ContactCheck Resistance query command. </summary>
    ''' <remarks> scpi: "SYST:CCH:RES?". </remarks>
    ''' <value> the ContactCheck Resistance query command. </value>
    Protected Overridable Property ContactCheckResistanceQueryCommand As String

    ''' <summary> Queries the ContactCheck Resistance. </summary>
    ''' <returns> the ContactCheck Resistance or none if unknown. </returns>
    Public Function QueryContactCheckResistance() As Double?
        Me.ContactCheckResistance = MyBase.Query(Me.ContactCheckResistance, Me.ContactCheckResistanceQueryCommand)
        Return Me.ContactCheckResistance
    End Function

    ''' <summary> Gets or sets the ContactCheck Resistance command format. </summary>
    ''' <remarks> scpi: "SYST:CCH:RES {0}". </remarks>
    ''' <value> the ContactCheck Resistance command format. </value>
    Protected Overridable Property ContactCheckResistanceCommandFormat As String

    ''' <summary>
    ''' Writes the ContactCheck Resistance without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets the ContactCheck Resistance. </remarks>
    ''' <param name="value"> the ContactCheck Resistance. </param>
    ''' <returns> the ContactCheck Resistance. </returns>
    Public Function WriteContactCheckResistance(ByVal value As Double) As Double?
        Me.ContactCheckResistance = MyBase.Write(value, Me.ContactCheckResistanceCommandFormat)
        Return Me.ContactCheckResistance
    End Function

#End Region

#Region " CONTACT CHECK SUPPORTED "

    ''' <summary> The contact check supported. </summary>
    Private _ContactCheckSupported As Boolean?

    ''' <summary> Gets or sets the contact check supported. </summary>
    ''' <value> The contact check supported. </value>
    Public Property ContactCheckSupported() As Boolean?
        Get
            Return Me._ContactCheckSupported
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ContactCheckSupported, value) Then
                Me._ContactCheckSupported = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the contact check option value. </summary>
    ''' <value> The contact check option value. </value>
    Protected Overridable Property ContactCheckOptionValue As String = "CONTACT-CHECK"

    ''' <summary> Determines if contact check option is supported. </summary>
    ''' <param name="options"> The options. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overridable Function IsContactCheckSupported(ByVal options As String) As Boolean
        Return Not String.IsNullOrWhiteSpace(options) AndAlso options.Contains(Me.ContactCheckOptionValue)
    End Function

    ''' <summary> Queries support for contact check. </summary>
    ''' <returns> <c>True</c> if supports contact check; otherwise <c>False</c>. </returns>
    Public Overridable Function QueryContactCheckSupported() As Boolean?
        Me.QueryOptions()
        Return Me.ContactCheckSupported
    End Function

#End Region

#Region " FAN LEVEL "

    ''' <summary> The Fan Level. </summary>
    Private _FanLevel As FanLevel?

    ''' <summary> Gets or sets the cached Fan Level. </summary>
    ''' <value> The Fan Level or null if unknown. </value>
    Public Property FanLevel As FanLevel?
        Get
            Return Me._FanLevel
        End Get
        Protected Set(ByVal value As FanLevel?)
            If Not Nullable.Equals(Me.FanLevel, value) Then
                Me._FanLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Converts the specified value to string. </summary>
    ''' <param name="value"> The <see cref="FanLevel">Fan Level</see>. </param>
    ''' <returns> A String. </returns>
    Protected Overridable Function FromFanLevel(ByVal value As FanLevel) As String
        Return If(value = VI.FanLevel.Normal, "NORM", "QUIET")
    End Function

    ''' <summary> Converts a value to a fan level. </summary>
    ''' <param name="value"> The <see cref="FanLevel">Route Fan Level</see>. </param>
    ''' <returns> Value as a FanLevel. </returns>
    Private Function ToFanLevel(ByVal value As String) As FanLevel
        Return If(value.StartsWith(Me.FromFanLevel(VI.FanLevel.Quiet), StringComparison.OrdinalIgnoreCase),
            VI.FanLevel.Quiet,
            VI.FanLevel.Normal)
    End Function

    ''' <summary> Writes and reads back the Fan Level. </summary>
    ''' <param name="value"> The <see cref="FanLevel">Fan Level</see>. </param>
    ''' <returns> The Fan Level or null if unknown. </returns>
    Public Function ApplyFanLevel(ByVal value As FanLevel) As FanLevel?
        Me.WriteFanLevel(value)
        Return Me.QueryFanLevel()
    End Function

    ''' <summary> Gets or sets the Fan Level query command. </summary>
    ''' <value> The Fan Level command. </value>
    Protected Overridable Property FanLevelQueryCommand As String

    ''' <summary>
    ''' Queries the Fan Level. Also sets the <see cref="FanLevel">output on</see> sentinel.
    ''' </summary>
    ''' <returns> The Fan Level or null if unknown. </returns>
    Public Function QueryFanLevel() As FanLevel?
        Return Me.ToFanLevel(Me.Query(Me.FromFanLevel(Me.FanLevel.GetValueOrDefault(VI.FanLevel.Normal)), Me.FanLevelQueryCommand))
    End Function

    ''' <summary> Gets or sets the Fan Level command format. </summary>
    ''' <value> The Fan Level command format. </value>
    Protected Overridable Property FanLevelCommandFormat As String

    ''' <summary> Writes the Fan Level. Does not read back from the instrument. </summary>
    ''' <param name="value"> The Fan Level. </param>
    ''' <returns> The Fan Level or null if unknown. </returns>
    Public Function WriteFanLevel(ByVal value As FanLevel) As FanLevel?
        Me.Write(Me.FanLevelCommandFormat, Me.FromFanLevel(value))
        Me.FanLevel = value
        Return Me.FanLevel
    End Function

#End Region

#Region " FOUR WIRE SENSE ENABLED "

    ''' <summary> The four wire sense enabled. </summary>
    Private _FourWireSenseEnabled As Boolean?

    ''' <summary> Gets or sets the cached Four Wire Sense enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Four Wire Sense enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property FourWireSenseEnabled As Boolean?
        Get
            Return Me._FourWireSenseEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.FourWireSenseEnabled, value) Then
                Me._FourWireSenseEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Four Wire Sense enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyFourWireSenseEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteFourWireSenseEnabled(value)
        Return Me.QueryFourWireSenseEnabled()
    End Function

    ''' <summary> Gets the Four Wire Sense enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:RSEN?". </remarks>
    ''' <value> The Four Wire Sense enabled query command. </value>
    Protected Overridable Property FourWireSenseEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Four Wire Sense Enabled sentinel. Also sets the
    ''' <see cref="FourWireSenseEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryFourWireSenseEnabled() As Boolean?
        Me.FourWireSenseEnabled = Me.Query(Me.FourWireSenseEnabled, Me.FourWireSenseEnabledQueryCommand)
        Return Me.FourWireSenseEnabled
    End Function

    ''' <summary> Gets the Four Wire Sense enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:RSEN {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Four Wire Sense enabled query command. </value>
    Protected Overridable Property FourWireSenseEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Four Wire Sense Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteFourWireSenseEnabled(ByVal value As Boolean) As Boolean?
        Me.FourWireSenseEnabled = Me.Write(value, Me.FourWireSenseEnabledCommandFormat)
        Return Me.FourWireSenseEnabled
    End Function

#End Region

#Region " FRONT TERMINALS SELECTED "

    ''' <summary> Gets the front terminal label. </summary>
    ''' <value> The front terminal label. </value>
    Public Property FrontTerminalLabel As String = "F"

    ''' <summary> Gets the rear terminal label. </summary>
    ''' <value> The rear terminal label. </value>
    Public Property RearTerminalLabel As String = "R"

    ''' <summary> Gets the unknown terminal label. </summary>
    ''' <value> The unknown terminal label. </value>
    Public Property UnknownTerminalLabel As String = String.Empty

    ''' <summary> Gets the terminals caption. </summary>
    ''' <value> The terminals caption. </value>
    Public ReadOnly Property TerminalsCaption As String
        Get
            Return If(Me.FrontTerminalsSelected.HasValue, If(Me.FrontTerminalsSelected.Value, Me.FrontTerminalLabel, Me.RearTerminalLabel), Me.UnknownTerminalLabel)
        End Get
    End Property

    ''' <summary> Gets true if the subsystem supports front terminals selection query. </summary>
    ''' <value>
    ''' The value indicating if the subsystem supports front terminals selection query.
    ''' </value>
    Public ReadOnly Property SupportsFrontTerminalsSelectionQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.FrontTerminalsSelectedQueryCommand)
        End Get
    End Property

    ''' <summary> The front terminals selected. </summary>
    Private _FrontTerminalsSelected As Boolean?

    ''' <summary> Gets or sets the cached Front Terminals Selected sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Front Terminals Selected is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property FrontTerminalsSelected As Boolean?
        Get
            Return Me._FrontTerminalsSelected
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.FrontTerminalsSelected, value) Then
                Me._FrontTerminalsSelected = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(MultimeterSubsystemBase.TerminalsCaption))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Front Terminals Selected sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if Selected; otherwise <c>False</c>. </returns>
    Public Function ApplyFrontTerminalsSelected(ByVal value As Boolean) As Boolean?
        Me.WriteFrontTerminalsSelected(value)
        Return Me.QueryFrontTerminalsSelected()
    End Function

    ''' <summary> Gets or sets the Front Terminals Selected query command. </summary>
    ''' <remarks> SCPI: ":SYST:FRSW?". </remarks>
    ''' <value> The Front Terminals Selected query command. </value>
    Protected Overridable Property FrontTerminalsSelectedQueryCommand As String

    ''' <summary>
    ''' Queries the Front Terminals Selected sentinel. Also sets the
    ''' <see cref="FrontTerminalsSelected">Selected</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if Selected; otherwise <c>False</c>. </returns>
    Public Function QueryFrontTerminalsSelected() As Boolean?
        Me.FrontTerminalsSelected = Me.Query(Me.FrontTerminalsSelected, Me.FrontTerminalsSelectedQueryCommand)
        Return Me.FrontTerminalsSelected
    End Function

    ''' <summary> Gets or sets the Front Terminals Selected command Format. </summary>
    ''' <remarks> SCPI: ":SYST:FRSW {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Front Terminals Selected query command. </value>
    Protected Overridable Property FrontTerminalsSelectedCommandFormat As String

    ''' <summary>
    ''' Writes the Front Terminals Selected sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is Selected. </param>
    ''' <returns> <c>True</c> if Selected; otherwise <c>False</c>. </returns>
    Public Function WriteFrontTerminalsSelected(ByVal value As Boolean) As Boolean?
        Me.FrontTerminalsSelected = Me.Write(value, Me.FrontTerminalsSelectedCommandFormat)
        Return Me.FrontTerminalsSelected
    End Function

#End Region

#Region " MEMORY: INITIALIZE "

    ''' <summary> Gets or sets the initialize memory command. </summary>
    ''' <value> The initialize memory command. </value>
    Protected Overridable Property InitializeMemoryCommand As String

    ''' <summary>
    ''' Initializes battery backed RAM. This initializes trace, source list, user-defined math,
    ''' source-memory locations, standard save setups, and all call math expressions.
    ''' </summary>
    ''' <remarks> Sends the <see cref="InitializeMemoryCommand"/> message. </remarks>
    Public Sub InitializeMemory()
        If Not String.IsNullOrWhiteSpace(Me.InitializeMemoryCommand) Then
            Me.Session.WriteLine(Me.InitializeMemoryCommand)
        End If
    End Sub

#End Region

#Region " OPTIONS "

    ''' <summary> Gets or sets the option query command. </summary>
    ''' <value> The option query command. </value>
    Protected Overridable Property OptionQueryCommand As String

    ''' <summary> Options for controlling the operation. </summary>
    Private _Options As String

    ''' <summary> Gets or sets options for controlling the operation. </summary>
    ''' <value> The options. </value>
    Public Property Options As String
        Get
            Return Me._Options
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.Options) Then
                Me._Options = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries the options. </summary>
    ''' <returns> The options. </returns>
    Public Overridable Function QueryOptions() As String
        Me.Options = If(String.IsNullOrWhiteSpace(Me.OptionQueryCommand), String.Empty, Me.Session.Query(Me.OptionQueryCommand))
        Me.ContactCheckSupported = Me.IsContactCheckSupported(Me.Options)
        Me.EnumerateInstalledScanCards(Me.Options)
        Me.ParseMemoryOption(Me.Options)
        Return Me.Options
    End Function

#End Region

#Region " MEMORY OPTION "

    ''' <summary> The memory option. </summary>
    Private _MemoryOption As Integer?

    ''' <summary> Gets or sets the memory Option. </summary>
    ''' <value> The memory Option. </value>
    Public Property MemoryOption() As Integer?
        Get
            Return Me._MemoryOption
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Integer?.Equals(Me.MemoryOption, value) Then
                Me._MemoryOption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the memory 1 option value. </summary>
    ''' <value> The memory 1 option value. </value>
    Protected Overridable Property Memory1OptionValue As String = "MEM1"

    ''' <summary> Gets or sets the memory 2 option value. </summary>
    ''' <value> The memory 2 option value. </value>
    Protected Overridable Property Memory2OptionValue As String = "MEM2"

    ''' <summary> Parses the memory option. </summary>
    ''' <param name="options"> The options. </param>
    ''' <returns> An Integer. </returns>
    Public Overridable Function ParseMemoryOption(ByVal options As String) As Integer
        If String.IsNullOrWhiteSpace(options) Then
            Me.MemoryOption = 0
        ElseIf options.IndexOf(Me.Memory1OptionValue, StringComparison.OrdinalIgnoreCase) >= 0 Then
            Me.MemoryOption = 1
        ElseIf options.IndexOf(Me.Memory2OptionValue, StringComparison.OrdinalIgnoreCase) >= 0 Then
            Me.MemoryOption = 2
        Else
            Me.MemoryOption = 0
        End If
        Return Me.MemoryOption.Value
    End Function

#End Region

#Region " PRESET "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <remarks>
    ''' SCPI: ":SYST:PRES".
    ''' <see cref="VI.Pith.Scpi.Syntax.StatusPresetCommand"> </see>
    ''' </remarks>
    ''' <value> The preset command. </value>
    Protected Overridable Property PresetCommand As String = VI.Pith.Scpi.Syntax.SystemPresetCommand

    ''' <summary> Sets the known preset state. </summary>
    Public Overrides Sub PresetKnownState()
        If Not String.IsNullOrWhiteSpace(Me.PresetCommand) Then
            Me.Session.OperationCompleted = New Boolean?
            Me.Execute(Me.PresetCommand)
            If Me.Session.IsSessionOpen Then
                isr.Core.ApplianceBase.DoEventsWait(Me.PresetRefractoryPeriod)
            End If
            Me.Session.QueryOperationCompleted()
            Me.Session.ReadStatusRegister()
        End If
    End Sub

    ''' <summary> The preset refractory period. </summary>
    Private _PresetRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the post-preset refractory period. </summary>
    ''' <value> The post-preset refractory period. </value>
    Public Property PresetRefractoryPeriod As TimeSpan
        Get
            Return Me._PresetRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.PresetRefractoryPeriod) Then
                Me._PresetRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SCAN-CARD INSTALLED "

    ''' <summary> The installed scan cards. </summary>
    Private ReadOnly _InstalledScanCards As New List(Of String)

    ''' <summary> Gets or sets the installed scan cards. </summary>
    ''' <value> The installed scan cards. </value>
    Public Property InstalledScanCards As IList(Of String)
        Get
            Return Me._InstalledScanCards
        End Get
        Set(value As IList(Of String))
            If Not List(Of String).Equals(value, Me.InstalledScanCards) Then
                Me._InstalledScanCards.Clear()
                Me._InstalledScanCards.AddRange(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets a list of names of the scan cards. The list is empty of the instrument does not support
    ''' scan cards.
    ''' </summary>
    ''' <value> A list of names of the scan cards. </value>
    Protected Overridable Property ScanCardNames As IList(Of String) = Array.Empty(Of String)

    ''' <summary> Enumerates the installed scan cards. </summary>
    ''' <param name="options"> The options. </param>
    ''' <returns> <c>true</c> if any scan cards are installed; otherwise <c>false</c> </returns>
    Public Overridable Function EnumerateInstalledScanCards(ByVal options As String) As Boolean
        Dim l As New List(Of String)
        If Me.ScanCardNames.Any Then
            For Each cardName As String In Me.ScanCardNames
                If options.Contains(cardName) Then l.Add(cardName)
            Next
        End If
        Me.InstalledScanCards = l
        Return Me.InstalledScanCards.Any
    End Function

    ''' <summary> Queries the installed scan cards. </summary>
    ''' <returns> <c>True</c> if any scan cards were installed; otherwise <c>False</c>. </returns>
    Public Overridable Function QueryInstalledScanCards() As Boolean?
        Me.QueryOptions()
        Return Me.InstalledScanCards.Any
    End Function

    ''' <summary> Returns true if the instrument supports scan card options. </summary>
    ''' <value> <c>True</c> if the instrument supports scan card options. </value>
    Public ReadOnly Property SupportsScanCardOption As Boolean
        Get
            Return Me.ScanCardNames.Any
        End Get
    End Property

#End Region

#Region " VERSION: FIRMWARE "

    ''' <summary> The Firmware Version. </summary>
    Private _FirmwareVersion As Double?

    ''' <summary> Gets or sets the cached version the Firmware. </summary>
    ''' <value> The Firmware Version. </value>
    Public Property FirmwareVersion As Double?
        Get
            Return Me._FirmwareVersion
        End Get
        Protected Set(value As Double?)
            Me._FirmwareVersion = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets or sets the Firmware Version query command. </summary>
    ''' <value> The Firmware Version query command. </value>
    Protected Overridable Property FirmwareVersionQueryCommand As String

    ''' <summary> Queries the Firmware version. </summary>
    ''' <returns> System.Nullable{System.Double}. </returns>
    Public Function QueryFirmwareVersion() As Double?
        If Not String.IsNullOrWhiteSpace(Me.FirmwareVersionQueryCommand) Then
            Me._FirmwareVersion = Me.Session.Query(0.0F, Me.FirmwareVersionQueryCommand)
        End If
        Return Me.FirmwareVersion
    End Function

#End Region

#Region " VERSION: LANGUAGE "

    ''' <summary> The Language revision. </summary>
    Private _LanguageRevision As Double?

    ''' <summary>
    ''' Gets or sets the cached version level of the Language standard implemented by the device.
    ''' </summary>
    ''' <value> The Language revision. </value>
    Public Property LanguageRevision As Double?
        Get
            Return Me._LanguageRevision
        End Get
        Protected Set(value As Double?)
            Me._LanguageRevision = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets or sets the Language revision query command. </summary>
    ''' <remarks> ':SYST:VERS?'. </remarks>
    ''' <value> The Language revision query command. </value>
    Protected Overridable Property LanguageRevisionQueryCommand As String

    ''' <summary>
    ''' Queries the version level of the Language standard implemented by the device.
    ''' </summary>
    ''' <remarks> Sends the ':SYST:VERS?' query. </remarks>
    ''' <returns> System.Nullable{System.Double}. </returns>
    Public Function QueryLanguageRevision() As Double?
        If Not String.IsNullOrWhiteSpace(Me.LanguageRevisionQueryCommand) Then
            Me._LanguageRevision = Me.Session.Query(0.0F, Me.LanguageRevisionQueryCommand)
        End If
        Return Me.LanguageRevision
    End Function

#End Region

End Class

''' <summary> Values that represent fan levels. </summary>
Public Enum FanLevel

    ''' <summary> An enum constant representing the normal option. </summary>
    <Description("Normal")>
    Normal

    ''' <summary> An enum constant representing the quiet option. </summary>
    <Description("Quiet")>
    Quiet
End Enum
