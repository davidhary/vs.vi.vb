'---------------------------------------------------------------------------------------------------
' file:		National.Visa\Code\GpibInterfaceExtensions.vb
'
' summary:	Gpib interface extensions class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices

''' <summary> Extensions for GPIB Interface. </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2005-01-21, 1.0.1847.x. </para></remarks>
Friend Module GpibInterfaceExtensions

#Region " GPIB INTERFACE "

    ''' <summary> Returns all instruments to some default state. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    <Extension()>
    Public Sub ClearDevices(ByVal value As NationalInstruments.Visa.GpibInterface)
        If value Is Nothing Then
            Throw New ArgumentNullException(NameOf(value))
        End If
        ' Transmit the DCL command to the interface.
        value.SendCommand(VI.Pith.Ieee488.Syntax.BuildDeviceClear.ToArray)
    End Sub

    ''' <summary> Clears the specified device. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">       The value. </param>
    ''' <param name="gpibAddress"> The instrument address. </param>
    <Extension()>
    Public Sub SelectiveDeviceClear(ByVal value As NationalInstruments.Visa.GpibInterface, ByVal gpibAddress As Integer)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        value.SendCommand(VI.Pith.Ieee488.Syntax.BuildSelectiveDeviceClear(CByte(gpibAddress)).ToArray)
    End Sub

    ''' <summary> Clears the specified device. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        The value. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    <Extension()>
    Public Sub SelectiveDeviceClear(ByVal value As NationalInstruments.Visa.GpibInterface, ByVal resourceName As String)
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))
        Using rc As ResourceManager = New ResourceManager()
            Dim resourceNameInfo As VI.Pith.ResourceNameInfo = rc.ParseResource(resourceName)
            If resourceNameInfo.GpibAddress > 0 Then value.SelectiveDeviceClear(resourceNameInfo.GpibAddress)
        End Using
    End Sub

    ''' <summary> Clears the interface. Resets interface if instruments are not connected. </summary>
    ''' <param name="value"> The value. </param>
    <Extension()>
    Public Sub ClearInterface(ByVal value As NationalInstruments.Visa.GpibInterface)
        value.SendInterfaceClear()
        value.ClearDevices()
    End Sub

#End Region

End Module
