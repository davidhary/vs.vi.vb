'---------------------------------------------------------------------------------------------------
' file:		.\Device\SessionFactory_Select.vb
'
' summary:	Session factory select class
'---------------------------------------------------------------------------------------------------

Partial Public Class SessionFactory

    ''' <summary> Applies the session factory. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    Public Sub ApplySessionFactory()
        Me.Factory = New Foundation.SessionFactory
        ' Me.Factory = New isr.VI.National.Visa.SessionFactory
    End Sub

End Class
