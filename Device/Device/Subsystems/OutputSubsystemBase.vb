''' <summary> Defines the contract that must be implemented by an Output Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class OutputSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="OutputSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DefineOutputOffModeReadWrites()
        Me.DefineOutputTerminalsModeReadWrites()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.OutputOnState = False
        Me.OutputOffMode = OutputOffModes.Normal
        Me.OutputTerminalsMode = OutputTerminalsModes.Front
    End Sub

#End Region

#Region " ON/OFF STATE "

    ''' <summary> State of the output on. </summary>
    Private _OutputOnState As Boolean?

    ''' <summary> Gets or sets the cached output on/off state. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property OutputOnState As Boolean?
        Get
            Return Me._OutputOnState
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.OutputOnState, value) Then
                Me._OutputOnState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the output on/off state. </summary>
    ''' <param name="value"> if set to <c>True</c> if turning on; False if turning output off. </param>
    ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
    Public Function ApplyOutputOnState(ByVal value As Boolean) As Boolean?
        Me.WriteOutputOnState(value)
        Return Me.QueryOutputOnState()
    End Function

    ''' <summary> Gets or sets the output on state query command. </summary>
    ''' <value> The output on state query command. </value>
    Protected Overridable Property OutputOnStateQueryCommand As String

    ''' <summary>
    ''' Queries the output on/off state. Also sets the <see cref="OutputOnState">output on</see>
    ''' sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
    Public Overridable Function QueryOutputOnState() As Boolean?
        Me.OutputOnState = Me.StatusSubsystem.Query(Me.OutputOnState.GetValueOrDefault(True), Me.OutputOnStateQueryCommand)
        Return Me.OutputOnState
    End Function

    ''' <summary> Gets or sets the output on state command format. </summary>
    ''' <value> The output on state command format. </value>
    Protected Overridable Property OutputOnStateCommandFormat As String

    ''' <summary> Writes the output on/off state. Does not read back from the instrument. </summary>
    ''' <param name="value"> if set to <c>True</c> [is on]. </param>
    ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
    Public Overridable Function WriteOutputOnState(ByVal value As Boolean) As Boolean?
        Me.Session.WriteLine(Me.OutputOnStateQueryCommand, CType(value, Integer))
        Me.OutputOnState = value
        Return Me.OutputOnState
    End Function

#End Region

End Class
