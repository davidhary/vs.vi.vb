'---------------------------------------------------------------------------------------------------
' file:		.\Assist\TestInfo_Config.vb
'
' summary:	Test information configuration class
'---------------------------------------------------------------------------------------------------

''' <summary> Gets or sets the filename of the trace event project identities file. </summary>
Partial Friend NotInheritable Class TestAssist

#Region " TRACE EVENT IDENTITY FILE "

    ''' <summary> Gets the filename of the trace event project identities file. </summary>
    ''' <value> The filename of the trace event project identities file. </value>
    Public Shared ReadOnly Property TraceEventProjectIdentitiesFileName As String
        Get
            Return isr.Core.AppSettingsReader.Get().AppSettingValue
        End Get
    End Property

#End Region

End Class
