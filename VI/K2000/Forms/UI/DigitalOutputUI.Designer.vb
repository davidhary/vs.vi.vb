<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DigitalOutputUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._DigitalOutputView = New isr.VI.K2002.Forms.DigitalOutputView()
        Me.SuspendLayout()
        '
        '_DigitalOutputView
        '
        Me._DigitalOutputView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._DigitalOutputView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._DigitalOutputView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._DigitalOutputView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DigitalOutputView.Location = New System.Drawing.Point(1, 1)
        Me._DigitalOutputView.Name = "_DigitalOutputView"
        Me._DigitalOutputView.Padding = New System.Windows.Forms.Padding(1)
        Me._DigitalOutputView.Size = New System.Drawing.Size(375, 351)
        Me._DigitalOutputView.TabIndex = 24
        '
        'DigitalOutputUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._DigitalOutputView)
        Me.Name = "DigitalOutputUI"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(377, 353)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _DigitalOutputView As isr.VI.K2002.Forms.DigitalOutputView
End Class
