'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\StatusSubsystemBase.vb
'
' summary:	Status subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EscapeSequencesExtensions
Imports isr.Core.StackTraceExtensions

''' <summary> Defines the contract that must be implemented by Status Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Partial Public MustInherit Class StatusSubsystemBase
    Inherits SubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Protected Sub New(ByVal session As VI.Pith.SessionBase)
        Me.New(session, VI.Pith.Scpi.Syntax.NoErrorCompoundMessage)
        Me._PresetRefractoryPeriod = TimeSpan.FromMilliseconds(100)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="session">                A reference to a <see cref="Session">message based
    '''                                           session</see>. </param>
    ''' <param name="noErrorCompoundMessage"> A message describing the no error. </param>
    Protected Sub New(ByVal session As VI.Pith.SessionBase, ByVal noErrorCompoundMessage As String)
        MyBase.New(Pith.SessionBase.Validated(session))
        session.ResourcesFilter = VI.SessionFactory.Get.Factory.ResourcesProvider.ResourceFinder.BuildMinimalResourcesFilter
        Me.ApplySessionThis(session)
        Me._ExpectedLanguage = String.Empty
        Me._DeviceErrorBuilder = New System.Text.StringBuilder
        Me._NoErrorCompoundMessage = noErrorCompoundMessage
        Me._DeviceErrorQueue = New DeviceErrorQueue(noErrorCompoundMessage)
        Me._InitializeTimeout = TimeSpan.FromMilliseconds(5000)
        Me._InitRefractoryPeriod = TimeSpan.FromMilliseconds(100)
        ' Me.StandardServiceEnableCommandFormat = Vi.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat
        ' Me.StandardServiceEnableCompleteCommandFormat = Vi.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat
        Me._OperationEventMap = New Dictionary(Of Integer, String)
    End Sub

    ''' <summary> Validated the given status subsystem base. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="statusSubsystemBase"> The status subsystem base. </param>
    ''' <returns> A StatusSubsystemBase. </returns>
    Public Shared Function Validated(ByVal statusSubsystemBase As VI.StatusSubsystemBase) As StatusSubsystemBase
        If statusSubsystemBase Is Nothing Then Throw New ArgumentNullException(NameOf(statusSubsystemBase))
        Return statusSubsystemBase
    End Function

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Executes the device open actions. </summary>
    ''' <remarks>
    ''' This was added in order to defer reading error status after clearing the device state when
    ''' opening the device.
    ''' </remarks>
    Public Overridable Sub OnDeviceOpen()

        ' clear the device active state
        Me.Session.ClearActiveState()

        ' reset device
        Me.Session.ResetKnownState()

        ' Clear the device Status and set more defaults
        Me.Session.ClearExecutionState()

    End Sub

    ''' <summary> The initialize refractory period. </summary>
    Private _InitRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the initialize refractory period. </summary>
    ''' <value> The initialize refractory period. </value>
    Public Property InitRefractoryPeriod As TimeSpan
        Get
            Return Me._InitRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.InitRefractoryPeriod) Then
                Me._InitRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Enables and sets the default bitmasks for wait complete. </summary>
    ''' <param name="standardEventEnableBitmask">  The standard event enable bitmask. </param>
    ''' <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
    Public Overridable Sub EnableServiceRequestWaitComplete(ByVal standardEventEnableBitmask As VI.Pith.StandardEvents, ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.Session.EnableServiceRequestWaitComplete(standardEventEnableBitmask, serviceRequestEnableBitmask)
    End Sub

    ''' <summary> Enables the service requests. </summary>
    ''' <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
    Public Overridable Sub EnableServiceRequestEvents(ByVal serviceRequestEnableBitmask As VI.Pith.ServiceRequests)
        Me.Session.ApplyServiceRequestEnableBitmask(serviceRequestEnableBitmask)
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        If Me.Session.IsSessionOpen Then
            isr.Core.ApplianceBase.DoEvents()
            ' !@# testing using OPC in place of the refractory periods.
            If Me.Session.SupportsOperationComplete Then
                Me.Session.QueryOperationCompleted()
                isr.Core.ApplianceBase.DoEventsWait(Me.Session.StatusReadDelay)
            Else
                isr.Core.ApplianceBase.DoEventsWait(Me.InitRefractoryPeriod)
            End If
        End If
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Session.ResourceNameCaption} clearing error queue" : Me.PublishVerbose($"{activity};. ")
            Me.ClearErrorQueue()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
        Try
            activity = $"{Me.Session.ResourceNameCaption} clearing error cache" : Me.PublishVerbose($"{activity};. ")
            Me.ClearErrorCache()
            activity = $"{Me.Session.ResourceNameCaption} clearing error report" : Me.PublishVerbose($"{activity};. ")
            Me.ClearErrorReport()
            activity = $"{Me.Session.ResourceNameCaption} clearing orphan messages" : Me.PublishVerbose($"{activity};. ")
            Me.ClearOrphanMessages()
            Me.LastOrphanMessage = String.Empty
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
        Try
            activity = $"{Me.Session.ResourceNameCaption} defining wait complete bit masks" : Me.PublishVerbose($"{activity};. ")
            Me.EnableServiceRequestWaitComplete(Me.Session.DefaultStandardEventEnableBitmask, Me.Session.DefaultOperationCompleteBitmask)
        Catch ex As Exception
            ex.Data.Add($"data{ex.Data.Count}.resource", Me.Session.ResourceNameCaption)
            Me.PublishException(activity, ex)
        End Try
        Try
            activity = $"{Me.Session.ResourceNameCaption} querying identity" : Me.PublishVerbose($"{activity};. ")
            Me.QueryIdentity()
        Catch ex As Exception
            ex.Data.Add($"data{ex.Data.Count}.resource", Me.Session.ResourceNameCaption)
            Me.PublishException(activity, ex)
        End Try
        Try
            activity = $"{Me.Session.ResourceNameCaption} defining register event bit values" : Me.PublishVerbose($"{activity};. ")
            Me.DefineEventBitmasks()
            activity = $"{Me.Session.ResourceNameCaption} enable full service request" : Me.PublishVerbose($"{activity};. ")
            Me.EnableServiceRequestEvents(Me.Session.DefaultServiceRequestEnableBitmask)
        Catch ex As Exception
            ex.Data.Add($"data{ex.Data.Count}.resource", Me.Session.ResourceNameCaption)
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> The initialize timeout. </summary>
    Private _InitializeTimeout As TimeSpan

    ''' <summary> Gets or sets the time out for doing a reset and clear on the instrument. </summary>
    ''' <value> The connect timeout. </value>
    Public Property InitializeTimeout() As TimeSpan
        Get
            Return Me._InitializeTimeout
        End Get
        Set(ByVal value As TimeSpan)
            If Not value.Equals(Me.InitializeTimeout) Then
                Me._InitializeTimeout = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> Clears the queues and sets all registers to zero. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Identity = String.Empty
        Me.ResetRegistersKnownState()
        Me.QueryLineFrequency()
        Me.Session.ReadStatusRegister()
    End Sub

#Region " PRESET "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <remarks>
    ''' SCPI: ":STAT:PRES".
    ''' <see cref="VI.Pith.Scpi.Syntax.StatusPresetCommand"> </see>
    ''' </remarks>
    ''' <value> The preset command. </value>
    Protected Overridable Property PresetCommand As String = VI.Pith.Scpi.Syntax.StatusPresetCommand

    ''' <summary> Returns the instrument registers to there preset power on state. </summary>
    ''' <remarks>
    ''' SCPI: "*???".<para>
    ''' <see cref="VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand"> </see> </para><para>
    ''' When this command is sent, the SCPI event registers are affected as follows:<p>
    '''   1. All bits of the positive transition filter registers are set to one (1).</p><p>
    '''   2. All bits of the negative transition filter registers are cleared to zero (0).</p><p>
    '''   3. All bits of the following registers are cleared to zero (0):</p><p>
    '''      a. Operation Event Enable Register.</p><p>
    '''      b. Questionable Event Enable Register.</p><p>
    '''   4. All bits of the following registers are set to one (1):</p><p>
    '''      a. Trigger Event Enable Register.</p><p>
    '''      b. Arm Event Enable Register.</p><p>
    '''      c. Sequence Event Enable Register.</p><p>
    ''' Note: Registers not included in the above list are not affected by this command.</p> </para>
    ''' </remarks>
    Public Overrides Sub PresetKnownState()
        If Not String.IsNullOrWhiteSpace(Me.PresetCommand) Then
            Me.Session.OperationCompleted = New Boolean?
            Me.Execute(Me.PresetCommand)
            If Me.Session.IsSessionOpen Then
                isr.Core.ApplianceBase.DoEventsWait(Me.PresetRefractoryPeriod)
            End If
            Me.Session.QueryOperationCompleted()
            Me.Session.ReadStatusRegister()
            Me.PresetRegistersKnownState()
        End If
    End Sub

    ''' <summary> The preset refractory period. </summary>
    Private _PresetRefractoryPeriod As TimeSpan

    ''' <summary> Gets or sets the post-preset refractory period. </summary>
    ''' <value> The post-preset refractory period. </value>
    Public Property PresetRefractoryPeriod As TimeSpan
        Get
            Return Me._PresetRefractoryPeriod
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.PresetRefractoryPeriod) Then
                Me._PresetRefractoryPeriod = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#End Region

#Region " SESSION "

    ''' <summary> Applies the session described by visaSession. </summary>
    ''' <param name="session"> A reference to a <see cref="Pith.SessionBase">message based
    '''                        session</see>. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub ApplySessionThis(ByVal session As VI.Pith.SessionBase)
        session.ReadTerminationCharacterEnabled = True
        session.ReadTerminationCharacter = isr.Core.EscapeSequencesExtensions.Methods.NewLineValue
        AddHandler session.DeviceErrorOccurred, AddressOf Me.HandleDeviceErrorOccurred
    End Sub

    ''' <summary> Handles the device error occurred. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub HandleDeviceErrorOccurred(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sessionBase As Pith.SessionBase = TryCast(sender, Pith.SessionBase)
        Dim activity As String = "checking if already reading error"
        Try
            If sessionBase IsNot Nothing AndAlso Me.ErrorAvailable AndAlso Not Me.MessageAvailable Then
                activity = "reading device errors"
                Me.QueryExistingDeviceErrors()
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handles the session property changed action. </summary>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overrides Sub HandlePropertyChanged(ByVal sender As VI.Pith.SessionBase, ByVal propertyName As String)
        MyBase.HandlePropertyChanged(sender, propertyName)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName

            Case NameOf(VI.Pith.SessionBase.ErrorAvailable)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.ErrorAvailable))
            Case NameOf(VI.Pith.SessionBase.ErrorAvailableBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.ErrorAvailableBit))
            Case NameOf(VI.Pith.SessionBase.HasMeasurementEvent)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.HasMeasurementEvent))
            Case NameOf(VI.Pith.SessionBase.MeasurementEventBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.MeasurementEventBit))
            Case NameOf(VI.Pith.SessionBase.MessageAvailableBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.MessageAvailableBit))
            Case NameOf(VI.Pith.SessionBase.MessageAvailable)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.MessageAvailable))
            Case NameOf(VI.Pith.SessionBase.HasOperationEvent)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.HasOperationEvent))
            Case NameOf(VI.Pith.SessionBase.OperationEventBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.OperationEventBit))
            Case NameOf(VI.Pith.SessionBase.HasQuestionableEvent)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.HasQuestionableEvent))
            Case NameOf(VI.Pith.SessionBase.QuestionableEventBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.QuestionableEventBit))
            Case NameOf(VI.Pith.SessionBase.HasStandardEvent)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.HasStandardEvent))
            Case NameOf(VI.Pith.SessionBase.StandardEventBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.StandardEventBit))
            Case NameOf(VI.Pith.SessionBase.HasSystemEvent)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.HasSystemEvent))
            Case NameOf(VI.Pith.SessionBase.SystemEventBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.SystemEventBit))
            Case NameOf(VI.Pith.SessionBase.RequestingService)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.RequestingService))
            Case NameOf(VI.Pith.SessionBase.RequestingServiceBit)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.RequestingServiceBit))

            Case NameOf(VI.Pith.SessionBase.LastMessageReceived)
                Dim value As String = sender.LastMessageReceived
                If Not String.IsNullOrWhiteSpace(value) Then
                    Me.PublishInfo($"{Me.ResourceNameCaption} received: '{value.InsertCommonEscapeSequences}'")
                End If
            Case NameOf(VI.Pith.SessionBase.LastMessageSent)
                Dim value As String = sender.LastMessageSent
                If Not String.IsNullOrWhiteSpace(value) Then
                    Me.PublishInfo($"{Me.ResourceNameCaption} sent: '{value}'")
                End If

            Case NameOf(VI.Pith.SessionBase.OperationCompleted)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.OperationCompleted))

            Case NameOf(VI.Pith.SessionBase.ServiceRequestStatus)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.ServiceRequestStatus))
            Case NameOf(VI.Pith.SessionBase.ServiceRequestEnabledBitmask)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.ServiceRequestEnabledBitmask))
            Case NameOf(VI.Pith.SessionBase.ServiceRequestWaitCompleteEnabledBitmask)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.ServiceRequestWaitCompleteEnabledBitmask))
            Case NameOf(VI.Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask))
            Case NameOf(VI.Pith.SessionBase.StandardEventEnableBitmask)
                Me.NotifyPropertyChanged(NameOf(VI.Pith.SessionBase.StandardEventEnableBitmask))
        End Select
    End Sub

#End Region

#Region " IDENTITY "

    ''' <summary> The identity. </summary>
    Private _Identity As String

    ''' <summary> Gets or sets the device identity string (*IDN?). </summary>
    ''' <value> The identity. </value>
    Public Property Identity As String
        Get
            Return Me._Identity
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then value = String.Empty
            If Not String.Equals(value, Me.Identity) Then
                Me._Identity = value
                Me.NotifyPropertyChanged()
                If Not String.IsNullOrWhiteSpace(value) Then Me.PublishInfo($"{Me.ResourceNameCaption} identified;. as {value}")
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <remarks>
    ''' SCPI: "*IDN?".
    ''' <see cref="VI.Pith.Ieee488.Syntax.IdentityQueryCommand"> </see>
    ''' </remarks>
    ''' <value> The identity query command. </value>
    Protected Overridable Property IdentityQueryCommand As String = VI.Pith.Ieee488.Syntax.IdentityQueryCommand

    ''' <summary> Writes the identity query command. </summary>
    ''' <remarks> This is required for systems which require asynchronous communication. </remarks>
    Public Sub WriteIdentityQueryCommand()
        Me.Write(Me.IdentityQueryCommand)
    End Sub

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the '*IDN?' query. </remarks>
    ''' <returns> System.String. </returns>
    Public MustOverride Function QueryIdentity() As String

    ''' <summary> The version information base. </summary>
    Private _VersionInfoBase As VersionInfoBase

    ''' <summary> Gets or sets information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public Property VersionInfoBase As VersionInfoBase
        Get
            Return Me._VersionInfoBase
        End Get
        Set(value As VersionInfoBase)
            Me._VersionInfoBase = value
            If value IsNot Nothing Then
                Me.SerialNumberReading = value.SerialNumber
            End If
        End Set
    End Property


#Region " SERIAL NUMBER "

    ''' <summary> The serial number reading. </summary>
    Private _SerialNumberReading As String

    ''' <summary> Gets or sets the serial number reading. </summary>
    ''' <value> The serial number reading. </value>
    Public Property SerialNumberReading As String
        Get
            Return Me._SerialNumberReading
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then value = String.Empty
            If Not String.Equals(value, Me.SerialNumberReading) Then
                Me._SerialNumberReading = value
                Me.NotifyPropertyChanged()
                If String.IsNullOrWhiteSpace(value) Then
                    Me.SerialNumber = New Long?
                Else
                    Dim numericValue As Long = 0
                    If Long.TryParse(Me.SerialNumberReading, Globalization.NumberStyles.Number, Globalization.CultureInfo.InvariantCulture, numericValue) Then
                    End If
                    Me.SerialNumber = numericValue
                End If
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the serial number query command. </summary>
    ''' <value> The serial number query command. </value>
    Protected Overridable Property SerialNumberQueryCommand As String

    ''' <summary> Reads and returns the instrument serial number. </summary>
    '''  <exception cref="VI.Pith.NativeException"> Thrown when a Visa error condition occurs. </exception>
    Public Function QuerySerialNumber() As String
        If Not String.IsNullOrWhiteSpace(Me.SerialNumberQueryCommand) Then
            Me.Session.LastAction = Me.PublishInfo("Reading serial number;. ")
            Me.Session.LastNodeNumber = New Integer?
            Dim value As String = Me.Session.QueryTrimEnd(Me.SerialNumberQueryCommand)
            Me.CheckThrowDeviceException(False, "getting serial number;. using {0}.", Me.Session.LastMessageSent)
            Me.SerialNumberReading = value
        ElseIf String.IsNullOrWhiteSpace(Me.SerialNumberReading) AndAlso Me.SerialNumber.HasValue Then
            Me.SerialNumberReading = CStr(Me.SerialNumber.Value)
        End If
        Return Me.SerialNumberReading
    End Function

    ''' <summary> The serial number. </summary>
    Private _SerialNumber As Long?

    ''' <summary> Reads and returns the instrument serial number. </summary>
    ''' <value> The serial number. </value>
    Public Property SerialNumber() As Long?
        Get
            Return Me._SerialNumber
        End Get
        Set(ByVal value As Long?)
            If Not Nullable.Equals(Me.SerialNumber, value) Then
                Me._SerialNumber = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#End Region

#Region " LANGUAGE "

    ''' <summary> The Language. </summary>
    Private _Language As String

    ''' <summary> Gets or sets the device Language string (*IDN?). </summary>
    ''' <value> The Language. </value>
    Public Property Language As String
        Get
            Return Me._Language
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then value = String.Empty
            If Not String.Equals(value, Me.Language) Then
                value = value.Trim
                Me._Language = value
                Me.NotifyPropertyChanged()
            End If
            Me.LanguageValidated = Me.IsExpectedLanguage()
        End Set
    End Property

    ''' <summary> The expected language. </summary>
    Private _ExpectedLanguage As String

    ''' <summary> Gets or sets the device ExpectedLanguage string (*IDN?). </summary>
    ''' <value> The ExpectedLanguage. </value>
    Public Property ExpectedLanguage As String
        Get
            Return Me._ExpectedLanguage
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then value = String.Empty
            If Not String.Equals(value, Me.ExpectedLanguage) Then
                value = value.Trim
                Me._ExpectedLanguage = value
                Me.NotifyPropertyChanged()
            End If
            Me.LanguageValidated = Me.IsExpectedLanguage()
        End Set
    End Property

    ''' <summary>
    ''' Query if the language is the expected language or if the expected language is empty and thus
    ''' invariant.
    ''' </summary>
    ''' <returns> <c>true</c> if expected language; otherwise <c>false</c> </returns>
    Public Function IsExpectedLanguage() As Boolean
        Return Not String.IsNullOrWhiteSpace(Me.Language) AndAlso String.Equals(Me.ExpectedLanguage, Me.Language, StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary> True if language validated. </summary>
    Private _LanguageValidated As Boolean

    ''' <summary> Gets or sets the sentinel indicating if the language was validated. </summary>
    ''' <value> The LanguageValidated. </value>
    Public Property LanguageValidated As Boolean
        Get
            Return Me._LanguageValidated
        End Get
        Set(ByVal value As Boolean)
            If Not Boolean.Equals(value, Me.LanguageValidated) Then
                Me._LanguageValidated = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the language. </summary>
    ''' <param name="value"> The required language. </param>
    ''' <returns> The language. </returns>
    Public Function ApplyLanguage(ByVal value As String) As String
        Me.WriteLanguage(value)
        Return Me.QueryLanguage()
    End Function

    ''' <summary> Attempts to apply expected language from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    Public Function TryApplyExpectedLanguage() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        ' check the language status. 
        Me.QueryLanguage()
        If Me.LanguageValidated Then
            Me.PublishVerbose($"Device language {Me.Language} validated;. ")
        Else
            ' set the device to the correct language.
            Me.PublishInfo($"Setting device to {Me.ExpectedLanguage} language;. ")
            Me.ApplyLanguage(Me.ExpectedLanguage)
            If Not Me.LanguageValidated Then
                result = (False, Me.PublishWarning($"Incorrect {NameOf(StatusSubsystemBase.Language)} settings {Me.Language}; must be {Me.ExpectedLanguage}"))
            End If
        End If
        Return result
    End Function

    ''' <summary> Gets or sets the Language query command. </summary>
    ''' <remarks> SCPI: "*LANG?". </remarks>
    ''' <exception cref="isr.VI.Pith.DeviceException">       Thrown when a Device error condition occurs. </exception>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The Language query command. </value>
    Protected Overridable Property LanguageQueryCommand As String

    ''' <summary> Queries the Language. </summary>
    ''' <remarks> Sends the '*LANG?' query. </remarks>
    ''' <returns> The Language or Empty if not required or unknown. </returns>
    Public Overridable Function QueryLanguage() As String
        Me.Language = If(String.IsNullOrWhiteSpace(Me.LanguageQueryCommand), String.Empty, Me.Session.QueryTrimEnd(Me.LanguageQueryCommand))
        Return Me.Language
    End Function

    ''' <summary> Gets or sets the Language command format. </summary>
    ''' <remarks> *LANG {0}". </remarks>
    ''' <exception cref="isr.VI.Pith.DeviceException">       Thrown when a Device error condition occurs. </exception>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The Language command format. </value>
    Protected Overridable Property LanguageCommandFormat As String

    ''' <summary> Writes the Language value without reading back the value from the device. </summary>
    ''' <remarks> Changing the language causes the instrument to reboot. </remarks>
    ''' <param name="value"> The current Language. </param>
    ''' <returns> The Language or Empty if not required or unknown. </returns>
    Public Function WriteLanguage(ByVal value As String) As String
        Me.Language = If(String.IsNullOrWhiteSpace(Me.LanguageQueryCommand), value, Me.Write(Me.LanguageCommandFormat, value))
        Return Me.Language
    End Function

#End Region

#Region " CHECK AND THROW "

    ''' <summary>
    ''' Checks and throws an exception if device errors occurred. Can only be used after receiving a
    ''' full reply from the device.
    ''' </summary>
    ''' <exception cref="VI.Pith.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowDeviceException(ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        If (Me.Session.ReadStatusRegister And Me.ErrorAvailableBit) <> 0 Then
            If flushReadFirst Then Me.Session.DiscardUnreadData()
            Me.Session.QueryStandardEventStatus()
            Me.QueryDeviceErrors()
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
#Disable Warning CA1825 ' Avoid zero-length array allocations.
            Throw New isr.VI.Pith.DeviceException(Me.ResourceNameCaption, $"{details}. {Me.DeviceErrorReport}.")
#Enable Warning CA1825 ' Avoid zero-length array allocations.
        End If
    End Sub

#End Region

#Region " CHECK AND TRACE "

    ''' <summary>
    ''' Check and reports visa or device error occurred. Can only be used after receiving a full
    ''' reply from the device.
    ''' </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Specifies the report format. </param>
    ''' <param name="args">       Specifies the report arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overridable Function TraceVisaDeviceOperationOkay(ByVal nodeNumber As Integer,
                                                             ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Checks and reports if a visa or device error occurred. Can only be used after receiving a
    ''' full reply from the device.
    ''' </summary>
    ''' <param name="nodeNumber">     Specifies the remote node number to validate. </param>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function TraceVisaDeviceOperationOkay(ByVal nodeNumber As Integer,
                                                 ByVal flushReadFirst As Boolean, ByVal format As String,
                                                 ByVal ParamArray args() As Object) As Boolean
        Dim success As Boolean
        Me.TraceVisaOperation(nodeNumber, format, args)
        Me.Session.ReadStatusRegister()
        success = Not Me.ErrorAvailable
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        If success Then
            Me.PublishInfo($"{Me.ResourceNameCaption} node {nodeNumber} done {details}")
        Else
            If flushReadFirst Then Me.Session.DiscardUnreadData()
            Me.Session.QueryStandardEventStatus()
            Me.QueryDeviceErrors()
            Dim errors As String = Me.DeviceErrorReport
            If Not String.IsNullOrWhiteSpace(errors) Then
                Me.PublishWarning($"{Me.ResourceNameCaption} node {nodeNumber} device errors: {errors};. Details: 
{details}
{New StackFrame(True).UserCallStack}")
            End If
        End If
        Return success
    End Function

    ''' <summary>
    ''' Checks and reports if a visa or device error occurred. Can only be used after receiving a
    ''' full reply from the device.
    ''' </summary>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function TraceVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal format As String,
                                                 ByVal ParamArray args() As Object) As Boolean
        Me.TraceVisaOperation(format, args)
        Me.Session.ReadStatusRegister()
        Dim success As Boolean = Not Me.ErrorAvailable
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        If success Then
            Me.PublishInfo($"{Me.ResourceNameCaption} done {details}")
        Else
            If flushReadFirst Then Me.Session.DiscardUnreadData()
            Me.Session.QueryStandardEventStatus()
            Me.QueryDeviceErrors()
            Dim errors As String = Me.DeviceErrorReport
            If Not String.IsNullOrWhiteSpace(errors) Then
                Me.PublishWarning($"{Me.ResourceNameCaption} device errors: {errors};. Details: 
{details}
{New StackFrame(True).UserCallStack}")
            End If
        End If
        Return success
    End Function

    ''' <summary> Reports operation action. Can be used with queries. </summary>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal format As String, ByVal ParamArray args() As Object)
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me.PublishInfo($"{Me.ResourceNameCaption} done {details}")
    End Sub

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="ex">     The exception. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal ex As VI.Pith.NativeException, ByVal format As String, ByVal ParamArray args() As Object)
        If ex Is Nothing Then Throw New ArgumentNullException(NameOf(ex))
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                          $"{Me.ResourceNameCaption} VISA error: {ex.InnerError.BuildErrorCodeDetails()};. Details: 
{details}
{New StackFrame(True).UserCallStack}")
    End Sub

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="ex">     The exception. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub TraceOperation(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        If ex Is Nothing Then Throw New ArgumentNullException(NameOf(ex))
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                          $"{Me.ResourceNameCaption} exception: {ex.Message};. Details: 
{details}
{New StackFrame(True).UserCallStack}")
    End Sub

    ''' <summary> Trace visa node operation. Can be used with queries. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me.PublishInfo($"{Me.ResourceNameCaption} node {nodeNumber} done {details}")
    End Sub

    ''' <summary> Trace visa failed operation. Can be used with queries. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="ex">         The exception. </param>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub TraceVisaOperation(ByVal ex As VI.Pith.NativeException, ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        If ex Is Nothing Then Throw New ArgumentNullException(NameOf(ex))
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me.PublishWarning(
                          $"{Me.ResourceNameCaption} node {nodeNumber} VISA error: {ex.InnerError.BuildErrorCodeDetails()};. Details: 
{details}
{New StackFrame(True).UserCallStack}")
    End Sub

    ''' <summary> Trace failed operation. Can be used with queries. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="ex">         The exception. </param>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Describes the format to use. </param>
    ''' <param name="args">       A variable-length parameters list containing arguments. </param>
    Public Sub TraceOperation(ByVal ex As Exception, ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        If ex Is Nothing Then Throw New ArgumentNullException(NameOf(ex))
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                          $"{Me.ResourceNameCaption} node {nodeNumber} exception: {ex.Message};. Details: 
{details}
{New StackFrame(True).UserCallStack}")
    End Sub

#End Region

#Region " COLLECT GARBAGE "

    ''' <summary> Gets or sets the collect garbage wait complete command. </summary>
    ''' <value> The collect garbage wait complete command. </value>
    Protected Overridable Property CollectGarbageWaitCompleteCommand As String = String.Empty

    ''' <summary> Collect garbage wait complete. </summary>
    ''' <param name="timeout"> Specifies how long to wait for the service request before throwing
    '''                        the timeout exception. Set to zero for an infinite (120 seconds)
    '''                        timeout. </param>
    ''' <param name="format">  Specifies the report format. </param>
    ''' <param name="args">    Specifies the report arguments. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function CollectGarbageWaitComplete(ByVal timeout As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return String.IsNullOrWhiteSpace(Me.CollectGarbageWaitCompleteCommand) OrElse Me.CollectGarbageWaitCompleteThis(timeout, format, args)
    End Function

    ''' <summary> Does garbage collection. Reports operations synopsis. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    '''                        completed. </param>
    ''' <param name="format">  Describes the format to use. </param>
    ''' <param name="args">    A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function CollectGarbageWaitCompleteThis(ByVal timeout As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object) As Boolean

        Dim affirmative As Boolean
        Try
            Me.Session.LastAction = Me.PublishInfo(
                                                       "Collecting garbage after {0};. ",
                                                       String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            Me.Session.LastNodeNumber = New Integer?
            ' do a garbage collection
            Me.Session.EnableServiceRequestWaitComplete()
            Me.Write(Me.CollectGarbageWaitCompleteCommand)
            affirmative = Me.TraceVisaDeviceOperationOkay(True, "collecting garbage after {0};. ",
                                                          String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        Catch ex As VI.Pith.NativeException
            Me.TraceVisaOperation(ex, "collecting garbage after {0};. ",
                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            affirmative = False
        Catch ex As Exception
            Me.TraceOperation(ex, "collecting garbage after {0};. ",
                              String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            affirmative = False
        End Try
        If affirmative Then
            Try
                Me.Session.ApplyServiceRequest(Me.Session.AwaitOperationCompleted(timeout).Status)
                affirmative = Me.TraceVisaDeviceOperationOkay(True, "awaiting completion after collecting garbage after {0};. ",
                                                              String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            Catch ex As VI.Pith.NativeException
                Me.TraceVisaOperation(ex, "awaiting completion after collecting garbage after {0};. ",
                                      String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
                affirmative = False
            Catch ex As Exception
                Me.TraceOperation(ex, "awaiting completion after collecting garbage after {0};. ",
                                  String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
                affirmative = False
            End Try
        End If
        Return affirmative

    End Function

#End Region

#Region " LINE FREQUENCY "

    ''' <summary> Gets or sets the station line frequency. </summary>
    ''' <remarks> This value is shared and used for all systems on this station. </remarks>
    ''' <value> The station line frequency. </value>
    Public Shared Property StationLineFrequency As Double? = New Double?

    ''' <summary> The line frequency. </summary>
    Private _LineFrequency As Double?

    ''' <summary> Gets or sets the line frequency. </summary>
    ''' <value> The line frequency. </value>
    Public Property LineFrequency() As Double?
        Get
            Return Me._LineFrequency
        End Get
        Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.LineFrequency, value) Then
                StatusSubsystemBase.StationLineFrequency = value
                Me._LineFrequency = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the default line frequency. </summary>
    ''' <value> The default line frequency. </value>
    Public Shared Property DefaultLineFrequency As Double = 60

    ''' <summary> Gets or sets line frequency query command. </summary>
    ''' <value> The line frequency query command. </value>
    Protected Overridable Property LineFrequencyQueryCommand As String = VI.Pith.Scpi.Syntax.ReadLineFrequencyCommand

    ''' <summary> Reads the line frequency. </summary>
    ''' <remarks> Sends the <see cref="LineFrequencyQueryCommand"/> query. </remarks>
    ''' <returns> System.Nullable{System.Double}. </returns>
    Public Function QueryLineFrequency() As Double?
        If Not Me.LineFrequency.HasValue Then
            Me.LineFrequency = If(String.IsNullOrWhiteSpace(Me.LineFrequencyQueryCommand),
                StatusSubsystemBase.DefaultLineFrequency,
                Me.Session.Query(Me.LineFrequency.GetValueOrDefault(StatusSubsystemBase.DefaultLineFrequency), Me.LineFrequencyQueryCommand))
        End If
        Return Me.LineFrequency
    End Function

    ''' <summary> Converts power line cycles to time span. </summary>
    ''' <param name="powerLineCycles"> The power line cycles. </param>
    ''' <returns>
    ''' The integration period corresponding to the specified number of power line .
    ''' </returns>
    Public Shared Function FromPowerLineCycles(ByVal powerLineCycles As Double) As TimeSpan
        Return StatusSubsystemBase.FromPowerLineCycles(powerLineCycles, StatusSubsystemBase.StationLineFrequency.GetValueOrDefault(StatusSubsystemBase.DefaultLineFrequency))
    End Function

    ''' <summary> Converts integration period to Power line cycles. </summary>
    ''' <param name="integrationPeriod"> The integration period. </param>
    ''' <returns> The number of power line cycles corresponding to the integration period. </returns>
    Public Shared Function ToPowerLineCycles(ByVal integrationPeriod As TimeSpan) As Double
        Return StatusSubsystemBase.ToPowerLineCycles(integrationPeriod, StatusSubsystemBase.StationLineFrequency.GetValueOrDefault(StatusSubsystemBase.DefaultLineFrequency))
    End Function

    ''' <summary> Converts power line cycles to time span. </summary>
    ''' <param name="powerLineCycles"> The power line cycles. </param>
    ''' <param name="frequency">       The frequency. </param>
    ''' <returns>
    ''' The integration period corresponding to the specified number of power line .
    ''' </returns>
    Public Shared Function FromPowerLineCycles(ByVal powerLineCycles As Double, ByVal frequency As Double) As TimeSpan
        Return StatusSubsystemBase.FromSecondsPrecise(powerLineCycles / frequency)
    End Function

    ''' <summary> Converts integration period to Power line cycles. </summary>
    ''' <param name="integrationPeriod"> The integration period. </param>
    ''' <param name="frequency">         The frequency. </param>
    ''' <returns> The number of power line cycles corresponding to the integration period. </returns>
    Public Shared Function ToPowerLineCycles(ByVal integrationPeriod As TimeSpan, ByVal frequency As Double) As Double
        Return StatusSubsystemBase.TotalSecondsPrecise(integrationPeriod) * frequency
    End Function

    ''' <summary> Total seconds precise. </summary>
    ''' <param name="timespan"> The timespan. </param>
    ''' <returns> The total number of seconds precise. </returns>
    Public Shared Function TotalSecondsPrecise(ByVal timespan As TimeSpan) As Double
        Return timespan.Ticks / TimeSpan.TicksPerSecond
    End Function

    ''' <summary> Converts seconds to time span with tick timespan accuracy. </summary>
    ''' <param name="seconds"> The seconds. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function FromSecondsPrecise(ByVal seconds As Double) As TimeSpan
        Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * seconds))
    End Function

#End Region

End Class


