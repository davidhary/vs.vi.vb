''' <summary> The Meter Cold Resistance. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-23 </para>
''' </remarks>
Public Class MeterColdResistance
    Inherits MeterSubsystemBase

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    ''' <param name="resistance">      The cold resistance. </param>
    ''' <param name="meterEntity">     The meter entity type. </param>
    Public Sub New(ByVal statusSubsystem As StatusSubsystemBase, ByVal resistance As ColdResistance, ByVal meterEntity As ThermalTransientMeterEntity)
        MyBase.New(statusSubsystem)
        Me.MeterEntity = meterEntity
        Me.ColdResistance = resistance
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Defines the parameters for the Clears known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineClearExecutionState()
        Me.PublishVerbose("Clearing {0} resistance execution state;. ", Me.EntityName)
        MyBase.DefineClearExecutionState()
        Me.ColdResistance.DefineClearExecutionState()
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()

        ' Me.Session.WriteLine("{0}.level = nil", Me.EntityName)
        ' Dim b As Boolean = Me.Session.IsStatementTrue("{0}.level==nil", Me.EntityName)
        ' 'Me.Session.WriteLine("{0}.level = 0.09", Me.EntityName)
        ' 'B = Me.Session.IsStatementTrue("{0}.level==nil", Me.EntityName)
        ' Dim val As String = Me.Session.QueryPrintStringFormat("%9.6f", "_G.ttm.coldResistance.Defaults.level")

        Me.PublishVerbose("Resetting {0} resistance known state;. ", Me.EntityName)
        isr.Core.ApplianceBase.DoEvents()
        MyBase.DefineKnownResetState()

        isr.Core.ApplianceBase.DoEvents()
        Me.ColdResistance.ResetKnownState()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

#End Region

#Region " DEVICE UNDER TEST: COLD RESISTANCE "

    ''' <summary> Gets the <see cref="ColdResistance">cold resistance</see>. </summary>
    ''' <value> The cold resistance. </value>
    Public Property ColdResistance As ColdResistance

    ''' <summary> Gets the <see cref="ResistanceMeasureBase">part resistance element</see>. </summary>
    ''' <value> The cold resistance. </value>
    Public Overrides ReadOnly Property Resistance As ResistanceMeasureBase
        Get
            Return Me.ColdResistance
        End Get
    End Property

#End Region

#Region " CONFIGURE "

    ''' <summary> Reads instrument defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub ReadInstrumentDefaults()
        MyBase.ReadInstrumentDefaults()
        If Not Me.Session.TryQueryPrint(7.4D, My.MySettings.Default.ColdResistanceApertureDefault, "{0}.aperture", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance aperture;. Sent:'{0}; Received:'{1}'.",
                                       Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceCurrentLevelDefault, "{0}.level", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance current level;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceCurrentMinimum, "{0}.minCurrent", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance minimum current;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceCurrentMaximum, "{0}.maxCurrent", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance maximum current;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceVoltageLimitDefault, "{0}.limit", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance voltage limit;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceVoltageMinimum, "{0}.minVoltage", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance minimum voltage;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceVoltageMaximum, "{0}.maxVoltage", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance maximum voltage;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceLowLimitDefault, "{0}.lowLimit", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance low limit;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceHighLimitDefault, "{0}.highLimit", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance high limit;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceMinimum, "{0}.minResistance", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance minimum resistance;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ColdResistanceMaximum, "{0}.maxResistance", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default cold resistance maximum resistance;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Configures the meter for making the cold resistance measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The cold resistance. </param>
    Public Overrides Sub Configure(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        MyBase.Configure(resistance)
        Me.CheckThrowDeviceException(False, "configuring {0} measurement;. ", Me.EntityName)
        Me.ColdResistance.CheckThrowUnequalConfiguration(resistance)
    End Sub

    ''' <summary> Applies changed meter configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The cold resistance. </param>
    Public Overrides Sub ConfigureChanged(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        MyBase.ConfigureChanged(resistance)
        Me.StatusSubsystem.CheckThrowDeviceException(False, "configuring {0} measurement;. ", Me.EntityName)
        Me.ColdResistance.CheckThrowUnequalConfiguration(resistance)
    End Sub

    ''' <summary> Queries the configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub QueryConfiguration()
        MyBase.QueryConfiguration()
        Me.StatusSubsystem.CheckThrowDeviceException(False, "Reading {0} configuration;. ", Me.EntityName)
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Measures the Final resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The resistance. </param>
    Public Overloads Sub Measure(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        Me.Session.MakeEmulatedReply(resistance.GenerateRandomReading)
        MyBase.Measure(resistance)
        Me.ReadResistance(resistance)
        ' Me.EmulateResistance(resistance)
    End Sub

#End Region

#Region " READ "

    ''' <summary>
    ''' Reads the resistance. Sets the last <see cref="LastReading">reading</see>,
    ''' <see cref="LastOutcome">outcome</see> <see cref="LastMeasurementStatus">status</see> and
    ''' <see cref="MeasurementAvailable">Measurement available sentinel</see>.
    ''' The outcome is left empty if measurements were not made.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub ReadResistance()

        If Me.QueryOutcomeNil() Then
            Me.LastReading = String.Empty
            Me.LastOutcome = String.Empty
            Me.LastMeasurementStatus = String.Empty
            Throw New InvalidOperationException("Measurement not made.")
        Else
            If Me.QueryOkay() Then
                Me.LastReading = Me.Session.QueryPrintStringFormatTrimEnd(8.5D, "{0}.resistance", Me.EntityName)
                Me.StatusSubsystem.CheckThrowDeviceException(False, "reading resistance;. Sent: '{0}'; Received: '{1}'.",
                                                             Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
                Me.LastOutcome = "0"
                Me.LastMeasurementStatus = String.Empty
            Else
                ' if outcome failed, read and parse the outcome and status.
                Me.LastOutcome = Me.Session.QueryPrintStringFormatTrimEnd(1, "{0}.outcome", Me.EntityName)
                Me.StatusSubsystem.CheckThrowDeviceException(False, "reading outcome;. Sent: '{0}'; Received: '{1}'.",
                                                             Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
                Me.LastMeasurementStatus = Me.Session.QueryPrintStringFormatTrimEnd(1, "{0}.status", Me.EntityName)
                Me.StatusSubsystem.CheckThrowDeviceException(False, "reading status;. Sent: '{0}'; Received: '{1}'.",
                                                             Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
                Me.LastReading = String.Empty
            End If
        End If
        Me.ColdResistance.LastOutcome = Me.LastOutcome

        Me.MeasurementAvailable = True
    End Sub

    ''' <summary> Reads cold resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The cold resistance. </param>
    Public Sub ReadResistance(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        Me.ReadResistance()
        Me.StatusSubsystem.CheckThrowDeviceException(False, "Reading Resistance;. last command: '{0}'", Me.Session.LastMessageSent)
        Dim measurementOutcome As MeasurementOutcomes = MeasurementOutcomes.None
        If Me.LastOutcome <> "0" Then
            Dim details As String = String.Empty
            measurementOutcome = Me.ParseOutcome(details)
            Me.PublishWarning("Measurement failed;. Details: {0}", details)
        End If
        Me.ColdResistance.ParseReading(Me.LastReading, measurementOutcome)
        resistance.ParseReading(Me.LastReading, measurementOutcome)
    End Sub

    ''' <summary> Emulates cold resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The cold resistance. </param>
    Public Sub EmulateResistance(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        Me.ColdResistance.EmulateReading()
        Me.LastReading = Me.ColdResistance.LastReading
        Me.LastOutcome = Me.ColdResistance.LastOutcome
        Me.LastMeasurementStatus = String.Empty
        Dim measurementOutcome As MeasurementOutcomes = MeasurementOutcomes.None
        Me.ColdResistance.ParseReading(Me.LastReading, measurementOutcome)
        resistance.ParseReading(Me.LastReading, measurementOutcome)
    End Sub

#End Region

End Class
