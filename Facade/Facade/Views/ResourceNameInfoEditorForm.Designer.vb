<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResourceNameInfoEditorForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ResourceNameInfoEditor = New isr.VI.Facade.ResourceNameInfoEditor()
        Me.SuspendLayout()
        '
        'ResourceNameInfoEditor
        '
        Me.ResourceNameInfoEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ResourceNameInfoEditor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ResourceNameInfoEditor.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResourceNameInfoEditor.Location = New System.Drawing.Point(0, 0)
        Me.ResourceNameInfoEditor.Name = "ResourceNameInfoEditor"
        Me.ResourceNameInfoEditor.Size = New System.Drawing.Size(423, 423)
        Me.ResourceNameInfoEditor.TabIndex = 0
        '
        'ResourceNameInfoEditorForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(423, 423)
        Me.Controls.Add(Me.ResourceNameInfoEditor)
        Me.Name = "ResourceNameInfoEditorForm"
        Me.Opacity = 0.999R
        Me.TargetOpacity = 1.0R
        Me.Text = "ResourceNameInfoEditorForm"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents ResourceNameInfoEditor As ResourceNameInfoEditor
End Class
