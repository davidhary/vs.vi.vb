''' <summary> K2002 Visa View unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2002")>
Public Class VisaViewTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(K2002FormsTests.ResourceSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SELECTOR OPENER: SELECT, OPNE, CLOSE "

    ''' <summary> (Unit Test Method) tests selector opener. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SelectorOpenerTest()
        isr.VI.FacadeTests.DeviceManager.AssertResourceOpenClose(TestInfo, ResourceSettings.Get)
    End Sub

#End Region

#Region " VISA VIEW: DEVICE OPEN TEST "

    ''' <summary> (Unit Test Method) tests selected resource name visa view. </summary>
    ''' <remarks>
    ''' Visa View Timing:<para>
    ''' Ping elapsed 0.123   </para><para>
    ''' Create device 0.097   </para><para>
    ''' Check selected resource name 2.298   </para><para>
    '''  </para>
    ''' </remarks>
    <TestMethod()>
    Public Sub SelectedResourceNameVisaViewTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        TestInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            TestInfo.TraceMessage($"Create device {sw.Elapsed:s\.fff}")
            sw.Restart()
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.CheckSelectedResourceName(TestInfo, view, ResourceSettings.Get)
                TestInfo.TraceMessage($"Check selected resource name {sw.Elapsed:s\.fff}")
                sw.Restart()
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests selected resource name visa tree view. </summary>
    ''' <remarks>
    ''' Visa Tree View Timing:<para>
    ''' Ping elapsed 0.119   </para><para>
    ''' Create device 0.125   </para><para>
    ''' Check selected resource name 2.293   </para><para>
    '''  </para>
    ''' </remarks>
    <TestMethod()>
    Public Sub SelectedResourceNameVisaTreeViewTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        TestInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            TestInfo.TraceMessage($"Create device {sw.Elapsed:s\.fff}")
            sw.Restart()
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.CheckSelectedResourceName(TestInfo, view, ResourceSettings.Get)
                TestInfo.TraceMessage($"Check selected resource name {sw.Elapsed:s\.fff}")
                sw.Restart()
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests talker trace message visa view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TalkerTraceMessageVisaViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            isr.VI.FacadeTests.DeviceManager.AssertViewAddsTraceMessages(TestInfo, device)
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests talker trace message visa tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TalkerTraceMessageVisaTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertTalkerPublishWarning(view, TestInfo.TraceMessagesQueueListener)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session visa view. </summary>
    ''' <remarks>
    ''' <para>
    ''' Ping elapsed 0.126   </para><para>
    ''' Session open 5.423   </para><para>
    ''' Session checked 0.001   </para><para>
    ''' Session closed 0.024   </para><para>
    '''    </para>
    ''' </remarks>
    <TestMethod()>
    Public Sub OpenSessionVisaViewTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        TestInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            sw.Restart()
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                Try
                    isr.VI.FacadeTests.DeviceManager.OpenSession(TestInfo, 0, view, ResourceSettings.Get)
                    TestInfo.TraceMessage($"Session open {sw.Elapsed:s\.fff}")
                    sw.Restart()
                    isr.VI.FacadeTests.DeviceManager.CheckSession(view.VisaSessionBase.Session, ResourceSettings.Get)
                    TestInfo.TraceMessage($"Session checked {sw.Elapsed:s\.fff}")
                    sw.Restart()
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.CloseSession(TestInfo, 0, view)
                    TestInfo.TraceMessage($"Session closed {sw.Elapsed:s\.fff}")
                    sw.Restart()
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session visa tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionVisaTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                Try
                    isr.VI.FacadeTests.DeviceManager.OpenSession(TestInfo, 0, view, ResourceSettings.Get)
                    isr.VI.FacadeTests.DeviceManager.CheckSession(view.VisaSessionBase.Session, ResourceSettings.Get)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.CloseSession(TestInfo, 0, view)
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session twice visa view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTwiceVisaViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.OpenCloseSession(TestInfo, 1, view, ResourceSettings.Get)
            End Using
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.OpenCloseSession(TestInfo, 2, view, ResourceSettings.Get)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session twice visa tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTwiceVisaTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.OpenCloseSession(TestInfo, 1, view, ResourceSettings.Get)
            End Using
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.OpenCloseSession(TestInfo, 2, view, ResourceSettings.Get)
            End Using
        End Using
    End Sub

#End Region

#Region " VISA VIEW: ASSIGNED DEVICE TESTS "

    ''' <summary> (Unit Test Method) tests assign device tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignDeviceTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using view As VI.K2002.Forms.TreeView = New VI.K2002.Forms.TreeView
            Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                view.AssignDevice(device)
                isr.VI.FacadeTests.DeviceManager.OpenCloseSession(TestInfo, 1, view, ResourceSettings.Get)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests assign open device tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignOpenDeviceTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using view As VI.K2002.Forms.TreeView = New VI.K2002.Forms.TreeView
            Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.FacadeTests.DeviceManager.OpenSession(TestInfo, 1, device, ResourceSettings.Get)
                    view.AssignDevice(device)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.CloseSession(TestInfo, 1, view)
                End Try
            End Using
        End Using
    End Sub

#End Region

End Class
