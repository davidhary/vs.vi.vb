'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\BinningView.vb
'
' summary:	Binning view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.VI.ExceptionExtensions
Imports isr.Core.SplitExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions

''' <summary> A Calculate Subsystem view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class BinningView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._PassBitPatternNumeric.NumericUpDownControl.Minimum = 1
        Me._PassBitPatternNumeric.NumericUpDownControl.Maximum = 63
        Me._PassBitPatternNumeric.NumericUpDownControl.Value = 32
        Me._PassBitPatternNumeric.NumericUpDownControl.DecimalPlaces = 0

        Me._BinningStrobeDurationNumeric.NumericUpDownControl.Minimum = 0
        Me._BinningStrobeDurationNumeric.NumericUpDownControl.Maximum = 999
        Me._BinningStrobeDurationNumeric.NumericUpDownControl.Value = 0
        Me._BinningStrobeDurationNumeric.NumericUpDownControl.DecimalPlaces = 2
    End Sub

    ''' <summary> Creates a new Calculate View. </summary>
    ''' <returns> A Calculate View. </returns>
    Public Shared Function Create() As BinningView
        Dim view As BinningView = Nothing
        Try
            view = New BinningView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Applies the settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying Binning subsystem state" : Me.PublishInfo($"{activity};. ")
            Me.BinningSubsystem.StartElapsedStopwatch()
            Me.BinningSubsystem.ApplyPassSource(CInt(Me._PassBitPatternNumeric.Value))
            Me.BinningSubsystem.ApplyBinningStrobeEnabled(Me._BinningStobeToggleButton.CheckState = CheckState.Checked)
            Me.BinningSubsystem.StopElapsedStopwatch()
            Me.ApplySettings(Me.DigitalOutputSubsystem)
            Me._Limit1View.ApplySettings()
            Me._Limit2View.ApplySettings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading Binning subsystem state" : Me.PublishInfo($"{activity};. ")
            BinningView.ReadSettings(Me.BinningSubsystem)
            Me.ApplyPropertyChanged(Me.BinningSubsystem)
            BinningView.ReadSettings(Me.DigitalOutputSubsystem)
            Me.ApplyPropertyChanged(Me.DigitalOutputSubsystem)
            Me._Limit1View.ReadSettings()
            Me._Limit2View.ReadSettings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Preform a limit test and read the fail state. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub PerformLimitTest()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} perform Binning limit test" : Me.PublishInfo($"{activity};. ")
            BinningView.PerformLimitTest(Me.BinningSubsystem)
            Me.ReadLimitTestState()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> read limit test result. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadLimitTestState()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.BinningSubsystem.StartElapsedStopwatch()
            Me.BinningSubsystem.QueryLimitsFailed()
            Me.BinningSubsystem.QueryLimit1Failed()
            Me.BinningSubsystem.QueryLimit2Failed()
            Me.Device.StatusSubsystemBase.QueryMeasurementEventStatus()
            Me.BinningSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(BinningView).SplitWords}")
        End If
        Me._Limit1View.AssignDevice(value)
        Me._Limit2View.AssignDevice(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " BINNING "

    ''' <summary> Gets or sets the <see cref="BinningSubsystemBase"/>. </summary>
    ''' <value> The <see cref="BinningSubsystemBase"/>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BinningSubsystem As BinningSubsystemBase

    ''' <summary> Binds the <see cref="BinningSubsystemBase"/> subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As BinningSubsystemBase)
        If Me.BinningSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.BinningSubsystem)
            Me._BinningSubsystem = Nothing
        End If
        Me._BinningSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, subsystem)
        End If
        Me._Limit1View.BindSubsystem(subsystem, 1)
        Me._Limit2View.BindSubsystem(subsystem, 2)
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As BinningSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.BinningSubsystemBasePropertyChanged
            BinningView.ReadSettings(subsystem)
            BinningView.ReadLimitTestState(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.BinningSubsystemBasePropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As BinningSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.BinningStrobeEnabled))
        Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.LimitsFailed))
        Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.PassSource))
    End Sub

    ''' <summary> Tests perform limit. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub PerformLimitTest(ByVal subsystem As BinningSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.Immediate()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> read limit test result. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadLimitTestState(ByVal subsystem As BinningSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryLimitsFailed()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As BinningSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryPassSource()
        subsystem.QueryBinningStrobeEnabled()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the Binning subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As BinningSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(BinningSubsystemBase.BinningStrobeEnabled)
                Me._BinningStobeToggleButton.CheckState = subsystem.BinningStrobeEnabled.ToCheckState
            Case NameOf(BinningSubsystemBase.PassSource)
                If subsystem.PassSource.HasValue Then Me._PassBitPatternNumeric.Value = CDec(subsystem.PassSource.Value)
            Case NameOf(BinningSubsystemBase.LimitsFailed)
                Me._LimitFailedButton.CheckState = subsystem.LimitsFailed.ToCheckState
            Case NameOf(BinningSubsystemBase.BinningStrobeDuration)
                Me._BinningStrobeDurationNumeric.Value = CDec(subsystem.BinningStrobeDuration.Ticks / TimeSpan.TicksPerMillisecond)
        End Select
    End Sub

    ''' <summary> Binning subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BinningSubsystemBasePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.BinningSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.BinningSubsystemBasePropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.BinningSubsystemBasePropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.BinningSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DIGITAL OUTPUT SUBSYSTEM "

    ''' <summary> Gets or sets the digital output subsystem. </summary>
    ''' <value> The digital output subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DigitalOutputSubsystem As DigitalOutputSubsystemBase

    ''' <summary> Bind ArmLayer2 subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As DigitalOutputSubsystemBase)
        If Me.DigitalOutputSubsystem IsNot Nothing Then
            Me._DigitalOutputSubsystem = Nothing
        End If
        Me._DigitalOutputSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, subsystem)
        End If
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub ReadSettings(ByVal subsystem As DigitalOutputSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryDigitalActiveLevels(New Integer() {1, 2, 3, 4})
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub ApplySettings(ByVal subsystem As DigitalOutputSubsystemBase)
        BinningView.ApplySettings(subsystem, If(Me._DigitalOutputActiveHighMenuItem.Checked, DigitalActiveLevels.High, DigitalActiveLevels.Low))
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="polarity">  The polarity. </param>
    Public Shared Sub ApplySettings(ByVal subsystem As DigitalOutputSubsystemBase, ByVal polarity As DigitalActiveLevels)
        subsystem.StartElapsedStopwatch()
        subsystem.ApplyDigitalActiveLevel(New Integer() {1, 2, 3, 4}, polarity)
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As DigitalOutputSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.DigitalOutputSubsystemBasePropertyChanged
            BinningView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.DigitalOutputSubsystemBasePropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As DigitalOutputSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(Me.DigitalOutputSubsystem.CurrentDigitalActiveLevel))
    End Sub

    ''' <summary> Handle the Calculate subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As DigitalOutputSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(DigitalOutputSubsystemBase.CommonDigitalActiveLevel)
                Me._DigitalOutputActiveHighMenuItem.CheckState = (subsystem.CommonDigitalActiveLevel = DigitalActiveLevels.High).ToCheckState
        End Select
    End Sub

    ''' <summary> Digital Output subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DigitalOutputSubsystemBasePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.DigitalOutputSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DigitalOutputSubsystemBasePropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DigitalOutputSubsystemBasePropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.DigitalOutputSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub


#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Pass bit pattern numeric button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PassBitPatternNumericButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _PassBitPatternNumericButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Pass {If(button.Checked, "0x", "0d")}"
            Me._PassBitPatternNumeric.NumericUpDownControl.Hexadecimal = button.Checked
        End If
    End Sub

    ''' <summary> Binning stobe toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BinningStobeToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _BinningStobeToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Strobe: {button.CheckState.ToCheckStateCaption("On", "Off", "?")}"
        End If
    End Sub

    ''' <summary> Limit failed button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LimitFailedButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _LimitFailedButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = button.CheckState.ToCheckStateCaption("Fail", "Pass", "P/F ?")
        End If
    End Sub

    ''' <summary> Applies the settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Reads state menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadStateMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadLimitTestStateMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ReadLimitTestState()
    End Sub

    ''' <summary> Preform limit test menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PreformLimitTestMenuItem_Click(sender As Object, e As EventArgs) Handles _PreformLimitTestMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.PerformLimitTest()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As isr.Core.ITraceMessageTalker)
        Me._Limit1View.AssignTalker(talker)
        Me._Limit2View.AssignTalker(talker)
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
