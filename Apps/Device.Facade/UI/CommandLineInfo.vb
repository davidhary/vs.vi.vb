'---------------------------------------------------------------------------------------------------
' file:		.\UI\CommandLineInfo.vb
'
' summary:	Command line information class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

''' <summary>
''' A sealed class the parses the command line and provides the command line values.
''' </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2011-02-02, x.x.4050.x. </para>
''' </remarks>
Public NotInheritable Class CommandLineInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " PARSER "

    ''' <summary> Gets the no-operation command line option. </summary>
    ''' <value> The no operation option. </value>
    Public Shared ReadOnly Property NoOperationOption As String
        Get
            Return "/nop"
        End Get
    End Property

    ''' <summary> Gets the Device-Enabled option. </summary>
    ''' <value> The Device enabled option. </value>
    Public Shared ReadOnly Property DevicesEnabledOption As String
        Get
            Return "/d:"
        End Get
    End Property

    ''' <summary> Gets the virtual instrument name option. </summary>
    ''' <value> The virtual instrument name option. </value>
    Public Shared ReadOnly Property VirtualInstrumentNameOption As String
        Get
            Return "/vi:"
        End Get
    End Property

    ''' <summary> Validate the command line. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <exception cref="ArgumentException"> This exception is raised if a command line argument is
    '''                                      not handled. </exception>
    ''' <param name="commandLineArguments"> The command line arguments. </param>
    Public Shared Sub ValidateCommandLine(ByVal commandLineArguments As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArguments IsNot Nothing Then
            For Each argument As String In commandLineArguments
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.VirtualInstrumentNameOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Nop = True
                Else
                    Throw New ArgumentException($"Unknown command line argument '{argument}' was detected. Should be Ignored.", NameOf(commandLineArguments))
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As String)
        If Not String.IsNullOrWhiteSpace(commandLineArgs) Then ParseCommandLine(New ObjectModel.ReadOnlyCollection(Of String)(commandLineArgs.Split(" "c)))
    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        Dim commandLineBuilder As New System.Text.StringBuilder
        If commandLineArgs IsNot Nothing Then
            For Each argument As String In commandLineArgs
                commandLineBuilder.AppendFormat("{0} ", argument)
                If argument.StartsWith(NoOperationOption, StringComparison.OrdinalIgnoreCase) Then
                    Nop = True
                ElseIf argument.StartsWith(CommandLineInfo.DevicesEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.DevicesEnabled = argument.Substring(CommandLineInfo.VirtualInstrumentNameOption.Length).StartsWith("n", StringComparison.OrdinalIgnoreCase)
                ElseIf argument.StartsWith(CommandLineInfo.VirtualInstrumentNameOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.VirtualInstrumentName = argument.Substring(CommandLineInfo.VirtualInstrumentNameOption.Length)
                Else
                    ' do nothing
                End If
            Next
        End If
        _CommandLine = commandLineBuilder.ToString

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As (Success As Boolean, Details As String)
        Dim activity As String = "joining the command line arguments"
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If commandLineArgs Is Nothing OrElse Not commandLineArgs.Any Then
            CommandLineInfo._CommandLine = String.Empty
        Else
            Try
                CommandLineInfo._CommandLine = String.Join(",", commandLineArgs)
                activity = $"Parsing the commandLine {CommandLineInfo.CommandLine}"
                ' Data.ParseCommandLine(commandLineArgs)
                CommandLineInfo.ParseCommandLine(commandLineArgs)
            Catch ex As ArgumentException
                result = (False, $"Unknown argument value for '{ex.ParamName}' ignored")
            Catch ex As System.Exception
                result = (False, $"Failed {activity};. {ex.ToFullBlownString}")
            End Try
        End If
        Return result
    End Function

#End Region

#Region " COMMAND LINE ELEMENTS "

    ''' <summary> Gets or sets the command line. </summary>
    ''' <value> The command line. </value>
    Public Shared ReadOnly Property CommandLine As String

    ''' <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
    ''' <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
    Public Shared Property DevicesEnabled() As Boolean?

    ''' <summary> Gets or sets the no operation option. </summary>
    ''' <value> The no-op. </value>
    Public Shared Property Nop As Boolean

    ''' <summary> Name of the virtual instrument. </summary>
    Private Shared _VirtualInstrumentName As String = String.Empty

    ''' <summary> Gets or sets the name of the virtual instrument. </summary>
    ''' <value> The name of the virtual instrument. </value>
    Public Shared Property VirtualInstrumentName As String
        Get
            Return CommandLineInfo._VirtualInstrumentName
        End Get
        Set(value As String)
            CommandLineInfo._VirtualInstrumentName = value
            Dim vi As VirtualInstrument
            If Not [Enum].TryParse(Of VirtualInstrument)(value, True, vi) Then
                vi = Facade.VirtualInstrument.ScpiSession
            End If
            CommandLineInfo.VirtualInstrument = vi
        End Set
    End Property

    ''' <summary> Gets or sets the virtual instrument. </summary>
    ''' <value> The virtual instrument. </value>
    Public Shared Property VirtualInstrument As VirtualInstrument?

#End Region

End Class

''' <summary> Values that represent virtual instruments. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Enum VirtualInstrument

    ''' <summary> An enum constant representing the switchboard option. </summary>
    <System.ComponentModel.Description("Switchboard")>
    Switchboard

    ''' <summary> An enum constant representing the ats 600 option. </summary>
    <System.ComponentModel.Description("ATS600")>
    Ats600

    ''' <summary> An enum constant representing the 4990 option. </summary>
    <System.ComponentModel.Description("E4990")>
    E4990

    ''' <summary> An enum constant representing the eg 2000 option. </summary>
    <System.ComponentModel.Description("EG2000")>
    EG2000

    ''' <summary> An enum constant representing the 2002 option. </summary>
    <System.ComponentModel.Description("K2002")>
    K2002

    ''' <summary> An enum constant representing the 2400 option. </summary>
    <System.ComponentModel.Description("K2400")>
    K2400

    ''' <summary> An enum constant representing the 2450 option. </summary>
    <System.ComponentModel.Description("K2450")>
    K2450

    ''' <summary> An enum constant representing the 2600 option. </summary>
    <System.ComponentModel.Description("K2600")>
    K2600

    ''' <summary> An enum constant representing the 2700 option. </summary>
    <System.ComponentModel.Description("K2700")>
    K2700

    ''' <summary> An enum constant representing the 3458 option. </summary>
    <System.ComponentModel.Description("K3458")>
    K3458

    ''' <summary> An enum constant representing the 34980 option. </summary>
    <System.ComponentModel.Description("K34980")>
    K34980

    ''' <summary> An enum constant representing the 3700rs option. </summary>
    <System.ComponentModel.Description("K3700 R Meter")>
    K3700rs

    ''' <summary> An enum constant representing the 3700 option. </summary>
    <System.ComponentModel.Description("K3700")>
    K3700

    ''' <summary> An enum constant representing the 6500 option. </summary>
    <System.ComponentModel.Description("K6500")>
    K6500

    ''' <summary> An enum constant representing the 7000 option. </summary>
    <System.ComponentModel.Description("K7000")>
    K7000

    ''' <summary> An enum constant representing the 7500 option. </summary>
    <System.ComponentModel.Description("K7500")>
    K7500

    ''' <summary> An enum constant representing the 7500 s option. </summary>
    <System.ComponentModel.Description("K7500S")>
    K7500S

    ''' <summary> An enum constant representing the 1750 option. </summary>
    <System.ComponentModel.Description("T1750")>
    T1750

    ''' <summary> An enum constant representing the tsp rig option. </summary>
    <System.ComponentModel.Description("TspRig")>
    TspRig

    ''' <summary> An enum constant representing the scpi session option. </summary>
    <System.ComponentModel.Description("SCPI Session")>
    ScpiSession

    ''' <summary> An enum constant representing the tsp session option. </summary>
    <System.ComponentModel.Description("Tsp Session")>
    TspSession

    ''' <summary> An enum constant representing the tsp 2 session option. </summary>
    <System.ComponentModel.Description("Tsp2 Session")>
    Tsp2Session
End Enum
