''' <summary> A channel resistor. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-13 </para>
''' </remarks>
Public Class ChannelResistor

    ''' <summary> Constructor. </summary>
    ''' <param name="title">       The title. </param>
    ''' <param name="channelList"> A list of channels. </param>
    Public Sub New(ByVal title As String, ByVal channelList As String)
        MyBase.New()
        Me.Title = title
        Me.ChannelList = channelList
    End Sub

    ''' <summary> Gets the title. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The title. </value>
    Public ReadOnly Property Title As String

    ''' <summary> Gets a list of channels. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> A list of channels. </value>
    Public ReadOnly Property ChannelList As String

    ''' <summary> Gets the resistance. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The resistance. </value>
    Public Property Resistance As Double

    ''' <summary> Gets the status. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The status. </value>
    Public Property Status As MetaStatus
End Class

''' <summary> channel Resistor Collection: an ordered collection of resistors. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-13 </para>
''' </remarks>
Public Class ChannelResistorCollection
    Inherits ObjectModel.KeyedCollection(Of String, ChannelResistor)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As ChannelResistor) As String
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.Title
    End Function

    ''' <summary> Adds a new resistor. </summary>
    ''' <param name="title">       The title. </param>
    ''' <param name="channelList"> List of channels. </param>
    Public Sub AddResistor(ByVal title As String, ByVal channelList As String)
        Me.Add(New ChannelResistor(title, channelList))
    End Sub

    ''' <summary> Gets the resistances. </summary>
    ''' <value> The resistances. </value>
    Public ReadOnly Property Resistances As IList(Of Double)
        Get
            Dim l As New List(Of Double)
            For Each resistor As ChannelResistor In Me
                l.Add(resistor.Resistance)
            Next
            Return l
        End Get
    End Property

    ''' <summary> Builds status report. </summary>
    ''' <returns> A String. </returns>
    Public Function BuildStatusReport() As String
        Dim infoBuilder As New System.Text.StringBuilder
        For Each r As ChannelResistor In Me
            infoBuilder.AppendLine($"{r.Title}:{r.Status.ToLongDescription("")}")
        Next
        Return infoBuilder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

    ''' <summary> Gets the status. </summary>
    ''' <value> The status. </value>
    Public ReadOnly Property Status As MetaStatus
        Get
            Dim result As New MetaStatus
            For Each r As ChannelResistor In Me
                result.Append(r.Status)
            Next
            Return result
        End Get
    End Property

End Class

