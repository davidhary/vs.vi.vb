﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FunctionView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FunctionView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._QueryStatusButton = New System.Windows.Forms.Button()
        Me._OperatorCycleTabs = New System.Windows.Forms.TabControl()
        Me._CycleTabPage1 = New System.Windows.Forms.TabPage()
        Me._CycleToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ResetCycleModeButton = New System.Windows.Forms.ToolStripButton()
        Me._StartCycleButton = New System.Windows.Forms.ToolStripButton()
        Me._NextSetpointButton = New System.Windows.Forms.ToolStripButton()
        Me._StopCycleButton = New System.Windows.Forms.ToolStripButton()
        Me._CyclesCompletedCheckBox1 = New isr.Core.Controls.ToolStripCheckBox()
        Me._CycleCompletedCheckBox1 = New isr.Core.Controls.ToolStripCheckBox()
        Me._CyclingStoppedCheckBox = New isr.Core.Controls.ToolStripCheckBox()
        Me._FindLastSetpointButton1 = New System.Windows.Forms.ToolStripButton()
        Me._OperatorTabPage = New System.Windows.Forms.TabPage()
        Me._ResetDelayNumericLabel = New System.Windows.Forms.Label()
        Me._ResetQueryDelayNumeric = New isr.Core.Controls.NumericUpDown()
        Me._ResetOperatorModeButton = New System.Windows.Forms.Button()
        Me._SetpointNumberNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._TestTimeElapsedCheckBox = New isr.Core.Controls.CheckBox()
        Me._CycleCountNumericLabel = New System.Windows.Forms.Label()
        Me._CycleCountNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._MaxTestTimeNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._MaxTestTimeNumericLabel = New System.Windows.Forms.Label()
        Me._ReadSetpointButton = New System.Windows.Forms.Button()
        Me._ReadyCheckBox = New isr.Core.Controls.CheckBox()
        Me._NotAtTempCheckBox = New isr.Core.Controls.CheckBox()
        Me._AtTempCheckBox = New isr.Core.Controls.CheckBox()
        Me._HeadDownCheckBox = New isr.Core.Controls.CheckBox()
        Me._SetpointWindowNumericLabel = New System.Windows.Forms.Label()
        Me._SetpointWindowNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._SetpointNumberNumericLabel = New System.Windows.Forms.Label()
        Me._RampRateNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._SoakTimeNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._SetpointNumeric = New isr.Core.Controls.SelectorNumeric()
        Me._RampRateNumericLabel = New System.Windows.Forms.Label()
        Me._SoakTimeNumericLabel = New System.Windows.Forms.Label()
        Me._SetpointNumericLabel = New System.Windows.Forms.Label()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        Me._OperatorCycleTabs.SuspendLayout()
        Me._CycleTabPage1.SuspendLayout()
        Me._CycleToolStrip.SuspendLayout()
        Me._OperatorTabPage.SuspendLayout()
        CType(Me._ResetQueryDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(379, 322)
        Me._Layout.TabIndex = 0
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._QueryStatusButton)
        Me._Panel.Controls.Add(Me._OperatorCycleTabs)
        Me._Panel.Controls.Add(Me._SetpointNumberNumeric)
        Me._Panel.Controls.Add(Me._TestTimeElapsedCheckBox)
        Me._Panel.Controls.Add(Me._CycleCountNumericLabel)
        Me._Panel.Controls.Add(Me._CycleCountNumeric)
        Me._Panel.Controls.Add(Me._MaxTestTimeNumeric)
        Me._Panel.Controls.Add(Me._MaxTestTimeNumericLabel)
        Me._Panel.Controls.Add(Me._ReadSetpointButton)
        Me._Panel.Controls.Add(Me._ReadyCheckBox)
        Me._Panel.Controls.Add(Me._NotAtTempCheckBox)
        Me._Panel.Controls.Add(Me._AtTempCheckBox)
        Me._Panel.Controls.Add(Me._HeadDownCheckBox)
        Me._Panel.Controls.Add(Me._SetpointWindowNumericLabel)
        Me._Panel.Controls.Add(Me._SetpointWindowNumeric)
        Me._Panel.Controls.Add(Me._SetpointNumberNumericLabel)
        Me._Panel.Controls.Add(Me._RampRateNumeric)
        Me._Panel.Controls.Add(Me._SoakTimeNumeric)
        Me._Panel.Controls.Add(Me._SetpointNumeric)
        Me._Panel.Controls.Add(Me._RampRateNumericLabel)
        Me._Panel.Controls.Add(Me._SoakTimeNumericLabel)
        Me._Panel.Controls.Add(Me._SetpointNumericLabel)
        Me._Panel.Location = New System.Drawing.Point(13, 23)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(352, 276)
        Me._Panel.TabIndex = 0
        '
        '_QueryStatusButton
        '
        Me._QueryStatusButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._QueryStatusButton.Location = New System.Drawing.Point(237, 5)
        Me._QueryStatusButton.Name = "_QueryStatusButton"
        Me._QueryStatusButton.Size = New System.Drawing.Size(107, 28)
        Me._QueryStatusButton.TabIndex = 24
        Me._QueryStatusButton.Text = "Refresh Status"
        Me._QueryStatusButton.UseVisualStyleBackColor = True
        '
        '_OperatorCycleTabs
        '
        Me._OperatorCycleTabs.Controls.Add(Me._CycleTabPage1)
        Me._OperatorCycleTabs.Controls.Add(Me._OperatorTabPage)
        Me._OperatorCycleTabs.Location = New System.Drawing.Point(207, 60)
        Me._OperatorCycleTabs.Name = "_OperatorCycleTabs"
        Me._OperatorCycleTabs.SelectedIndex = 0
        Me._OperatorCycleTabs.Size = New System.Drawing.Size(138, 208)
        Me._OperatorCycleTabs.TabIndex = 44
        '
        '_CycleTabPage1
        '
        Me._CycleTabPage1.Controls.Add(Me._CycleToolStrip)
        Me._CycleTabPage1.Location = New System.Drawing.Point(4, 26)
        Me._CycleTabPage1.Name = "_CycleTabPage1"
        Me._CycleTabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me._CycleTabPage1.Size = New System.Drawing.Size(130, 178)
        Me._CycleTabPage1.TabIndex = 0
        Me._CycleTabPage1.Text = "Cycle"
        Me._CycleTabPage1.UseVisualStyleBackColor = True
        '
        '_CycleToolStrip
        '
        Me._CycleToolStrip.Dock = System.Windows.Forms.DockStyle.Left
        Me._CycleToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._CycleToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ResetCycleModeButton, Me._StartCycleButton, Me._NextSetpointButton, Me._StopCycleButton, Me._CyclesCompletedCheckBox1, Me._CycleCompletedCheckBox1, Me._CyclingStoppedCheckBox, Me._FindLastSetpointButton1})
        Me._CycleToolStrip.Location = New System.Drawing.Point(3, 3)
        Me._CycleToolStrip.Name = "_CycleToolStrip"
        Me._CycleToolStrip.Size = New System.Drawing.Size(123, 172)
        Me._CycleToolStrip.TabIndex = 22
        Me._CycleToolStrip.Text = "ToolStrip1"
        '
        '_ResetCycleModeButton
        '
        Me._ResetCycleModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ResetCycleModeButton.Image = CType(resources.GetObject("_ResetCycleModeButton.Image"), System.Drawing.Image)
        Me._ResetCycleModeButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ResetCycleModeButton.Margin = New System.Windows.Forms.Padding(0)
        Me._ResetCycleModeButton.Name = "_ResetCycleModeButton"
        Me._ResetCycleModeButton.Size = New System.Drawing.Size(120, 19)
        Me._ResetCycleModeButton.Text = "Cycle Mode: On/Off"
        '
        '_StartCycleButton
        '
        Me._StartCycleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StartCycleButton.Image = CType(resources.GetObject("_StartCycleButton.Image"), System.Drawing.Image)
        Me._StartCycleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._StartCycleButton.Margin = New System.Windows.Forms.Padding(0)
        Me._StartCycleButton.Name = "_StartCycleButton"
        Me._StartCycleButton.Size = New System.Drawing.Size(120, 19)
        Me._StartCycleButton.Text = "Start"
        '
        '_NextSetpointButton
        '
        Me._NextSetpointButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._NextSetpointButton.Image = CType(resources.GetObject("_NextSetpointButton.Image"), System.Drawing.Image)
        Me._NextSetpointButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._NextSetpointButton.Margin = New System.Windows.Forms.Padding(0)
        Me._NextSetpointButton.Name = "_NextSetpointButton"
        Me._NextSetpointButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._NextSetpointButton.Size = New System.Drawing.Size(120, 19)
        Me._NextSetpointButton.Text = "Next"
        '
        '_StopCycleButton
        '
        Me._StopCycleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StopCycleButton.Image = CType(resources.GetObject("_StopCycleButton.Image"), System.Drawing.Image)
        Me._StopCycleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._StopCycleButton.Margin = New System.Windows.Forms.Padding(0)
        Me._StopCycleButton.Name = "_StopCycleButton"
        Me._StopCycleButton.Size = New System.Drawing.Size(120, 19)
        Me._StopCycleButton.Text = "Stop"
        Me._StopCycleButton.ToolTipText = "Stop cycle"
        '
        '_CyclesCompletedCheckBox1
        '
        Me._CyclesCompletedCheckBox1.Checked = False
        Me._CyclesCompletedCheckBox1.Margin = New System.Windows.Forms.Padding(0)
        Me._CyclesCompletedCheckBox1.Name = "_CyclesCompletedCheckBox1"
        Me._CyclesCompletedCheckBox1.Size = New System.Drawing.Size(120, 19)
        Me._CyclesCompletedCheckBox1.Text = "Cycles Completed"
        '
        '_CycleCompletedCheckBox1
        '
        Me._CycleCompletedCheckBox1.Checked = False
        Me._CycleCompletedCheckBox1.Margin = New System.Windows.Forms.Padding(0)
        Me._CycleCompletedCheckBox1.Name = "_CycleCompletedCheckBox1"
        Me._CycleCompletedCheckBox1.Size = New System.Drawing.Size(120, 19)
        Me._CycleCompletedCheckBox1.Text = "Cycle Complete"
        '
        '_CyclingStoppedCheckBox
        '
        Me._CyclingStoppedCheckBox.Checked = False
        Me._CyclingStoppedCheckBox.Margin = New System.Windows.Forms.Padding(0)
        Me._CyclingStoppedCheckBox.Name = "_CyclingStoppedCheckBox"
        Me._CyclingStoppedCheckBox.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._CyclingStoppedCheckBox.Size = New System.Drawing.Size(120, 19)
        Me._CyclingStoppedCheckBox.Text = "Cycling Stopped"
        '
        '_FindLastSetpointButton1
        '
        Me._FindLastSetpointButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._FindLastSetpointButton1.Image = CType(resources.GetObject("_FindLastSetpointButton1.Image"), System.Drawing.Image)
        Me._FindLastSetpointButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FindLastSetpointButton1.Margin = New System.Windows.Forms.Padding(0)
        Me._FindLastSetpointButton1.Name = "_FindLastSetpointButton1"
        Me._FindLastSetpointButton1.Size = New System.Drawing.Size(120, 19)
        Me._FindLastSetpointButton1.Text = "Find Last Setpoint: -1"
        '
        '_OperatorTabPage
        '
        Me._OperatorTabPage.Controls.Add(Me._ResetDelayNumericLabel)
        Me._OperatorTabPage.Controls.Add(Me._ResetQueryDelayNumeric)
        Me._OperatorTabPage.Controls.Add(Me._ResetOperatorModeButton)
        Me._OperatorTabPage.Location = New System.Drawing.Point(4, 26)
        Me._OperatorTabPage.Name = "_OperatorTabPage"
        Me._OperatorTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._OperatorTabPage.Size = New System.Drawing.Size(130, 178)
        Me._OperatorTabPage.TabIndex = 1
        Me._OperatorTabPage.Text = "Operator"
        Me._OperatorTabPage.UseVisualStyleBackColor = True
        '
        '_ResetDelayNumericLabel
        '
        Me._ResetDelayNumericLabel.AutoSize = True
        Me._ResetDelayNumericLabel.Location = New System.Drawing.Point(5, 67)
        Me._ResetDelayNumericLabel.Name = "_ResetDelayNumericLabel"
        Me._ResetDelayNumericLabel.Size = New System.Drawing.Size(72, 17)
        Me._ResetDelayNumericLabel.TabIndex = 14
        Me._ResetDelayNumericLabel.Text = "Delay [ms]:"
        '
        '_ResetQueryDelayNumeric
        '
        Me._ResetQueryDelayNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResetQueryDelayNumeric.Location = New System.Drawing.Point(80, 63)
        Me._ResetQueryDelayNumeric.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._ResetQueryDelayNumeric.Name = "_ResetQueryDelayNumeric"
        Me._ResetQueryDelayNumeric.NullValue = New Decimal(New Integer() {5, 0, 0, 0})
        Me._ResetQueryDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._ResetQueryDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._ResetQueryDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._ResetQueryDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._ResetQueryDelayNumeric.Size = New System.Drawing.Size(45, 25)
        Me._ResetQueryDelayNumeric.TabIndex = 13
        Me._ResetQueryDelayNumeric.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        '_ResetOperatorModeButton
        '
        Me._ResetOperatorModeButton.Location = New System.Drawing.Point(8, 6)
        Me._ResetOperatorModeButton.Name = "_ResetOperatorModeButton"
        Me._ResetOperatorModeButton.Size = New System.Drawing.Size(114, 49)
        Me._ResetOperatorModeButton.TabIndex = 0
        Me._ResetOperatorModeButton.Text = "Operator Mode: On/Off"
        Me._ResetOperatorModeButton.UseVisualStyleBackColor = True
        '
        '_SetpointNumberNumeric
        '
        Me._SetpointNumberNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SetpointNumberNumeric.CanSelectEmptyValue = True
        Me._SetpointNumberNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._SetpointNumberNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SetpointNumberNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SetpointNumberNumeric.Location = New System.Drawing.Point(117, 88)
        Me._SetpointNumberNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetpointNumberNumeric.Minimum = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SetpointNumberNumeric.Name = "_SetpointNumberNumeric"
        Me._SetpointNumberNumeric.SelectorIcon = CType(resources.GetObject("_SetpointNumberNumeric.SelectorIcon"), System.Drawing.Image)
        Me._SetpointNumberNumeric.Size = New System.Drawing.Size(84, 25)
        Me._SetpointNumberNumeric.TabIndex = 30
        Me._SetpointNumberNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SetpointNumberNumeric.Watermark = Nothing
        '
        '_TestTimeElapsedCheckBox
        '
        Me._TestTimeElapsedCheckBox.AutoSize = True
        Me._TestTimeElapsedCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._TestTimeElapsedCheckBox.Location = New System.Drawing.Point(25, 48)
        Me._TestTimeElapsedCheckBox.Name = "_TestTimeElapsedCheckBox"
        Me._TestTimeElapsedCheckBox.ReadOnly = True
        Me._TestTimeElapsedCheckBox.Size = New System.Drawing.Size(104, 21)
        Me._TestTimeElapsedCheckBox.TabIndex = 28
        Me._TestTimeElapsedCheckBox.Text = "Test Timeout:"
        Me._TestTimeElapsedCheckBox.ThreeState = True
        Me._TestTimeElapsedCheckBox.UseVisualStyleBackColor = True
        '
        '_CycleCountNumericLabel
        '
        Me._CycleCountNumericLabel.AutoSize = True
        Me._CycleCountNumericLabel.Location = New System.Drawing.Point(35, 248)
        Me._CycleCountNumericLabel.Name = "_CycleCountNumericLabel"
        Me._CycleCountNumericLabel.Size = New System.Drawing.Size(79, 17)
        Me._CycleCountNumericLabel.TabIndex = 43
        Me._CycleCountNumericLabel.Text = "Cycle Count:"
        '
        '_CycleCountNumeric
        '
        Me._CycleCountNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._CycleCountNumeric.CanSelectEmptyValue = True
        Me._CycleCountNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._CycleCountNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._CycleCountNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CycleCountNumeric.Location = New System.Drawing.Point(117, 244)
        Me._CycleCountNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CycleCountNumeric.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me._CycleCountNumeric.Minimum = New Decimal(New Integer() {0, 0, 0, 0})
        Me._CycleCountNumeric.Name = "_CycleCountNumeric"
        Me._CycleCountNumeric.SelectorIcon = CType(resources.GetObject("_CycleCountNumeric.SelectorIcon"), System.Drawing.Image)
        Me._CycleCountNumeric.Size = New System.Drawing.Size(84, 25)
        Me._CycleCountNumeric.TabIndex = 42
        Me._CycleCountNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._CycleCountNumeric.Watermark = Nothing
        '
        '_MaxTestTimeNumeric
        '
        Me._MaxTestTimeNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._MaxTestTimeNumeric.CanSelectEmptyValue = True
        Me._MaxTestTimeNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._MaxTestTimeNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._MaxTestTimeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MaxTestTimeNumeric.Location = New System.Drawing.Point(117, 218)
        Me._MaxTestTimeNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._MaxTestTimeNumeric.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me._MaxTestTimeNumeric.Minimum = New Decimal(New Integer() {0, 0, 0, 0})
        Me._MaxTestTimeNumeric.Name = "_MaxTestTimeNumeric"
        Me._MaxTestTimeNumeric.SelectorIcon = CType(resources.GetObject("_MaxTestTimeNumeric.SelectorIcon"), System.Drawing.Image)
        Me._MaxTestTimeNumeric.Size = New System.Drawing.Size(84, 25)
        Me._MaxTestTimeNumeric.TabIndex = 41
        Me._MaxTestTimeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._MaxTestTimeNumeric.Watermark = Nothing
        '
        '_MaxTestTimeNumericLabel
        '
        Me._MaxTestTimeNumericLabel.AutoSize = True
        Me._MaxTestTimeNumericLabel.Location = New System.Drawing.Point(29, 223)
        Me._MaxTestTimeNumericLabel.Name = "_MaxTestTimeNumericLabel"
        Me._MaxTestTimeNumericLabel.Size = New System.Drawing.Size(84, 17)
        Me._MaxTestTimeNumericLabel.TabIndex = 40
        Me._MaxTestTimeNumericLabel.Text = "Test Time [s]:"
        '
        '_ReadSetpointButton
        '
        Me._ReadSetpointButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadSetpointButton.Location = New System.Drawing.Point(237, 33)
        Me._ReadSetpointButton.Name = "_ReadSetpointButton"
        Me._ReadSetpointButton.Size = New System.Drawing.Size(109, 25)
        Me._ReadSetpointButton.TabIndex = 39
        Me._ReadSetpointButton.Text = "Read Setpoint"
        Me._ReadSetpointButton.UseVisualStyleBackColor = True
        '
        '_ReadyCheckBox
        '
        Me._ReadyCheckBox.AutoSize = True
        Me._ReadyCheckBox.Location = New System.Drawing.Point(143, 10)
        Me._ReadyCheckBox.Name = "_ReadyCheckBox"
        Me._ReadyCheckBox.ReadOnly = True
        Me._ReadyCheckBox.Size = New System.Drawing.Size(66, 21)
        Me._ReadyCheckBox.TabIndex = 26
        Me._ReadyCheckBox.Text = ":Ready"
        Me._ReadyCheckBox.ThreeState = True
        Me._ReadyCheckBox.UseVisualStyleBackColor = True
        '
        '_NotAtTempCheckBox
        '
        Me._NotAtTempCheckBox.AutoSize = True
        Me._NotAtTempCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._NotAtTempCheckBox.Location = New System.Drawing.Point(25, 29)
        Me._NotAtTempCheckBox.Name = "_NotAtTempCheckBox"
        Me._NotAtTempCheckBox.ReadOnly = True
        Me._NotAtTempCheckBox.Size = New System.Drawing.Size(104, 21)
        Me._NotAtTempCheckBox.TabIndex = 27
        Me._NotAtTempCheckBox.Text = "Not At Temp:"
        Me._NotAtTempCheckBox.ThreeState = True
        Me._NotAtTempCheckBox.UseVisualStyleBackColor = True
        '
        '_AtTempCheckBox
        '
        Me._AtTempCheckBox.AutoSize = True
        Me._AtTempCheckBox.Location = New System.Drawing.Point(143, 29)
        Me._AtTempCheckBox.Name = "_AtTempCheckBox"
        Me._AtTempCheckBox.ReadOnly = True
        Me._AtTempCheckBox.Size = New System.Drawing.Size(78, 21)
        Me._AtTempCheckBox.TabIndex = 25
        Me._AtTempCheckBox.Text = ":At Temp"
        Me._AtTempCheckBox.ThreeState = True
        Me._AtTempCheckBox.UseVisualStyleBackColor = True
        '
        '_HeadDownCheckBox
        '
        Me._HeadDownCheckBox.AutoSize = True
        Me._HeadDownCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._HeadDownCheckBox.Location = New System.Drawing.Point(32, 10)
        Me._HeadDownCheckBox.Name = "_HeadDownCheckBox"
        Me._HeadDownCheckBox.ReadOnly = True
        Me._HeadDownCheckBox.Size = New System.Drawing.Size(98, 21)
        Me._HeadDownCheckBox.TabIndex = 23
        Me._HeadDownCheckBox.Text = "Head Down:"
        Me._HeadDownCheckBox.ThreeState = True
        Me._HeadDownCheckBox.UseVisualStyleBackColor = True
        '
        '_SetpointWindowNumericLabel
        '
        Me._SetpointWindowNumericLabel.AutoSize = True
        Me._SetpointWindowNumericLabel.Location = New System.Drawing.Point(31, 144)
        Me._SetpointWindowNumericLabel.Name = "_SetpointWindowNumericLabel"
        Me._SetpointWindowNumericLabel.Size = New System.Drawing.Size(83, 17)
        Me._SetpointWindowNumericLabel.TabIndex = 33
        Me._SetpointWindowNumericLabel.Text = "Window [°C]:"
        '
        '_SetpointWindowNumeric
        '
        Me._SetpointWindowNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SetpointWindowNumeric.DecimalPlaces = 1
        Me._SetpointWindowNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._SetpointWindowNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SetpointWindowNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SetpointWindowNumeric.Location = New System.Drawing.Point(117, 140)
        Me._SetpointWindowNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetpointWindowNumeric.Maximum = New Decimal(New Integer() {99, 0, 0, 65536})
        Me._SetpointWindowNumeric.Minimum = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SetpointWindowNumeric.Name = "_SetpointWindowNumeric"
        Me._SetpointWindowNumeric.SelectorIcon = CType(resources.GetObject("_SetpointWindowNumeric.SelectorIcon"), System.Drawing.Image)
        Me._SetpointWindowNumeric.Size = New System.Drawing.Size(84, 25)
        Me._SetpointWindowNumeric.TabIndex = 34
        Me._SetpointWindowNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SetpointWindowNumeric.Watermark = Nothing
        '
        '_SetpointNumberNumericLabel
        '
        Me._SetpointNumberNumericLabel.AutoSize = True
        Me._SetpointNumberNumericLabel.Location = New System.Drawing.Point(6, 90)
        Me._SetpointNumberNumericLabel.Name = "_SetpointNumberNumericLabel"
        Me._SetpointNumberNumericLabel.Size = New System.Drawing.Size(108, 17)
        Me._SetpointNumberNumericLabel.TabIndex = 29
        Me._SetpointNumberNumericLabel.Text = "Setpoint number:"
        '
        '_RampRateNumeric
        '
        Me._RampRateNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._RampRateNumeric.CanSelectEmptyValue = True
        Me._RampRateNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._RampRateNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._RampRateNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RampRateNumeric.Location = New System.Drawing.Point(117, 192)
        Me._RampRateNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._RampRateNumeric.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me._RampRateNumeric.Minimum = New Decimal(New Integer() {0, 0, 0, 0})
        Me._RampRateNumeric.Name = "_RampRateNumeric"
        Me._RampRateNumeric.SelectorIcon = CType(resources.GetObject("_RampRateNumeric.SelectorIcon"), System.Drawing.Image)
        Me._RampRateNumeric.Size = New System.Drawing.Size(84, 25)
        Me._RampRateNumeric.TabIndex = 37
        Me._RampRateNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._RampRateNumeric.Watermark = Nothing
        '
        '_SoakTimeNumeric
        '
        Me._SoakTimeNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SoakTimeNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._SoakTimeNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SoakTimeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SoakTimeNumeric.Location = New System.Drawing.Point(117, 166)
        Me._SoakTimeNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SoakTimeNumeric.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me._SoakTimeNumeric.Minimum = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SoakTimeNumeric.Name = "_SoakTimeNumeric"
        Me._SoakTimeNumeric.SelectorIcon = CType(resources.GetObject("_SoakTimeNumeric.SelectorIcon"), System.Drawing.Image)
        Me._SoakTimeNumeric.Size = New System.Drawing.Size(84, 25)
        Me._SoakTimeNumeric.TabIndex = 38
        Me._SoakTimeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SoakTimeNumeric.Watermark = Nothing
        '
        '_SetpointNumeric
        '
        Me._SetpointNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._SetpointNumeric.DecimalPlaces = 1
        Me._SetpointNumeric.DirtyBackColor = System.Drawing.Color.Orange
        Me._SetpointNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption
        Me._SetpointNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SetpointNumeric.Location = New System.Drawing.Point(117, 114)
        Me._SetpointNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetpointNumeric.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me._SetpointNumeric.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me._SetpointNumeric.Name = "_SetpointNumeric"
        Me._SetpointNumeric.SelectorIcon = CType(resources.GetObject("_SetpointNumeric.SelectorIcon"), System.Drawing.Image)
        Me._SetpointNumeric.Size = New System.Drawing.Size(84, 25)
        Me._SetpointNumeric.TabIndex = 32
        Me._SetpointNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me._SetpointNumeric.Watermark = Nothing
        '
        '_RampRateNumericLabel
        '
        Me._RampRateNumericLabel.AutoSize = True
        Me._RampRateNumericLabel.Location = New System.Drawing.Point(26, 196)
        Me._RampRateNumericLabel.Name = "_RampRateNumericLabel"
        Me._RampRateNumericLabel.Size = New System.Drawing.Size(88, 17)
        Me._RampRateNumericLabel.TabIndex = 35
        Me._RampRateNumericLabel.Text = "Ramp [°/min]:"
        Me._RampRateNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SoakTimeNumericLabel
        '
        Me._SoakTimeNumericLabel.AutoSize = True
        Me._SoakTimeNumericLabel.Location = New System.Drawing.Point(25, 170)
        Me._SoakTimeNumericLabel.Name = "_SoakTimeNumericLabel"
        Me._SoakTimeNumericLabel.Size = New System.Drawing.Size(89, 17)
        Me._SoakTimeNumericLabel.TabIndex = 36
        Me._SoakTimeNumericLabel.Text = "Soak Time [s]:"
        Me._SoakTimeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SetpointNumericLabel
        '
        Me._SetpointNumericLabel.AutoSize = True
        Me._SetpointNumericLabel.Location = New System.Drawing.Point(30, 118)
        Me._SetpointNumericLabel.Name = "_SetpointNumericLabel"
        Me._SetpointNumericLabel.Size = New System.Drawing.Size(84, 17)
        Me._SetpointNumericLabel.TabIndex = 31
        Me._SetpointNumericLabel.Text = "Setpoint [°C]:"
        Me._SetpointNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FunctionView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "FunctionView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me._OperatorCycleTabs.ResumeLayout(False)

        Me._CycleTabPage1.ResumeLayout(False)
        Me._CycleTabPage1.PerformLayout()
        Me._CycleToolStrip.ResumeLayout(False)
        Me._CycleToolStrip.PerformLayout()
        Me._OperatorTabPage.ResumeLayout(False)
        Me._OperatorTabPage.PerformLayout()
        CType(Me._ResetQueryDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _QueryStatusButton As Windows.Forms.Button
    Private WithEvents _OperatorCycleTabs As Windows.Forms.TabControl
    Private WithEvents _CycleTabPage1 As Windows.Forms.TabPage
    Private WithEvents _CycleToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ResetCycleModeButton As Windows.Forms.ToolStripButton
    Private WithEvents _StartCycleButton As Windows.Forms.ToolStripButton
    Private WithEvents _NextSetpointButton As Windows.Forms.ToolStripButton
    Private WithEvents _StopCycleButton As Windows.Forms.ToolStripButton
    Private WithEvents _CyclesCompletedCheckBox1 As Core.Controls.ToolStripCheckBox
    Private WithEvents _CycleCompletedCheckBox1 As Core.Controls.ToolStripCheckBox
    Private WithEvents _CyclingStoppedCheckBox As Core.Controls.ToolStripCheckBox
    Private WithEvents _FindLastSetpointButton1 As Windows.Forms.ToolStripButton
    Private WithEvents _OperatorTabPage As Windows.Forms.TabPage
    Private WithEvents _ResetDelayNumericLabel As Windows.Forms.Label
    Private WithEvents _ResetQueryDelayNumeric As Core.Controls.NumericUpDown
    Private WithEvents _ResetOperatorModeButton As Windows.Forms.Button
    Private WithEvents _SetpointNumberNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _TestTimeElapsedCheckBox As Core.Controls.CheckBox
    Private WithEvents _CycleCountNumericLabel As Windows.Forms.Label
    Private WithEvents _CycleCountNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _MaxTestTimeNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _MaxTestTimeNumericLabel As Windows.Forms.Label
    Private WithEvents _ReadSetpointButton As Windows.Forms.Button
    Private WithEvents _ReadyCheckBox As Core.Controls.CheckBox
    Private WithEvents _NotAtTempCheckBox As Core.Controls.CheckBox
    Private WithEvents _AtTempCheckBox As Core.Controls.CheckBox
    Private WithEvents _HeadDownCheckBox As Core.Controls.CheckBox
    Private WithEvents _SetpointWindowNumericLabel As Windows.Forms.Label
    Private WithEvents _SetpointWindowNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _SetpointNumberNumericLabel As Windows.Forms.Label
    Private WithEvents _RampRateNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _SoakTimeNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _SetpointNumeric As Core.Controls.SelectorNumeric
    Private WithEvents _RampRateNumericLabel As Windows.Forms.Label
    Private WithEvents _SoakTimeNumericLabel As Windows.Forms.Label
    Private WithEvents _SetpointNumericLabel As Windows.Forms.Label
End Class
