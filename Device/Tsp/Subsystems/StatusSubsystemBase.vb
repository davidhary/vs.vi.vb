'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\StatusSubsystemBase.vb
'
' summary:	Status subsystem base class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EscapeSequencesExtensions

''' <summary> Defines a Status Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public MustInherit Class StatusSubsystemBase
    Inherits VI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="session"> A reference to a <see cref="Session">message based TSP session</see>. </param>
    Protected Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(VI.Pith.SessionBase.Validated(session), Tsp.Syntax.EventLog.NoErrorCompoundMessage)
        Me._VersionInfo = New VersionInfo
        StatusSubsystemBase.InitializeSession(session)
    End Sub

    #End Region

    #Region " SESSION "

    ''' <summary> Initializes the session. </summary>
    ''' <param name="session"> A reference to a <see cref="Session">message based TSP session</see>. </param>
    Private Shared Sub InitializeSession(ByVal session As VI.Pith.SessionBase)

        session.ClearExecutionStateCommand = Tsp.Syntax.Status.ClearExecutionStateCommand

        session.DeviceClearDelayPeriod = TimeSpan.FromMilliseconds(10)

        session.OperationCompleteCommand = Tsp.Syntax.Lua.OperationCompleteCommand
        session.OperationCompletedQueryCommand = Tsp.Syntax.Lua.OperationCompletedQueryCommand

        session.ResetKnownStateCommand = Tsp.Syntax.Lua.ResetKnownStateCommand

        session.ServiceRequestEnableCommandFormat = Tsp.Syntax.Status.ServiceRequestEnableCommandFormat

        ' session.ServiceRequestEnableQueryCommand = Tsp.Syntax.Status.ServiceRequestEnableQueryCommand
        session.ServiceRequestEnableQueryCommand = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand

        ' session.StandardEventStatusQueryCommand = Tsp.Syntax.Status.StandardEventStatusQueryCommand
        session.StandardEventStatusQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand

        ' session.StandardEventEnableQueryCommand = Tsp.Syntax.Status.StandardEventEnableQueryCommand
        session.StandardEventEnableQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand

        session.StandardServiceEnableCommandFormat = Tsp.Syntax.Status.StandardServiceEnableCommandFormat
        session.StandardServiceEnableCompleteCommandFormat = Tsp.Syntax.Status.StandardServiceEnableCompleteCommandFormat
        session.WaitCommand = Tsp.Syntax.Lua.WaitCommand
        ' session.WaitCommand = VI.Pith.Ieee488.Syntax.WaitCommand

        session.ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        session.StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
    End Sub

    #End Region

    #Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()

        Me.SerialNumber = New Long?
        Me.SerialNumberReading = String.Empty
        Dim activity As String = String.Empty

        MyBase.InitKnownState()

        Try
            activity = $"{Me.ResourceTitleCaption} storing communication timeout" : Me.PublishVerbose($"{activity};. ")
            Me.Session.StoreCommunicationTimeout(Me.InitializeTimeout)
            activity = $"{Me.ResourceTitleCaption} clearing error queue" : Me.PublishVerbose($"{activity};. ")
            ' clear the error queue on the controller node only.
            Me.ClearErrorQueue()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            activity = $"{Me.ResourceTitleCaption} restoring communication timeout" : Me.PublishVerbose($"{activity};. ")
            Me.Session.RestoreCommunicationTimeout()
        End Try

        Try
            activity = $"{Me.ResourceTitleCaption} discarding unread data #1"
            ' flush the input buffer in case the instrument has some leftovers.
            Me.Session.DiscardUnreadData()
            If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                Me.PublishInfo($"Done {activity};. Data: {Me.Session.DiscardedData}")
            End If
        Catch ex As VI.Pith.NativeException
            Me.PublishException(activity, ex)
        End Try

        Try
            ' flush write may cause the instrument to send off a new data.
            activity = $"{Me.ResourceTitleCaption} discarding unread data #2"
            Me.Session.DiscardUnreadData()
            If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                Me.PublishInfo($"Done {activity};. Data: {Me.Session.DiscardedData}")
            End If
        Catch ex As VI.Pith.NativeException
            Me.PublishException(activity, ex)
        End Try

        ' wait for operations to complete
        Me.Session.QueryOperationCompleted()

    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <remarks>
    ''' SCPI: ":STAT:PRES".
    ''' <see cref="F:isr.VI.Pith.Scpi.Syntax.ScpiSyntax.StatusPresetCommand"></see>
    ''' </remarks>
    ''' <value> The preset command. </value>
    Protected Overrides Property PresetCommand As String = String.Empty

    #End Region

    #Region " MEASUREMENT EVENTS "

    ''' <summary> Gets or sets the measurement status query command. </summary>
    ''' <value> The measurement status query command. </value>
    Protected Overrides Property MeasurementStatusQueryCommand As String = Tsp.Syntax.Status.MeasurementEventQueryCommand

    ''' <summary> Gets or sets the measurement event condition query command. </summary>
    ''' <value> The measurement event condition query command. </value>
    Protected Overrides Property MeasurementEventConditionQueryCommand As String = Tsp.Syntax.Status.MeasurementEventConditionQueryCommand

    #End Region

    #Region " OPERATION REGISTER EVENTS "

    ''' <summary> Gets or sets the operation event enable Query command. </summary>
    ''' <value> The operation event enable Query command. </value>
    Protected Overrides Property OperationEventEnableQueryCommand As String = Tsp.Syntax.Status.OperationEventEnableQueryCommand

    ''' <summary> Gets or sets the operation event enable command format. </summary>
    ''' <value> The operation event enable command format. </value>
    Protected Overrides Property OperationEventEnableCommandFormat As String = Tsp.Syntax.Status.OperationEventEnableCommandFormat

    ''' <summary> Gets or sets the operation event status query command. </summary>
    ''' <value> The operation event status query command. </value>
    Protected Overrides Property OperationEventStatusQueryCommand As String = Tsp.Syntax.Status.OperationEventQueryCommand

    ''' <summary> Programs the Operation register event enable bit mask. </summary>
    ''' <param name="value"> The bitmask. </param>
    ''' <returns> The mask to use for enabling the events; nothing if unknown. </returns>
    Public Overrides Function WriteOperationEventEnableBitmask(ByVal value As Integer) As Integer?
        If (value And OperationEventBits.UserRegister) <> 0 Then
            ' if enabling the user register, enable all events on the user register. 
            value = &H4FFF
        End If
        Return Me.WriteOperationEventEnableBitmask(value)
    End Function

#End Region

#Region " QUESTIONABLE REGISTER "

    ''' <summary> Gets or sets the questionable status query command. </summary>
    ''' <value> The questionable status query command. </value>
    Protected Overrides Property QuestionableStatusQueryCommand As String = VI.Pith.Scpi.Syntax.QuestionableEventQueryCommand

    #End Region

    #Region " LINE FREQUENCY "

    ''' <summary> Gets or sets line frequency query command. </summary>
    ''' <value> The line frequency query command. </value>
    Protected Overrides Property LineFrequencyQueryCommand As String = Tsp.Syntax.LocalNode.LineFrequencyQueryCommand

    #End Region

    #Region " IDENTITY "

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = Tsp.Syntax.LocalNode.IdentityQueryCommand

    ''' <summary> Gets or sets the serial number query command. </summary>
    ''' <value> The serial number query command. </value>
    Protected Overrides Property SerialNumberQueryCommand As String = Tsp.Syntax.LocalNode.SerialNumberFormattedQueryCommand

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
    ''' <returns> System.String. </returns>
    Public Overrides Function QueryIdentity() As String
        If Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.PublishVerbose("Requesting identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Me.WriteIdentityQueryCommand()
            Me.PublishVerbose("Trying to read identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Dim value As String = Me.Session.ReadLineTrimEnd
            value = value.ReplaceCommonEscapeSequences.Trim
            Me.PublishVerbose($"Setting identity to {value};. ")
            Me.VersionInfo.Parse(value)
            MyBase.VersionInfoBase = Me.VersionInfo
            Me.Identity = Me.VersionInfo.Identity
        End If
        Return Me.Identity
    End Function

    ''' <summary> Gets or sets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public ReadOnly Property VersionInfo As VersionInfo

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String = String.Empty 'VI.Pith.Scpi.Syntax.LastSystemErrorQueryCommand

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = Tsp.Syntax.ErrorQueue.ClearErrorQueueCommand

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = Tsp.Syntax.ErrorQueue.ErrorQueueQueryCommand

    ''' <summary> Gets or sets the 'Next Error' query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property DequeueErrorQueryCommand As String = String.Empty ' VI.Pith.Scpi.Syntax.LastSystemErrorQueryCommand

    ''' <summary> Queue device error using the TSP device error class. </summary>
    ''' <param name="compoundErrorMessage"> Message describing the compound error. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected Overrides Function EnqueueDeviceError(ByVal compoundErrorMessage As String) As VI.DeviceError
        Dim de As TspDeviceError = New TspDeviceError
        de.Parse(compoundErrorMessage)
        If de.IsError Then Me.DeviceErrorQueue.Enqueue(de)
        Return de
    End Function

    #End Region

    #Region " COLLECT GARBAGE "

    ''' <summary> Gets or sets the collect garbage wait complete command. </summary>
    ''' <value> The collect garbage wait complete command. </value>
    Protected Overrides Property CollectGarbageWaitCompleteCommand As String = Tsp.Syntax.Lua.CollectGarbageWaitCompleteCommand

    #End Region

End Class

''' <summary> Enumerates the status bits for the operations register. </summary>
<Flags()> Public Enum OperationEventBits

    ''' <summary>Empty.</summary>
    <ComponentModel.Description("Empty")>
    None = 0

    ''' <summary>Calibrating.</summary>
    <ComponentModel.Description("Calibrating")>
    Calibrating = &H1

    ''' <summary>Measuring.</summary>
    <ComponentModel.Description("Measuring")>
    Measuring = &H10

    ''' <summary>Prompts enabled.</summary>
    <ComponentModel.Description("Prompts Enabled")>
    Prompts = &H800

    ''' <summary>User Register.</summary>
    <ComponentModel.Description("User Register")>
    UserRegister = &H1000

    ''' <summary>User Register.</summary>
    <ComponentModel.Description("Instrument summary")>
    InstrumentSummary = &H2000

    ''' <summary>Program running.</summary>
    <ComponentModel.Description("Program Running")>
    ProgramRunning = &H4000

    ''' <summary>Unknown value. Sets bit 16 (zero based and beyond the register size).</summary>
    <ComponentModel.Description("Unknown")>
    Unknown = &H10000

End Enum

