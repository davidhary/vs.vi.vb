''' <summary> K2002 Device resource only unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2002")>
Partial Public Class ResourceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(K2002Tests.ResourceSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " RESOURCE "

    ''' <summary> (Unit Test Method) tests visa resource. </summary>
    ''' <remarks> Finds the resource using the session factory resources manager. </remarks>
    <TestMethod()>
    Public Sub VisaResourceTest()
        isr.VI.DeviceTests.DeviceManager.AssertVisaResourceManagerIncludesResource(ResourceSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) tests device resource. </summary>
    <TestMethod()>
    Public Sub DeviceResourceTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            isr.VI.DeviceTests.DeviceManager.AssertResourceFound(device, ResourceSettings.Get)
        End Using
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> (Unit Test Method) tests Visa Session talker. </summary>
    ''' <remarks> Checks if the device adds a trace message to a listener. </remarks>
    <TestMethod()>
    Public Sub TalkerTest()
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            isr.VI.DeviceTests.DeviceManager.AssertDeviceTalks(TestInfo, session)
        End Using
    End Sub

#End Region

#Region " SESSION TESTS: OPEN/CLOSE, STATUS "

    ''' <summary> (Unit Test Method) tests session base. </summary>
    <TestMethod()>
    Public Sub SessionBaseTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.OpenSessionBase(TestInfo, device, ResourceSettings.Get)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseSessionBase(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> Queries operation completion. </summary>
    ''' <param name="len">             The length. </param>
    ''' <param name="previousCommand"> The previous command. </param>
    ''' <param name="sw">              The software. </param>
    ''' <param name="session">         The session. </param>
    Private Shared Sub QueryOperationCompletion(ByVal len As Integer, ByVal previousCommand As String, ByVal sw As Stopwatch, ByVal session As Pith.SessionBase)
        Dim command As String = "*OPC?"
        Dim expectedOperationCompleteValue As String = "1"
        Dim actualOperationCompleteValue As String = session.QueryTrimEnd(command)
        Assert.AreEqual(expectedOperationCompleteValue, actualOperationCompleteValue, $"Expected operation completion after {previousCommand}")
        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
    End Sub

    ''' <summary> Queries measurement enabled. </summary>
    ''' <param name="len">     The length. </param>
    ''' <param name="sw">      The software. </param>
    ''' <param name="session"> The session. </param>
    Private Shared Sub QueryMeasurementEnabled(ByVal len As Integer, ByVal sw As Stopwatch, ByVal session As Pith.SessionBase)
        TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} MEAS: Enable={session.QueryTrimEnd(":stat:meas:ENAB?")}; PTR={session.QueryTrimEnd(":stat:meas:PTR?")}. NTR: {session.QueryTrimEnd(":stat:meas:NTR?")}")
        Dim Command As String = ":stat:meas:ENAB?; :stat:meas:PTR?; :stat:meas:NTR?"
        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {Command}") : sw.Restart()
    End Sub

    ''' <summary> Queries measurement event. </summary>
    ''' <param name="len">     The length. </param>
    ''' <param name="sw">      The software. </param>
    ''' <param name="session"> The session. </param>
    Private Shared Sub QueryMeasurementEvent(ByVal len As Integer, ByVal sw As Stopwatch, ByVal session As Pith.SessionBase)
        Dim command As String = ":stat:meas:even?; :stat:meas:cond?;"
        TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} MEAS: Event={session.QueryTrimEnd(":stat:meas:even?")}. Cond={session.QueryTrimEnd(":stat:meas:cond?")}")
        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
    End Sub

    ''' <summary> Queries measurement condition. </summary>
    ''' <param name="len">     The length. </param>
    ''' <param name="sw">      The software. </param>
    ''' <param name="session"> The session. </param>
    Private Shared Sub QueryMeasurementCondition(ByVal len As Integer, ByVal sw As Stopwatch, ByVal session As Pith.SessionBase)
        Dim command As String = ":stat:meas:cond?"
        TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} MEAS: Condition={session.QueryTrimEnd(command)}.")
        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
    End Sub

    ''' <summary> (Unit Test Method) tests session base measure. </summary>
    ''' <remarks>
    ''' <list type=" bullet">2020-01-09 w/o OPC <item>
    ''' MS: 823: Command: *RST                          </item><item>
    ''' MS: 918: Command: :STAT:PRESET                  </item><item>
    ''' MS: 919: MEAS: Enable=32767; PTR=32767. NTR: 0  </item><item>
    ''' MS: 996: MEAS: Event=32.                        </item><item>
    ''' MS: 1083: Command: ABORT                        </item><item>
    ''' MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
    ''' MEAS: Event=0.                                  </item><item>
    ''' MS: 1145: Pre-iNIT                              </item><item>
    ''' MS: 1255: Command: INIT                         </item><item>
    ''' MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
    ''' MS: 1306: Command: FETCH?                       </item><item>
    ''' MEAS: Event=32. READING: +0.00400E-03           </item><item>
    ''' MEAS: Event=0.                                  </item><item>
    ''' MS: 1391: Pre-iNIT                              </item><item>
    ''' MS: 1496: Command: INIT                         </item><item>
    ''' MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
    ''' MS: 1543: Command: FETCH?                       </item><item>
    ''' MEAS: Event=32. READING: +0.00393E-03           </item><item>
    ''' MEAS: Event=0.                                  </item><item>
    ''' MS: 1624: Command: :CALC3:LIM:LOW 1; :CALC3:LIM:STAT ON</item><item>
    ''' CALC3: STAT=1; LOW: 1.000000E+00                </item><item>
    ''' MS: 1713: Pre-iNIT                              </item><item>
    ''' MS: 1817: Command: INIT                         </item><item>
    ''' MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
    ''' MS: 1860: Command: FETCH?                       </item><item>
    ''' MEAS: Event=34. READING: +0.00383E-03           </item><item>
    ''' MEAS: Event=0.                                  </item><item>
    ''' </item></list>
    ''' 
    ''' <list type=" bullet">2020-01-09 w OPC <item>
    ''' MS:   633: Open Session                                   </item><item>
    ''' MS:     7: Command: *RST                                  </item><item>
    ''' MS:    90: Command: *OPC?                                 </item><item>
    ''' MS:     9: Command: :STAT:PRESET                          </item><item>
    ''' MS:    19: Command: *OPC?                                 </item><item>
    '''                MEAS: Enable=32767; PTR=32767. NTR: 0      </item><item>
    ''' MS:    48: Command: :stat:meas:ENAB?; :stat:meas:PTR?; :stat:meas:NTR? </item><item>
    '''                MEAS: Event=0. Cond=0                      </item><item>
    ''' MS:    29: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' MS:     4: Command: *CLS                                  </item><item>
    ''' MS:    13: Command: *OPC?                                 </item><item>
    ''' MS:     4: Command: :ABOR                                 </item><item>
    ''' MS:    20: Command: *OPC?                                 </item><item>
    '''                MEAS: Enable=32767; PTR=32767. NTR: 0      </item><item>
    ''' MS:    46: Command: :stat:meas:ENAB?; :stat:meas:PTR?; :stat:meas:NTR? </item><item>
    '''                MEAS: Event=0. Cond=0                      </item><item>
    ''' MS:    31: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' MS:     4: Command: :INIT                                 </item><item>
    ''' MS:   164: MEAS: Event=32                                 </item><item>
    '''                MEAS: Event=0. Cond=0. READING: +0.00029E-03            </item><item>
    ''' MS:    48: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
    '''                MEAS: Event=0. Cond=0                      </item><item>
    ''' MS:    29: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' MS:     3: Command: :INIT                                 </item><item>
    ''' MS:    99: MEAS: Event=32                                 </item><item>
    '''                MEAS: Event=0. Cond=0. READING: +0.00047E-03            </item><item>
    ''' MS:    48: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
    '''                MEAS: Event=0. Cond=0                      </item><item>
    ''' MS:    31: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' MS:    11: Command: :CALC3:LIM:LOW 1; :CALC3:LIM:STAT ON  </item><item>
    ''' MS:    24: Command: *OPC?                                 </item><item>
    '''                CALC3: STAT=1; LOW: 1.000000E+00           </item><item>
    ''' MS:    34: Command: :CALC3:LIM:STAT?; :CALC3:LIM:LOW?     </item><item>
    ''' MS:     3: Command: :INIT                                 </item><item>
    ''' MS:   101: MEAS: Event=34                                 </item><item>
    '''                MEAS: Event=0. Cond=2. READING: +0.00080E-03            </item><item>
    ''' MS:    44: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
    '''                MEAS: Event=0. Cond=2                      </item><item>
    ''' MS:    32: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' MS:     5: Command: :CALC3:LIM:STAT OFF                   </item><item>
    ''' MS:    17: Command: *OPC?                                 </item><item>
    '''                CALC3: STAT=0                              </item><item>
    ''' MS:    14: Command: :CALC3:LIM:STAT?                      </item><item>
    ''' MS:     4: Command: :INIT                                 </item><item>
    ''' MS:    95: MEAS: Event=32                                 </item><item>
    '''                MEAS: Event=0. Cond=0. READING: +0.00088E-03            </item><item>
    ''' MS:    47: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
    '''                MEAS: Event=0. Cond=0                      </item><item>
    ''' MS:    29: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' MS:     3: Command: :INIT                                 </item><item>
    ''' MS:    95: MEAS: Event=32                                 </item><item>
    '''                MEAS: Event=0. Cond=0. READING: +0.00096E-03      </item><item>
    ''' MS:    46: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?  </item><item>
    '''                MEAS: Event=0. Cond=0                      </item><item>
    ''' MS:    28: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
    ''' </item></list>
    ''' </remarks>
    <TestMethod()>
    Public Sub SessionBaseMeasureTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Dim sw As Stopwatch = Stopwatch.StartNew
            Dim len As Integer = 5
            Try
                isr.VI.DeviceTests.DeviceManager.OpenSessionBase(TestInfo, device, ResourceSettings.Get)
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Open Session") : sw.Restart()
                ' MEAS (POWER ON): Enable=0; PTR=32767. NTR: 0
                ' MEAS (PRESET): Enable=32767; PTR=32767. NTR: 0
                Dim command As String = "*RST" : device.Session.WriteLine(command)
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()

                ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)

                command = ":STAT:PRESET" : device.Session.WriteLine(command)
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()

                ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)

                ResourceTests.QueryMeasurementEnabled(len, sw, device.Session)

                ResourceTests.QueryMeasurementEvent(len, sw, device.Session)

                command = "*CLS" : device.Session.WriteLine(command)
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()

                ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)

                command = ":CALC3:LIM:CLE:AUTO ON" : device.Session.WriteLine(command)
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)
                command = ":CALC3:LIM:CLE:AUTO?"
                TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} CALC3: AUTO={device.Session.QueryTrimEnd(command)}")
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()

                command = ":ABOR" : device.Session.WriteLine(command)
                TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()

                ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)
                ResourceTests.QueryMeasurementEnabled(len, sw, device.Session)
                ResourceTests.QueryMeasurementEvent(len, sw, device.Session)

                Dim measEvent As Integer?
                Dim timedOut As Boolean
                For i As Integer = 1 To 5

                    If i = 1 Then
                        command = ":CALC3:LIM:STAT OFF" : device.Session.WriteLine(command)
                        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                        ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)
                        command = ":CALC3:LIM:STAT?"
                        TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} CALC3: STAT={device.Session.QueryTrimEnd(":CALC3:LIM:STAT?")}")
                        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                    End If

                    command = ":INIT" : device.Session.WriteLine(command)
                    TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                    Dim task As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As Integer)) = Pith.SessionBase.StartAwaitingBitmaskTask(32, TimeSpan.FromMilliseconds(400),
                                                                                                                                               Function() Convert.ToInt32(device.Session.QueryTrimEnd(":stat:meas:even?")))
                    task.Wait()
                    measEvent = task.Result.Status
                    timedOut = task.Result.TimedOut

                    TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: MEAS: Event={measEvent}") : sw.Restart()
                    Assert.IsFalse(timedOut, "Measurement timed out")
                    sw.Restart()
                    command = ":stat:meas:even?; :stat:meas:cond?; :FETCH?"
                    TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} MEAS: Event={device.Session.QueryTrimEnd(":stat:meas:even?")}. Cond={device.Session.QueryTrimEnd(":stat:meas:cond?")}. READING: {device.Session.QueryTrimEnd(":FETCH?")}")
                    TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()

                    ResourceTests.QueryMeasurementEvent(len, sw, device.Session)

                    If i = 2 Then
                        command = ":CALC3:LIM:LOW 1; :CALC3:LIM:STAT ON" : device.Session.WriteLine(command)
                        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                        ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)
                        command = ":CALC3:LIM:STAT?; :CALC3:LIM:LOW?"
                        TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} CALC3: STAT={device.Session.QueryTrimEnd(":CALC3:LIM:STAT?")}; LOW: {device.Session.QueryTrimEnd(":CALC3:LIM:LOW?")}")
                        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                    ElseIf i = 3 Then
                        command = ":CALC3:LIM:STAT OFF" : device.Session.WriteLine(command)
                        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                        ResourceTests.QueryOperationCompletion(len, command, sw, device.Session)
                        command = ":CALC3:LIM:STAT?"
                        TestInfo.TraceMessage($"{" ".PadLeft(3 * len, " "c)} CALC3: STAT={device.Session.QueryTrimEnd(":CALC3:LIM:STAT?")}")
                        TestInfo.TraceMessage($"MS: {sw.ElapsedMilliseconds.ToString.PadLeft(len, " "c)}: Command: {command}") : sw.Restart()
                    End If


                Next

            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.CloseSessionBase(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> Gets the function. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <returns> A Func(Of T) </returns>
    Private Function Func(Of T)() As Func(Of T)
        Throw New NotImplementedException()
    End Function

#End Region

#Region " DEVICE TESTS: OPEN, CLOSE, CHECK SUSBSYSTEMS "

    ''' <summary> (Unit Test Method) tests open session. </summary>
    ''' <remarks> Tests opening and closing a VISA session. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " DEVICE EVENT TESTS "

    ''' <summary> (Unit Test Method) handles the service request test. </summary>
    <TestMethod()>
    Public Sub HandleServiceRequestTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertHandlingServiceRequest(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests device handle service request. </summary>
    ''' <remarks>
    ''' This test will fail the first time it is run if Windows requests access through the Firewall.
    ''' </remarks>
    <TestMethod()>
    Public Sub DeviceHandleServiceRequestTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceHandlingServiceRequest(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests device polling. </summary>
    <TestMethod()>
    Public Sub DevicePollingTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K2002.K2002Device = VI.K2002.K2002Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertDevicePolling(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class
