<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DisplayView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._MeasureStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._TerminalsStatusLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._MeasureMetaStatusLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ReadingAmountLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._MeasureElapsedTimeLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._StandardRegisterLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._StatusRegisterLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._SessionReadingStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._LastReadingLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._SessionElapsedTimeLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._SubsystemStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._SubsystemReadingLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._SubsystemElapsedTimeLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._TitleStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._TitleLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._SessionOpenCloseStatusLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ErrorStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._ErrorLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._ServiceRequestEnableBitmaskLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._StandardEventEnableBitmaskLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ServiceRequestEnabledLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._OperationSummaryBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ServiceRequestBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._EventSummaryBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._MessageAvailableBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._QuestionableSummaryBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ErrorAvailableBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._SystemEventBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._MeasurementEventBitLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._PowerOnEventLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._UserRequestEventLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._CommandErrorEventLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ExecutionErrorEventLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._DeviceDependentErrorEventToolLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._QueryErrorEventLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._OperationCompleteEventLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._ServiceRequestHandledLabel = New isr.Core.Controls.ToolStripStatusLabel()
        Me._MeasurementEventStatusLabel = New isr.Core.Controls.ToolStripStatusLabel
        Me._MeasureStatusStrip.SuspendLayout()
        Me._Layout.SuspendLayout()
        Me._SessionReadingStatusStrip.SuspendLayout()
        Me._SubsystemStatusStrip.SuspendLayout()
        Me._TitleStatusStrip.SuspendLayout()
        Me._ErrorStatusStrip.SuspendLayout()
        Me._StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_MeasureStatusStrip
        '
        Me._MeasureStatusStrip.BackColor = System.Drawing.Color.Transparent
        Me._MeasureStatusStrip.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasureStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._MeasureStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TerminalsStatusLabel, Me._MeasureMetaStatusLabel, Me._ReadingAmountLabel, Me._MeasureElapsedTimeLabel})
        Me._MeasureStatusStrip.Location = New System.Drawing.Point(0, 22)
        Me._MeasureStatusStrip.Name = "_MeasureStatusStrip"
        Me._MeasureStatusStrip.ShowItemToolTips = True
        Me._MeasureStatusStrip.Size = New System.Drawing.Size(441, 42)
        Me._MeasureStatusStrip.SizingGrip = False
        Me._MeasureStatusStrip.TabIndex = 18
        '
        '_TerminalsStatusLabel
        '
        Me._TerminalsStatusLabel.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TerminalsStatusLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._TerminalsStatusLabel.Name = "_TerminalsStatusLabel"
        Me._TerminalsStatusLabel.Size = New System.Drawing.Size(30, 37)
        Me._TerminalsStatusLabel.Text = "F"
        Me._TerminalsStatusLabel.ToolTipText = "Terminals status"
        '
        '_MeasureMetaStatusLabel
        '
        Me._MeasureMetaStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._MeasureMetaStatusLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasureMetaStatusLabel.ForeColor = System.Drawing.Color.LimeGreen
        Me._MeasureMetaStatusLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._MeasureMetaStatusLabel.Name = "_MeasureMetaStatusLabel"
        Me._MeasureMetaStatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._MeasureMetaStatusLabel.Size = New System.Drawing.Size(16, 42)
        Me._MeasureMetaStatusLabel.Text = "p"
        Me._MeasureMetaStatusLabel.ToolTipText = "Measure meta status"
        '
        '_ReadingAmountLabel
        '
        Me._ReadingAmountLabel.AutoSize = False
        Me._ReadingAmountLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadingAmountLabel.ForeColor = System.Drawing.Color.Aquamarine
        Me._ReadingAmountLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._ReadingAmountLabel.Name = "_ReadingAmountLabel"
        Me._ReadingAmountLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._ReadingAmountLabel.Size = New System.Drawing.Size(357, 42)
        Me._ReadingAmountLabel.Spring = True
        Me._ReadingAmountLabel.Text = "-.------- mV"
        Me._ReadingAmountLabel.ToolTipText = "Reading value and unit"
        '
        '_MeasureElapsedTimeLabel
        '
        Me._MeasureElapsedTimeLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._MeasureElapsedTimeLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasureElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._MeasureElapsedTimeLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._MeasureElapsedTimeLabel.Name = "_MeasureElapsedTimeLabel"
        Me._MeasureElapsedTimeLabel.Size = New System.Drawing.Size(23, 42)
        Me._MeasureElapsedTimeLabel.Text = "ms"
        Me._MeasureElapsedTimeLabel.ToolTipText = "Measure last action time in ms"
        '
        '_StandardRegisterLabel
        '
        Me._StandardRegisterLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StandardRegisterLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StandardRegisterLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._StandardRegisterLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._StandardRegisterLabel.Name = "_StandardRegisterLabel"
        Me._StandardRegisterLabel.Size = New System.Drawing.Size(19, 22)
        Me._StandardRegisterLabel.Text = "0x.."
        Me._StandardRegisterLabel.ToolTipText = "Standard Register Value"
        Me._StandardRegisterLabel.Visible = False
        '
        '_StatusRegisterLabel
        '
        Me._StatusRegisterLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StatusRegisterLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatusRegisterLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._StatusRegisterLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._StatusRegisterLabel.Name = "_StatusRegisterLabel"
        Me._StatusRegisterLabel.Size = New System.Drawing.Size(19, 22)
        Me._StatusRegisterLabel.Text = "0x.."
        Me._StatusRegisterLabel.ToolTipText = "Status Register Value"
        '
        '_Layout
        '
        Me._Layout.AutoSize = True
        Me._Layout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._Layout.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(44, Byte), Integer))
        Me._Layout.ColumnCount = 1
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Controls.Add(Me._SessionReadingStatusStrip, 0, 2)
        Me._Layout.Controls.Add(Me._MeasureStatusStrip, 0, 1)
        Me._Layout.Controls.Add(Me._SubsystemStatusStrip, 0, 3)
        Me._Layout.Controls.Add(Me._TitleStatusStrip, 0, 0)
        Me._Layout.Controls.Add(Me._ErrorStatusStrip, 0, 4)
        Me._Layout.Controls.Add(Me._StatusStrip, 0, 5)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Top
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Margin = New System.Windows.Forms.Padding(0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 6
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.Size = New System.Drawing.Size(441, 152)
        Me._Layout.TabIndex = 23
        '
        '_SessionReadingStatusStrip
        '
        Me._SessionReadingStatusStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(44, Byte), Integer))
        Me._SessionReadingStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SessionReadingStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._MeasurementEventStatusLabel, Me._LastReadingLabel, Me._SessionElapsedTimeLabel})
        Me._SessionReadingStatusStrip.Location = New System.Drawing.Point(0, 64)
        Me._SessionReadingStatusStrip.Name = "_SessionReadingStatusStrip"
        Me._SessionReadingStatusStrip.ShowItemToolTips = True
        Me._SessionReadingStatusStrip.Size = New System.Drawing.Size(441, 22)
        Me._SessionReadingStatusStrip.SizingGrip = False
        Me._SessionReadingStatusStrip.TabIndex = 0
        Me._SessionReadingStatusStrip.Text = "Session Status Strip"
        '
        '_LastReadingLabel
        '
        Me._LastReadingLabel.ForeColor = System.Drawing.Color.Aquamarine
        Me._LastReadingLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._LastReadingLabel.Name = "_LastReadingLabel"
        Me._LastReadingLabel.Size = New System.Drawing.Size(343, 22)
        Me._LastReadingLabel.Spring = True
        Me._LastReadingLabel.Text = "last reading"
        Me._LastReadingLabel.ToolTipText = "Last reading"
        '
        '_SessionElapsedTimeLabel
        '
        Me._SessionElapsedTimeLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SessionElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._SessionElapsedTimeLabel.Name = "_SessionElapsedTimeLabel"
        Me._SessionElapsedTimeLabel.Size = New System.Drawing.Size(23, 17)
        Me._SessionElapsedTimeLabel.Text = "ms"
        Me._SessionElapsedTimeLabel.ToolTipText = "Session last action time in ms"
        '
        '_SubsystemStatusStrip
        '
        Me._SubsystemStatusStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(44, Byte), Integer))
        Me._SubsystemStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemReadingLabel, Me._SubsystemElapsedTimeLabel})
        Me._SubsystemStatusStrip.Location = New System.Drawing.Point(0, 86)
        Me._SubsystemStatusStrip.Name = "_SubsystemStatusStrip"
        Me._SubsystemStatusStrip.ShowItemToolTips = True
        Me._SubsystemStatusStrip.Size = New System.Drawing.Size(441, 22)
        Me._SubsystemStatusStrip.SizingGrip = False
        Me._SubsystemStatusStrip.TabIndex = 19
        Me._SubsystemStatusStrip.Text = "Subsystem Status Strip"
        '
        '_SubsystemReadingLabel
        '
        Me._SubsystemReadingLabel.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemReadingLabel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemReadingLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._SubsystemReadingLabel.Name = "_SubsystemReadingLabel"
        Me._SubsystemReadingLabel.Size = New System.Drawing.Size(403, 17)
        Me._SubsystemReadingLabel.Spring = True
        Me._SubsystemReadingLabel.Text = "-.-------- mA"
        Me._SubsystemReadingLabel.ToolTipText = "Subsystem reading"
        '
        '_SubsystemElapsedTimeLabel
        '
        Me._SubsystemElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._SubsystemElapsedTimeLabel.Name = "_SubsystemElapsedTimeLabel"
        Me._SubsystemElapsedTimeLabel.Size = New System.Drawing.Size(23, 17)
        Me._SubsystemElapsedTimeLabel.Text = "ms"
        Me._SubsystemElapsedTimeLabel.ToolTipText = "Subsystem last action time in ms"
        '
        '_TitleStatusStrip
        '
        Me._TitleStatusStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(44, Byte), Integer))
        Me._TitleStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TitleStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TitleLabel, Me._SessionOpenCloseStatusLabel})
        Me._TitleStatusStrip.Location = New System.Drawing.Point(0, 0)
        Me._TitleStatusStrip.Name = "_TitleStatusStrip"
        Me._TitleStatusStrip.ShowItemToolTips = True
        Me._TitleStatusStrip.Size = New System.Drawing.Size(441, 22)
        Me._TitleStatusStrip.SizingGrip = False
        Me._TitleStatusStrip.TabIndex = 20
        Me._TitleStatusStrip.Text = "Title Status Strip"
        '
        '_TitleLabel
        '
        Me._TitleLabel.BackColor = System.Drawing.Color.Transparent
        Me._TitleLabel.ForeColor = System.Drawing.SystemColors.Info
        Me._TitleLabel.Name = "_TitleLabel"
        Me._TitleLabel.Size = New System.Drawing.Size(426, 17)
        Me._TitleLabel.Spring = True
        Me._TitleLabel.Text = "Multimeter"
        Me._TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._TitleLabel.ToolTipText = "Device title"
        '
        '_SessionOpenCloseStatusLabel
        '
        Me._SessionOpenCloseStatusLabel.Name = "_SessionOpenCloseStatusLabel"
        Me._SessionOpenCloseStatusLabel.Size = New System.Drawing.Size(0, 17)
        Me._SessionOpenCloseStatusLabel.ToolTipText = "Session open status"
        '
        '_ErrorStatusStrip
        '
        Me._ErrorStatusStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(44, Byte), Integer))
        Me._ErrorStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ErrorStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ErrorLabel})
        Me._ErrorStatusStrip.Location = New System.Drawing.Point(0, 108)
        Me._ErrorStatusStrip.Name = "_ErrorStatusStrip"
        Me._ErrorStatusStrip.ShowItemToolTips = True
        Me._ErrorStatusStrip.Size = New System.Drawing.Size(441, 22)
        Me._ErrorStatusStrip.SizingGrip = False
        Me._ErrorStatusStrip.TabIndex = 21
        Me._ErrorStatusStrip.Text = "Error Status Strip"
        '
        '_ErrorLabel
        '
        Me._ErrorLabel.BackColor = System.Drawing.Color.Transparent
        Me._ErrorLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ErrorLabel.ForeColor = System.Drawing.Color.LimeGreen
        Me._ErrorLabel.Name = "_ErrorLabel"
        Me._ErrorLabel.Size = New System.Drawing.Size(426, 17)
        Me._ErrorLabel.Spring = True
        Me._ErrorLabel.Text = "000, No Errors"
        Me._ErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_StatusStrip
        '
        Me._StatusStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(44, Byte), Integer))
        Me._StatusStrip.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServiceRequestEnableBitmaskLabel, Me._StandardEventEnableBitmaskLabel, Me._ServiceRequestEnabledLabel, Me._StatusRegisterLabel, Me._OperationSummaryBitLabel, Me._ServiceRequestBitLabel, Me._EventSummaryBitLabel, Me._MessageAvailableBitLabel, Me._QuestionableSummaryBitLabel, Me._ErrorAvailableBitLabel, Me._SystemEventBitLabel, Me._MeasurementEventBitLabel, Me._StandardRegisterLabel, Me._PowerOnEventLabel, Me._UserRequestEventLabel, Me._CommandErrorEventLabel, Me._ExecutionErrorEventLabel, Me._DeviceDependentErrorEventToolLabel, Me._QueryErrorEventLabel, Me._OperationCompleteEventLabel, Me._ServiceRequestHandledLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 130)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.ShowItemToolTips = True
        Me._StatusStrip.Size = New System.Drawing.Size(441, 22)
        Me._StatusStrip.SizingGrip = False
        Me._StatusStrip.TabIndex = 22
        Me._StatusStrip.Text = "Status Strip"
        '
        '_ServiceRequestEnableBitmaskLabel
        '
        Me._ServiceRequestEnableBitmaskLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._ServiceRequestEnableBitmaskLabel.Name = "_ServiceRequestEnableBitmaskLabel"
        Me._ServiceRequestEnableBitmaskLabel.Size = New System.Drawing.Size(19, 17)
        Me._ServiceRequestEnableBitmaskLabel.Text = "0x.."
        Me._ServiceRequestEnableBitmaskLabel.ToolTipText = "Service Request Enable Bitmask"
        '
        '_StandardEventEnableBitmaskLabel
        '
        Me._StandardEventEnableBitmaskLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._StandardEventEnableBitmaskLabel.Name = "_StandardEventEnableBitmaskLabel"
        Me._StandardEventEnableBitmaskLabel.Size = New System.Drawing.Size(19, 17)
        Me._StandardEventEnableBitmaskLabel.Text = "0x.."
        Me._StandardEventEnableBitmaskLabel.ToolTipText = "Standard Event Enable Bitmask"
        '
        '_ServiceRequestEnabledLabel
        '
        Me._ServiceRequestEnabledLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ServiceRequestEnabledLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ServiceRequestEnabledLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._ServiceRequestEnabledLabel.Image = Global.isr.VI.Facade.My.Resources.Resources.lightning_16
        Me._ServiceRequestEnabledLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ServiceRequestEnabledLabel.Name = "_ServiceRequestEnabledLabel"
        Me._ServiceRequestEnabledLabel.Size = New System.Drawing.Size(16, 17)
        Me._ServiceRequestEnabledLabel.Text = "SRE"
        Me._ServiceRequestEnabledLabel.ToolTipText = "Service request is enabled"
        Me._ServiceRequestEnabledLabel.Visible = False
        '
        '_OperationSummaryBitLabel
        '
        Me._OperationSummaryBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OperationSummaryBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._OperationSummaryBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._OperationSummaryBitLabel.Name = "_OperationSummaryBitLabel"
        Me._OperationSummaryBitLabel.Size = New System.Drawing.Size(21, 22)
        Me._OperationSummaryBitLabel.Text = "osb"
        Me._OperationSummaryBitLabel.ToolTipText = " Operation Summary Bit (OSB) is on"
        Me._OperationSummaryBitLabel.Visible = False
        '
        '_ServiceRequestBitLabel
        '
        Me._ServiceRequestBitLabel.BackColor = System.Drawing.Color.Transparent
        Me._ServiceRequestBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ServiceRequestBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._ServiceRequestBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._ServiceRequestBitLabel.Name = "_ServiceRequestBitLabel"
        Me._ServiceRequestBitLabel.Size = New System.Drawing.Size(19, 22)
        Me._ServiceRequestBitLabel.Text = "rqs"
        Me._ServiceRequestBitLabel.ToolTipText = "Request for Service (RQS) bit or the Master Summary Status Bit (MSB) bit is on"
        Me._ServiceRequestBitLabel.Visible = False
        '
        '_EventSummaryBitLabel
        '
        Me._EventSummaryBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EventSummaryBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._EventSummaryBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._EventSummaryBitLabel.Name = "_EventSummaryBitLabel"
        Me._EventSummaryBitLabel.Size = New System.Drawing.Size(20, 22)
        Me._EventSummaryBitLabel.Text = "esb"
        Me._EventSummaryBitLabel.ToolTipText = "Event Summary Bit (ESB) is on"
        Me._EventSummaryBitLabel.Visible = False
        '
        '_MessageAvailableBitLabel
        '
        Me._MessageAvailableBitLabel.BackColor = System.Drawing.Color.Transparent
        Me._MessageAvailableBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MessageAvailableBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._MessageAvailableBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._MessageAvailableBitLabel.Name = "_MessageAvailableBitLabel"
        Me._MessageAvailableBitLabel.Size = New System.Drawing.Size(23, 22)
        Me._MessageAvailableBitLabel.Text = "mav"
        Me._MessageAvailableBitLabel.ToolTipText = "Message Available Bit (MAV) is on"
        Me._MessageAvailableBitLabel.Visible = False
        '
        '_QuestionableSummaryBitLabel
        '
        Me._QuestionableSummaryBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._QuestionableSummaryBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._QuestionableSummaryBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._QuestionableSummaryBitLabel.Name = "_QuestionableSummaryBitLabel"
        Me._QuestionableSummaryBitLabel.Size = New System.Drawing.Size(21, 22)
        Me._QuestionableSummaryBitLabel.Text = "qsb"
        Me._QuestionableSummaryBitLabel.ToolTipText = "Questionable Summary Bit (QSB) is on"
        Me._QuestionableSummaryBitLabel.Visible = False
        '
        '_ErrorAvailableBitLabel
        '
        Me._ErrorAvailableBitLabel.BackColor = System.Drawing.Color.Transparent
        Me._ErrorAvailableBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ErrorAvailableBitLabel.ForeColor = System.Drawing.Color.Red
        Me._ErrorAvailableBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._ErrorAvailableBitLabel.Name = "_ErrorAvailableBitLabel"
        Me._ErrorAvailableBitLabel.Size = New System.Drawing.Size(20, 22)
        Me._ErrorAvailableBitLabel.Text = "Eav"
        Me._ErrorAvailableBitLabel.ToolTipText = "Error Available Bit (EAV) is on"
        Me._ErrorAvailableBitLabel.Visible = False
        '
        '_SystemEventBitLabel
        '
        Me._SystemEventBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SystemEventBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._SystemEventBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._SystemEventBitLabel.Name = "_SystemEventBitLabel"
        Me._SystemEventBitLabel.Size = New System.Drawing.Size(19, 22)
        Me._SystemEventBitLabel.Text = "ssb"
        Me._SystemEventBitLabel.ToolTipText = "System Event Bit (SSB) is on"
        Me._SystemEventBitLabel.Visible = False
        '
        '_MeasurementEventBitLabel
        '
        Me._MeasurementEventBitLabel.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasurementEventBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._MeasurementEventBitLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._MeasurementEventBitLabel.Name = "_MeasurementEventBitLabel"
        Me._MeasurementEventBitLabel.Size = New System.Drawing.Size(23, 22)
        Me._MeasurementEventBitLabel.Text = "msb"
        Me._MeasurementEventBitLabel.ToolTipText = "Measurement Event Bit (MEB) is on"
        Me._MeasurementEventBitLabel.Visible = False
        '
        '_PowerOnEventLabel
        '
        Me._PowerOnEventLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._PowerOnEventLabel.Name = "_PowerOnEventLabel"
        Me._PowerOnEventLabel.Size = New System.Drawing.Size(22, 17)
        Me._PowerOnEventLabel.Text = "pon"
        Me._PowerOnEventLabel.ToolTipText = "Power on"
        Me._PowerOnEventLabel.Visible = False
        '
        '_UserRequestEventLabel
        '
        Me._UserRequestEventLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._UserRequestEventLabel.Name = "_UserRequestEventLabel"
        Me._UserRequestEventLabel.Size = New System.Drawing.Size(20, 17)
        Me._UserRequestEventLabel.Text = "urq"
        Me._UserRequestEventLabel.ToolTipText = "User request "
        Me._UserRequestEventLabel.Visible = False
        '
        '_CommandErrorEventLabel
        '
        Me._CommandErrorEventLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._CommandErrorEventLabel.Name = "_CommandErrorEventLabel"
        Me._CommandErrorEventLabel.Size = New System.Drawing.Size(22, 17)
        Me._CommandErrorEventLabel.Text = "cme"
        Me._CommandErrorEventLabel.ToolTipText = "Command error "
        Me._CommandErrorEventLabel.Visible = False
        '
        '_ExecutionErrorEventLabel
        '
        Me._ExecutionErrorEventLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._ExecutionErrorEventLabel.Name = "_ExecutionErrorEventLabel"
        Me._ExecutionErrorEventLabel.Size = New System.Drawing.Size(20, 17)
        Me._ExecutionErrorEventLabel.Text = "exe"
        Me._ExecutionErrorEventLabel.ToolTipText = "Execution error"
        Me._ExecutionErrorEventLabel.Visible = False
        '
        '_DeviceDependentErrorEventToolLabel
        '
        Me._DeviceDependentErrorEventToolLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._DeviceDependentErrorEventToolLabel.Name = "_DeviceDependentErrorEventToolLabel"
        Me._DeviceDependentErrorEventToolLabel.Size = New System.Drawing.Size(22, 17)
        Me._DeviceDependentErrorEventToolLabel.Text = "dde"
        Me._DeviceDependentErrorEventToolLabel.ToolTipText = "Device dependent error"
        Me._DeviceDependentErrorEventToolLabel.Visible = False
        '
        '_QueryErrorEventLabel
        '
        Me._QueryErrorEventLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._QueryErrorEventLabel.Name = "_QueryErrorEventLabel"
        Me._QueryErrorEventLabel.Size = New System.Drawing.Size(21, 17)
        Me._QueryErrorEventLabel.Text = "qye"
        Me._QueryErrorEventLabel.ToolTipText = "Query error"
        Me._QueryErrorEventLabel.Visible = False
        '
        '_OperationCompleteEventLabel
        '
        Me._OperationCompleteEventLabel.ForeColor = System.Drawing.Color.DarkOrange
        Me._OperationCompleteEventLabel.Name = "_OperationCompleteEventLabel"
        Me._OperationCompleteEventLabel.Size = New System.Drawing.Size(21, 17)
        Me._OperationCompleteEventLabel.Text = "opc"
        Me._OperationCompleteEventLabel.ToolTipText = "Operation complete"
        Me._OperationCompleteEventLabel.Visible = False
        '
        '_ServiceRequestHandledLabel
        '
        Me._ServiceRequestHandledLabel.Image = Global.isr.VI.Facade.My.Resources.Resources.EventHandleGreen16
        Me._ServiceRequestHandledLabel.Name = "_ServiceRequestHandledLabel"
        Me._ServiceRequestHandledLabel.Size = New System.Drawing.Size(16, 17)
        Me._ServiceRequestHandledLabel.ToolTipText = "Service request handler assigned"
        Me._ServiceRequestHandledLabel.Visible = False
        '
        '_MeasurementEventStatusLabel
        '
        Me._MeasurementEventStatusLabel.ForeColor = System.Drawing.Color.LightSkyBlue
        Me._MeasurementEventStatusLabel.Name = "_MeasurementEventStatusLabel"
        Me._MeasurementEventStatusLabel.Size = New System.Drawing.Size(29, 17)
        Me._MeasurementEventStatusLabel.Text = "mes"
        Me._MeasurementEventStatusLabel.ToolTipText = "Measurement Event Status"
        '
        'DisplayView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Name = "DisplayView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(443, 148)
        Me._MeasureStatusStrip.ResumeLayout(False)
        Me._MeasureStatusStrip.PerformLayout()
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._SessionReadingStatusStrip.ResumeLayout(False)
        Me._SessionReadingStatusStrip.PerformLayout()
        Me._SubsystemStatusStrip.ResumeLayout(False)
        Me._SubsystemStatusStrip.PerformLayout()
        Me._TitleStatusStrip.ResumeLayout(False)
        Me._TitleStatusStrip.PerformLayout()
        Me._ErrorStatusStrip.ResumeLayout(False)
        Me._ErrorStatusStrip.PerformLayout()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _MeasureStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _MeasureMetaStatusLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ReadingAmountLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _StatusRegisterLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _StandardRegisterLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _MeasureElapsedTimeLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _SessionReadingStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _LastReadingLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _SubsystemStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _SubsystemReadingLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _TitleStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _TitleLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ErrorStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _ErrorLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _SessionElapsedTimeLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ServiceRequestBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _MessageAvailableBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _StatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _ServiceRequestEnabledLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ErrorAvailableBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _SubsystemElapsedTimeLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _TerminalsStatusLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _SessionOpenCloseStatusLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _OperationSummaryBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _EventSummaryBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _QuestionableSummaryBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _SystemEventBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _MeasurementEventBitLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _PowerOnEventLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _UserRequestEventLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _CommandErrorEventLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ExecutionErrorEventLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _DeviceDependentErrorEventToolLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _QueryErrorEventLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _OperationCompleteEventLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ServiceRequestEnableBitmaskLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _StandardEventEnableBitmaskLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _ServiceRequestHandledLabel As isr.Core.Controls.ToolStripStatusLabel
    Private WithEvents _MeasurementEventStatusLabel As isr.Core.Controls.ToolStripStatusLabel
End Class
