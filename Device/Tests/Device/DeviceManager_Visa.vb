'---------------------------------------------------------------------------------------------------
' file:		.\DeviceManager_Visa.vb
'
' summary:	Device manager visa class
'---------------------------------------------------------------------------------------------------

Partial Public NotInheritable Class DeviceManager

#Region " VISA RESOURCE CHECKS "

    ''' <summary> Assert visa resource manager includes resource. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub AssertVisaResourceManagerIncludesResource(ByVal resourceSettings As ResourceSettingsBase)
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        Dim resourcesFilter As String = VI.SessionFactory.Get.Factory.ResourcesProvider.ResourceFinder.BuildMinimalResourcesFilter
        Dim resources As String()
        Using rm As VI.Pith.ResourcesProviderBase = VI.SessionFactory.Get.Factory.ResourcesProvider()
            resources = rm.FindResources(resourcesFilter).ToArray
        End Using
        Assert.IsTrue(resources.Any, $"VISA Resources {If(resources.Any, String.Empty, "not")} found among {resourcesFilter}")
        Assert.IsTrue(resources.Contains(resourceSettings.ResourceName), $"Resource {resourceSettings.ResourceName} not found among {resourcesFilter}")
    End Sub

    ''' <summary> Assert resource found. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device">           The device. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub AssertResourceFound(ByVal device As VisaSessionBase, ByVal resourceSettings As ResourceSettingsBase)
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        Assert.IsTrue(VisaSessionBase.Find(resourceSettings.ResourceName, device.ResourcesFilter),
                      $"VISA Resource {resourceSettings.ResourceName} not found among {device.ResourcesFilter}")
    End Sub

    ''' <summary> Assert resource found. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">          The device. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub AssertResourceFound(ByVal session As VisaSession, ByVal resourceSettings As ResourceSettingsBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        Assert.IsTrue(VisaSessionBase.Find(resourceSettings.ResourceName, session.ResourcesFilter),
                      $"VISA Resource {resourceSettings.ResourceName} not found among {session.ResourcesFilter}")
    End Sub

#End Region

End Class
