'---------------------------------------------------------------------------------------------------
' file:		.\Views\VisaView.vb
'
' summary:	Visa view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Drawing

Imports isr.Core
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> Provides a user interface for a <see cref="VisaSession"/> </summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2018-12-24, 6.0.6932.x"> Create based on the read and write control panel </para></remarks>
<System.ComponentModel.DisplayName("VISA View"), System.ComponentModel.Description("User interface for VISA Sessions"),
      System.Drawing.ToolboxBitmap(GetType(VisaView))>
Public Class VisaView
    Inherits ModelViewTalkerBase
    Implements IVisaView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()

        ' note that the caption is not set if this is run inside the On Load function.
        ' set defaults for the messages box.
        Me._TraceMessagesBox.ResetCount = 500
        Me._TraceMessagesBox.PresetCount = 250
        Me._TraceMessagesBox.ContainerPanel = Me._MessagesTabPage
        Me._TraceMessagesBox.AlertsToggleControl = Me._MessagesTabPage
        Me._TraceMessagesBox.CommenceUpdates()
        Me._Tabs.DrawMode = TabDrawMode.OwnerDrawFixed
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="visaSessionBase"> The visa session base (device base) (device). </param>
    Public Sub New(ByVal visaSessionBase As VisaSessionBase)
        Me.New()
        ' assigns the visa session.
        Me.BindVisaSessionBaseThis(visaSessionBase)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.VI.Facade.VisaView and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                Me._TraceMessagesBox.SuspendUpdatesReleaseIndicators()
                ' make sure the session is unbound in case the form is closed without closing the session.
                Me.BindVisaSessionBaseThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Tabs draw item. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw item event information. </param>
    Private Sub Tabs_DrawItem(sender As Object, e As DrawItemEventArgs) Handles _Tabs.DrawItem
        Dim page As TabPage = Me._Tabs.TabPages(e.Index)
        Dim paddedBounds As Rectangle = e.Bounds
        Dim backClr As Color = If(e.State = DrawItemState.Selected, SystemColors.ControlDark, page.BackColor)
        Using brush As Brush = New SolidBrush(backClr)
            e.Graphics.FillRectangle(brush, paddedBounds)
        End Using
        Dim yOffset As Integer = If(e.State = DrawItemState.Selected, -2, 1)
        paddedBounds = e.Bounds
        paddedBounds.Offset(1, yOffset)
        TextRenderer.DrawText(e.Graphics, page.Text, page.Font, paddedBounds, page.ForeColor)
    End Sub


#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me.AddPrivateListener(Me._TraceMessagesBox)
            Me._TraceMessagesBox.ContainerPanel = Me._MessagesTabPage
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " VISA SESSION BASE (DEVICE BASE) "

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase Implements IVisaView.VisaSessionBase

    ''' <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
    ''' <param name="visaSessionBase"> The assigned visa session base (device base) or nothing to
    '''                                release the session. </param>
    Public Overridable Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        Me.BindVisaSessionBaseThis(visaSessionBase)
    End Sub

    ''' <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="visaSessionBase"> The assigned visa session or nothing to release the session. </param>
    Private Sub BindVisaSessionBaseThis(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            If Me.VisaSessionBase.Talker IsNot Nothing Then Me.VisaSessionBase.Talker.RemoveListeners()
            RemoveHandler Me.VisaSessionBase.Opening, AddressOf Me.HandleDeviceOpening
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            RemoveHandler Me.VisaSessionBase.Closing, AddressOf Me.HandleDeviceClosing
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed

            Me._SelectorOpener.AssignSelectorViewModel(Nothing)
            Me._SelectorOpener.AssignOpenerViewModel(Nothing)

            Me.BindVisaSession(False, Me.VisaSessionBase)
            Me.BindSession(False, Me.VisaSessionBase.Session)

            Me._VisaSessionBase = Nothing
        End If
        Me._VisaSessionBase = visaSessionBase
        If visaSessionBase IsNot Nothing Then
            visaSessionBase.AddPrivateListener(Me._TraceMessagesBox)
            AddHandler Me.VisaSessionBase.Opening, AddressOf Me.HandleDeviceOpening
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            AddHandler Me.VisaSessionBase.Closing, AddressOf Me.HandleDeviceClosing
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed

            Me._SelectorOpener.AssignSelectorViewModel(visaSessionBase.SessionFactory)
            Me._SelectorOpener.AssignOpenerViewModel(visaSessionBase)

            Me.BindVisaSession(True, Me.VisaSessionBase)
            Me.BindSession(True, Me.VisaSessionBase.Session)

            Me.VisaSessionBase.Session.ApplySettings()
            If Not String.IsNullOrWhiteSpace(Me.VisaSessionBase.CandidateResourceName) Then Me.VisaSessionBase.Session.CandidateResourceName = Me.VisaSessionBase.CandidateResourceName
            If Not String.IsNullOrWhiteSpace(Me.VisaSessionBase.CandidateResourceTitle) Then Me.VisaSessionBase.Session.CandidateResourceTitle = Me.VisaSessionBase.CandidateResourceTitle
            If Me.VisaSessionBase.IsDeviceOpen Then
                Me.VisaSessionBase.Session.OpenResourceName = Me.VisaSessionBase.OpenResourceName
                Me.VisaSessionBase.Session.OpenResourceTitle = Me.VisaSessionBase.OpenResourceTitle
            End If

            Me.AssignTalker(visaSessionBase.Talker)
            Me.ApplyListenerTraceLevel(ListenerType.Display, visaSessionBase.Talker.TraceShowLevel)

        End If
        Me._SessionView.BindVisaSessionBase(visaSessionBase)
        Me._DisplayView.BindVisaSessionBase(visaSessionBase)
        Me._StatusView.BindVisaSessionBase(visaSessionBase)
    End Sub

    ''' <summary> Bind visa session. </summary>
    ''' <param name="add">             True to add. </param>
    ''' <param name="visaSessionBase"> The assigned visa session or nothing to release the session. </param>
    Private Sub BindVisaSession(ByVal add As Boolean, ByVal visaSessionBase As VisaSessionBase)
        For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
            If Not (t Is Me._MessagesTabPage OrElse t Is Me._SessionTabPage) Then
                Me.AddRemoveBinding(t, add, NameOf(Control.Enabled), visaSessionBase, NameOf(visaSessionBase.IsSessionOpen))
            End If
        Next
    End Sub

    ''' <summary> Bind session. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindSession(ByVal add As Boolean, ByVal viewModel As VI.Pith.SessionBase)
        Me.AddRemoveBinding(Me._StatusPromptLabel, add, NameOf(Control.Text), viewModel, NameOf(VI.Pith.SessionBase.StatusPrompt))
    End Sub

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Gets the resource name caption. </summary>
    ''' <value> The resource name caption. </value>
    Private Property ResourceNameCaption As String = String.Empty

    ''' <summary> Executes the device Closing action. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Handles the device Closing. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceNameCaption} UI handling device closing"
            Me.OnDeviceClosing(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device Closed action. </summary>
    Protected Overridable Sub OnDeviceClosed()
    End Sub

    ''' <summary> Handles the device Close. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceNameCaption} UI handling device closed"
            Me.OnDeviceClosed()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device Opening action. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceOpening(ByVal e As System.ComponentModel.CancelEventArgs)
        Me.ResourceNameCaption = Me.VisaSessionBase.ResourceNameCaption
    End Sub

    ''' <summary> Handles the device Opening. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device Opening"
            Me.OnDeviceOpening(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    Protected Overridable Sub OnDeviceOpened()
        Me.ResourceNameCaption = Me.VisaSessionBase.ResourceNameCaption
    End Sub

    ''' <summary> Handles the device opened. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device opened"
            Me.OnDeviceOpened()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CUSTOMIZATION "

    ''' <summary> Gets the Display view. </summary>
    ''' <value> The Display view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DisplayView As DisplayView Implements IVisaView.DisplayView
        Get
            Return Me._DisplayView
        End Get
    End Property

    ''' <summary> Gets the status view. </summary>
    ''' <value> The status view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property StatusView As StatusView Implements IVisaView.StatusView
        Get
            Return Me._StatusView
        End Get
    End Property

    ''' <summary> Gets the number of views. </summary>
    ''' <value> The number of tabs. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property ViewsCount As Integer Implements IVisaView.ViewCount
        Get
            Return Me._Tabs.TabCount
        End Get
    End Property

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <param name="view"> The view control. </param>
    Public Sub AddView(ByVal view As VisaViewControl) Implements IVisaView.AddView
        Me.AddView(view.Control, view.Index, view.Name, view.Caption)
    End Sub

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="view">        The view control. </param>
    ''' <param name="viewIndex">   Zero-based index of the view. </param>
    ''' <param name="viewName">    The name of the view. </param>
    ''' <param name="viewCaption"> The caption of the view. </param>
    Public Sub AddView(ByVal view As Control, ByVal viewIndex As Integer, ByVal viewName As String, ByVal viewCaption As String) Implements IVisaView.AddView
        If view Is Nothing Then Throw New ArgumentNullException(NameOf(view))
        Dim tabPage As New TabPage(viewName)
        ' https://social.msdn.microsoft.com/Forums/windows/en-US/5d10fd0c-1aa6-4092-922e-1fd7af979663/tabpagesinsert-bug?forum=winforms
        ' https://stackoverflow.com/questions/1532301/visual-studio-tabcontrol-tabpages-insert-not-working
        ' The tab control's handler must be created for the insert method to work
        Dim h As IntPtr = Me._Tabs.Handle
        Me._Tabs.TabPages.Insert(viewIndex, tabPage)
        tabPage.SuspendLayout()
        tabPage.Controls.Add(view)
        tabPage.Location = New System.Drawing.Point(4, 26)
        tabPage.Name = $"{viewName?.Replace(" ", String.Empty)}TabPage"
        tabPage.Size = New System.Drawing.Size(356, 255)
        tabPage.Text = viewCaption
        tabPage.UseVisualStyleBackColor = True
        tabPage.ResumeLayout()
        view.Dock = DockStyle.Fill
    End Sub

    ''' <summary> Adds (inserts) a View. </summary>
    ''' <param name="view">        The view control. </param>
    ''' <param name="viewIndex">   Zero-based index of the view. </param>
    ''' <param name="viewName">    The name of the view. </param>
    ''' <param name="viewCaption"> The caption of the view. </param>
    Public Sub AddView(ByVal view As ModelViewTalkerBase, ByVal viewIndex As Integer, ByVal viewName As String, ByVal viewCaption As String) Implements IVisaView.AddView
        Me.AddView(TryCast(view, Control), viewIndex, viewName, viewCaption)
        view.AddPrivateListener(Me._TraceMessagesBox)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds a private listener. </summary>
    ''' <param name="listener"> The listener. </param>
    Private Sub AddPrivateListenerThis(ByVal listener As Core.IMessageListener)
        ' this causes a binding cross thread exception when binding to the selector opener. Weird. 
        Me._SelectorOpener.AddPrivateListener(listener)
        Me._StatusView.AddPrivateListener(listener)
        Me._DisplayView.AddPrivateListener(listener)
        Me._SessionView.AddPrivateListener(listener)
    End Sub

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As ITraceMessageTalker)
        Me._SelectorOpener.AssignTalker(talker)
        Me._StatusView.AssignTalker(talker)
        Me._DisplayView.AssignTalker(talker)
        Me._SessionView.AssignTalker(talker)
        Me.AddPrivateListenerThis(Me._TraceMessagesBox)
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        Me._SelectorOpener.ApplyListenerTraceLevel(listenerType, value)
        Me._SessionView?.ApplyListenerTraceLevel(listenerType, value)
        ' this should apply only to the listeners associated with this form
        ' MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " UNIT TESTS INTERNALS "

    ''' <summary> Gets the number of internal resource names. </summary>
    ''' <value> The number of internal resource names. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalResourceNamesCount As Integer Implements IVisaView.InternalResourceNamesCount
        Get
            Return Me._SelectorOpener.InternalResourceNamesCount
        End Get
    End Property

    ''' <summary> Gets the name of the internal selected resource. </summary>
    ''' <value> The name of the internal selected resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Friend ReadOnly Property InternalSelectedResourceName As String Implements IVisaView.InternalSelectedResourceName
        Get
            Return Me._SelectorOpener.InternalSelectedResourceName
        End Get
    End Property

#End Region

End Class

