''' <summary> Defines a System Subsystem for a InTest Thermo Stream instrument. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class SystemSubsystem
    Inherits VI.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.CoolingEnabled = New Boolean?
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the initialize memory command. </summary>
    ''' <value> The initialize memory command. </value>
    Protected Overrides Property InitializeMemoryCommand As String = String.Empty

    ''' <summary> Gets or sets the language revision query command. </summary>
    ''' <value> The language revision query command. </value>
    Protected Overrides Property LanguageRevisionQueryCommand As String = String.Empty


#End Region

#Region " SCPI VERSION "

    ''' <summary> The automatic tuning enabled. </summary>
    Private _AutoTuningEnabled As Boolean?

    ''' <summary>
    ''' Gets or sets the cached version level of the SCPI standard implemented by the device.
    ''' </summary>
    ''' <value> The Auto Tuning Enabled. </value>
    Public Property AutoTuningEnabled As Boolean?
        Get
            Return Me._AutoTuningEnabled
        End Get
        Protected Set(value As Boolean?)
            If Not Boolean?.Equals(Me.AutoTuningEnabled, value) Then
                Me._AutoTuningEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the AutoTuning enabled state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> If set to <c>True</c> enable. </param>
    ''' <returns>
    ''' <c>True</c> if AutoTuning is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function ApplyAutoTuningEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoTuningEnabled(value)
        Return Me.QueryAutoTuningEnabled()
    End Function

    ''' <summary> Queries the AutoTuning enabled state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns>
    ''' <c>True</c> if AutoTuning is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function QueryAutoTuningEnabled() As Boolean?
        Me.AutoTuningEnabled = Me.Session.Query(Me.AutoTuningEnabled.GetValueOrDefault(True), "LRNM?")
        Return Me.AutoTuningEnabled
    End Function

    ''' <summary> Writes the AutoTuning enabled state without reading back the actual value. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> if set to <c>True</c> [value]. </param>
    ''' <returns>
    ''' <c>True</c> if AutoTuning is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function WriteAutoTuningEnabled(ByVal value As Boolean) As Boolean?
        Me.Session.WriteLine("LRNM {0:'1';'1';'0'}", CType(value, Integer))
        Me.AutoTuningEnabled = value
        Return Me.AutoTuningEnabled
    End Function

#End Region

#Region " COOLING ENABLED "

    ''' <summary> The cooling enabled. </summary>
    Private _CoolingEnabled As Boolean?

    ''' <summary> Gets or sets a cached value indicating whether Cooling is enabled. </summary>
    ''' <value>
    ''' <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </value>
    Public Property CoolingEnabled() As Boolean?
        Get
            Return Me._CoolingEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.CoolingEnabled, value) Then
                Me._CoolingEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Cooling enabled state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> If set to <c>True</c> enable. </param>
    ''' <returns>
    ''' <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function ApplyCoolingEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteCoolingEnabled(value)
        Return Me.QueryCoolingEnabled()
    End Function

    ''' <summary> Queries the Cooling enabled state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns>
    ''' <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function QueryCoolingEnabled() As Boolean?
        Me.CoolingEnabled = Me.Session.Query(Me.CoolingEnabled.GetValueOrDefault(True), "COOL?")
        Return Me.CoolingEnabled
    End Function

    ''' <summary> Writes the Cooling enabled state without reading back the actual value. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> if set to <c>True</c> [value]. </param>
    ''' <returns>
    ''' <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function WriteCoolingEnabled(ByVal value As Boolean) As Boolean?
        Me.Session.WriteLine("COOL {0:'1';'1';'0'}", CType(value, Integer))
        Me.CoolingEnabled = value
        Return Me.CoolingEnabled
    End Function

#End Region

#Region " DEVICE CONTROL ENABLED "

    ''' <summary> The device control enabled. </summary>
    Private _DeviceControlEnabled As Boolean?

    ''' <summary> Gets or sets a value indicating whether Device Control is enabled. </summary>
    ''' <value> <c>True</c> if Device Control mode is enabled; otherwise, <c>False</c>. </value>
    Public Property DeviceControlEnabled() As Boolean?
        Get
            Return Me._DeviceControlEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.DeviceControlEnabled, value) Then
                Me._DeviceControlEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Device Control enabled state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> If set to <c>True</c> enable. </param>
    ''' <returns>
    ''' <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function ApplyDeviceControlEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteDeviceControlEnabled(value)
        Return Me.QueryDeviceControlEnabled()
    End Function

    ''' <summary> Queries the Device Control enabled state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns>
    ''' <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function QueryDeviceControlEnabled() As Boolean?
        Me.DeviceControlEnabled = Me.Session.Query(Me.DeviceControlEnabled.GetValueOrDefault(True), "DUTM?")
        Return Me.DeviceControlEnabled
    End Function

    ''' <summary>
    ''' Writes the Device Control enabled state without reading back the actual value.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> if set to <c>True</c> [value]. </param>
    ''' <returns>
    ''' <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or unknown.
    ''' </returns>
    Public Function WriteDeviceControlEnabled(ByVal value As Boolean) As Boolean?
        Me.DeviceControlEnabled = New Boolean?
        Me.Session.WriteLine("DUTM {0:'1';'1';'0'}", CType(value, Integer))
        Me.DeviceControlEnabled = value
        Return Me.DeviceControlEnabled
    End Function

#End Region

End Class
