'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\BufferStreamMonitorView.vb
'
' summary:	Buffer stream monitor view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.VI.Facade.DataGridViewExtensions
Imports isr.Core.SplitExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A buffer stream trigger monitor view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
<Obsolete("Work in progress")>
Public Class BufferStreamMonitorView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._BufferSizeNumeric.NumericUpDownControl.CausesValidation = True
        Me._BufferSizeNumeric.NumericUpDownControl.Minimum = 0
        Me._BufferSizeNumeric.NumericUpDownControl.Maximum = 27500000
    End Sub

    ''' <summary> Creates a new <see cref="BufferStreamMonitorView"/> </summary>
    ''' <returns> A <see cref="BufferStreamMonitorView"/>. </returns>
    Public Shared Function Create() As BufferStreamMonitorView
        Dim view As BufferStreamMonitorView = Nothing
        Try
            view = New BufferStreamMonitorView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS: BUFFER "

    ''' <summary> Applies the buffer capacity. </summary>
    ''' <param name="capacity"> The capacity. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplyBufferCapacity(ByVal capacity As Integer)
        Dim activity As String = String.Empty
        Try
            If Me.Device.IsDeviceOpen Then
                Dim value As Integer = CInt(Me._BufferSizeNumeric.Value)
                If Me.BufferSubsystem IsNot Nothing Then
                    activity = $"{Me.Device.ResourceNameCaption} setting {GetType(TraceSubsystemBase)}.{NameOf(BufferSubsystemBase.Capacity)} to {value}"
                    Me.PublishVerbose(activity)
                    BufferStreamMonitorView.ApplyBufferCapacity(Me.BufferSubsystem, value)
                ElseIf Me.TraceSubsystem IsNot Nothing Then
                    activity = $"{Me.Device.ResourceNameCaption} setting {GetType(TraceSubsystemBase)}.{NameOf(TraceSubsystemBase.PointsCount)} to {value}"
                    Me.PublishVerbose(activity)
                    BufferStreamMonitorView.ApplyBufferCapacity(Me.TraceSubsystem, value)
                End If
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        End Try
    End Sub

    ''' <summary> Reads the buffer. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadBuffer()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            If Me.BufferSubsystem IsNot Nothing Then
                Me.ReadBuffer(Me.BufferSubsystem)
            ElseIf Me.TraceSubsystem IsNot Nothing Then
                Me.ReadBuffer(Me.TraceSubsystem)
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the given subsystem. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Read()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} querying terminal mode" : Me.PublishVerbose($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                BufferStreamMonitorView.ReadTerminalsState(Me.MultimeterSubsystem)
            ElseIf Me.SystemSubsystem IsNot Nothing Then
                BufferStreamMonitorView.ReadSettings(Me.SystemSubsystem)
                ' Me.ApplyPropertyChanged(Me.SystemSubsystem)
            End If
            activity = $"{Me.Device.ResourceNameCaption} measuring" : Me.PublishVerbose($"{activity};. ")
            If Me.MultimeterSubsystem IsNot Nothing Then
                BufferStreamMonitorView.Read(Me.MultimeterSubsystem)
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS: BUFFER DISPLAY "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DataGridView As DataGridView
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the data grid view. </summary>
    ''' <value> The data grid view. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DataGridView As DataGridView
        Get
            Return Me._DataGridView
        End Get
        Set(value As DataGridView)
            Me._DataGridView = value
        End Set
    End Property

    ''' <summary> Gets the buffer readings. </summary>
    ''' <value> The buffer readings. </value>
    Private ReadOnly Property BufferReadings As New VI.BufferReadingBindingList

    ''' <summary> Displays a buffer readings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub DisplayBufferReadings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            Me.BufferSubsystem.StartElapsedStopwatch()
            Me.ClearBufferDisplay()
            Me.BufferReadings.Add(Me.BufferSubsystem.QueryBufferReadings)
            Me.BufferSubsystem.LastReading = Me.BufferReadings.LastReading
            Me.BufferSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the buffer display. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ClearBufferDisplay()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing buffer display" : Me.PublishVerbose($"{activity};. ")
            If Me.BufferReadings Is Nothing Then
                Me._BufferReadings = New VI.BufferReadingBindingList()
                Me.DataGridView?.Bind(Me.BufferReadings, True)
            End If
            Me.BufferReadings.Clear()
        Catch ex As Exception
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(BufferStreamMonitorView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " BUFFER SUBSYSTEM "

    ''' <summary> Gets or sets the Buffer subsystem. </summary>
    ''' <value> The Buffer subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property BufferSubsystem As BufferSubsystemBase

    ''' <summary> Bind Buffer subsystem. </summary>
    ''' <param name="subsystem">  The subsystem. </param>
    ''' <param name="bufferName"> Name of the buffer. </param>
    Public Sub BindSubsystem(ByVal subsystem As BufferSubsystemBase, ByVal bufferName As String)
        If Me.BufferSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.BufferSubsystem)
            Me._BufferSubsystem = Nothing
        End If
        Me._BufferSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.BufferSubsystem)
            Me._SubsystemSplitButton.Text = bufferName
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As BufferSubsystemBase)
        If add Then Me.DataGridView.Bind(subsystem.BufferReadingsBindingList, True)
        Dim binding As Binding = Me.AddRemoveBinding(Me._BufferSizeNumeric, add, NameOf(NumericUpDown.Value), subsystem, NameOf(BufferSubsystemBase.Capacity))
        ' has to apply the value.
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
        Me.AddRemoveBinding(Me._BufferCountLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.ActualPointCount))
        Me.AddRemoveBinding(Me._FirstPointNumberLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.FirstPointNumber))
        Me.AddRemoveBinding(Me._LastPointNumberLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.LastPointNumber))
        BufferStreamMonitorView.ReadSettings(subsystem)
        ' Me.ApplyPropertyChanged(Subsystem)
    End Sub

    ''' <summary> Applies the buffer capacity. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="capacity">  The capacity. </param>
    Private Shared Sub ApplyBufferCapacity(ByVal subsystem As BufferSubsystemBase, ByVal capacity As Integer)
        ' overrides and set to the minimum size: Me._TraceSizeNumeric.Value = Me._TraceSizeNumeric.NumericUpDownControl.Minimum
        subsystem.StartElapsedStopwatch()
        subsystem.ApplyCapacity(capacity)
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As BufferSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryCapacity()
        subsystem.QueryFirstPointNumber()
        subsystem.QueryLastPointNumber()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the buffer. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadBuffer(ByVal subsystem As BufferSubsystemBase)
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            subsystem.StartElapsedStopwatch()
            Me.ClearBufferDisplay()
            Me.BufferReadings.Add(subsystem.QueryBufferReadings)
            subsystem.LastReading = Me.BufferReadings.LastReading
            subsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TRACE SUBSYSTEM "

    ''' <summary> Gets or sets the Trace subsystem. </summary>
    ''' <value> The Trace subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property TraceSubsystem As TraceSubsystemBase

    ''' <summary> Bind Trace subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As TraceSubsystemBase)
        If Me.TraceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TraceSubsystem)
            Me._TraceSubsystem = Nothing
        End If
        Me._TraceSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me._SubsystemSplitButton.Text = "Trace"
            Me.BindSubsystem(True, Me.TraceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TraceSubsystemBase)
        If add Then Me.DataGridView.Bind(subsystem.BufferReadingsBindingList, True)
        Dim binding As Binding = Me.AddRemoveBinding(Me._BufferSizeNumeric, add, NameOf(NumericUpDown.Value), subsystem, NameOf(TraceSubsystemBase.PointsCount))
        ' has to apply the value.
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
        Me.AddRemoveBinding(Me._BufferCountLabel, add, NameOf(Control.Text), subsystem, NameOf(TraceSubsystemBase.ActualPointCount))
        Me._FirstPointNumberLabel.Visible = False
        Me._LastPointNumberLabel.Visible = False
        BufferStreamMonitorView.ReadSettings(subsystem)
        ' Me.ApplyPropertyChanged(subsystem)
    End Sub

    ''' <summary> Applies the buffer capacity. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="capacity">  The capacity. </param>
    Private Shared Sub ApplyBufferCapacity(ByVal subsystem As TraceSubsystemBase, ByVal capacity As Integer)
        ' overrides and set to the minimum size: Me._TraceSizeNumeric.Value = Me._TraceSizeNumeric.NumericUpDownControl.Minimum
        subsystem.StartElapsedStopwatch()
        subsystem.ApplyPointsCount(capacity)
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As TraceSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryPointsCount()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the buffer. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ReadBuffer(ByVal subsystem As TraceSubsystemBase)
        subsystem.StartElapsedStopwatch()
        Me.ClearBufferDisplay()
        Me.BufferReadings.Add(subsystem.QueryBufferReadings)
        subsystem.LastBufferReading = Me.BufferReadings.LastReading
        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " MUTLIMETER "

    ''' <summary> Gets or sets the Multimeter subsystem. </summary>
    ''' <value> The Multimeter subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property MultimeterSubsystem As MultimeterSubsystemBase

    ''' <summary> Bind Multimeter subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindMultimeterSubsystem(ByVal subsystem As MultimeterSubsystemBase)
        If Me.MultimeterSubsystem IsNot Nothing Then
            Me._MultimeterSubsystem = Nothing
        End If
        Me._MultimeterSubsystem = subsystem
        If subsystem IsNot Nothing Then
        End If
    End Sub

    ''' <summary> Reads terminals state. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadTerminalsState(ByVal subsystem As MultimeterSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFrontTerminalsSelected()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Reads the given subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub Read(ByVal subsystem As MultimeterSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.MeasurePrimaryReading()
        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets or sets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystemBase

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystemBase)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
            ' BufferStreamMonitorView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As TriggerSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(TriggerSubsystemBase.TriggerState))
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(TriggerSubsystemBase.TriggerState)
                ' TO_DO: Bind trigger state label caption to the Subsystem reading label in the display view.
                ' TO_DO: Move functionality (e.g., handler trigger plan state change) from the user interface to the subsystem
                Me._TriggerStateLabel.Visible = subsystem.TriggerState.HasValue
                If subsystem.TriggerState.HasValue Then
                    Me._TriggerStateLabel.Text = subsystem.TriggerState.Value.ToString
                    If Me.TriggerPlanStateChangeHandlerEnabled Then
                        Me.HandleTriggerPlanStateChange(subsystem.TriggerState.Value)
                    End If
                    If Me.BufferStreamingHandlerEnabled AndAlso Not subsystem.IsTriggerStateActive Then
                        Me.ToggleBufferStream()
                    End If
                End If
                ' ?? this causes a cross thread exception. 
                ' Me._TriggerStateLabel.Invalidate()
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Initiate trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitiateTriggerPlan()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.StartElapsedStopwatch()
            Me.TriggerSubsystem.Initiate()
            Me.TriggerSubsystem.QueryTriggerState()
            Me.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Aborts trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub AbortTriggerPlan()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.Abort()
            Me.Device.Session.QueryOperationCompleted(TimeSpan.FromMilliseconds(100))
            Me.TriggerSubsystem.QueryTriggerState()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SYSTEM SUBSYSTEM "

    ''' <summary> Gets or sets the System subsystem. </summary>
    ''' <value> The System subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property SystemSubsystem As SystemSubsystemBase

    ''' <summary> Bind System subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub BindSubsystem(ByVal subsystem As SystemSubsystemBase)
        If Me.SystemSubsystem IsNot Nothing Then
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If subsystem IsNot Nothing Then
            BufferStreamMonitorView.ReadSettings(subsystem)
            ' Me.ApplyPropertyChanged(Subsystem)
        End If
    End Sub

    ''' <summary> Reads the settings. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As SystemSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryFrontTerminalsSelected()
        subsystem.StopElapsedStopwatch()
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: STREAM BUFFER "

    ''' <summary> Gets or sets the buffer streaming handler enabled. </summary>
    ''' <value> The buffer streaming handler enabled. </value>
    Private Property BufferStreamingHandlerEnabled As Boolean

    ''' <summary> Starts or stop buffer stream. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ToggleBufferStream()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.BufferStreamingHandlerEnabled Then
                Me.BufferStreamingHandlerEnabled = False
                activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan to stop buffer streaming" : Me.PublishInfo($"{activity};. ")
                Me.AbortTriggerPlan()
                Me.TriggerSubsystem.QueryTriggerState()
            Else
                activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
                Me.Device.ClearExecutionState()
                Me.BufferStreamingHandlerEnabled = False
                activity = $"{Me.Device.ResourceNameCaption} start buffer streaming" : Me.PublishInfo($"{activity};. ")
                Me.DataGridView.DataSource = Nothing
                Me.TriggerSubsystem.Initiate()
                isr.Core.ApplianceBase.DoEvents()
                If Me.BufferSubsystem IsNot Nothing Then
                    Me.BufferSubsystem.StartBufferStream(Me.TriggerSubsystem, TimeSpan.FromMilliseconds(5))
                End If
                Me.BufferStreamingHandlerEnabled = True
            End If
            Me._ToggleBufferStreamMenuItem.Checked = Me.BufferStreamingHandlerEnabled
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Stream buffer menu item check state changed. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ToggleBufferStreamMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _ToggleBufferStreamMenuItem.CheckStateChanged
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem IsNot Nothing Then
            menuItem.Text = $"{menuItem.CheckState.ToCheckStateCaption("Stop", "Start", "?")} Buffer Stream"
        End If
    End Sub

    ''' <summary> Toggle buffer stream menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ToggleBufferStreamMenuItem_Click(sender As Object, e As EventArgs) Handles _ToggleBufferStreamMenuItem.Click
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Try
            menuItem.Enabled = False
            Me.ToggleBufferStream()
        Catch
        Finally
            menuItem.Enabled = True
        End Try
    End Sub

#End Region

#Region " TRIGGER MONITOR BUFFER STREAMING "

    ''' <summary> Values that represent trigger plan states. </summary>
    Private Enum TriggerPlanState

        ''' <summary> An enum constant representing the none option. </summary>
        None

        ''' <summary> An enum constant representing the started option. </summary>
        Started

        ''' <summary> An enum constant representing the completed option. </summary>
        Completed
    End Enum

    ''' <summary> Gets or sets the trigger plan state change handler enabled. </summary>
    ''' <value> The trigger plan state change handler enabled. </value>
    Private Property TriggerPlanStateChangeHandlerEnabled As Boolean

    ''' <summary> Gets or sets the state of the local trigger plan. </summary>
    ''' <value> The local trigger plan state. </value>
    Private Property LocalTriggerPlanState As TriggerPlanState

    ''' <summary> Handles the trigger plan state change described by triggerState. </summary>
    ''' <param name="triggerState"> State of the trigger. </param>
    Private Sub HandleTriggerPlanStateChange(ByVal triggerState As VI.TriggerState)
        If triggerState = TriggerState.Running OrElse triggerState = TriggerState.Waiting Then
            Me.LocalTriggerPlanState = TriggerPlanState.Started
        ElseIf triggerState = TriggerState.Idle AndAlso Me.LocalTriggerPlanState = TriggerPlanState.Started Then
            Me.LocalTriggerPlanState = TriggerPlanState.Completed
            Me.TryReadNewBufferReadings()
            If Me._RepeatMenuItem.Checked Then
                Me.InitiateMonitorTriggerPlan(True)
            End If
        Else
            Me.LocalTriggerPlanState = TriggerPlanState.None
        End If
    End Sub

    ''' <summary> Try read buffer. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryReadNewBufferReadings()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading"
        Try
            Me.ReadNewBufferReadings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
        End Try
    End Sub

    ''' <summary> Reads the buffer. </summary>
    Private Sub ReadNewBufferReadings()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} fetching buffer count"
        Me.PublishVerbose($"{activity};. ")
        ' this assume buffer is cleared upon each new cycle
        Dim newBufferCount As Integer = Me.BufferSubsystem.QueryActualPointCount.GetValueOrDefault(0)
        activity = $"buffer count {newBufferCount}"
        Me.PublishVerbose($"{activity};. ")
        If newBufferCount > 0 Then
            activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
            Me.PublishVerbose($"{activity};. ")
            Me.BufferSubsystem.StartElapsedStopwatch()
            Me.BufferReadings.Add(Me.BufferSubsystem.QueryBufferReadings())
            Me.BufferSubsystem.StopElapsedStopwatch()
        End If
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Initiate monitor trigger plan. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="stateChangeHandlingEnabled"> True to enable, false to disable the state change
    '''                                           handling. </param>
    Private Sub InitiateMonitorTriggerPlan(ByVal stateChangeHandlingEnabled As Boolean)
        Dim activity As String = $"{Me.Device.ResourceNameCaption} clearing execution state"
        Me.PublishVerbose($"{activity};. ")
        Me.Device.ClearExecutionState()
        activity = $"{Me.Device.ResourceNameCaption} Initiating trigger plan and monitor"
        Me.PublishVerbose($"{activity};. ")
        Me.TriggerSubsystem.Initiate()
        isr.Core.ApplianceBase.DoEvents()
        Me.ClearBufferDisplay()
        Me.TriggerPlanStateChangeHandlerEnabled = stateChangeHandlingEnabled
        Me.TriggerSubsystem.AsyncMonitorTriggerState(TimeSpan.FromMilliseconds(5))
    End Sub

    ''' <summary> Try restart trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryRestartTriggerPlan()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} Initiating trigger plan and monitor"
        Try
            Me.InitiateMonitorTriggerPlan(True)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
        End Try
    End Sub

    ''' <summary> Monitor active trigger plan menu item click. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MonitorActiveTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _MonitorActiveTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} start monitoring trigger plan"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.TriggerPlanStateChangeHandlerEnabled = False
            Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.AsyncMonitorTriggerState(TimeSpan.FromMilliseconds(5))
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initializes the monitor read repeat menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitMonitorReadRepeatMenuItem_Click(sender As Object, e As EventArgs) Handles _InitMonitorReadRepeatMenuItem.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} Initiating trigger plan and monitor"
        Try
            Me.InitiateMonitorTriggerPlan(True)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
        End Try
    End Sub

#End Region

#Region " TRIGGER MONITOR EVENT HANDLING "

    ''' <summary> Handles the measurement completed request. </summary>
    ''' <param name="sender"> <see cref="Object"/>
    '''                                             instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementCompletedRequest(sender As Object, e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} handling SRQ: {Me.Device.Session.ServiceRequestStatus:X}" : Me.PublishVerbose($"{activity};. ")
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} kludge: reading buffer count"
            Me.PublishVerbose($"{activity};. ")

            ' this assume buffer is cleared upon each new cycle
            Dim newBufferCount As Integer = Me.BufferSubsystem.QueryActualPointCount.GetValueOrDefault(0)

            If newBufferCount > 0 Then

                activity = $"{Me.Device.ResourceNameCaption} kludge: buffer has data..."
                Me.PublishVerbose($"{activity};. ")

                activity = $"{Me.Device.ResourceNameCaption} handling measurement available"
                Me.PublishVerbose($"{activity};. ")

                If False Then
                    activity = $"{Me.Device.ResourceNameCaption} fetching a single reading"
                    Me.PublishVerbose($"{activity};. ")
                    Me.MultimeterSubsystem.MeasurePrimaryReading()
                Else
                    activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
                    Me.PublishVerbose($"{activity};. ")
                    Me.BufferReadings.Add(Me.BufferSubsystem.QueryBufferReadings())

                    activity = $"{Me.Device.ResourceNameCaption} updating the display" : Me.PublishVerbose($"{activity};. ")
                    Me.DataGridView.Invalidate()
                    Me.BufferSubsystem.StopElapsedStopwatch()
                End If
                If Me._RepeatMenuItem.Checked Then
                    activity = $"{Me.Device.ResourceNameCaption} initiating next measurement(s)" : Me.PublishVerbose($"{activity};. ")
                    Me.BufferSubsystem.StartElapsedStopwatch()
                    Me.BufferSubsystem.ClearBuffer() ' ?@3 removed 17-7-6
                    Me.TriggerSubsystem.Initiate()
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} trigger plan started; buffer empty"
                Me.PublishVerbose($"{activity};. ")
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the measurement complete handler added. </summary>
    ''' <value> The measurement complete handler added. </value>
    Private Property MeasurementCompleteHandlerAdded As Boolean

    ''' <summary> Adds measurement complete event handler. </summary>
    Private Sub AddMeasurementCompleteEventHandler()

        Dim activity As String = String.Empty
        If Not Me.MeasurementCompleteHandlerAdded Then

            ' clear execution state before enabling events
            activity = $"{Me.Device.ResourceNameCaption} Clearing execution state"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} Enabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.EnableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Adding device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.AddServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning on measurement events"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.StatusSubsystemBase.ApplyMeasurementEventEnableBitmask(&H7FFF)
            ' 
            ' if handling buffer full, use the 4917 event to detect buffer full. 

            activity = $"{Me.Device.ResourceNameCaption} Turning on status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)

            activity = $"{Me.Device.ResourceNameCaption} Adding re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            AddHandler Me.Device.ServiceRequested, AddressOf Me.HandleMeasurementCompletedRequest
            Me.MeasurementCompleteHandlerAdded = True
        End If
    End Sub

    ''' <summary> Removes the measurement complete event handler. </summary>
    Private Sub RemoveMeasurementCompleteEventHandler()

        Dim activity As String = String.Empty
        If Me.MeasurementCompleteHandlerAdded Then

            activity = $"{Me.Device.ResourceNameCaption} Disabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.DisableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Removing device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.RemoveServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning off measurement events"
            Me.PublishVerbose($"{activity};. ")
            ' Me.Device.StatusSubsystemBase.ApplyQuestionableEventEnableBitmask(0)

            activity = $"{Me.Device.ResourceNameCaption} Turning off status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.None)

            activity = $"{Me.Device.ResourceNameCaption} Removing re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.HandleMeasurementCompletedRequest

            Me.MeasurementCompleteHandlerAdded = False

        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by HandleMeasurementEventMenuItem for check state changed events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementEventMenuItem_CheckStateChanged(sender As Object, e As EventArgs)

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan()

            If menuItem.Checked Then

                activity = $"{Me.Device.ResourceNameCaption} Adding measurement completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.AddMeasurementCompleteEventHandler()

            Else

                activity = $"{Me.Device.ResourceNameCaption} Removing measurement completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.RemoveMeasurementCompleteEventHandler()

            End If

        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " BUFFER HANDLER "

    ''' <summary> Handles the buffer full request. </summary>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleBufferFullRequest(sender As Object, e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} handling SRQ: {Me.Device.Session.ServiceRequestStatus:X2}"
            Me.PublishVerbose($"{activity};. ")

            If Me.Device.Session.OperationCompleted Then

                activity = $"{Me.Device.ResourceNameCaption} handling operation completed"
                Me.PublishVerbose($"{activity};. ")

                ' TO_DO: See if can only do a set condition and not read this.
                Dim condition As Integer = Me.Device.StatusSubsystemBase.QueryOperationEventCondition().GetValueOrDefault(0)
                activity = $"{Me.Device.ResourceNameCaption} OPER: {condition:X2}" : Me.PublishVerbose($"{activity};. ")

                ' If Bit 0 Is set then the buffer is full
                If (condition And (1 << Me.BufferFullOperationConditionBitNumber)) <> 0 Then
                    activity = $"{Me.Device.ResourceNameCaption} handling buffer full"
                    Me.PublishVerbose($"{activity};. ")

                    activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
                    Me.PublishVerbose($"{activity};. ")
                    Me.BufferReadings.Add(Me.BufferSubsystem.QueryBufferReadings())

                    activity = $"{Me.Device.ResourceNameCaption} updating the display" : Me.PublishVerbose($"{activity};. ")
                    Me.BufferSubsystem.LastReading = Me.BufferReadings.LastReading
                    Me.BufferSubsystem.StopElapsedStopwatch()
                    If Me._RepeatMenuItem.Checked Then
                        activity = $"{Me.Device.ResourceNameCaption} initiating next measurement(s)"
                        Me.PublishVerbose($"{activity};. ")
                        Me.BufferSubsystem.StartElapsedStopwatch()
                        Me.BufferSubsystem.ClearBuffer() ' ?@# removed 17-7-6
                        Me.TriggerSubsystem.Initiate()
                    End If
                Else
                    activity = $"{Me.Device.ResourceNameCaption} handling buffer clear: NOP"
                    Me.PublishVerbose($"{activity};. ")
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} operation not completed"
                Me.PublishVerbose($"{activity};. ")
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the Buffer Full handler added. </summary>
    ''' <value> The Buffer Full handler added. </value>
    Private Property BufferFullHandlerAdded As Boolean

    ''' <summary> Gets or sets the buffer full operation condition bit number. </summary>
    ''' <value> The buffer full operation condition bit number. </value>
    Private Property BufferFullOperationConditionBitNumber As Integer

    ''' <summary> Adds Buffer Full event handler. </summary>
    Private Sub AddBufferFullEventHandler()

        Dim activity As String = String.Empty
        If Not Me.BufferFullHandlerAdded Then

            ' clear execution state before enabling events
            activity = $"{Me.Device.ResourceNameCaption} Clearing execution state"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} Enabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.EnableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Adding device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.AddServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning on Buffer events"
            Me.PublishVerbose($"{activity};. ")
            Me.BufferFullOperationConditionBitNumber = 0
            Me.Device.StatusSubsystemBase.ApplyOperationEventMap(Me.BufferFullOperationConditionBitNumber, Me.Device.StatusSubsystemBase.BufferFullEventNumber, Me.Device.StatusSubsystemBase.BufferEmptyEventNumber)
            Me.Device.StatusSubsystemBase.ApplyOperationEventEnableBitmask(1 << Me.BufferFullOperationConditionBitNumber)

            activity = $"{Me.Device.ResourceNameCaption} Turning on status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)

            activity = $"{Me.Device.ResourceNameCaption} Adding re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            AddHandler Me.Device.ServiceRequested, AddressOf Me.HandleBufferFullRequest
            Me.BufferFullHandlerAdded = True

            Me.ClearBufferDisplay()
        End If
    End Sub

    ''' <summary> Removes the Buffer Full event handler. </summary>
    Private Sub RemoveBufferFullEventHandler()

        Dim activity As String = String.Empty
        If Me.BufferFullHandlerAdded Then

            activity = $"{Me.Device.ResourceNameCaption} Disabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.DisableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Removing device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.RemoveServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning off Buffer events"
            Me.PublishVerbose($"{activity};. ")
            Me.BufferFullOperationConditionBitNumber = 0
            Me.Device.StatusSubsystemBase.ApplyOperationEventMap(Me.BufferFullOperationConditionBitNumber, 0, 0)
            Me.Device.StatusSubsystemBase.ApplyOperationEventEnableBitmask(0)

            activity = $"{Me.Device.ResourceNameCaption} Turning off status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.None)

            activity = $"{Me.Device.ResourceNameCaption} Removing re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.HandleBufferFullRequest

            Me.BufferFullHandlerAdded = False

        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by _HandleBufferEventMenuItem for check state changed events.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleBufferEventMenuItem_CheckStateChanged(sender As Object, e As EventArgs)

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan()
            If menuItem.Checked Then
                activity = $"{Me.Device.ResourceNameCaption} Adding Buffer completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.AddBufferFullEventHandler()
            Else
                activity = $"{Me.Device.ResourceNameCaption} Removing Buffer completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.RemoveBufferFullEventHandler()
            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " TRIGGER CONTROLS ON THE READING PANEL "

    ''' <summary> Starts trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub StartTriggerMonitor()

        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()
            activity = $"{Me.Device.ResourceNameCaption} clearing buffer and display"
            Me.PublishVerbose($"{activity};. ")
            Me.ClearBufferDisplay()
            Me.BufferSubsystem.ClearBuffer()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.Initiate()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Event handler. Called by the Initiate Button for click events. Initiates a reading for
    ''' retrieval by way of the service request event.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortStartTriggerPlanMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AbortStartTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan()
            activity = $"{Me.Device.ResourceNameCaption} Starting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.StartTriggerMonitor()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initiate trigger plan menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitiateTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _InitiateTriggerPlanMenuItem.Click

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.TriggerSubsystem.StartElapsedStopwatch()
            Me.TriggerSubsystem.Initiate()
            Me.TriggerSubsystem.QueryTriggerState()
            Me.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by AbortButton for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AbortButton_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.AbortTriggerPlan()
    End Sub

#End Region

#Region " BUFFER "

    ''' <summary> Buffer size text box validating. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub BufferSizeNumeric_Validating(sender As Object, e As CancelEventArgs) Handles _BufferSizeNumeric.Validating
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplyBufferCapacity(CInt(Me._BufferSizeNumeric.Value))
    End Sub

    ''' <summary> Handles the DataError event of the _dataGridView control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
    '''                       event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BufferDataGridView_DataError(sender As Object, e As DataGridViewDataErrorEventArgs)
        Try
            ' prevent error reporting when adding a new row or editing a cell
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid IsNot Nothing Then
                If grid.CurrentRow IsNot Nothing AndAlso grid.CurrentRow.IsNewRow Then Return
                If grid.IsCurrentCellInEditMode Then Return
                If grid.IsCurrentRowDirty Then Return
                Dim activity As String = $"{Me.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString}"

                Me.PublishVerbose(activity)
                Me.InfoProvider.Annunciate(grid, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End If
        Catch
        End Try
    End Sub

    ''' <summary> Event handler. Called by DisplayBufferMenuItem for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DisplayBufferMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.DisplayBufferReadings()
    End Sub

    ''' <summary> Event handler. Called by ClearBufferDisplayMenuItem for click events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearBufferDisplayMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ClearBufferDisplay()
    End Sub


#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

