''' <summary> A session factory base. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-11-29 </para>
''' </remarks>
Public MustInherit Class SessionFactoryBase

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    Protected Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates Gpib interface SessionBase. </summary>
    ''' <returns> The new Gpib interface SessionBase. </returns>
    Public MustOverride Function GpibInterfaceSession() As VI.Pith.InterfaceSessionBase

    ''' <summary> Creates resources manager. </summary>
    ''' <returns> The new resources manager. </returns>
    Public MustOverride Function ResourcesProvider() As VI.Pith.ResourcesProviderBase

    ''' <summary> Creates the SessionBase. </summary>
    ''' <returns> The new SessionBase. </returns>
    Public MustOverride Function Session() As VI.Pith.SessionBase

End Class
