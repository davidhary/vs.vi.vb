''' <summary> Defines a SCPI Route Subsystem for Source Measure SCPI devices. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class RouteSubsystem
    Inherits VI.RouteSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "



#End Region

#Region " COMMAND SYNTAX "

#Region " CLOSED CHANNEL "

    ''' <summary> Gets the closed Channel query command. </summary>
    ''' <value> The closed Channel query command. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property ClosedChannelQueryCommand As String = String.Empty

    ''' <summary> Gets the closed Channel command format. </summary>
    ''' <value> The closed Channel command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property ClosedChannelCommandFormat As String = String.Empty

    ''' <summary> Gets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property OpenChannelCommandFormat As String = String.Empty

#End Region

#Region " CLOSED CHANNELS "

    ''' <summary> Gets the closed channels query command. </summary>
    ''' <value> The closed channels query command. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property ClosedChannelsQueryCommand As String = String.Empty

    ''' <summary> Gets the closed channels command format. </summary>
    ''' <value> The closed channels command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property ClosedChannelsCommandFormat As String = String.Empty

    ''' <summary> Gets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property OpenChannelsCommandFormat As String = String.Empty

#End Region

#Region " CHANNELS "

    ''' <summary> Gets the recall channel pattern command format. </summary>
    ''' <value> The recall channel pattern command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property RecallChannelPatternCommandFormat As String = String.Empty

    ''' <summary> Gets the save channel pattern command format. </summary>
    ''' <value> The save channel pattern command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property SaveChannelPatternCommandFormat As String = String.Empty

    ''' <summary> Gets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property OpenChannelsCommand As String = String.Empty

#End Region

#Region " SCAN LIST "

    ''' <summary> Gets or sets the scan list command query. </summary>
    ''' <value> The scan list query command. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property ScanListQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the scan list command format. </summary>
    ''' <value> The scan list command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property ScanListCommandFormat As String = String.Empty

#End Region

#Region " SLOT CARD TYPE "

    ''' <summary> Gets the slot card type command format. </summary>
    ''' <value> The slot card type command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property SlotCardTypeCommandFormat As String = String.Empty

    ''' <summary> Gets the slot card type query command format. </summary>
    ''' <value> The slot card type query command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property SlotCardTypeQueryCommandFormat As String = String.Empty

#End Region

#Region " SLOT CARD SETTLING TIME "

    ''' <summary> Gets the slot card settling time command format. </summary>
    ''' <value> The slot card settling time command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property SlotCardSettlingTimeCommandFormat As String = String.Empty

    ''' <summary> Gets the slot card settling time query command format. </summary>
    ''' <value> The slot card settling time query command format. </value>
    <Obsolete("Not supported for the Source Meters.")>
    Protected Overrides Property SlotCardSettlingTimeQueryCommandFormat As String = String.Empty

#End Region

#Region " TERMINAL MODE "

    ''' <summary> Gets or sets the terminals mode query command. </summary>
    ''' <value> The terminals mode command. </value>
    Protected Overrides Property TerminalsModeQueryCommand As String = ":ROUT:TERM?"

    ''' <summary> Gets or sets the terminals mode command format. </summary>
    ''' <value> The terminals mode command format. </value>
    Protected Overrides Property TerminalsModeCommandFormat As String = ":ROUT:TERM {0}"

#End Region

#End Region

End Class
