''' <summary> Defines the contract that must be implemented by a Prober Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class ProberSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="OutputSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ErrorRead = False
        Me.LastReading = String.Empty
        Me.IdentityRead = False
        Me.MessageCompleted = False
        Me.MessageFailed = False
        Me.PatternCompleteReceived = New Boolean?
        Me.SetModeSent = False
        Me.IsFirstTestStart = New Boolean?
        Me.RetestRequested = New Boolean?
        Me.TestAgainRequested = New Boolean?
        Me.TestCompleteSent = New Boolean?
        Me.TestStartReceived = New Boolean?
        Me.WaferStartReceived = New Boolean?
    End Sub

#End Region

#Region " ERROR READ "

    ''' <summary> True to error read. </summary>
    Private _ErrorRead As Boolean

    ''' <summary> Gets or sets the Error Read sentinel. </summary>
    ''' <value> The Error Read. </value>
    Public Property ErrorRead As Boolean
        Get
            Return Me._ErrorRead
        End Get
        Set(ByVal value As Boolean)
            Me._ErrorRead = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " IDENTITY READ "

    ''' <summary> True to identity read. </summary>
    Private _IdentityRead As Boolean

    ''' <summary> Gets or sets the Identity Read sentinel. </summary>
    ''' <value> The Identity Read. </value>
    Public Property IdentityRead As Boolean
        Get
            Return Me._IdentityRead
        End Get
        Set(ByVal value As Boolean)
            Me._IdentityRead = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " MESSAGE FAILED "

    ''' <summary> True if message failed. </summary>
    Private _MessageFailed As Boolean

    ''' <summary> Gets or sets the message failed. </summary>
    ''' <value> The message failed. </value>
    Public Property MessageFailed As Boolean
        Get
            Return Me._MessageFailed
        End Get
        Set(ByVal value As Boolean)
            Me._MessageFailed = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " MESSAGE COMPLETED "

    ''' <summary> True if message completed. </summary>
    Private _MessageCompleted As Boolean

    ''' <summary> Gets or sets the message Completed. </summary>
    ''' <value> The message Completed. </value>
    Public Property MessageCompleted As Boolean
        Get
            Return Me._MessageCompleted
        End Get
        Set(ByVal value As Boolean)
            Me._MessageCompleted = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " PATTERN COMPLETE "

    ''' <summary> A sentinel indicating that the Pattern Complete message was received. </summary>
    Private _PatternCompleteReceived As Boolean?

    ''' <summary> Gets or sets the cached Pattern Complete message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if Pattern Complete was received; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property PatternCompleteReceived As Boolean?
        Get
            Return Me._PatternCompleteReceived
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._PatternCompleteReceived = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
            If value.GetValueOrDefault(False) Then
                ' if pattern completed, turn off all other flags.
                Me.UnhandledMessageReceived = False
                Me.TestCompleteSent = False
                Me.TestStartReceived = False
                Me.WaferStartReceived = False
            End If
        End Set
    End Property

#End Region

#Region " SET MODE SENT "

    ''' <summary> A sentinel indicating that the Set Mode message was Sent. </summary>
    Private _SetModeSent As Boolean

    ''' <summary> Gets or sets the cached Set Mode message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if Set Mode was Sent; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property SetModeSent As Boolean
        Get
            Return Me._SetModeSent
        End Get
        Protected Set(ByVal value As Boolean)
            Me._SetModeSent = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " TEST COMPLETE SENT "

    ''' <summary> A sentinel indicating that the Test Complete message was Sent. </summary>
    Private _TestCompleteSent As Boolean?

    ''' <summary> Gets or sets the cached Test Complete message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if Test Complete was Sent; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property TestCompleteSent As Boolean?
        Get
            Return Me._TestCompleteSent
        End Get
        Set(ByVal value As Boolean?)
            Me._TestCompleteSent = value
            If value.GetValueOrDefault(False) Then
                ' if pattern completed, turn off all other flags.
                Me.UnhandledMessageReceived = False
                Me.TestStartReceived = False
            End If
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " TEST START "

    ''' <summary> A sentinel indicating that the test start message was received. </summary>
    Private _TestStartReceived As Boolean?

    ''' <summary> Gets or sets the cached test start message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if test start was received; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property TestStartReceived As Boolean?
        Get
            Return Me._TestStartReceived
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._TestStartReceived = value
            If value.GetValueOrDefault(False) Then
                ' turn off relevant sentinels.
                Me.UnhandledMessageReceived = False
                Me.PatternCompleteReceived = False
                Me.TestCompleteSent = False
            End If
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

    ''' <summary> A sentinel indicating that the retest is requested. </summary>
    Private _RetestRequested As Boolean?

    ''' <summary> Gets or sets the cached retest requested sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if retest is request; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property RetestRequested As Boolean?
        Get
            Return Me._RetestRequested
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._RetestRequested = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

    ''' <summary> A sentinel indicating that the Test Again is requested. </summary>
    Private _TestAgainRequested As Boolean?

    ''' <summary> Gets or sets the cached Test Again requested sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if TestAgain is request; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property TestAgainRequested As Boolean?
        Get
            Return Me._TestAgainRequested
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._TestAgainRequested = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " FIRST TEST START "

    ''' <summary> A sentinel indicating that the test start message was received. </summary>
    Private _IsFirstTestStart As Boolean?

    ''' <summary> Gets or sets the cached first test start message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if test start was received; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property IsFirstTestStart As Boolean?
        Get
            Return Me._IsFirstTestStart
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._IsFirstTestStart = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " WAFER START "

    ''' <summary> A sentinel indicating that the Wafer start message was received. </summary>
    Private _WaferStartReceived As Boolean?

    ''' <summary> Gets or sets the cached Wafer start message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if Wafer start was received; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property WaferStartReceived As Boolean?
        Get
            Return Me._WaferStartReceived
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._WaferStartReceived = value
            If value.GetValueOrDefault(False) Then
                ' turn off relevant sentinels.
                Me.UnhandledMessageReceived = False
                Me.PatternCompleteReceived = False
                Me.TestCompleteSent = False
                Me.TestStartReceived = False
            End If
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " UNHANDLED MESSAGE RECEIVED "

    ''' <summary> A sentinel indicating that the Unhandled Message was received. </summary>
    Private _UnhandledMessageReceived As Boolean?

    ''' <summary> Gets or sets the cached Unhandled Message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if Unhandled Message was received; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property UnhandledMessageReceived As Boolean?
        Get
            Return Me._UnhandledMessageReceived
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._UnhandledMessageReceived = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " UNHANDLED MESSAGE SENT "

    ''' <summary> A sentinel indicating that the Unhandled Message was Sent. </summary>
    Private _UnhandledMessageSent As Boolean?

    ''' <summary> Gets or sets the cached Unhandled Message sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if Unhandled Message was Sent; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property UnhandledMessageSent As Boolean?
        Get
            Return Me._UnhandledMessageSent
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._UnhandledMessageSent = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " FETCH "

    ''' <summary> The last reading. </summary>
    Private _LastReading As String

    ''' <summary> Gets or sets the last reading. </summary>
    ''' <value> The last reading. </value>
    Public Property LastReading As String
        Get
            Return Me._LastReading
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(Me.LastReading) Then
                If String.IsNullOrWhiteSpace(value) Then
                    Return
                Else
                    Me._LastReading = String.Empty
                End If
            End If
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            Me._LastReading = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

    ''' <summary> Parses the message. </summary>
    ''' <param name="reading"> The reading. </param>
    Public MustOverride Sub ParseReading(ByVal reading As String)

    ''' <summary>
    ''' Fetches and parses a message from the instrument. The message must already be present.
    ''' </summary>
    ''' <param name="readStatusDelay"> The read status delay. </param>
    ''' <param name="readDelay">       The read delay. </param>
    Public Sub FetchAndParse(ByVal readStatusDelay As TimeSpan, ByVal readDelay As TimeSpan)
        Me.PublishVerbose($"waiting read status delay {readStatusDelay:ss\.fff};. ")
        isr.Core.ApplianceBase.DoEventsWait(readStatusDelay)
        Me.PublishVerbose($"reading status;. ")
        Me.Session.ReadStatusRegister()
        Me.PublishVerbose($"waiting read delay {readDelay:ss\.fff};. ")
        isr.Core.ApplianceBase.DoEventsWait(readDelay)
        Me.PublishVerbose("Fetching;. ")
        Me.LastReading = Me.Session.ReadLineTrimEnd
        Me.PublishVerbose("Parsing;. {0}", Me.LastReading)
        Me.ParseReading(Me.LastReading)
    End Sub

    ''' <summary>
    ''' Fetches and parses a message from the instrument. The message must already be present.
    ''' </summary>
    Public Sub FetchAndParse()
        Me.PublishVerbose("Fetching;. ")
        Me.LastReading = Me.Session.ReadLineTrimEnd
        Me.PublishVerbose("Parsing;. {0}", Me.LastReading)
        Me.ParseReading(Me.LastReading)
    End Sub

    ''' <summary>
    ''' Fetches and parses a message from the instrument. The message must already be present.
    ''' </summary>
    ''' <param name="readDelay"> The read delay. </param>
    Public Sub FetchAndParse(ByVal readDelay As TimeSpan)
        Me.PublishVerbose($"waiting read delay {readDelay:ss\.fff};. ")
        isr.Core.ApplianceBase.DoEventsWait(readDelay)
        Me.PublishVerbose("Fetching;. ")
        Me.LastReading = Me.Session.ReadLineTrimEnd
        Me.PublishVerbose($"Parsing;. {Me.LastReading};. ")
        Me.ParseReading(Me.LastReading)
    End Sub

#End Region

End Class

