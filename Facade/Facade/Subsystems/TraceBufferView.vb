'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\TraceBufferView.vb
'
' summary:	Trace buffer view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.Core.EnumExtensions
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.Core.SplitExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> An trace buffer view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-01-13 </para>
''' </remarks>
Public Class TraceBufferView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._SizeNumeric.NumericUpDownControl.Minimum = 0
        Me._SizeNumeric.NumericUpDownControl.Maximum = Integer.MaxValue
        Me._SizeNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._SizeNumeric.NumericUpDownControl.Value = 2

        Me._PreTriggerCountNumeric.NumericUpDownControl.Minimum = 0
        Me._PreTriggerCountNumeric.NumericUpDownControl.Maximum = Integer.MaxValue
        Me._PreTriggerCountNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._PreTriggerCountNumeric.NumericUpDownControl.Value = 0

        ' Hide to be done items
        Me._ElementGroupToggleButton.Visible = False
        Me._PreTriggerCountNumeric.Visible = False
        Me._PreTriggerCountNumericLabel.Visible = False
        Me._TimestampFormatToggleButton.Visible = False

    End Sub

    ''' <summary> Creates a new TraceBufferView. </summary>
    ''' <returns> A TraceBufferView. </returns>
    Public Shared Function Create() As TraceBufferView
        Dim view As TraceBufferView = Nothing
        Try
            view = New TraceBufferView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Name of the buffer. </summary>
    Private _BufferName As String

    ''' <summary> Gets or sets the name of the buffer. </summary>
    ''' <value> The name of the buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property BufferName As String
        Get
            Return Me._BufferName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.BufferName) Then
                Me._BufferName = value
                Me._BufferNameLabel.Text = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the size of the buffer. </summary>
    ''' <value> The size of the buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property BufferSize As Integer
        Get
            Return CInt(Me._SizeNumeric.Value)
        End Get
        Set(value As Integer)
            If value <> Me.BufferSize Then
                Me._SizeNumeric.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the feed source. </summary>
    ''' <value> The feed source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property FeedSource As FeedSources
        Get
            Return CType(Me._FeedSourceComboBox.ComboBox.SelectedValue, FeedSources)
        End Get
        Set(value As FeedSources)
            If value <> Me.FeedSource Then
                Me._FeedSourceComboBox.ComboBox.SelectedItem = value.ValueNamePair
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the feed control. </summary>
    ''' <value> The feed control. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property FeedControl As FeedControls
        Get
            Return CType(Me._FeedControlComboBox.ComboBox.SelectedValue, FeedControls)
        End Get
        Set(value As FeedControls)
            If value <> Me.FeedSource Then
                Me._FeedControlComboBox.ComboBox.SelectedItem = value.ValueNamePair
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Applies the settings onto the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying {Me._SubsystemSplitButton.Text} settings " : Me.PublishInfo($"{activity};. ")
            Me.TraceSubsystem.StartElapsedStopwatch()
            Me.TraceSubsystem.ApplyPointsCount(CInt(Me._SizeNumeric.Value))
            Me.TraceSubsystem.ApplyFeedSource(CType(Me._FeedSourceComboBox.ComboBox.SelectedValue, FeedSources))
            Me.TraceSubsystem.ApplyFeedControl(CType(Me._FeedControlComboBox.ComboBox.SelectedValue, FeedControls))
            Me.TraceSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading  {Me._SubsystemSplitButton.Text} settings" : Me.PublishInfo($"{activity};. ")
            TraceBufferView.ReadSettings(Me.TraceSubsystem)
            Me.ApplyPropertyChanged(Me.TraceSubsystem)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the buffer to its blank/initial state. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Clear()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} Clearing {Me._SubsystemSplitButton.Text}" : Me.PublishInfo($"{activity};. ")
            Me.TraceSubsystem.StartElapsedStopwatch()
            Me.TraceSubsystem.ClearBuffer()
            Me.TraceSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(TraceBufferView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TRACE SUBSYSTEM "

    ''' <summary> Gets or sets the trace subsystem. </summary>
    ''' <value> The trace subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TraceSubsystem As TraceSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">  The subsystem. </param>
    ''' <param name="bufferName"> The name of the buffer. </param>
    Public Sub BindSubsystem(ByVal subsystem As TraceSubsystemBase, ByVal bufferName As String)
        If Me.TraceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TraceSubsystem)
            Me._TraceSubsystem = Nothing
        End If
        Me.BufferName = bufferName
        Me._TraceSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.TraceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TraceSubsystemBase)
        If add Then
            Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.SupportedFeedControls))
            Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.SupportedFeedSources))
            AddHandler subsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
            TraceBufferView.ReadSettings(subsystem)
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TraceSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As TraceSubsystemBase)
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.AvailablePointCount))
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.PointsCount))
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.FeedControl))
        Me.HandlePropertyChanged(subsystem, NameOf(TraceSubsystemBase.FeedSource))
    End Sub

    ''' <summary> Reads the settings from the instrument. </summary>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As TraceSubsystemBase)
        subsystem.StartElapsedStopwatch()
        subsystem.QueryBufferFreePointCount()
        subsystem.QueryPointsCount()
        subsystem.QueryFeedSource()
        subsystem.QueryFeedControl()
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the Calculate subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TraceSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName

            Case NameOf(TraceSubsystemBase.AvailablePointCount)
                Me._FreeCountLabel.Text = If(subsystem.AvailablePointCount Is Nothing, "?", subsystem.AvailablePointCount.Value.ToString)

            Case NameOf(TraceSubsystemBase.FeedControl)
                If subsystem.FeedControl.HasValue AndAlso Me._FeedControlComboBox.ComboBox.Items.Count > 0 Then
                    Me._FeedControlComboBox.ComboBox.SelectedItem = subsystem.FeedControl.Value.ValueNamePair
                End If

            Case NameOf(TraceSubsystemBase.FeedSource)
                If subsystem.FeedSource.HasValue AndAlso Me._FeedSourceComboBox.ComboBox.Items.Count > 0 Then
                    Me._FeedSourceComboBox.ComboBox.SelectedItem = subsystem.FeedSource.Value.ValueNamePair
                End If

            Case NameOf(TraceSubsystemBase.PointsCount)
                If subsystem.PointsCount.HasValue Then Me._SizeNumeric.Value = CDec(subsystem.PointsCount.Value)

            Case NameOf(TraceSubsystemBase.SupportedFeedControls)
                Me._FeedControlComboBox.ComboBox.ListSupportedFeedControls(subsystem.SupportedFeedControls)
                If subsystem.FeedControl.HasValue AndAlso Me._FeedControlComboBox.ComboBox.Items.Count > 0 Then
                    Me._FeedControlComboBox.ComboBox.SelectedItem = subsystem.FeedControl.Value.ValueNamePair
                End If

            Case NameOf(TraceSubsystemBase.SupportedFeedSources)
                Me._FeedSourceComboBox.ComboBox.ListSupportedFeedSources(subsystem.SupportedFeedSources)
                If subsystem.FeedSource.HasValue AndAlso Me._FeedSourceComboBox.ComboBox.Items.Count > 0 Then
                    Me._FeedSourceComboBox.ComboBox.SelectedItem = subsystem.FeedSource.Value.ValueNamePair
                End If

        End Select

    End Sub

    ''' <summary> Trace subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.TraceSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TraceSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TraceSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.TraceSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Clears the buffer button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearBufferButton_Click(sender As Object, e As EventArgs) Handles _ClearBufferButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.Clear()
    End Sub

    ''' <summary> Element group toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ElementGroupToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _ElementGroupToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Elements: {button.CheckState.ToCheckStateCaption("Full", "Compact", "?")}"
        End If
    End Sub

    ''' <summary> Timestamp format toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TimestampFormatToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _TimestampFormatToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Timestamp: {button.CheckState.ToCheckStateCaption("Absolute", "Delta", "?")}"
        End If
    End Sub

    ''' <summary> Applies the settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub


#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
