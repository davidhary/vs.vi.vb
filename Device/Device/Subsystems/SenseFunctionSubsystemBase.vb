''' <summary>
''' Defines the contract that must be implemented by a Sense function Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific ReSenses, Inc.<para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SenseFunctionSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SenseFunctionSubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefaultFunctionRange = VI.Pith.Ranges.NonnegativeFullRange
        Me.DefaultFunctionModeDecimalPlaces = 3
        Me.ReadingAmounts = readingAmounts
        Me.PowerLineCyclesRange = VI.Pith.Ranges.StandardPowerLineCyclesRange
        Me.FunctionUnit = Me.DefaultFunctionUnit
        Me.DefaultFunctionRange = VI.Pith.Ranges.NonnegativeFullRange
        Me.FunctionRange = Me.DefaultFunctionRange
        Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
        Me.DefineFunctionModeDecimalPlaces()
        Me.DefineFunctionModeReadWrites()
        Me.DefineFunctionModeRanges()
        Me.DefineFunctionModeUnits()
        Me.DefineAverageFilterTypesReadWrites()
        Me.DefineConfigurationModeReadWrites()
        Me.AverageCountRange = New Core.Constructs.RangeI(1, 100)
        Me.AveragePercentWindowRange = New Core.Constructs.RangeR(0.0, 1)
        Me.ResolutionDigitsRange = New Core.Constructs.RangeI(4, 9)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the clear execution state (CLS) by setting system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.DefineFunctionClearKnownState()
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.AutoZeroEnabled = True
        Me.AutoRangeEnabled = True
        Me.PowerLineCycles = 1
        Me.PowerLineCyclesRange = VI.Pith.Ranges.StandardPowerLineCyclesRange
        Me.FunctionRange = Me.DefaultFunctionRange
        Me.FunctionUnit = Me.DefaultFunctionUnit
        Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
        Me.AverageCount = 10
        Me.AveragePercentWindow = 1
    End Sub

#End Region

#Region " APERTURE (NPLC) "

    ''' <summary> The aperture range. </summary>
    Private _ApertureRange As Core.Constructs.RangeR

    ''' <summary> The Range of the Aperture. </summary>
    ''' <value> The aperture range. </value>
    Public Property ApertureRange As Core.Constructs.RangeR
        Get
            Return Me._ApertureRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.ApertureRange <> value Then
                Me._ApertureRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Aperture. </summary>
    Private _Aperture As Double?

    ''' <summary>
    ''' Gets or sets the cached sense Aperture. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Aperture As Double?
        Get
            Return Me._Aperture
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Aperture, value) Then
                Me._Aperture = value
                Me.NotifyPropertyChanged()
                Me.PowerLineCycles = If(value.HasValue,
                    StatusSubsystemBase.ToPowerLineCycles(TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * value.Value))),
                    New Double?)
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense Aperture. </summary>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Aperture. </returns>
    Public Function ApplyAperture(ByVal value As Double) As Double?
        Me.WriteAperture(value)
        Return Me.QueryAperture
    End Function

    ''' <summary> Gets or sets The Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overridable Property ApertureQueryCommand As String

    ''' <summary> Queries The Aperture. </summary>
    ''' <returns> The Aperture or none if unknown. </returns>
    Public Function QueryAperture() As Double?
        Me.Aperture = Me.Query(Me.Aperture, Me.ApertureQueryCommand)
        Return Me.Aperture
    End Function

    ''' <summary> Gets or sets The Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overridable Property ApertureCommandFormat As String

    ''' <summary> Writes The Aperture without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Aperture. </remarks>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Aperture. </returns>
    Public Function WriteAperture(ByVal value As Double) As Double?
        Me.Aperture = Me.Write(value, Me.ApertureCommandFormat)
        Return Me.Aperture
    End Function

#End Region

#Region " AUTO DELAY "

    ''' <summary> Gets or sets the supports automatic Delay. </summary>
    ''' <value> The supports automatic Delay. </value>
    Public ReadOnly Property SupportsAutoDelay As Boolean = False

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Auto Range enabled. </summary>
    Private _AutoRangeEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoRangeEnabled As Boolean?
        Get
            Return Me._AutoRangeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoRangeEnabled, value) Then
                Me._AutoRangeEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoRangeEnabled(value)
        Return Me.QueryAutoRangeEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <remarks> SCPI: "CURR:RANG:AUTO?". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overridable Property AutoRangeEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Range Enabled sentinel. Also sets the
    ''' <see cref="AutoRangeEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoRangeEnabled() As Boolean?
        Me.AutoRangeEnabled = Me.Query(Me.AutoRangeEnabled, Me.AutoRangeEnabledQueryCommand)
        Return Me.AutoRangeEnabled
    End Function

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <remarks> SCPI: "CURR:RANG:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overridable Property AutoRangeEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoRangeEnabled = Me.Write(value, Me.AutoRangeEnabledCommandFormat)
        Return Me.AutoRangeEnabled
    End Function

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Auto Zero enabled. </summary>
    Private _AutoZeroEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Zero Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Auto Zero Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoZeroEnabled As Boolean?
        Get
            Return Me._AutoZeroEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoZeroEnabled, value) Then
                Me._AutoZeroEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Zero Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoZeroEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoZeroEnabled(value)
        Return Me.QueryAutoZeroEnabled()
    End Function

    ''' <summary> Gets the automatic Zero enabled query command. </summary>
    ''' <remarks> SCPI: "CURR:AZER?". </remarks>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overridable Property AutoZeroEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Zero Enabled sentinel. Also sets the
    ''' <see cref="AutoZeroEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoZeroEnabled() As Boolean?
        Me.AutoZeroEnabled = Me.Query(Me.AutoZeroEnabled, Me.AutoZeroEnabledQueryCommand)
        Return Me.AutoZeroEnabled
    End Function

    ''' <summary> Gets the automatic Zero enabled command Format. </summary>
    ''' <remarks> SCPI: "CURR:AZER {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overridable Property AutoZeroEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Zero Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoZeroEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoZeroEnabled = Me.Write(value, Me.AutoZeroEnabledCommandFormat)
        Return Me.AutoZeroEnabled
    End Function

    ''' <summary> Gets the supports automatic zero. </summary>
    ''' <value> The supports automatic zero. </value>
    Public ReadOnly Property SupportsAutoZero As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.AutoZeroEnabledCommandFormat)
        End Get
    End Property

#End Region

#Region " CONFIGURATION MODE "

    ''' <summary> Define configuration mode read writes. </summary>
    Private Sub DefineConfigurationModeReadWrites()
        Me._ConfigurationModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As ConfigurationModes In [Enum].GetValues(GetType(ConfigurationModes))
            Me._ConfigurationModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Configuration Mode parses. </summary>
    ''' <value> A Dictionary of Configuration Mode parses. </value>
    Public ReadOnly Property ConfigurationModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported configuration modes. </summary>
    Private _SupportedConfigurationModes As ConfigurationModes

    ''' <summary> Gets or sets the supported Configuration Modes. </summary>
    ''' <value> The supported Configuration Modes. </value>
    Public Property SupportedConfigurationModes() As ConfigurationModes
        Get
            Return Me._SupportedConfigurationModes
        End Get
        Set(ByVal value As ConfigurationModes)
            If Not Me.SupportedConfigurationModes.Equals(value) Then
                Me._SupportedConfigurationModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The configuration mode. </summary>
    Private _ConfigurationMode As ConfigurationModes?

    ''' <summary> Gets or sets the cached source ConfigurationMode. </summary>
    ''' <value>
    ''' The <see cref="ConfigurationMode">source Configuration Mode</see> or none if not set or
    ''' unknown.
    ''' </value>
    Public Overloads Property ConfigurationMode As ConfigurationModes?
        Get
            Return Me._ConfigurationMode
        End Get
        Protected Set(ByVal value As ConfigurationModes?)
            If Not Me.ConfigurationMode.Equals(value) Then
                Me._ConfigurationMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Configuration Mode. </summary>
    ''' <param name="value"> The  Source Configuration Mode. </param>
    ''' <returns>
    ''' The <see cref="ConfigurationMode">source Configuration Mode</see> or none if unknown.
    ''' </returns>
    Public Function ApplyConfigurationMode(ByVal value As ConfigurationModes) As ConfigurationModes?
        Me.WriteConfigurationMode(value)
        Return Me.QueryConfigurationMode()
    End Function

    ''' <summary> Gets or sets the Configuration Mode query command. </summary>
    ''' <remarks> SCPI: :SENS:RES:MODE? </remarks>
    ''' <value> The Configuration Mode query command. </value>
    Protected Overridable Property ConfigurationModeQueryCommand As String

    ''' <summary> Queries the Configuration Mode. </summary>
    ''' <returns>
    ''' The <see cref="ConfigurationMode">Configuration Mode</see> or none if unknown.
    ''' </returns>
    Public Function QueryConfigurationMode() As ConfigurationModes?
        Me.ConfigurationMode = Me.Query(Of ConfigurationModes)(Me.ConfigurationModeQueryCommand,
                                                       Me.ConfigurationMode.GetValueOrDefault(ConfigurationModes.None),
                                                       Me.ConfigurationModeReadWrites)
        Return Me.ConfigurationMode
    End Function

    ''' <summary> Gets or sets the Configuration Mode command format. </summary>
    ''' <remarks> SCPI: :SENS:RES:MODE {0}. </remarks>
    ''' <value> The write Configuration Mode command format. </value>
    Protected Overridable Property ConfigurationModeCommandFormat As String

    ''' <summary>
    ''' Writes the Configuration Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Configuration Mode. </param>
    ''' <returns>
    ''' The <see cref="ConfigurationMode">Configuration Mode</see> or none if unknown.
    ''' </returns>
    Public Function WriteConfigurationMode(ByVal value As ConfigurationModes) As ConfigurationModes?
        Me.ConfigurationMode = Me.Write(Of ConfigurationModes)(Me.ConfigurationModeCommandFormat, value, Me.ConfigurationModeReadWrites)
        Return Me.ConfigurationMode
    End Function

#End Region

#Region " DELAY "

    ''' <summary> The delay. </summary>
    Private _Delay As TimeSpan?

    ''' <summary> Gets or sets the cached Trigger Delay. </summary>
    ''' <remarks>
    ''' The delay is used to delay operation in the trigger layer. After the programmed trigger event
    ''' occurs, the instrument waits until the delay period expires before performing the Device
    ''' Action.
    ''' </remarks>
    ''' <value> The Trigger Delay or none if not set or unknown. </value>
    Public Overloads Property Delay As TimeSpan?
        Get
            Return Me._Delay
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Nullable.Equals(Me.Delay, value) Then
                Me._Delay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Delay. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Trigger Delay or none if unknown. </returns>
    Public Function ApplyDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteDelay(value)
        Return Me.QueryDelay()
    End Function

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <remarks> SCPI: ":SENS:FRES:DEL?". </remarks>
    ''' <value> The delay query command. </value>
    Protected Overridable Property DelayQueryCommand As String

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
    ''' <value> The Delay query command. </value>
    Protected Overridable Property DelayFormat As String

    ''' <summary> Queries the Delay. </summary>
    ''' <returns> The Delay or none if unknown. </returns>
    Public Function QueryDelay() As TimeSpan?
        Me.Delay = Me.Query(Me.Delay, Me.DelayFormat, Me.DelayQueryCommand)
        Return Me.Delay
    End Function

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <remarks> SCPI: ":SENS:FRES:DEL {0:s\.FFFFFFF}". </remarks>
    ''' <value> The delay command format. </value>
    Protected Overridable Property DelayCommandFormat As String

    ''' <summary> Writes the Trigger Delay without reading back the value from the device. </summary>
    ''' <param name="value"> The current Delay. </param>
    ''' <returns> The Trigger Delay or none if unknown. </returns>
    Public Function WriteDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.Delay = Me.Write(value, Me.DelayCommandFormat)
        Return Me.Delay
    End Function

#End Region

#Region " OPEN LEAD DETECTOR ENABLED "

    ''' <summary> Open Lead Detector enabled. </summary>
    Private _OpenLeadDetectorEnabled As Boolean?

    ''' <summary> Gets or sets the cached Open Lead Detector Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Open Lead Detector Enabled is not known; <c>True</c> if output is on;
    ''' otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property OpenLeadDetectorEnabled As Boolean?
        Get
            Return Me._OpenLeadDetectorEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.OpenLeadDetectorEnabled, value) Then
                Me._OpenLeadDetectorEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Open Lead Detector Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyOpenLeadDetectorEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteOpenLeadDetectorEnabled(value)
        Return Me.QueryOpenLeadDetectorEnabled()
    End Function

    ''' <summary> Gets the Open Lead Detector enabled query command. </summary>
    ''' <remarks> SCPI: ":FRES:ODET?". </remarks>
    ''' <value> The Open Lead Detector enabled query command. </value>
    Protected Overridable Property OpenLeadDetectorEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Open Lead Detector Enabled sentinel. Also sets the
    ''' <see cref="OpenLeadDetectorEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryOpenLeadDetectorEnabled() As Boolean?
        Me.OpenLeadDetectorEnabled = Me.Query(Me.OpenLeadDetectorEnabled, Me.OpenLeadDetectorEnabledQueryCommand)
        Return Me.OpenLeadDetectorEnabled
    End Function

    ''' <summary> Gets the Open Lead Detector enabled command Format. </summary>
    ''' <remarks> SCPI: ":FRES:ODET {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Open Lead Detector enabled query command. </value>
    Protected Overridable Property OpenLeadDetectorEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Open Lead Detector Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteOpenLeadDetectorEnabled(ByVal value As Boolean) As Boolean?
        Me.OpenLeadDetectorEnabled = Me.Write(value, Me.OpenLeadDetectorEnabledCommandFormat)
        Return Me.OpenLeadDetectorEnabled
    End Function

    ''' <summary> Gets the supports open lead detector. </summary>
    ''' <value> The supports open lead detector. </value>
    Public ReadOnly Property SupportsOpenLeadDetector As Boolean
        Get
            Return Not String.IsNullOrEmpty(Me.OpenLeadDetectorEnabledCommandFormat)
        End Get
    End Property

#End Region

#Region " POWER LINE CYCLES (NPLC) "

    ''' <summary> Gets the power line cycles decimal places. </summary>
    ''' <value> The power line decimal places. </value>
    Public ReadOnly Property PowerLineCyclesDecimalPlaces As Integer
        Get
            Return CInt(Math.Max(0, 1 - Math.Log10(Me.PowerLineCyclesRange.Min)))
        End Get
    End Property

    ''' <summary> The power line cycles range. </summary>
    Private _PowerLineCyclesRange As Core.Constructs.RangeR

    ''' <summary> The Range of the power line cycles. </summary>
    ''' <value> The power line cycles range. </value>
    Public Property PowerLineCyclesRange As Core.Constructs.RangeR
        Get
            Return Me._PowerLineCyclesRange
        End Get
        Set(value As Core.Constructs.RangeR)
            ' force a unit change as the value needs to be updated when the subsystem is switched.
            Me._PowerLineCyclesRange = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The Power Line Cycles. </summary>
    Private _PowerLineCycles As Double?

    ''' <summary> Gets the integration period. </summary>
    ''' <value> The integration period. </value>
    Public ReadOnly Property IntegrationPeriod As TimeSpan?
        Get
            Return If(Me.PowerLineCycles.HasValue, StatusSubsystemBase.FromPowerLineCycles(Me.PowerLineCycles.Value), New TimeSpan?)
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the cached sense PowerLineCycles. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property PowerLineCycles As Double?
        Get
            Return Me._PowerLineCycles
        End Get
        Protected Set(ByVal value As Double?)
            ' force a unit change as the value needs to be updated when the subsystem is switched.
            Me._PowerLineCycles = value
            Me.Aperture = If(value.HasValue, StatusSubsystemBase.FromPowerLineCycles(Me.PowerLineCycles.Value).TotalSeconds, New Double?)
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Writes and reads back the sense PowerLineCycles. </summary>
    ''' <param name="value"> The Power Line Cycles. </param>
    ''' <returns> The Power Line Cycles. </returns>
    Public Function ApplyPowerLineCycles(ByVal value As Double) As Double?
        Me.WritePowerLineCycles(value)
        Return Me.QueryPowerLineCycles
    End Function

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overridable Property PowerLineCyclesQueryCommand As String

    ''' <summary> Queries The Power Line Cycles. </summary>
    ''' <returns> The Power Line Cycles or none if unknown. </returns>
    Public Function QueryPowerLineCycles() As Double?
        Me.PowerLineCycles = Me.Query(Me.PowerLineCycles, Me.PowerLineCyclesQueryCommand)
        Return Me.PowerLineCycles
    End Function

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overridable Property PowerLineCyclesCommandFormat As String

    ''' <summary>
    ''' Writes The Power Line Cycles without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Power Line Cycles. </remarks>
    ''' <param name="value"> The Power Line Cycles. </param>
    ''' <returns> The Power Line Cycles. </returns>
    Public Function WritePowerLineCycles(ByVal value As Double) As Double?
        Me.PowerLineCycles = Me.Write(value, Me.PowerLineCyclesCommandFormat)
        Return Me.PowerLineCycles
    End Function

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> The Current Limit. </summary>
    Private _ProtectionLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached source current Limit for a voltage source. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property ProtectionLevel As Double?
        Get
            Return Me._ProtectionLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.ProtectionLevel, value) Then
                Me._ProtectionLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the protection level. </summary>
    ''' <param name="value"> the protection level. </param>
    ''' <returns> the protection level. </returns>
    Public Function ApplyProtectionLevel(ByVal value As Double) As Double?
        Me.WriteProtectionLevel(value)
        Return Me.QueryProtectionLevel
    End Function

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overridable Property ProtectionLevelQueryCommand As String

    ''' <summary> Queries the protection level. </summary>
    ''' <returns> the protection level or none if unknown. </returns>
    Public Function QueryProtectionLevel() As Double?
        Me.ProtectionLevel = Me.Query(Me.ProtectionLevel, Me.ProtectionLevelQueryCommand)
        Return Me.ProtectionLevel
    End Function

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overridable Property ProtectionLevelCommandFormat As String

    ''' <summary>
    ''' Writes the protection level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets the protection level. </remarks>
    ''' <param name="value"> the protection level. </param>
    ''' <returns> the protection level. </returns>
    Public Function WriteProtectionLevel(ByVal value As Double) As Double?
        Me.ProtectionLevel = Me.Write(value, Me.ProtectionLevelCommandFormat)
        Return Me.ProtectionLevel
    End Function

#End Region

#Region " PROTECTION ENABLED "

    ''' <summary> Protection enabled. </summary>
    Private _ProtectionEnabled As Boolean?

    ''' <summary>
    ''' Gets or sets a cached value indicating whether Sense Voltage protection is enabled.
    ''' </summary>
    ''' <remarks>
    ''' :SENSE:VOLT:PROT:STAT The setter enables or disables the over-Voltage protection (OCP)
    ''' function. The enabled state is On (1); the disabled state is Off (0). If the over-Voltage
    ''' protection function is enabled and the output goes into constant Voltage operation, the
    ''' output is disabled and OCP is set in the Questionable Condition status register. The *RST
    ''' value = Off.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if state is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property ProtectionEnabled As Boolean?
        Get
            Return Me._ProtectionEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ProtectionEnabled, value) Then
                Me._ProtectionEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Protection Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyProtectionEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteProtectionEnabled(value)
        Return Me.QueryProtectionEnabled()
    End Function

    ''' <summary> Gets or sets the Protection enabled query command. </summary>
    ''' <remarks> SCPI: ":SENSE:PROT:STAT?". </remarks>
    ''' <value> The Protection enabled query command. </value>
    Protected Overridable Property ProtectionEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Protection Enabled sentinel. Also sets the
    ''' <see cref="ProtectionEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryProtectionEnabled() As Boolean?
        Me.ProtectionEnabled = Me.Query(Me.ProtectionEnabled, Me.ProtectionEnabledQueryCommand)
        Return Me.ProtectionEnabled
    End Function

    ''' <summary> Gets or sets the Protection enabled command Format. </summary>
    ''' <remarks> SCPI: ""SENSE:PROT:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Protection enabled query command. </value>
    Protected Overridable Property ProtectionEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Protection Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteProtectionEnabled(ByVal value As Boolean) As Boolean?
        Me.ProtectionEnabled = Me.Write(value, Me.ProtectionEnabledCommandFormat)
        Return Me.ProtectionEnabled
    End Function

#End Region

#Region " RANGE "

    ''' <summary> The range. </summary>
    Private _Range As Double?

    ''' <summary>
    ''' Gets or sets the cached range. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Range As Double?
        Get
            Return Me._Range
        End Get
        Protected Set(ByVal value As Double?)
            ' force a unit change as the value needs to be updated when the subsystem is switched.
            Me._Range = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Writes and reads back the range. </summary>
    ''' <param name="value"> The range. </param>
    ''' <returns> The range. </returns>
    Public Function ApplyRange(ByVal value As Double) As Double?
        Me.WriteRange(value)
        Return Me.QueryRange
    End Function

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overridable Property RangeQueryCommand As String

    ''' <summary> Queries the range. </summary>
    ''' <returns> The range or none if unknown. </returns>
    Public Function QueryRange() As Double?
        Me.Range = Me.Query(Me.Range, Me.RangeQueryCommand)
        Return Me.Range
    End Function

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overridable Property RangeCommandFormat As String

    ''' <summary> Writes the range without reading back the value from the device. </summary>
    ''' <remarks> This command sets the range. </remarks>
    ''' <param name="value"> The range. </param>
    ''' <returns> The range. </returns>
    Public Function WriteRange(ByVal value As Double) As Double?
        Me.Range = Me.Write(value, Me.RangeCommandFormat)
        Return Me.Range
    End Function

#End Region

#Region " RESOLUTION DIGITS "

    ''' <summary> The resolution digits range. </summary>
    Private _ResolutionDigitsRange As Core.Constructs.RangeI

    ''' <summary> The resolution digits range in seconds. </summary>
    ''' <value> The resolution digits range. </value>
    Public Property ResolutionDigitsRange As Core.Constructs.RangeI
        Get
            Return Me._ResolutionDigitsRange
        End Get
        Set(value As Core.Constructs.RangeI)
            If Me.ResolutionDigitsRange <> value Then
                Me._ResolutionDigitsRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The ResolutionDigits. </summary>
    Private _ResolutionDigits As Double?

    ''' <summary> Gets or sets the cached ResolutionDigits. </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property ResolutionDigits As Double?
        Get
            Return Me._ResolutionDigits
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.ResolutionDigits, value) Then
                Me._ResolutionDigits = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the ResolutionDigits. </summary>
    ''' <param name="value"> The ResolutionDigits. </param>
    ''' <returns> The ResolutionDigits. </returns>
    Public Function ApplyResolutionDigits(ByVal value As Double) As Double?
        Me.WriteResolutionDigits(value)
        Return Me.QueryResolutionDigits
    End Function

    ''' <summary> Gets or sets the ResolutionDigits query command. </summary>
    ''' <value> The ResolutionDigits query command. </value>
    Protected Overridable Property ResolutionDigitsQueryCommand As String

    ''' <summary> Queries the ResolutionDigits. </summary>
    ''' <returns> The ResolutionDigits or none if unknown. </returns>
    Public Function QueryResolutionDigits() As Double?
        Me.ResolutionDigits = Me.Query(Me.ResolutionDigits, Me.ResolutionDigitsQueryCommand)
        Return Me.ResolutionDigits
    End Function

    ''' <summary> Gets or sets the ResolutionDigits command format. </summary>
    ''' <value> The ResolutionDigits command format. </value>
    Protected Overridable Property ResolutionDigitsCommandFormat As String

    ''' <summary>
    ''' Writes the ResolutionDigits without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets the ResolutionDigits. </remarks>
    ''' <param name="value"> The ResolutionDigits. </param>
    ''' <returns> The ResolutionDigits. </returns>
    Public Function WriteResolutionDigits(ByVal value As Double) As Double?
        Me.ResolutionDigits = Me.Write(value, Me.ResolutionDigitsCommandFormat)
        Return Me.ResolutionDigits
    End Function

#End Region

End Class

''' <summary> Enumerates the configuration mode. </summary>
<Flags>
Public Enum ConfigurationModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the auto] option. </summary>
    <ComponentModel.Description("Auto (AUTO)")>
    [Auto] = 1

    ''' <summary> An enum constant representing the manual] option. </summary>
    <ComponentModel.Description("Manual (MAN)")>
    [Manual] = 2
End Enum

