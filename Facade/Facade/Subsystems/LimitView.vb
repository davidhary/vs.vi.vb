'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\LimitView.vb
'
' summary:	Limit view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports isr.VI.ExceptionExtensions
Imports isr.Core.SplitExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions

''' <summary> A Limit Subsystem view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class LimitView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._UpperLimitBitPatternNumeric.NumericUpDownControl.Minimum = 1
        Me._UpperLimitBitPatternNumeric.NumericUpDownControl.Maximum = 63
        Me._UpperLimitBitPatternNumeric.NumericUpDownControl.Value = 16

        Me._LowerLimitBitPatternNumeric.NumericUpDownControl.Minimum = 1
        Me._LowerLimitBitPatternNumeric.NumericUpDownControl.Maximum = 63
        Me._LowerLimitBitPatternNumeric.NumericUpDownControl.Value = 48

        Me._UpperLimitDecimalsNumeric.NumericUpDownControl.Minimum = 0
        Me._UpperLimitDecimalsNumeric.NumericUpDownControl.Maximum = 10
        Me._UpperLimitDecimalsNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._UpperLimitDecimalsNumeric.NumericUpDownControl.Value = 3

        Me._UpperLimitNumeric.NumericUpDownControl.Minimum = 0
        Me._UpperLimitNumeric.NumericUpDownControl.Maximum = 5000000D
        Me._UpperLimitNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._UpperLimitNumeric.NumericUpDownControl.Value = 9

        Me._LowerLimitNumeric.NumericUpDownControl.Minimum = 0
        Me._LowerLimitNumeric.NumericUpDownControl.Maximum = 5000000D
        Me._LowerLimitNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._LowerLimitNumeric.NumericUpDownControl.Value = 90

        Me._LowerLimitDecimalsNumeric.NumericUpDownControl.Minimum = 0
        Me._LowerLimitDecimalsNumeric.NumericUpDownControl.Maximum = 10
        Me._LowerLimitDecimalsNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._LowerLimitDecimalsNumeric.NumericUpDownControl.Value = 3

        Me._UpperLimitNumeric.NumericUpDownControl.DecimalPlaces = CInt(Me._UpperLimitDecimalsNumeric.Value)
        Me._LowerLimitNumeric.NumericUpDownControl.DecimalPlaces = CInt(Me._LowerLimitDecimalsNumeric.Value)

    End Sub

    ''' <summary> Creates a new LimitView. </summary>
    ''' <returns> A LimitView. </returns>
    Public Shared Function Create() As LimitView
        Dim view As LimitView = Nothing
        Try
            view = New LimitView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> The limit number. </summary>
    Private _LimitNumber As Integer

    ''' <summary> Gets or sets the limit number. </summary>
    ''' <value> The limit number. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property LimitNumber As Integer
        Get
            Return Me._LimitNumber
        End Get
        Set(value As Integer)
            If value <> Me.LimitNumber Then
                Me._LimitNumber = value
                Me._SubsystemSplitButton.Text = $"Limit{value}"
            End If
        End Set
    End Property

    ''' <summary> Applies the settings onto the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} applying Binning subsystem {Me._SubsystemSplitButton.Text} state" : Me.PublishInfo($"{activity};. ")
            Me.BinningSubsystem.StartElapsedStopwatch()
            If Me.LimitNumber = 1 Then
                Me.BinningSubsystem.ApplyLimit1AutoClear(Me._AutoClearToggleButton.CheckState = CheckState.Checked)
                Me.BinningSubsystem.ApplyLimit1Enabled(Me._LimitEnabledToggleButton.CheckState = CheckState.Checked)
                Me.BinningSubsystem.ApplyLimit1LowerLevel(Me._LowerLimitNumeric.Value)
                Me.BinningSubsystem.ApplyLimit1LowerSource(CInt(Me._LowerLimitBitPatternNumeric.Value))
                Me.BinningSubsystem.ApplyLimit1UpperLevel(Me._UpperLimitNumeric.Value)
                Me.BinningSubsystem.ApplyLimit1UpperSource(CInt(Me._LowerLimitBitPatternNumeric.Value))
            Else
                Me.BinningSubsystem.ApplyLimit2AutoClear(Me._AutoClearToggleButton.CheckState = CheckState.Checked)
                Me.BinningSubsystem.ApplyLimit2Enabled(Me._LimitEnabledToggleButton.CheckState = CheckState.Checked)
                Me.BinningSubsystem.ApplyLimit2LowerLevel(Me._LowerLimitNumeric.Value)
                Me.BinningSubsystem.ApplyLimit2LowerSource(CInt(Me._LowerLimitBitPatternNumeric.Value))
                Me.BinningSubsystem.ApplyLimit2UpperLevel(Me._UpperLimitNumeric.Value)
                Me.BinningSubsystem.ApplyLimit2UpperSource(CInt(Me._LowerLimitBitPatternNumeric.Value))
            End If
            Me.BinningSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads settings from the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            Me.Device.Session.EnableServiceRequestWaitComplete()
            activity = $"{Me.Device.ResourceNameCaption} reading Binning subsystem {Me._SubsystemSplitButton.Text} state" : Me.PublishInfo($"{activity};. ")
            LimitView.ReadSettings(Me.BinningSubsystem, Me.LimitNumber)
            Me.ApplyPropertyChanged(Me.BinningSubsystem)
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Preform a limit test and read the fail state. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub PerformLimitTest()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing exception state" : Me.PublishInfo($"{activity};. ")
            Me.BinningSubsystem.StartElapsedStopwatch()
            Me.BinningSubsystem.Immediate()
            Me.BinningSubsystem.QueryLimitsFailed()
            Me.BinningSubsystem.QueryLimit1Failed()
            Me.BinningSubsystem.QueryLimit2Failed()
            Me.BinningSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> read limit test result. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadLimitTestState()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.BinningSubsystem.StartElapsedStopwatch()
            If Me.LimitNumber = 1 Then
                Me.BinningSubsystem.QueryLimit1Failed()
            Else
                Me.BinningSubsystem.QueryLimit2Failed()
            End If
            Me.Device.StatusSubsystemBase.QueryMeasurementEventStatus()
            Me.BinningSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemToolStrip, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub


#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishVerbose($"{value.ResourceNameCaption} assigned to {NameOf(LimitView).SplitWords}")
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " BINNING "

    ''' <summary> Gets or sets the binning subsystem. </summary>
    ''' <value> The binning subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BinningSubsystem As BinningSubsystemBase

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="limitNumber"> The limit number. </param>
    Public Sub BindSubsystem(ByVal subsystem As BinningSubsystemBase, ByVal limitNumber As Integer)
        If Me.BinningSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.BinningSubsystem)
            Me._BinningSubsystem = Nothing
        End If
        Me.LimitNumber = limitNumber
        Me._BinningSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.BinningSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As BinningSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.BinningSubsystemPropertyChanged
            LimitView.ReadSettings(Me.BinningSubsystem, Me.LimitNumber)
            Me.ReadLimitTestState()
            Me.ApplyPropertyChanged(subsystem)
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.BinningSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Applies the property changed described by subsystem. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub ApplyPropertyChanged(ByVal subsystem As BinningSubsystemBase)
        Select Case Me.LimitNumber
            Case 1
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1AutoClear))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1Enabled))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1Failed))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1LowerLevel))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1LowerSource))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1UpperLevel))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit1UpperSource))
            Case 2
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2AutoClear))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2Enabled))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2Failed))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2LowerLevel))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2LowerSource))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2UpperLevel))
                Me.HandlePropertyChanged(subsystem, NameOf(BinningSubsystemBase.Limit2UpperSource))
            Case Else
                Throw New InvalidOperationException($"Limit number {Me.LimitNumber} must be either 1 or 2")
        End Select
    End Sub

    ''' <summary> Reads settings from the instrument. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="limitNumber"> The limit number. </param>
    Private Shared Sub ReadSettings(ByVal subsystem As BinningSubsystemBase, ByVal limitNumber As Integer)
        subsystem.StartElapsedStopwatch()
        If limitNumber = 1 Then
            subsystem.QueryLimit1AutoClear()
            subsystem.QueryLimit1Enabled()
            subsystem.QueryLimit1LowerLevel()
            subsystem.QueryLimit1LowerSource()
            subsystem.QueryLimit1UpperLevel()
            subsystem.QueryLimit1UpperSource()
        Else
            subsystem.QueryLimit2AutoClear()
            subsystem.QueryLimit2Enabled()
            subsystem.QueryLimit2LowerLevel()
            subsystem.QueryLimit2LowerSource()
            subsystem.QueryLimit2UpperLevel()
            subsystem.QueryLimit2UpperSource()
        End If
        subsystem.StopElapsedStopwatch()
    End Sub

    ''' <summary> Handle the Calculate subsystem property changed event. </summary>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As BinningSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If Me.LimitNumber = 1 Then
            Select Case propertyName
                Case NameOf(BinningSubsystemBase.Limit1AutoClear)
                    Me._AutoClearToggleButton.CheckState = subsystem.Limit1AutoClear.ToCheckState

                Case NameOf(BinningSubsystemBase.Limit1Enabled)
                    Me._LimitEnabledToggleButton.CheckState = subsystem.Limit1Enabled.ToCheckState

                Case NameOf(BinningSubsystemBase.Limit1Failed)
                    Me._LimitFailedButton.CheckState = subsystem.Limit1Failed.ToCheckState

                Case NameOf(BinningSubsystemBase.Limit1LowerLevel)
                    If subsystem.Limit1LowerLevel.HasValue Then Me._LowerLimitNumeric.Value = CDec(subsystem.Limit1LowerLevel.Value)

                Case NameOf(BinningSubsystemBase.Limit1LowerSource)
                    If subsystem.Limit1LowerSource.HasValue Then Me._LowerLimitBitPatternNumeric.Value = CDec(subsystem.Limit1LowerSource.Value)

                Case NameOf(BinningSubsystemBase.Limit1UpperLevel)
                    If subsystem.Limit1UpperLevel.HasValue Then Me._UpperLimitNumeric.Value = CDec(subsystem.Limit1UpperLevel.Value)

                Case NameOf(BinningSubsystemBase.Limit1UpperSource)
                    If subsystem.Limit1UpperSource.HasValue Then Me._UpperLimitBitPatternNumeric.Value = CDec(subsystem.Limit1UpperSource.Value)

            End Select
        Else
            Select Case propertyName
                Case NameOf(BinningSubsystemBase.Limit2AutoClear)
                    Me._AutoClearToggleButton.CheckState = subsystem.Limit2AutoClear.ToCheckState

                Case NameOf(BinningSubsystemBase.Limit2Enabled)
                    Me._LimitEnabledToggleButton.CheckState = subsystem.Limit2Enabled.ToCheckState

                Case NameOf(BinningSubsystemBase.Limit2Failed)
                    Me._LimitFailedButton.CheckState = subsystem.Limit2Failed.ToCheckState

                Case NameOf(BinningSubsystemBase.Limit2LowerLevel)
                    If subsystem.Limit2LowerLevel.HasValue Then Me._LowerLimitNumeric.Value = CDec(subsystem.Limit2LowerLevel.Value)

                Case NameOf(BinningSubsystemBase.Limit2LowerSource)
                    If subsystem.Limit2LowerSource.HasValue Then Me._LowerLimitBitPatternNumeric.Value = CDec(subsystem.Limit2LowerSource.Value)

                Case NameOf(BinningSubsystemBase.Limit2UpperLevel)
                    If subsystem.Limit2UpperLevel.HasValue Then Me._UpperLimitNumeric.Value = CDec(subsystem.Limit2UpperLevel.Value)

                Case NameOf(BinningSubsystemBase.Limit2UpperSource)
                    If subsystem.Limit2UpperSource.HasValue Then Me._UpperLimitBitPatternNumeric.Value = CDec(subsystem.Limit2UpperSource.Value)

            End Select
        End If
    End Sub

    ''' <summary> Binning subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BinningSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.BinningSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.BinningSubsystemPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.BinningSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.BinningSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: CALCULATE "

    ''' <summary> Automatic clear button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoClearButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _AutoClearToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Auto Clear: {button.CheckState.ToCheckStateCaption("On", "Off", "?")}"
        End If
    End Sub

    ''' <summary> Limit enabled toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LimitEnabledToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _LimitEnabledToggleButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = button.CheckState.ToCheckStateCaption("Enabled", "Disabled", "Enabled ?")
        End If
    End Sub

    ''' <summary> Limit failed toggle button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LimitFailedToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _LimitFailedButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = button.CheckState.ToCheckStateCaption("Fail", "Pass", "P/F ?")
        End If
    End Sub

    ''' <summary> Lower bit pattern numeric button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LowerBitPatternNumericButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _LowerLimitBitPatternNumericButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            Me._LowerLimitBitPatternNumeric.NumericUpDownControl.Hexadecimal = button.Checked
            button.Text = $"Source {If(button.Checked, "0x", "0d")}"
        End If
    End Sub

    ''' <summary> Upper limit bit pattern numeric button check state changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UpperLimitBitPatternNumericButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _UpperLimitBitPatternNumericButton.CheckStateChanged
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button IsNot Nothing Then
            button.Text = $"Source {If(button.Checked, "0x", "0d")}"
            Me._UpperLimitBitPatternNumeric.NumericUpDownControl.Hexadecimal = button.Checked
        End If
    End Sub

    ''' <summary> Upper limit decimals numeric value changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub UpperLimitDecimalsNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _UpperLimitDecimalsNumeric.ValueChanged
        If Me._UpperLimitNumeric IsNot Nothing Then
            Me._UpperLimitNumeric.NumericUpDownControl.DecimalPlaces = CInt(Me._UpperLimitDecimalsNumeric.Value)
        End If
    End Sub

    ''' <summary> Lower limit decimals numeric value changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LowerLimitDecimalsNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _LowerLimitDecimalsNumeric.ValueChanged
        If Me._LowerLimitNumeric IsNot Nothing Then
            Me._LowerLimitNumeric.NumericUpDownControl.DecimalPlaces = CInt(Me._LowerLimitDecimalsNumeric.Value)
        End If
    End Sub

    ''' <summary> Applies the settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Performs the limit strip menu item click action. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PerformLimitStripMenuItem_Click(sender As Object, e As EventArgs) Handles _PerformLimitStripMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.PerformLimitTest()
    End Sub

    ''' <summary> Reads limit test menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadLimitTestMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadLimitTestMenuItem.Click
        If Me.InitializingComponents Then Return
        Me.ReadLimitTestState()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
