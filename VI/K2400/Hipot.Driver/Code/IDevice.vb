''' <summary>
''' Defines an interface for the high potential insulation resistance meter device.
''' </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-12-25, 1.0.4011.x. </para>
''' </remarks>
Public Interface IDevice
    Inherits IDisposable

#Region " CONNECT "

    ''' <summary> Connects to the thermal transient device. </summary>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <returns> <c>True</c> if the device connected. </returns>
    Function Connect(ByVal resourceName As String) As Boolean

    ''' <summary> Disconnects from the thermal transient meter. </summary>
    ''' <returns> <c>True</c> if the device  disconnected. </returns>
    Function Disconnect() As Boolean

    ''' <summary> Flushes the read buffers. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function FlushRead() As Boolean

    ''' <summary> Gets or sets the device identity. </summary>
    ''' <value> The identity. </value>
    ReadOnly Property Identity() As String

    ''' <summary>
    ''' Gets or sets the sentinel indicating if the thermal transient meter is connected.
    ''' </summary>
    ''' <value> The is connected. </value>
    ReadOnly Property IsConnected() As Boolean

    ''' <summary>
    ''' Aborts execution, resets the device to known state and clears queues and register.
    ''' </summary>
    ''' <remarks> Typically implements SDC, RST, CLS and clearing error queue. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function ResetAndClear() As Boolean

    ''' <summary> Gets or sets the instrument resource address. </summary>
    ''' <value> The name of the resource. </value>
    Property ResourceName() As String

#End Region

#Region " CONFIGURE "

    ''' <summary> Configures the HIPOT Meter for making measurements. </summary>
    ''' <returns> True if success or False if failed. </returns>
    Function Configure() As Boolean

    ''' <summary> Gets or sets the dwell time in seconds. </summary>
    ''' <value> The dwell time. </value>
    Property DwellTime() As Single

    ''' <summary> Gets or sets the aperture in power line cycles. </summary>
    ''' <value> The aperture. </value>
    Property Aperture() As Single

    ''' <summary> Gets or sets the voltage level in volts. </summary>
    ''' <value> The voltage level. </value>
    Property VoltageLevel() As Single

    ''' <summary> Gets or sets the current limit in Amperes. </summary>
    ''' <value> The current limit. </value>
    Property CurrentLimit() As Single

    ''' <summary> Gets or sets the resistance limit in ohms. </summary>
    ''' <value> The resistance low limit. </value>
    Property ResistanceLowLimit() As Single

    ''' <summary> Gets or sets the resistance range in ohms. </summary>
    ''' <value> The resistance range. </value>
    Property ResistanceRange() As Single

    ''' <summary> Gets or sets the sentinel indicating that contact check is supported. </summary>
    ''' <value> The contact check supported. </value>
    ReadOnly Property ContactCheckSupported As Boolean

    ''' <summary> Gets or sets the contact check enabled. </summary>
    ''' <value> The contact check enabled. </value>
    Property ContactCheckEnabled As Boolean

    ''' <summary> Gets or sets the contact check bit pattern. </summary>
    ''' <value> The contact check bit pattern. </value>
    Property ContactCheckBitPattern As Integer

#End Region

#Region " BINNING "

    ''' <summary> Gets or sets the duration of the end-of=test strobe. </summary>
    ''' <value> The duration of the end-of-test strobe. </value>
    Property EotStrobeDuration() As Single

    ''' <summary> Gets or sets the pass bit pattern. </summary>
    ''' <value> The pass bit pattern. </value>
    Property PassBitPattern As Integer

    ''' <summary> Gets or sets the fail bit pattern. </summary>
    ''' <value> The fail bit pattern. </value>
    Property FailBitPattern As Integer

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Sends an Exception message to the client.
    ''' </summary>
    Event ExceptionAvailable As EventHandler(Of System.Threading.ThreadExceptionEventArgs)

    ''' <summary> Raises the exception available event. </summary>
    ''' <remarks>
    ''' This method raises the exception if the exception messaging is not handled.
    ''' </remarks>
    ''' <param name="ex"> Specifies the exception. </param>
    Sub OnExceptionAvailable(ByVal ex As Exception)

    ''' <summary> Raises the exception available event. </summary>
    ''' <remarks>
    ''' This method raises the exception if the exception messaging is not handled.
    ''' </remarks>
    ''' <param name="ex">     Specifies the exception. </param>
    ''' <param name="format"> Specifies the event message format. </param>
    ''' <param name="args">   Specifies the event message format arguments. </param>
    Sub OnExceptionAvailable(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)

    ''' <summary>Sends a message to the client.
    ''' </summary>
    Event MessageAvailable As EventHandler(Of MessageEventArgs)

    ''' <summary> Raises the Message Available event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> The message event arguments. </param>
    Sub OnMessageAvailable(ByVal e As MessageEventArgs)

    ''' <summary> Raises the Message Available event. </summary>
    ''' <param name="traceLevel"> Specifies the event arguments message
    '''                           <see cref="Diagnostics.TraceEventType">trace level</see>. </param>
    ''' <param name="synopsis">   Specifies the message Synopsis. </param>
    ''' <param name="format">     Specifies the message details. </param>
    ''' <param name="args">       Arguments to use in the format statement. </param>
    Sub OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)

#End Region

#Region " MEASURE "

    ''' <summary> Gets or sets the measured Current. </summary>
    ''' <value> The current. </value>
    ReadOnly Property Current() As Single

    ''' <summary> Gets or sets the measured Current reading. </summary>
    ''' <value> The current reading. </value>
    ReadOnly Property CurrentReading() As String

    ''' <summary> Makes the HIPOT measurements manually. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function Measure() As Boolean

    ''' <summary> Gets or sets the measurement outcome for the measurements. </summary>
    ''' <value> The outcome. </value>
    ReadOnly Property Outcome() As MeasurementOutcomes

    ''' <summary> Gets or sets the measurement Resistance. </summary>
    ''' <value> The resistance. </value>
    ReadOnly Property Resistance() As Single

    ''' <summary> Gets or sets the measurement Resistance reading. </summary>
    ''' <value> The resistance reading. </value>
    ReadOnly Property ResistanceReading() As String

    ''' <summary> Gets or sets the reading as fetched from the instrument. </summary>
    ''' <value> The reading. </value>
    ReadOnly Property Reading() As String

    ''' <summary> Gets or sets the parsed measurement status. </summary>
    ''' <value> The status. </value>
    ReadOnly Property Status() As Long

    ''' <summary> Gets or sets the measured voltage. </summary>
    ''' <value> The voltage. </value>
    ReadOnly Property Voltage() As Single

    ''' <summary> Gets or sets the measured voltage reading. </summary>
    ''' <value> The voltage reading. </value>
    ReadOnly Property VoltageReading() As String

#End Region

#Region " Settings "

    ''' <summary> Saves the settings. </summary>
    Sub SaveSettings()

    ''' <summary>
    ''' Reads the settings and set the device values -- this does not send the settings to the
    ''' instrument.
    ''' </summary>
    Sub ReadSettings()

    ''' <summary>
    ''' Updates the current settings from the device -- this does not read the settings from the
    ''' instrument.
    ''' </summary>
    Sub UpdateSettings()

#End Region

#Region " SYNCHRNIZER "

    ''' <summary>
    ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see>
    ''' to use for marshaling events.
    ''' </summary>
    ''' <value> The synchronizer. </value>
    Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke

    ''' <summary>
    ''' Sets the <see cref="System.ComponentModel.ISynchronizeInvoke">synchronization object</see>
    ''' to use for marshaling events.
    ''' </summary>
    ''' <param name="value"> The value. </param>
    Sub SynchronizerSetter(ByVal value As System.ComponentModel.ISynchronizeInvoke)
#End Region

#Region " TRIGGER "

    ''' <summary> Aborts waiting for a triggered measurement. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function AbortMeasurement() As Boolean

    ''' <summary> Asserts a trigger for making a triggered measurement. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function TriggerMeasurement() As Boolean

    ''' <summary> Returns true if measurement completed. </summary>
    ''' <returns> true if measurement completed, false if not. </returns>
    Function IsMeasurementCompleted() As Boolean

    ''' <summary>
    ''' Prime the instrument to wait for a trigger. This clears the digital outputs and loops until
    ''' trigger or external TRG command.
    ''' </summary>
    ''' <remarks>
    ''' Using manual trigger was required because the i2400 instrument fails to respond to the bus
    ''' trigger.
    ''' </remarks>
    ''' <param name="useManualTrigger"> True if using manual trigger. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function PrepareForTrigger(ByVal useManualTrigger As Boolean) As Boolean

    ''' <summary> Read the measurements from the instrument. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Function ReadMeasurements() As Boolean

#End Region

End Interface

''' <summary> Enumerates the measurement outcomes. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum StatusBit
    ''' <summary> An enum constant representing the overflow option. </summary>

    <ComponentModel.Description("Measurement was made while in over-range (OFLO)")>
    Overflow = 0
    ''' <summary> An enum constant representing the filter enabled option. </summary>

    <ComponentModel.Description("Measurement was made with the filter enabled (Filter)")>
    FilterEnabled = 1
    ''' <summary> An enum constant representing the hit compliance option. </summary>

    <ComponentModel.Description("Hit real compliance (Compliance)")>
    HitCompliance = 3
    ''' <summary> An enum constant representing the hit overvoltage protection option. </summary>

    <ComponentModel.Description("Hit over voltage protection limit (OVP)")>
    HitOvervoltageProtection = 4
    ''' <summary> An enum constant representing the hit range compliance option. </summary>

    <ComponentModel.Description("Hit range compliance (Range Compliance)")>
    HitRangeCompliance = 16
    ''' <summary> An enum constant representing the failed contact check option. </summary>

    <ComponentModel.Description("Failed contact check (CCH)")>
    FailedContactCheck = 18
End Enum

''' <summary> Enumerates the measurement outcomes. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()> Public Enum MeasurementOutcomes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined")>
    None
    ''' <summary> An enum constant representing the part passed option. </summary>

    <ComponentModel.Description("Measured value is in range")>
    PartPassed = 1

    ''' <summary>Out of range.</summary>
    <ComponentModel.Description("Measured values is out of range")>
    PartFailed = 2

    ''' <summary>Measurement Failed.</summary>
    <ComponentModel.Description("Measurement Failed")>
    MeasurementFailed = 4

    ''' <summary>Measurement Not made.</summary>
    <ComponentModel.Description("Measurement Not Made")>
    MeasurementNotMade = 8

    ''' <summary>Instrument failed to measure because it hit the voltage limit.</summary>
    <ComponentModel.Description("Compliance")>
    HitCompliance = 16

    ''' <summary>The message received from the instrument did not parse.</summary>
    <ComponentModel.Description("Unexpected Reading format")>
    UnexpectedReadingFormat = 32

    ''' <summary>The message received from the instrument did not parse.</summary>
    <ComponentModel.Description("Unexpected Outcome format")>
    UnexpectedOutcomeFormat = 64

    ''' <summary>One of a number of failure occurred at the device level.</summary>
    <ComponentModel.Description("Unspecified Status Exception")>
    UnspecifiedStatusException = 128

    ''' <summary>One of a number of failure occurred at the program level.</summary>
    <ComponentModel.Description("Unspecified Program Failure")>
    UnspecifiedProgramFailure = 256

    ''' <summary>Device Error.</summary>
    <ComponentModel.Description("Device Error")>
    DeviceError = 512
    ''' <summary> An enum constant representing the failed contact check option. </summary>

    <ComponentModel.Description("Contact Check")>
    FailedContactCheck = 1024
    ''' <summary> An enum constant representing the undefined 4 option. </summary>

    <ComponentModel.Description("Undefined4")>
    Undefined4 = 2048
    ''' <summary> An enum constant representing the undefined 5 option. </summary>

    <ComponentModel.Description("Undefined5")>
    Undefined5 = 4096
    ''' <summary> An enum constant representing the undefined 6 option. </summary>

    <ComponentModel.Description("Undefined6")>
    Undefined6 = 8192

    ''' <summary>Unknown outcome - assert.</summary>
    <ComponentModel.Description("Unknown outcome - assert")>
    UnknownOutcome = Undefined4 + Undefined5 + Undefined6
End Enum

