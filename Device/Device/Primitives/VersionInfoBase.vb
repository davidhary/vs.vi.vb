''' <summary> Parses and holds the instrument version information. </summary>
''' <remarks>
''' (c) 2008 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2008-01-15, 2.0.2936. Derived from previous Scpi Instrument implementation. </para>
''' </remarks>
Public MustInherit Class VersionInfoBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
        Me.ClearThis()
    End Sub

#End Region

#Region " INFORMATION PROPERTIES "

    ''' <summary>
    ''' Gets or sets the identity information that was used to parse the version information.
    ''' </summary>
    ''' <value> The identity. </value>
    Public Property Identity() As String

    ''' <summary> Gets or sets the extended version information. </summary>
    ''' <value> The firmware revision. </value>
    Public Property FirmwareRevision() As String

    ''' <summary> Gets or sets the instrument manufacturer name . </summary>
    ''' <value> The name of the manufacturer. </value>
    Public Property ManufacturerName() As String

    ''' <summary> Gets or sets the instrument model number. </summary>
    ''' <value>
    ''' A string property that may include additional precursors such as 'Model' to the relevant
    ''' information.
    ''' </value>
    Public Property Model() As String

    ''' <summary> Gets or sets the serial number. </summary>
    ''' <value> The serial number. </value>
    Public Property SerialNumber() As String

    ''' <summary> Gets or sets the list of revision elements. </summary>
    ''' <value> The firmware revision elements. </value>
    Public Property FirmwareRevisionElements() As System.Collections.Specialized.StringDictionary

#End Region

#Region " PARSE "

    ''' <summary> Clears this object to its blank/initial state. </summary>
    Private Sub ClearThis()
        Me._FirmwareRevisionElements = New System.Collections.Specialized.StringDictionary
        Me._Identity = String.Empty
        Me._FirmwareRevision = String.Empty
        Me._ManufacturerName = String.Empty
        Me._Model = String.Empty
        Me._SerialNumber = String.Empty
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    Public Overridable Sub Clear()
        Me.ClearThis()
    End Sub

    ''' <summary> Parses the instrument firmware revision. </summary>
    ''' <param name="revision"> Specifies the instrument firmware revision. </param>
    Protected Overridable Sub ParseFirmwareRevision(ByVal revision As String)
        Me._FirmwareRevisionElements = New System.Collections.Specialized.StringDictionary
    End Sub

    ''' <summary> Builds the identity. </summary>
    ''' <returns> A String. </returns>
    Public Function BuildIdentity() As String
        Dim builder As New System.Text.StringBuilder
        If String.IsNullOrWhiteSpace(Me.ManufacturerName) Then
            builder.Append("Manufacturer,")
        Else
            builder.Append($"{Me.ManufacturerName},")
        End If
        If String.IsNullOrWhiteSpace(Me.Model) Then
            builder.Append("Model,")
        Else
            builder.Append($"{Me.Model},")
        End If
        If String.IsNullOrWhiteSpace(Me.SerialNumber) Then
            builder.Append("1,")
        Else
            builder.Append($"{Me.SerialNumber},")
        End If
        If String.IsNullOrWhiteSpace(Me.FirmwareRevision) Then
            builder.Append("Oct 10 1997")
        Else
            builder.Append($"{Me.FirmwareRevision}")
        End If
        Return builder.ToString
    End Function

    ''' <summary> Parses the instrument identity string. </summary>
    ''' <remarks> The firmware revision can be further interpreted by the child instruments. </remarks>
    ''' <param name="value"> Specifies the instrument identity string, which includes at a minimum the
    '''                      following information:
    '''                      <see cref="ManufacturerName">manufacturer</see>,
    '''                      <see cref="Model">model</see>,
    '''                      <see cref="SerialNumber">serial number</see>, e.g., <para>
    '''                      <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997
    '''                      09:51:36/A02
    '''                      /D/B/E.</c>.</para> </param>
    Public Overridable Sub Parse(ByVal value As String)

        If String.IsNullOrEmpty(value) Then
            Me.Clear()
        Else

            ' save the identity.
            Me._Identity = value

            ' Parse the id to get the revision number
            Dim idItems As Queue(Of String) = New Queue(Of String)(value.Split(","c))

            ' company, e.g., KEITHLEY INSTRUMENTS,
            Me._ManufacturerName = idItems.Dequeue

            ' 24xx: MODEL 2420
            ' 7510: MODEL DMM7510
            Me._Model = idItems.Dequeue
            Dim stripCandidate As String = "MODEL "
            If Me._Model.StartsWith(stripCandidate, StringComparison.OrdinalIgnoreCase) Then
                Me._Model = Me.Model.Substring(stripCandidate.Length)
            End If

            ' Serial Number: 0669977
            Me._SerialNumber = idItems.Dequeue

            ' 2002: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            ' 7510: 1.0.0i
            Me._FirmwareRevision = idItems.Dequeue

            ' parse thee firmware revision
            Me.ParseFirmwareRevision(Me._FirmwareRevision)
        End If

    End Sub

#End Region

End Class

''' <summary>
''' Enumerates the instrument board types as defined by the instrument identity.
''' </summary>
Public Enum FirmwareRevisionElement

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None

    ''' <summary> An enum constant representing the analog option. </summary>
    <ComponentModel.Description("Analog")>
    Analog

    ''' <summary> An enum constant representing the digital option. </summary>
    <ComponentModel.Description("Digital")>
    Digital

    ''' <summary> An enum constant representing the display option. </summary>
    <ComponentModel.Description("Display")>
    Display

    ''' <summary> An enum constant representing the contact check option. </summary>
    <ComponentModel.Description("Contact Check")>
    ContactCheck

    ''' <summary> An enum constant representing the LED display option. </summary>
    <ComponentModel.Description("LED Display")>
    LedDisplay

    ''' <summary> An enum constant representing the primary option. </summary>
    <ComponentModel.Description("Primary")>
    Primary

    ''' <summary> An enum constant representing the secondary option. </summary>
    <ComponentModel.Description("Secondary")>
    Secondary

    ''' <summary> An enum constant representing the ternary option. </summary>
    <ComponentModel.Description("Ternary")>
    Ternary

    ''' <summary> An enum constant representing the quaternary option. </summary>
    <ComponentModel.Description("Quaternary")>
    Quaternary

    ''' <summary> An enum constant representing the mainframe option. </summary>
    <ComponentModel.Description("Mainframe")>
    Mainframe

    ''' <summary> An enum constant representing the boot code option. </summary>
    <ComponentModel.Description("Boot code")>
    BootCode

    ''' <summary> An enum constant representing the front panel option. </summary>
    <ComponentModel.Description("Front panel")>
    FrontPanel

    ''' <summary> An enum constant representing the internal meter option. </summary>
    <ComponentModel.Description("Internal Meter")>
    InternalMeter
End Enum
