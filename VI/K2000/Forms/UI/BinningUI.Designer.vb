<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BinningUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._BinningView = New isr.VI.K2002.Forms.BinningView()
        Me.SuspendLayout()
        '
        '_BinningView
        '
        Me._BinningView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._BinningView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._BinningView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BinningView.Location = New System.Drawing.Point(1, 1)
        Me._BinningView.Name = "_BinningView"
        Me._BinningView.Padding = New System.Windows.Forms.Padding(1)
        Me._BinningView.Size = New System.Drawing.Size(407, 272)
        Me._BinningView.TabIndex = 0
        '
        'BinningUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._BinningView)
        Me.Name = "BinningUI"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(409, 274)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _BinningView As BinningView
End Class
