'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\CompensateChannelSubsystemBase_scpi.vb
'
' summary:	Compensate channel subsystem base scpi class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.EnumExtensions

Partial Public Class CompensateChannelSubsystemBase

#Region " COMPENSATION TYPE "

    ''' <summary> Define compensation type read writes. </summary>
    Private Sub DefineCompensationTypeReadWrites()
        Me._CompensationTypeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As CompensationTypes In [Enum].GetValues(GetType(CompensationTypes))
            Me._CompensationTypeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets the compensation type read writes. </summary>
    ''' <value> The compensation type read writes. </value>
    Public ReadOnly Property CompensationTypeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> List of types of the supported compensations. </summary>
    Private _SupportedCompensationTypes As CompensationTypes

    ''' <summary>
    ''' Gets or sets the supported Compensation Type. This is a subset of the functions supported by
    ''' the instrument.
    ''' </summary>
    ''' <value> A list of types of the supported compensations. </value>
    Public Property SupportedCompensationTypes() As CompensationTypes
        Get
            Return Me._SupportedCompensationTypes
        End Get
        Set(ByVal value As CompensationTypes)
            If Not Me.SupportedCompensationTypes.Equals(value) Then
                Me._SupportedCompensationTypes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the compensation type code. </summary>
    ''' <value> The compensation type code. </value>
    Protected ReadOnly Property CompensationTypeCode As String

    ''' <summary> The Compensation Type. </summary>
    Private _CompensationType As CompensationTypes?

    ''' <summary> Applies the compensation type described by value. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub ApplyCompensationType(ByVal value As CompensationTypes)
        Me._CompensationType = value
        Me._CompensationTypeCode = value.ExtractBetween
    End Sub

    ''' <summary> Gets or sets the cached source CompensationType. </summary>
    ''' <value>
    ''' The <see cref="CompensationTypes">source Compensation Type</see> or none if not set or
    ''' unknown.
    ''' </value>
    Public Overridable Property CompensationType As CompensationTypes?
        Get
            Return Me._CompensationType
        End Get
        Protected Set(ByVal value As CompensationTypes?)
            If Not Nullable.Equals(Me.CompensationType, value) Then
                Me._CompensationType = value
                If value.HasValue Then
                    Me.ApplyCompensationType(value.Value)
                Else
                    Me._CompensationTypeCode = String.Empty
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class

''' <summary> A bit-field of flags for specifying compensation types. </summary>
<Flags>
Public Enum CompensationTypes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the open circuit option. </summary>
    <ComponentModel.Description("Open (OPEN)")>
    OpenCircuit = 1

    ''' <summary> An enum constant representing the short circuit option. </summary>
    <ComponentModel.Description("Short (SHOR)")>
    ShortCircuit = 2

    ''' <summary> An enum constant representing the load option. </summary>
    <ComponentModel.Description("Load (LOAD)")>
    Load = 4
End Enum

