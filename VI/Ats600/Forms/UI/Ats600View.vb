'---------------------------------------------------------------------------------------------------
' file:		Forms\UI\Ats600View.vb
'
' summary:	Ats 600 view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> Temptonic Ats600 Device User Interface. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-30 </para></remarks>
<System.ComponentModel.DisplayName("Ats600 User Interface"),
      System.ComponentModel.Description("Temptonic Ats600 Device User Interface"),
      System.Drawing.ToolboxBitmap(GetType(Ats600View))>
Public Class Ats600View
    Inherits isr.VI.Facade.VisaView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()

        Me.InitializingComponents = True
        Dim index As Integer = 0
        index += 1
        Me._ReadingView = ReadingView.Create
        MyBase.AddView(New VI.Facade.VisaViewControl(Me._ReadingView, index, "_ReadingTabPage", "Read"))
        index += 1
        Me._FunctionView = FunctionView.Create
        MyBase.AddView(New VI.Facade.VisaViewControl(Me._FunctionView, index, "_FunctionTabPage", "Function"))
        index += 1
        Me._SensorView = SensorView.Create
        MyBase.AddView(New VI.Facade.VisaViewControl(Me._SensorView, index, "_SensorTabPage", "Sensor"))
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As Ats600Device)
        Me.New()
        Me.AssignDeviceThis(device)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the Ats600 View and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                If Me.FunctionView IsNot Nothing Then Me.FunctionView.Dispose() : Me._FunctionView = Nothing
                If Me.SensorView IsNot Nothing Then Me.SensorView.Dispose() : Me._SensorView = Nothing
                If Me.ReadingView IsNot Nothing Then Me.ReadingView.Dispose() : Me._ReadingView = Nothing
                Me.AssignDeviceThis(Nothing)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As Ats600Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As Ats600Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Private Sub AssignDeviceThis(ByVal value As Ats600Device)
        If Me._Device IsNot Nothing OrElse MyBase.VisaSessionBase IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = Nothing
            MyBase.StatusView.UserInterfaceSettings = Nothing
            Me._Device = Nothing
        End If
        Me._Device = value
        MyBase.BindVisaSessionBase(value)
        If value IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = isr.VI.Ats600.My.Settings
            MyBase.StatusView.UserInterfaceSettings = Nothing
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Public Overloads Sub AssignDevice(ByVal value As Ats600Device)
        Me.AssignDeviceThis(value)
    End Sub

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Executes the device closing action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.OnDeviceClosing(e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            ' release the device before subsystems are disposed
            '!@# Me.SourceView.AssignDevice(Nothing)
        End If
    End Sub

    ''' <summary> Executes the device closed action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Protected Overrides Sub OnDeviceClosed()
        MyBase.OnDeviceClosed()
        ' remove binding after subsystems are disposed
        ' because the device closed the subsystems are null and binding will be removed.
        ' TO_DO: Use a Dual Display interface to bind the display on the display View.
        ' MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Protected Overrides Sub OnDeviceOpened()
        MyBase.OnDeviceOpened()
        ' assigning device and subsystems after the subsystems are created
        'TO_DO: Me.SourceView.AssignDevice(Me.Device)
        ' MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
    End Sub

#End Region

#End Region

#Region " VIEWS  "

    ''' <summary> Gets or sets the function view. </summary>
    ''' <value> The function view. </value>
    Private ReadOnly Property FunctionView As FunctionView

    ''' <summary> Gets or sets the reading view. </summary>
    ''' <value> The reading view. </value>
    Private ReadOnly Property ReadingView As ReadingView

    ''' <summary> Gets or sets the sensor view. </summary>
    ''' <value> The sensor view. </value>
    Private ReadOnly Property SensorView As SensorView

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
