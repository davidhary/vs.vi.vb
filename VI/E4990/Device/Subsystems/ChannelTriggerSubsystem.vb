''' <summary> Defines a SCPI Channel Trigger Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class ChannelTriggerSubsystem
    Inherits VI.ChannelTriggerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(channelNumber, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ContinuousEnabled = False
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " INITIATE "

    ''' <summary> Gets the initiate command. </summary>
    ''' <remarks> SCPI: ":INIT&lt;c#&gt;:IMM". </remarks>
    ''' <value> The initiate command. </value>
    Protected Overrides Property InitiateCommand As String
        Get
            Return $":INIT{Me.ChannelNumber}:IMM"
        End Get
    End Property

    ''' <summary> Gets the continuous initiation enabled query command. </summary>
    ''' <value> The continuous initiation enabled query command. </value>
    Protected Overrides Property ContinuousEnabledQueryCommand As String
        Get
            Return $":CALC{Me.ChannelNumber}:AVER?"
        End Get
    End Property

    ''' <summary> Gets the continuous initiation enabled command Format. </summary>
    ''' <value> The continuous initiation enabled query command. </value>
    Protected Overrides Property ContinuousEnabledCommandFormat As String
        Get
            Return $":CALC{Me.ChannelNumber}:AVER {{0:1;1;0}}"
        End Get
    End Property

#End Region

#End Region

End Class
