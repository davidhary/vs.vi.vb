''' <summary> E4990 Device resource only unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("e4990")>
Public Class ResourceTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings not found")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(E4990ResourceInfo.Exists, $"{GetType(E4990Tests.ResourceSettings)} settings not found")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " VISA RESOURCE TESTS "

    ''' <summary> (Unit Test Method) tests visa resource. </summary>
    ''' <remarks> Finds the resource using the session factory resources manager. </remarks>
    <TestMethod()>
    Public Sub VisaResourceTest()
        isr.VI.DeviceTests.DeviceManager.AssertVisaResourceManagerIncludesResource(E4990ResourceInfo)
    End Sub

    ''' <summary> (Unit Test Method) tests device resource. </summary>
    ''' <remarks> Finds the resource using the device class. </remarks>
    <TestMethod()>
    Public Sub DeviceResourceTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As isr.VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            isr.VI.DeviceTests.DeviceManager.AssertResourceFound(device, E4990ResourceInfo)
        End Using
    End Sub

#End Region

#Region " DEVICE TALKER TESTS "

    ''' <summary> (Unit Test Method) tests device talker. </summary>
    ''' <remarks> Checks if the device adds a trace message to a listener. </remarks>
    <TestMethod()>
    Public Sub DeviceTalkerTest()
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            isr.VI.DeviceTests.DeviceManager.AssertDeviceTalks(TestInfo, device)
        End Using
    End Sub

#End Region

#Region " DEVICE TESTS: OPEN, CLOSE, CHECK SUSBSYSTEMS "

    ''' <summary> (Unit Test Method) tests open session. </summary>
    ''' <remarks> Tests opening and closing a VISA session. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " DEVICE EVENT TESTS "

    ''' <summary> (Unit Test Method) tests open status session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenStatusSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests wait for service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub WaitForMessageAvailableTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertWaitForMessageAvailable(TestInfo, device.Session)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests toggling service request handling. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ToggleServiceRequestHandlingTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertToggleServiceRequestHandling(TestInfo, device.Session)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) handles the service request test. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub HandleServiceRequestTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertHandlingServiceRequest(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests device handle service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DeviceHandleServiceRequestTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceHandlingServiceRequest(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests device polling. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DevicePollingTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly
                isr.VI.DeviceTests.DeviceManager.AssertOpenSession(TestInfo, device, E4990ResourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDevicePolling(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class
