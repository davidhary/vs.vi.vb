﻿'---------------------------------------------------------------------------------------------------
' file:		National.Visa\My\MyLibrary_Info.vb
'
' summary:	My library information class
'---------------------------------------------------------------------------------------------------
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = VI.Pith.My.ProjectTraceEventId.NationalVisa

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "VI National Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "National Virtual Instrument Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "VI.National.Visa"

    End Class

End Namespace

