<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BufferStreamView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BufferStreamView))
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DisplayBufferMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ClearBufferDisplayMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._BufferStreamingMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ExplainBufferStreamingMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._UsingScanCardMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConfigureStreamingMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._StartBufferStreamMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortButton = New System.Windows.Forms.ToolStripButton()
        Me._BufferCountLabel = New isr.Core.Controls.ToolStripLabel()
        Me._LastPointNumberLabel = New isr.Core.Controls.ToolStripLabel()
        Me._FirstPointNumberLabel = New isr.Core.Controls.ToolStripLabel()
        Me._BufferSizeNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BufferSizeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._TriggerStateLabel = New isr.Core.Controls.ToolStripLabel()
        Me._AssertBusTriggerButton = New System.Windows.Forms.ToolStripButton()
        Me.UseTriggerPlanMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._AbortButton, Me._BufferCountLabel, Me._LastPointNumberLabel, Me._FirstPointNumberLabel, Me._BufferSizeNumericLabel, Me._BufferSizeNumeric, Me._TriggerStateLabel, Me._AssertBusTriggerButton})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Padding = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(349, 27)
        Me._SubsystemToolStrip.TabIndex = 11
        Me._SubsystemToolStrip.Text = "Buffer"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DisplayBufferMenuItem, Me._ClearBufferDisplayMenuItem, Me._BufferStreamingMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(55, 24)
        Me._SubsystemSplitButton.Text = "Buffer"
        '
        '_DisplayBufferMenuItem
        '
        Me._DisplayBufferMenuItem.Name = "_DisplayBufferMenuItem"
        Me._DisplayBufferMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._DisplayBufferMenuItem.Text = "Display Buffer"
        Me._DisplayBufferMenuItem.ToolTipText = "Displays the buffer"
        '
        '_ClearBufferDisplayMenuItem
        '
        Me._ClearBufferDisplayMenuItem.Name = "_ClearBufferDisplayMenuItem"
        Me._ClearBufferDisplayMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ClearBufferDisplayMenuItem.Text = "Clear Buffer Display"
        Me._ClearBufferDisplayMenuItem.ToolTipText = "Clears the display"
        '
        '_BufferStreamingMenuItem
        '
        Me._BufferStreamingMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ExplainBufferStreamingMenuItem, Me._UsingScanCardMenuItem, Me.UseTriggerPlanMenuItem, Me._ConfigureStreamingMenuItem, Me._StartBufferStreamMenuItem})
        Me._BufferStreamingMenuItem.Name = "_BufferStreamingMenuItem"
        Me._BufferStreamingMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._BufferStreamingMenuItem.Text = "Buffer Streaming"
        Me._BufferStreamingMenuItem.ToolTipText = "Select action"
        '
        '_ExplainBufferStreamingMenuItem
        '
        Me._ExplainBufferStreamingMenuItem.Name = "_ExplainBufferStreamingMenuItem"
        Me._ExplainBufferStreamingMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ExplainBufferStreamingMenuItem.Text = "Info"
        Me._ExplainBufferStreamingMenuItem.ToolTipText = "Display buffer streaming settings"
        '
        '_UsingScanCardMenuItem
        '
        Me._UsingScanCardMenuItem.CheckOnClick = True
        Me._UsingScanCardMenuItem.Name = "_UsingScanCardMenuItem"
        Me._UsingScanCardMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._UsingScanCardMenuItem.Text = "Use Scan Card"
        Me._UsingScanCardMenuItem.ToolTipText = "Use scan card"
        '
        '_ConfigureStreamingMenuItem
        '
        Me._ConfigureStreamingMenuItem.CheckOnClick = True
        Me._ConfigureStreamingMenuItem.Name = "_ConfigureStreamingMenuItem"
        Me._ConfigureStreamingMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ConfigureStreamingMenuItem.Text = "Configure"
        Me._ConfigureStreamingMenuItem.ToolTipText = "Configure buffer streaming"
        '
        '_StartBufferStreamMenuItem
        '
        Me._StartBufferStreamMenuItem.CheckOnClick = True
        Me._StartBufferStreamMenuItem.Name = "_StartBufferStreamMenuItem"
        Me._StartBufferStreamMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._StartBufferStreamMenuItem.Text = "Start"
        Me._StartBufferStreamMenuItem.ToolTipText = "Continuously reads new values while a trigger plan is active"
        '
        '_AbortButton
        '
        Me._AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AbortButton.Image = CType(resources.GetObject("_AbortButton.Image"), System.Drawing.Image)
        Me._AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(45, 24)
        Me._AbortButton.Text = "Abort"
        Me._AbortButton.ToolTipText = "Aborts triggering"
        '
        '_BufferCountLabel
        '
        Me._BufferCountLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._BufferCountLabel.Name = "_BufferCountLabel"
        Me._BufferCountLabel.Size = New System.Drawing.Size(15, 24)
        Me._BufferCountLabel.Text = "0"
        Me._BufferCountLabel.ToolTipText = "Buffer count"
        '
        '_LastPointNumberLabel
        '
        Me._LastPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._LastPointNumberLabel.Name = "_LastPointNumberLabel"
        Me._LastPointNumberLabel.Size = New System.Drawing.Size(15, 24)
        Me._LastPointNumberLabel.Text = "2"
        Me._LastPointNumberLabel.ToolTipText = "Number of last buffer reading"
        '
        '_FirstPointNumberLabel
        '
        Me._FirstPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._FirstPointNumberLabel.Name = "_FirstPointNumberLabel"
        Me._FirstPointNumberLabel.Size = New System.Drawing.Size(15, 24)
        Me._FirstPointNumberLabel.Text = "1"
        Me._FirstPointNumberLabel.ToolTipText = "Number of the first buffer reading"
        '
        '_BufferSizeNumericLabel
        '
        Me._BufferSizeNumericLabel.Name = "_BufferSizeNumericLabel"
        Me._BufferSizeNumericLabel.Size = New System.Drawing.Size(34, 24)
        Me._BufferSizeNumericLabel.Text = "Size:"
        '
        '_BufferSizeNumeric
        '
        Me._BufferSizeNumeric.AutoSize = False
        Me._BufferSizeNumeric.Name = "_BufferSizeNumeric"
        Me._BufferSizeNumeric.Size = New System.Drawing.Size(60, 24)
        Me._BufferSizeNumeric.Text = "0"
        Me._BufferSizeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_TriggerStateLabel
        '
        Me._TriggerStateLabel.Name = "_TriggerStateLabel"
        Me._TriggerStateLabel.Size = New System.Drawing.Size(29, 24)
        Me._TriggerStateLabel.Text = "idle"
        Me._TriggerStateLabel.ToolTipText = "Trigger state"
        '
        '_AssertBusTriggerButton
        '
        Me._AssertBusTriggerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AssertBusTriggerButton.Image = CType(resources.GetObject("_AssertBusTriggerButton.Image"), System.Drawing.Image)
        Me._AssertBusTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AssertBusTriggerButton.Name = "_AssertBusTriggerButton"
        Me._AssertBusTriggerButton.Size = New System.Drawing.Size(48, 24)
        Me._AssertBusTriggerButton.Text = "Assert"
        Me._AssertBusTriggerButton.ToolTipText = "Assert BUS trigger"
        '
        'UseTriggerPlanMenuItem
        '
        Me.UseTriggerPlanMenuItem.CheckOnClick = True
        Me.UseTriggerPlanMenuItem.Name = "UseTriggerPlanMenuItem"
        Me.UseTriggerPlanMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.UseTriggerPlanMenuItem.Text = "Use Trigger Plan"
        Me.UseTriggerPlanMenuItem.ToolTipText = "Select trigger plan or application settings for trigger source and trigger count." &
    ""
        '
        'BufferStreamView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._SubsystemToolStrip)
        Me.Name = "BufferStreamView"
        Me.Size = New System.Drawing.Size(349, 26)
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _BufferCountLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _LastPointNumberLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _FirstPointNumberLabel As isr.Core.Controls.ToolStripLabel
    Private WithEvents _BufferSizeNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _BufferSizeNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _AbortButton As Windows.Forms.ToolStripButton
    Private WithEvents _TriggerStateLabel As Core.Controls.ToolStripLabel
    Private WithEvents _DisplayBufferMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ClearBufferDisplayMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _UsingScanCardMenuItem As ToolStripMenuItem
    Private WithEvents _ConfigureStreamingMenuItem As ToolStripMenuItem
    Private WithEvents _StartBufferStreamMenuItem As ToolStripMenuItem
    Private WithEvents _BufferStreamingMenuItem As ToolStripMenuItem
    Private WithEvents _ExplainBufferStreamingMenuItem As ToolStripMenuItem
    Private WithEvents _AssertBusTriggerButton As ToolStripButton
    Private WithEvents UseTriggerPlanMenuItem As ToolStripMenuItem
End Class
