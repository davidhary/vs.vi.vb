
Partial Public Class SenseFunctionSubsystemBase

#Region " AVERAGE COUNT "

    ''' <summary> The average count range. </summary>
    Private _AverageCountRange As Core.Constructs.RangeI

    ''' <summary> The Average Count range in seconds. </summary>
    ''' <value> The average count range. </value>
    Public Property AverageCountRange As Core.Constructs.RangeI
        Get
            Return Me._AverageCountRange
        End Get
        Set(value As Core.Constructs.RangeI)
            If Me.AverageCountRange <> value Then
                Me._AverageCountRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The average count. </summary>
    Private _AverageCount As Integer?

    ''' <summary>
    ''' Gets or sets the cached average count. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property AverageCount As Integer?
        Get
            Return Me._AverageCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.AverageCount, value) Then
                Me._AverageCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the average count. </summary>
    ''' <param name="value"> The average count. </param>
    ''' <returns> The average count. </returns>
    Public Function ApplyAverageCount(ByVal value As Integer) As Integer?
        Me.WriteAverageCount(value)
        Return Me.QueryAverageCount
    End Function

    ''' <summary> Gets or sets The average count query command. </summary>
    ''' <value> The average count query command. </value>
    Protected Overridable Property AverageCountQueryCommand As String

    ''' <summary> Queries The average count. </summary>
    ''' <returns> The average count or none if unknown. </returns>
    Public Function QueryAverageCount() As Integer?
        Me.AverageCount = Me.Query(Me.AverageCount, Me.AverageCountQueryCommand)
        Return Me.AverageCount
    End Function

    ''' <summary> Gets or sets The average count command format. </summary>
    ''' <value> The average count command format. </value>
    Protected Overridable Property AverageCountCommandFormat As String

    ''' <summary> Writes The average count without reading back the value from the device. </summary>
    ''' <remarks> This command sets The average count. </remarks>
    ''' <param name="value"> The average count. </param>
    ''' <returns> The average count. </returns>
    Public Function WriteAverageCount(ByVal value As Integer) As Integer?
        Me.AverageCount = Me.Write(value, Me.AverageCountCommandFormat)
        Return Me.AverageCount
    End Function

#End Region

#Region " AVERAGE ENABLED "

    ''' <summary> Average enabled. </summary>
    Private _AverageEnabled As Boolean?

    ''' <summary> Gets or sets the cached Average Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Average Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AverageEnabled As Boolean?
        Get
            Return Me._AverageEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AverageEnabled, value) Then
                Me._AverageEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Average Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAverageEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAverageEnabled(value)
        Return Me.QueryAverageEnabled()
    End Function

    ''' <summary> Gets or sets the Average enabled query command. </summary>
    ''' <remarks> SCPI: "CURR:AVER:STAT?". </remarks>
    ''' <value> The Average enabled query command. </value>
    Protected Overridable Property AverageEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Average Enabled sentinel. Also sets the
    ''' <see cref="AverageEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAverageEnabled() As Boolean?
        Me.AverageEnabled = Me.Query(Me.AverageEnabled, Me.AverageEnabledQueryCommand)
        Return Me.AverageEnabled
    End Function

    ''' <summary> Gets or sets the Average enabled command Format. </summary>
    ''' <remarks> SCPI: "CURR:AVER:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Average enabled query command. </value>
    Protected Overridable Property AverageEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Average Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAverageEnabled(ByVal value As Boolean) As Boolean?
        Me.AverageEnabled = Me.Write(value, Me.AverageEnabledCommandFormat)
        Return Me.AverageEnabled
    End Function

#End Region

#Region " AVERAGE FILTER TYPE "

    ''' <summary> Define average filter types read writes. </summary>
    Private Sub DefineAverageFilterTypesReadWrites()
        Me._AverageFilterTypeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As AverageFilterTypes In [Enum].GetValues(GetType(AverageFilterTypes))
            Me._AverageFilterTypeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of Average Filter Type parses. </summary>
    ''' <value> A Dictionary of Average Filter Type parses. </value>
    Public ReadOnly Property AverageFilterTypeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> List of types of the supported average filters. </summary>
    Private _SupportedAverageFilterTypes As AverageFilterTypes

    ''' <summary> Gets or sets the supported Average Filter Types. </summary>
    ''' <value> The supported Average Filter Types. </value>
    Public Property SupportedAverageFilterTypes() As AverageFilterTypes
        Get
            Return Me._SupportedAverageFilterTypes
        End Get
        Set(ByVal value As AverageFilterTypes)
            If Not Me.SupportedAverageFilterTypes.Equals(value) Then
                Me._SupportedAverageFilterTypes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Type of the average filter. </summary>
    Private _AverageFilterType As AverageFilterTypes?

    ''' <summary> Gets or sets the cached source AverageFilterType. </summary>
    ''' <value>
    ''' The <see cref="AverageFilterType">source Average Filter Type</see> or none if not set or
    ''' unknown.
    ''' </value>
    Public Overloads Property AverageFilterType As AverageFilterTypes?
        Get
            Return Me._AverageFilterType
        End Get
        Protected Set(ByVal value As AverageFilterTypes?)
            If Not Me.AverageFilterType.Equals(value) Then
                Me._AverageFilterType = value
                Me.NotifyPropertyChanged()
                Me.MovingAverageFilterEnabled = Nullable.Equals(value, AverageFilterTypes.Moving)
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Average Filter Type. </summary>
    ''' <param name="value"> The  Source Average Filter Type. </param>
    ''' <returns>
    ''' The <see cref="AverageFilterType">source Average Filter Type</see> or none if unknown.
    ''' </returns>
    Public Function ApplyAverageFilterType(ByVal value As AverageFilterTypes) As AverageFilterTypes?
        Me.WriteAverageFilterType(value)
        Return Me.QueryAverageFilterType()
    End Function

    ''' <summary> Gets or sets the Average Filter Type query command. </summary>
    ''' <remarks> SCPI: SENS:CURR:DC:AVER:TCON?". </remarks>
    ''' <value> The Average Filter Type query command. </value>
    Protected Overridable Property AverageFilterTypeQueryCommand As String

    ''' <summary> Queries the Average Filter Type. </summary>
    ''' <returns>
    ''' The <see cref="AverageFilterType">Average Filter Type</see> or none if unknown.
    ''' </returns>
    Public Function QueryAverageFilterType() As AverageFilterTypes?
        Me.AverageFilterType = Me.Query(Of AverageFilterTypes)(Me.AverageFilterTypeQueryCommand,
                                                               Me.AverageFilterType.GetValueOrDefault(VI.AverageFilterTypes.None),
                                                               Me.AverageFilterTypeReadWrites)
        Return Me.AverageFilterType
    End Function

    ''' <summary> Gets or sets the Average Filter Type command format. </summary>
    ''' <remarks> SCPI: SENS:CURR:DC:AVER:TCON {0}. </remarks>
    ''' <value> The write Average Filter Type command format. </value>
    Protected Overridable Property AverageFilterTypeCommandFormat As String

    ''' <summary>
    ''' Writes the Average Filter Type without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The Average Filter Type. </param>
    ''' <returns>
    ''' The <see cref="AverageFilterType">Average Filter Type</see> or none if unknown.
    ''' </returns>
    Public Function WriteAverageFilterType(ByVal value As AverageFilterTypes) As AverageFilterTypes?
        Me.AverageFilterType = Me.Write(Of AverageFilterTypes)(Me.AverageFilterTypeCommandFormat, value, Me.AverageFilterTypeReadWrites)
        Return Me.AverageFilterType
    End Function

#End Region

#Region " AVERAGE MOVING FILTER ENABLED "

    ''' <summary> Moving Average Filter enabled. </summary>
    Private _MovingAverageFilterEnabled As Boolean?

    ''' <summary> Gets or sets the cached Moving Average Filter Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Moving Average Filter Enabled is not known; <c>True</c> if output is on;
    ''' otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property MovingAverageFilterEnabled As Boolean?
        Get
            Return Me._MovingAverageFilterEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.MovingAverageFilterEnabled, value) Then
                Me._MovingAverageFilterEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Moving Average Filter Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyMovingAverageFilterEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteMovingAverageFilterEnabled(value)
        Return Me.QueryMovingAverageFilterEnabled()
    End Function

    ''' <summary>
    ''' Queries the Moving Average Filter Enabled sentinel. Also sets the
    ''' <see cref="MovingAverageFilterEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryMovingAverageFilterEnabled() As Boolean?
        Me.MovingAverageFilterEnabled = Nullable.Equals(AverageFilterTypes.Moving, Me.QueryAverageFilterType())
        Return Me.MovingAverageFilterEnabled
    End Function

    ''' <summary>
    ''' Writes the Moving Average Filter Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteMovingAverageFilterEnabled(ByVal value As Boolean) As Boolean?
        Me.MovingAverageFilterEnabled = Nullable.Equals(AverageFilterTypes.Moving, Me.WriteAverageFilterType(If(value, AverageFilterTypes.Moving, AverageFilterTypes.Repeat)))
        Return Me.MovingAverageFilterEnabled
    End Function

#End Region

#Region " AVERAGE PERCENT WINDOW "

    ''' <summary> The average percent window range. </summary>
    Private _AveragePercentWindowRange As Core.Constructs.RangeR

    ''' <summary> The Average Percent Window range. </summary>
    ''' <value> The average percent window range. </value>
    Public Property AveragePercentWindowRange As Core.Constructs.RangeR
        Get
            Return Me._AveragePercentWindowRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.AveragePercentWindowRange <> value Then
                Me._AveragePercentWindowRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Average Percent Window. </summary>
    Private _AveragePercentWindow As Double?

    ''' <summary>
    ''' Gets or sets the cached Average Percent Window. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property AveragePercentWindow As Double?
        Get
            Return Me._AveragePercentWindow
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.AveragePercentWindow, value) Then
                Me._AveragePercentWindow = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Average Percent Window. </summary>
    ''' <param name="value"> The Average Percent Window. </param>
    ''' <returns> The Average Percent Window. </returns>
    Public Function ApplyAveragePercentWindow(ByVal value As Double) As Double?
        Me.WriteAveragePercentWindow(value)
        Return Me.QueryAveragePercentWindow
    End Function

    ''' <summary> Gets or sets The Average Percent Window query command. </summary>
    ''' <value> The Average Percent Window query command. </value>
    Protected Overridable Property AveragePercentWindowQueryCommand As String

    ''' <summary> Queries The Average Percent Window. </summary>
    ''' <returns> The Average Percent Window or none if unknown. </returns>
    Public Function QueryAveragePercentWindow() As Double?
        Me.AveragePercentWindow = Me.Query(Me.AveragePercentWindow, Me.AveragePercentWindowQueryCommand)
        Return Me.AveragePercentWindow
    End Function

    ''' <summary> Gets or sets The Average Percent Window command format. </summary>
    ''' <value> The Average Percent Window command format. </value>
    Protected Overridable Property AveragePercentWindowCommandFormat As String

    ''' <summary>
    ''' Writes The Average Percent Window without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Average Percent Window. </remarks>
    ''' <param name="value"> The Average Percent Window. </param>
    ''' <returns> The Average Percent Window. </returns>
    Public Function WriteAveragePercentWindow(ByVal value As Double) As Double?
        Me.AveragePercentWindow = Me.Write(value, Me.AveragePercentWindowCommandFormat)
        Return Me.AveragePercentWindow
    End Function

#End Region

End Class

''' <summary> Values that represent average filter types. </summary>
<Flags>
Public Enum AverageFilterTypes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not Defined ()")>
    None = 0

    ''' <summary> An enum constant representing the repeat option. </summary>
    <ComponentModel.Description("Repeat (REP)")>
    Repeat = 1

    ''' <summary> An enum constant representing the moving option. </summary>
    <ComponentModel.Description("Moving (MOV)")>
    Moving = 2
End Enum

