''' <summary> Defines a SCPI Binning Subsystem (CALC2 or CALC3). </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class BinningSubsystem
    Inherits VI.BinningSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="BinningSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.BinningStrobeDuration = TimeSpan.FromMilliseconds(100)
        Me.BinningStrobeEnabled = False
        Me.PassSource = 0
        Me.Limit1AutoClear = True
        Me.Limit1Enabled = False
        Me.Limit1LowerLevel = -1
        Me.Limit1LowerSource = 0
        Me.Limit1UpperLevel = 1
        Me.Limit1UpperSource = 0
        Me.Limit2AutoClear = True
        Me.Limit2Enabled = False
        Me.Limit2LowerLevel = -1
        Me.Limit2LowerSource = 0
        Me.Limit2UpperLevel = 1
        Me.Limit2UpperSource = 0
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <value> The Immediate command. </value>
    Protected Overrides Property ImmediateCommand As String = ":CALC3:IMM"

    ''' <summary> Gets or sets the limit 1 clear command. </summary>
    ''' <value> The limit 1 clear command. </value>
    Protected Overrides Property Limit1ClearCommand As String = ":CALC3:LIM1:CLE"

    ''' <summary> Gets or sets the limit 2 clear command. </summary>
    ''' <value> The limit 2 clear command. </value>
    Protected Overrides Property Limit2ClearCommand As String = ":CALC3:LIM2:CLE"

#End Region

#Region " BINNING ENABLED "

    ''' <summary> Gets or sets the Binning Strobe Enabled status command Format. </summary>
    ''' <value> The Binning Strobe Enabled status query command. </value>
    Protected Overrides Property BinningStrobeEnabledCommandFormat As String = ":CALC3:BSTR:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Binning Strobe Enabled status query command. </summary>
    ''' <value> The Binning Strobe Enabled status query command. </value>
    Protected Overrides Property BinningStrobeEnabledQueryCommand As String = ":CALC3:BSTR:STAT?"

#End Region

#Region " PASS SOURCE "

    ''' <summary> Gets or sets The Pass Source command format. </summary>
    ''' <value> The Pass Source command format. </value>
    Protected Overrides Property PassSourceCommandFormat As String = ":CALC3:PASS:SOUR {0}"

    ''' <summary> Gets or sets The Pass Source query command. </summary>
    ''' <value> The Pass Source query command. </value>
    Protected Overrides Property PassSourceQueryCommand As String = ":CALC3:PASS:SOUR?"

#End Region

#Region " LIMITs FAILED "

    ''' <summary> Gets or sets the Limits Failed query command. </summary>
    ''' <value> The Limits Failed query command. </value>
    Protected Overrides Property LimitsFailedQueryCommand As String = ":CALC3:CLIM:FAIL?"

#End Region

#Region " LIMIT 1 "

#Region " LIMIT1 AUTO CLEAR "

    ''' <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
    ''' <value> The Limit1 AutoClear query command. </value>
    Protected Overrides Property Limit1AutoClearCommandFormat As String = ":CALC3:LIM1:CLE:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
    ''' <value> The Limit1 AutoClear query command. </value>
    Protected Overrides Property Limit1AutoClearQueryCommand As String = ":CALC3:LIM1:CLE:AUTO?"

#End Region

#Region " LIMIT1 ENABLED "

    ''' <summary> Gets or sets the Limit1 enabled command Format. </summary>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overrides Property Limit1EnabledCommandFormat As String = ":CALC3:LIM1:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit1 enabled query command. </summary>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overrides Property Limit1EnabledQueryCommand As String = ":CALC3:LIM1:STAT?"

#End Region

#Region " LIMIT1 FAILED "

    ''' <summary> Gets or sets the Limit1 Failed query command. </summary>
    ''' <value> The Limit1 Failed query command. </value>
    Protected Overrides Property Limit1FailedQueryCommand As String = ":CALC3:LIM1:FAIL?"

#End Region

#Region " LIMIT1 LOWER LEVEL "

    ''' <summary> Gets or sets the Limit1 Lower Level command format. </summary>
    ''' <value> The Limit1LowerLevel command format. </value>
    Protected Overrides Property Limit1LowerLevelCommandFormat As String = ":CALC3:LIM1:LOW {0}"

    ''' <summary> Gets or sets the Limit1 Lower Level query command. </summary>
    ''' <value> The Limit1LowerLevel query command. </value>
    Protected Overrides Property Limit1LowerLevelQueryCommand As String = ":CALC3:LIM1:LOW?"

#End Region

#Region " Limit1 UPPER LEVEL "

    ''' <summary> Gets or sets the Limit1 Upper Level command format. </summary>
    ''' <value> The Limit1UpperLevel command format. </value>
    Protected Overrides Property Limit1UpperLevelCommandFormat As String = ":CALC3:LIM1:UPP {0}"

    ''' <summary> Gets or sets the Limit1 Upper Level query command. </summary>
    ''' <value> The Limit1UpperLevel query command. </value>
    Protected Overrides Property Limit1UpperLevelQueryCommand As String = ":CALC3:LIM1:UPP?"

#End Region

#Region " LIMIT1 LOWER SOURCE "

    ''' <summary> Gets or sets the Limit1 Lower Source command format. </summary>
    ''' <value> The Limit1LowerSource command format. </value>
    Protected Overrides Property Limit1LowerSourceCommandFormat As String = ":CALC3:LIM1:LOW:SOUR {0}"

    ''' <summary> Gets or sets the Limit1 Lower Source query command. </summary>
    ''' <value> The Limit1LowerSource query command. </value>
    Protected Overrides Property Limit1LowerSourceQueryCommand As String = ":CALC3:LIM1:LOW:SOUR?"

#End Region

#Region " LIMIT1 UPPER SOURCE "

    ''' <summary> Gets or sets the Limit1 Upper Source command format. </summary>
    ''' <value> The Limit1UpperSource command format. </value>
    Protected Overrides Property Limit1UpperSourceCommandFormat As String = ":CALC3:LIM1:UPP:SOUR {0}"

    ''' <summary> Gets or sets the Limit1 Upper Source query command. </summary>
    ''' <value> The Limit1UpperSource query command. </value>
    Protected Overrides Property Limit1UpperSourceQueryCommand As String = ":CALC3:LIM1:UPP:SOUR?"

#End Region

#End Region

#Region " LIMIT 2 "

#Region " LIMIT2 AUTO CLEAR "

    ''' <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
    ''' <value> The Limit2 AutoClear query command. </value>
    Protected Overrides Property Limit2AutoClearCommandFormat As String = ":CALC3:LIM2:CLE:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
    ''' <value> The Limit2 AutoClear query command. </value>
    Protected Overrides Property Limit2AutoClearQueryCommand As String = ":CALC3:LIM2:CLE:AUTO?"

#End Region

#Region " LIMIT2 ENABLED "

    ''' <summary> Gets or sets the Limit2 enabled command Format. </summary>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overrides Property Limit2EnabledCommandFormat As String = ":CALC3:LIM2:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit2 enabled query command. </summary>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overrides Property Limit2EnabledQueryCommand As String = ":CALC3:LIM2:STAT?"

#End Region

#Region " LIMIT2 FAILED "

    ''' <summary> Gets or sets the Limit2 Failed query command. </summary>
    ''' <value> The Limit2 Failed query command. </value>
    Protected Overrides Property Limit2FailedQueryCommand As String = ":CALC3:LIM2:FAIL?"

#End Region

#Region " LIMIT2 LOWER LEVEL "

    ''' <summary> Gets or sets the Limit2 Lower Level command format. </summary>
    ''' <value> The Limit2LowerLevel command format. </value>
    Protected Overrides Property Limit2LowerLevelCommandFormat As String = ":CALC3:LIM2:LOW {0}"

    ''' <summary> Gets or sets the Limit2 Lower Level query command. </summary>
    ''' <value> The Limit2LowerLevel query command. </value>
    Protected Overrides Property Limit2LowerLevelQueryCommand As String = ":CALC3:LIM2:LOW?"

#End Region

#Region " LIMIT2 UPPER LEVEL "

    ''' <summary> Gets or sets the Limit2 Upper Level command format. </summary>
    ''' <value> The Limit2UpperLevel command format. </value>
    Protected Overrides Property Limit2UpperLevelCommandFormat As String = ":CALC3:LIM2:UPP {0}"

    ''' <summary> Gets or sets the Limit2 Upper Level query command. </summary>
    ''' <value> The Limit2UpperLevel query command. </value>
    Protected Overrides Property Limit2UpperLevelQueryCommand As String = ":CALC3:LIM2:UPP?"

#End Region

#Region " LIMIT2 LOWER SOURCE "

    ''' <summary> Gets or sets the Limit2 Lower Source command format. </summary>
    ''' <value> The Limit2LowerSource command format. </value>
    Protected Overrides Property Limit2LowerSourceCommandFormat As String = ":CALC3:LIM2:LOW:SOUR {0}"

    ''' <summary> Gets or sets the Limit2 Lower Source query command. </summary>
    ''' <value> The Limit2LowerSource query command. </value>
    Protected Overrides Property Limit2LowerSourceQueryCommand As String = ":CALC3:LIM2:LOW:SOUR?"

#End Region

#Region " LIMIT2 UPPER SOURCE "

    ''' <summary> Gets or sets the Limit2 Upper Source command format. </summary>
    ''' <value> The Limit2UpperSource command format. </value>
    Protected Overrides Property Limit2UpperSourceCommandFormat As String = ":CALC3:LIM2:UPP:SOUR {0}"

    ''' <summary> Gets or sets the Limit2 Upper Source query command. </summary>
    ''' <value> The Limit2UpperSource query command. </value>
    Protected Overrides Property Limit2UpperSourceQueryCommand As String = ":CALC3:LIM2:UPP:SOUR?"

#End Region

#End Region

End Class
