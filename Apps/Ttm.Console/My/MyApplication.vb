'---------------------------------------------------------------------------------------------------
' file:		.\My\MyApplication_Info.vb
'
' summary:	My application information class
'---------------------------------------------------------------------------------------------------
Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.VI.Pith.My.ProjectTraceEventId.TtmConsole

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "TTM Driver Console"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Thermal Transient Meter Driver Console"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "TTM.Driver.Console"

    End Class

End Namespace

