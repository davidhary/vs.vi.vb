'---------------------------------------------------------------------------------------------------
' file:		.\Views\SplitVisaSesssionView.vb
'
' summary:	Split visa sesssion view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A visa session view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-04-24 </para>
''' </remarks>
Public Class SplitVisaSessionView
    Inherits isr.VI.Facade.SplitVisaView

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.AssignTopHeader(New DisplayView)
        Me.AssignVisaSessionStatusView(New SessionStatusView)
    End Sub

    ''' <summary> Creates a new <see cref="SplitVisaSessionView"/> </summary>
    ''' <returns> A <see cref="SplitVisaSessionView"/>. </returns>
    Public Overloads Shared Function Create() As SplitVisaSessionView
        Dim view As SplitVisaSessionView = Nothing
        Try
            view = New SplitVisaSessionView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " VISA SESSION BASE (DEVICE BASE) "

    ''' <summary> Binds the visa session base (device base) to its controls. </summary>
    ''' <param name="visaSessionBase"> The visa session base (device base) view model. </param>
    Public Overrides Sub BindVisaSessionBase(ByVal visaSessionBase As VisaSessionBase)
        MyBase.BindVisaSessionBase(visaSessionBase)
        Me.TopHeader.BindVisaSessionBase(visaSessionBase)
        Me.SessionStatusView.BindVisaSessionBase(visaSessionBase)
        Me.SessionStatusView.StatusView.SessionSettings = isr.VI.Pith.My.MySettings.Default
        Me.SessionStatusView.StatusView.DeviceSettings = Nothing
        Me.SessionStatusView.StatusView.UserInterfaceSettings = My.Settings
    End Sub

#End Region

#Region " DEVICE EVENTS "

    ''' <summary>
    ''' Event handler. Called upon device opening so as to instantiated all subsystems.
    ''' </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.DeviceOpening(sender, e)
        Me.PublishVerbose($"Opening access to {Me.ResourceName};. ")
    End Sub

    ''' <summary>
    ''' Event handler. Called after the device opened and all subsystems were defined.
    ''' </summary>
    ''' <param name="sender"> <see cref="T:System.Object" /> instance of this
    '''                       <see cref="T:System.Windows.Forms.Control" /> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.DeviceOpened(sender, e)
        Dim activity As String = String.Empty
        Try
            activity = "displaying top header resource name" : Me.PublishVerbose($"{activity};. ")
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Device initializing. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Overrides Sub DeviceInitializing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.DeviceInitializing(sender, e)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overrides Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.DeviceInitialized(sender, e)
    End Sub

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <param name="sender"> <see cref="T:System.Object" /> instance of this
    '''                                             <see cref="T:System.Windows.Forms.Control" /> 
    ''' </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.DeviceClosing(sender, e)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.DeviceClosed(sender, e)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

#End Region

#Region " TOP HEADER "

    ''' <summary> Gets or sets the top header. </summary>
    ''' <value> The top header. </value>
    Private ReadOnly Property TopHeader As DisplayView

    ''' <summary> Assign top header. </summary>
    ''' <param name="topHeader"> The top header. </param>
    Private Sub AssignTopHeader(ByVal topHeader As DisplayView)
        Me._TopHeader = topHeader
        MyBase.AddHeader(topHeader)
    End Sub

#End Region

#Region " VISA SESSION STATUS CONNECTOR VIEW "

    ''' <summary> Gets or sets the session status view. </summary>
    ''' <value> The session status view. </value>
    Private ReadOnly Property SessionStatusView As SessionStatusView

    ''' <summary> Gets or sets the name of the visa session status node. </summary>
    ''' <value> The name of the visa session status node. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionStatusNodeName As String = "Session"

    ''' <summary> Assign visa session status view. </summary>
    ''' <param name="sessionStatusView"> The session status view. </param>
    Private Sub AssignVisaSessionStatusView(ByVal sessionStatusView As SessionStatusView)
        Me._SessionStatusView = sessionStatusView
        MyBase.AddNode(Me.VisaSessionStatusNodeName, Me.VisaSessionStatusNodeName, Me.TopHeader)
    End Sub

#End Region

#Region " SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("Device Facade Settings Editor", My.Settings)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
