<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TriggerView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TriggerView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._ArmLayer1View = New isr.VI.Facade.ArmLayerView()
        Me._ArmLayer2View = New isr.VI.Facade.ArmLayerView()
        Me._TriggerLayer1View = New isr.VI.Facade.TriggerLayerView()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._InitiateButton = New System.Windows.Forms.ToolStripButton()
        Me._AbortButton = New System.Windows.Forms.ToolStripButton()
        Me._SendBusTriggerButton = New System.Windows.Forms.ToolStripButton()
        Me._Layout.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 2
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._Layout.Controls.Add(Me._InfoTextBox, 0, 4)
        Me._Layout.Controls.Add(Me._ArmLayer1View, 0, 1)
        Me._Layout.Controls.Add(Me._ArmLayer2View, 0, 2)
        Me._Layout.Controls.Add(Me._TriggerLayer1View, 0, 3)
        Me._Layout.Controls.Add(Me._SubsystemToolStrip, 0, 0)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 5
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._Layout.Size = New System.Drawing.Size(415, 352)
        Me._Layout.TabIndex = 0
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InfoTextBox.Location = New System.Drawing.Point(3, 310)
        Me._InfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ReadOnly = True
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InfoTextBox.Size = New System.Drawing.Size(407, 38)
        Me._InfoTextBox.TabIndex = 14
        Me._InfoTextBox.Text = "<info>"
        '
        '_ArmLayer1View
        '
        Me._ArmLayer1View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ArmLayer1View.Dock = System.Windows.Forms.DockStyle.Top
        Me._ArmLayer1View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ArmLayer1View.LayerNumber = 1
        Me._ArmLayer1View.Location = New System.Drawing.Point(3, 28)
        Me._ArmLayer1View.Name = "_ArmLayer1View"
        Me._ArmLayer1View.Padding = New System.Windows.Forms.Padding(1)
        Me._ArmLayer1View.Size = New System.Drawing.Size(407, 88)
        Me._ArmLayer1View.TabIndex = 0
        '
        '_ArmLayer2View
        '
        Me._ArmLayer2View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ArmLayer2View.Dock = System.Windows.Forms.DockStyle.Top
        Me._ArmLayer2View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ArmLayer2View.LayerNumber = 2
        Me._ArmLayer2View.Location = New System.Drawing.Point(3, 122)
        Me._ArmLayer2View.Name = "_ArmLayer2View"
        Me._ArmLayer2View.Padding = New System.Windows.Forms.Padding(1)
        Me._ArmLayer2View.Size = New System.Drawing.Size(407, 88)
        Me._ArmLayer2View.TabIndex = 1
        '
        '_TriggerLayer1View
        '
        Me._TriggerLayer1View.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._TriggerLayer1View.Dock = System.Windows.Forms.DockStyle.Top
        Me._TriggerLayer1View.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerLayer1View.LayerNumber = 1
        Me._TriggerLayer1View.Location = New System.Drawing.Point(3, 216)
        Me._TriggerLayer1View.Name = "_TriggerLayer1View"
        Me._TriggerLayer1View.Padding = New System.Windows.Forms.Padding(1)
        Me._TriggerLayer1View.Size = New System.Drawing.Size(407, 87)
        Me._TriggerLayer1View.TabIndex = 2
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.AutoSize = False
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._InitiateButton, Me._AbortButton, Me._SendBusTriggerButton})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(413, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 9
        Me._SubsystemToolStrip.Text = "Trigger"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(60, 22)
        Me._SubsystemSplitButton.Text = "Trigger"
        Me._SubsystemSplitButton.ToolTipText = "Subsystem actions split button"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(180, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        '
        '_InitiateButton
        '
        Me._InitiateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._InitiateButton.Image = CType(resources.GetObject("_InitiateButton.Image"), System.Drawing.Image)
        Me._InitiateButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._InitiateButton.Name = "_InitiateButton"
        Me._InitiateButton.Size = New System.Drawing.Size(47, 22)
        Me._InitiateButton.Text = "Initiate"
        Me._InitiateButton.ToolTipText = "Starts the trigger plan"
        '
        '_AbortButton
        '
        Me._AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AbortButton.Image = CType(resources.GetObject("_AbortButton.Image"), System.Drawing.Image)
        Me._AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AbortButton.Margin = New System.Windows.Forms.Padding(10, 1, 0, 2)
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.Size = New System.Drawing.Size(41, 22)
        Me._AbortButton.Text = "Abort"
        Me._AbortButton.ToolTipText = "Aborts a trigger plan"
        '
        '_SendBusTriggerButton
        '
        Me._SendBusTriggerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SendBusTriggerButton.Image = CType(resources.GetObject("_SendBusTriggerButton.Image"), System.Drawing.Image)
        Me._SendBusTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SendBusTriggerButton.Name = "_SendBusTriggerButton"
        Me._SendBusTriggerButton.Size = New System.Drawing.Size(43, 22)
        Me._SendBusTriggerButton.Text = "Assert"
        Me._SendBusTriggerButton.ToolTipText = "Sends a bus trigger to the instrument"
        '
        'TriggerView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "TriggerView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(417, 354)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ArmLayer1View As Facade.ArmLayerView
    Private WithEvents _ArmLayer2View As Facade.ArmLayerView
    Private WithEvents _TriggerLayer1View As Facade.TriggerLayerView
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _InitiateButton As Windows.Forms.ToolStripButton
    Private WithEvents _AbortButton As Windows.Forms.ToolStripButton
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _SendBusTriggerButton As Windows.Forms.ToolStripButton
    Private WithEvents _InfoTextBox As Windows.Forms.TextBox
End Class
