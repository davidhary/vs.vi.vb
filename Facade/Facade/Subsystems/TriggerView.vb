'---------------------------------------------------------------------------------------------------
' file:		.\Subsystems\TriggerView.vb
'
' summary:	Trigger view class
'---------------------------------------------------------------------------------------------------
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A trigger subsystem user interface. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class TriggerView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="TriggerView"/> </summary>
    ''' <returns> A <see cref="TriggerView"/>. </returns>
    Public Shared Function Create() As TriggerView
        Dim view As TriggerView = Nothing
        Try
            view = New TriggerView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PUBLIC MEMBERS "

    ''' <summary> Adds a menu item. </summary>
    ''' <param name="item"> The item. </param>
    Public Sub AddMenuItem(ByVal item As ToolStripMenuItem)
        Me._SubsystemSplitButton.DropDownItems.Add(item)
    End Sub

    ''' <summary> Applies the trigger plan settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub ApplySettings()
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} applying arm 1 subsystem instrument settings"
            Me._ArmLayer1View.ApplySettings()
            activity = $"{Me.Device.ResourceNameCaption} applying arm 2 subsystem instrument settings"
            Me._ArmLayer2View.ApplySettings()
            activity = $"{Me.Device.ResourceNameCaption} applying trigger subsystem instrument settings"
            Me._TriggerLayer1View.ApplySettings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads instrument settings. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub ReadSettings()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading arm 1 subsystem instrument settings"
            Me._ArmLayer1View.ReadSettings()
            activity = $"{Me.Device.ResourceNameCaption} reading arm 2 subsystem instrument settings"
            Me._ArmLayer2View.ReadSettings()
            activity = $"{Me.Device.ResourceNameCaption} reading trigger subsystem instrument settings"
            Me._TriggerLayer1View.ReadSettings()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initiates the trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub Initiate()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me._TriggerLayer1View.TriggerSubsystem.StartElapsedStopwatch()
            Me._TriggerLayer1View.TriggerSubsystem.Initiate()
            Me._TriggerLayer1View.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
            Try
                activity = $"{Me.Device.ResourceNameCaption} aborting trigger plan"
                Me._TriggerLayer1View.TriggerSubsystem.Abort()
            Catch
                Me.Device.Session.StatusPrompt = $"failed {activity}"
                activity = Me.PublishException(activity, ex)
                Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End Try
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Aborts the trigger plan. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub Abort()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} aborting trigger plan"
            Me._TriggerLayer1View.TriggerSubsystem.StartElapsedStopwatch()
            Me._TriggerLayer1View.TriggerSubsystem.Abort()
            Me._TriggerLayer1View.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Sends the bus trigger. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub SendBusTrigger()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} sending bus trigger"
            Me.Device.Session.AssertTrigger()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " CUSTOM ACTIONS "

    ''' <summary> Initiate wait read. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Sub InitiateWaitRead()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing execution state"
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} initiating wait read"

            ' Me.Device.ClearExecutionState()
            ' set the service request
            ' Me.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
            ' Me.Device.StatusSubsystem.EnableServiceRequest(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)
            ' Me.Device.Session.Write("*SRE 1") ' Set MSB bit of SRE register
            ' Me.Device.Session.Write("stat:meas:ptr 32767; ntr 0; enab 512") ' Set all PTR bits and clear all NTR bits for measurement events Set Buffer Full bit of Measurement
            ' Me.Device.Session.Write(":trac:feed calc") ' Select Calculate as reading source
            ' Me.Device.Session.Write(":trac:poin 10")   ' Set buffer size to 10 points 
            ' Me.Device.Session.Write(":trac:egr full")  ' Select Full element group

            ' trigger the initiation of the measurement letting the triggering or service request do the rest.
            activity = $"{Me.Device.ResourceNameCaption} Initiating meter" : Me.PublishVerbose($"{activity};. ")
            Me._TriggerLayer1View.TriggerSubsystem.Initiate()
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SubsystemSplitButton, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As VisaSessionBase

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As VisaSessionBase
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As VisaSessionBase)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        Me._ArmLayer1View.AssignDevice(value)
        Me._ArmLayer2View.AssignDevice(value)
        Me._TriggerLayer1View.AssignDevice(value)
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="layerNumber"> The layer number. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As ArmLayerSubsystemBase, ByVal layerNumber As Integer)
        Select Case layerNumber
            Case 1
                Me._ArmLayer1View.BindSubsystem(subsystem, layerNumber)
            Case 2
                Me._ArmLayer2View.BindSubsystem(subsystem, layerNumber)
        End Select
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="layerNumber"> The layer number. </param>
    Protected Overridable Sub BindSubsystem(ByVal subsystem As TriggerSubsystemBase, ByVal layerNumber As Integer)
        Me._TriggerLayer1View.BindSubsystem(subsystem, layerNumber)
        If subsystem Is Nothing Then
            RemoveHandler Me._TriggerLayer1View.PropertyChanged, AddressOf Me.TriggerLayer1ViewPropertyChanged
        Else
            AddHandler Me._TriggerLayer1View.PropertyChanged, AddressOf Me.TriggerLayer1ViewPropertyChanged
            Me.HandlePropertyChanged(Me._TriggerLayer1View, NameOf(isr.VI.Facade.TriggerLayerView.Count))
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As VisaSessionBase)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TRIGGER LAYER 1 VIEW "

    ''' <summary> Gets the arm layer 1 source. </summary>
    ''' <value> The arm layer 1 source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayer1Source As ArmSources
        Get
            Return Me._ArmLayer1View.Source
        End Get
    End Property

    ''' <summary> Gets the arm layer 2 source. </summary>
    ''' <value> The arm layer 2 source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayer2Source As ArmSources
        Get
            Return Me._ArmLayer2View.Source
        End Get
    End Property

    ''' <summary> Gets the trigger layer source. </summary>
    ''' <value> The trigger layer source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerLayerSource As TriggerSources
        Get
            Return Me._TriggerLayer1View.Source
        End Get
    End Property

    ''' <summary> Gets the number of triggers. </summary>
    ''' <value> The number of triggers. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerCount As Integer
        Get
            Return Me._TriggerLayer1View.Count
        End Get
    End Property

    ''' <summary> Handle the TriggerLayer1 view property changed event. </summary>
    ''' <param name="view">         The view. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Protected Overridable Sub HandlePropertyChanged(ByVal view As VI.Facade.TriggerLayerView, ByVal propertyName As String)
        If view Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Facade.TriggerLayerView.Source)
                Me._SendBusTriggerButton.Enabled = view.Source = TriggerSources.Bus
        End Select
    End Sub

    ''' <summary> TriggerLayer1 view property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerLayer1ViewPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(isr.VI.Facade.TriggerLayerView)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerLayer1ViewPropertyChanged), New Object() {sender, e})
            ElseIf Me._SubsystemToolStrip.InvokeRequired Then
                ' Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                Me._SubsystemToolStrip.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerLayer1ViewPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Facade.TriggerLayerView), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Applies the settings tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplySettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _ApplySettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplySettings()
    End Sub

    ''' <summary> Reads settings tool strip menu item click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ReadSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadSettingsMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ReadSettings()
    End Sub

    ''' <summary> Initiate click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InitiateButton_Click(sender As Object, e As EventArgs) Handles _InitiateButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.Initiate()
    End Sub

    ''' <summary> Abort button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AbortButton_Click(sender As Object, e As EventArgs) Handles _AbortButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.Abort()
    End Sub

    ''' <summary> Sends the bus trigger button click. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SendBusTriggerButton_Click(sender As Object, e As EventArgs) Handles _SendBusTriggerButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.SendBusTrigger()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Assigns talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As isr.Core.ITraceMessageTalker)
        Me._ArmLayer1View.AssignTalker(talker)
        Me._ArmLayer2View.AssignTalker(talker)
        Me._TriggerLayer1View.AssignTalker(talker)
        ' assigned last as this identifies all talkers.
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
