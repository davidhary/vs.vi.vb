''' <summary> The Subsystems Test Information. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class SubsystemsSettings
    Inherits isr.VI.DeviceTests.SubsystemsSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(SubsystemsSettings)} Editor", SubsystemsSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As SubsystemsSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SubsystemsSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New SubsystemsSettings()), SubsystemsSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the Initial scan list settings. </summary>
    ''' <value> The initial scan list settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1,2,3,4,5,6,7,8,9,10)")>
    Public Overrides Property InitialScanList As String
        Get
            Return MyBase.InitialScanList
        End Get
        Set(value As String)
            MyBase.InitialScanList = value
        End Set
    End Property

    ''' <summary> Gets or sets the scan card installed. </summary>
    ''' <value> The scan card installed. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overrides Property ScanCardInstalled As Boolean
        Get
            Return MyBase.ScanCardInstalled
        End Get
        Set(value As Boolean)
            MyBase.ScanCardInstalled = value
        End Set
    End Property


#End Region

End Class

Partial Friend Module K2002Properties

    ''' <summary> Gets information describing the Keithley 2002 subsystems. </summary>
    ''' <value> Information describing the Keithley 2002 subsystems. </value>
    Public ReadOnly Property K2002SubsystemsInfo As isr.VI.K2002Tests.SubsystemsSettings
        Get
            Return isr.VI.K2002Tests.SubsystemsSettings.Get
        End Get
    End Property
End Module


