''' <summary> Defines a Format Subsystem for a Keithley 2002 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class FormatSubsystem
    Inherits VI.FormatSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="FormatSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        ' at this time, Readings does not parse the 2002 reading string, which has a complex structure 
        ' note also that, because the status included in the reading string is rudimentary, it is read from the measurement register.
        ' #If to_do Then
        '        Me.OrderedSupportedElements = New List(Of ReadingElementTypes) From {ReadingElementTypes.Reading, ReadingElementTypes.Timestamp,
        '                                                                             ReadingElementTypes.ReadingNumber, ReadingElementTypes.Channel,
        '                                                                             ReadingElementTypes.Units, ReadingElementTypes.Status}
        ' #Else
        '        Me.OrderedSupportedElements = New List(Of ReadingElementTypes) From {ReadingElementTypes.Reading}
        ' #End If
#If to_do Then
        Me.OrderedSupportedElements = New List(Of ReadingElementTypes) From {ReadingElementTypes.Reading, ReadingElementTypes.Timestamp,
                                                                             ReadingElementTypes.ReadingNumber, ReadingElementTypes.Channel,
                                                                             ReadingElementTypes.Units, ReadingElementTypes.Status}
#Else
        Me.OrderedSupportedElements = New List(Of ReadingElementTypes) From {ReadingElementTypes.Reading}
#End If
    End Sub

    ''' <summary> Sets the subsystem known preset state. </summary>
    ''' <remarks>
    ''' <para>
    ''' Status: N-Normal, O-Overflow, R-Relative is appended to the reading </para><para>
    ''' Units are also appended to the reading. </para>
    ''' </remarks>
    Public Overrides Sub PresetKnownState()
        MyBase.DefineKnownResetState()
        ' note the only Reading is parsed with the Reading class..
        Me.Elements = ReadingElementTypes.Reading Or ReadingElementTypes.Timestamp Or ReadingElementTypes.ReadingNumber Or
                      ReadingElementTypes.Channel Or ReadingElementTypes.Units Or ReadingElementTypes.Status
        ' therefore, the supported elements are set with Initialize Known state.
        Me.ApplyElements(Me.SupportedElements)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Elements = ReadingElementTypes.Reading
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " ELEMENTS "

    ''' <summary> Gets or sets the elements query command. </summary>
    ''' <value> The elements query command. </value>
    Protected Overrides Property ElementsQueryCommand As String = ":FORM:ELEM?"

    ''' <summary> Gets or sets the elements command format. </summary>
    ''' <value> The elements command format. </value>
    Protected Overrides Property ElementsCommandFormat As String = ":FORM:ELEM {0}"

#End Region

#End Region

End Class
