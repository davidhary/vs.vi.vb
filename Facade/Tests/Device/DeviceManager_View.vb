'---------------------------------------------------------------------------------------------------
' file:		.\Device\DeviceManager_View.vb
'
' summary:	Device manager view class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

Partial Public NotInheritable Class DeviceManager

    ''' <summary> Assert talker publish warning. </summary>
    ''' <param name="view">          The view. </param>
    ''' <param name="queueListener"> The queue listener. </param>
    Public Shared Sub AssertTalkerPublishWarning(ByVal view As isr.VI.Facade.IVisaView,
                                                 ByVal queueListener As Core.TraceMessagesQueueListener)
        Dim payload As String = $"{NameOf(isr.VI.Facade.IVisaView).SplitWords} trace message"
        Dim traceEventId As Integer = 1
        view.Talker.Publish(TraceEventType.Warning, traceEventId, payload)
        Dim fetchNumber As Integer = 0
        Dim traceMessage As Core.TraceMessage = Nothing
        isr.Core.ApplianceBase.DoEventsWaitUntil(TimeSpan.FromMilliseconds(200), Function()
                                                                                     ' with the new talker, the device identifies the following libraries: 
                                                                                     ' 0x0100 core agnostic; 0x01006 vi device and 0x01026 Keithley Meter
                                                                                     ' so these test looks for the first warning
                                                                                     fetchNumber = 0
                                                                                     traceMessage = Nothing
                                                                                     Do
                                                                                         Do Until queueListener.Any
                                                                                             isr.Core.ApplianceBase.DoEvents()
                                                                                         Loop
                                                                                         Do While queueListener.Any
                                                                                             isr.Core.ApplianceBase.DoEvents()
                                                                                             traceMessage = queueListener.TryDequeue()
                                                                                             fetchNumber += 1
                                                                                             If traceMessage.EventType <= TraceEventType.Warning Then
                                                                                                 ' we expect a single such message
                                                                                                 Exit Do
                                                                                             End If
                                                                                         Loop
                                                                                     Loop Until traceMessage.EventType <= TraceEventType.Warning
                                                                                     Return traceMessage.EventType <= TraceEventType.Warning
                                                                                 End Function)
        If traceMessage Is Nothing Then Assert.Fail($"{payload} failed to trace fetch number {fetchNumber}")
        Assert.AreEqual(traceEventId, traceMessage.Id, $"{payload} trace event id mismatch fetch #{fetchNumber} message {traceMessage.Details}")
        ' the queue may already have some messages
        'Assert.AreEqual(0, queueListener.Count, $"{payload} expected no more messages after fetch #{fetchNumber} message {traceMessage.Details}")
    End Sub

    ''' <summary> Check model. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="subsystem">        The subsystem. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub CheckModel(ByVal subsystem As VI.StatusSubsystemBase, ByVal resourceSettings As isr.VI.DeviceTests.ResourceSettingsBase)
        If subsystem Is Nothing Then Throw New ArgumentNullException(NameOf(subsystem))
        Assert.AreEqual(resourceSettings.ResourceModel, subsystem.VersionInfoBase.Model,
                        $"Version Info Model {subsystem.ResourceNameCaption} Identity: '{subsystem.VersionInfoBase.Identity}'", Globalization.CultureInfo.CurrentCulture)
    End Sub

    ''' <summary> Check selected resource name. </summary>
    ''' <remarks>
    ''' Visa Tree View Timing:<para>
    ''' Ping elapsed 0.119   </para><para>
    ''' Create device 0.125   </para><para>
    ''' Session Factory enumerating resource names 0.388   </para><para>
    ''' Connector listing resource names 0.002   </para><para>
    ''' Session factory listing resource names 0.000   </para><para>
    ''' Session factory selecting TCPIP0:192.168.0.254:gpib0,22:INSTR selected  0.337   </para><para>
    ''' Factory selected resource 0.000   </para><para>
    ''' Check selected resource name 2.293   </para><para>
    '''  </para>
    ''' Visa View Timing:<para>
    ''' Ping elapsed 0.123   </para><para>
    ''' Create device 0.097   </para><para>
    ''' Session Factory enumerating resource names 0.369   </para><para>
    ''' Connector listing resource names 0.005   </para><para>
    ''' Session factory listing resource names 0.000   </para><para>
    ''' Session factory selecting TCPIP0:192.168.0.254:gpib0,22:INSTR selected  0.354   </para><para>
    ''' Factory selected resource 0.000   </para><para>
    ''' Check selected resource name 2.298   </para><para>
    ''' </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="visaView">         The visa view control. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub CheckSelectedResourceName(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal visaView As isr.VI.Facade.IVisaView, ByVal resourceSettings As isr.VI.DeviceTests.ResourceSettingsBase)
        If visaView Is Nothing Then Throw New ArgumentNullException(NameOf(visaView))
        Assert.IsTrue(visaView.Talker IsNot Nothing, $"Talker is nothing #1")
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim e As New Core.ActionEventArgs
        Dim sessionBase As VI.VisaSessionBase = visaView.VisaSessionBase

        Assert.IsFalse(String.IsNullOrEmpty(sessionBase.Session.ResourcesFilter), "Resources filter should be defined")

        Dim activity As String = "Session Factory enumerating resource names"
        Dim factory As VI.SessionFactory = sessionBase.SessionFactory
        ' enumerate resources without filtering
        factory.EnumerateResources(False)
        testInfo.TraceMessage($"{activity} {sw.Elapsed:s\.fff}")
        sw.Restart()

        Dim actualBoolean As Boolean = factory.HasResources
        Dim expectedBoolean As Boolean = True
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{activity} should succeed")

        activity = "Connector listing resource names"
        actualBoolean = visaView.InternalResourceNamesCount > 0
        expectedBoolean = True
        testInfo.TraceMessage($"{activity} {sw.Elapsed:s\.fff}")
        sw.Restart()
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{activity} should succeed")

        activity = "Session factory listing resource names"
        actualBoolean = sessionBase.SessionFactory.ResourceNames.Count > 0
        expectedBoolean = True
        testInfo.TraceMessage($"{activity} {sw.Elapsed:s\.fff}")
        sw.Restart()
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{activity} should succeed")

        activity = $"Session factory selecting {resourceSettings.ResourceName} selected {factory.ValidatedResourceName}"
        Dim r As (Success As Boolean, Details As String) = factory.TryValidateResource(resourceSettings.ResourceName)
        testInfo.TraceMessage($"{activity} {sw.Elapsed:s\.fff}")
        sw.Restart()
        Assert.IsTrue(r.Success, $"{activity} should succeed: { r.Details}")

        Assert.IsTrue(visaView.Talker IsNot Nothing, $"Talker should not be nothing #2")

        actualBoolean = Not String.IsNullOrWhiteSpace(factory.ValidatedResourceName)
        expectedBoolean = True
        Assert.AreEqual(expectedBoolean, actualBoolean, $"Resource {resourceSettings.ResourceName} not found")

        Assert.IsTrue(visaView.Talker IsNot Nothing, $"Talker should not be nothing #3")

        activity = "Factory selected resource"
        Dim actualResource As String = factory.ValidatedResourceName
        Dim expectedResource As String = resourceSettings.ResourceName
        testInfo.TraceMessage($"{activity} {sw.Elapsed:s\.fff}")
        sw.Restart()
        Assert.AreEqual(expectedResource, actualResource, $"{activity} should match")

    End Sub

    ''' <summary> Check session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session">          The session. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub CheckSession(ByVal session As VI.Pith.SessionBase, ByVal resourceSettings As isr.VI.DeviceTests.ResourceSettingsBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Dim expectedErrorAvailableBits As Integer = VI.Pith.ServiceRequests.ErrorAvailable
        Dim actualErrorAvailableBits As Integer = session.ErrorAvailableBit
        Assert.AreEqual(expectedErrorAvailableBits, actualErrorAvailableBits, $"Error available bits on creating device.")
        Dim actualResource As String = session.ValidatedResourceName
        Dim expectedResource As String = resourceSettings.ResourceName
        Assert.AreEqual(expectedResource, actualResource, $"Visa Session validated resource mismatch")
        actualResource = session.CandidateResourceName
        expectedResource = resourceSettings.ResourceName
        Assert.AreEqual(expectedResource, actualResource, $"Visa Session candidate resource mismatch")
        actualResource = session.OpenResourceName
        expectedResource = resourceSettings.ResourceName
        Assert.AreEqual(expectedResource, actualResource, $"Visa Session open resource mismatch")
        Dim actualTitle As String = session.OpenResourceTitle
        Dim expectedTitle As String = resourceSettings.ResourceTitle
        Assert.AreEqual(expectedTitle, actualTitle, $"Visa Session open title mismatch")
        actualTitle = session.CandidateResourceTitle
        expectedTitle = resourceSettings.ResourceTitle
        Assert.AreEqual(expectedTitle, actualTitle, $"Visa Session candidate resource title mismatch")
    End Sub

    ''' <summary> Opens a session. </summary>
    ''' <remarks>
    ''' The session open overhead is due to the device initialization. Timing: <para>
    ''' Ping elapsed 0.194     </para><para>
    ''' Ping elapsed 0.000     </para><para>
    ''' Test session opened 0.627     </para><para>
    ''' Test session Closed 0.013     </para><para>
    ''' Session opened 3.803     </para><para>
    ''' Session open 6.021     </para><para>
    ''' Session checked 0.001     </para><para>
    ''' Session closed 0.019     </para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="trialNumber">      The trial number. </param>
    ''' <param name="visaSessionBase">  The session base. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub OpenSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal trialNumber As Integer,
                                  ByVal visaSessionBase As VI.VisaSessionBase, ByVal resourceSettings As isr.VI.DeviceTests.ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If visaSessionBase Is Nothing Then Throw New ArgumentNullException(NameOf(visaSessionBase))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        testInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        If visaSessionBase Is Nothing Then Throw New ArgumentNullException(NameOf(visaSessionBase))
        Dim actualBoolean As Boolean
        sw.Restart()
        Dim expectedBoolean As Boolean
        If Not visaSessionBase.IsDeviceOpen Then
            Dim r As (Success As Boolean, Details As String) = visaSessionBase.TryOpenSession(resourceSettings.ResourceName, resourceSettings.ResourceTitle)
            actualBoolean = r.Success
            expectedBoolean = True
            Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} opening session {resourceSettings.ResourceName} canceled; { r.Details}")
        End If
        actualBoolean = visaSessionBase.IsSessionOpen
        expectedBoolean = True
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} opening session {resourceSettings.ResourceName} session not open")
        testInfo.TraceMessage($"Session opened {sw.Elapsed:s\.fff}")
        sw.Restart()

        actualBoolean = visaSessionBase.IsDeviceOpen
        expectedBoolean = True
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} opening session {resourceSettings.ResourceName} device not open")

        DeviceManager.CheckModel(visaSessionBase.StatusSubsystemBase, resourceSettings)

        testInfo.AssertMessageQueue()
        If visaSessionBase.IsSessionOpen Then visaSessionBase.StatusSubsystemBase.TryQueryExistingDeviceErrors()
        testInfo.AssertMessageQueue()

    End Sub

    ''' <summary> Opens a session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="trialNumber">      The trial number. </param>
    ''' <param name="visaView">         The View control. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub OpenSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal trialNumber As Integer,
                                  ByVal visaView As isr.VI.Facade.IVisaView, ByVal resourceSettings As isr.VI.DeviceTests.ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If visaView Is Nothing Then Throw New ArgumentNullException(NameOf(visaView))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        If visaView Is Nothing Then Throw New ArgumentNullException(NameOf(visaView))
        DeviceManager.OpenSession(testInfo, trialNumber, visaView.VisaSessionBase, resourceSettings)
    End Sub

    ''' <summary> Closes a session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">        Information describing the test. </param>
    ''' <param name="trialNumber">     The trial number. </param>
    ''' <param name="visaSessionBase"> The session base. </param>
    Public Shared Sub CloseSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal trialNumber As Integer, ByVal visaSessionBase As VI.VisaSessionBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If visaSessionBase Is Nothing Then Throw New ArgumentNullException(NameOf(visaSessionBase))
        Dim actualBoolean As Boolean
        Try
            testInfo.AssertMessageQueue()
            If visaSessionBase.IsSessionOpen Then visaSessionBase.StatusSubsystemBase.TryQueryExistingDeviceErrors()
            testInfo.AssertMessageQueue()
        Catch
            Throw
        Finally
            visaSessionBase.TryCloseSession()
        End Try
        Assert.IsFalse(visaSessionBase.IsDeviceOpen, $"{visaSessionBase.ResourceNameCaption} failed closing session")
        testInfo.AssertMessageQueue()

        actualBoolean = visaSessionBase.IsDeviceOpen
        Dim expectedBoolean As Boolean = False
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} closing session {visaSessionBase.OpenResourceName} control still connected")
        actualBoolean = visaSessionBase.IsDeviceOpen
        expectedBoolean = False
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} closing session {visaSessionBase.OpenResourceName} device still open")
    End Sub

    ''' <summary> Closes a session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">    Information describing the test. </param>
    ''' <param name="trialNumber"> The trial number. </param>
    ''' <param name="visaView">    The control. </param>
    Public Shared Sub CloseSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal trialNumber As Integer, ByVal visaView As VI.Facade.IVisaView)
        If visaView Is Nothing Then Throw New ArgumentNullException(NameOf(visaView))
        DeviceManager.CloseSession(testInfo, trialNumber, visaView.VisaSessionBase)
    End Sub

    ''' <summary> Opens close session. </summary>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="trialNumber">      The trial number. </param>
    ''' <param name="visaView">         The control. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub OpenCloseSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal trialNumber As Integer,
                                       ByVal visaView As VI.Facade.IVisaView, ByVal resourceSettings As isr.VI.DeviceTests.ResourceSettingsBase)
        Try
            DeviceManager.OpenSession(testInfo, trialNumber, visaView, resourceSettings)
        Catch
            Throw
        Finally
            DeviceManager.CloseSession(testInfo, trialNumber, visaView)
        End Try
    End Sub

    ''' <summary> Assert view adds trace messages. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="device">   The device. </param>
    Public Shared Sub AssertViewAddsTraceMessages(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal device As VisaSessionBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        device.AddListener(testInfo.TraceMessagesQueueListener)
        Dim messageCount As Integer = testInfo.TraceMessagesQueueListener.Count
        Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
            ' when assigning the device to the view, the listener is assigned.
            isr.Core.ApplianceBase.DoEventsWaitUntil(TimeSpan.FromMilliseconds(400), Function()
                                                                                         Return messageCount <> testInfo.TraceMessagesQueueListener.Count
                                                                                     End Function)
            ' this often failed on the first round.
            ' Assert.AreNotEqual(messageCount, testInfo.TraceMessagesQueueListener.Count, $"Constructing a device should add trace messages")

            ' view.AddListener(TestInfo.TraceMessagesQueueListener)
            DeviceManager.AssertTalkerPublishWarning(view, testInfo.TraceMessagesQueueListener)
        End Using
    End Sub

End Class

