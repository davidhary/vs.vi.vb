'---------------------------------------------------------------------------------------------------
' file:		.\DeviceManager_IEEE488p2.vb
'
' summary:	Device manager ieee 488p 2 class
'---------------------------------------------------------------------------------------------------
Imports isr.Core.SplitExtensions

Partial Public NotInheritable Class DeviceManager

#Region " SESSION OPEN, CLOSE "

    ''' <summary> Opens Visa session Base. </summary>
    ''' <remarks>
    ''' Like <see cref="AssertOpenSession(TestSite, VisaSessionBase, ResourceSettingsBase)"/>, this
    ''' function uses the Visa Session base to open the session but does not create a status
    ''' subsystem.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="session">          The visa session. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub OpenVisaSession(ByVal testInfo As TestSite, ByVal session As VI.VisaSession, ByVal resourceSettings As ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        session.SubsystemSupportMode = SubsystemSupportMode.Native
        session.OpenSession(resourceSettings.ResourceName, resourceSettings.ResourceTitle)
        testInfo.AssertMessageQueue()
    End Sub

    ''' <summary> Closes visa session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The visa session. </param>
    Public Shared Sub CloseVisaSession(ByVal testInfo As TestSite, ByVal session As VI.VisaSession)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Try
            testInfo.AssertMessageQueue()
            session.CloseSession()
        Catch
            Throw
        Finally
        End Try
        Assert.IsFalse(session.IsDeviceOpen, $"{session.ResourceNameCaption} failed closing session")
        testInfo.AssertMessageQueue()
    End Sub

    ''' <summary> Opens session base. </summary>
    ''' <remarks>
    ''' Unlike <see cref="AssertOpenSession(TestSite, VisaSessionBase, ResourceSettingsBase)"/>, this
    ''' function uses the Session base to open the session thus not assume the existence of the
    ''' status subsystem.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="session">          The visa session. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub OpenSessionBase(ByVal testInfo As TestSite, ByVal session As VI.VisaSession, ByVal resourceSettings As ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        session.SubsystemSupportMode = SubsystemSupportMode.Native
        session.Session.OpenSession(resourceSettings.ResourceName, resourceSettings.ResourceTitle)
        testInfo.AssertMessageQueue()
    End Sub

    ''' <summary> Closes a session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The visa session. </param>
    Public Shared Sub CloseSessionBase(ByVal testInfo As TestSite, ByVal session As VI.VisaSession)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Try
            testInfo.AssertMessageQueue()
            session.Session.CloseSession()
        Catch
            Throw
        Finally
        End Try
        Assert.IsFalse(session.IsDeviceOpen, $"{session.ResourceNameCaption} failed closing session")
        testInfo.AssertMessageQueue()
    End Sub

    ''' <summary> Opens the base visa session. </summary>
    ''' <remarks>
    ''' Unlike <see cref="AssertOpenSession(TestSite, VisaSessionBase, ResourceSettingsBase)"/>, this
    ''' function uses the Session base to open the session thus not assume the existence of the
    ''' status subsystem.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="device">           The device. </param>
    ''' <param name="resourceSettings"> The resource settings. </param>
    Public Shared Sub OpenSessionBase(ByVal testInfo As TestSite, ByVal device As VI.VisaSessionBase, ByVal resourceSettings As ResourceSettingsBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        If resourceSettings Is Nothing Then Throw New ArgumentNullException(NameOf(resourceSettings))
        If Not resourceSettings.ResourcePinged Then Assert.Inconclusive($"{resourceSettings.ResourceTitle} not found")
        device.SubsystemSupportMode = SubsystemSupportMode.Native
        device.Session.OpenSession(resourceSettings.ResourceName, resourceSettings.ResourceTitle)
        testInfo.AssertMessageQueue()
    End Sub

    ''' <summary> Closes a session. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="device">   The device. </param>
    Public Shared Sub CloseSessionBase(ByVal testInfo As TestSite, ByVal device As VI.VisaSessionBase)
        If testInfo Is Nothing Then Throw New ArgumentNullException(NameOf(testInfo))
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Try
            testInfo.AssertMessageQueue()
            device.Session.CloseSession()
        Catch
            Throw
        Finally
        End Try
        Assert.IsFalse(device.IsDeviceOpen, $"{device.ResourceNameCaption} failed closing session")
        testInfo.AssertMessageQueue()
    End Sub

#End Region

#Region " SERVICE REQUEST EVENTS "

    ''' <summary> Assert await status bitmask. </summary>
    ''' <param name="testInfo">        Information describing the test. </param>
    ''' <param name="session">         The session. </param>
    ''' <param name="expectedBitmask"> The expected bitmask. </param>
    ''' <param name="timeout">         The timeout. </param>
    Public Shared Sub AssertAwaitBitmask(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase,
                                         ByVal expectedBitmask As Pith.ServiceRequests, ByVal timeout As TimeSpan)

        Dim expectedOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = (False, expectedBitmask)
        Dim actualOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = session.AwaitStatusBitmask(expectedBitmask, timeout)

        Assert.IsFalse(actualOutcome.TimedOut, $"Getting 0x{CInt(actualOutcome.Status):X2} awaiting 0x{CInt(expectedBitmask):X2} should not time out")

        Assert.IsTrue((actualOutcome.Status And expectedOutcome.Status) <> 0,
                      $"Status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(expectedOutcome.Status):X2} bitmask should be set")
        testInfo.TraceMessage($"{session.OpenResourceTitle} status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(expectedOutcome.Status):X2} bitmask was set")

    End Sub

    ''' <summary> Assert await status bitmask. </summary>
    ''' <param name="testInfo">         Information describing the test. </param>
    ''' <param name="session">          The session. </param>
    ''' <param name="expectedBitmask">  The expected bitmask. </param>
    ''' <param name="secondaryBitmask"> The secondary bitmask. </param>
    ''' <param name="timeout">          The timeout. </param>
    Public Shared Sub AssertAwaitSetBitmasks(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase,
                                             ByVal expectedBitmask As Pith.ServiceRequests,
                                             ByVal secondaryBitmask As Pith.ServiceRequests, ByVal timeout As TimeSpan)

        Dim expectedOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = (False, expectedBitmask)
        Dim actualOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = session.AwaitStatusBitmask(expectedBitmask, timeout)

        Assert.IsFalse(actualOutcome.TimedOut, $"Awaiting 0x{CInt(expectedBitmask):X2} should not time out")

        Assert.IsTrue((actualOutcome.Status And expectedOutcome.Status) <> 0,
                      $"Status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(expectedOutcome.Status):X2} bitmask should be set")

        Assert.IsTrue((actualOutcome.Status And secondaryBitmask) <> 0,
                      $"Status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(secondaryBitmask):X2} bitmask should also be set")

        testInfo.TraceMessage($"{session.OpenResourceTitle} status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(expectedOutcome.Status):X2} bitmask was set")

    End Sub

    ''' <summary> Assert await set clear status bitmasks. </summary>
    ''' <param name="testInfo">     Information describing the test. </param>
    ''' <param name="session">      The session. </param>
    ''' <param name="setBitmask">   The set bitmask. </param>
    ''' <param name="clearBitmask"> The clear bitmask. </param>
    ''' <param name="timeout">      The timeout. </param>
    Public Shared Sub AssertAwaitSetClearBitmasks(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase,
                                                  ByVal setBitmask As Pith.ServiceRequests,
                                                  ByVal clearBitmask As Pith.ServiceRequests, ByVal timeout As TimeSpan)

        Dim expectedOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = (False, setBitmask)
        Dim actualOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = session.AwaitStatusBitmask(setBitmask, timeout)

        Assert.IsFalse(actualOutcome.TimedOut, $"Awaiting 0x{CInt(setBitmask):X2} should not time out")

        Assert.IsTrue((actualOutcome.Status And expectedOutcome.Status) <> 0,
                      $"Status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(expectedOutcome.Status):X2} bitmask should be set")

        Assert.IsTrue((actualOutcome.Status And clearBitmask) = 0,
                      $"Status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(clearBitmask):X2} bitmask should also be clear")

        testInfo.TraceMessage($"{session.OpenResourceTitle} status byte 0x{CInt(actualOutcome.Status):X2} 0x{CInt(expectedOutcome.Status):X2} bitmask was set")

    End Sub

    ''' <summary> Assert waits for status bitmask. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertWaitsForStatusBitmask(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")
        If session.ServiceRequestEventEnabled Then
            testInfo.TraceMessage($"{session.OpenResourceTitle} Disabling service request")
            session.DisableServiceRequestEventHandler()
        Else
            testInfo.TraceMessage($"{session.OpenResourceTitle} service request handler was disabled")
        End If
        Assert.IsFalse(session.ServiceRequestEventEnabled, $"Service request event should be disabled")

        Dim queryCommand As String = "*CLS; *WAI"
        session.WriteLine(queryCommand)

        isr.Core.ApplianceBase.DoEventsWait(session.StatusReadDelay)
        Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(200)
        Dim expectedBitmask As Pith.ServiceRequests = Pith.ServiceRequests.StandardEvent
        Dim expectedOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = (False, expectedBitmask)
        Dim actualOutcome As (TimedOut As Boolean, Status As Pith.ServiceRequests) = session.AwaitStatusBitmask(expectedBitmask, timeout)

        ' session.MakeEmulatedReplyIfEmpty(expectedBitmask)
        Dim task As Threading.Tasks.Task(Of (TimedOut As Boolean, Status As VI.Pith.ServiceRequests)) = Pith.SessionBase.StartAwaitingBitmaskTask(expectedBitmask,
                                                                                                                                                  timeout, Function() session.ReadStatusByte)
        task.Wait()
        ' actualOutcome = (Not .Wait(timeout), CType(.Result, Pith.ServiceRequests))
        actualOutcome = task.Result

        actualOutcome = session.AwaitStatusBitmask(expectedBitmask, timeout)

        Assert.IsTrue(actualOutcome.TimedOut, $"waiting for event summary event on *CLS is expected to time out")

    End Sub

    ''' <summary> Assert enable wait complete. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertEnableWaitComplete(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        Dim propertyName As String = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Dim actualStandardEventEnableBitmask As VI.Pith.StandardEvents = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualStandardEventEnableBitmask):X2}")

        ' program the standard event request register.

        Dim expectedStandardEventEnableBitmask As VI.Pith.StandardEvents = session.StandardEventWaitCompleteEnabledBitmask
        propertyName = NameOf(Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask).SplitWords
        Assert.IsTrue(CInt(expectedStandardEventEnableBitmask) > 0, $"{propertyName} 0x{CInt(expectedStandardEventEnableBitmask):X2} should be positive")
        session.EnableWaitComplete()

        actualStandardEventEnableBitmask = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)

        propertyName = NameOf(Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after enabling")

        session.ClearExecutionState()
        session.QueryOperationCompleted()
        actualStandardEventEnableBitmask = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        propertyName = NameOf(Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after clearing execution state.")
        testInfo.TraceMessage($"Enabled {propertyName} is 0x{CInt(actualStandardEventEnableBitmask):X2}")

    End Sub

    ''' <summary> Assert enable wait complete service request. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertEnableWaitCompleteServiceRequest(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        Dim propertyName As String = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        Dim actualServiceRequestEnableBitmask As VI.Pith.ServiceRequests = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualServiceRequestEnableBitmask):X2}")

        propertyName = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Dim actualStandardEventEnableBitmask As VI.Pith.StandardEvents = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualStandardEventEnableBitmask):X2}")

        ' program the service request register.
        Dim expectedServiceRequestEnableBitmask As VI.Pith.ServiceRequests = session.ServiceRequestWaitCompleteEnabledBitmask
        Dim expectedStandardEventEnableBitmask As VI.Pith.StandardEvents = session.StandardEventWaitCompleteEnabledBitmask
        session.EnableServiceRequestWaitComplete()

        propertyName = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        Assert.AreEqual(expectedServiceRequestEnableBitmask, session.ServiceRequestWaitCompleteEnabledBitmask, $"{propertyName} should match after enabling")

        propertyName = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, session.StandardEventWaitCompleteEnabledBitmask, $"{propertyName} should match after enabling")

        session.ClearExecutionState()
        session.QueryOperationCompleted()

        actualStandardEventEnableBitmask = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        propertyName = NameOf(Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after clearing execution state.")
        testInfo.TraceMessage($"Enabled {propertyName} is 0x{CInt(actualStandardEventEnableBitmask):X2}")

        propertyName = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        actualServiceRequestEnableBitmask = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        Assert.AreEqual(expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after clearing execution state.")
        testInfo.TraceMessage($"Enabled {propertyName} is 0x{CInt(actualServiceRequestEnableBitmask):X2}")

    End Sub

    ''' <summary> Assert enable standard service request. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertEnableStandardServiceRequest(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        Dim propertyName As String = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        Dim actualServiceRequestEnableBitmask As VI.Pith.ServiceRequests = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualServiceRequestEnableBitmask):X2}")

        propertyName = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Dim actualStandardEventEnableBitmask As VI.Pith.StandardEvents = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualStandardEventEnableBitmask):X2}")

        ' program the service request register.
        Dim expectedServiceRequestEnableBitmask As VI.Pith.ServiceRequests = session.DefaultServiceRequestEnableBitmask
        Dim expectedStandardEventEnableBitmask As VI.Pith.StandardEvents = session.DefaultStandardEventEnableBitmask
        session.ApplyStandardServiceRequestEnableBitmasks(expectedStandardEventEnableBitmask, expectedServiceRequestEnableBitmask)

        actualServiceRequestEnableBitmask = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        actualStandardEventEnableBitmask = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)

        propertyName = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        Assert.AreEqual(expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after enabling")

        propertyName = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after enabling")

        session.ClearExecutionState()

        session.QueryOperationCompleted()
        actualStandardEventEnableBitmask = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        propertyName = NameOf(Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after clearing execution state.")

        propertyName = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        actualServiceRequestEnableBitmask = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        Assert.AreEqual(expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after clearing execution state.")

    End Sub

    ''' <summary> Assert disable standard service request. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertDisableStandardServiceRequest(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        Dim propertyName As String = NameOf(Pith.SessionBase.ServiceRequestWaitCompleteEnabledBitmask).SplitWords
        Dim actualServiceRequestEnableBitmask As VI.Pith.ServiceRequests = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualServiceRequestEnableBitmask):X2}")

        propertyName = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Dim actualStandardEventEnableBitmask As VI.Pith.StandardEvents = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)
        testInfo.TraceMessage($"Initial {propertyName} is 0x{CInt(actualStandardEventEnableBitmask):X2}")

        ' program the service request register.
        Dim expectedServiceRequestEnableBitmask As VI.Pith.ServiceRequests = Pith.ServiceRequests.None
        Dim expectedStandardEventEnableBitmask As VI.Pith.StandardEvents = Pith.StandardEvents.None
        session.ApplyStandardServiceRequestEnableBitmasks(expectedStandardEventEnableBitmask, expectedServiceRequestEnableBitmask)

        actualServiceRequestEnableBitmask = CType(session.QueryServiceRequestEnableBitmask(), Pith.ServiceRequests)
        actualStandardEventEnableBitmask = CType(session.QueryStandardEventEnableBitmask, Pith.StandardEvents)

        propertyName = NameOf(Pith.SessionBase.ServiceRequestEnabledBitmask).SplitWords
        Assert.AreEqual(expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after enabling")

        propertyName = NameOf(Pith.SessionBase.StandardEventEnableBitmask).SplitWords
        Assert.AreEqual(expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after enabling")

    End Sub

    ''' <summary> Assert wait for message available. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertWaitForMessageAvailable(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        ' set service request enable
        DeviceManager.AssertEnableStandardServiceRequest(testInfo, session)

        Dim queryCommand As String = "*OPC?"
        session.WriteLine(queryCommand)

        Try
            DeviceManager.AssertAwaitSetBitmasks(testInfo, session, Pith.ServiceRequests.MessageAvailable,
                                                 Pith.ServiceRequests.RequestingService, TimeSpan.FromMilliseconds(200))
        Catch
            Throw
        Finally
            Dim result As String = session.ReadLineTrimEnd()
            ' must refresh the service request here to update the status register flags.
            session.ReadStatusRegister()
            Assert.IsTrue(result.StartsWith("0", StringComparison.OrdinalIgnoreCase) OrElse result.StartsWith("1", StringComparison.OrdinalIgnoreCase), $"{queryCommand} returns 0 or 1")
        End Try


        ' clear service request enable
        DeviceManager.AssertDisableStandardServiceRequest(testInfo, session)

        session.WriteLine(queryCommand)

        Try
            DeviceManager.AssertAwaitBitmask(testInfo, session, Pith.ServiceRequests.MessageAvailable, TimeSpan.FromMilliseconds(200))
        Catch
            Throw
        Finally
            Dim result As String = session.ReadLineTrimEnd()
            ' must refresh the service request here to update the status register flags.
            session.ReadStatusRegister()
            Assert.IsTrue(result.StartsWith("0", StringComparison.OrdinalIgnoreCase) OrElse result.StartsWith("1", StringComparison.OrdinalIgnoreCase), $"{queryCommand} returns 0 or 1")
        End Try

    End Sub

    ''' <summary> Assert wait for operation completion. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">      Information describing the test. </param>
    ''' <param name="session">       The session. </param>
    ''' <param name="actionCommand"> The action command, which, by added '?' or value can be used to
    '''                              read current value and set the same value thus not changing
    '''                              things. </param>
    Public Shared Sub AssertWaitForOperationCompletion(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase, ByVal actionCommand As String)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        DeviceManager.AssertEnableWaitComplete(testInfo, session)

        session.WriteLine(actionCommand)
        DeviceManager.AssertAwaitBitmask(testInfo, session, Pith.ServiceRequests.StandardEvent, TimeSpan.FromMilliseconds(200))

    End Sub

    ''' <summary> Assert wait for service request operation completion. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo">      Information describing the test. </param>
    ''' <param name="session">       The session. </param>
    ''' <param name="actionCommand"> The action command, which, by added '?' or value can be used to
    '''                              read current value and set the same value thus not changing
    '''                              things. </param>
    Public Shared Sub AssertWaitForServiceRequestOperationCompletion(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase, ByVal actionCommand As String)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")

        DeviceManager.AssertEnableWaitCompleteServiceRequest(testInfo, session)

        session.WriteLine(actionCommand)
        DeviceManager.AssertAwaitSetBitmasks(testInfo, session, Pith.ServiceRequests.StandardEvent, Pith.ServiceRequests.RequestingService, TimeSpan.FromMilliseconds(200))

    End Sub

    ''' <summary> Assert toggling service request handling. </summary>
    ''' <remarks> Leaves the system at it present start. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="session">  The session. </param>
    Public Shared Sub AssertToggleServiceRequestHandling(ByVal testInfo As TestSite, ByVal session As VI.Pith.SessionBase)
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        Assert.IsTrue(session.IsDeviceOpen, $"Attempted a call with closed session")
        For i As Integer = 1 To 2
            If session.ServiceRequestEventEnabled Then
                testInfo.TraceMessage("Disabling service request handling")
                session.DisableServiceRequestEventHandler()
                Assert.IsFalse(session.ServiceRequestEventEnabled, $"Service request event not disabled")
            Else
                testInfo.TraceMessage("Enabling service request handling")
                session.EnableServiceRequestEventHandler()
                Assert.IsTrue(session.ServiceRequestEventEnabled, $"Service request event not enabled")
            End If
        Next
    End Sub

#End Region

End Class
