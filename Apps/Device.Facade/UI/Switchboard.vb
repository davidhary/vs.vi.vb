'---------------------------------------------------------------------------------------------------
' file:		.\UI\Switchboard.vb
'
' summary:	Switchboard class
'---------------------------------------------------------------------------------------------------
Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports isr.VI.ExceptionExtensions

''' <summary> Switches between test panels. </summary>
''' <remarks>
''' (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2014-04-05, . based on legacy code. </para>
''' </remarks>
Public Class Switchboard
    Inherits isr.Core.Forma.ListenerFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try


            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._MessagesTextBox.AddMessage("Loading...")

            ' instantiate form objects
            Me.PopulateTestPanelSelector()

            ' set the form caption
            Me.Text = Extensions.BuildDefaultCaption("VI Switchboard")

            ' center the form
            Me.CenterToScreen()

        Catch

            Me._MessagesTextBox.AddMessage("Exception...")

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me._MessagesTextBox.AddMessage("Loaded.")

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Displays an executable path menu item click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DisplayExecutablePathMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DisplayExecutablePathMenuItem.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            activity = $"Getting {NameOf(System.Windows.Forms.Application.ExecutablePath)}"
            Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            activity = $"Displaying {NameOf(System.IO.FileInfo.FullName)}"
            Me._MessagesTextBox.AppendText(fi.FullName)
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Me.PublishException(activity, ex)
            ex.Data.Add("@isr", "Unhandled Exception.")
            System.Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SWITCH BOARD "

    ''' <summary> Open selected items. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenVirtualInstrumentConsoleButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            activity = $"Selecting VI"
            Dim vi As VirtualInstrument = CType(CType(Me._VirtualInstrumentsComboBox.SelectedItem, KeyValuePair(Of [Enum], String)).Key, VirtualInstrument)
            My.Settings.LastVirtualInstrument = CInt(vi)
            activity = $"opening {vi.Description}"
            Dim myForm As New VisaTreeViewForm(vi)
            myForm.Show()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Me.PublishException(activity, ex)
            ex.Data.Add("@isr", "Unhandled Exception.")
            System.Windows.Forms.MessageBox.Show(ex.ToFullBlownString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Populates the list of test panels. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub PopulateTestPanelSelector()
        Me._VirtualInstrumentsComboBox.ComboBox.DataSource = Nothing
        Me._VirtualInstrumentsComboBox.ComboBox.Items.Clear()
        Me._VirtualInstrumentsComboBox.ComboBox.DataSource = Extensions.ValueDescriptionPairs(GetType(VirtualInstrument))
        Me._VirtualInstrumentsComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._VirtualInstrumentsComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        If [Enum].IsDefined(GetType(VirtualInstrument), My.Settings.LastVirtualInstrument) Then
            Me._VirtualInstrumentsComboBox.ComboBox.SelectedItem = Extensions.ValueDescriptionPair(CType(My.Settings.LastVirtualInstrument, VirtualInstrument))
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        VI.My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

Public Module Extensions

    ''' <summary> Adds a message to 'message'. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="box">     The box control. </param>
    ''' <param name="message"> The message. </param>
    <Extension()>
    Public Sub AddMessage(ByVal box As TextBox, ByVal message As String)
        If box IsNot Nothing Then
            box.SelectionStart = box.Text.Length
            box.SelectionLength = 0
            box.SelectedText = message & Environment.NewLine
        End If
    End Sub

    ''' <summary>
    ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    <Extension()>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then Return String.Empty

        Dim candidate As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
        Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            candidate = attributes(0).Description
        End If
        Return candidate

    End Function

    ''' <summary> Gets a Key Value Pair description item. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A list of. </returns>
    <Extension()>
    Public Function ValueDescriptionPair(ByVal value As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
        Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(value, value.Description)
    End Function

    ''' <summary> Value description pairs. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="type"> The type. </param>
    ''' <returns> An IList. </returns>
    <Extension()>
    Public Function ValueDescriptionPairs(ByVal type As Type) As IList

        Dim keyValuePairs As ArrayList = New ArrayList()
        For Each value As System.Enum In System.Enum.GetValues(type)
            keyValuePairs.Add(value.ValueDescriptionPair())
        Next value
        Return keyValuePairs

    End Function

    ''' <summary> Builds the default caption. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDefaultCaption(ByVal subtitle As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.Append(My.Application.Info.Title)
        builder.Append(" ")
        builder.Append(My.Application.Info.Version.ToString)
        If My.Application.Info.Version.Major < 1 Then
            builder.Append(".")
            Select Case My.Application.Info.Version.Minor
                Case 0
                    builder.Append("Alpha")
                Case 1
                    builder.Append("Beta")
                Case 2 To 8
                    builder.Append($"RC{My.Application.Info.Version.Minor - 1}")
                Case Else
                    builder.Append("Gold")
            End Select
        End If
        If Not String.IsNullOrWhiteSpace(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString

    End Function

End Module
