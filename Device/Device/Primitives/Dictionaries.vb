''' <summary>
''' Dictionary of ranges ordered by integer key, which could come from function modes.
''' </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-01-16 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class RangeDictionary
    Inherits Collections.Generic.Dictionary(Of Integer, Core.Constructs.RangeR)
End Class

''' <summary> Dictionary of boolean values, which could be searched by function modes. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-02-08 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class BooleanDictionary
    Inherits Collections.Generic.Dictionary(Of Integer, Boolean)
End Class

''' <summary>
''' Dictionary of <see cref="Arebis.TypedUnits.Unit"/> which could be searched by function modes.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-03-30 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class UnitDictionary
    Inherits Collections.Generic.Dictionary(Of Integer, Arebis.TypedUnits.Unit)
End Class

''' <summary> Dictionary of integer values, which could be searched by function modes. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-03-30 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class IntegerDictionary
    Inherits Collections.Generic.Dictionary(Of Integer, Integer)
End Class
