'---------------------------------------------------------------------------------------------------
' file:		.\Tsp\Syntax\LocalNodeSyntax.vb
'
' summary:	Local node syntax class
'---------------------------------------------------------------------------------------------------
Namespace TspSyntax.LocalNode

    ''' <summary> Defines the TSP Local Node syntax. Modified for TSP2. </summary>
    ''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2005-01-15, 1.0.1841.x. </para></remarks>
    Public Module LocalNodeSyntax

        ''' <summary> The first line frequency query command. </summary>
        Public Const LineFrequencyQueryCommand1 As String = "localnode.linefreq"

        ''' <summary> The line frequency print command. </summary>
        Public Const LineFrequencyPrintCommand As String = "_G.print(localnode.linefreq)"

        ''' <summary> Gets or sets the 'identity query' command. </summary>
        ''' <value> The 'identity query' command. </value>
        Public ReadOnly Property IdentityQueryCommand As String = $"{VI.Pith.Ieee488.Syntax.IdentityQueryCommand} {VI.Pith.Ieee488.Syntax.WaitCommand}"

        ''' <summary> The serial number print command. </summary>
        Public Const SerialNumberPrintCommand As String = "_G.print(G.localnode.serialno)"

        ''' <summary> The serial number print number command. </summary>
        Public Const SerialNumberPrintNumberCommand As String = "_G.print(string.format('%d',_G.localnode.serialno))"

        ''' <summary> The firmware version print command. </summary>
        Public Const FirmwareVersionPrintCommand As String = "_G.print(_G.localnode.version)"

        ''' <summary> The model print command. </summary>
        Public Const ModelPrintCommand As String = "_G.print(_G.localnode.model)"

        ''' <summary> Gets or sets the IDN print command. </summary>
        ''' <remarks>
        ''' Returns a message similar to '*IDN?' with Keithley Instruments as the manufacturer.
        ''' </remarks>
        ''' <value> The identity print command format. </value>
        Public Property IdentityPrintCommand As String = "_G.print(""Keithley Instruments Inc., Model "".._G.localnode.model.."", "".._G.localnode.serialno.."", "".._G.localnode.version)"


    End Module

    ''' <summary> A bit-field of flags for specifying event log modes. </summary>
    <Flags>
    Public Enum EventLogModes

        ''' <summary> No errors. </summary>
        <ComponentModel.Description("No events")>
        None = 0

        ''' <summary> Errors only. </summary>
        <ComponentModel.Description("Errors only (eventlog.SEV_ERR)")>
        Errors = 1

        ''' <summary> Warnings only. </summary>
        <ComponentModel.Description("Warnings only (eventlog.SEV_WARN)")>
        Warnings = 2

        ''' <summary> Information only. </summary>
        <ComponentModel.Description("Information only (eventlog.SEV_INFO)")>
        Information = 4
    End Enum

End Namespace
