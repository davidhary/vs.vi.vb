<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ChannelScanView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChannelScanView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._InfoTextBox = New System.Windows.Forms.TextBox()
        Me._ChannelView = New isr.VI.Facade.ChannelView()
        Me._ScanView = New isr.VI.Facade.ScanView()
        Me._RouteToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._Layout.SuspendLayout()
        Me._RouteToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 2
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2.0!))
        Me._Layout.Controls.Add(Me._InfoTextBox, 0, 3)
        Me._Layout.Controls.Add(Me._ChannelView, 0, 1)
        Me._Layout.Controls.Add(Me._ScanView, 0, 2)
        Me._Layout.Controls.Add(Me._RouteToolStrip, 0, 0)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 4
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.Size = New System.Drawing.Size(397, 274)
        Me._Layout.TabIndex = 0
        '
        '_InfoTextBox
        '
        Me._InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InfoTextBox.Location = New System.Drawing.Point(3, 95)
        Me._InfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._InfoTextBox.Multiline = True
        Me._InfoTextBox.Name = "_InfoTextBox"
        Me._InfoTextBox.ReadOnly = True
        Me._InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InfoTextBox.Size = New System.Drawing.Size(389, 175)
        Me._InfoTextBox.TabIndex = 27
        Me._InfoTextBox.Text = "<info>"
        '
        '_ChannelView
        '
        Me._ChannelView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ChannelView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ChannelView.Dock = System.Windows.Forms.DockStyle.Top
        Me._ChannelView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ChannelView.Location = New System.Drawing.Point(3, 28)
        Me._ChannelView.Name = "_ChannelView"
        Me._ChannelView.Padding = New System.Windows.Forms.Padding(1)
        Me._ChannelView.Size = New System.Drawing.Size(389, 27)
        Me._ChannelView.TabIndex = 10
        '
        '_ScanView
        '
        Me._ScanView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ScanView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ScanView.Dock = System.Windows.Forms.DockStyle.Top
        Me._ScanView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ScanView.Location = New System.Drawing.Point(3, 61)
        Me._ScanView.Name = "_ScanView"
        Me._ScanView.Padding = New System.Windows.Forms.Padding(1)
        Me._ScanView.Size = New System.Drawing.Size(389, 27)
        Me._ScanView.TabIndex = 11
        '
        '_RouteToolStrip
        '
        Me._RouteToolStrip.AutoSize = False
        Me._RouteToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._RouteToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._RouteToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._RouteToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton})
        Me._RouteToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._RouteToolStrip.Name = "_RouteToolStrip"
        Me._RouteToolStrip.Size = New System.Drawing.Size(395, 25)
        Me._RouteToolStrip.Stretch = True
        Me._RouteToolStrip.TabIndex = 9
        Me._RouteToolStrip.Text = "Trigger"
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Image = CType(resources.GetObject("_SubsystemSplitButton.Image"), System.Drawing.Image)
        Me._SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(53, 22)
        Me._SubsystemSplitButton.Text = "Route"
        Me._SubsystemSplitButton.ToolTipText = "Subsystem actions split button"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(146, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings"
        '
        'ChannelScanView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "ChannelScanView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(399, 276)
        Me._Layout.ResumeLayout(False)
        Me._Layout.PerformLayout()
        Me._RouteToolStrip.ResumeLayout(False)
        Me._RouteToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _RouteToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ChannelView As Facade.ChannelView
    Private WithEvents _ScanView As Facade.ScanView
    Private WithEvents _InfoTextBox As Windows.Forms.TextBox
    Private WithEvents _ReadSettingsMenuItem As ToolStripMenuItem
End Class
