'---------------------------------------------------------------------------------------------------
' file:		.VI\Subsystems\RouteSubsystemBase_Terminals.vb
'
' summary:	Route subsystem base terminals class
'---------------------------------------------------------------------------------------------------

Partial Public Class RouteSubsystemBase

    ''' <summary> Define terminals mode read writes. </summary>
    Private Sub DefineTerminalsModeReadWrites()
        Me._TerminalsModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As RouteTerminalsModes In [Enum].GetValues(GetType(RouteTerminalsModes))
            Me._TerminalsModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> The front terminals selected. </summary>
    Private _FrontTerminalsSelected As Boolean?

    ''' <summary>
    ''' Gets or sets a cached value indicating whether Front terminals are selected.
    ''' </summary>
    ''' <value>
    ''' <c>True</c> if Front is Switched; <c>False</c> if not or none if not set or unknown.
    ''' </value>
    Public Property FrontTerminalsSelected() As Boolean?
        Get
            Return Me._FrontTerminalsSelected
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.FrontTerminalsSelected, value) Then
                Me._FrontTerminalsSelected = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a dictionary of output Terminals Mode parses. </summary>
    ''' <value> A Dictionary of output Terminals Mode parses. </value>
    Public ReadOnly Property TerminalsModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported terminals modes. </summary>
    Private _SupportedTerminalsModes As RouteTerminalsModes

    ''' <summary> Gets or sets the supported Terminals Modes. </summary>
    ''' <value> The supported Terminals Modes. </value>
    Public Property SupportedTerminalsModes() As RouteTerminalsModes
        Get
            Return Me._SupportedTerminalsModes
        End Get
        Set(ByVal value As RouteTerminalsModes)
            If Not Me.SupportedTerminalsModes.Equals(value) Then
                Me._SupportedTerminalsModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Route Terminals mode. </summary>
    Private _TerminalsMode As RouteTerminalsModes?

    ''' <summary> Gets or sets the cached Route Terminals mode. </summary>
    ''' <value> The Route Terminals mode or null if unknown. </value>
    Public Property TerminalsMode As RouteTerminalsModes?
        Get
            Return Me._TerminalsMode
        End Get
        Protected Set(ByVal value As RouteTerminalsModes?)
            If Not Nullable.Equals(Me.TerminalsMode, value) Then
                Me._TerminalsMode = value
                Me.NotifyPropertyChanged()
                Me.FrontTerminalsSelected = RouteTerminalsModes.Front = value.GetValueOrDefault(RouteTerminalsModes.None)
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Route Terminals mode. </summary>
    ''' <param name="value"> The <see cref="RouteTerminalsModes">Route Terminals mode</see>. </param>
    ''' <returns> The Route Terminals mode or null if unknown. </returns>
    Public Function ApplyTerminalsMode(ByVal value As RouteTerminalsModes) As RouteTerminalsModes?
        Me.WriteTerminalsMode(value)
        Return Me.QueryTerminalsMode()
    End Function

    ''' <summary> Gets or sets the terminals mode query command. </summary>
    ''' <value> The terminals mode command. </value>
    Protected Overridable Property TerminalsModeQueryCommand As String

    ''' <summary>
    ''' Queries the Route Terminals Mode. Also sets the <see cref="TerminalsMode">output on</see>
    ''' sentinel.
    ''' </summary>
    ''' <returns> The Route Terminals mode or null if unknown. </returns>
    Public Function QueryTerminalsMode() As RouteTerminalsModes?
        Me.TerminalsMode = Me.Query(Of RouteTerminalsModes)(Me.TerminalsModeQueryCommand, Me.TerminalsMode)
        Return Me.TerminalsMode
    End Function

    ''' <summary> Gets or sets the terminals mode command format. </summary>
    ''' <value> The terminals mode command format. </value>
    Protected Overridable Property TerminalsModeCommandFormat As String

    ''' <summary> Writes the Route Terminals mode. Does not read back from the instrument. </summary>
    ''' <param name="value"> The Terminal mode. </param>
    ''' <returns> The Route Terminals mode or null if unknown. </returns>
    Public Function WriteTerminalsMode(ByVal value As RouteTerminalsModes) As RouteTerminalsModes?
        Me.TerminalsMode = Me.Write(Of RouteTerminalsModes)(Me.TerminalsModeCommandFormat, value)
        Return Me.TerminalsMode
    End Function

End Class

''' <summary> Specifies the route terminals mode. </summary>
<Flags>
Public Enum RouteTerminalsModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not set ()")>
    None = 0

    ''' <summary> An enum constant representing the front option. </summary>
    <ComponentModel.Description("Front (FRON)")>
    Front = 1

    ''' <summary> An enum constant representing the rear option. </summary>
    <ComponentModel.Description("Rear (REAR)")>
    Rear = 2
End Enum
