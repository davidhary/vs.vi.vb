''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public MustInherit Class SystemSubsystemBase
    Inherits VI.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SystemSubsystemBase" /> class.
    ''' </summary>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

    #End Region

    #Region " SYNTAX "

    ''' <summary> Gets or sets the Firmware Version query command. </summary>
    ''' <value> The Firmware Version query command. </value>
    Protected Overrides Property FirmwareVersionQueryCommand As String = Tsp.Syntax.LocalNode.FirmwareVersionQueryCommand

    #End Region

End Class
