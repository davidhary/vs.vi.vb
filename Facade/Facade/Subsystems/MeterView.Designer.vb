<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MeterView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MeterView))
        Me._ReadingToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReadSplitButton = New System.Windows.Forms.ToolStripLabel()
        Me._ReadingElementTypesComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReadingElementTypesComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._MeasureValueButton = New System.Windows.Forms.ToolStripButton()
        Me._FunctionConfigurationToolStrip = New System.Windows.Forms.ToolStrip()
        Me._FunctionLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ApertureNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ApertureNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._SenseRangeNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SenseRangeNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._AutoRangeToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._AutoZeroToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._SubsystemToolStrip = New System.Windows.Forms.ToolStrip()
        Me._SubsystemSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ApplyFunctionModeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadFunctionModeMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MeasureOptionsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._MeasureImmediateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._FetchOnMeasurementEventMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AutoInitiateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ApplySettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SenseFunctionComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ApplyFunctionModeButton = New System.Windows.Forms.ToolStripButton()
        Me._FilterToolStrip = New System.Windows.Forms.ToolStrip()
        Me._FilterLabel = New System.Windows.Forms.ToolStripLabel()
        Me._FilterEnabledToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._FilterCountNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._FilterCountNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._FilterWindowNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._FilterWindowNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._WindowTypeToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._InfoToolStrip = New System.Windows.Forms.ToolStrip()
        Me._InfoLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AutoDelayToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenDetectorToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._TerminalStateReadButton = New System.Windows.Forms.ToolStripButton()
        Me._ResolutionDigitsNumericLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ResolutionDigitsNumeric = New isr.Core.Controls.ToolStripNumericUpDown()
        Me._ToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me._ReadingToolStrip.SuspendLayout()
        Me._FunctionConfigurationToolStrip.SuspendLayout()
        Me._SubsystemToolStrip.SuspendLayout()
        Me._FilterToolStrip.SuspendLayout()
        Me._InfoToolStrip.SuspendLayout()
        Me._ToolStripPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ReadingToolStrip
        '
        Me._ReadingToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._ReadingToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._ReadingToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._ReadingToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadSplitButton, Me._ReadingElementTypesComboBoxLabel, Me._ReadingElementTypesComboBox, Me._MeasureValueButton})
        Me._ReadingToolStrip.Location = New System.Drawing.Point(0, 25)
        Me._ReadingToolStrip.Name = "_ReadingToolStrip"
        Me._ReadingToolStrip.Size = New System.Drawing.Size(413, 25)
        Me._ReadingToolStrip.Stretch = True
        Me._ReadingToolStrip.TabIndex = 1
        Me._ReadingToolStrip.Text = "Meter Tool Strip"
        '
        '_ReadSplitButton
        '
        Me._ReadSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadSplitButton.DoubleClickEnabled = True
        Me._ReadSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ReadSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadSplitButton.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._ReadSplitButton.Name = "_ReadSplitButton"
        Me._ReadSplitButton.Size = New System.Drawing.Size(34, 22)
        Me._ReadSplitButton.Text = "Read"
        '
        '_ReadingElementTypesComboBoxLabel
        '
        Me._ReadingElementTypesComboBoxLabel.Name = "_ReadingElementTypesComboBoxLabel"
        Me._ReadingElementTypesComboBoxLabel.Size = New System.Drawing.Size(34, 22)
        Me._ReadingElementTypesComboBoxLabel.Text = "Type:"
        '
        '_ReadingElementTypesComboBox
        '
        Me._ReadingElementTypesComboBox.Name = "_ReadingElementTypesComboBox"
        Me._ReadingElementTypesComboBox.Size = New System.Drawing.Size(195, 25)
        Me._ReadingElementTypesComboBox.ToolTipText = "Select reading type to display"
        '
        '_MeasureValueButton
        '
        Me._MeasureValueButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._MeasureValueButton.Image = CType(resources.GetObject("_MeasureValueButton.Image"), System.Drawing.Image)
        Me._MeasureValueButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._MeasureValueButton.Name = "_MeasureValueButton"
        Me._MeasureValueButton.Size = New System.Drawing.Size(56, 22)
        Me._MeasureValueButton.Text = "Measure"
        Me._MeasureValueButton.ToolTipText = "Initiate a measurement and fetch value based on the fetch options"
        '
        '_FunctionConfigurationToolStrip
        '
        Me._FunctionConfigurationToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._FunctionConfigurationToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._FunctionConfigurationToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._FunctionConfigurationToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FunctionLabel, Me._ApertureNumericLabel, Me._ApertureNumeric, Me._SenseRangeNumericLabel, Me._SenseRangeNumeric, Me._AutoRangeToggleButton, Me._AutoZeroToggleButton})
        Me._FunctionConfigurationToolStrip.Location = New System.Drawing.Point(0, 50)
        Me._FunctionConfigurationToolStrip.Name = "_FunctionConfigurationToolStrip"
        Me._FunctionConfigurationToolStrip.Size = New System.Drawing.Size(413, 28)
        Me._FunctionConfigurationToolStrip.Stretch = True
        Me._FunctionConfigurationToolStrip.TabIndex = 2
        '
        '_FunctionLabel
        '
        Me._FunctionLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FunctionLabel.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._FunctionLabel.Name = "_FunctionLabel"
        Me._FunctionLabel.Size = New System.Drawing.Size(28, 25)
        Me._FunctionLabel.Text = "Volt"
        Me._FunctionLabel.ToolTipText = "Selected function mode"
        '
        '_ApertureNumericLabel
        '
        Me._ApertureNumericLabel.Name = "_ApertureNumericLabel"
        Me._ApertureNumericLabel.Size = New System.Drawing.Size(56, 25)
        Me._ApertureNumericLabel.Text = "Aperture:"
        '
        '_ApertureNumeric
        '
        Me._ApertureNumeric.Name = "_ApertureNumeric"
        Me._ApertureNumeric.Size = New System.Drawing.Size(41, 25)
        Me._ApertureNumeric.Text = "3"
        Me._ApertureNumeric.ToolTipText = "Measurement aperture in number of power line cycles (NPLC)"
        Me._ApertureNumeric.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        '_SenseRangeNumericLabel
        '
        Me._SenseRangeNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel"
        Me._SenseRangeNumericLabel.Size = New System.Drawing.Size(43, 25)
        Me._SenseRangeNumericLabel.Text = "Range:"
        '
        '_SenseRangeNumeric
        '
        Me._SenseRangeNumeric.Name = "_SenseRangeNumeric"
        Me._SenseRangeNumeric.Size = New System.Drawing.Size(41, 25)
        Me._SenseRangeNumeric.Text = "0"
        Me._SenseRangeNumeric.ToolTipText = "Sense range"
        Me._SenseRangeNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_AutoRangeToggleButton
        '
        Me._AutoRangeToggleButton.Checked = True
        Me._AutoRangeToggleButton.CheckOnClick = True
        Me._AutoRangeToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._AutoRangeToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AutoRangeToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AutoRangeToggleButton.Name = "_AutoRangeToggleButton"
        Me._AutoRangeToggleButton.Size = New System.Drawing.Size(45, 25)
        Me._AutoRangeToggleButton.Text = "Auto ?"
        Me._AutoRangeToggleButton.ToolTipText = "Toggle auto range"
        '
        '_AutoZeroToggleButton
        '
        Me._AutoZeroToggleButton.Checked = True
        Me._AutoZeroToggleButton.CheckOnClick = True
        Me._AutoZeroToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._AutoZeroToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AutoZeroToggleButton.Image = CType(resources.GetObject("_AutoZeroToggleButton.Image"), System.Drawing.Image)
        Me._AutoZeroToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AutoZeroToggleButton.Name = "_AutoZeroToggleButton"
        Me._AutoZeroToggleButton.Size = New System.Drawing.Size(43, 25)
        Me._AutoZeroToggleButton.Text = "Zero ?"
        Me._AutoZeroToggleButton.ToolTipText = "Toggle auto zero"
        '
        '_SubsystemToolStrip
        '
        Me._SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SubsystemToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._SubsystemToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SubsystemToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SubsystemSplitButton, Me._SenseFunctionComboBox, Me._ApplyFunctionModeButton})
        Me._SubsystemToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SubsystemToolStrip.Name = "_SubsystemToolStrip"
        Me._SubsystemToolStrip.Size = New System.Drawing.Size(413, 25)
        Me._SubsystemToolStrip.Stretch = True
        Me._SubsystemToolStrip.TabIndex = 0
        '
        '_SubsystemSplitButton
        '
        Me._SubsystemSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ApplyFunctionModeMenuItem, Me._ReadFunctionModeMenuItem, Me._MeasureOptionsMenuItem, Me._ApplySettingsMenuItem, Me._ReadSettingsMenuItem})
        Me._SubsystemSplitButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SubsystemSplitButton.Name = "_SubsystemSplitButton"
        Me._SubsystemSplitButton.Size = New System.Drawing.Size(58, 22)
        Me._SubsystemSplitButton.Text = "Meter"
        Me._SubsystemSplitButton.ToolTipText = "Selects function actions"
        '
        '_ApplyFunctionModeMenuItem
        '
        Me._ApplyFunctionModeMenuItem.Name = "_ApplyFunctionModeMenuItem"
        Me._ApplyFunctionModeMenuItem.Size = New System.Drawing.Size(185, 22)
        Me._ApplyFunctionModeMenuItem.Text = "Apply Function Mode"
        Me._ApplyFunctionModeMenuItem.ToolTipText = "Applies the selected function mode"
        '
        '_ReadFunctionModeMenuItem
        '
        Me._ReadFunctionModeMenuItem.Name = "_ReadFunctionModeMenuItem"
        Me._ReadFunctionModeMenuItem.Size = New System.Drawing.Size(185, 22)
        Me._ReadFunctionModeMenuItem.Text = "Read Function Mode"
        Me._ReadFunctionModeMenuItem.ToolTipText = "reads the instrument function mode"
        '
        '_MeasureOptionsMenuItem
        '
        Me._MeasureOptionsMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._MeasureImmediateMenuItem, Me._FetchOnMeasurementEventMenuItem, Me._AutoInitiateMenuItem})
        Me._MeasureOptionsMenuItem.Enabled = False
        Me._MeasureOptionsMenuItem.Name = "_MeasureOptionsMenuItem"
        Me._MeasureOptionsMenuItem.Size = New System.Drawing.Size(185, 22)
        Me._MeasureOptionsMenuItem.Text = "Measure Options"
        Me._MeasureOptionsMenuItem.ToolTipText = "Selects measurement options"
        '
        '_MeasureImmediateMenuItem
        '
        Me._MeasureImmediateMenuItem.Name = "_MeasureImmediateMenuItem"
        Me._MeasureImmediateMenuItem.Size = New System.Drawing.Size(225, 22)
        Me._MeasureImmediateMenuItem.Text = "Immediate"
        Me._MeasureImmediateMenuItem.ToolTipText = "Fetches a measurement irrespective of the 'Fetch of Measurement Event' setting"
        '
        '_FetchOnMeasurementEventMenuItem
        '
        Me._FetchOnMeasurementEventMenuItem.CheckOnClick = True
        Me._FetchOnMeasurementEventMenuItem.Name = "_FetchOnMeasurementEventMenuItem"
        Me._FetchOnMeasurementEventMenuItem.Size = New System.Drawing.Size(225, 22)
        Me._FetchOnMeasurementEventMenuItem.Text = "Fetch on Measurement Event"
        Me._FetchOnMeasurementEventMenuItem.ToolTipText = "Fetch on measurement event; Pressing Measure, sets the measurement event handling" &
    " and initiates a measurement, which is fetched upon the event."
        '
        '_AutoInitiateMenuItem
        '
        Me._AutoInitiateMenuItem.CheckOnClick = True
        Me._AutoInitiateMenuItem.Name = "_AutoInitiateMenuItem"
        Me._AutoInitiateMenuItem.Size = New System.Drawing.Size(225, 22)
        Me._AutoInitiateMenuItem.Text = "Auto Initiate"
        Me._AutoInitiateMenuItem.ToolTipText = "When checked, trigger plan is restarted following each data fetch"
        '
        '_ApplySettingsMenuItem
        '
        Me._ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem"
        Me._ApplySettingsMenuItem.Size = New System.Drawing.Size(185, 22)
        Me._ApplySettingsMenuItem.Text = "Apply Settings"
        Me._ApplySettingsMenuItem.ToolTipText = "Applies settings"
        '
        '_ReadSettingsMenuItem
        '
        Me._ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem"
        Me._ReadSettingsMenuItem.Size = New System.Drawing.Size(185, 22)
        Me._ReadSettingsMenuItem.Text = "Read Settings"
        Me._ReadSettingsMenuItem.ToolTipText = "Reads settings"
        '
        '_SenseFunctionComboBox
        '
        Me._SenseFunctionComboBox.Name = "_SenseFunctionComboBox"
        Me._SenseFunctionComboBox.Size = New System.Drawing.Size(200, 25)
        Me._SenseFunctionComboBox.Text = "Voltage"
        Me._SenseFunctionComboBox.ToolTipText = "Selects the function"
        '
        '_ApplyFunctionModeButton
        '
        Me._ApplyFunctionModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ApplyFunctionModeButton.Image = CType(resources.GetObject("_ApplyFunctionModeButton.Image"), System.Drawing.Image)
        Me._ApplyFunctionModeButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ApplyFunctionModeButton.Name = "_ApplyFunctionModeButton"
        Me._ApplyFunctionModeButton.Size = New System.Drawing.Size(27, 22)
        Me._ApplyFunctionModeButton.Text = "Set"
        Me._ApplyFunctionModeButton.ToolTipText = "Set the selected function"
        '
        '_FilterToolStrip
        '
        Me._FilterToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._FilterToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._FilterToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._FilterToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._FilterLabel, Me._FilterEnabledToggleButton, Me._FilterCountNumericLabel, Me._FilterCountNumeric, Me._FilterWindowNumericLabel, Me._FilterWindowNumeric, Me._WindowTypeToggleButton})
        Me._FilterToolStrip.Location = New System.Drawing.Point(0, 78)
        Me._FilterToolStrip.Name = "_FilterToolStrip"
        Me._FilterToolStrip.Size = New System.Drawing.Size(413, 28)
        Me._FilterToolStrip.Stretch = True
        Me._FilterToolStrip.TabIndex = 3
        '
        '_FilterLabel
        '
        Me._FilterLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilterLabel.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._FilterLabel.Name = "_FilterLabel"
        Me._FilterLabel.Size = New System.Drawing.Size(33, 25)
        Me._FilterLabel.Text = "Filter"
        Me._FilterLabel.ToolTipText = "Averaging Window"
        '
        '_FilterEnabledToggleButton
        '
        Me._FilterEnabledToggleButton.Checked = True
        Me._FilterEnabledToggleButton.CheckOnClick = True
        Me._FilterEnabledToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._FilterEnabledToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._FilterEnabledToggleButton.Image = CType(resources.GetObject("_FilterEnabledToggleButton.Image"), System.Drawing.Image)
        Me._FilterEnabledToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._FilterEnabledToggleButton.Name = "_FilterEnabledToggleButton"
        Me._FilterEnabledToggleButton.Size = New System.Drawing.Size(35, 25)
        Me._FilterEnabledToggleButton.Text = "On ?"
        Me._FilterEnabledToggleButton.ToolTipText = "Toggle filter enabled"
        '
        '_FilterCountNumericLabel
        '
        Me._FilterCountNumericLabel.Name = "_FilterCountNumericLabel"
        Me._FilterCountNumericLabel.Size = New System.Drawing.Size(43, 25)
        Me._FilterCountNumericLabel.Text = "Count:"
        '
        '_FilterCountNumeric
        '
        Me._FilterCountNumeric.AutoSize = False
        Me._FilterCountNumeric.Name = "_FilterCountNumeric"
        Me._FilterCountNumeric.Size = New System.Drawing.Size(50, 25)
        Me._FilterCountNumeric.Text = "0"
        Me._FilterCountNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_FilterWindowNumericLabel
        '
        Me._FilterWindowNumericLabel.Margin = New System.Windows.Forms.Padding(3, 1, 0, 2)
        Me._FilterWindowNumericLabel.Name = "_FilterWindowNumericLabel"
        Me._FilterWindowNumericLabel.Size = New System.Drawing.Size(75, 25)
        Me._FilterWindowNumericLabel.Text = "Window [%]:"
        '
        '_FilterWindowNumeric
        '
        Me._FilterWindowNumeric.AutoSize = False
        Me._FilterWindowNumeric.Name = "_FilterWindowNumeric"
        Me._FilterWindowNumeric.Size = New System.Drawing.Size(50, 25)
        Me._FilterWindowNumeric.Text = "10"
        Me._FilterWindowNumeric.ToolTipText = "Noise tolerance window in % of range"
        Me._FilterWindowNumeric.Value = New Decimal(New Integer() {9999, 0, 0, 196608})
        '
        '_WindowTypeToggleButton
        '
        Me._WindowTypeToggleButton.Checked = True
        Me._WindowTypeToggleButton.CheckOnClick = True
        Me._WindowTypeToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._WindowTypeToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._WindowTypeToggleButton.Image = CType(resources.GetObject("_WindowTypeToggleButton.Image"), System.Drawing.Image)
        Me._WindowTypeToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._WindowTypeToggleButton.Name = "_WindowTypeToggleButton"
        Me._WindowTypeToggleButton.Size = New System.Drawing.Size(60, 25)
        Me._WindowTypeToggleButton.Text = "Moving ?"
        Me._WindowTypeToggleButton.ToolTipText = "Toggle window type"
        '
        '_InfoToolStrip
        '
        Me._InfoToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._InfoToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._InfoToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._InfoToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InfoLabel, Me._AutoDelayToggleButton, Me._OpenDetectorToggleButton, Me._TerminalStateReadButton, Me._ResolutionDigitsNumericLabel, Me._ResolutionDigitsNumeric})
        Me._InfoToolStrip.Location = New System.Drawing.Point(0, 106)
        Me._InfoToolStrip.Name = "_InfoToolStrip"
        Me._InfoToolStrip.Size = New System.Drawing.Size(413, 28)
        Me._InfoToolStrip.Stretch = True
        Me._InfoToolStrip.TabIndex = 4
        '
        '_InfoLabel
        '
        Me._InfoLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InfoLabel.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._InfoLabel.Name = "_InfoLabel"
        Me._InfoLabel.Size = New System.Drawing.Size(27, 25)
        Me._InfoLabel.Text = "Info"
        '
        '_AutoDelayToggleButton
        '
        Me._AutoDelayToggleButton.Checked = True
        Me._AutoDelayToggleButton.CheckOnClick = True
        Me._AutoDelayToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._AutoDelayToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AutoDelayToggleButton.Image = CType(resources.GetObject("_AutoDelayToggleButton.Image"), System.Drawing.Image)
        Me._AutoDelayToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AutoDelayToggleButton.Name = "_AutoDelayToggleButton"
        Me._AutoDelayToggleButton.Size = New System.Drawing.Size(51, 25)
        Me._AutoDelayToggleButton.Text = "Delay: ?"
        '
        '_OpenDetectorToggleButton
        '
        Me._OpenDetectorToggleButton.Checked = True
        Me._OpenDetectorToggleButton.CheckOnClick = True
        Me._OpenDetectorToggleButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._OpenDetectorToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._OpenDetectorToggleButton.Image = CType(resources.GetObject("_OpenDetectorToggleButton.Image"), System.Drawing.Image)
        Me._OpenDetectorToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenDetectorToggleButton.Name = "_OpenDetectorToggleButton"
        Me._OpenDetectorToggleButton.Size = New System.Drawing.Size(96, 25)
        Me._OpenDetectorToggleButton.Text = "Open Detector ?"
        Me._OpenDetectorToggleButton.ToolTipText = "Toggle enabling open detector"
        '
        '_TerminalStateReadButton
        '
        Me._TerminalStateReadButton.Checked = True
        Me._TerminalStateReadButton.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me._TerminalStateReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TerminalStateReadButton.Image = CType(resources.GetObject("_TerminalStateReadButton.Image"), System.Drawing.Image)
        Me._TerminalStateReadButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TerminalStateReadButton.Name = "_TerminalStateReadButton"
        Me._TerminalStateReadButton.Size = New System.Drawing.Size(98, 25)
        Me._TerminalStateReadButton.Text = "Terminals: Rear ?"
        Me._TerminalStateReadButton.ToolTipText = "Read and display terminal state"
        '
        '_ResolutionDigitsNumericLabel
        '
        Me._ResolutionDigitsNumericLabel.Name = "_ResolutionDigitsNumericLabel"
        Me._ResolutionDigitsNumericLabel.Size = New System.Drawing.Size(40, 25)
        Me._ResolutionDigitsNumericLabel.Text = "Digits:"
        '
        '_ResolutionDigitsNumeric
        '
        Me._ResolutionDigitsNumeric.AutoSize = False
        Me._ResolutionDigitsNumeric.Name = "_ResolutionDigitsNumeric"
        Me._ResolutionDigitsNumeric.Size = New System.Drawing.Size(31, 25)
        Me._ResolutionDigitsNumeric.Text = "0"
        Me._ResolutionDigitsNumeric.ToolTipText = "Sets the measurement resolution"
        Me._ResolutionDigitsNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_ToolStripPanel
        '
        Me._ToolStripPanel.BackColor = System.Drawing.Color.Transparent
        Me._ToolStripPanel.Controls.Add(Me._SubsystemToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._ReadingToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._FunctionConfigurationToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._FilterToolStrip)
        Me._ToolStripPanel.Controls.Add(Me._InfoToolStrip)
        Me._ToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ToolStripPanel.Location = New System.Drawing.Point(1, 1)
        Me._ToolStripPanel.Name = "_ToolStripPanel"
        Me._ToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me._ToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me._ToolStripPanel.Size = New System.Drawing.Size(413, 134)
        '
        'MeterView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ToolStripPanel)
        Me.Name = "MeterView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(415, 132)
        Me._ReadingToolStrip.ResumeLayout(False)
        Me._ReadingToolStrip.PerformLayout()
        Me._FunctionConfigurationToolStrip.ResumeLayout(False)
        Me._FunctionConfigurationToolStrip.PerformLayout()
        Me._SubsystemToolStrip.ResumeLayout(False)
        Me._SubsystemToolStrip.PerformLayout()
        Me._FilterToolStrip.ResumeLayout(False)
        Me._FilterToolStrip.PerformLayout()
        Me._InfoToolStrip.ResumeLayout(False)
        Me._InfoToolStrip.PerformLayout()
        Me._ToolStripPanel.ResumeLayout(False)
        Me._ToolStripPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SubsystemToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _SenseFunctionComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _FunctionConfigurationToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _FunctionLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ApertureNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ApertureNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _SenseRangeNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SenseRangeNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _AutoZeroToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _FilterToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _FilterLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _FilterCountNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _FilterWindowNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _FilterWindowNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _AutoRangeToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _FilterCountNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _ApplyFunctionModeButton As Windows.Forms.ToolStripButton
    Private WithEvents _FilterEnabledToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _WindowTypeToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _InfoToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _InfoLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _AutoDelayToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _OpenDetectorToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _TerminalStateReadButton As Windows.Forms.ToolStripButton
    Private WithEvents _ResolutionDigitsNumericLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ResolutionDigitsNumeric As Core.Controls.ToolStripNumericUpDown
    Private WithEvents _ReadingToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ReadingElementTypesComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _ReadingElementTypesComboBoxLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _SubsystemSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _ReadFunctionModeMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ApplyFunctionModeMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _MeasureValueButton As Windows.Forms.ToolStripButton
    Private WithEvents _MeasureOptionsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _MeasureImmediateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _FetchOnMeasurementEventMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AutoInitiateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ApplySettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSettingsMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadSplitButton As Windows.Forms.ToolStripLabel
    Private WithEvents _ToolStripPanel As Windows.Forms.ToolStripPanel
End Class
