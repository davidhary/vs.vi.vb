'---------------------------------------------------------------------------------------------------
' file:		Pith\Code\ResourceFinderBase.vb
'
' summary:	Resource finder base class
'---------------------------------------------------------------------------------------------------
Imports isr.VI.Pith.ExceptionExtensions

''' <summary> A VISA Resources Manager Base class. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001.x. </para>
''' </remarks>
Public MustInherit Class ResourceFinderBase

#Region " PARSE RESOURCES "

    ''' <summary> Parse resource. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <see cref="vi.Pith.ResourceNameInfo"/>. </returns>
    Public MustOverride Function ParseResource(ByVal resourceName As String) As VI.Pith.ResourceNameInfo

    ''' <summary> Gets or sets the using like pattern. </summary>
    ''' <value> The using like pattern. </value>
    Public MustOverride ReadOnly Property UsingLikePattern As Boolean

    ''' <summary> Builds minimal resources filter. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <returns> A String. </returns>
    Public Function BuildMinimalResourcesFilter() As String
        Return VI.Pith.ResourceNamesManager.BuildInstrumentFilter(VI.Pith.HardwareInterfaceType.Gpib,
                                                          VI.Pith.HardwareInterfaceType.Tcpip,
                                                          VI.Pith.HardwareInterfaceType.Usb, Me.UsingLikePattern)
    End Function

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <returns> List of all resources. </returns>
    Public MustOverride Function FindResources() As IEnumerable(Of String)

    ''' <summary> Tries to find resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindResources() As (Success As Boolean, Details As String)
        Try
            Return (Me.FindResources().Any, "Resources not found")
        Catch ex As Exception
            Return (False, ex.ToFullBlownString)
        End Try
    End Function

    ''' <summary> Lists all resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> List of all resources. </returns>
    Public MustOverride Function FindResources(ByVal filter As String) As IEnumerable(Of String)

    ''' <summary> Tries to find resources in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns>
    ''' The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindResources(ByVal filter As String) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Try
            Dim resources As IEnumerable(Of String) = Me.FindResources(filter)
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString, Array.Empty(Of String))
        End Try
    End Function

    ''' <summary> Returns true if the specified resource exists in the resource names cache. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resourceName"> The resource naResourceManagerBase. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    Public Function Exists(ByVal resourceName As String) As Boolean
        If String.IsNullOrWhiteSpace(resourceName) Then Throw New ArgumentNullException(NameOf(resourceName))
        Dim resources As IEnumerable(Of String) = Me.FindResources()
        Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
    End Function

    ''' <summary> Searches for the interface in the resource names cache. </summary>
    ''' <param name="resourceName"> The interface resource naResourceManagerBase. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    Public Function InterfaceExists(ByVal resourceName As String) As Boolean
        Dim resourceNameInfo As VI.Pith.ResourceNameInfo = Me.ParseResource(resourceName)
        If resourceNameInfo.IsParsed Then
            Dim r As (Success As Boolean, Details As String, Resources As IEnumerable(Of String)) = Me.TryFindInterfaces(resourceNameInfo.InterfaceType)
            Return r.Success AndAlso r.Resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
        End If
        Return False
    End Function

    ''' <summary> Searches for all interfaces in the resource names cache. </summary>
    ''' <returns> The found interface resource names. </returns>
    Public MustOverride Function FindInterfaces() As IEnumerable(Of String)

    ''' <summary> Tries to find interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns>
    ''' <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindInterfaces() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Try
            Dim resources As IEnumerable(Of String) = Me.FindInterfaces()
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString, Array.Empty(Of String))
        End Try
    End Function

    ''' <summary> Searches for the interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> The found interface resource names. </returns>
    Public MustOverride Function FindInterfaces(ByVal filter As String) As IEnumerable(Of String)

    ''' <summary> Searches for the interfaces in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    Public MustOverride Function FindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))

    ''' <summary> Tries to find interfaces in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindInterfaces(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Try
            Return Me.FindInterfaces(interfaceType)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString, Array.Empty(Of String))
        End Try
    End Function

    ''' <summary> Searches for the instrument in the resource names cache. </summary>
    ''' <param name="resourceName"> The instrument resource naResourceManagerBase. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    Public Function FindInstrument(ByVal resourceName As String) As Boolean
        Dim resourceNameInfo As VI.Pith.ResourceNameInfo = Me.ParseResource(resourceName)
        If resourceNameInfo.IsParsed Then
            Dim resources As IEnumerable(Of String) = Array.Empty(Of String)()
            Dim r As (Success As Boolean, Details As String, Resources As IEnumerable(Of String)) = Me.TryFindInstruments(resourceNameInfo.InterfaceType, resourceNameInfo.InterfaceNumber)
            Return r.Success AndAlso r.Resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
        End If
        Return False
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-04-10. </remarks>
    ''' <param name="filter"> A pattern specifying the search. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments(ByVal filter As String) As IEnumerable(Of String)

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments() As IEnumerable(Of String)

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-07. </remarks>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindInstruments() As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Try
            Dim resources As IEnumerable(Of String) = Me.FindInstruments()
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString, Array.Empty(Of String))
        End Try
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As IEnumerable(Of String)

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Try
            Dim resources As IEnumerable(Of String) = Me.FindInstruments(interfaceType)
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString, Array.Empty(Of String))
        End Try
    End Function

    ''' <summary> Searches for instruments in the resource names cache. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    Public MustOverride Function FindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType, ByVal boardNumber As Integer) As IEnumerable(Of String)

    ''' <summary> Tries to find instruments in the resource names cache. </summary>
    ''' <remarks> David, 2020-06-06. </remarks>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number. </param>
    ''' <returns>
    ''' <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of
    ''' the.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function TryFindInstruments(ByVal interfaceType As VI.Pith.HardwareInterfaceType,
                                              ByVal interfaceNumber As Integer) As (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        Try
            Dim resources As IEnumerable(Of String) = Me.FindInstruments(interfaceType, interfaceNumber)
            Return (resources.Any, "Resources not found", resources)
        Catch ex As Exception
            Return (False, ex.ToFullBlownString, Array.Empty(Of String))
        End Try
    End Function

#End Region

End Class
