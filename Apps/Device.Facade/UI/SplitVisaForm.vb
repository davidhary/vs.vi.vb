'---------------------------------------------------------------------------------------------------
' file:		.\UI\SplitVisaForm.vb
'
' summary:	Split visa Windows Form
'---------------------------------------------------------------------------------------------------
Imports isr.VI.ExceptionExtensions

''' <summary> Form for viewing the split visa. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class SplitVisaForm
    Inherits isr.VI.Facade.SplitVisaViewForm

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        Me.New(CommandLineInfo.VirtualInstrument.GetValueOrDefault(VirtualInstrument.ScpiSession))
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="instrumentOption"> The instrument option. </param>
    Public Sub New(ByVal instrumentOption As VirtualInstrument)
        MyBase.New()
        If instrumentOption = VirtualInstrument.Switchboard Then instrumentOption = VirtualInstrument.ScpiSession
        Select Case instrumentOption

            Case VirtualInstrument.ScpiSession

                Me.VisaSessionBase = New VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.ScpiSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ScpiSessionResourceName}


            Case VirtualInstrument.TspSession

                Me.VisaSessionBase = New Tsp.VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.TspSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.TspSessionResourceName}

            Case VirtualInstrument.Tsp2Session

                Me.VisaSessionBase = New Tsp2.VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.Tsp2SessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.Tsp2SessionResourceName}

            Case Else

                Me.VisaSessionBase = New VisaSession With {
                    .CandidateResourceTitle = My.MySettings.Default.ScpiSessionResourceTitle,
                    .CandidateResourceName = My.MySettings.Default.ScpiSessionResourceName}

        End Select

        Dim view As VI.Facade.SplitVisaSessionView = VI.Facade.SplitVisaSessionView.Create
        Me.VisaSessionBase.AddListeners(Me.Talker)
        view.BindVisaSessionBase(Me.VisaSessionBase)
        Me.SplitVisaView = view

        Me.VisaViewDisposeEnabled = True
        If Me.VisaViewEnabled Then
            Me.SplitVisaView.ApplyTalkerTraceLevel(Core.ListenerType.Display, TraceEventType.Verbose)
            Me.SplitVisaView.ApplyTalkerTraceLevel(Core.ListenerType.Logger, TraceEventType.Verbose)
        End If
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.SplitVisaView IsNot Nothing Then Me.SplitVisaView.Dispose() : Me.SplitVisaView = Nothing
        ' the session needs to be disposed after the device releases the session.
        If Me.VisaSessionBase IsNot Nothing Then Me.VisaSessionBase.Dispose()
        Me.VisaSessionBase = Nothing
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        If Not Me.VisaViewEnabled Then Return
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim inst As VirtualInstrument = CommandLineInfo.VirtualInstrument.GetValueOrDefault(VirtualInstrument.ScpiSession)
            If inst = VirtualInstrument.Switchboard Then inst = VirtualInstrument.ScpiSession
            Select Case inst

                Case VirtualInstrument.ScpiSession
                    If Me.SplitVisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceName = Me.SplitVisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.SplitVisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceTitle = Me.SplitVisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.TspSession
                    If Me.SplitVisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.TspSessionResourceName = Me.SplitVisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.SplitVisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.TspSessionResourceTitle = Me.SplitVisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case VirtualInstrument.Tsp2Session
                    If Me.SplitVisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.Tsp2SessionResourceName = Me.SplitVisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.SplitVisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.Tsp2SessionResourceTitle = Me.SplitVisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
                Case Else
                    If Me.SplitVisaView.VisaSessionBase.ResourceNameConnected Then
                        isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceName = Me.SplitVisaView.VisaSessionBase.OpenResourceName
                        If Not String.IsNullOrWhiteSpace(Me.SplitVisaView.VisaSessionBase.OpenResourceTitle) Then
                            isr.VI.Device.Facade.My.MySettings.Default.ScpiSessionResourceTitle = Me.SplitVisaView.VisaSessionBase.OpenResourceTitle
                        End If
                    End If
            End Select
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Gets the visa view enabled. </summary>
    ''' <value> The visa view enabled. </value>
    Private ReadOnly Property VisaViewEnabled As Boolean
        Get
            Return Me.SplitVisaView IsNot Nothing
        End Get
    End Property

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        If Me.VisaViewEnabled Then
            Me.AddListener(My.Application.Logger)
        End If
        MyBase.OnLoad(e)
    End Sub

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
