'---------------------------------------------------------------------------------------------------
' file:		Pith\Session\SessionBase_Status.vb
'
' summary:	Session base status class
'---------------------------------------------------------------------------------------------------
Imports isr.Core

Partial Public Class SessionBase

    ''' <summary> Initializes the service request register bits. </summary>
    Private Sub InitializeServiceRequestRegisterBitsThis()
        Me._MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        Me._SystemEventBit = VI.Pith.ServiceRequests.SystemEvent
        Me._ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        Me._QuestionableEventBit = VI.Pith.ServiceRequests.QuestionableEvent
        Me._MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        Me._StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
        Me._RequestingServiceBit = VI.Pith.ServiceRequests.RequestingService
        Me._OperationEventBit = VI.Pith.ServiceRequests.OperationEvent
    End Sub

#Region " BIT MASKING "

    ''' <summary> Query if 'statusValue' is fully bit-masked. </summary>
    ''' <param name="statusValue"> The status value. </param>
    ''' <param name="bitmask">     The bitmask. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Shared Function IsFullyMasked(ByVal statusValue As Integer, ByVal bitmask As Integer) As Boolean
        Return (bitmask <> 0) AndAlso (statusValue And bitmask) = bitmask
    End Function

    ''' <summary> Checks if the service request status if fully masked. </summary>
    ''' <param name="bitmask"> The bitmask. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function IsFullyMasked(ByVal bitmask As Pith.ServiceRequests) As Boolean
        Return SessionBase.IsFullyMasked(Me.ServiceRequestStatus, bitmask)
    End Function

    ''' <summary> Query if 'bitmask' is partially masked. </summary>
    ''' <param name="statusValue"> The status value. </param>
    ''' <param name="bitmask">     The bitmask. </param>
    ''' <returns> <c>true</c> if partially masked; otherwise <c>false</c> </returns>
    Public Shared Function IsPartiallyMasked(ByVal statusValue As Integer, ByVal bitmask As Integer) As Boolean
        Return (bitmask <> 0) AndAlso (statusValue And bitmask) <> 0
    End Function

    ''' <summary> Query if 'bitmask' is partially masked. </summary>
    ''' <param name="bitmask"> The bitmask. </param>
    ''' <returns> <c>true</c> if partially masked; otherwise <c>false</c> </returns>
    Public Function IsPartiallyMasked(ByVal bitmask As Pith.ServiceRequests) As Boolean
        Return SessionBase.IsPartiallyMasked(Me.ServiceRequestStatus, bitmask)
    End Function

    ''' <summary> Count set bits. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The total number of set bits. </returns>
    Public Shared Function CountSetBits(ByVal value As Integer) As Integer
        Dim count As Integer = 0
        Do While value > 0
            count += value And 1
            value >>= 1
        Loop
        Return count
    End Function

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: MEASUREMENT "

    ''' <summary> The measurement event bit. </summary>
    Private _MeasurementEventBit As VI.Pith.ServiceRequests


    ''' <summary>
    ''' Gets or sets the bit that would be set when an enabled measurement event has occurred.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Measurement event bit value. </value>
    Public Property MeasurementEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me._MeasurementEventBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.MeasurementEventBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Measurement Event Status bit cannot be set to a value={value} having more than one bit")
                End If
                Me._MeasurementEventBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an enabled measurement event has occurred. </summary>
    ''' <value>
    ''' <c>True</c> if an enabled measurement event has occurred; otherwise, <c>False</c>.
    ''' </value>
    Public ReadOnly Property HasMeasurementEvent As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.MeasurementEventBit)
        End Get
    End Property

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: SYSTEM EVENT "

    ''' <summary> The system event bit. </summary>
    Private _SystemEventBit As VI.Pith.ServiceRequests


    ''' <summary>
    ''' Gets or sets the bit that would be set for detecting if a System Event has occurred.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The System Event bit value. </value>
    Public Property SystemEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me._SystemEventBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.SystemEventBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"System Event Status bit cannot be set to a value={value} having more than one bit")
                End If
                Me._SystemEventBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether a System Event has occurred. </summary>
    ''' <value> <c>True</c> if System Event bit is on; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasSystemEvent As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.SystemEventBit)
        End Get
    End Property

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: ERROR "

    ''' <summary> The error available bit. </summary>
    Private _ErrorAvailableBit As VI.Pith.ServiceRequests

    ''' <summary> Gets or sets the bit that would be set if an error event has occurred. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The error event bit. </value>
    Public Property ErrorAvailableBit() As VI.Pith.ServiceRequests
        Get
            Return Me._ErrorAvailableBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.ErrorAvailableBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Error Available Status Bit cannot be set to a value={value} having more than one bit")
                End If
                Me._ErrorAvailableBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether [Error available]. </summary>
    ''' <value> <c>True</c> if [Error available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property ErrorAvailable As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.ErrorAvailableBit)
        End Get
    End Property


    ''' <summary>
    ''' Reads the status registers and returns true if the error bit is set; Does not affect the
    ''' cached <see cref="ErrorAvailable"/> property.
    ''' </summary>
    ''' <returns> <c>true</c> if it error bit is on; otherwise <c>false</c> </returns>
    Public Function IsErrorBitSet() As Boolean
        ' read status byte without affecting the error available property.
        Return (Me.ErrorAvailableBit And Me.ReadStatusByte) <> 0
    End Function

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: QUESTIONABLE "

    ''' <summary> The questionable event bit. </summary>
    Private _QuestionableEventBit As VI.Pith.ServiceRequests


    ''' <summary>
    ''' Gets or sets the bit that would be set if a Questionable event has occurred.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Questionable event bit. </value>
    Public Property QuestionableEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me._QuestionableEventBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.QuestionableEventBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Questionable Event Status bit cannot be set to a value={value} having more than one bit")
                End If
                Me._QuestionableEventBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether a Questionable event has occurred. </summary>
    ''' <value> <c>True</c> if Questionable event has occurred; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasQuestionableEvent As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.QuestionableEventBit)
        End Get
    End Property

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: MESSAGE "

    ''' <summary> The message available bit. </summary>
    Private _MessageAvailableBit As VI.Pith.ServiceRequests

    ''' <summary> Gets or sets the bit that would be set if a message is available. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Message available bit. </value>
    Public Property MessageAvailableBit() As VI.Pith.ServiceRequests
        Get
            Return Me._MessageAvailableBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.MessageAvailableBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Message Available Status Bit cannot be set to a value={value} having more than one bit")
                End If
                Me._MessageAvailableBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether a message is available. </summary>
    ''' <value> <c>True</c> if a message is available; otherwise, <c>False</c>. </value>
    Public ReadOnly Property MessageAvailable As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.MessageAvailableBit)
        End Get
    End Property

    ''' <summary> Reads the status register a few times or until a message is available. </summary>
    ''' <remarks>
    ''' Delays looking for the message status by the status latency to make sure the instrument had
    ''' enough time to process the previous command.
    ''' </remarks>
    ''' <param name="latency">     The latency. </param>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <returns> <c>True</c> if message is available. </returns>
    Public Function QueryMessageAvailableStatus(ByVal latency As TimeSpan, ByVal repeatCount As Integer) As Boolean
        Do
            If latency > TimeSpan.Zero Then isr.Core.ApplianceBase.Delay(latency)
            Me.ReadStatusRegister()
            repeatCount -= 1
        Loop Until repeatCount <= 0 OrElse Me.MessageAvailable
        Return Me.MessageAvailable
    End Function

    ''' <summary> Reads the status byte and returns true if a message is available. </summary>
    ''' <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
    Public Function QueryMessageAvailableStatus() As Boolean
        Me.ReadStatusRegister()
        Return Me.MessageAvailable
    End Function

    ''' <summary> Reads the status register until timeout or a message is available. </summary>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="timeout">      Specifies the time to wait for message available. </param>
    ''' <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
    Public Function QueryMessageAvailableStatus(ByVal pollInterval As TimeSpan, ByVal timeout As TimeSpan) As Boolean
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim messageAvailable As Boolean = Me.QueryMessageAvailableStatus()
        Do Until messageAvailable OrElse sw.Elapsed > timeout
            Threading.Thread.SpinWait(10)
            isr.Core.ApplianceBase.DoEventsWait(pollInterval)
            messageAvailable = Me.QueryMessageAvailableStatus()
        Loop
        Return messageAvailable
    End Function

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: STANDARD EVENT "

    ''' <summary> The standard event bit. </summary>
    Private _StandardEventBit As VI.Pith.ServiceRequests


    ''' <summary>
    ''' Gets or sets bit that would be set if an enabled standard event has occurred.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Standard Event bit. </value>
    Public Property StandardEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me._StandardEventBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.StandardEventBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Standard Event Status bit cannot be set to a value={value} having more than one bit")
                End If
                Me._StandardEventBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an enabled standard event has occurred. </summary>
    ''' <value>
    ''' <c>True</c> if an enabled standard event has occurred; otherwise, <c>False</c>.
    ''' </value>
    Public ReadOnly Property HasStandardEvent As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.StandardEventBit)
        End Get
    End Property

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: SERVICE EVENT (SRQ) "

    ''' <summary> The requesting service bit. </summary>
    Private _RequestingServiceBit As VI.Pith.ServiceRequests = ServiceRequests.RequestingService


    ''' <summary>
    ''' Gets or sets bit that would be set if a requested service or master summary status event has
    ''' occurred.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The requested service or master summary status bit. </value>
    Public Property RequestingServiceBit() As VI.Pith.ServiceRequests
        Get
            Return Me._RequestingServiceBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.RequestingServiceBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Requesting Service Status Bit cannot be set to a value={value} having more than one bit")
                End If
                Me._RequestingServiceBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating if the device has requested service. </summary>
    ''' <value> <c>True</c> if the device has requested service; otherwise, <c>False</c>. </value>
    Public ReadOnly Property RequestingService As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.RequestingServiceBit)
        End Get
    End Property

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: OPERATION "

    ''' <summary> The operation event bit. </summary>
    Private _OperationEventBit As VI.Pith.ServiceRequests

    ''' <summary> Gets or sets the bit that would be set if an operation event has occurred. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Operation event bit. </value>
    Public Property OperationEventBit() As VI.Pith.ServiceRequests
        Get
            Return Me._OperationEventBit
        End Get
        Set(ByVal value As VI.Pith.ServiceRequests)
            If Not value.Equals(Me.OperationEventBit) Then
                If SessionBase.CountSetBits(value) > 1 Then
                    Throw New InvalidOperationException($"Operation Event Status bit cannot be set to a value={value} having more than one bit")
                End If
                Me._OperationEventBit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets a value indicating whether an operation event has occurred. </summary>
    ''' <value> <c>True</c> if an operation event has occurred; otherwise, <c>False</c>. </value>
    Public ReadOnly Property HasOperationEvent As Boolean
        Get
            Return 0 <> (Me.ServiceRequestStatus And Me.OperationEventBit)
        End Get
    End Property

#End Region

#Region " EVENTS: DEVICE ERROR OCCURRED "

    ''' <summary> Removes the <see cref="DeviceErrorOccurred"/> event handlers. </summary>
    Protected Sub RemoveDeviceErrorOccurredEventHandlers()
        Me._DeviceErrorOccurredEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The  <see cref="DeviceErrorOccurred"/>  event handlers. </summary>
    Private ReadOnly _DeviceErrorOccurredEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in <see cref="DeviceErrorOccurred"/> events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DeviceErrorOccurred As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._DeviceErrorOccurredEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._DeviceErrorOccurredEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._DeviceErrorOccurredEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event


    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DeviceErrorOccurred">Error Occurred Event</see>.
    ''' </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyDeviceErrorOccurred(ByVal e As System.EventArgs)
        Me._DeviceErrorOccurredEventHandlers.Send(Me, e)
    End Sub

#End Region

End Class

