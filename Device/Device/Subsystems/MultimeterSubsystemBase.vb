''' <summary> Defines a Multimeter Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-15 </para>
''' </remarks>
Public MustInherit Class MultimeterSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    ''' <param name="readingAmounts">  The reading amounts. </param>
    Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase, ByVal readingAmounts As ReadingAmounts)
        MyBase.New(statusSubsystem)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.DefaultFunctionRange = VI.Pith.Ranges.NonnegativeFullRange
        Me.DefaultFunctionModeDecimalPlaces = 3
        Me.ReadingAmounts = readingAmounts
        Me.ApertureRange = VI.Pith.Ranges.StandardApertureRange
        Me.FilterCountRange = VI.Pith.Ranges.StandardFilterCountRange
        Me.FilterWindowRange = VI.Pith.Ranges.StandardFilterWindowRange
        Me.PowerLineCyclesRange = VI.Pith.Ranges.StandardPowerLineCyclesRange
        Me.FunctionUnit = Me.DefaultFunctionUnit
        Me.FunctionRange = Me.DefaultFunctionRange
        Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
        Me.DefineFunctionModeReadWrites()
        Me.DefineOpenDetectorCapabilities()
        Me.DefineFunctionModeDecimalPlaces()
        Me.DefineFunctionModeRanges()
        Me.DefineFunctionModeUnits()
        Me.DefineAutoDelayModeReadWrites()
        Me.DefineMultimeterMeasurementUnitReadWrites()
        Me.DefineInputImpedanceModeReadWrites()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the clear execution state (CLS) by setting system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.DefineFunctionClearKnownState()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.ParsePrimaryReading(String.Empty)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ApertureRange = VI.Pith.Ranges.StandardApertureRange
        Me.FilterCountRange = VI.Pith.Ranges.StandardFilterCountRange
        Me.FilterWindowRange = VI.Pith.Ranges.StandardFilterWindowRange
        Me.PowerLineCyclesRange = VI.Pith.Ranges.StandardPowerLineCyclesRange
        Me.FunctionUnit = Me.DefaultFunctionUnit
        Me.FunctionRange = Me.DefaultFunctionRange
        Me.FunctionRangeDecimalPlaces = Me.DefaultFunctionModeDecimalPlaces
    End Sub

#End Region

#Region " APERTURE "

    ''' <summary> The aperture range. </summary>
    Private _ApertureRange As Core.Constructs.RangeR

    ''' <summary> The aperture range in seconds. </summary>
    ''' <value> The aperture range. </value>
    Public Property ApertureRange As Core.Constructs.RangeR
        Get
            Return Me._ApertureRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.ApertureRange <> value Then
                Me._ApertureRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Aperture. </summary>
    Private _Aperture As Double?

    ''' <summary>
    ''' Gets or sets the cached sense Aperture. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' The aperture sets the amount of time the ADC takes when making a measurement, which is the
    ''' integration period For the selected measurement Function. The integration period Is specified
    ''' In seconds. In general, a short integration period provides a fast reading rate, while a long
    ''' integration period provides better accuracy. The selected integration period Is a compromise
    ''' between speed And accuracy. During the integration period, If an external trigger With a
    ''' count Of 1 Is sent, the trigger Is ignored. If the count Is Set To more than 1, the first
    ''' reading Is initialized by this trigger. Subsequent readings occur as rapidly as the
    ''' instrument can make them. If a trigger occurs during the group measurement, the trigger Is
    ''' latched And another group Of measurements With the same count will be triggered after the
    ''' current group completes. You can also Set the integration rate by setting the number Of power
    ''' line cycles (NPLCs). Changing the NPLC value changes the aperture time And changing the
    ''' aperture time changes the NPLC value.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Aperture As Double?
        Get
            Return Me._Aperture
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Aperture, value) Then
                Me._Aperture = value
                Me.PowerLineCycles = If(value.HasValue,
                    StatusSubsystemBase.ToPowerLineCycles(TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * Me._Aperture.Value))),
                    New Double?)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense Aperture. </summary>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Aperture. </returns>
    Public Function ApplyAperture(ByVal value As Double) As Double?
        Me.WriteAperture(value)
        Return Me.QueryAperture
    End Function

    ''' <summary> Gets or sets The Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overridable Property ApertureQueryCommand As String

    ''' <summary> Queries The Aperture. </summary>
    ''' <returns> The Aperture or none if unknown. </returns>
    Public Function QueryAperture() As Double?
        Me.Aperture = Me.Query(Me.Aperture, Me.ApertureQueryCommand)
        Return Me.Aperture
    End Function

    ''' <summary> Gets or sets The Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overridable Property ApertureCommandFormat As String

    ''' <summary> Writes The Aperture without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Aperture. </remarks>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Aperture. </returns>
    Public Function WriteAperture(ByVal value As Double) As Double?
        Me.Aperture = Me.Write(value, Me.ApertureCommandFormat)
        Return Me.Aperture
    End Function

#End Region

#Region " AUTO DELAY ENABLED "

    ''' <summary> Auto Delay enabled. </summary>
    Private _AutoDelayEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Delay Enabled sentinel. </summary>
    ''' <remarks> When this is enabled, a delay is added before each measurement. </remarks>
    ''' <value>
    ''' <c>null</c> if Auto Delay Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoDelayEnabled As Boolean?
        Get
            Return Me._AutoDelayEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoDelayEnabled, value) Then
                Me._AutoDelayEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Delay Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoDelayEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoDelayEnabled(value)
        Return Me.QueryAutoDelayEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Delay enabled query command. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overridable Property AutoDelayEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Delay Enabled sentinel. Also sets the
    ''' <see cref="AutoDelayEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoDelayEnabled() As Boolean?
        Me.AutoDelayEnabled = Me.Query(Me.AutoDelayEnabled, Me.AutoDelayEnabledQueryCommand)
        Return Me.AutoDelayEnabled
    End Function

    ''' <summary> Gets or sets the automatic Delay enabled command Format. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overridable Property AutoDelayEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoDelayEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoDelayEnabled = Me.Write(value, Me.AutoDelayEnabledCommandFormat)
        Return Me.AutoDelayEnabled
    End Function

#End Region

#Region " AUTO DELAY MODE "

    ''' <summary> Define automatic delay mode read writes. </summary>
    Private Sub DefineAutoDelayModeReadWrites()
        Me._AutoDelayModeReadWrites = New Pith.EnumReadWriteCollection
        For Each enumValue As MultimeterAutoDelayModes In [Enum].GetValues(GetType(MultimeterAutoDelayModes))
            Me._AutoDelayModeReadWrites.Add(enumValue)
        Next
    End Sub

    ''' <summary> Gets or sets a dictionary of multimeter Auto Delay Mode parses. </summary>
    ''' <value> A Dictionary of multimeter Auto Delay Mode parses. </value>
    Public ReadOnly Property AutoDelayModeReadWrites As Pith.EnumReadWriteCollection

    ''' <summary> The supported automatic delay modes. </summary>
    Private _SupportedAutoDelayModes As MultimeterAutoDelayModes

    ''' <summary>
    ''' Gets or sets the supported Auto Delay Modes. This is a subset of the AutoDelays supported by
    ''' the instrument.
    ''' </summary>
    ''' <value> The supported multimeter Auto Delay Modes. </value>
    Public Property SupportedAutoDelayModes() As MultimeterAutoDelayModes
        Get
            Return Me._SupportedAutoDelayModes
        End Get
        Set(ByVal value As MultimeterAutoDelayModes)
            If Not Me.SupportedAutoDelayModes.Equals(value) Then
                Me._SupportedAutoDelayModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The automatic delay mode. </summary>
    Private _AutoDelayMode As MultimeterAutoDelayModes?

    ''' <summary> Gets or sets the cached multimeter Auto Delay Mode. </summary>
    ''' <value>
    ''' The <see cref="AutoDelayMode">multimeter Auto Delay Mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property AutoDelayMode As MultimeterAutoDelayModes?
        Get
            Return Me._AutoDelayMode
        End Get
        Protected Set(ByVal value As MultimeterAutoDelayModes?)
            If Not Me.AutoDelayMode.Equals(value) Then
                Me._AutoDelayMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the multimeter Auto Delay Mode. </summary>
    ''' <param name="value"> The  multimeter Auto Delay Mode. </param>
    ''' <returns>
    ''' The <see cref="AutoDelayMode">source multimeter Auto Delay Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function ApplyAutoDelayMode(ByVal value As MultimeterAutoDelayModes) As MultimeterAutoDelayModes?
        Me.WriteAutoDelayMode(value)
        Return Me.QueryAutoDelayMode()
    End Function

    ''' <summary> Gets or sets the multimeter Auto Delay Mode query command. </summary>
    ''' <value> The multimeter Auto Delay Mode query command. </value>
    Protected Overridable Property AutoDelayModeQueryCommand As String

    ''' <summary> Queries the multimeter Auto Delay Mode. </summary>
    ''' <returns>
    ''' The <see cref="AutoDelayMode">multimeter Auto Delay Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryAutoDelayMode() As MultimeterAutoDelayModes?
        Return Me.QueryAutoDelayMode(Me.AutoDelayModeQueryCommand)
    End Function

    ''' <summary> Queries print Auto Delay Mode. </summary>
    ''' <returns> The print Auto Delay Mode. </returns>
    Public Overridable Function QueryPrintAutoDelayMode() As MultimeterAutoDelayModes?
        Return Me.QueryAutoDelayMode(Me.Session.BuildQueryPrintCommand(Me.AutoDelayModeQueryCommand))
    End Function

    ''' <summary> Queries the multimeter Auto Delay Mode. </summary>
    ''' <param name="queryCommand"> The query command. </param>
    ''' <returns>
    ''' The <see cref="AutoDelayMode">multimeter Auto Delay Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function QueryAutoDelayMode(ByVal queryCommand As String) As MultimeterAutoDelayModes?
        Me.AutoDelayMode = Me.Query(Of MultimeterAutoDelayModes)(queryCommand,
                                                               Me.AutoDelayMode.GetValueOrDefault(MultimeterAutoDelayModes.Off),
                                                               Me.AutoDelayModeReadWrites)
        Return Me.AutoDelayMode
    End Function

    ''' <summary> Gets or sets the multimeter Auto Delay Mode command format. </summary>
    ''' <value> The multimeter Auto Delay Mode command format. </value>
    Protected Overridable Property AutoDelayModeCommandFormat As String

    ''' <summary>
    ''' Writes the multimeter Auto Delay Mode without reading back the value from the device.
    ''' </summary>
    ''' <param name="value"> The multimeter Auto Delay Mode. </param>
    ''' <returns>
    ''' The <see cref="AutoDelayMode">multimeter Auto Delay Mode</see> or none if unknown.
    ''' </returns>
    Public Overridable Function WriteAutoDelayMode(ByVal value As MultimeterAutoDelayModes) As MultimeterAutoDelayModes?
        Me.AutoDelayMode = Me.Write(Of MultimeterAutoDelayModes)(Me.AutoDelayModeCommandFormat, value, Me.AutoDelayModeReadWrites)
        Return Me.AutoDelayMode
    End Function

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Auto Range enabled. </summary>
    Private _AutoRangeEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
    ''' <remarks>
    ''' When this command is set to off, you must set the range. If you do not set the range, the
    ''' instrument remains at the range that was selected by auto range. When this command Is set to
    ''' on, the instrument automatically goes to the most sensitive range to perform the measurement.
    ''' If a range Is manually selected through the front panel Or a remote command, this command Is
    ''' automatically set to off. Auto range selects the best range In which To measure the signal
    ''' that Is applied To the input terminals of the instrument. When auto range Is enabled, the
    ''' range increases at 120 percent of range And decreases occurs When the reading Is less than 10
    ''' percent Of nominal range. For example, If you are On the 1 volt range And auto range Is
    ''' enabled, the instrument auto ranges up To the 10 volt range When the measurement exceeds 1.2
    ''' volts. It auto ranges down To the 100 mV range When the measurement falls below 1 volt.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoRangeEnabled As Boolean?
        Get
            Return Me._AutoRangeEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoRangeEnabled, value) Then
                Me._AutoRangeEnabled = value
                Me.NotifyPropertyChanged()
                If value.HasValue AndAlso value.Value Then
                    Me.Range = New Double?
                End If
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoRangeEnabled(value)
        Return Me.QueryAutoRangeEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <remarks> SCPI: ":SENSE:RANG:AUTO?". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overridable Property AutoRangeEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Range Enabled sentinel. Also sets the
    ''' <see cref="AutoRangeEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoRangeEnabled() As Boolean?
        Me.AutoRangeEnabled = Me.Query(Me.AutoRangeEnabled, Me.AutoRangeEnabledQueryCommand)
        Return Me.AutoRangeEnabled
    End Function

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <remarks> SCPI: ":SENSE:RANGE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overridable Property AutoRangeEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoRangeEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoRangeEnabled = Me.Write(value, Me.AutoRangeEnabledCommandFormat)
        Return Me.AutoRangeEnabled
    End Function

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Auto Zero enabled. </summary>
    Private _AutoZeroEnabled As Boolean?

    ''' <summary> Gets or sets the cached Auto Zero Enabled sentinel. </summary>
    ''' <remarks>
    ''' To ensure the accuracy of readings, the instrument must periodically get new measurements of
    ''' its internal ground And voltage reference. The time interval between updates To these
    ''' reference measurements Is determined by the integration aperture that Is being used for
    ''' measurements. The Model DMM7510 uses separate reference And zero measurements For Each
    ''' aperture. By Default, the instrument automatically checks these reference measurements
    ''' whenever a signal measurement Is made. The time To make the reference measurements Is In
    ''' addition To the normal measurement time. If timing Is critical, you can disable auto zero to
    ''' avoid this time penalty. When auto zero Is set to off, the instrument may gradually drift out
    ''' of specification. To minimize the drift, you can send the once command to make a reference
    ''' And zero measurement immediately before a test sequence. For AC voltage And AC current
    ''' measurements where the detector bandwidth Is set To 3 Hz Or 30 Hz, auto zero Is set on And
    ''' cannot be changed.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Auto Zero Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property AutoZeroEnabled As Boolean?
        Get
            Return Me._AutoZeroEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.AutoZeroEnabled, value) Then
                Me._AutoZeroEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Zero Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyAutoZeroEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteAutoZeroEnabled(value)
        Return Me.QueryAutoZeroEnabled()
    End Function

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <remarks> SCPI: ":SENSE:RANG:AUTO?". </remarks>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overridable Property AutoZeroEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Auto Zero Enabled sentinel. Also sets the
    ''' <see cref="AutoZeroEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryAutoZeroEnabled() As Boolean?
        Me.AutoZeroEnabled = Me.Query(Me.AutoZeroEnabled, Me.AutoZeroEnabledQueryCommand)
        Return Me.AutoZeroEnabled
    End Function

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <remarks> SCPI: ":SENSE:Zero:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overridable Property AutoZeroEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Auto Zero Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteAutoZeroEnabled(ByVal value As Boolean) As Boolean?
        Me.AutoZeroEnabled = Me.Write(value, Me.AutoZeroEnabledCommandFormat)
        Return Me.AutoZeroEnabled
    End Function

#End Region

#Region " AUTO ZERO ONCE "

    ''' <summary> Gets or sets the automatic zero once command. </summary>
    ''' <value> The automatic zero once command. </value>
    Protected Overridable Property AutoZeroOnceCommand As String

    ''' <summary> Request a single auto zero. </summary>
    Public Sub AutoZeroOnce()
        Me.Execute(Me.AutoZeroOnceCommand)
    End Sub

#End Region

#Region " BIAS "

#Region " BIAS ACTUAL "

    ''' <summary> The BiasActual. </summary>
    Private _BiasActual As Double?

    ''' <summary>
    ''' Gets or sets the cached Multimeter Bias Actual. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property BiasActual As Double?
        Get
            Return Me._BiasActual
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.BiasActual, value) Then
                Me._BiasActual = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets The Bias Actual query command. </summary>
    ''' <value> The BiasActual query command. </value>
    Protected Overridable Property BiasActualQueryCommand As String

    ''' <summary> Queries The Bias Actual. </summary>
    ''' <returns> The Bias Actual or none if unknown. </returns>
    Public Function QueryBiasActual() As Double?
        Me.BiasActual = Me.Query(Me.BiasActual, Me.BiasActualQueryCommand)
        Return Me.BiasActual
    End Function

#End Region

#Region " BIAS LEVEL "

    ''' <summary> The BiasLevel. </summary>
    Private _BiasLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Multimeter Bias Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' Selects the amount of current that is sourced by the instrument to make measurements. Applies
    ''' to diode measurements.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property BiasLevel As Double?
        Get
            Return Me._BiasLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.BiasLevel, value) Then
                Me._BiasLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Multimeter Bias Level. </summary>
    ''' <param name="value"> The Bias Level. </param>
    ''' <returns> The Bias Level. </returns>
    Public Function ApplyBiasLevel(ByVal value As Double) As Double?
        Me.WriteBiasLevel(value)
        Return Me.QueryBiasLevel
    End Function

    ''' <summary> Gets or sets The Bias Level query command. </summary>
    ''' <value> The BiasLevel query command. </value>
    Protected Overridable Property BiasLevelQueryCommand As String

    ''' <summary> Queries The Bias Level. </summary>
    ''' <returns> The Bias Level or none if unknown. </returns>
    Public Function QueryBiasLevel() As Double?
        Dim value As Double? = Me.Query(Me.BiasLevel, Me.BiasLevelQueryCommand)
        If value.HasValue Then Me.BiasLevel = value.Value Else Me.BiasLevel = New Double?
        Return Me.BiasLevel
    End Function

    ''' <summary> Gets or sets The Bias Level command format. </summary>
    ''' <value> The BiasLevel command format. </value>
    Protected Overridable Property BiasLevelCommandFormat As String

    ''' <summary> Writes The Bias Level without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Bias Level. </remarks>
    ''' <param name="value"> The Bias Level. </param>
    ''' <returns> The Bias Level. </returns>
    Public Function WriteBiasLevel(ByVal value As Double) As Double?
        Me.BiasLevel = Me.Write(value, Me.BiasLevelCommandFormat)
        Return Me.BiasLevel
    End Function

#End Region

#End Region

#Region " CONNECT/DISCONNECT "

    ''' <summary> Gets or sets the 'connect' command. </summary>
    ''' <value> The 'connect' command. </value>
    Protected Overridable Property ConnectCommand As String

    ''' <summary> Connects this object. </summary>
    Public Sub Connect()
        Me.Session.Execute(Me.ConnectCommand)
    End Sub

    ''' <summary> Gets or sets the 'disconnect' command. </summary>
    ''' <value> The 'disconnect' command. </value>
    Protected Overridable Property DisconnectCommand As String

    ''' <summary> Disconnects this object. </summary>
    Public Sub Disconnect()
        Me.Session.Execute(Me.DisconnectCommand)
    End Sub

#End Region

#Region " FILTER "

#Region " FILTER COUNT "

    ''' <summary> The filter count range. </summary>
    Private _FilterCountRange As Core.Constructs.RangeI

    ''' <summary> The Filter Count range in seconds. </summary>
    ''' <value> The filter count range. </value>
    Public Property FilterCountRange As Core.Constructs.RangeI
        Get
            Return Me._FilterCountRange
        End Get
        Set(value As Core.Constructs.RangeI)
            If Me.FilterCountRange <> value Then
                Me._FilterCountRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The FilterCount. </summary>
    Private _FilterCount As Integer?

    ''' <summary>
    ''' Gets or sets the cached sense Filter Count. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property FilterCount As Integer?
        Get
            Return Me._FilterCount
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.FilterCount, value) Then
                Me._FilterCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense Filter Count. </summary>
    ''' <param name="value"> The Filter Count. </param>
    ''' <returns> The Filter Count. </returns>
    Public Function ApplyFilterCount(ByVal value As Integer) As Integer?
        Me.WriteFilterCount(value)
        Return Me.QueryFilterCount
    End Function

    ''' <summary> Gets or sets The Filter Count query command. </summary>
    ''' <value> The FilterCount query command. </value>
    Protected Overridable Property FilterCountQueryCommand As String

    ''' <summary> Queries The Filter Count. </summary>
    ''' <returns> The Filter Count or none if unknown. </returns>
    Public Function QueryFilterCount() As Integer?
        Me.FilterCount = Me.Query(Me.FilterCount, Me.FilterCountQueryCommand)
        Return Me.FilterCount
    End Function

    ''' <summary> Gets or sets The Filter Count command format. </summary>
    ''' <value> The FilterCount command format. </value>
    Protected Overridable Property FilterCountCommandFormat As String

    ''' <summary> Writes The Filter Count without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Filter Count. </remarks>
    ''' <param name="value"> The Filter Count. </param>
    ''' <returns> The Filter Count. </returns>
    Public Function WriteFilterCount(ByVal value As Integer) As Integer?
        Me.FilterCount = Me.Write(value, Me.FilterCountCommandFormat)
        Return Me.FilterCount
    End Function

#End Region

#Region " FILTER ENABLED "

    ''' <summary> Filter enabled. </summary>
    Private _FilterEnabled As Boolean?

    ''' <summary> Gets or sets the cached Filter Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Filter Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property FilterEnabled As Boolean?
        Get
            Return Me._FilterEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.FilterEnabled, value) Then
                Me._FilterEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Filter Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyFilterEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteFilterEnabled(value)
        Return Me.QueryFilterEnabled()
    End Function

    ''' <summary> Gets or sets the Filter enabled query command. </summary>
    ''' <remarks> TSP: _G.print(dmm.filter.enable==1) </remarks>
    ''' <value> The Filter enabled query command. </value>
    Protected Overridable Property FilterEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Filter Enabled sentinel. Also sets the
    ''' <see cref="FilterEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryFilterEnabled() As Boolean?
        Me.FilterEnabled = Me.Query(Me.FilterEnabled, Me.FilterEnabledQueryCommand)
        Return Me.FilterEnabled
    End Function

    ''' <summary> Gets or sets the Filter enabled command Format. </summary>
    ''' <remarks> TSP "dmm.filter.enable={0:'1';'1';'0'}". </remarks>
    ''' <value> The Filter enabled query command. </value>
    Protected Overridable Property FilterEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Filter Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteFilterEnabled(ByVal value As Boolean) As Boolean?
        Me.FilterEnabled = Me.Write(value, Me.FilterEnabledCommandFormat)
        Return Me.FilterEnabled
    End Function

#End Region

#Region " MOVING AVERAGE FILTER ENABLED "

    ''' <summary> Moving Average Filter enabled. </summary>
    Private _MovingAverageFilterEnabled As Boolean?

    ''' <summary> Gets or sets the cached Moving Average Filter Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Moving Average Filter Enabled is not known; <c>True</c> if output is on;
    ''' otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property MovingAverageFilterEnabled As Boolean?
        Get
            Return Me._MovingAverageFilterEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.MovingAverageFilterEnabled, value) Then
                Me._MovingAverageFilterEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Moving Average Filter Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyMovingAverageFilterEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteMovingAverageFilterEnabled(value)
        Return Me.QueryMovingAverageFilterEnabled()
    End Function

    ''' <summary> Gets or sets the Moving Average Filter enabled query command. </summary>
    ''' <remarks> TSP: _G.print(dmm.filter.type=0) </remarks>
    ''' <value> The Moving Average Filter enabled query command. </value>
    Protected Overridable Property MovingAverageFilterEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Moving Average Filter Enabled sentinel. Also sets the
    ''' <see cref="MovingAverageFilterEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryMovingAverageFilterEnabled() As Boolean?
        Me.MovingAverageFilterEnabled = Me.Query(Me.MovingAverageFilterEnabled, Me.MovingAverageFilterEnabledQueryCommand)
        Return Me.MovingAverageFilterEnabled
    End Function

    ''' <summary> Gets or sets the Moving Average Filter enabled command Format. </summary>
    ''' <remarks> TSP: "dmm.filter.type={0:'0';'0';'1'}". </remarks>
    ''' <value> The Moving Average Filter enabled query command. </value>
    Protected Overridable Property MovingAverageFilterEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Moving Average Filter Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteMovingAverageFilterEnabled(ByVal value As Boolean) As Boolean?
        Me.MovingAverageFilterEnabled = Me.Write(value, Me.MovingAverageFilterEnabledCommandFormat)
        Return Me.MovingAverageFilterEnabled
    End Function

#End Region

#Region " FILTER WINDOW "

    ''' <summary> The filter window range. </summary>
    Private _FilterWindowRange As Core.Constructs.RangeR

    ''' <summary> The Filter Window range. </summary>
    ''' <value> The filter window range. </value>
    Public Property FilterWindowRange As Core.Constructs.RangeR
        Get
            Return Me._FilterWindowRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.FilterWindowRange <> value Then
                Me._FilterWindowRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The FilterWindow. </summary>
    Private _FilterWindow As Double?

    ''' <summary>
    ''' Gets or sets the cached sense Filter Window. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property FilterWindow As Double?
        Get
            Return Me._FilterWindow
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.FilterWindow, value) Then
                Me._FilterWindow = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense Filter Window. </summary>
    ''' <param name="value"> The Filter Window. </param>
    ''' <returns> The Filter Window. </returns>
    Public Function ApplyFilterWindow(ByVal value As Double) As Double?
        Me.WriteFilterWindow(value)
        Return Me.QueryFilterWindow
    End Function

    ''' <summary> Gets or sets The Filter Window query command. </summary>
    ''' <value> The FilterWindow query command. </value>
    Protected Overridable Property FilterWindowQueryCommand As String

    ''' <summary> The filter window scale. </summary>
    Private _FilterWindowScale As Double = 100

    ''' <summary> Gets or sets the filter window scale. </summary>
    ''' <value> The filter window scale. </value>
    Public Property FilterWindowScale As Double
        Get
            Return Me._FilterWindowScale
        End Get
        Set(value As Double)
            If Me.FilterWindowScale <> value Then
                Me._FilterWindowScale = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries The Filter Window. </summary>
    ''' <returns> The Filter Window or none if unknown. </returns>
    Public Function QueryFilterWindow() As Double?
        Dim value As Double? = Me.Query(Me.FilterWindow, Me.FilterWindowQueryCommand)
        If value.HasValue Then Me.FilterWindow = value.Value / Me.FilterWindowScale Else Me.FilterWindow = New Double?
        Return Me.FilterWindow
    End Function

    ''' <summary> Gets The Filter Window command format. </summary>
    ''' <value> The FilterWindow command format. </value>
    Protected Overridable Property FilterWindowCommandFormat As String

    ''' <summary> Writes The Filter Window without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Filter Window. </remarks>
    ''' <param name="value"> The Filter Window. </param>
    ''' <returns> The Filter Window. </returns>
    Public Function WriteFilterWindow(ByVal value As Double) As Double?
        Me.FilterWindow = Me.Write(Me.FilterWindowScale * value, Me.FilterWindowCommandFormat)
        Return Me.FilterWindow
    End Function

#End Region

#End Region

#Region " FRONT TERMINALS SELECTED "

    ''' <summary> Gets the front terminal label. </summary>
    ''' <value> The front terminal label. </value>
    Public Property FrontTerminalLabel As String = "F"

    ''' <summary> Gets the rear terminal label. </summary>
    ''' <value> The rear terminal label. </value>
    Public Property RearTerminalLabel As String = "R"

    ''' <summary> Gets the unknown terminal label. </summary>
    ''' <value> The unknown terminal label. </value>
    Public Property UnknownTerminalLabel As String = String.Empty

    ''' <summary> Gets the terminals caption. </summary>
    ''' <value> The terminals caption. </value>
    Public ReadOnly Property TerminalsCaption As String
        Get
            Return If(Me.FrontTerminalsSelected.HasValue, If(Me.FrontTerminalsSelected.Value, Me.FrontTerminalLabel, Me.RearTerminalLabel), Me.UnknownTerminalLabel)
        End Get
    End Property

    ''' <summary> Gets true if the subsystem supports front terminals selection query. </summary>
    ''' <value>
    ''' The value indicating if the subsystem supports front terminals selection query.
    ''' </value>
    Public ReadOnly Property SupportsFrontTerminalsSelectionQuery As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.FrontTerminalsSelectedQueryCommand)
        End Get
    End Property

    ''' <summary> Front Terminals Selected. </summary>
    Private _FrontTerminalsSelected As Boolean?

    ''' <summary> Gets or sets the cached Front Terminals Selected sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Front Terminals Selected is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property FrontTerminalsSelected As Boolean?
        Get
            Return Me._FrontTerminalsSelected
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.FrontTerminalsSelected, value) Then
                Me._FrontTerminalsSelected = value
                Me.NotifyPropertyChanged()
                Me.NotifyPropertyChanged(NameOf(MultimeterSubsystemBase.TerminalsCaption))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Front Terminals Selected sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyFrontTerminalsSelected(ByVal value As Boolean) As Boolean?
        Me.WriteFrontTerminalsSelected(value)
        Return Me.QueryFrontTerminalsSelected()
    End Function

    ''' <summary> Gets or sets the front terminals selected query command. </summary>
    ''' <value> The front terminals selected query command. </value>
    Protected Overridable Property FrontTerminalsSelectedQueryCommand As String

    ''' <summary>
    ''' Queries the Front Terminals Selected sentinel. Also sets the
    ''' <see cref="FrontTerminalsSelected">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryFrontTerminalsSelected() As Boolean?
        Me.FrontTerminalsSelected = Me.Query(Me.FrontTerminalsSelected, Me.FrontTerminalsSelectedQueryCommand)
        Return Me.FrontTerminalsSelected
    End Function

    ''' <summary> Gets or sets the front terminals selected command format. </summary>
    ''' <value> The front terminals selected command format. </value>
    Protected Overridable Property FrontTerminalsSelectedCommandFormat As String

    ''' <summary>
    ''' Writes the Front Terminals Selected sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteFrontTerminalsSelected(ByVal value As Boolean) As Boolean?
        Me.FrontTerminalsSelected = Me.Write(value, Me.FrontTerminalsSelectedCommandFormat)
        Return Me.FrontTerminalsSelected
    End Function

#End Region

#Region " LIMIT 1 "

#Region " LIMIT1 AUTO CLEAR "

    ''' <summary> Limit1 Auto Clear. </summary>
    Private _Limit1AutoClear As Boolean?

    ''' <summary> Gets or sets the cached Limit1 Auto Clear sentinel. </summary>
    ''' <remarks>
    ''' When auto clear is set to on for a measure function, limit conditions are cleared
    ''' automatically after each measurement. If you are making a series of measurements, the
    ''' instrument shows the limit test result of the last measurement for the pass Or fail
    ''' indication for the limit. If you want To know If any Of a series Of measurements failed the
    ''' limit, Set the auto clear setting To off. When this set to off, a failed indication Is Not
    ''' cleared automatically. It remains set until it Is cleared With the clear command. The auto
    ''' clear setting affects both the high And low limits.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Limit1 Auto Clear is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit1AutoClear As Boolean?
        Get
            Return Me._Limit1AutoClear
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit1AutoClear, value) Then
                Me._Limit1AutoClear = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Auto Clear sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit1AutoClear(ByVal value As Boolean) As Boolean?
        Me.WriteLimit1AutoClear(value)
        Return Me.QueryLimit1AutoClear()
    End Function

    ''' <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
    ''' <remarks> TSP: _G.print(_G.dmm.measure.limit1.autoclear==dmm.ON) </remarks>
    ''' <value> The Limit1 Auto Clear query command. </value>
    Protected Overridable Property Limit1AutoClearQueryCommand As String

    ''' <summary>
    ''' Queries the Limit1 Auto Clear sentinel. Also sets the
    ''' <see cref="Limit1AutoClear">AutoClear</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function QueryLimit1AutoClear() As Boolean?
        Me.Limit1AutoClear = Me.Query(Me.Limit1AutoClear, Me.Limit1AutoClearQueryCommand)
        Return Me.Limit1AutoClear
    End Function

    ''' <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
    ''' <remarks> TSP: _G.dmm.measure.limit1.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}". </remarks>
    ''' <value> The Limit1 Auto Clear query command. </value>
    Protected Overridable Property Limit1AutoClearCommandFormat As String

    ''' <summary>
    ''' Writes the Limit1 Auto Clear sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is Auto Clear. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function WriteLimit1AutoClear(ByVal value As Boolean) As Boolean?
        Me.Limit1AutoClear = Me.Write(value, Me.Limit1AutoClearCommandFormat)
        Return Me.Limit1AutoClear
    End Function

#End Region

#Region " LIMIT1 ENABLED "

    ''' <summary> Limit1 enabled. </summary>
    Private _Limit1Enabled As Boolean?

    ''' <summary> Gets or sets the cached Limit1 Enabled sentinel. </summary>
    ''' <remarks>
    ''' This command enables or disables a limit test for the selected measurement function. When
    ''' this attribute Is enabled, the limit 1 testing occurs on each measurement made by the
    ''' instrument. Limit 1 testing compares the measurements To the high And low limit values. If a
    ''' measurement falls outside these limits, the test fails.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Limit1 Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit1Enabled As Boolean?
        Get
            Return Me._Limit1Enabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit1Enabled, value) Then
                Me._Limit1Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit1Enabled(ByVal value As Boolean) As Boolean?
        Me.WriteLimit1Enabled(value)
        Return Me.QueryLimit1Enabled()
    End Function

    ''' <summary> Gets or sets the Limit1 enabled query command. </summary>
    ''' <remarks> TSP: _G.print(_G.dmm.measure.limit1.autoclear==dmm.ON) </remarks>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overridable Property Limit1EnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Limit1 Enabled sentinel. Also sets the
    ''' <see cref="Limit1Enabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryLimit1Enabled() As Boolean?
        Me.Limit1Enabled = Me.Query(Me.Limit1Enabled, Me.Limit1EnabledQueryCommand)
        Return Me.Limit1Enabled
    End Function

    ''' <summary> Gets or sets the Limit1 enabled command Format. </summary>
    ''' <remarks> TSP _G.dmm.measure.limit1.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'} </remarks>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overridable Property Limit1EnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Limit1 Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteLimit1Enabled(ByVal value As Boolean) As Boolean?
        Me.Limit1Enabled = Me.Write(value, Me.Limit1EnabledCommandFormat)
        Return Me.Limit1Enabled
    End Function

#End Region

#Region " LIMIT1 LOWER LEVEL "

    ''' <summary> The Limit1 Lower Level. </summary>
    Private _Limit1LowerLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit1 Lower Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the lower limit for the limit 1 test for the selected measure function.
    ''' When limit 1 testing Is enabled, this causes a fail indication to occur when the measurement
    ''' value Is less than this value.  Default Is 0.3 For limit 1 When the diode Function Is
    ''' selected. The Default For limit 2 For the diode Function is() –1.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit1LowerLevel As Double?
        Get
            Return Me._Limit1LowerLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit1LowerLevel, value) Then
                Me._Limit1LowerLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Lower Level. </summary>
    ''' <param name="value"> The Limit1 Lower Level. </param>
    ''' <returns> The Limit1 Lower Level. </returns>
    Public Function ApplyLimit1LowerLevel(ByVal value As Double) As Double?
        Me.WriteLimit1LowerLevel(value)
        Return Me.QueryLimit1LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Lower Level query command. </summary>
    ''' <value> The Limit1 Lower Level query command. </value>
    Protected Overridable Property Limit1LowerLevelQueryCommand As String

    ''' <summary> Queries The Limit1 Lower Level. </summary>
    ''' <returns> The Limit1 Lower Level or none if unknown. </returns>
    Public Function QueryLimit1LowerLevel() As Double?
        Me.Limit1LowerLevel = Me.Query(Me.Limit1LowerLevel, Me.Limit1LowerLevelQueryCommand)
        Return Me.Limit1LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Lower Level command format. </summary>
    ''' <value> The Limit1 Lower Level command format. </value>
    Protected Overridable Property Limit1LowerLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit1 Lower Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit1 Lower Level. </remarks>
    ''' <param name="value"> The Limit1 Lower Level. </param>
    ''' <returns> The Limit1 Lower Level. </returns>
    Public Function WriteLimit1LowerLevel(ByVal value As Double) As Double?
        Me.Limit1LowerLevel = Me.Write(value, Me.Limit1LowerLevelCommandFormat)
        Return Me.Limit1LowerLevel
    End Function

#End Region

#Region " LIMIT1 UPPER LEVEL "

    ''' <summary> The Limit1 Upper Level. </summary>
    Private _Limit1UpperLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit1 Upper Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the high limit for the limit 2 test for the selected measurement function.
    ''' When limit 2 testing Is enabled, the instrument generates a fail indication When the
    ''' measurement value Is more than this value. Default Is 0.8 For limit 1 When the diode Function
    ''' Is selected; 10 When the continuity Function Is selected. The default for limit 2 for the
    ''' diode And continuity functions Is 1.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit1UpperLevel As Double?
        Get
            Return Me._Limit1UpperLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit1UpperLevel, value) Then
                Me._Limit1UpperLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit1 Upper Level. </summary>
    ''' <param name="value"> The Limit1 Upper Level. </param>
    ''' <returns> The Limit1 Upper Level. </returns>
    Public Function ApplyLimit1UpperLevel(ByVal value As Double) As Double?
        Me.WriteLimit1UpperLevel(value)
        Return Me.QueryLimit1UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Upper Level query command. </summary>
    ''' <value> The Limit1 Upper Level query command. </value>
    Protected Overridable Property Limit1UpperLevelQueryCommand As String

    ''' <summary> Queries The Limit1 Upper Level. </summary>
    ''' <returns> The Limit1 Upper Level or none if unknown. </returns>
    Public Function QueryLimit1UpperLevel() As Double?
        Me.Limit1UpperLevel = Me.Query(Me.Limit1UpperLevel, Me.Limit1UpperLevelQueryCommand)
        Return Me.Limit1UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit1 Upper Level command format. </summary>
    ''' <value> The Limit1 Upper Level command format. </value>
    Protected Overridable Property Limit1UpperLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit1 Upper Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit1 Upper Level. </remarks>
    ''' <param name="value"> The Limit1 Upper Level. </param>
    ''' <returns> The Limit1 Upper Level. </returns>
    Public Function WriteLimit1UpperLevel(ByVal value As Double) As Double?
        Me.Limit1UpperLevel = Me.Write(value, Me.Limit1UpperLevelCommandFormat)
        Return Me.Limit1UpperLevel
    End Function

#End Region

#End Region

#Region " LIMIT 2 "

#Region " LIMIT2 AUTO CLEAR "

    ''' <summary> Limit2 Auto Clear. </summary>
    Private _Limit2AutoClear As Boolean?

    ''' <summary> Gets or sets the cached Limit2 Auto Clear sentinel. </summary>
    ''' <remarks>
    ''' When auto clear is set to on for a measure function, limit conditions are cleared
    ''' automatically after each measurement. If you are making a series of measurements, the
    ''' instrument shows the limit test result of the last measurement for the pass Or fail
    ''' indication for the limit. If you want To know If any Of a series Of measurements failed the
    ''' limit, Set the auto clear setting To off. When this set to off, a failed indication Is Not
    ''' cleared automatically. It remains set until it Is cleared With the clear command. The auto
    ''' clear setting affects both the high And low limits.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Limit2 Auto Clear is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit2AutoClear As Boolean?
        Get
            Return Me._Limit2AutoClear
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit2AutoClear, value) Then
                Me._Limit2AutoClear = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Auto Clear sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit2AutoClear(ByVal value As Boolean) As Boolean?
        Me.WriteLimit2AutoClear(value)
        Return Me.QueryLimit2AutoClear()
    End Function

    ''' <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
    ''' <remarks> TSP: _G.print(_G.dmm.measure.limit1.autoclear==dmm.ON) </remarks>
    ''' <value> The Limit2 Auto Clear query command. </value>
    Protected Overridable Property Limit2AutoClearQueryCommand As String

    ''' <summary>
    ''' Queries the Limit2 Auto Clear sentinel. Also sets the
    ''' <see cref="Limit2AutoClear">AutoClear</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function QueryLimit2AutoClear() As Boolean?
        Me.Limit2AutoClear = Me.Query(Me.Limit2AutoClear, Me.Limit2AutoClearQueryCommand)
        Return Me.Limit2AutoClear
    End Function

    ''' <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
    ''' <remarks> TSP: "_G.dmm.measure.limit1.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}". </remarks>
    ''' <value> The Limit2 Auto Clear query command. </value>
    Protected Overridable Property Limit2AutoClearCommandFormat As String

    ''' <summary>
    ''' Writes the Limit2 Auto Clear sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is Auto Clear. </param>
    ''' <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
    Public Function WriteLimit2AutoClear(ByVal value As Boolean) As Boolean?
        Me.Limit2AutoClear = Me.Write(value, Me.Limit2AutoClearCommandFormat)
        Return Me.Limit2AutoClear
    End Function

#End Region

#Region " LIMIT2 ENABLED "

    ''' <summary> Limit2 enabled. </summary>
    Private _Limit2Enabled As Boolean?

    ''' <summary> Gets or sets the cached Limit2 Enabled sentinel. </summary>
    ''' <remarks>
    ''' This command enables or disables a limit test for the selected measurement function. When
    ''' this attribute Is enabled, the limit 2 testing occurs on each measurement made by the
    ''' instrument. Limit 2 testing compares the measurements To the high And low limit values. If a
    ''' measurement falls outside these limits, the test fails.
    ''' </remarks>
    ''' <value>
    ''' <c>null</c> if Limit2 Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property Limit2Enabled As Boolean?
        Get
            Return Me._Limit2Enabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.Limit2Enabled, value) Then
                Me._Limit2Enabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyLimit2Enabled(ByVal value As Boolean) As Boolean?
        Me.WriteLimit2Enabled(value)
        Return Me.QueryLimit2Enabled()
    End Function

    ''' <summary> Gets or sets the Limit2 enabled query command. </summary>
    ''' <remarks> TSP: _G.print(_G.dmm.measure.limit2.autoclear==dmm.ON) </remarks>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overridable Property Limit2EnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Limit2 Enabled sentinel. Also sets the
    ''' <see cref="Limit2Enabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryLimit2Enabled() As Boolean?
        Me.Limit2Enabled = Me.Query(Me.Limit2Enabled, Me.Limit2EnabledQueryCommand)
        Return Me.Limit2Enabled
    End Function

    ''' <summary> Gets or sets the Limit2 enabled command Format. </summary>
    ''' <remarks> TSP: _G.dmm.measure.limit2.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'} </remarks>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overridable Property Limit2EnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Limit2 Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteLimit2Enabled(ByVal value As Boolean) As Boolean?
        Me.Limit2Enabled = Me.Write(value, Me.Limit2EnabledCommandFormat)
        Return Me.Limit2Enabled
    End Function

#End Region

#Region " LIMIT2 LOWER LEVEL "

    ''' <summary> The Limit2 Lower Level. </summary>
    Private _Limit2LowerLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit2 Lower Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the lower limit for the limit 1 test for the selected measure function.
    ''' When limit 1 testing Is enabled, this causes a fail indication to occur when the measurement
    ''' value Is less than this value.  Default Is 0.3 For limit 1 When the diode Function Is
    ''' selected. The Default For limit 2 For the diode Function is() –1.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit2LowerLevel As Double?
        Get
            Return Me._Limit2LowerLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit2LowerLevel, value) Then
                Me._Limit2LowerLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Lower Level. </summary>
    ''' <param name="value"> The Limit2 Lower Level. </param>
    ''' <returns> The Limit2 Lower Level. </returns>
    Public Function ApplyLimit2LowerLevel(ByVal value As Double) As Double?
        Me.WriteLimit2LowerLevel(value)
        Return Me.QueryLimit2LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Lower Level query command. </summary>
    ''' <value> The Limit2 Lower Level query command. </value>
    Protected Overridable Property Limit2LowerLevelQueryCommand As String

    ''' <summary> Queries The Limit2 Lower Level. </summary>
    ''' <returns> The Limit2 Lower Level or none if unknown. </returns>
    Public Function QueryLimit2LowerLevel() As Double?
        Me.Limit2LowerLevel = Me.Query(Me.Limit2LowerLevel, Me.Limit2LowerLevelQueryCommand)
        Return Me.Limit2LowerLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Lower Level command format. </summary>
    ''' <value> The Limit2 Lower Level command format. </value>
    Protected Overridable Property Limit2LowerLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit2 Lower Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit2 Lower Level. </remarks>
    ''' <param name="value"> The Limit2 Lower Level. </param>
    ''' <returns> The Limit2 Lower Level. </returns>
    Public Function WriteLimit2LowerLevel(ByVal value As Double) As Double?
        Me.Limit2LowerLevel = Me.Write(value, Me.Limit2LowerLevelCommandFormat)
        Return Me.Limit2LowerLevel
    End Function

#End Region

#Region " LIMIT2 UPPER LEVEL "

    ''' <summary> The Limit2 Upper Level. </summary>
    Private _Limit2UpperLevel As Double?

    ''' <summary>
    ''' Gets or sets the cached Limit2 Upper Level. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the high limit for the limit 2 test for the selected measurement function.
    ''' When limit
    ''' 2 testing Is enabled, the instrument generates a fail indication When the measurement value
    ''' Is more than this value. Default Is 0.8 For limit 1 When the diode Function Is selected; 10
    ''' When the continuity Function Is selected. The default for limit 2 for the diode And
    ''' continuity functions Is 1.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Limit2UpperLevel As Double?
        Get
            Return Me._Limit2UpperLevel
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Limit2UpperLevel, value) Then
                Me._Limit2UpperLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Limit2 Upper Level. </summary>
    ''' <param name="value"> The Limit2 Upper Level. </param>
    ''' <returns> The Limit2 Upper Level. </returns>
    Public Function ApplyLimit2UpperLevel(ByVal value As Double) As Double?
        Me.WriteLimit2UpperLevel(value)
        Return Me.QueryLimit2UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Upper Level query command. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Limit2 Upper Level query command. </value>
    Protected Overridable Property Limit2UpperLevelQueryCommand As String

    ''' <summary> Queries The Limit2 Upper Level. </summary>
    ''' <returns> The Limit2 Upper Level or none if unknown. </returns>
    Public Function QueryLimit2UpperLevel() As Double?
        Me.Limit2UpperLevel = Me.Query(Me.Limit2UpperLevel, Me.Limit2UpperLevelQueryCommand)
        Return Me.Limit2UpperLevel
    End Function

    ''' <summary> Gets or sets The Limit2 Upper Level command format. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The Limit2 Upper Level command format. </value>
    Protected Overridable Property Limit2UpperLevelCommandFormat As String

    ''' <summary>
    ''' Writes The Limit2 Upper Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Limit2 Upper Level. </remarks>
    ''' <param name="value"> The Limit2 Upper Level. </param>
    ''' <returns> The Limit2 Upper Level. </returns>
    Public Function WriteLimit2UpperLevel(ByVal value As Double) As Double?
        Me.Limit2UpperLevel = Me.Write(value, Me.Limit2UpperLevelCommandFormat)
        Return Me.Limit2UpperLevel
    End Function

#End Region

#End Region

#Region " MEASURE "

    ''' <summary> Estimates the lower bound on measurement time. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> A TimeSpan. </returns>
    Public Overridable Function EstimateMeasurementTime() As TimeSpan
        If Not Me.PowerLineCycles.HasValue Then Throw New InvalidOperationException($"{NameOf(MultimeterSubsystemBase.PowerLineCycles)} value not set")
        If Not Me.FilterEnabled.HasValue Then Throw New InvalidOperationException($"{NameOf(MultimeterSubsystemBase.FilterEnabled)} value not set")
        Dim aperture As Double = Me.PowerLineCycles.Value / Me.StatusSubsystem.LineFrequency.GetValueOrDefault(60)
        Dim timeSeconds As Double = 0
        If Me.FilterEnabled.Value Then
            If Not Me.FilterCount.HasValue Then Throw New InvalidOperationException($"{NameOf(MultimeterSubsystemBase.FilterCount)} value not set")
            If Me.FilterCount.Value > 0 Then
                ' if auto zero once is included the time maybe too long
                timeSeconds = aperture * Me.FilterCount.Value
            Else
                ' assumes auto zero
                timeSeconds = aperture * 2
            End If
        End If
        If Me.FunctionMode = MultimeterFunctionModes.ResistanceFourWire OrElse
            Me.FunctionMode = MultimeterFunctionModes.ResistanceTwoWire OrElse
            Me.FunctionMode = MultimeterFunctionModes.ResistanceCommonSide Then
            ' assumes that resistance measurements take twice the duration.
            timeSeconds += timeSeconds
        End If
        Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * timeSeconds))
    End Function

    ''' <summary> Gets or sets The Measure query command. </summary>
    ''' <value> The Measure query command. </value>
    Protected Overridable Property MeasureQueryCommand As String

    ''' <summary> Reads a value in to the primary reading and converts it to Double. </summary>
    ''' <returns> The measured value or none if unknown. </returns>
    Public Overridable Function MeasurePrimaryReading() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return Me.MeasurePrimaryReading(Me.MeasureQueryCommand)
    End Function

    ''' <summary> Queries the reading and parse the reading amounts. </summary>
    ''' <returns> The reading or none if unknown. </returns>
    Public Overridable Function MeasureReadingAmounts() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return Me.MeasureReadingAmounts(Me.MeasureQueryCommand)
    End Function

#End Region

#Region " OPEN DETECTOR ENABLED "

    ''' <summary> Define open detector capabilities. </summary>
    Private Sub DefineOpenDetectorCapabilities()
        Me._OpenDetectorCapabilities = New BooleanDictionary
        For Each functionMode As VI.MultimeterFunctionModes In [Enum].GetValues(GetType(VI.MultimeterFunctionModes))
            Me._OpenDetectorCapabilities.Add(functionMode, False)
        Next
    End Sub

    ''' <summary>
    ''' Gets or sets a list of open detector capability for each multimeter function mode.
    ''' </summary>
    ''' <value> The open detector capability for each multimeter function mode. </value>
    Public ReadOnly Property OpenDetectorCapabilities As BooleanDictionary

    ''' <summary> Gets or sets the default open detector capable. </summary>
    ''' <value> The default open detector capable. </value>
    Public Property DefaultOpenDetectorCapable As Boolean = False

    ''' <summary> Query if 'functionMode' is open detector capable. </summary>
    ''' <param name="functionMode"> The function mode. </param>
    ''' <returns> <c>true</c> if open detector capable; otherwise <c>false</c> </returns>
    Public Overridable Function IsOpenDetectorCapable(ByVal functionMode As Integer) As Boolean
        Return Me.OpenDetectorCapabilities(functionMode)
    End Function

    ''' <summary> True if function open detector capable. </summary>
    Private _FunctionOpenDetectorCapable As Boolean

    ''' <summary> Gets or sets the function open detector capable. </summary>
    ''' <value> The function open detector capable. </value>
    Public Property FunctionOpenDetectorCapable As Boolean
        Get
            Return Me._FunctionOpenDetectorCapable
        End Get
        Protected Set(value As Boolean)
            If Me.FunctionOpenDetectorCapable <> value Then
                Me._FunctionOpenDetectorCapable = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Open Detector enabled. </summary>
    Private _OpenDetectorEnabled As Boolean?

    ''' <summary> Gets or sets the cached Open Detector Enabled sentinel. </summary>
    ''' <value>
    ''' <c>null</c> if Open Detector Enabled is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>.
    ''' </value>
    Public Property OpenDetectorEnabled As Boolean?
        Get
            Return Me._OpenDetectorEnabled
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.OpenDetectorEnabled, value) Then
                Me._OpenDetectorEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Open Detector Enabled sentinel. </summary>
    ''' <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function ApplyOpenDetectorEnabled(ByVal value As Boolean) As Boolean?
        Me.WriteOpenDetectorEnabled(value)
        Return Me.QueryOpenDetectorEnabled()
    End Function

    ''' <summary> Gets the automatic Zero enabled query command. </summary>
    ''' <remarks> TSP: _G.print(_G.dmm.opendetector==1) </remarks>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overridable Property OpenDetectorEnabledQueryCommand As String

    ''' <summary>
    ''' Queries the Open Detector Enabled sentinel. Also sets the
    ''' <see cref="OpenDetectorEnabled">Enabled</see> sentinel.
    ''' </summary>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function QueryOpenDetectorEnabled() As Boolean?
        Me.OpenDetectorEnabled = Me.Query(Me.OpenDetectorEnabled, Me.OpenDetectorEnabledQueryCommand)
        Return Me.OpenDetectorEnabled
    End Function

    ''' <summary> Gets the automatic Zero enabled command Format. </summary>
    ''' <remarks> TSP: _G.opendetector={0:'1';'1';'0'}". </remarks>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overridable Property OpenDetectorEnabledCommandFormat As String

    ''' <summary>
    ''' Writes the Open Detector Enabled sentinel. Does not read back from the instrument.
    ''' </summary>
    ''' <param name="value"> if set to <c>True</c> is enabled. </param>
    ''' <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
    Public Function WriteOpenDetectorEnabled(ByVal value As Boolean) As Boolean?
        Me.OpenDetectorEnabled = Me.Write(value, Me.OpenDetectorEnabledCommandFormat)
        Return Me.OpenDetectorEnabled
    End Function

#End Region

#Region " POWER LINE CYCLES (NPLC) "

    ''' <summary> Gets the power line cycles decimal places. </summary>
    ''' <value> The power line decimal places. </value>
    Public ReadOnly Property PowerLineCyclesDecimalPlaces As Integer
        Get
            Return CInt(Math.Max(0, 1 - Math.Log10(Me.PowerLineCyclesRange.Min)))
        End Get
    End Property

    ''' <summary> The power line cycles range. </summary>
    Private _PowerLineCyclesRange As Core.Constructs.RangeR

    ''' <summary> The power line cycles range in units. </summary>
    ''' <value> The power line cycles range. </value>
    Public Property PowerLineCyclesRange As Core.Constructs.RangeR
        Get
            Return Me._PowerLineCyclesRange
        End Get
        Set(value As Core.Constructs.RangeR)
            If Me.PowerLineCyclesRange <> value Then
                Me._PowerLineCyclesRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The Power Line Cycles. </summary>
    Private _PowerLineCycles As Double?

    ''' <summary>
    ''' Gets or sets the cached sense PowerLineCycles. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property PowerLineCycles As Double?
        Get
            Return Me._PowerLineCycles
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.PowerLineCycles, value) Then
                Me._PowerLineCycles = value
                Me._Aperture = StatusSubsystemBase.FromPowerLineCycles(Me._PowerLineCycles.Value).TotalSeconds
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense PowerLineCycles. </summary>
    ''' <param name="value"> The Power Line Cycles. </param>
    ''' <returns> The Power Line Cycles. </returns>
    Public Function ApplyPowerLineCycles(ByVal value As Double) As Double?
        Me.WritePowerLineCycles(value)
        Return Me.QueryPowerLineCycles
    End Function

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overridable Property PowerLineCyclesQueryCommand As String

    ''' <summary> Queries The Power Line Cycles. </summary>
    ''' <returns> The Power Line Cycles or none if unknown. </returns>
    Public Function QueryPowerLineCycles() As Double?
        Me.PowerLineCycles = Me.Query(Me.PowerLineCycles, Me.PowerLineCyclesQueryCommand)
        Return Me.PowerLineCycles
    End Function

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overridable Property PowerLineCyclesCommandFormat As String

    ''' <summary>
    ''' Writes The Power Line Cycles without reading back the value from the device.
    ''' </summary>
    ''' <remarks> This command sets The Power Line Cycles. </remarks>
    ''' <param name="value"> The Power Line Cycles. </param>
    ''' <returns> The Power Line Cycles. </returns>
    Public Function WritePowerLineCycles(ByVal value As Double) As Double?
        Me.PowerLineCycles = Me.Write(value, Me.PowerLineCyclesCommandFormat)
        Return Me.PowerLineCycles
    End Function

#End Region

#Region " RANGE "

    ''' <summary> The Range. </summary>
    Private _Range As Double?

    ''' <summary>
    ''' Gets or sets the cached sense Range. Set to
    ''' <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
    ''' <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
    ''' </summary>
    ''' <remarks>
    ''' You can assign any real number using this command. The instrument selects the closest fixed
    ''' range that Is large enough to measure the entered number. For example, for current
    ''' measurements, if you expect a reading Of approximately 9 mA, Set the range To 9 mA To Select
    ''' the 10 mA range. When you read this setting, you see the positive full-scale value Of the
    ''' measurement range that the instrument Is presently using. This command Is primarily intended
    ''' To eliminate the time that Is required by the instrument To automatically search For a range.
    ''' When a range Is fixed, any signal greater than the entered range generates an overrange
    ''' condition. When an over-range condition occurs, the front panel displays "Overflow" And the
    ''' remote interface returns 9.9e+37.
    ''' </remarks>
    ''' <value> <c>null</c> if value is not known. </value>
    Public Overloads Property Range As Double?
        Get
            Return Me._Range
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.Range, value) Then
                Me._Range = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the sense Range. </summary>
    ''' <param name="value"> The Range. </param>
    ''' <returns> The Range. </returns>
    Public Function ApplyRange(ByVal value As Double) As Double?
        Me.WriteRange(value)
        Return Me.QueryRange
    End Function

    ''' <summary> Gets or sets The Range query command. </summary>
    ''' <value> The Range query command. </value>
    Protected Overridable Property RangeQueryCommand As String

    ''' <summary> Queries The Range. </summary>
    ''' <returns> The Range or none if unknown. </returns>
    Public Function QueryRange() As Double?
        Me.Range = Me.Query(Me.Range, Me.RangeQueryCommand)
        Return Me.Range
    End Function

    ''' <summary> Gets or sets The Range command format. </summary>
    ''' <value> The Range command format. </value>
    Protected Overridable Property RangeCommandFormat As String

    ''' <summary> Writes The Range without reading back the value from the device. </summary>
    ''' <remarks> This command sets The Range. </remarks>
    ''' <param name="value"> The Range. </param>
    ''' <returns> The Range. </returns>
    Public Function WriteRange(ByVal value As Double) As Double?
        Me.Range = Me.Write(value, Me.RangeCommandFormat)
        Return Me.Range
    End Function

#End Region

#Region " READ "

    ''' <summary> Gets or sets the read buffer query command format. </summary>
    ''' <value> The read buffer query command format. </value>
    Protected Overridable Property ReadBufferQueryCommandFormat As String

    ''' <summary> Queries buffer reading. </summary>
    ''' <remarks>
    ''' This command initiates measurements using the present function setting, stores the readings
    ''' in a reading buffer, and returns the last reading. The dmm.measure.count attribute determines
    ''' how many measurements are performed. When you use a reading buffer with a command Or action
    ''' that makes multiple readings, all readings are available In the reading buffer. However, only
    ''' the last reading Is returned As a reading With the command. If you define a specific reading
    ''' buffer, the reading buffer must exist before you make the measurement.
    ''' </remarks>
    ''' <param name="bufferName"> Name of the buffer. </param>
    ''' <returns> The reading or none if unknown. </returns>
    Public Overloads Function MeasureBuffer(ByVal bufferName As String) As Double?
        Dim value As String = Me.Query(Me.Session.EmulatedReply, String.Format(Me.ReadBufferQueryCommandFormat, bufferName))
        ' the emulator will set the last reading. 
        Return Me.ParsePrimaryReading(value)
    End Function

#End Region

End Class

''' <summary> Specifies the Auto Delay modes. </summary>
<Flags>
Public Enum MultimeterAutoDelayModes

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("No set ()")>
    None = 0

    ''' <summary> An enum constant representing the off] option. </summary>
    <ComponentModel.Description("Off (_G.dmm.OFF)")>
    [Off] = 1

    ''' <summary> An enum constant representing the on] option. </summary>
    <ComponentModel.Description("On (_G.dmm.ON)")>
    [On] = 2

    ''' <summary> An enum constant representing the once] option. </summary>
    <ComponentModel.Description("Once (_G.dmm.AUTODELAY_ONCE)")>
    [Once] = 4
End Enum

